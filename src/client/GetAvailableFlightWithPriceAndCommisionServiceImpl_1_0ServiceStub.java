/**
 * GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */
package client;

/*
*  GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub java implementation
*/

public class GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub extends org.apache.axis2.client.Stub {
    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();

    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();

    private java.util.HashMap faultMessageMap = new java.util.HashMap();

    private static int counter = 0;

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }
        counter = counter + 1;
        return java.lang.Long.toString(System.currentTimeMillis()) + "_" + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {

        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService(
                "GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0Service" + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                "getAvailableFlightWithPriceAndCommision"));
        _service.addOperation(__operation);

        _operations[0] = __operation;

    }

    //populates the faults
    private void populateFaults() {

    }

    /**
      *Constructor that takes in a configContext
      */

    public GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
      * Constructor that takes in a configContext  and useseperate listner
      */
    public GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint,
            boolean useSeparateListener) throws org.apache.axis2.AxisFault {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);

        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

    }

    /**
     * Default Constructor
     */
    public GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub(
            org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {

        this(configurationContext,
                "http://ws.tongyedns.com:8000/ltips/services/getAvailableFlightWithPriceAndCommisionService1.0");

    }

    /**
     * Default Constructor
     */
    public GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub() throws org.apache.axis2.AxisFault {

        this("http://ws.tongyedns.com:8000/ltips/services/getAvailableFlightWithPriceAndCommisionService1.0");

    }

    /**
     * Constructor taking the target endpoint
     */
    public GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub(java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    /**
     * Auto generated method signature
     * 
     * @see client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0Service#getAvailableFlightWithPriceAndCommision
     * @param getAvailableFlightWithPriceAndCommision
    
     */

    public client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE getAvailableFlightWithPriceAndCommision(

            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE getAvailableFlightWithPriceAndCommision)

    throws java.rmi.RemoteException

    {
        org.apache.axis2.context.MessageContext _messageContext = null;
        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0]
                    .getName());
            _operationClient
                    .getOptions()
                    .setAction(
                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/GetAvailableFlightWithPriceAndCommisionService_1_0/getAvailableFlightWithPriceAndCommision");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
            _operationClient.getOptions().setTimeOutInMilliSeconds(30000L);//默认30秒超时时间

            addPropertyToOperationClient(_operationClient,
                    org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(
                    getFactory(_operationClient.getOptions().getSoapVersionURI()),
                    getAvailableFlightWithPriceAndCommision,
                    optimizeContent(new javax.xml.namespace.QName(
                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                            "getAvailableFlightWithPriceAndCommision")));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient
                    .getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(
                    _returnEnv.getBody().getFirstElement(),
                    client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE) object;

        }
        catch (org.apache.axis2.AxisFault f) {

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(faultElt.getQName())) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                                .get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex = (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }
                    catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }
                else {
                    throw f;
                }
            }
            else {
                throw f;
            }
        }
        finally {
            _messageContext.getTransportOut().getSender().cleanup(_messageContext);
        }
    }

    /**
     *  A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private javax.xml.namespace.QName[] opNameArray = null;

    private boolean optimizeContent(javax.xml.namespace.QName opName) {

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }
        return false;
    }

    //http://ws.tongyedns.com:8000/ltips/services/getAvailableFlightWithPriceAndCommisionService1.0
    public static class AbstractLiantuoRequest implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = abstractLiantuoRequest
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AgencyCode
        */

        protected java.lang.String localAgencyCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAgencyCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getAgencyCode() {
            return localAgencyCode;
        }

        /**
           * Auto generated setter method
           * @param param AgencyCode
           */
        public void setAgencyCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localAgencyCodeTracker = true;
            }
            else {
                localAgencyCodeTracker = false;

            }

            this.localAgencyCode = param;

        }

        /**
        * field for Sign
        */

        protected java.lang.String localSign;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSignTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSign() {
            return localSign;
        }

        /**
           * Auto generated setter method
           * @param param Sign
           */
        public void setSign(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSignTracker = true;
            }
            else {
                localSignTracker = false;

            }

            this.localSign = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AbstractLiantuoRequest.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":abstractLiantuoRequest", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "abstractLiantuoRequest", xmlWriter);
                }

            }
            if (localAgencyCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "agencyCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "agencyCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("agencyCode");
                }

                if (localAgencyCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAgencyCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localSignTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "sign", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "sign");
                    }

                }
                else {
                    xmlWriter.writeStartElement("sign");
                }

                if (localSign == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSign);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAgencyCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "agencyCode"));

                if (localAgencyCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAgencyCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");
                }
            }
            if (localSignTracker) {
                elementList.add(new javax.xml.namespace.QName("", "sign"));

                if (localSign != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSign));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AbstractLiantuoRequest parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AbstractLiantuoRequest object = null;

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"abstractLiantuoRequest".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AbstractLiantuoRequest) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                    "The an abstract class can not be instantiated !!!");

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "agencyCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAgencyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "sign").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSign(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetAvailableFlightWithPriceAndCommisionE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                "getAvailableFlightWithPriceAndCommision", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetAvailableFlightWithPriceAndCommision
        */

        protected GetAvailableFlightWithPriceAndCommision localGetAvailableFlightWithPriceAndCommision;

        /**
        * Auto generated getter method
        * @return GetAvailableFlightWithPriceAndCommision
        */
        public GetAvailableFlightWithPriceAndCommision getGetAvailableFlightWithPriceAndCommision() {
            return localGetAvailableFlightWithPriceAndCommision;
        }

        /**
           * Auto generated setter method
           * @param param GetAvailableFlightWithPriceAndCommision
           */
        public void setGetAvailableFlightWithPriceAndCommision(GetAvailableFlightWithPriceAndCommision param) {

            this.localGetAvailableFlightWithPriceAndCommision = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetAvailableFlightWithPriceAndCommisionE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetAvailableFlightWithPriceAndCommision == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetAvailableFlightWithPriceAndCommision.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetAvailableFlightWithPriceAndCommision.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetAvailableFlightWithPriceAndCommisionE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetAvailableFlightWithPriceAndCommisionE object = new GetAvailableFlightWithPriceAndCommisionE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getAvailableFlightWithPriceAndCommision").equals(reader.getName())) {

                                object.setGetAvailableFlightWithPriceAndCommision(GetAvailableFlightWithPriceAndCommision.Factory
                                        .parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class WsSeatWithPriceAndComisionItem implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = wsSeatWithPriceAndComisionItem
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for Discount
        */

        protected double localDiscount;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDiscountTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getDiscount() {
            return localDiscount;
        }

        /**
           * Auto generated setter method
           * @param param Discount
           */
        public void setDiscount(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localDiscountTracker = false;

            }
            else {
                localDiscountTracker = true;
            }

            this.localDiscount = param;

        }

        /**
        * field for FlightNo
        */

        protected java.lang.String localFlightNo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightNoTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightNo() {
            return localFlightNo;
        }

        /**
           * Auto generated setter method
           * @param param FlightNo
           */
        public void setFlightNo(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightNoTracker = true;
            }
            else {
                localFlightNoTracker = false;

            }

            this.localFlightNo = param;

        }

        /**
        * field for ParPrice
        */

        protected int localParPrice;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParPriceTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getParPrice() {
            return localParPrice;
        }

        /**
           * Auto generated setter method
           * @param param ParPrice
           */
        public void setParPrice(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localParPriceTracker = false;

            }
            else {
                localParPriceTracker = true;
            }

            this.localParPrice = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PolicyData
        */

        protected WsPolicyData localPolicyData;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyDataTracker = false;

        /**
        * Auto generated getter method
        * @return WsPolicyData
        */
        public WsPolicyData getPolicyData() {
            return localPolicyData;
        }

        /**
           * Auto generated setter method
           * @param param PolicyData
           */
        public void setPolicyData(WsPolicyData param) {

            if (param != null) {
                //update the setting tracker
                localPolicyDataTracker = true;
            }
            else {
                localPolicyDataTracker = false;

            }

            this.localPolicyData = param;

        }

        /**
        * field for SeatCode
        */

        protected java.lang.String localSeatCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSeatCode() {
            return localSeatCode;
        }

        /**
           * Auto generated setter method
           * @param param SeatCode
           */
        public void setSeatCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSeatCodeTracker = true;
            }
            else {
                localSeatCodeTracker = false;

            }

            this.localSeatCode = param;

        }

        /**
        * field for SeatMsg
        */

        protected java.lang.String localSeatMsg;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatMsgTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSeatMsg() {
            return localSeatMsg;
        }

        /**
           * Auto generated setter method
           * @param param SeatMsg
           */
        public void setSeatMsg(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSeatMsgTracker = true;
            }
            else {
                localSeatMsgTracker = false;

            }

            this.localSeatMsg = param;

        }

        /**
        * field for SeatStatus
        */

        protected java.lang.String localSeatStatus;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatStatusTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getSeatStatus() {
            return localSeatStatus;
        }

        /**
           * Auto generated setter method
           * @param param SeatStatus
           */
        public void setSeatStatus(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localSeatStatusTracker = true;
            }
            else {
                localSeatStatusTracker = false;

            }

            this.localSeatStatus = param;

        }

        /**
        * field for SeatType
        */

        protected int localSeatType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatTypeTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getSeatType() {
            return localSeatType;
        }

        /**
           * Auto generated setter method
           * @param param SeatType
           */
        public void setSeatType(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localSeatTypeTracker = false;

            }
            else {
                localSeatTypeTracker = true;
            }

            this.localSeatType = param;

        }

        /**
        * field for SettlePrice
        */

        protected double localSettlePrice;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSettlePriceTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getSettlePrice() {
            return localSettlePrice;
        }

        /**
           * Auto generated setter method
           * @param param SettlePrice
           */
        public void setSettlePrice(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localSettlePriceTracker = false;

            }
            else {
                localSettlePriceTracker = true;
            }

            this.localSettlePrice = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    WsSeatWithPriceAndComisionItem.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":wsSeatWithPriceAndComisionItem", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "wsSeatWithPriceAndComisionItem", xmlWriter);
                }

            }
            if (localDiscountTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "discount", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "discount");
                    }

                }
                else {
                    xmlWriter.writeStartElement("discount");
                }

                if (java.lang.Double.isNaN(localDiscount)) {

                    throw new org.apache.axis2.databinding.ADBException("discount cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localDiscount));
                }

                xmlWriter.writeEndElement();
            }
            if (localFlightNoTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightNo", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightNo");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightNo");
                }

                if (localFlightNo == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightNo cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightNo);

                }

                xmlWriter.writeEndElement();
            }
            if (localParPriceTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "parPrice", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "parPrice");
                    }

                }
                else {
                    xmlWriter.writeStartElement("parPrice");
                }

                if (localParPrice == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("parPrice cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localParPrice));
                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyDataTracker) {
                if (localPolicyData == null) {
                    throw new org.apache.axis2.databinding.ADBException("policyData cannot be null!!");
                }
                localPolicyData.serialize(new javax.xml.namespace.QName("", "policyData"), factory, xmlWriter);
            }
            if (localSeatCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatCode");
                }

                if (localSeatCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("seatCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSeatCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatMsgTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatMsg", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatMsg");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatMsg");
                }

                if (localSeatMsg == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("seatMsg cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSeatMsg);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatStatusTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatStatus", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatStatus");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatStatus");
                }

                if (localSeatStatus == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("seatStatus cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSeatStatus);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatType");
                }

                if (localSeatType == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("seatType cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localSeatType));
                }

                xmlWriter.writeEndElement();
            }
            if (localSettlePriceTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "settlePrice", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "settlePrice");
                    }

                }
                else {
                    xmlWriter.writeStartElement("settlePrice");
                }

                if (java.lang.Double.isNaN(localSettlePrice)) {

                    throw new org.apache.axis2.databinding.ADBException("settlePrice cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localSettlePrice));
                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localDiscountTracker) {
                elementList.add(new javax.xml.namespace.QName("", "discount"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscount));
            }
            if (localFlightNoTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightNo"));

                if (localFlightNo != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFlightNo));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightNo cannot be null!!");
                }
            }
            if (localParPriceTracker) {
                elementList.add(new javax.xml.namespace.QName("", "parPrice"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParPrice));
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPolicyDataTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyData"));

                if (localPolicyData == null) {
                    throw new org.apache.axis2.databinding.ADBException("policyData cannot be null!!");
                }
                elementList.add(localPolicyData);
            }
            if (localSeatCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatCode"));

                if (localSeatCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("seatCode cannot be null!!");
                }
            }
            if (localSeatMsgTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatMsg"));

                if (localSeatMsg != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatMsg));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("seatMsg cannot be null!!");
                }
            }
            if (localSeatStatusTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatStatus"));

                if (localSeatStatus != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatStatus));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("seatStatus cannot be null!!");
                }
            }
            if (localSeatTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatType"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatType));
            }
            if (localSettlePriceTracker) {
                elementList.add(new javax.xml.namespace.QName("", "settlePrice"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSettlePrice));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static WsSeatWithPriceAndComisionItem parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                WsSeatWithPriceAndComisionItem object = new WsSeatWithPriceAndComisionItem();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"wsSeatWithPriceAndComisionItem".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsSeatWithPriceAndComisionItem) ExtensionMapper.getTypeObject(nsUri, type,
                                        reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "discount").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDiscount(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setDiscount(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightNo").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightNo(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "parPrice").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParPrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setParPrice(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyData").equals(reader.getName())) {

                        object.setPolicyData(WsPolicyData.Factory.parse(reader));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatMsg").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatMsg(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatStatus").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatStatus(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatType(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setSeatType(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "settlePrice").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSettlePrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setSettlePrice(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class WsFlightWithPriceAndCommision implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = wsFlightWithPriceAndCommision
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AirportTax
        */

        protected double localAirportTax;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAirportTaxTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getAirportTax() {
            return localAirportTax;
        }

        /**
           * Auto generated setter method
           * @param param AirportTax
           */
        public void setAirportTax(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localAirportTaxTracker = false;

            }
            else {
                localAirportTaxTracker = true;
            }

            this.localAirportTax = param;

        }

        /**
        * field for ArriModifyTime
        */

        protected java.lang.String localArriModifyTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localArriModifyTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getArriModifyTime() {
            return localArriModifyTime;
        }

        /**
           * Auto generated setter method
           * @param param ArriModifyTime
           */
        public void setArriModifyTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localArriModifyTimeTracker = true;
            }
            else {
                localArriModifyTimeTracker = false;

            }

            this.localArriModifyTime = param;

        }

        /**
        * field for ArriTime
        */

        protected java.lang.String localArriTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localArriTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getArriTime() {
            return localArriTime;
        }

        /**
           * Auto generated setter method
           * @param param ArriTime
           */
        public void setArriTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localArriTimeTracker = true;
            }
            else {
                localArriTimeTracker = false;

            }

            this.localArriTime = param;

        }

        /**
        * field for CodeShare
        */

        protected boolean localCodeShare;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCodeShareTracker = false;

        /**
        * Auto generated getter method
        * @return boolean
        */
        public boolean getCodeShare() {
            return localCodeShare;
        }

        /**
           * Auto generated setter method
           * @param param CodeShare
           */
        public void setCodeShare(boolean param) {

            // setting primitive attribute tracker to true

            if (false) {
                localCodeShareTracker = false;

            }
            else {
                localCodeShareTracker = true;
            }

            this.localCodeShare = param;

        }

        /**
        * field for DepModifyTime
        */

        protected java.lang.String localDepModifyTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDepModifyTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDepModifyTime() {
            return localDepModifyTime;
        }

        /**
           * Auto generated setter method
           * @param param DepModifyTime
           */
        public void setDepModifyTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDepModifyTimeTracker = true;
            }
            else {
                localDepModifyTimeTracker = false;

            }

            this.localDepModifyTime = param;

        }

        /**
        * field for DepTime
        */

        protected java.lang.String localDepTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDepTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDepTime() {
            return localDepTime;
        }

        /**
           * Auto generated setter method
           * @param param DepTime
           */
        public void setDepTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDepTimeTracker = true;
            }
            else {
                localDepTimeTracker = false;

            }

            this.localDepTime = param;

        }

        /**
        * field for DstCity
        */

        protected java.lang.String localDstCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDstCityTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDstCity() {
            return localDstCity;
        }

        /**
           * Auto generated setter method
           * @param param DstCity
           */
        public void setDstCity(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDstCityTracker = true;
            }
            else {
                localDstCityTracker = false;

            }

            this.localDstCity = param;

        }

        /**
        * field for DstJetquay
        */

        protected java.lang.String localDstJetquay;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDstJetquayTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDstJetquay() {
            return localDstJetquay;
        }

        /**
           * Auto generated setter method
           * @param param DstJetquay
           */
        public void setDstJetquay(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDstJetquayTracker = true;
            }
            else {
                localDstJetquayTracker = false;

            }

            this.localDstJetquay = param;

        }

        /**
        * field for FlightNo
        */

        protected java.lang.String localFlightNo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightNoTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getFlightNo() {
            return localFlightNo;
        }

        /**
           * Auto generated setter method
           * @param param FlightNo
           */
        public void setFlightNo(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localFlightNoTracker = true;
            }
            else {
                localFlightNoTracker = false;

            }

            this.localFlightNo = param;

        }

        /**
        * field for FuelTax
        */

        protected double localFuelTax;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFuelTaxTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getFuelTax() {
            return localFuelTax;
        }

        /**
           * Auto generated setter method
           * @param param FuelTax
           */
        public void setFuelTax(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localFuelTaxTracker = false;

            }
            else {
                localFuelTaxTracker = true;
            }

            this.localFuelTax = param;

        }

        /**
        * field for IsElectronicTicket
        */

        protected boolean localIsElectronicTicket;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localIsElectronicTicketTracker = false;

        /**
        * Auto generated getter method
        * @return boolean
        */
        public boolean getIsElectronicTicket() {
            return localIsElectronicTicket;
        }

        /**
           * Auto generated setter method
           * @param param IsElectronicTicket
           */
        public void setIsElectronicTicket(boolean param) {

            // setting primitive attribute tracker to true

            if (false) {
                localIsElectronicTicketTracker = false;

            }
            else {
                localIsElectronicTicketTracker = true;
            }

            this.localIsElectronicTicket = param;

        }

        /**
        * field for Link
        */

        protected java.lang.String localLink;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localLinkTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getLink() {
            return localLink;
        }

        /**
           * Auto generated setter method
           * @param param Link
           */
        public void setLink(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localLinkTracker = true;
            }
            else {
                localLinkTracker = false;

            }

            this.localLink = param;

        }

        /**
        * field for Meal
        */

        protected java.lang.String localMeal;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localMealTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getMeal() {
            return localMeal;
        }

        /**
           * Auto generated setter method
           * @param param Meal
           */
        public void setMeal(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localMealTracker = true;
            }
            else {
                localMealTracker = false;

            }

            this.localMeal = param;

        }

        /**
        * field for OrgCity
        */

        protected java.lang.String localOrgCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOrgCityTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getOrgCity() {
            return localOrgCity;
        }

        /**
           * Auto generated setter method
           * @param param OrgCity
           */
        public void setOrgCity(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localOrgCityTracker = true;
            }
            else {
                localOrgCityTracker = false;

            }

            this.localOrgCity = param;

        }

        /**
        * field for OrgJetquay
        */

        protected java.lang.String localOrgJetquay;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOrgJetquayTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getOrgJetquay() {
            return localOrgJetquay;
        }

        /**
           * Auto generated setter method
           * @param param OrgJetquay
           */
        public void setOrgJetquay(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localOrgJetquayTracker = true;
            }
            else {
                localOrgJetquayTracker = false;

            }

            this.localOrgJetquay = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PlaneType
        */

        protected java.lang.String localPlaneType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPlaneTypeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPlaneType() {
            return localPlaneType;
        }

        /**
           * Auto generated setter method
           * @param param PlaneType
           */
        public void setPlaneType(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPlaneTypeTracker = true;
            }
            else {
                localPlaneTypeTracker = false;

            }

            this.localPlaneType = param;

        }

        /**
        * field for SeatItems
        * This was an Array!
        */

        protected WsSeatWithPriceAndComisionItem[] localSeatItems;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatItemsTracker = false;

        /**
        * Auto generated getter method
        * @return WsSeatWithPriceAndComisionItem[]
        */
        public WsSeatWithPriceAndComisionItem[] getSeatItems() {
            return localSeatItems;
        }

        /**
         * validate the array for SeatItems
         */
        protected void validateSeatItems(WsSeatWithPriceAndComisionItem[] param) {

        }

        /**
         * Auto generated setter method
         * @param param SeatItems
         */
        public void setSeatItems(WsSeatWithPriceAndComisionItem[] param) {

            validateSeatItems(param);

            if (param != null) {
                //update the setting tracker
                localSeatItemsTracker = true;
            }
            else {
                localSeatItemsTracker = true;

            }

            this.localSeatItems = param;
        }

        /**
        * Auto generated add method for the array for convenience
        * @param param WsSeatWithPriceAndComisionItem
        */
        public void addSeatItems(WsSeatWithPriceAndComisionItem param) {
            if (localSeatItems == null) {
                localSeatItems = new WsSeatWithPriceAndComisionItem[] {};
            }

            //update the setting tracker
            localSeatItemsTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localSeatItems);
            list.add(param);
            this.localSeatItems = (WsSeatWithPriceAndComisionItem[]) list
                    .toArray(new WsSeatWithPriceAndComisionItem[list.size()]);

        }

        /**
        * field for ShareNum
        */

        protected java.lang.String localShareNum;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localShareNumTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getShareNum() {
            return localShareNum;
        }

        /**
           * Auto generated setter method
           * @param param ShareNum
           */
        public void setShareNum(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localShareNumTracker = true;
            }
            else {
                localShareNumTracker = false;

            }

            this.localShareNum = param;

        }

        /**
        * field for Stopnum
        */

        protected int localStopnum;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localStopnumTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getStopnum() {
            return localStopnum;
        }

        /**
           * Auto generated setter method
           * @param param Stopnum
           */
        public void setStopnum(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localStopnumTracker = false;

            }
            else {
                localStopnumTracker = true;
            }

            this.localStopnum = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    WsFlightWithPriceAndCommision.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":wsFlightWithPriceAndCommision", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "wsFlightWithPriceAndCommision", xmlWriter);
                }

            }
            if (localAirportTaxTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "airportTax", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "airportTax");
                    }

                }
                else {
                    xmlWriter.writeStartElement("airportTax");
                }

                if (java.lang.Double.isNaN(localAirportTax)) {

                    throw new org.apache.axis2.databinding.ADBException("airportTax cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localAirportTax));
                }

                xmlWriter.writeEndElement();
            }
            if (localArriModifyTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "arriModifyTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "arriModifyTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("arriModifyTime");
                }

                if (localArriModifyTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("arriModifyTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localArriModifyTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localArriTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "arriTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "arriTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("arriTime");
                }

                if (localArriTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("arriTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localArriTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localCodeShareTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "codeShare", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "codeShare");
                    }

                }
                else {
                    xmlWriter.writeStartElement("codeShare");
                }

                if (false) {

                    throw new org.apache.axis2.databinding.ADBException("codeShare cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCodeShare));
                }

                xmlWriter.writeEndElement();
            }
            if (localDepModifyTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "depModifyTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "depModifyTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("depModifyTime");
                }

                if (localDepModifyTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("depModifyTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDepModifyTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localDepTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "depTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "depTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("depTime");
                }

                if (localDepTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("depTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDepTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localDstCityTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "dstCity", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "dstCity");
                    }

                }
                else {
                    xmlWriter.writeStartElement("dstCity");
                }

                if (localDstCity == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("dstCity cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDstCity);

                }

                xmlWriter.writeEndElement();
            }
            if (localDstJetquayTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "dstJetquay", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "dstJetquay");
                    }

                }
                else {
                    xmlWriter.writeStartElement("dstJetquay");
                }

                if (localDstJetquay == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("dstJetquay cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDstJetquay);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightNoTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "flightNo", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "flightNo");
                    }

                }
                else {
                    xmlWriter.writeStartElement("flightNo");
                }

                if (localFlightNo == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("flightNo cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localFlightNo);

                }

                xmlWriter.writeEndElement();
            }
            if (localFuelTaxTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "fuelTax", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "fuelTax");
                    }

                }
                else {
                    xmlWriter.writeStartElement("fuelTax");
                }

                if (java.lang.Double.isNaN(localFuelTax)) {

                    throw new org.apache.axis2.databinding.ADBException("fuelTax cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localFuelTax));
                }

                xmlWriter.writeEndElement();
            }
            if (localIsElectronicTicketTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "isElectronicTicket", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "isElectronicTicket");
                    }

                }
                else {
                    xmlWriter.writeStartElement("isElectronicTicket");
                }

                if (false) {

                    throw new org.apache.axis2.databinding.ADBException("isElectronicTicket cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localIsElectronicTicket));
                }

                xmlWriter.writeEndElement();
            }
            if (localLinkTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "link", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "link");
                    }

                }
                else {
                    xmlWriter.writeStartElement("link");
                }

                if (localLink == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("link cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localLink);

                }

                xmlWriter.writeEndElement();
            }
            if (localMealTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "meal", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "meal");
                    }

                }
                else {
                    xmlWriter.writeStartElement("meal");
                }

                if (localMeal == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("meal cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localMeal);

                }

                xmlWriter.writeEndElement();
            }
            if (localOrgCityTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "orgCity", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "orgCity");
                    }

                }
                else {
                    xmlWriter.writeStartElement("orgCity");
                }

                if (localOrgCity == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("orgCity cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localOrgCity);

                }

                xmlWriter.writeEndElement();
            }
            if (localOrgJetquayTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "orgJetquay", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "orgJetquay");
                    }

                }
                else {
                    xmlWriter.writeStartElement("orgJetquay");
                }

                if (localOrgJetquay == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("orgJetquay cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localOrgJetquay);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPlaneTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "planeType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "planeType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("planeType");
                }

                if (localPlaneType == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("planeType cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPlaneType);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatItemsTracker) {
                if (localSeatItems != null) {
                    for (int i = 0; i < localSeatItems.length; i++) {
                        if (localSeatItems[i] != null) {
                            localSeatItems[i].serialize(new javax.xml.namespace.QName("", "seatItems"), factory,
                                    xmlWriter);
                        }
                        else {

                            // write null attribute
                            java.lang.String namespace2 = "";
                            if (!namespace2.equals("")) {
                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                if (prefix2 == null) {
                                    prefix2 = generatePrefix(namespace2);

                                    xmlWriter.writeStartElement(prefix2, "seatItems", namespace2);
                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                    xmlWriter.setPrefix(prefix2, namespace2);

                                }
                                else {
                                    xmlWriter.writeStartElement(namespace2, "seatItems");
                                }

                            }
                            else {
                                xmlWriter.writeStartElement("seatItems");
                            }

                            // write the nil attribute
                            writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();

                        }

                    }
                }
                else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "seatItems", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        }
                        else {
                            xmlWriter.writeStartElement(namespace2, "seatItems");
                        }

                    }
                    else {
                        xmlWriter.writeStartElement("seatItems");
                    }

                    // write the nil attribute
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }
            }
            if (localShareNumTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "shareNum", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "shareNum");
                    }

                }
                else {
                    xmlWriter.writeStartElement("shareNum");
                }

                if (localShareNum == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("shareNum cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localShareNum);

                }

                xmlWriter.writeEndElement();
            }
            if (localStopnumTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "stopnum", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "stopnum");
                    }

                }
                else {
                    xmlWriter.writeStartElement("stopnum");
                }

                if (localStopnum == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("stopnum cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localStopnum));
                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAirportTaxTracker) {
                elementList.add(new javax.xml.namespace.QName("", "airportTax"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAirportTax));
            }
            if (localArriModifyTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "arriModifyTime"));

                if (localArriModifyTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localArriModifyTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("arriModifyTime cannot be null!!");
                }
            }
            if (localArriTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "arriTime"));

                if (localArriTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArriTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("arriTime cannot be null!!");
                }
            }
            if (localCodeShareTracker) {
                elementList.add(new javax.xml.namespace.QName("", "codeShare"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodeShare));
            }
            if (localDepModifyTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "depModifyTime"));

                if (localDepModifyTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localDepModifyTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("depModifyTime cannot be null!!");
                }
            }
            if (localDepTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "depTime"));

                if (localDepTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDepTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("depTime cannot be null!!");
                }
            }
            if (localDstCityTracker) {
                elementList.add(new javax.xml.namespace.QName("", "dstCity"));

                if (localDstCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDstCity));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("dstCity cannot be null!!");
                }
            }
            if (localDstJetquayTracker) {
                elementList.add(new javax.xml.namespace.QName("", "dstJetquay"));

                if (localDstJetquay != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDstJetquay));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("dstJetquay cannot be null!!");
                }
            }
            if (localFlightNoTracker) {
                elementList.add(new javax.xml.namespace.QName("", "flightNo"));

                if (localFlightNo != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFlightNo));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("flightNo cannot be null!!");
                }
            }
            if (localFuelTaxTracker) {
                elementList.add(new javax.xml.namespace.QName("", "fuelTax"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFuelTax));
            }
            if (localIsElectronicTicketTracker) {
                elementList.add(new javax.xml.namespace.QName("", "isElectronicTicket"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localIsElectronicTicket));
            }
            if (localLinkTracker) {
                elementList.add(new javax.xml.namespace.QName("", "link"));

                if (localLink != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLink));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("link cannot be null!!");
                }
            }
            if (localMealTracker) {
                elementList.add(new javax.xml.namespace.QName("", "meal"));

                if (localMeal != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMeal));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("meal cannot be null!!");
                }
            }
            if (localOrgCityTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orgCity"));

                if (localOrgCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrgCity));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("orgCity cannot be null!!");
                }
            }
            if (localOrgJetquayTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orgJetquay"));

                if (localOrgJetquay != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrgJetquay));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("orgJetquay cannot be null!!");
                }
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPlaneTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "planeType"));

                if (localPlaneType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPlaneType));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("planeType cannot be null!!");
                }
            }
            if (localSeatItemsTracker) {
                if (localSeatItems != null) {
                    for (int i = 0; i < localSeatItems.length; i++) {

                        if (localSeatItems[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("", "seatItems"));
                            elementList.add(localSeatItems[i]);
                        }
                        else {

                            elementList.add(new javax.xml.namespace.QName("", "seatItems"));
                            elementList.add(null);

                        }

                    }
                }
                else {

                    elementList.add(new javax.xml.namespace.QName("", "seatItems"));
                    elementList.add(localSeatItems);

                }

            }
            if (localShareNumTracker) {
                elementList.add(new javax.xml.namespace.QName("", "shareNum"));

                if (localShareNum != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShareNum));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("shareNum cannot be null!!");
                }
            }
            if (localStopnumTracker) {
                elementList.add(new javax.xml.namespace.QName("", "stopnum"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStopnum));
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static WsFlightWithPriceAndCommision parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                WsFlightWithPriceAndCommision object = new WsFlightWithPriceAndCommision();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"wsFlightWithPriceAndCommision".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsFlightWithPriceAndCommision) ExtensionMapper.getTypeObject(nsUri, type,
                                        reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list21 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "airportTax").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAirportTax(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setAirportTax(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "arriModifyTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setArriModifyTime(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "arriTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setArriTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "codeShare").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCodeShare(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "depModifyTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDepModifyTime(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "depTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDepTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "dstCity").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDstCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "dstJetquay").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDstJetquay(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightNo").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFlightNo(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "fuelTax").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setFuelTax(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setFuelTax(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "isElectronicTicket").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setIsElectronicTicket(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToBoolean(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "link").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setLink(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "meal").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setMeal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "orgCity").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOrgCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "orgJetquay").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOrgJetquay(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "planeType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPlaneType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatItems").equals(reader.getName())) {

                        // Process the array and step past its final element's end.

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                            list21.add(null);
                            reader.next();
                        }
                        else {
                            list21.add(WsSeatWithPriceAndComisionItem.Factory.parse(reader));
                        }
                        //loop until we find a start element that is not part of this array
                        boolean loopDone21 = false;
                        while (!loopDone21) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone21 = true;
                            }
                            else {
                                if (new javax.xml.namespace.QName("", "seatItems").equals(reader.getName())) {

                                    nillableValue = reader.getAttributeValue(
                                            "http://www.w3.org/2001/XMLSchema-instance", "nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                                        list21.add(null);
                                        reader.next();
                                    }
                                    else {
                                        list21.add(WsSeatWithPriceAndComisionItem.Factory.parse(reader));
                                    }
                                }
                                else {
                                    loopDone21 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setSeatItems((WsSeatWithPriceAndComisionItem[]) org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToArray(WsSeatWithPriceAndComisionItem.class, list21));

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "shareNum").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setShareNum(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "stopnum").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setStopnum(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setStopnum(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class WsFlightWithPriceAndCommisionItem implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = wsFlightWithPriceAndCommisionItem
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AudletAirportTax
        */

        protected double localAudletAirportTax;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAudletAirportTaxTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getAudletAirportTax() {
            return localAudletAirportTax;
        }

        /**
           * Auto generated setter method
           * @param param AudletAirportTax
           */
        public void setAudletAirportTax(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localAudletAirportTaxTracker = false;

            }
            else {
                localAudletAirportTaxTracker = true;
            }

            this.localAudletAirportTax = param;

        }

        /**
        * field for AudletFuelTax
        */

        protected double localAudletFuelTax;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAudletFuelTaxTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getAudletFuelTax() {
            return localAudletFuelTax;
        }

        /**
           * Auto generated setter method
           * @param param AudletFuelTax
           */
        public void setAudletFuelTax(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localAudletFuelTaxTracker = false;

            }
            else {
                localAudletFuelTaxTracker = true;
            }

            this.localAudletFuelTax = param;

        }

        /**
        * field for BasePrice
        */

        protected double localBasePrice;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localBasePriceTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getBasePrice() {
            return localBasePrice;
        }

        /**
           * Auto generated setter method
           * @param param BasePrice
           */
        public void setBasePrice(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localBasePriceTracker = false;

            }
            else {
                localBasePriceTracker = true;
            }

            this.localBasePrice = param;

        }

        /**
        * field for Date
        */

        protected java.lang.String localDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDate() {
            return localDate;
        }

        /**
           * Auto generated setter method
           * @param param Date
           */
        public void setDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDateTracker = true;
            }
            else {
                localDateTracker = false;

            }

            this.localDate = param;

        }

        /**
        * field for Distance
        */

        protected double localDistance;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDistanceTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getDistance() {
            return localDistance;
        }

        /**
           * Auto generated setter method
           * @param param Distance
           */
        public void setDistance(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localDistanceTracker = false;

            }
            else {
                localDistanceTracker = true;
            }

            this.localDistance = param;

        }

        /**
        * field for DstCity
        */

        protected java.lang.String localDstCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDstCityTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDstCity() {
            return localDstCity;
        }

        /**
           * Auto generated setter method
           * @param param DstCity
           */
        public void setDstCity(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDstCityTracker = true;
            }
            else {
                localDstCityTracker = false;

            }

            this.localDstCity = param;

        }

        /**
        * field for Flights
        * This was an Array!
        */

        protected WsFlightWithPriceAndCommision[] localFlights;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightsTracker = false;

        /**
        * Auto generated getter method
        * @return WsFlightWithPriceAndCommision[]
        */
        public WsFlightWithPriceAndCommision[] getFlights() {
            return localFlights;
        }

        /**
         * validate the array for Flights
         */
        protected void validateFlights(WsFlightWithPriceAndCommision[] param) {

        }

        /**
         * Auto generated setter method
         * @param param Flights
         */
        public void setFlights(WsFlightWithPriceAndCommision[] param) {

            validateFlights(param);

            if (param != null) {
                //update the setting tracker
                localFlightsTracker = true;
            }
            else {
                localFlightsTracker = true;

            }

            this.localFlights = param;
        }

        /**
        * Auto generated add method for the array for convenience
        * @param param WsFlightWithPriceAndCommision
        */
        public void addFlights(WsFlightWithPriceAndCommision param) {
            if (localFlights == null) {
                localFlights = new WsFlightWithPriceAndCommision[] {};
            }

            //update the setting tracker
            localFlightsTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localFlights);
            list.add(param);
            this.localFlights = (WsFlightWithPriceAndCommision[]) list.toArray(new WsFlightWithPriceAndCommision[list
                    .size()]);

        }

        /**
        * field for OrgCity
        */

        protected java.lang.String localOrgCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOrgCityTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getOrgCity() {
            return localOrgCity;
        }

        /**
           * Auto generated setter method
           * @param param OrgCity
           */
        public void setOrgCity(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localOrgCityTracker = true;
            }
            else {
                localOrgCityTracker = false;

            }

            this.localOrgCity = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    WsFlightWithPriceAndCommisionItem.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":wsFlightWithPriceAndCommisionItem", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "wsFlightWithPriceAndCommisionItem", xmlWriter);
                }

            }
            if (localAudletAirportTaxTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "audletAirportTax", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "audletAirportTax");
                    }

                }
                else {
                    xmlWriter.writeStartElement("audletAirportTax");
                }

                if (java.lang.Double.isNaN(localAudletAirportTax)) {

                    throw new org.apache.axis2.databinding.ADBException("audletAirportTax cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localAudletAirportTax));
                }

                xmlWriter.writeEndElement();
            }
            if (localAudletFuelTaxTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "audletFuelTax", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "audletFuelTax");
                    }

                }
                else {
                    xmlWriter.writeStartElement("audletFuelTax");
                }

                if (java.lang.Double.isNaN(localAudletFuelTax)) {

                    throw new org.apache.axis2.databinding.ADBException("audletFuelTax cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localAudletFuelTax));
                }

                xmlWriter.writeEndElement();
            }
            if (localBasePriceTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "basePrice", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "basePrice");
                    }

                }
                else {
                    xmlWriter.writeStartElement("basePrice");
                }

                if (java.lang.Double.isNaN(localBasePrice)) {

                    throw new org.apache.axis2.databinding.ADBException("basePrice cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localBasePrice));
                }

                xmlWriter.writeEndElement();
            }
            if (localDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "date", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "date");
                    }

                }
                else {
                    xmlWriter.writeStartElement("date");
                }

                if (localDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localDistanceTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "distance", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "distance");
                    }

                }
                else {
                    xmlWriter.writeStartElement("distance");
                }

                if (java.lang.Double.isNaN(localDistance)) {

                    throw new org.apache.axis2.databinding.ADBException("distance cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localDistance));
                }

                xmlWriter.writeEndElement();
            }
            if (localDstCityTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "dstCity", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "dstCity");
                    }

                }
                else {
                    xmlWriter.writeStartElement("dstCity");
                }

                if (localDstCity == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("dstCity cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDstCity);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightsTracker) {
                if (localFlights != null) {
                    for (int i = 0; i < localFlights.length; i++) {
                        if (localFlights[i] != null) {
                            localFlights[i].serialize(new javax.xml.namespace.QName("", "flights"), factory, xmlWriter);
                        }
                        else {

                            // write null attribute
                            java.lang.String namespace2 = "";
                            if (!namespace2.equals("")) {
                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                if (prefix2 == null) {
                                    prefix2 = generatePrefix(namespace2);

                                    xmlWriter.writeStartElement(prefix2, "flights", namespace2);
                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                    xmlWriter.setPrefix(prefix2, namespace2);

                                }
                                else {
                                    xmlWriter.writeStartElement(namespace2, "flights");
                                }

                            }
                            else {
                                xmlWriter.writeStartElement("flights");
                            }

                            // write the nil attribute
                            writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();

                        }

                    }
                }
                else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "flights", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        }
                        else {
                            xmlWriter.writeStartElement(namespace2, "flights");
                        }

                    }
                    else {
                        xmlWriter.writeStartElement("flights");
                    }

                    // write the nil attribute
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }
            }
            if (localOrgCityTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "orgCity", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "orgCity");
                    }

                }
                else {
                    xmlWriter.writeStartElement("orgCity");
                }

                if (localOrgCity == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("orgCity cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localOrgCity);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAudletAirportTaxTracker) {
                elementList.add(new javax.xml.namespace.QName("", "audletAirportTax"));

                elementList
                        .add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAudletAirportTax));
            }
            if (localAudletFuelTaxTracker) {
                elementList.add(new javax.xml.namespace.QName("", "audletFuelTax"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAudletFuelTax));
            }
            if (localBasePriceTracker) {
                elementList.add(new javax.xml.namespace.QName("", "basePrice"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBasePrice));
            }
            if (localDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "date"));

                if (localDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");
                }
            }
            if (localDistanceTracker) {
                elementList.add(new javax.xml.namespace.QName("", "distance"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDistance));
            }
            if (localDstCityTracker) {
                elementList.add(new javax.xml.namespace.QName("", "dstCity"));

                if (localDstCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDstCity));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("dstCity cannot be null!!");
                }
            }
            if (localFlightsTracker) {
                if (localFlights != null) {
                    for (int i = 0; i < localFlights.length; i++) {

                        if (localFlights[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("", "flights"));
                            elementList.add(localFlights[i]);
                        }
                        else {

                            elementList.add(new javax.xml.namespace.QName("", "flights"));
                            elementList.add(null);

                        }

                    }
                }
                else {

                    elementList.add(new javax.xml.namespace.QName("", "flights"));
                    elementList.add(localFlights);

                }

            }
            if (localOrgCityTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orgCity"));

                if (localOrgCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrgCity));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("orgCity cannot be null!!");
                }
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static WsFlightWithPriceAndCommisionItem parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                WsFlightWithPriceAndCommisionItem object = new WsFlightWithPriceAndCommisionItem();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"wsFlightWithPriceAndCommisionItem".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsFlightWithPriceAndCommisionItem) ExtensionMapper.getTypeObject(nsUri, type,
                                        reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list7 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "audletAirportTax").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAudletAirportTax(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setAudletAirportTax(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "audletFuelTax").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAudletFuelTax(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setAudletFuelTax(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "basePrice").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setBasePrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setBasePrice(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "date").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "distance").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDistance(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setDistance(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "dstCity").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDstCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flights").equals(reader.getName())) {

                        // Process the array and step past its final element's end.

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                            list7.add(null);
                            reader.next();
                        }
                        else {
                            list7.add(WsFlightWithPriceAndCommision.Factory.parse(reader));
                        }
                        //loop until we find a start element that is not part of this array
                        boolean loopDone7 = false;
                        while (!loopDone7) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone7 = true;
                            }
                            else {
                                if (new javax.xml.namespace.QName("", "flights").equals(reader.getName())) {

                                    nillableValue = reader.getAttributeValue(
                                            "http://www.w3.org/2001/XMLSchema-instance", "nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                                        list7.add(null);
                                        reader.next();
                                    }
                                    else {
                                        list7.add(WsFlightWithPriceAndCommision.Factory.parse(reader));
                                    }
                                }
                                else {
                                    loopDone7 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setFlights((WsFlightWithPriceAndCommision[]) org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToArray(WsFlightWithPriceAndCommision.class, list7));

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "orgCity").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOrgCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AvailableFlightWithPriceAndCommisionRequestE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                "availableFlightWithPriceAndCommisionRequest", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AvailableFlightWithPriceAndCommisionRequest
        */

        protected AvailableFlightWithPriceAndCommisionRequest localAvailableFlightWithPriceAndCommisionRequest;

        /**
        * Auto generated getter method
        * @return AvailableFlightWithPriceAndCommisionRequest
        */
        public AvailableFlightWithPriceAndCommisionRequest getAvailableFlightWithPriceAndCommisionRequest() {
            return localAvailableFlightWithPriceAndCommisionRequest;
        }

        /**
           * Auto generated setter method
           * @param param AvailableFlightWithPriceAndCommisionRequest
           */
        public void setAvailableFlightWithPriceAndCommisionRequest(AvailableFlightWithPriceAndCommisionRequest param) {

            this.localAvailableFlightWithPriceAndCommisionRequest = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AvailableFlightWithPriceAndCommisionRequestE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localAvailableFlightWithPriceAndCommisionRequest == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localAvailableFlightWithPriceAndCommisionRequest.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localAvailableFlightWithPriceAndCommisionRequest.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AvailableFlightWithPriceAndCommisionRequestE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AvailableFlightWithPriceAndCommisionRequestE object = new AvailableFlightWithPriceAndCommisionRequestE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "availableFlightWithPriceAndCommisionRequest").equals(reader.getName())) {

                                object.setAvailableFlightWithPriceAndCommisionRequest(AvailableFlightWithPriceAndCommisionRequest.Factory
                                        .parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AvailableFlightWithPriceAndCommisionReply extends AbstractLiantuoReply implements
            org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = availableFlightWithPriceAndCommisionReply
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for FlightItems
        * This was an Array!
        */

        protected WsFlightWithPriceAndCommisionItem[] localFlightItems;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localFlightItemsTracker = false;

        /**
        * Auto generated getter method
        * @return WsFlightWithPriceAndCommisionItem[]
        */
        public WsFlightWithPriceAndCommisionItem[] getFlightItems() {
            return localFlightItems;
        }

        /**
         * validate the array for FlightItems
         */
        protected void validateFlightItems(WsFlightWithPriceAndCommisionItem[] param) {

        }

        /**
         * Auto generated setter method
         * @param param FlightItems
         */
        public void setFlightItems(WsFlightWithPriceAndCommisionItem[] param) {

            validateFlightItems(param);

            if (param != null) {
                //update the setting tracker
                localFlightItemsTracker = true;
            }
            else {
                localFlightItemsTracker = true;

            }

            this.localFlightItems = param;
        }

        /**
        * Auto generated add method for the array for convenience
        * @param param WsFlightWithPriceAndCommisionItem
        */
        public void addFlightItems(WsFlightWithPriceAndCommisionItem param) {
            if (localFlightItems == null) {
                localFlightItems = new WsFlightWithPriceAndCommisionItem[] {};
            }

            //update the setting tracker
            localFlightItemsTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localFlightItems);
            list.add(param);
            this.localFlightItems = (WsFlightWithPriceAndCommisionItem[]) list
                    .toArray(new WsFlightWithPriceAndCommisionItem[list.size()]);

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AvailableFlightWithPriceAndCommisionReply.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
            if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                        + ":availableFlightWithPriceAndCommisionReply", xmlWriter);
            }
            else {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "availableFlightWithPriceAndCommisionReply", xmlWriter);
            }

            if (localReturnCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnCode");
                }

                if (localReturnCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localReturnMessageTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnMessage", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnMessage");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnMessage");
                }

                if (localReturnMessage == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnMessage);

                }

                xmlWriter.writeEndElement();
            }
            if (localFlightItemsTracker) {
                if (localFlightItems != null) {
                    for (int i = 0; i < localFlightItems.length; i++) {
                        if (localFlightItems[i] != null) {
                            localFlightItems[i].serialize(new javax.xml.namespace.QName("", "flightItems"), factory,
                                    xmlWriter);
                        }
                        else {

                            // write null attribute
                            java.lang.String namespace2 = "";
                            if (!namespace2.equals("")) {
                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                if (prefix2 == null) {
                                    prefix2 = generatePrefix(namespace2);

                                    xmlWriter.writeStartElement(prefix2, "flightItems", namespace2);
                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                    xmlWriter.setPrefix(prefix2, namespace2);

                                }
                                else {
                                    xmlWriter.writeStartElement(namespace2, "flightItems");
                                }

                            }
                            else {
                                xmlWriter.writeStartElement("flightItems");
                            }

                            // write the nil attribute
                            writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();

                        }

                    }
                }
                else {

                    // write null attribute
                    java.lang.String namespace2 = "";
                    if (!namespace2.equals("")) {
                        java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                        if (prefix2 == null) {
                            prefix2 = generatePrefix(namespace2);

                            xmlWriter.writeStartElement(prefix2, "flightItems", namespace2);
                            xmlWriter.writeNamespace(prefix2, namespace2);
                            xmlWriter.setPrefix(prefix2, namespace2);

                        }
                        else {
                            xmlWriter.writeStartElement(namespace2, "flightItems");
                        }

                    }
                    else {
                        xmlWriter.writeStartElement("flightItems");
                    }

                    // write the nil attribute
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
                    xmlWriter.writeEndElement();

                }
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList
                    .add(new javax.xml.namespace.QName(
                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                            "availableFlightWithPriceAndCommisionReply"));
            if (localReturnCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnCode"));

                if (localReturnCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");
                }
            }
            if (localReturnMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnMessage"));

                if (localReturnMessage != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localReturnMessage));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");
                }
            }
            if (localFlightItemsTracker) {
                if (localFlightItems != null) {
                    for (int i = 0; i < localFlightItems.length; i++) {

                        if (localFlightItems[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("", "flightItems"));
                            elementList.add(localFlightItems[i]);
                        }
                        else {

                            elementList.add(new javax.xml.namespace.QName("", "flightItems"));
                            elementList.add(null);

                        }

                    }
                }
                else {

                    elementList.add(new javax.xml.namespace.QName("", "flightItems"));
                    elementList.add(localFlightItems);

                }

            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AvailableFlightWithPriceAndCommisionReply parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AvailableFlightWithPriceAndCommisionReply object = new AvailableFlightWithPriceAndCommisionReply();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"availableFlightWithPriceAndCommisionReply".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AvailableFlightWithPriceAndCommisionReply) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list3 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnMessage").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnMessage(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "flightItems").equals(reader.getName())) {

                        // Process the array and step past its final element's end.

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                            list3.add(null);
                            reader.next();
                        }
                        else {
                            list3.add(WsFlightWithPriceAndCommisionItem.Factory.parse(reader));
                        }
                        //loop until we find a start element that is not part of this array
                        boolean loopDone3 = false;
                        while (!loopDone3) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone3 = true;
                            }
                            else {
                                if (new javax.xml.namespace.QName("", "flightItems").equals(reader.getName())) {

                                    nillableValue = reader.getAttributeValue(
                                            "http://www.w3.org/2001/XMLSchema-instance", "nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
                                        list3.add(null);
                                        reader.next();
                                    }
                                    else {
                                        list3.add(WsFlightWithPriceAndCommisionItem.Factory.parse(reader));
                                    }
                                }
                                else {
                                    loopDone3 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setFlightItems((WsFlightWithPriceAndCommisionItem[]) org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToArray(WsFlightWithPriceAndCommisionItem.class, list3));

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AvailableFlightWithPriceAndCommisionRequest extends AbstractLiantuoRequest implements
            org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = availableFlightWithPriceAndCommisionRequest
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AirlineCode
        */

        protected java.lang.String localAirlineCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localAirlineCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getAirlineCode() {
            return localAirlineCode;
        }

        /**
           * Auto generated setter method
           * @param param AirlineCode
           */
        public void setAirlineCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localAirlineCodeTracker = true;
            }
            else {
                localAirlineCodeTracker = false;

            }

            this.localAirlineCode = param;

        }

        /**
        * field for Date
        */

        protected java.lang.String localDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDateTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDate() {
            return localDate;
        }

        /**
           * Auto generated setter method
           * @param param Date
           */
        public void setDate(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDateTracker = true;
            }
            else {
                localDateTracker = false;

            }

            this.localDate = param;

        }

        /**
        * field for DepTime
        */

        protected java.lang.String localDepTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDepTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDepTime() {
            return localDepTime;
        }

        /**
           * Auto generated setter method
           * @param param DepTime
           */
        public void setDepTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDepTimeTracker = true;
            }
            else {
                localDepTimeTracker = false;

            }

            this.localDepTime = param;

        }

        /**
        * field for Direct
        */

        protected java.lang.String localDirect;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDirectTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDirect() {
            return localDirect;
        }

        /**
           * Auto generated setter method
           * @param param Direct
           */
        public void setDirect(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDirectTracker = true;
            }
            else {
                localDirectTracker = false;

            }

            this.localDirect = param;

        }

        /**
        * field for DstAirportCode
        */

        protected java.lang.String localDstAirportCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localDstAirportCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getDstAirportCode() {
            return localDstAirportCode;
        }

        /**
           * Auto generated setter method
           * @param param DstAirportCode
           */
        public void setDstAirportCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localDstAirportCodeTracker = true;
            }
            else {
                localDstAirportCodeTracker = false;

            }

            this.localDstAirportCode = param;

        }

        /**
        * field for Group
        */

        protected java.lang.String localGroup;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localGroupTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getGroup() {
            return localGroup;
        }

        /**
           * Auto generated setter method
           * @param param Group
           */
        public void setGroup(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localGroupTracker = true;
            }
            else {
                localGroupTracker = false;

            }

            this.localGroup = param;

        }

        /**
        * field for OfficeNo
        */

        protected java.lang.String localOfficeNo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOfficeNoTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getOfficeNo() {
            return localOfficeNo;
        }

        /**
           * Auto generated setter method
           * @param param OfficeNo
           */
        public void setOfficeNo(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localOfficeNoTracker = true;
            }
            else {
                localOfficeNoTracker = false;

            }

            this.localOfficeNo = param;

        }

        /**
        * field for OnlyAvailableSeat
        */

        protected int localOnlyAvailableSeat;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOnlyAvailableSeatTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getOnlyAvailableSeat() {
            return localOnlyAvailableSeat;
        }

        /**
           * Auto generated setter method
           * @param param OnlyAvailableSeat
           */
        public void setOnlyAvailableSeat(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localOnlyAvailableSeatTracker = false;

            }
            else {
                localOnlyAvailableSeatTracker = true;
            }

            this.localOnlyAvailableSeat = param;

        }

        /**
        * field for OnlyNormalCommision
        */

        protected int localOnlyNormalCommision;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOnlyNormalCommisionTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getOnlyNormalCommision() {
            return localOnlyNormalCommision;
        }

        /**
           * Auto generated setter method
           * @param param OnlyNormalCommision
           */
        public void setOnlyNormalCommision(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localOnlyNormalCommisionTracker = false;

            }
            else {
                localOnlyNormalCommisionTracker = true;
            }

            this.localOnlyNormalCommision = param;

        }

        /**
        * field for OnlyOnWorkingCommision
        */

        protected int localOnlyOnWorkingCommision;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOnlyOnWorkingCommisionTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getOnlyOnWorkingCommision() {
            return localOnlyOnWorkingCommision;
        }

        /**
           * Auto generated setter method
           * @param param OnlyOnWorkingCommision
           */
        public void setOnlyOnWorkingCommision(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localOnlyOnWorkingCommisionTracker = false;

            }
            else {
                localOnlyOnWorkingCommisionTracker = true;
            }

            this.localOnlyOnWorkingCommision = param;

        }

        /**
        * field for OnlySelfPNR
        */

        protected int localOnlySelfPNR;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOnlySelfPNRTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getOnlySelfPNR() {
            return localOnlySelfPNR;
        }

        /**
           * Auto generated setter method
           * @param param OnlySelfPNR
           */
        public void setOnlySelfPNR(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localOnlySelfPNRTracker = false;

            }
            else {
                localOnlySelfPNRTracker = true;
            }

            this.localOnlySelfPNR = param;

        }

        /**
        * field for OrgAirportCode
        */

        protected java.lang.String localOrgAirportCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localOrgAirportCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getOrgAirportCode() {
            return localOrgAirportCode;
        }

        /**
           * Auto generated setter method
           * @param param OrgAirportCode
           */
        public void setOrgAirportCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localOrgAirportCodeTracker = true;
            }
            else {
                localOrgAirportCodeTracker = false;

            }

            this.localOrgAirportCode = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for Pid
        */

        protected java.lang.String localPid;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPidTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPid() {
            return localPid;
        }

        /**
           * Auto generated setter method
           * @param param Pid
           */
        public void setPid(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPidTracker = true;
            }
            else {
                localPidTracker = false;

            }

            this.localPid = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AvailableFlightWithPriceAndCommisionRequest.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
            if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                        + ":availableFlightWithPriceAndCommisionRequest", xmlWriter);
            }
            else {
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "availableFlightWithPriceAndCommisionRequest", xmlWriter);
            }

            if (localAgencyCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "agencyCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "agencyCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("agencyCode");
                }

                if (localAgencyCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAgencyCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localSignTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "sign", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "sign");
                    }

                }
                else {
                    xmlWriter.writeStartElement("sign");
                }

                if (localSign == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localSign);

                }

                xmlWriter.writeEndElement();
            }
            if (localAirlineCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "airlineCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "airlineCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("airlineCode");
                }

                if (localAirlineCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("airlineCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localAirlineCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localDateTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "date", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "date");
                    }

                }
                else {
                    xmlWriter.writeStartElement("date");
                }

                if (localDate == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDate);

                }

                xmlWriter.writeEndElement();
            }
            if (localDepTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "depTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "depTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("depTime");
                }

                if (localDepTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("depTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDepTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localDirectTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "direct", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "direct");
                    }

                }
                else {
                    xmlWriter.writeStartElement("direct");
                }

                if (localDirect == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("direct cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDirect);

                }

                xmlWriter.writeEndElement();
            }
            if (localDstAirportCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "dstAirportCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "dstAirportCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("dstAirportCode");
                }

                if (localDstAirportCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("dstAirportCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localDstAirportCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localGroupTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "group", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "group");
                    }

                }
                else {
                    xmlWriter.writeStartElement("group");
                }

                if (localGroup == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localGroup);

                }

                xmlWriter.writeEndElement();
            }
            if (localOfficeNoTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "officeNo", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "officeNo");
                    }

                }
                else {
                    xmlWriter.writeStartElement("officeNo");
                }

                if (localOfficeNo == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("officeNo cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localOfficeNo);

                }

                xmlWriter.writeEndElement();
            }
            if (localOnlyAvailableSeatTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "onlyAvailableSeat", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "onlyAvailableSeat");
                    }

                }
                else {
                    xmlWriter.writeStartElement("onlyAvailableSeat");
                }

                if (localOnlyAvailableSeat == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("onlyAvailableSeat cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOnlyAvailableSeat));
                }

                xmlWriter.writeEndElement();
            }
            if (localOnlyNormalCommisionTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "onlyNormalCommision", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "onlyNormalCommision");
                    }

                }
                else {
                    xmlWriter.writeStartElement("onlyNormalCommision");
                }

                if (localOnlyNormalCommision == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("onlyNormalCommision cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOnlyNormalCommision));
                }

                xmlWriter.writeEndElement();
            }
            if (localOnlyOnWorkingCommisionTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "onlyOnWorkingCommision", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "onlyOnWorkingCommision");
                    }

                }
                else {
                    xmlWriter.writeStartElement("onlyOnWorkingCommision");
                }

                if (localOnlyOnWorkingCommision == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("onlyOnWorkingCommision cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOnlyOnWorkingCommision));
                }

                xmlWriter.writeEndElement();
            }
            if (localOnlySelfPNRTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "onlySelfPNR", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "onlySelfPNR");
                    }

                }
                else {
                    xmlWriter.writeStartElement("onlySelfPNR");
                }

                if (localOnlySelfPNR == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("onlySelfPNR cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOnlySelfPNR));
                }

                xmlWriter.writeEndElement();
            }
            if (localOrgAirportCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "orgAirportCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "orgAirportCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("orgAirportCode");
                }

                if (localOrgAirportCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("orgAirportCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localOrgAirportCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPidTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "pid", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "pid");
                    }

                }
                else {
                    xmlWriter.writeStartElement("pid");
                }

                if (localPid == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("pid cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPid);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance", "type"));
            attribList
                    .add(new javax.xml.namespace.QName(
                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                            "availableFlightWithPriceAndCommisionRequest"));
            if (localAgencyCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "agencyCode"));

                if (localAgencyCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAgencyCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("agencyCode cannot be null!!");
                }
            }
            if (localSignTracker) {
                elementList.add(new javax.xml.namespace.QName("", "sign"));

                if (localSign != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSign));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("sign cannot be null!!");
                }
            }
            if (localAirlineCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "airlineCode"));

                if (localAirlineCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAirlineCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("airlineCode cannot be null!!");
                }
            }
            if (localDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "date"));

                if (localDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDate));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");
                }
            }
            if (localDepTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "depTime"));

                if (localDepTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDepTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("depTime cannot be null!!");
                }
            }
            if (localDirectTracker) {
                elementList.add(new javax.xml.namespace.QName("", "direct"));

                if (localDirect != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirect));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("direct cannot be null!!");
                }
            }
            if (localDstAirportCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "dstAirportCode"));

                if (localDstAirportCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localDstAirportCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("dstAirportCode cannot be null!!");
                }
            }
            if (localGroupTracker) {
                elementList.add(new javax.xml.namespace.QName("", "group"));

                if (localGroup != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGroup));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                }
            }
            if (localOfficeNoTracker) {
                elementList.add(new javax.xml.namespace.QName("", "officeNo"));

                if (localOfficeNo != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfficeNo));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("officeNo cannot be null!!");
                }
            }
            if (localOnlyAvailableSeatTracker) {
                elementList.add(new javax.xml.namespace.QName("", "onlyAvailableSeat"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localOnlyAvailableSeat));
            }
            if (localOnlyNormalCommisionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "onlyNormalCommision"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localOnlyNormalCommision));
            }
            if (localOnlyOnWorkingCommisionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "onlyOnWorkingCommision"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                        .convertToString(localOnlyOnWorkingCommision));
            }
            if (localOnlySelfPNRTracker) {
                elementList.add(new javax.xml.namespace.QName("", "onlySelfPNR"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnlySelfPNR));
            }
            if (localOrgAirportCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orgAirportCode"));

                if (localOrgAirportCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localOrgAirportCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("orgAirportCode cannot be null!!");
                }
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPidTracker) {
                elementList.add(new javax.xml.namespace.QName("", "pid"));

                if (localPid != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPid));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("pid cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AvailableFlightWithPriceAndCommisionRequest parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AvailableFlightWithPriceAndCommisionRequest object = new AvailableFlightWithPriceAndCommisionRequest();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"availableFlightWithPriceAndCommisionRequest".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AvailableFlightWithPriceAndCommisionRequest) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "agencyCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAgencyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "sign").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSign(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "airlineCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setAirlineCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "date").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "depTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDepTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "direct").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDirect(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "dstAirportCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setDstAirportCode(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "group").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setGroup(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "officeNo").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOfficeNo(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "onlyAvailableSeat").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOnlyAvailableSeat(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setOnlyAvailableSeat(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "onlyNormalCommision").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOnlyNormalCommision(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setOnlyNormalCommision(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "onlyOnWorkingCommision").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOnlyOnWorkingCommision(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setOnlyOnWorkingCommision(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "onlySelfPNR").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOnlySelfPNR(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setOnlySelfPNR(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "orgAirportCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setOrgAirportCode(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "pid").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class AvailableFlightWithPriceAndCommisionReplyE implements org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                "availableFlightWithPriceAndCommisionReply", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for AvailableFlightWithPriceAndCommisionReply
        */

        protected AvailableFlightWithPriceAndCommisionReply localAvailableFlightWithPriceAndCommisionReply;

        /**
        * Auto generated getter method
        * @return AvailableFlightWithPriceAndCommisionReply
        */
        public AvailableFlightWithPriceAndCommisionReply getAvailableFlightWithPriceAndCommisionReply() {
            return localAvailableFlightWithPriceAndCommisionReply;
        }

        /**
           * Auto generated setter method
           * @param param AvailableFlightWithPriceAndCommisionReply
           */
        public void setAvailableFlightWithPriceAndCommisionReply(AvailableFlightWithPriceAndCommisionReply param) {

            this.localAvailableFlightWithPriceAndCommisionReply = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AvailableFlightWithPriceAndCommisionReplyE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localAvailableFlightWithPriceAndCommisionReply == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localAvailableFlightWithPriceAndCommisionReply.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localAvailableFlightWithPriceAndCommisionReply.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AvailableFlightWithPriceAndCommisionReplyE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AvailableFlightWithPriceAndCommisionReplyE object = new AvailableFlightWithPriceAndCommisionReplyE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "availableFlightWithPriceAndCommisionReply").equals(reader.getName())) {

                                object.setAvailableFlightWithPriceAndCommisionReply(AvailableFlightWithPriceAndCommisionReply.Factory
                                        .parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class ExtensionMapper {

        public static java.lang.Object getTypeObject(java.lang.String namespaceURI, java.lang.String typeName,
                javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "getAvailableFlightWithPriceAndCommisionResponse".equals(typeName)) {

                return GetAvailableFlightWithPriceAndCommisionResponse.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "getAvailableFlightWithPriceAndCommision".equals(typeName)) {

                return GetAvailableFlightWithPriceAndCommision.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "wsFlightWithPriceAndCommisionItem".equals(typeName)) {

                return WsFlightWithPriceAndCommisionItem.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "availableFlightWithPriceAndCommisionReply".equals(typeName)) {

                return AvailableFlightWithPriceAndCommisionReply.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "availableFlightWithPriceAndCommisionRequest".equals(typeName)) {

                return AvailableFlightWithPriceAndCommisionRequest.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "abstractLiantuoReply".equals(typeName)) {

                return AbstractLiantuoReply.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "wsPolicyData".equals(typeName)) {

                return WsPolicyData.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "abstractLiantuoRequest".equals(typeName)) {

                return AbstractLiantuoRequest.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "wsFlightWithPriceAndCommision".equals(typeName)) {

                return WsFlightWithPriceAndCommision.Factory.parse(reader);

            }

            if ("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/"
                    .equals(namespaceURI) && "wsSeatWithPriceAndComisionItem".equals(typeName)) {

                return WsSeatWithPriceAndComisionItem.Factory.parse(reader);

            }

            throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
        }

    }

    public static class AbstractLiantuoReply implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = abstractLiantuoReply
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for ReturnCode
        */

        protected java.lang.String localReturnCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localReturnCodeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getReturnCode() {
            return localReturnCode;
        }

        /**
           * Auto generated setter method
           * @param param ReturnCode
           */
        public void setReturnCode(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localReturnCodeTracker = true;
            }
            else {
                localReturnCodeTracker = false;

            }

            this.localReturnCode = param;

        }

        /**
        * field for ReturnMessage
        */

        protected java.lang.String localReturnMessage;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localReturnMessageTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getReturnMessage() {
            return localReturnMessage;
        }

        /**
           * Auto generated setter method
           * @param param ReturnMessage
           */
        public void setReturnMessage(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localReturnMessageTracker = true;
            }
            else {
                localReturnMessageTracker = false;

            }

            this.localReturnMessage = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    AbstractLiantuoReply.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":abstractLiantuoReply", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "abstractLiantuoReply",
                            xmlWriter);
                }

            }
            if (localReturnCodeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnCode", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnCode");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnCode");
                }

                if (localReturnCode == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnCode);

                }

                xmlWriter.writeEndElement();
            }
            if (localReturnMessageTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "returnMessage", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "returnMessage");
                    }

                }
                else {
                    xmlWriter.writeStartElement("returnMessage");
                }

                if (localReturnMessage == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localReturnMessage);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localReturnCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnCode"));

                if (localReturnCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");
                }
            }
            if (localReturnMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("", "returnMessage"));

                if (localReturnMessage != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localReturnMessage));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("returnMessage cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static AbstractLiantuoReply parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                AbstractLiantuoReply object = null;

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"abstractLiantuoReply".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AbstractLiantuoReply) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                            throw new org.apache.axis2.databinding.ADBException(
                                    "The an abstract class can not be instantiated !!!");

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnCode").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "returnMessage").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setReturnMessage(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetAvailableFlightWithPriceAndCommisionResponseE implements
            org.apache.axis2.databinding.ADBBean {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                "getAvailableFlightWithPriceAndCommisionResponse", "ns1");

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for GetAvailableFlightWithPriceAndCommisionResponse
        */

        protected GetAvailableFlightWithPriceAndCommisionResponse localGetAvailableFlightWithPriceAndCommisionResponse;

        /**
        * Auto generated getter method
        * @return GetAvailableFlightWithPriceAndCommisionResponse
        */
        public GetAvailableFlightWithPriceAndCommisionResponse getGetAvailableFlightWithPriceAndCommisionResponse() {
            return localGetAvailableFlightWithPriceAndCommisionResponse;
        }

        /**
           * Auto generated setter method
           * @param param GetAvailableFlightWithPriceAndCommisionResponse
           */
        public void setGetAvailableFlightWithPriceAndCommisionResponse(
                GetAvailableFlightWithPriceAndCommisionResponse param) {

            this.localGetAvailableFlightWithPriceAndCommisionResponse = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetAvailableFlightWithPriceAndCommisionResponseE.this.serialize(MY_QNAME, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(MY_QNAME, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it

            if (localGetAvailableFlightWithPriceAndCommisionResponse == null) {
                throw new org.apache.axis2.databinding.ADBException("Property cannot be null!");
            }
            localGetAvailableFlightWithPriceAndCommisionResponse.serialize(MY_QNAME, factory, xmlWriter);

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            //We can safely assume an element has only one type associated with it
            return localGetAvailableFlightWithPriceAndCommisionResponse.getPullParser(MY_QNAME);

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetAvailableFlightWithPriceAndCommisionResponseE parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetAvailableFlightWithPriceAndCommisionResponseE object = new GetAvailableFlightWithPriceAndCommisionResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {

                            if (reader.isStartElement()
                                    && new javax.xml.namespace.QName(
                                            "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/",
                                            "getAvailableFlightWithPriceAndCommisionResponse").equals(reader.getName())) {

                                object.setGetAvailableFlightWithPriceAndCommisionResponse(GetAvailableFlightWithPriceAndCommisionResponse.Factory
                                        .parse(reader));

                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                        + reader.getLocalName());
                            }

                        }
                        else {
                            reader.next();
                        }
                    } // end of while loop

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetAvailableFlightWithPriceAndCommisionResponse implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getAvailableFlightWithPriceAndCommisionResponse
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for _return
        */

        protected AvailableFlightWithPriceAndCommisionReply local_return;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean local_returnTracker = false;

        /**
        * Auto generated getter method
        * @return AvailableFlightWithPriceAndCommisionReply
        */
        public AvailableFlightWithPriceAndCommisionReply get_return() {
            return local_return;
        }

        /**
           * Auto generated setter method
           * @param param _return
           */
        public void set_return(AvailableFlightWithPriceAndCommisionReply param) {

            if (param != null) {
                //update the setting tracker
                local_returnTracker = true;
            }
            else {
                local_returnTracker = false;

            }

            this.local_return = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetAvailableFlightWithPriceAndCommisionResponse.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":getAvailableFlightWithPriceAndCommisionResponse", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "getAvailableFlightWithPriceAndCommisionResponse", xmlWriter);
                }

            }
            if (local_returnTracker) {
                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException("return cannot be null!!");
                }
                local_return.serialize(new javax.xml.namespace.QName("", "return"), factory, xmlWriter);
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException("return cannot be null!!");
                }
                elementList.add(local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetAvailableFlightWithPriceAndCommisionResponse parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetAvailableFlightWithPriceAndCommisionResponse object = new GetAvailableFlightWithPriceAndCommisionResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getAvailableFlightWithPriceAndCommisionResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetAvailableFlightWithPriceAndCommisionResponse) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "return").equals(reader.getName())) {

                        object.set_return(AvailableFlightWithPriceAndCommisionReply.Factory.parse(reader));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class WsPolicyData implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = wsPolicyData
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for Comment
        */

        protected java.lang.String localComment;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommentTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getComment() {
            return localComment;
        }

        /**
           * Auto generated setter method
           * @param param Comment
           */
        public void setComment(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localCommentTracker = true;
            }
            else {
                localCommentTracker = false;

            }

            this.localComment = param;

        }

        /**
        * field for CommisionMoney
        */

        protected double localCommisionMoney;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommisionMoneyTracker = false;

        /**
        * Auto generated getter method
        * @return double
        */
        public double getCommisionMoney() {
            return localCommisionMoney;
        }

        /**
           * Auto generated setter method
           * @param param CommisionMoney
           */
        public void setCommisionMoney(double param) {

            // setting primitive attribute tracker to true

            if (java.lang.Double.isNaN(param)) {
                localCommisionMoneyTracker = false;

            }
            else {
                localCommisionMoneyTracker = true;
            }

            this.localCommisionMoney = param;

        }

        /**
        * field for CommisionPoint
        */

        protected java.lang.String localCommisionPoint;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommisionPointTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getCommisionPoint() {
            return localCommisionPoint;
        }

        /**
           * Auto generated setter method
           * @param param CommisionPoint
           */
        public void setCommisionPoint(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localCommisionPointTracker = true;
            }
            else {
                localCommisionPointTracker = false;

            }

            this.localCommisionPoint = param;

        }

        /**
        * field for CommisionType
        */

        protected int localCommisionType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localCommisionTypeTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getCommisionType() {
            return localCommisionType;
        }

        /**
           * Auto generated setter method
           * @param param CommisionType
           */
        public void setCommisionType(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localCommisionTypeTracker = false;

            }
            else {
                localCommisionTypeTracker = true;
            }

            this.localCommisionType = param;

        }

        /**
        * field for NeedSwitchPNR
        */

        protected int localNeedSwitchPNR;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localNeedSwitchPNRTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getNeedSwitchPNR() {
            return localNeedSwitchPNR;
        }

        /**
           * Auto generated setter method
           * @param param NeedSwitchPNR
           */
        public void setNeedSwitchPNR(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localNeedSwitchPNRTracker = false;

            }
            else {
                localNeedSwitchPNRTracker = true;
            }

            this.localNeedSwitchPNR = param;

        }

        /**
        * field for Param1
        */

        protected java.lang.String localParam1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam1Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam1() {
            return localParam1;
        }

        /**
           * Auto generated setter method
           * @param param Param1
           */
        public void setParam1(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam1Tracker = true;
            }
            else {
                localParam1Tracker = false;

            }

            this.localParam1 = param;

        }

        /**
        * field for Param2
        */

        protected java.lang.String localParam2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam2Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam2() {
            return localParam2;
        }

        /**
           * Auto generated setter method
           * @param param Param2
           */
        public void setParam2(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam2Tracker = true;
            }
            else {
                localParam2Tracker = false;

            }

            this.localParam2 = param;

        }

        /**
        * field for Param3
        */

        protected java.lang.String localParam3;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam3Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam3() {
            return localParam3;
        }

        /**
           * Auto generated setter method
           * @param param Param3
           */
        public void setParam3(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam3Tracker = true;
            }
            else {
                localParam3Tracker = false;

            }

            this.localParam3 = param;

        }

        /**
        * field for Param4
        */

        protected java.lang.String localParam4;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localParam4Tracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getParam4() {
            return localParam4;
        }

        /**
           * Auto generated setter method
           * @param param Param4
           */
        public void setParam4(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localParam4Tracker = true;
            }
            else {
                localParam4Tracker = false;

            }

            this.localParam4 = param;

        }

        /**
        * field for PolicyBelongTo
        */

        protected int localPolicyBelongTo;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyBelongToTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getPolicyBelongTo() {
            return localPolicyBelongTo;
        }

        /**
           * Auto generated setter method
           * @param param PolicyBelongTo
           */
        public void setPolicyBelongTo(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localPolicyBelongToTracker = false;

            }
            else {
                localPolicyBelongToTracker = true;
            }

            this.localPolicyBelongTo = param;

        }

        /**
        * field for PolicyId
        */

        protected int localPolicyId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyIdTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getPolicyId() {
            return localPolicyId;
        }

        /**
           * Auto generated setter method
           * @param param PolicyId
           */
        public void setPolicyId(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localPolicyIdTracker = false;

            }
            else {
                localPolicyIdTracker = true;
            }

            this.localPolicyId = param;

        }

        /**
        * field for PolicyType
        */

        protected java.lang.String localPolicyType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localPolicyTypeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getPolicyType() {
            return localPolicyType;
        }

        /**
           * Auto generated setter method
           * @param param PolicyType
           */
        public void setPolicyType(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localPolicyTypeTracker = true;
            }
            else {
                localPolicyTypeTracker = false;

            }

            this.localPolicyType = param;

        }

        /**
        * field for SeatType
        */

        protected int localSeatType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localSeatTypeTracker = false;

        /**
        * Auto generated getter method
        * @return int
        */
        public int getSeatType() {
            return localSeatType;
        }

        /**
           * Auto generated setter method
           * @param param SeatType
           */
        public void setSeatType(int param) {

            // setting primitive attribute tracker to true

            if (param == java.lang.Integer.MIN_VALUE) {
                localSeatTypeTracker = false;

            }
            else {
                localSeatTypeTracker = true;
            }

            this.localSeatType = param;

        }

        /**
        * field for VtWorkTime
        */

        protected java.lang.String localVtWorkTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localVtWorkTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getVtWorkTime() {
            return localVtWorkTime;
        }

        /**
           * Auto generated setter method
           * @param param VtWorkTime
           */
        public void setVtWorkTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localVtWorkTimeTracker = true;
            }
            else {
                localVtWorkTimeTracker = false;

            }

            this.localVtWorkTime = param;

        }

        /**
        * field for WorkTime
        */

        protected java.lang.String localWorkTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localWorkTimeTracker = false;

        /**
        * Auto generated getter method
        * @return java.lang.String
        */
        public java.lang.String getWorkTime() {
            return localWorkTime;
        }

        /**
           * Auto generated setter method
           * @param param WorkTime
           */
        public void setWorkTime(java.lang.String param) {

            if (param != null) {
                //update the setting tracker
                localWorkTimeTracker = true;
            }
            else {
                localWorkTimeTracker = false;

            }

            this.localWorkTime = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    WsPolicyData.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":wsPolicyData", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "wsPolicyData",
                            xmlWriter);
                }

            }
            if (localCommentTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "comment", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "comment");
                    }

                }
                else {
                    xmlWriter.writeStartElement("comment");
                }

                if (localComment == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localComment);

                }

                xmlWriter.writeEndElement();
            }
            if (localCommisionMoneyTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "commisionMoney", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "commisionMoney");
                    }

                }
                else {
                    xmlWriter.writeStartElement("commisionMoney");
                }

                if (java.lang.Double.isNaN(localCommisionMoney)) {

                    throw new org.apache.axis2.databinding.ADBException("commisionMoney cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCommisionMoney));
                }

                xmlWriter.writeEndElement();
            }
            if (localCommisionPointTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "commisionPoint", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "commisionPoint");
                    }

                }
                else {
                    xmlWriter.writeStartElement("commisionPoint");
                }

                if (localCommisionPoint == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("commisionPoint cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localCommisionPoint);

                }

                xmlWriter.writeEndElement();
            }
            if (localCommisionTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "commisionType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "commisionType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("commisionType");
                }

                if (localCommisionType == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("commisionType cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCommisionType));
                }

                xmlWriter.writeEndElement();
            }
            if (localNeedSwitchPNRTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "needSwitchPNR", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "needSwitchPNR");
                    }

                }
                else {
                    xmlWriter.writeStartElement("needSwitchPNR");
                }

                if (localNeedSwitchPNR == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("needSwitchPNR cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localNeedSwitchPNR));
                }

                xmlWriter.writeEndElement();
            }
            if (localParam1Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param1", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param1");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param1");
                }

                if (localParam1 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam1);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam2Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param2", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param2");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param2");
                }

                if (localParam2 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam2);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam3Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param3", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param3");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param3");
                }

                if (localParam3 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam3);

                }

                xmlWriter.writeEndElement();
            }
            if (localParam4Tracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "param4", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "param4");
                    }

                }
                else {
                    xmlWriter.writeStartElement("param4");
                }

                if (localParam4 == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localParam4);

                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyBelongToTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyBelongTo", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyBelongTo");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyBelongTo");
                }

                if (localPolicyBelongTo == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("policyBelongTo cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPolicyBelongTo));
                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyIdTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyId", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyId");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyId");
                }

                if (localPolicyId == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("policyId cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localPolicyId));
                }

                xmlWriter.writeEndElement();
            }
            if (localPolicyTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "policyType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "policyType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("policyType");
                }

                if (localPolicyType == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("policyType cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localPolicyType);

                }

                xmlWriter.writeEndElement();
            }
            if (localSeatTypeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "seatType", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "seatType");
                    }

                }
                else {
                    xmlWriter.writeStartElement("seatType");
                }

                if (localSeatType == java.lang.Integer.MIN_VALUE) {

                    throw new org.apache.axis2.databinding.ADBException("seatType cannot be null!!");

                }
                else {
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localSeatType));
                }

                xmlWriter.writeEndElement();
            }
            if (localVtWorkTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "vtWorkTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "vtWorkTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("vtWorkTime");
                }

                if (localVtWorkTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("vtWorkTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localVtWorkTime);

                }

                xmlWriter.writeEndElement();
            }
            if (localWorkTimeTracker) {
                namespace = "";
                if (!namespace.equals("")) {
                    prefix = xmlWriter.getPrefix(namespace);

                    if (prefix == null) {
                        prefix = generatePrefix(namespace);

                        xmlWriter.writeStartElement(prefix, "workTime", namespace);
                        xmlWriter.writeNamespace(prefix, namespace);
                        xmlWriter.setPrefix(prefix, namespace);

                    }
                    else {
                        xmlWriter.writeStartElement(namespace, "workTime");
                    }

                }
                else {
                    xmlWriter.writeStartElement("workTime");
                }

                if (localWorkTime == null) {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("workTime cannot be null!!");

                }
                else {

                    xmlWriter.writeCharacters(localWorkTime);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localCommentTracker) {
                elementList.add(new javax.xml.namespace.QName("", "comment"));

                if (localComment != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComment));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");
                }
            }
            if (localCommisionMoneyTracker) {
                elementList.add(new javax.xml.namespace.QName("", "commisionMoney"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCommisionMoney));
            }
            if (localCommisionPointTracker) {
                elementList.add(new javax.xml.namespace.QName("", "commisionPoint"));

                if (localCommisionPoint != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil
                            .convertToString(localCommisionPoint));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("commisionPoint cannot be null!!");
                }
            }
            if (localCommisionTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "commisionType"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCommisionType));
            }
            if (localNeedSwitchPNRTracker) {
                elementList.add(new javax.xml.namespace.QName("", "needSwitchPNR"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNeedSwitchPNR));
            }
            if (localParam1Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param1"));

                if (localParam1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam1));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param1 cannot be null!!");
                }
            }
            if (localParam2Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param2"));

                if (localParam2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam2));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param2 cannot be null!!");
                }
            }
            if (localParam3Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param3"));

                if (localParam3 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam3));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param3 cannot be null!!");
                }
            }
            if (localParam4Tracker) {
                elementList.add(new javax.xml.namespace.QName("", "param4"));

                if (localParam4 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParam4));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("param4 cannot be null!!");
                }
            }
            if (localPolicyBelongToTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyBelongTo"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPolicyBelongTo));
            }
            if (localPolicyIdTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyId"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPolicyId));
            }
            if (localPolicyTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "policyType"));

                if (localPolicyType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPolicyType));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("policyType cannot be null!!");
                }
            }
            if (localSeatTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "seatType"));

                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeatType));
            }
            if (localVtWorkTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "vtWorkTime"));

                if (localVtWorkTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVtWorkTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("vtWorkTime cannot be null!!");
                }
            }
            if (localWorkTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "workTime"));

                if (localWorkTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkTime));
                }
                else {
                    throw new org.apache.axis2.databinding.ADBException("workTime cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static WsPolicyData parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
                WsPolicyData object = new WsPolicyData();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"wsPolicyData".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsPolicyData) ExtensionMapper.getTypeObject(nsUri, type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "comment").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setComment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "commisionMoney").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCommisionMoney(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToDouble(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setCommisionMoney(java.lang.Double.NaN);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "commisionPoint").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCommisionPoint(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "commisionType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setCommisionType(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setCommisionType(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "needSwitchPNR").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setNeedSwitchPNR(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setNeedSwitchPNR(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param1").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param2").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param3").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("", "param4").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setParam4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyBelongTo").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyBelongTo(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setPolicyBelongTo(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyId").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setPolicyId(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "policyType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setPolicyType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "seatType").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setSeatType(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                        object.setSeatType(java.lang.Integer.MIN_VALUE);

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "vtWorkTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setVtWorkTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "workTime").equals(reader.getName())) {

                        java.lang.String content = reader.getElementText();

                        object.setWorkTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    public static class GetAvailableFlightWithPriceAndCommision implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
                name = getAvailableFlightWithPriceAndCommision
                Namespace URI = http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/
                Namespace Prefix = ns1
                */

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if (namespace
                    .equals("http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/")) {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
        * field for Request
        */

        protected AvailableFlightWithPriceAndCommisionRequest localRequest;

        /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
        protected boolean localRequestTracker = false;

        /**
        * Auto generated getter method
        * @return AvailableFlightWithPriceAndCommisionRequest
        */
        public AvailableFlightWithPriceAndCommisionRequest getRequest() {
            return localRequest;
        }

        /**
           * Auto generated setter method
           * @param param Request
           */
        public void setRequest(AvailableFlightWithPriceAndCommisionRequest param) {

            if (param != null) {
                //update the setting tracker
                localRequestTracker = true;
            }
            else {
                localRequestTracker = false;

            }

            this.localRequest = param;

        }

        /**
        * isReaderMTOMAware
        * @return true if the reader supports MTOM
        */
        public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
            boolean isReaderMTOMAware = false;

            try {
                isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader
                        .getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
            }
            catch (java.lang.IllegalArgumentException e) {
                isReaderMTOMAware = false;
            }
            return isReaderMTOMAware;
        }

        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
        public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {

            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName) {

                public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                        throws javax.xml.stream.XMLStreamException {
                    GetAvailableFlightWithPriceAndCommision.this.serialize(parentQName, factory, xmlWriter);
                }
            };
            return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(parentQName, factory, dataSource);

        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
            serialize(parentQName, factory, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory,
                org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter, boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();

            if ((namespace != null) && (namespace.trim().length() > 0)) {
                java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                if (writerPrefix != null) {
                    xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                }
                else {
                    if (prefix == null) {
                        prefix = generatePrefix(namespace);
                    }

                    xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }
            }
            else {
                xmlWriter.writeStartElement(parentQName.getLocalPart());
            }

            if (serializeType) {

                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://getavailableflightwithpriceandcommision.b2b.service.version1_0.webservice.model.ltips.com/");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix
                            + ":getAvailableFlightWithPriceAndCommision", xmlWriter);
                }
                else {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "getAvailableFlightWithPriceAndCommision", xmlWriter);
                }

            }
            if (localRequestTracker) {
                if (localRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException("request cannot be null!!");
                }
                localRequest.serialize(new javax.xml.namespace.QName("", "request"), factory, xmlWriter);
            }
            xmlWriter.writeEndElement();

        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);

            }

            xmlWriter.writeAttribute(namespace, attName, attValue);

        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
          * Util method to write an attribute without the ns prefix
          */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":"
                            + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite
                                    .append(prefix)
                                    .append(":")
                                    .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                    .convertToString(qnames[i]));
                        }
                    }
                    else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil
                                .convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }

        /**
        * Register a namespace prefix
        */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
                throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException {

            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localRequestTracker) {
                elementList.add(new javax.xml.namespace.QName("", "request"));

                if (localRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException("request cannot be null!!");
                }
                elementList.add(localRequest);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(),
                    attribList.toArray());

        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {

            /**
            * static method to create the object
            * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
            *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
            * Postcondition: If this object is an element, the reader is positioned at its end element
            *                If this object is a complex type, the reader is positioned at the end element of its outer element
            */
            public static GetAvailableFlightWithPriceAndCommision parse(javax.xml.stream.XMLStreamReader reader)
                    throws java.lang.Exception {
                GetAvailableFlightWithPriceAndCommision object = new GetAvailableFlightWithPriceAndCommision();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance", "type");
                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"getAvailableFlightWithPriceAndCommision".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetAvailableFlightWithPriceAndCommision) ExtensionMapper.getTypeObject(nsUri,
                                        type, reader);
                            }

                        }

                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()
                            && new javax.xml.namespace.QName("", "request").equals(reader.getName())) {

                        object.setRequest(AvailableFlightWithPriceAndCommisionRequest.Factory.parse(reader));

                        reader.next();

                    } // End of if for expected property start element

                    else {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement "
                                + reader.getLocalName());

                }
                catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class

    }

    private org.apache.axiom.om.OMElement toOM(
            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.om.OMElement toOM(
            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {
            return param
                    .getOMElement(
                            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE.MY_QNAME,
                            org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
            org.apache.axiom.soap.SOAPFactory factory,
            client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE param,
            boolean optimizeContent) throws org.apache.axis2.AxisFault {

        try {

            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope
                    .getBody()
                    .addChild(
                            param.getOMElement(
                                    client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE.MY_QNAME,
                                    factory));
            return emptyEnvelope;
        }
        catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

    }

    /* methods to provide back word compatibility */

    /**
    *  get the default envelope
    */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {

        try {

            if (client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE.class
                    .equals(type)) {

                return client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

            if (client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE.class
                    .equals(type)) {

                return client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionResponseE.Factory
                        .parse(param.getXMLStreamReaderWithoutCaching());

            }

        }
        catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }

}
