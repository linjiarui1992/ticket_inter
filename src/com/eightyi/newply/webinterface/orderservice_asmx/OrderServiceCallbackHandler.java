
/**
 * OrderServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.eightyi.newply.webinterface.orderservice_asmx;

    /**
     *  OrderServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class OrderServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public OrderServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public OrderServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for orderPayOutTicketAndPly method
            * override this method for handling normal response from orderPayOutTicketAndPly operation
            */
           public void receiveResultorderPayOutTicketAndPly(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.OrderPayOutTicketAndPlyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from orderPayOutTicketAndPly operation
           */
            public void receiveErrororderPayOutTicketAndPly(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createOrderNewByCHD method
            * override this method for handling normal response from createOrderNewByCHD operation
            */
           public void receiveResultcreateOrderNewByCHD(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.CreateOrderNewByCHDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createOrderNewByCHD operation
           */
            public void receiveErrorcreateOrderNewByCHD(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPNRByOrderID method
            * override this method for handling normal response from getPNRByOrderID operation
            */
           public void receiveResultgetPNRByOrderID(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.GetPNRByOrderIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPNRByOrderID operation
           */
            public void receiveErrorgetPNRByOrderID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWhyByOrder method
            * override this method for handling normal response from getWhyByOrder operation
            */
           public void receiveResultgetWhyByOrder(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.GetWhyByOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWhyByOrder operation
           */
            public void receiveErrorgetWhyByOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for automatismPay method
            * override this method for handling normal response from automatismPay operation
            */
           public void receiveResultautomatismPay(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.AutomatismPayResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from automatismPay operation
           */
            public void receiveErrorautomatismPay(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for creatOrderNew method
            * override this method for handling normal response from creatOrderNew operation
            */
           public void receiveResultcreatOrderNew(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.CreatOrderNewResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from creatOrderNew operation
           */
            public void receiveErrorcreatOrderNew(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for payURL method
            * override this method for handling normal response from payURL operation
            */
           public void receiveResultpayURL(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.PayURLResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from payURL operation
           */
            public void receiveErrorpayURL(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateOrderTicketStatus method
            * override this method for handling normal response from updateOrderTicketStatus operation
            */
           public void receiveResultupdateOrderTicketStatus(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.UpdateOrderTicketStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateOrderTicketStatus operation
           */
            public void receiveErrorupdateOrderTicketStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for testOutticketInterior method
            * override this method for handling normal response from testOutticketInterior operation
            */
           public void receiveResulttestOutticketInterior(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.TestOutticketInteriorResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from testOutticketInterior operation
           */
            public void receiveErrortestOutticketInterior(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for returnALlOrderState method
            * override this method for handling normal response from returnALlOrderState operation
            */
           public void receiveResultreturnALlOrderState(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.ReturnALlOrderStateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnALlOrderState operation
           */
            public void receiveErrorreturnALlOrderState(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for payState method
            * override this method for handling normal response from payState operation
            */
           public void receiveResultpayState(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.PayStateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from payState operation
           */
            public void receiveErrorpayState(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for orderRGIChange method
            * override this method for handling normal response from orderRGIChange operation
            */
           public void receiveResultorderRGIChange(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.OrderRGIChangeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from orderRGIChange operation
           */
            public void receiveErrororderRGIChange(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for creatPNR method
            * override this method for handling normal response from creatPNR operation
            */
           public void receiveResultcreatPNR(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.CreatPNRResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from creatPNR operation
           */
            public void receiveErrorcreatPNR(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for clearCaChe method
            * override this method for handling normal response from clearCaChe operation
            */
           public void receiveResultclearCaChe(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.ClearCaCheResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from clearCaChe operation
           */
            public void receiveErrorclearCaChe(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for returnTicekNo method
            * override this method for handling normal response from returnTicekNo operation
            */
           public void receiveResultreturnTicekNo(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.ReturnTicekNoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnTicekNo operation
           */
            public void receiveErrorreturnTicekNo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for orderRGIInfo method
            * override this method for handling normal response from orderRGIInfo operation
            */
           public void receiveResultorderRGIInfo(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.OrderRGIInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from orderRGIInfo operation
           */
            public void receiveErrororderRGIInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for paySignOn method
            * override this method for handling normal response from paySignOn operation
            */
           public void receiveResultpaySignOn(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.PaySignOnResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from paySignOn operation
           */
            public void receiveErrorpaySignOn(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for selectScheduledFlightNew method
            * override this method for handling normal response from selectScheduledFlightNew operation
            */
           public void receiveResultselectScheduledFlightNew(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.SelectScheduledFlightNewResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from selectScheduledFlightNew operation
           */
            public void receiveErrorselectScheduledFlightNew(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for selectOrderLog method
            * override this method for handling normal response from selectOrderLog operation
            */
           public void receiveResultselectOrderLog(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.SelectOrderLogResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from selectOrderLog operation
           */
            public void receiveErrorselectOrderLog(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createOrderByPassenger method
            * override this method for handling normal response from createOrderByPassenger operation
            */
           public void receiveResultcreateOrderByPassenger(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.CreateOrderByPassengerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createOrderByPassenger operation
           */
            public void receiveErrorcreateOrderByPassenger(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for returnOrderState method
            * override this method for handling normal response from returnOrderState operation
            */
           public void receiveResultreturnOrderState(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.ReturnOrderStateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnOrderState operation
           */
            public void receiveErrorreturnOrderState(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for selectScheduledFlight method
            * override this method for handling normal response from selectScheduledFlight operation
            */
           public void receiveResultselectScheduledFlight(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.SelectScheduledFlightResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from selectScheduledFlight operation
           */
            public void receiveErrorselectScheduledFlight(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for creatOrder method
            * override this method for handling normal response from creatOrder operation
            */
           public void receiveResultcreatOrder(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.CreatOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from creatOrder operation
           */
            public void receiveErrorcreatOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isOrder method
            * override this method for handling normal response from isOrder operation
            */
           public void receiveResultisOrder(
                    com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.IsOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isOrder operation
           */
            public void receiveErrorisOrder(java.lang.Exception e) {
            }
                


    }
    