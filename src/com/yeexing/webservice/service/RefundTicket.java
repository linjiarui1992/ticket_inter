
package com.yeexing.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="passengerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="airId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refundType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refundMemo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refundSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refund_notify_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "orderid",
    "passengerName",
    "airId",
    "refundType",
    "refundMemo",
    "refundSegment",
    "refundNotifyUrl",
    "sign"
})
@XmlRootElement(name = "RefundTicket")
public class RefundTicket {

    @XmlElement(required = true, nillable = true)
    protected String userName;
    @XmlElement(required = true, nillable = true)
    protected String orderid;
    @XmlElement(required = true, nillable = true)
    protected String passengerName;
    @XmlElement(required = true, nillable = true)
    protected String airId;
    @XmlElement(required = true, nillable = true)
    protected String refundType;
    @XmlElement(required = true, nillable = true)
    protected String refundMemo;
    @XmlElement(required = true, nillable = true)
    protected String refundSegment;
    @XmlElement(name = "refund_notify_url", required = true, nillable = true)
    protected String refundNotifyUrl;
    @XmlElement(required = true, nillable = true)
    protected String sign;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the orderid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderid() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderid(String value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the passengerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerName() {
        return passengerName;
    }

    /**
     * Sets the value of the passengerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerName(String value) {
        this.passengerName = value;
    }

    /**
     * Gets the value of the airId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirId() {
        return airId;
    }

    /**
     * Sets the value of the airId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirId(String value) {
        this.airId = value;
    }

    /**
     * Gets the value of the refundType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundType() {
        return refundType;
    }

    /**
     * Sets the value of the refundType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundType(String value) {
        this.refundType = value;
    }

    /**
     * Gets the value of the refundMemo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundMemo() {
        return refundMemo;
    }

    /**
     * Sets the value of the refundMemo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundMemo(String value) {
        this.refundMemo = value;
    }

    /**
     * Gets the value of the refundSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundSegment() {
        return refundSegment;
    }

    /**
     * Sets the value of the refundSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundSegment(String value) {
        this.refundSegment = value;
    }

    /**
     * Gets the value of the refundNotifyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundNotifyUrl() {
        return refundNotifyUrl;
    }

    /**
     * Sets the value of the refundNotifyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundNotifyUrl(String value) {
        this.refundNotifyUrl = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

}
