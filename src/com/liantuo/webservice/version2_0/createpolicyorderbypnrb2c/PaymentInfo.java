
package com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payerAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="settlePrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="tradeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="webCharge" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="webPayCharge" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="webPayPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentInfo", propOrder = {
    "param1",
    "param2",
    "payerAccount",
    "paymentUrl",
    "settlePrice",
    "tradeNo",
    "webCharge",
    "webPayCharge",
    "webPayPrice"
})
public class PaymentInfo {

    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "payerAccount", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> payerAccount;
    @XmlElementRef(name = "paymentUrl", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> paymentUrl;
    @XmlElementRef(name = "settlePrice", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Double> settlePrice;
    @XmlElementRef(name = "tradeNo", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> tradeNo;
    @XmlElementRef(name = "webCharge", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Double> webCharge;
    @XmlElementRef(name = "webPayCharge", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Double> webPayCharge;
    @XmlElementRef(name = "webPayPrice", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Double> webPayPrice;

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the payerAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayerAccount() {
        return payerAccount;
    }

    /**
     * Sets the value of the payerAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayerAccount(JAXBElement<String> value) {
        this.payerAccount = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the paymentUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentUrl() {
        return paymentUrl;
    }

    /**
     * Sets the value of the paymentUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentUrl(JAXBElement<String> value) {
        this.paymentUrl = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the settlePrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSettlePrice() {
        return settlePrice;
    }

    /**
     * Sets the value of the settlePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSettlePrice(JAXBElement<Double> value) {
        this.settlePrice = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the tradeNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTradeNo() {
        return tradeNo;
    }

    /**
     * Sets the value of the tradeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTradeNo(JAXBElement<String> value) {
        this.tradeNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the webCharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getWebCharge() {
        return webCharge;
    }

    /**
     * Sets the value of the webCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setWebCharge(JAXBElement<Double> value) {
        this.webCharge = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the webPayCharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getWebPayCharge() {
        return webPayCharge;
    }

    /**
     * Sets the value of the webPayCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setWebPayCharge(JAXBElement<Double> value) {
        this.webPayCharge = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the webPayPrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getWebPayPrice() {
        return webPayPrice;
    }

    /**
     * Sets the value of the webPayPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setWebPayPrice(JAXBElement<Double> value) {
        this.webPayPrice = ((JAXBElement<Double> ) value);
    }

}
