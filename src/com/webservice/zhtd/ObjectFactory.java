
package com.webservice.zhtd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://tempuri.org/", "string");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetModifyApplyProvision }
     * 
     */
    public GetModifyApplyProvision createGetModifyApplyProvision() {
        return new GetModifyApplyProvision();
    }

    /**
     * Create an instance of {@link GetModifyApplyProvisionResponse }
     * 
     */
    public GetModifyApplyProvisionResponse createGetModifyApplyProvisionResponse() {
        return new GetModifyApplyProvisionResponse();
    }

    /**
     * Create an instance of {@link UpateStatus }
     * 
     */
    public UpateStatus createUpateStatus() {
        return new UpateStatus();
    }

    /**
     * Create an instance of {@link GetPolicyAllListResponse }
     * 
     */
    public GetPolicyAllListResponse createGetPolicyAllListResponse() {
        return new GetPolicyAllListResponse();
    }

    /**
     * Create an instance of {@link GetSuppierWorkTime }
     * 
     */
    public GetSuppierWorkTime createGetSuppierWorkTime() {
        return new GetSuppierWorkTime();
    }

    /**
     * Create an instance of {@link GetSuppierWorkTimeResponse }
     * 
     */
    public GetSuppierWorkTimeResponse createGetSuppierWorkTimeResponse() {
        return new GetSuppierWorkTimeResponse();
    }

    /**
     * Create an instance of {@link GetPolicyAllList }
     * 
     */
    public GetPolicyAllList createGetPolicyAllList() {
        return new GetPolicyAllList();
    }

    /**
     * Create an instance of {@link GetPolicyAsyncALLList }
     * 
     */
    public GetPolicyAsyncALLList createGetPolicyAsyncALLList() {
        return new GetPolicyAsyncALLList();
    }

    /**
     * Create an instance of {@link GetPolicyAsyncALLListResponse }
     * 
     */
    public GetPolicyAsyncALLListResponse createGetPolicyAsyncALLListResponse() {
        return new GetPolicyAsyncALLListResponse();
    }

    /**
     * Create an instance of {@link UpateStatusResponse }
     * 
     */
    public UpateStatusResponse createUpateStatusResponse() {
        return new UpateStatusResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

}
