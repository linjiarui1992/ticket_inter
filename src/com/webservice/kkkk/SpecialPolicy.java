
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecialPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecialPolicy">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}Policy">
 *       &lt;sequence>
 *         &lt;element name="PriceType" type="{http://tempuri.org/}SpecialPolicyType"/>
 *         &lt;element name="FixedType" type="{http://tempuri.org/}SpecialFixedAddType"/>
 *         &lt;element name="FixedValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsMustPat" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SpecialEI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecialPolicy", propOrder = {
    "priceType",
    "fixedType",
    "fixedValue",
    "isMustPat",
    "specialEI"
})
public class SpecialPolicy
    extends Policy
{

    @XmlElement(name = "PriceType", required = true)
    protected SpecialPolicyType priceType;
    @XmlElement(name = "FixedType", required = true, nillable = true)
    protected SpecialFixedAddType fixedType;
    @XmlElement(name = "FixedValue", required = true, nillable = true)
    protected BigDecimal fixedValue;
    @XmlElement(name = "IsMustPat", required = true, type = Boolean.class, nillable = true)
    protected Boolean isMustPat;
    @XmlElement(name = "SpecialEI")
    protected String specialEI;

    /**
     * Gets the value of the priceType property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialPolicyType }
     *     
     */
    public SpecialPolicyType getPriceType() {
        return priceType;
    }

    /**
     * Sets the value of the priceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialPolicyType }
     *     
     */
    public void setPriceType(SpecialPolicyType value) {
        this.priceType = value;
    }

    /**
     * Gets the value of the fixedType property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialFixedAddType }
     *     
     */
    public SpecialFixedAddType getFixedType() {
        return fixedType;
    }

    /**
     * Sets the value of the fixedType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialFixedAddType }
     *     
     */
    public void setFixedType(SpecialFixedAddType value) {
        this.fixedType = value;
    }

    /**
     * Gets the value of the fixedValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFixedValue() {
        return fixedValue;
    }

    /**
     * Sets the value of the fixedValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFixedValue(BigDecimal value) {
        this.fixedValue = value;
    }

    /**
     * Gets the value of the isMustPat property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsMustPat() {
        return isMustPat;
    }

    /**
     * Sets the value of the isMustPat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMustPat(Boolean value) {
        this.isMustPat = value;
    }

    /**
     * Gets the value of the specialEI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialEI() {
        return specialEI;
    }

    /**
     * Sets the value of the specialEI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialEI(String value) {
        this.specialEI = value;
    }

}
