
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Seat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Seat">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NeedDelete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SeatType" type="{http://tempuri.org/}SeatType"/>
 *         &lt;element name="SeatName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeatPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsHavePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RoundPreferential" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SeatRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SeatComeIn" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SeatFinalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OneWayRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OneWayComeIn" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OneWayFinalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="RoundPreferentialRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="RoundPreferentialComeIn" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="RoundPreferentialFinalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="LazyRebateStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecialPolicyList" type="{http://tempuri.org/}ArrayOfSpecialPolicy" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Seat", propOrder = {
    "name",
    "count",
    "needDelete",
    "seatType",
    "seatName",
    "ei",
    "seatPrice",
    "discount",
    "isHavePolicy",
    "roundPreferential",
    "seatRebate",
    "seatComeIn",
    "seatFinalPrice",
    "oneWayRebate",
    "oneWayComeIn",
    "oneWayFinalPrice",
    "roundPreferentialRebate",
    "roundPreferentialComeIn",
    "roundPreferentialFinalPrice",
    "lazyRebateStr",
    "specialPolicyList"
})
public class Seat {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Count")
    protected int count;
    @XmlElement(name = "NeedDelete")
    protected boolean needDelete;
    @XmlElement(name = "SeatType", required = true)
    protected SeatType seatType;
    @XmlElement(name = "SeatName")
    protected String seatName;
    @XmlElement(name = "EI")
    protected String ei;
    @XmlElement(name = "SeatPrice", required = true)
    protected BigDecimal seatPrice;
    @XmlElement(name = "Discount", required = true)
    protected BigDecimal discount;
    @XmlElement(name = "IsHavePolicy")
    protected boolean isHavePolicy;
    @XmlElement(name = "RoundPreferential")
    protected boolean roundPreferential;
    @XmlElement(name = "SeatRebate", required = true)
    protected BigDecimal seatRebate;
    @XmlElement(name = "SeatComeIn", required = true)
    protected BigDecimal seatComeIn;
    @XmlElement(name = "SeatFinalPrice", required = true)
    protected BigDecimal seatFinalPrice;
    @XmlElement(name = "OneWayRebate", required = true)
    protected BigDecimal oneWayRebate;
    @XmlElement(name = "OneWayComeIn", required = true)
    protected BigDecimal oneWayComeIn;
    @XmlElement(name = "OneWayFinalPrice", required = true)
    protected BigDecimal oneWayFinalPrice;
    @XmlElement(name = "RoundPreferentialRebate", required = true)
    protected BigDecimal roundPreferentialRebate;
    @XmlElement(name = "RoundPreferentialComeIn", required = true)
    protected BigDecimal roundPreferentialComeIn;
    @XmlElement(name = "RoundPreferentialFinalPrice", required = true)
    protected BigDecimal roundPreferentialFinalPrice;
    @XmlElement(name = "LazyRebateStr")
    protected String lazyRebateStr;
    @XmlElement(name = "SpecialPolicyList")
    protected ArrayOfSpecialPolicy specialPolicyList;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the count property.
     * 
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     */
    public void setCount(int value) {
        this.count = value;
    }

    /**
     * Gets the value of the needDelete property.
     * 
     */
    public boolean isNeedDelete() {
        return needDelete;
    }

    /**
     * Sets the value of the needDelete property.
     * 
     */
    public void setNeedDelete(boolean value) {
        this.needDelete = value;
    }

    /**
     * Gets the value of the seatType property.
     * 
     * @return
     *     possible object is
     *     {@link SeatType }
     *     
     */
    public SeatType getSeatType() {
        return seatType;
    }

    /**
     * Sets the value of the seatType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatType }
     *     
     */
    public void setSeatType(SeatType value) {
        this.seatType = value;
    }

    /**
     * Gets the value of the seatName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatName() {
        return seatName;
    }

    /**
     * Sets the value of the seatName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatName(String value) {
        this.seatName = value;
    }

    /**
     * Gets the value of the ei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEI() {
        return ei;
    }

    /**
     * Sets the value of the ei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEI(String value) {
        this.ei = value;
    }

    /**
     * Gets the value of the seatPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatPrice() {
        return seatPrice;
    }

    /**
     * Sets the value of the seatPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatPrice(BigDecimal value) {
        this.seatPrice = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the isHavePolicy property.
     * 
     */
    public boolean isIsHavePolicy() {
        return isHavePolicy;
    }

    /**
     * Sets the value of the isHavePolicy property.
     * 
     */
    public void setIsHavePolicy(boolean value) {
        this.isHavePolicy = value;
    }

    /**
     * Gets the value of the roundPreferential property.
     * 
     */
    public boolean isRoundPreferential() {
        return roundPreferential;
    }

    /**
     * Sets the value of the roundPreferential property.
     * 
     */
    public void setRoundPreferential(boolean value) {
        this.roundPreferential = value;
    }

    /**
     * Gets the value of the seatRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatRebate() {
        return seatRebate;
    }

    /**
     * Sets the value of the seatRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatRebate(BigDecimal value) {
        this.seatRebate = value;
    }

    /**
     * Gets the value of the seatComeIn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatComeIn() {
        return seatComeIn;
    }

    /**
     * Sets the value of the seatComeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatComeIn(BigDecimal value) {
        this.seatComeIn = value;
    }

    /**
     * Gets the value of the seatFinalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatFinalPrice() {
        return seatFinalPrice;
    }

    /**
     * Sets the value of the seatFinalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatFinalPrice(BigDecimal value) {
        this.seatFinalPrice = value;
    }

    /**
     * Gets the value of the oneWayRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOneWayRebate() {
        return oneWayRebate;
    }

    /**
     * Sets the value of the oneWayRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOneWayRebate(BigDecimal value) {
        this.oneWayRebate = value;
    }

    /**
     * Gets the value of the oneWayComeIn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOneWayComeIn() {
        return oneWayComeIn;
    }

    /**
     * Sets the value of the oneWayComeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOneWayComeIn(BigDecimal value) {
        this.oneWayComeIn = value;
    }

    /**
     * Gets the value of the oneWayFinalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOneWayFinalPrice() {
        return oneWayFinalPrice;
    }

    /**
     * Sets the value of the oneWayFinalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOneWayFinalPrice(BigDecimal value) {
        this.oneWayFinalPrice = value;
    }

    /**
     * Gets the value of the roundPreferentialRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoundPreferentialRebate() {
        return roundPreferentialRebate;
    }

    /**
     * Sets the value of the roundPreferentialRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoundPreferentialRebate(BigDecimal value) {
        this.roundPreferentialRebate = value;
    }

    /**
     * Gets the value of the roundPreferentialComeIn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoundPreferentialComeIn() {
        return roundPreferentialComeIn;
    }

    /**
     * Sets the value of the roundPreferentialComeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoundPreferentialComeIn(BigDecimal value) {
        this.roundPreferentialComeIn = value;
    }

    /**
     * Gets the value of the roundPreferentialFinalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRoundPreferentialFinalPrice() {
        return roundPreferentialFinalPrice;
    }

    /**
     * Sets the value of the roundPreferentialFinalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRoundPreferentialFinalPrice(BigDecimal value) {
        this.roundPreferentialFinalPrice = value;
    }

    /**
     * Gets the value of the lazyRebateStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLazyRebateStr() {
        return lazyRebateStr;
    }

    /**
     * Sets the value of the lazyRebateStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLazyRebateStr(String value) {
        this.lazyRebateStr = value;
    }

    /**
     * Gets the value of the specialPolicyList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSpecialPolicy }
     *     
     */
    public ArrayOfSpecialPolicy getSpecialPolicyList() {
        return specialPolicyList;
    }

    /**
     * Sets the value of the specialPolicyList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSpecialPolicy }
     *     
     */
    public void setSpecialPolicyList(ArrayOfSpecialPolicy value) {
        this.specialPolicyList = value;
    }

}
