
package com.webservice.kkkk;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfVoyageView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfVoyageView">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoyageView" type="{http://tempuri.org/}VoyageView" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfVoyageView", propOrder = {
    "voyageView"
})
public class ArrayOfVoyageView {

    @XmlElement(name = "VoyageView", nillable = true)
    protected List<VoyageView> voyageView;

    /**
     * Gets the value of the voyageView property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voyageView property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoyageView().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoyageView }
     * 
     * 
     */
    public List<VoyageView> getVoyageView() {
        if (voyageView == null) {
            voyageView = new ArrayList<VoyageView>();
        }
        return this.voyageView;
    }

}
