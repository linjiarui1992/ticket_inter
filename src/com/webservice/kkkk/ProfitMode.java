
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfitMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProfitMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Rebate"/>
 *     &lt;enumeration value="Money"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProfitMode")
@XmlEnum
public enum ProfitMode {

    @XmlEnumValue("Rebate")
    REBATE("Rebate"),
    @XmlEnumValue("Money")
    MONEY("Money");
    private final String value;

    ProfitMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProfitMode fromValue(String v) {
        for (ProfitMode c: ProfitMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
