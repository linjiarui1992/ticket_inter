
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Normal"/>
 *     &lt;enumeration value="Refound"/>
 *     &lt;enumeration value="Scrap"/>
 *     &lt;enumeration value="Postpone"/>
 *     &lt;enumeration value="Upgrade"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderType")
@XmlEnum
public enum OrderType {

    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Refound")
    REFOUND("Refound"),
    @XmlEnumValue("Scrap")
    SCRAP("Scrap"),
    @XmlEnumValue("Postpone")
    POSTPONE("Postpone"),
    @XmlEnumValue("Upgrade")
    UPGRADE("Upgrade");
    private final String value;

    OrderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrderType fromValue(String v) {
        for (OrderType c: OrderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
