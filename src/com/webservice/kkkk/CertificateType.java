
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CertificateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CertificateType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IdentityCard"/>
 *     &lt;enumeration value="Passport"/>
 *     &lt;enumeration value="StudentCard"/>
 *     &lt;enumeration value="BirthDate"/>
 *     &lt;enumeration value="CardSoldiers"/>
 *     &lt;enumeration value="MilitaryID"/>
 *     &lt;enumeration value="MTP"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CertificateType")
@XmlEnum
public enum CertificateType {

    @XmlEnumValue("IdentityCard")
    IDENTITY_CARD("IdentityCard"),
    @XmlEnumValue("Passport")
    PASSPORT("Passport"),
    @XmlEnumValue("StudentCard")
    STUDENT_CARD("StudentCard"),
    @XmlEnumValue("BirthDate")
    BIRTH_DATE("BirthDate"),
    @XmlEnumValue("CardSoldiers")
    CARD_SOLDIERS("CardSoldiers"),
    @XmlEnumValue("MilitaryID")
    MILITARY_ID("MilitaryID"),
    MTP("MTP"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    CertificateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CertificateType fromValue(String v) {
        for (CertificateType c: CertificateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
