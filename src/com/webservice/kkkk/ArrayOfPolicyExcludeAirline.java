
package com.webservice.kkkk;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPolicyExcludeAirline complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPolicyExcludeAirline">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyExcludeAirline" type="{http://tempuri.org/}PolicyExcludeAirline" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPolicyExcludeAirline", propOrder = {
    "policyExcludeAirline"
})
public class ArrayOfPolicyExcludeAirline {

    @XmlElement(name = "PolicyExcludeAirline", nillable = true)
    protected List<PolicyExcludeAirline> policyExcludeAirline;

    /**
     * Gets the value of the policyExcludeAirline property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyExcludeAirline property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyExcludeAirline().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyExcludeAirline }
     * 
     * 
     */
    public List<PolicyExcludeAirline> getPolicyExcludeAirline() {
        if (policyExcludeAirline == null) {
            policyExcludeAirline = new ArrayList<PolicyExcludeAirline>();
        }
        return this.policyExcludeAirline;
    }

}
