
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryPolicyCoordinationResult" type="{http://tempuri.org/}ArrayOfPolicyCoordination" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryPolicyCoordinationResult"
})
@XmlRootElement(name = "QueryPolicyCoordinationResponse")
public class QueryPolicyCoordinationResponse {

    @XmlElement(name = "QueryPolicyCoordinationResult")
    protected ArrayOfPolicyCoordination queryPolicyCoordinationResult;

    /**
     * Gets the value of the queryPolicyCoordinationResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicyCoordination }
     *     
     */
    public ArrayOfPolicyCoordination getQueryPolicyCoordinationResult() {
        return queryPolicyCoordinationResult;
    }

    /**
     * Sets the value of the queryPolicyCoordinationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicyCoordination }
     *     
     */
    public void setQueryPolicyCoordinationResult(ArrayOfPolicyCoordination value) {
        this.queryPolicyCoordinationResult = value;
    }

}
