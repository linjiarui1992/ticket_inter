
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplyStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplyStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Applied"/>
 *     &lt;enumeration value="Delay"/>
 *     &lt;enumeration value="Denied"/>
 *     &lt;enumeration value="Processed"/>
 *     &lt;enumeration value="Failed"/>
 *     &lt;enumeration value="Succeeded"/>
 *     &lt;enumeration value="DeniedByFinancial"/>
 *     &lt;enumeration value="ProcessedForAudit"/>
 *     &lt;enumeration value="Canceled"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApplyStatus")
@XmlEnum
public enum ApplyStatus {

    @XmlEnumValue("Applied")
    APPLIED("Applied"),
    @XmlEnumValue("Delay")
    DELAY("Delay"),
    @XmlEnumValue("Denied")
    DENIED("Denied"),
    @XmlEnumValue("Processed")
    PROCESSED("Processed"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Succeeded")
    SUCCEEDED("Succeeded"),
    @XmlEnumValue("DeniedByFinancial")
    DENIED_BY_FINANCIAL("DeniedByFinancial"),
    @XmlEnumValue("ProcessedForAudit")
    PROCESSED_FOR_AUDIT("ProcessedForAudit"),
    @XmlEnumValue("Canceled")
    CANCELED("Canceled");
    private final String value;

    ApplyStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApplyStatus fromValue(String v) {
        for (ApplyStatus c: ApplyStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
