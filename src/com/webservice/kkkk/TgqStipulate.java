
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TgqStipulate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TgqStipulate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="AirLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirLineCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeStipulate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuitTicketStipulate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlinePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeeTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderbyAirline" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TgqStipulate", propOrder = {
    "id",
    "airLine",
    "airLineCode",
    "changeStipulate",
    "quitTicketStipulate",
    "remark",
    "airlinePhone",
    "feeTicket",
    "orderbyAirline"
})
public class TgqStipulate {

    @XmlElement(name = "ID", required = true)
    protected BigDecimal id;
    @XmlElement(name = "AirLine")
    protected String airLine;
    @XmlElement(name = "AirLineCode")
    protected String airLineCode;
    @XmlElement(name = "ChangeStipulate")
    protected String changeStipulate;
    @XmlElement(name = "QuitTicketStipulate")
    protected String quitTicketStipulate;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "AirlinePhone")
    protected String airlinePhone;
    @XmlElement(name = "FeeTicket")
    protected String feeTicket;
    @XmlElement(name = "OrderbyAirline", required = true)
    protected BigDecimal orderbyAirline;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setID(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the airLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirLine() {
        return airLine;
    }

    /**
     * Sets the value of the airLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirLine(String value) {
        this.airLine = value;
    }

    /**
     * Gets the value of the airLineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirLineCode() {
        return airLineCode;
    }

    /**
     * Sets the value of the airLineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirLineCode(String value) {
        this.airLineCode = value;
    }

    /**
     * Gets the value of the changeStipulate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStipulate() {
        return changeStipulate;
    }

    /**
     * Sets the value of the changeStipulate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStipulate(String value) {
        this.changeStipulate = value;
    }

    /**
     * Gets the value of the quitTicketStipulate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuitTicketStipulate() {
        return quitTicketStipulate;
    }

    /**
     * Sets the value of the quitTicketStipulate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuitTicketStipulate(String value) {
        this.quitTicketStipulate = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the airlinePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlinePhone() {
        return airlinePhone;
    }

    /**
     * Sets the value of the airlinePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlinePhone(String value) {
        this.airlinePhone = value;
    }

    /**
     * Gets the value of the feeTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeTicket() {
        return feeTicket;
    }

    /**
     * Sets the value of the feeTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeTicket(String value) {
        this.feeTicket = value;
    }

    /**
     * Gets the value of the orderbyAirline property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderbyAirline() {
        return orderbyAirline;
    }

    /**
     * Sets the value of the orderbyAirline property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderbyAirline(BigDecimal value) {
        this.orderbyAirline = value;
    }

}
