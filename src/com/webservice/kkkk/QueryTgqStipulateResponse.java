
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryTgqStipulateResult" type="{http://tempuri.org/}ArrayOfTgqStipulate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryTgqStipulateResult"
})
@XmlRootElement(name = "QueryTgqStipulateResponse")
public class QueryTgqStipulateResponse {

    @XmlElement(name = "QueryTgqStipulateResult")
    protected ArrayOfTgqStipulate queryTgqStipulateResult;

    /**
     * Gets the value of the queryTgqStipulateResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTgqStipulate }
     *     
     */
    public ArrayOfTgqStipulate getQueryTgqStipulateResult() {
        return queryTgqStipulateResult;
    }

    /**
     * Sets the value of the queryTgqStipulateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTgqStipulate }
     *     
     */
    public void setQueryTgqStipulateResult(ArrayOfTgqStipulate value) {
        this.queryTgqStipulateResult = value;
    }

}
