
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsCanPayResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isCanPayResult"
})
@XmlRootElement(name = "IsCanPayResponse")
public class IsCanPayResponse {

    @XmlElement(name = "IsCanPayResult")
    protected boolean isCanPayResult;

    /**
     * Gets the value of the isCanPayResult property.
     * 
     */
    public boolean isIsCanPayResult() {
        return isCanPayResult;
    }

    /**
     * Sets the value of the isCanPayResult property.
     * 
     */
    public void setIsCanPayResult(boolean value) {
        this.isCanPayResult = value;
    }

}
