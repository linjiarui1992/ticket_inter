
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaserInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaserInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Type" type="{http://tempuri.org/}CompanyOemType"/>
 *         &lt;element name="SuperiorCompanyId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Purchaser" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PurchaserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaserInfo", propOrder = {
    "type",
    "superiorCompanyId",
    "purchaser",
    "purchaserName"
})
public class PurchaserInfo {

    @XmlElement(name = "Type", required = true)
    protected CompanyOemType type;
    @XmlElement(name = "SuperiorCompanyId", required = true)
    protected BigDecimal superiorCompanyId;
    @XmlElement(name = "Purchaser", required = true)
    protected BigDecimal purchaser;
    @XmlElement(name = "PurchaserName")
    protected String purchaserName;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyOemType }
     *     
     */
    public CompanyOemType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyOemType }
     *     
     */
    public void setType(CompanyOemType value) {
        this.type = value;
    }

    /**
     * Gets the value of the superiorCompanyId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSuperiorCompanyId() {
        return superiorCompanyId;
    }

    /**
     * Sets the value of the superiorCompanyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSuperiorCompanyId(BigDecimal value) {
        this.superiorCompanyId = value;
    }

    /**
     * Gets the value of the purchaser property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaser() {
        return purchaser;
    }

    /**
     * Sets the value of the purchaser property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaser(BigDecimal value) {
        this.purchaser = value;
    }

    /**
     * Gets the value of the purchaserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaserName() {
        return purchaserName;
    }

    /**
     * Sets the value of the purchaserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaserName(String value) {
        this.purchaserName = value;
    }

}
