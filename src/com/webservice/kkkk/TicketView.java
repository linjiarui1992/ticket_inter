
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TicketView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TicketView">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EdtzMode" type="{http://tempuri.org/}EtdzMode"/>
 *         &lt;element name="EdtzTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Voyages" type="{http://tempuri.org/}ArrayOfVoyageView" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketView", propOrder = {
    "id",
    "number",
    "edtzMode",
    "edtzTime",
    "voyages"
})
public class TicketView {

    @XmlElement(name = "Id", required = true)
    protected BigDecimal id;
    @XmlElement(name = "Number")
    protected String number;
    @XmlElement(name = "EdtzMode", required = true, nillable = true)
    protected EtdzMode edtzMode;
    @XmlElement(name = "EdtzTime", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar edtzTime;
    @XmlElement(name = "Voyages")
    protected ArrayOfVoyageView voyages;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the edtzMode property.
     * 
     * @return
     *     possible object is
     *     {@link EtdzMode }
     *     
     */
    public EtdzMode getEdtzMode() {
        return edtzMode;
    }

    /**
     * Sets the value of the edtzMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtdzMode }
     *     
     */
    public void setEdtzMode(EtdzMode value) {
        this.edtzMode = value;
    }

    /**
     * Gets the value of the edtzTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdtzTime() {
        return edtzTime;
    }

    /**
     * Sets the value of the edtzTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdtzTime(XMLGregorianCalendar value) {
        this.edtzTime = value;
    }

    /**
     * Gets the value of the voyages property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVoyageView }
     *     
     */
    public ArrayOfVoyageView getVoyages() {
        return voyages;
    }

    /**
     * Sets the value of the voyages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVoyageView }
     *     
     */
    public void setVoyages(ArrayOfVoyageView value) {
        this.voyages = value;
    }

}
