package com.cm.service;


import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.ElongAddOrder;
import com.ccservice.b2b2c.yilong.ElongCancelOrder;
import com.ccservice.b2b2c.yilong.ElongPaySuccess;
import com.ccservice.b2b2c.yilong.ElongRefundNotify;
import com.ccservice.b2b2c.yilong.HthyLockOrder;


public class ELongTrainOffline {

      public static void main(String[] args) {
        String ss="<?xml version=\"1.0\" encoding=\"utf-8\"?><OrderRequest>  <Authentication>    <ServiceName>order.pay</ServiceName>    <PartnerName>elong</PartnerName>    <TimeStamp>2016-06-01 17:17:21</TimeStamp>    <MessageIdentity>1c22a07411f02e26d84f490f042a3539</MessageIdentity>  </Authentication>  <TrainOrderService>    <OrderNumber>elong_20160531340021625</OrderNumber>    <PayedPrice>729.5</PayedPrice>    <PayTime>2016-06-01 17:17:21</PayTime>    <PayType>yuejie</PayType>    <tradeNumber>20160531340021625</tradeNumber>  </TrainOrderService></OrderRequest>";
        updateAction(ss);
      }
	public static String updateAction(String xml) {
		WriteLog.write("ELONG_请求xml", "xml:"+xml);
		String response="";
		try {
			Document document = DocumentHelper.parseText(xml);

			Element root = document.getRootElement();
			// Authentication
			Element Authentication = root.element("Authentication");
			String serviceName = Authentication.elementText("ServiceName");// 接口服务名
			System.out.println("serviceNameserviceName="+serviceName);
			
			if("order.addOrder".equals(serviceName)){
				response=new ElongAddOrder().addOfflinOrder(xml);
				WriteLog.write("ELong_ticket_inter下单回调接口response", "response："+response);
			}else if ("order.pay".equals(serviceName)){
				response=new ElongPaySuccess().paySuccess(xml);
				WriteLog.write("ELong_ticket_inter支付成功接口回调response", "response："+response);
				//支付成功后调用锁单操作
//				String lockresult=new HthyLockOrder().autoLockOrder(response);
//				WriteLog.write("ELong_ticket_inter请求艺龙锁单response信息", "response："+lockresult);
			}else if("order.cancelOrder".equals(serviceName)){
				response=new ElongCancelOrder().cancelOrder(xml);
				WriteLog.write("ELong_ticket_inter取消订单接口回调response", "response："+response);
			}else if("order.refundNotify".equals(serviceName)){
				response=new ElongRefundNotify().refundNotify(xml);
				WriteLog.write("ELong_ticket_inter退款成功接口回调response", "response："+response);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return response;
	}

}