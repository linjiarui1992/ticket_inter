
/**
 * AirWsCoManageCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.baitour.www.version1;

    /**
     *  AirWsCoManageCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AirWsCoManageCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AirWsCoManageCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AirWsCoManageCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getInvalidationProviderOfficeNoList method
            * override this method for handling normal response from getInvalidationProviderOfficeNoList operation
            */
           public void receiveResultgetInvalidationProviderOfficeNoList(
                    com.baitour.www.version1.AirWsCoManageStub.GetInvalidationProviderOfficeNoListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviderOfficeNoList operation
           */
            public void receiveErrorgetInvalidationProviderOfficeNoList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterDomesticCabinZRateByParam method
            * override this method for handling normal response from getAlterDomesticCabinZRateByParam operation
            */
           public void receiveResultgetAlterDomesticCabinZRateByParam(
                    com.baitour.www.version1.AirWsCoManageStub.GetAlterDomesticCabinZRateByParamResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterDomesticCabinZRateByParam operation
           */
            public void receiveErrorgetAlterDomesticCabinZRateByParam(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterDomesticCabinZRateByXml method
            * override this method for handling normal response from getAlterDomesticCabinZRateByXml operation
            */
           public void receiveResultgetAlterDomesticCabinZRateByXml(
                    com.baitour.www.version1.AirWsCoManageStub.GetAlterDomesticCabinZRateByXmlResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterDomesticCabinZRateByXml operation
           */
            public void receiveErrorgetAlterDomesticCabinZRateByXml(java.lang.Exception e) {
            }
                


    }
    