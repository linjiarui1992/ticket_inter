package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 华闽国际酒店数据
 */
public class UpdateInternationalHotelInfoJob implements Job {

	@Override
	public void execute(JobExecutionContext arg) throws JobExecutionException {
		System.out.println("开始更新华闽国际酒店");
		new HuaminInternationalHotelDB().getHmHotelInfo();
		System.out.println("更新华闽国际酒店完成");
	}
	
}
