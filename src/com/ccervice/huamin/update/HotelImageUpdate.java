package com.ccervice.huamin.update;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.io.FileOutputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.huamin.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 *去哪儿抓取酒店图片
 *http://hotel.qunar.com/render/hotelDetailAllImage.jsp?hotelseq=beijing_city_298
 *http://userimg.qunar.com/imgs/201012/24/kkQI3k1qkql1nUki312.jpg
 */
public class HotelImageUpdate {

    @SuppressWarnings("unchecked")
    public void test() {
        try {
            List<City> citys = Server.getInstance().getHotelService()
                    .findAllCity("where c_type = 1", "order by id", -1, 0);
            for (City city : citys) {
                new HotelImageUpdate().findImage(city.getId());
            }
        }
        catch (Exception e) {
            WriteLog.write("抓取去哪儿酒店图片异常", e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public void findImage(long cityid) {
        String sql = "where C_CITYID = "
                + cityid
                + " and ID not in (select distinct C_ZSHOTELID from T_HOTELIMAGE where C_ZSHOTELID is not null and C_SIZETYPE !=3)";
        List<Hotelall> hotelList = Server.getInstance().getHotelService().findAllHotelall(sql, "", -1, 0);
        int count = hotelList.size();
        for (Hotelall h : hotelList) {
            try {
                if (ElongHotelInterfaceUtil.StringIsNull(h.getQunarId()))
                    continue;
                List<Hotelimage> imageList = Server.getInstance().getHotelService()
                        .findAllHotelimage("where C_ZSHOTELID = " + h.getId() + " and C_SIZETYPE != 3", "", -1, 0);
                if (imageList != null && imageList.size() > 0) {
                    continue;
                }
                //请求地址
                String urlimg = "http://hotel.qunar.com/render/hotelDetailAllImage.jsp?hotelseq=" + h.getQunarId();
                System.out.println("当前抓取图片酒店：" + h.getName() + "[ " + h.getQunarId() + " ]，剩余酒店数量：" + count--
                        + "，请求地址：" + urlimg);
                //返回JSON串
                String json = PHUtil.submitPost(urlimg, "").toString();
                /**
                 * {
                 *   "normalType": "故宫景房",
                 *   "sourceWidth": 785,
                 *   "sourceHeight": 387,
                 *   "imageType": "客房",
                 *   "gid": "201304/01/Z7-ECTZiizaIie5DZ",
                 *   "source": "老板",
                 *   "title": "故宫景房",
                 *   "wrapperId": null,
                 *   "customer": "酒店上传",
                 *   "hotelSeq": "beijing_city_298",
                 *   "content": "2",
                 *   "fid": "1d09aa"
                 * }
                 * url：http://userimg.qunar.com/imgs/201012/24/kkQI3k1qkql1nUki312.jpg
                 */
                Map<String, Boolean> map = new HashMap<String, Boolean>(); //有重复图片的可能，用于判断图片是否存在
                JSONObject img = JSONObject.fromObject(json);
                JSONObject data = img.getJSONObject("data");
                JSONArray all = data.getJSONArray("all");
                if (all.size() == 0)
                    continue;
                //优先取酒店上传
                int ImageCount = 3; //存多少张图片
                for (int i = 0; i < all.size(); i++) {
                    if (map.size() >= ImageCount)
                        break;
                    JSONObject detail = all.getJSONObject(i);
                    String normalType = detail.getString("normalType");
                    String imageType = UpdateElDataUtil.StringIsNull(detail.getString("imageType")) ? "" : detail
                            .getString("imageType");
                    String gid = detail.getString("gid");
                    String customer = detail.getString("customer");
                    String hotelSeq = detail.getString("hotelSeq");
                    if ((map.get(gid) != null && map.get(gid)) || !"酒店上传".equals(customer)
                            || !h.getQunarId().equals(hotelSeq))
                        continue;
                    //赋值
                    Hotelimage hotelImage = new Hotelimage();
                    hotelImage.setPath(gid);//临时
                    hotelImage.setType(getImageType(imageType));
                    hotelImage.setDescription((UpdateElDataUtil.StringIsNull(normalType) || "null".equals(normalType
                            .toLowerCase())) ? imageType : normalType);
                    hotelImage.setHotelid(null); //去哪儿酒店，只存正式酒店ID
                    hotelImage.setLanguage(0);
                    hotelImage.setSizeType(1);//大图，321*208
                    hotelImage.setZshotelid(h.getId());
                    hotelImage.setIsNewFlag(2);//图片来源 2：去哪儿
                    //保存图片
                    saveImage(hotelImage, h.getCityid());
                    //已存在
                    map.put(gid, true);
                }
                //非酒店上传图片
                if (map.size() < ImageCount) {
                    for (int i = 0; i < all.size(); i++) {
                        if (map.size() >= ImageCount)
                            break;
                        JSONObject detail = all.getJSONObject(i);
                        String normalType = detail.getString("normalType");
                        String imageType = UpdateElDataUtil.StringIsNull(detail.getString("imageType")) ? "" : detail
                                .getString("imageType");
                        String gid = detail.getString("gid");
                        String customer = detail.getString("customer");
                        String hotelSeq = detail.getString("hotelSeq");
                        if ((map.get(gid) != null && map.get(gid)) || "酒店上传".equals(customer)
                                || !h.getQunarId().equals(hotelSeq))
                            continue;
                        //赋值
                        Hotelimage hotelImage = new Hotelimage();
                        hotelImage.setPath(gid);//临时
                        hotelImage.setType(getImageType(imageType));
                        hotelImage.setDescription((UpdateElDataUtil.StringIsNull(normalType) || "null"
                                .equals(normalType.toLowerCase())) ? imageType : normalType);
                        hotelImage.setHotelid(null); //去哪儿酒店，只存正式酒店ID
                        hotelImage.setLanguage(0);
                        hotelImage.setSizeType(1);//大图，321*208
                        hotelImage.setZshotelid(h.getId());
                        hotelImage.setIsNewFlag(2);//图片来源 2：去哪儿
                        //保存图片
                        saveImage(hotelImage, h.getCityid());
                        //已存在
                        map.put(gid, true);
                    }
                }
            }
            catch (Exception e) {
                String msg = ElongHotelInterfaceUtil.StringIsNull(e.getMessage()) ? "" : e.getMessage();
                if (msg.contains("503 for URL")
                        || msg.contains("A JSONObject text must begin with '{' at character 0 of")) {
                    //等待3分钟 
                    try {
                        System.out.println("访问频繁，等待3分钟...");
                        Thread.sleep(1000 * 60 * 3);
                    }
                    catch (Exception ex) {
                    }
                }
                else {
                    System.out.println("酒店：" + h.getName() + "[ " + h.getQunarId() + " ]抓取图片异常：" + e.getMessage());
                }
            }
        }
    }

    /**
     * 图片类型
     * 0-展示图；1-餐厅；2-休闲室；3-会议室 ；4-服务；5-酒店外观 ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
     */
    private int getImageType(String imageType) {
        int type = 10;
        if ("外观".equals(imageType)) {
            type = 5;
        }
        else if ("大厅".equals(imageType)) {
            type = 6;
        }
        else if ("客房".equals(imageType) || "浴室".equals(imageType)) {
            type = 8;
        }
        else if ("餐厅".equals(imageType)) {
            type = 1;
        }
        else if ("娱乐设施".equals(imageType)) {
            type = 2;
        }
        return type;
    }

    /**
     * 保存酒店列表图片，一个酒店只存一张
     */
    private void saveImage(Hotelimage img, long cityid) {
        try {
            long hotelid = img.getZshotelid();
            //本地存储路径
            String tempPath = "";
            if (hotelid < 10) {
                tempPath = "0" + hotelid;
            }
            else {
                String strid = Long.toString(hotelid);
                tempPath = strid.substring(strid.length() - 2, strid.length()); //后两位数字
            }
            String filePath = "D:\\hotelimage\\" + cityid + "\\" + tempPath + "\\" + hotelid;
            if (!new File(filePath).exists()) {
                new File(filePath).mkdirs();
            }
            //库中存储图片名称
            String ImgUrl = "http://userimg.qunar.com/imgs/" + img.getPath() + "i312.jpg";//http://userimg.qunar.com/imgs/201012/24/kkQI3k1qkql1nUki312.jpg
            String localImgName = ImgUrl.substring(ImgUrl.lastIndexOf("/") + 1, ImgUrl.lastIndexOf(".")) + ".jpg";
            //本地存储路径
            String localPath = filePath + "\\" + localImgName;
            //判断图片是否存在
            if (!new File(localPath).exists()) {
                //下载去哪儿图片
                download(ImgUrl, localPath);
            }
            //修改数据库记录
            img.setPath("/hotelimage/" + cityid + "/" + tempPath + "/" + hotelid + "/" + localImgName);
            img = Server.getInstance().getHotelService().createHotelimage(img);
            System.out.println("新增了一条图片记录----------" + img.getId());
        }
        catch (Exception e) {

        }
    }

    /**
     * 下载图片
     */
    private void download(String urlString, String filename) throws Exception {
        System.out.println("开始下载图片----------" + filename);
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        InputStream is = con.getInputStream();
        byte[] bs = new byte[1024];
        int len;
        OutputStream os = new FileOutputStream(filename);
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        os.close();
        is.close();
    }
}
