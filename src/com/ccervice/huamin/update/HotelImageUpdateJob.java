package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *去哪儿抓取酒店图片
 */
public class HotelImageUpdateJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("酒店图片程序开始");
		new HotelImageUpdate().test();
		System.out.println("酒店图片程序结束");
	}

}
