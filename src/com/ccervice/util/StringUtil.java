package com.ccervice.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * 
 * @time 2015年10月19日 下午3:58:33
 * @author chendong
 */
public class StringUtil {
    public static void main(String[] args) {
        System.out.println(getStringByRegex("<div class=\"con\">15118164820</div>", "\\d{11}"));
    }

    /**
     * 根据正则表达式获取字符串里的字符
     * @time 2015年10月19日 下午3:59:01
     * @author chendong
     */
    public static String getStringByRegex(String str, String regex) {
        //        String str = "";
        //        String regex = "(?<=异常消息:).*?(?=服务方向:)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        while (m.find()) {
            //            System.out.println(m.group());
            str = m.group();
            break;
        }

        return str;

    }
}