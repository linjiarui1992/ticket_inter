package com.ccservice.mail;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccervice.util.db.DBHelperOffline;

public class AddStationTrackInfoDao {

	public final String TongchengURL = "/offtickets/api/HsSupplier/AddStationTrackInfo";

	/**
	 * 8.邮寄接口
	 * @param Status 1: 开始配送, 2: 已送达
	 * @return jsonStr
	 * */
	public String AddStationTrackInfo(int Status,Long orderId) {
		String jsonStr = "";
		String sql = "select OrderNumber from TrainOrderOffline where Id=" + orderId;
		Object object = null;
		try {
			object = DBHelperOffline.GetSingle(sql, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (object != null) {
			String orderNo = object.toString(); // 同城快递单号
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
			int deliveryStatus = Status; // 1: 开始配送, 2: 已送达
			String deliveryTime = df.format(new Date());// new Date()为获取当前系统时间

			// 调用同城接口
			String paramContent = "orderNo=" + orderNo + "&deliveryStatus=" + deliveryStatus + "&deliveryStatus="
					+ deliveryStatus;
			jsonStr = SendPostandGet.submitPost(TongchengURL, paramContent, "UTF-8").toString();

			return jsonStr;
		} else {
			return "";
		}

	}

//	public String EndAddStationTrackInfo(Long orderId) {
//		String sql = "select OrderNumber from TrainOrderOffline where Id=" + orderId;
//		Object object = null;
//		try {
//			object = DBHelperOffline.GetSingle(sql, null);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (object != null) {
//			String orderNo = object.toString(); // 同城快递单号
//			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
//			int deliveryStatus = 2; // 1: 开始配送, 2: 已送达
//			String deliveryTime = df.format(new Date());// new Date()为获取当前系统时间
//
//			// 调用同城接口
//			JSONObject responsejson = new JSONObject();
//
//			return responsejson.toString();
//		} else {
//			return "";
//		}
//
//	}
}
