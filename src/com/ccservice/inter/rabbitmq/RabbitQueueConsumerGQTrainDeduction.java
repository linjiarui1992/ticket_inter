package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.train.qunar.GqTrainorderDeduction;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * 改签扣款实现
 * 
 * @author 之至
 * @time 2016年11月19日 下午5:12:22
 */
public class RabbitQueueConsumerGQTrainDeduction extends QueueConsumer implements Runnable,Consumer{
    private String orderNoticeResult;

    private long orderid;

    private long changeid;

    //2后扣款模式 1预付模式
    private int type;
    
    public RabbitQueueConsumerGQTrainDeduction(String endPointName) throws IOException {
        super(endPointName);
    }
    
    public void handleDelivery(String consumerTag, Envelope env,BasicProperties props, byte[] body) throws IOException {
        Map<?, ?> map = (HashMap<?, ?>)SerializationUtils.deserialize(body);
        try {
            orderNoticeResult=map.get("message number").toString();
            System.out.println("改签扣款  接收 ：————————>"+map.get("message number").toString());
            WriteLog.write("RabbitTrainorderDeductiongq", orderNoticeResult);
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            this.changeid = jsonobject.containsKey("changeid") ? jsonobject.getLongValue("changeid") : 0;
            this.type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            new GqTrainorderDeduction(this.orderid, type, changeid).tongchengDeduction();
            //空铁改签扣款编号
            String kt110 = PropertyUtil.getValue("ktgpkk", "rabbitMQ.properties");
            //平台
            String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
            List list = Server.getInstance().getSystemService().findMapResultBySql("select Status from [TrainPingTai] with(nolock)  where PingTai ="+pingTaiType+"and type ="+kt110, null);
            Map map1=(Map)list.get(0);
            String StatusString= map1.containsKey("Status")?map1.get("Status").toString():"";
            if (!"".equals(StatusString)&&"0".equals(StatusString)) {
                rmcb.getChannel().close();
                rmcb.getConnection().close();
                System.out.println("关闭消费者-----------");
                return;
            }
        }
        catch (Exception e) {
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", "orderid:" + this.orderid + "type:" + this.type+" changeid "+this.changeid+e.getMessage());
        }
    }
}
