package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.rabbitmq.util.RabbitMQUtil;
import com.ccservice.train.mqlistener.Method.TrainChangeListenerMethod;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.weixin.util.RequestUtil;
/**
 * 
 * 改签占座
 * @author RRRRRR
 * @time 2016年11月17日 上午9:18:57
 */
public class RabbitQueueConsumerRequestChange extends QueueConsumer implements Runnable,Consumer{
    
    private long changeId;
    
    public RabbitQueueConsumerRequestChange(String endPointName) throws IOException {
        super(endPointName);
    }
    
    public void handleDelivery(String consumerTag, Envelope env,BasicProperties props, byte[] body) throws IOException {
        Map<?, ?> map = (HashMap<?, ?>)SerializationUtils.deserialize(body);
        try {
            changeId=Long.valueOf(map.get("message number").toString());
            System.out.println("改签占座  接收 ：————————>"+map.get("message number").toString());
        }
        catch (Exception e) {
            changeId = 0;
        }
        //改签占座操作
        if (changeId > 0) {
            RepOperate();
        }
    }
    
    private void RepOperate() {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getRequestIsAsync() == null ? 0 : trainOrderChange
                .getRequestIsAsync().intValue();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            return;
        }
        //状态判断
        int tcstatus = trainOrderChange.getTcstatus();
        //改签退标识
        int changeType = trainOrderChange.getTcischangerefund();
        //问题改签
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        float changePrice = changeType == 1 ? 0 : trainOrderChange.getTcprice();
        //非等待下单
        if (tcstatus != Trainorderchange.APPLYCHANGE || isQuestionChange != 0 || changePrice > 0) {
            WriteLog.write("RabbitMQ重复进入改签占座队列", random + ":改签ID:" + changeId);
            return;
        }
        //是否需要走MemCached 如果距离发车不足3个小时为false，直接拒绝改签 
        boolean isneedmemcachedadd = true;
        //是否是传账号改签
        boolean isUserInformationByInterface = false;
        //通过订单ID查找12306账号
        String loginNameString = changeType == 1 ? "" : loginnameByOrderid(trainOrderChange.getOrderid());
        //改签退、客人账号
        if (ElongHotelInterfaceUtil.StringIsNull(loginNameString)
                || "UserInformationByInterface".equals(loginNameString)) {
            isUserInformationByInterface = true;
        }
        else {
            //将12306账号加入MemCached
            if (TrainChangeListenerMethod.changeing(loginNameString)) {
                isneedmemcachedadd = isNeedMemCachedAdd(trainOrderChange);
                if (isneedmemcachedadd) {
                    //睡眠半分钟
                    for (int i = 0; i < 5; i++) {
                        try {
                            Thread.sleep(6000L);
                        }
                        catch (InterruptedException e) {

                        }
                    }
                    //重新扔进改签队列
                    sendMessageRabbitMQ(changeId);
                }
            }
        }
        //更新改签
        int C_STATUS12306;
        int C_TCSTATUS;
        String updateSql;
        //改签结果
        JSONObject MQ = new JSONObject();
        if (isneedmemcachedadd) {
            C_STATUS12306 = Trainorderchange.ORDERING;
            C_TCSTATUS = Trainorderchange.APPLYROCESSING;
            updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                    + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = " + Trainorderchange.APPLYCHANGE;
            //更新结果
            int updateResult = Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
            //更新失败
            if (updateResult != 1) {
                WriteLog.write("RabbitMQ改签占座队列异常", random + ":改签ID:" + changeId + ":更新改签:" + updateResult);
                return;
            }
            MQ = Server.getInstance().getTrain12306Service().AsyncChangeRequest(trainOrderChange);
        }
        else {
            MQ.put("method", "train_request_change");
            MQ.put("reqtoken", trainOrderChange.getRequestReqtoken());
            MQ.put("callBackUrl", trainOrderChange.getRequestCallBackUrl());
            MQ.put("success", false);
            MQ.put("code", "999");
            MQ.put("msg", "改签占座失败");
            //查询订单
            long orderId = trainOrderChange.getOrderid();
            Trainorder trainOrder = Server.getInstance().getTrainService().findTrainorder(orderId);
            MQ.put("agentId", trainOrder.getAgentid());
            MQ.put("orderid", trainOrder.getQunarOrdernumber());
            MQ.put("transactionid", trainOrder.getOrdernumber());
            MQ.put("reqtoken", trainOrderChange.getRequestReqtoken());
            MQ.put("help_info", "改签失败");
            MQ.put("method", "train_request_change");
        }
        WriteLog.write("RabbitMQCreateChangeConsumerOrder_MQ", MQ.toString());
        //占座失败
        if (!MQ.getBooleanValue("success")) {
            //排队中
            if ("改签或变更到站已进入排队机制".equals(MQ.getString("msg"))) {
                return;
            }
            //改签状态
            C_TCSTATUS = Trainorderchange.FAILCHANGE;
            C_STATUS12306 = Trainorderchange.ORDERFALSE;
            updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                    + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = ";
            if (isneedmemcachedadd) {
                updateSql += Trainorderchange.APPLYROCESSING;
            }
            else {
                updateSql += Trainorderchange.APPLYCHANGE;
            }
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
            //非改签退
            if (changeType != 1) {
                //车票状态
                updateSql = "update T_TRAINTICKET set C_STATUS = " + Trainticket.ISSUED + " where C_CHANGEID = "
                        + changeId + " and C_STATUS = " + Trainticket.APPLYCHANGE;
                //更新车票
                Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
            }
            if (isneedmemcachedadd && !isUserInformationByInterface) {
                try {
                    //占座失败 去掉内存中订单
                    TrainChangeListenerMethod.remove(loginNameString);
                }
                catch (Exception e) {

                }
            }
        }
        //订单ID
        long orderId = trainOrderChange.getOrderid();
        //获取接口类型
        int interfacetype = interfacetypeByOrderid(orderId);
        WriteLog.write("RabbitMQCreateChangeOrder_interfacetype", orderId + ">>>" + interfacetype);
        //改签退
        if (changeType == 1) {
            //特殊回调
            callBackChangeRefund(MQ, trainOrderChange);
        }
        else if (interfacetype == TrainInterfaceMethod.TAOBAO) {
            WriteLog.write("TrainCreateChangeMessageListener_interfacetype", interfacetype + ">>>");
            MQ.put("apply_id", trainOrderChange.getTaobaoapplyid());
            MQ.put("refund_online", trainOrderChange.getIscanrefundonline());
            WriteLog.write("RabbitMQCreateChangeOrder_interfacetype_TAOBAO", MQ.toString());
            //回调淘宝
            callBackTaoBao(MQ, trainOrderChange);
        }
        else {
            //回调同程
            callBackTongCheng(MQ, trainOrderChange);
        }
    }

    /**
     * 改签退回调
     */
    private void callBackChangeRefund(JSONObject retobj, Trainorderchange trainOrderChange) {
        retobj.put("changeId", changeId);
        retobj.put("trainOrderId", trainOrderChange.getOrderid());
        //回调参数
        String jsonStr = "changeType=1&jsonStr=" + retobj;
        //回调地址
        String changeRefundCallBackUrl = trainOrderChange.getRequestCallBackUrl();
        //异步改签退回调
        String result = RequestUtil.post(changeRefundCallBackUrl, jsonStr, "UTF-8", new HashMap<String, String>(), 0);
        //回调成功
        boolean callbacktrue = "success".equalsIgnoreCase(result) ? true : false;
        //采购问题
        if (!callbacktrue) {
            String updateSql = "update T_TRAINORDERCHANGE set C_ISQUESTIONCHANGE = " + Trainorderchange.CAIGOUQUESTION
                    + " where ID = " + changeId;
            //占座成功
            if (retobj.getBooleanValue("success")) {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.THOUGHCHANGE;
            }
            else {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.FAILCHANGE;
            }
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        }
        //日志
        String changeResult = retobj.getBoolean("success") ? "成功" : "失败";
        String callbackResult = callbacktrue ? "成功" : "失败---" + result;
        writeRC(trainOrderChange.getOrderid(), "[改签 - " + changeId + "]---改签占座" + changeResult + "---回调"
                + callbackResult, "自动改签退", 0, 1);
    }

    private void callBackTaoBao(JSONObject retobj, Trainorderchange trainOrderChange) {
        //请求改签
        retobj.put("apply_id", trainOrderChange.getTaobaoapplyid());
        retobj.put("method", "train_request_change");
        retobj.put("changeorderid", trainOrderChange.getId());
        WriteLog.write("TrainCreateChangeMessageListener_callBackTaoBao", retobj.toString());
        String TaoBaoReqChangeCallBackUrl = PropertyUtil.getValue("TaoBao_Change_CallBack_Url", "train.properties");
        //回调
        String result = RequestUtil.post(TaoBaoReqChangeCallBackUrl, retobj.toString(), "UTF-8",
                new HashMap<String, String>(), 0);
        //回调成功
        boolean callbacktrue = "success".equalsIgnoreCase(result) ? true : false;
        //采购问题
        if (!callbacktrue) {
            String updateSql = "update T_TRAINORDERCHANGE set C_ISQUESTIONCHANGE = " + Trainorderchange.CAIGOUQUESTION
                    + " where ID = " + changeId;
            //占座成功
            if (retobj.getBooleanValue("success")) {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.THOUGHCHANGE;
            }
            else {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.FAILCHANGE;
            }
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        }
        //日志
        String changeResult = retobj.getBoolean("success") ? "成功" : "失败";
        String callbackResult = callbacktrue ? "成功" : "失败---" + result;
        writeRC(trainOrderChange.getOrderid(), "[改签 - " + changeId + "]---改签占座" + changeResult + "---回调"
                + callbackResult, "系统", 0, 1);

    }

    private void callBackTongCheng(JSONObject retobj, Trainorderchange trainOrderChange) {
        String tcTrainCallBack = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
        //回调
        String result = RequestUtil.post(tcTrainCallBack, retobj.toString(), "UTF-8", new HashMap<String, String>(), 0);
        //回调成功
        boolean callbacktrue = "success".equalsIgnoreCase(result) ? true : false;
        //采购问题
        if (!callbacktrue) {
            String updateSql = "update T_TRAINORDERCHANGE set C_ISQUESTIONCHANGE = " + Trainorderchange.CAIGOUQUESTION
                    + " where ID = " + changeId;
            //占座成功
            if (retobj.getBooleanValue("success")) {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.THOUGHCHANGE;
            }
            else {
                updateSql += " and C_TCSTATUS = " + Trainorderchange.FAILCHANGE;
            }
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        }
        //日志
        String changeResult = retobj.getBoolean("success") ? "成功" : "失败";
        String callbackResult = callbacktrue ? "成功" : "失败---" + result;
        writeRC(trainOrderChange.getOrderid(), "[改签 - " + changeId + "]---改签占座" + changeResult + "---回调"
                + callbackResult, "系统", 0, 1);
    }
    
    /**
     * 
     * @author RRRRRR
     * @time 2016年11月16日 下午2:55:38
     * @Description 改签占座重新扔进队列
     * @param changeId
     */
    public void sendMessageRabbitMQ(long changeId){
            String typeMsg = "";
            String QUEUE_NAME = "";
            //占座
            typeMsg = "占座";
            QUEUE_NAME = PropertyUtil.getValue("QueueMQ_TrainChange_WaitOrder", "rabbitMQ.properties");
            try {
                RabbitMQUtil.sendOnemessage(changeId, QUEUE_NAME);
            }
            catch (Exception e) {
                WriteLog.write(QUEUE_NAME, e.getMessage() + "---" + ElongHotelInterfaceUtil.errormsg(e));
            }
            finally {
                System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "RabbitMQ-->改签" + typeMsg + "-->" + changeId);
            }
    }

    @SuppressWarnings("rawtypes")
    public String loginnameByOrderid(Long orderid) {
        String loginname = "";
        String sql = "SELECT ISNULL(C_SUPPLYACCOUNT, '') C_SUPPLYACCOUNT,ISNULL(ordertype, 1) ordertype "
                + "FROM T_TRAINORDER WITH (NOLOCK) WHERE ID=" + orderid;
        try {
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            Map map = (Map) list.get(0);
            String ordertype = map.get("ordertype").toString();
            if (ordertype != null && ("3".equals(ordertype) || "4".equals(ordertype))) {
                return "UserInformationByInterface";
            }
            else {
                String sqlResultString = map.get("C_SUPPLYACCOUNT").toString();
                loginname = sqlResultString.split("/")[0];
            }
        }
        catch (Exception e) {

        }
        return loginname;
    }

    /**
     * 获取发车时间
     * @param orderid
     * @return
     */
    @SuppressWarnings("rawtypes")
    public long deptimeByOrderid(Long orderid) {
        long deptime = System.currentTimeMillis() + 10000l;
        String sql = "SELECT C_DEPARTTIME FROM T_TRAINTICKET with (nolock) where C_TRAINPID =(select top 1 ID  from T_TRAINPASSENGER where C_ORDERID="
                + orderid + " )";
        try {
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            Map map = (Map) list.get(0);
            String deptimeString = map.get("C_DEPARTTIME").toString();
            //2015-04-28 15:30
            Date date = stringToDate(deptimeString, "yyyy-MM-dd HH:mm"); // String类型转成date类型
            if (date != null) {
                deptime = date.getTime(); // date类型转成long类型
            }
        }
        catch (Exception e) {

        }
        return deptime;
    }

    /**
     * 获取发车时间
     */
    public long createtime(Trainorderchange trainorderchange) {
        long tccreatetime = System.currentTimeMillis();
        try {
            //2015-05-27 16:04:06.000
            Date date = stringToDate(trainorderchange.getTccreatetime(), "yyyy-MM-dd HH:mm:ss.SSS"); // String类型转成date类型
            if (date != null) {
                tccreatetime = date.getTime(); // date类型转成long类型
            }
        }
        catch (Exception e) {

        }
        return tccreatetime;
    }

    /**
     * @param strTime
     * @param formatType
     * @return
     * @throws ParseException
     */
    public Date stringToDate(String strTime, String formatType) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(formatType);
        Date date = null;
        date = formatter.parse(strTime);
        return date;
    }

    /**
     * 是否需要走MemCached
     * @param trainorderchange
     * @return
     */
    private boolean isNeedMemCachedAdd(Trainorderchange trainorderchange) {
        long deptime = deptimeByOrderid(trainorderchange.getOrderid());
        long tccreatetime = createtime(trainorderchange);
        long nowtime = System.currentTimeMillis();
        //距发车时间大于160分钟   当前时间-改签单创建时间小于30分钟  才可以走
        if ((deptime - nowtime) > (1000 * 60 * 160) && (nowtime - tccreatetime) < (30 * 1000 * 60)) {
            return true;
        }
        return false;
    }
    
    /**
     * 书写操作记录
     * @param trainorderid
     * @param content
     * @param createurser
     * @param status
     * @param ywtype
     * @time 2015年1月21日 下午7:05:04
     * @author fiend
     */
    public void writeRC(long trainorderid, String content, String createurser, int status, int ywtype) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createurser);
        rc.setYwtype(ywtype);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }
    
    /**
     * 通过订单ID查询接口类型
     * @param orderid
     * @return
     */
    @SuppressWarnings("rawtypes")
    public int interfacetypeByOrderid(long orderid) {
        int interfacetype = 0;
        try {
            String sql = "SELECT ISNULL(C_INTERFACETYPE, 0) C_INTERFACETYPE "
                    + "FROM T_TRAINORDER WITH (NOLOCK) WHERE ID = " + orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            //唯一
            if (list != null && list.size() == 1) {
                Map map = (Map) list.get(0);
                interfacetype = Integer.valueOf(map.get("C_INTERFACETYPE").toString());
            }
            //失败
            if (interfacetype == 0) {
                interfacetype = Server.getInstance().getInterfaceTypeService().getTrainInterfaceType(orderid);
            }
        }
        catch (NumberFormatException e) {
        }
        return interfacetype;
    }


}
