package com.ccservice.inter.rabbitmq;

import java.io.IOException;

//import javax.jms.Connection;
//import javax.jms.ConnectionFactory;
//import javax.jms.Destination;
//import javax.jms.MessageConsumer;
//import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

import com.ccservice.Util.file.WriteLog;
//import org.apache.activemq.ActiveMQConnectionFactory;
//import org.apache.activemq.command.ActiveMQQueue;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.rabbitmq.util.PropertyUtil;
import com.ccservice.train.mqlistener.TrainorderChangePayMessageListener;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
//import com.ccservice.train.mqlistener.TrainorderChangePayMessageListener;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 
 * 审核改签
 * @author RRRRRR
 * @time 2016年11月18日 下午2:00:48
 */
public class RabbitMQ_TrainRequestChangePayExamine extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        super.init();
        System.out.println("改签审核队列RabbitMQ-----开启");
        payExamine();
    }

    int threadNum = 0;

    private void payExamine() {
        System.out.println("改签审核消费者队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        String queueName = PropertyUtil.getValue("QueueMQ_TrainChange_PayExamine", "rabbitMQ.properties");//下单消费者在mq队列里的名字   "RabbitWaitOrder"线上叫这个
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.CHANGEPAYEXAMINE);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110
                + ":PublicConnectionInfos.CHANGEPAYEXAMINE:" + queueName + ":pingtaiType:" + pingtaiType
                + ":consumerType:" + consumerType);
        System.out.println("改签审核消费者队列【" + PublicConnectionInfos.CHANGEPAYEXAMINE + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.CHANGEPAYEXAMINE,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {

                    new RabbitQueueConsumer(PublicConnectionInfos.CHANGEPAYEXAMINE, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitMQ_TrainRequestChangePayExamine_log") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            long changeId = Long.valueOf(notice);
                            System.out.println("Rabbit改签支付审核  接收:" + notice);
                            //支付审核操作
                            if (changeId > 0) {
                                new TrainorderChangePayMessageListener().RepOperateV2(changeId);
                            }
                            return notice + "--->改签审核";
                        }
                    }.start();

                }

            }
        }.start();
        //监控结束

    }

    private void payExamineBack() {
        String QueueMQ_TrainChange_PayExamine = PropertyUtil.getValue("QueueMQ_TrainChange_PayExamine",
                "rabbitMQ.properties");
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                new RabbitQueueConsumerPayExamineChangeV3(QueueMQ_TrainChange_PayExamine, type).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
