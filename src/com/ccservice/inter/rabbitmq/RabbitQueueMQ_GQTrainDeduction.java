package com.ccservice.inter.rabbitmq;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.train.qunar.GqTrainorderDeduction;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 
 * 改签扣款
 * @author 之至
 * @time 2016年11月19日 下午5:10:44
 */
public class RabbitQueueMQ_GQTrainDeduction extends HttpServlet {
    private static final long serialVersionUID = 1L;

    int threadNum = 0;

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("改签扣款mq:开启");
        orderNotice();
    }

    private void orderNotice() {
        System.out.println("下单占座队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        String queueName = PropertyUtil.getValue("QueueMQ_TrainorderDeductionGq", "rabbitMQ.properties");//下单消费者在mq队列里的名字   "RabbitWaitOrder"线上叫这个
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(queueName);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:" + queueName
                + ":pingtaiType:" + pingtaiType + ":consumerType:" + consumerType);
        System.out.println("下单占座队列【" + queueName + "】【consumerType:" + consumerType + "】【kt110:" + kt110
                + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束

        /* 老的启动消费者的方式 ,弃用改为新的方式，通过监控的方式开关消费者
        MonitoringSupply monitoringSupply = new MonitoringSupply();
        RabbitmqBean rabbitmqBean = getRabbitmqBean();//获取当前使用的哪个mq服务的对象
        System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====rabbitmqBean=" + rabbitmqBean);
        if (rabbitmqBean != null) {
            System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====平台=" + rabbitmqBean.toString());
            System.out.println("下单占座队列【" + queueName + "】RabbitMQ-----====准备启动" + threadNum + "个消费者 ");
            for (int i = 0; i < threadNum; i++) {//启动多少个消费者
                //new RabbitQueueConsumerCreateOrderV3(name, type).start();//老的消费实际方法实现
                //新的消费者
                monitoringSupply.newRabbitQueueConsumerCreateOrderV3ByHost(queueName, pingtaiType, rabbitmqBean);//使用host启动的消费者
            }
        }
        else {
            System.out.println("下单占座队列获取MQ信息失败");
        }
        */

        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.QUEUEMQ_TRAINORDERDEDUCTIONGQ,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {

                    new RabbitQueueConsumer(PublicConnectionInfos.QUEUEMQ_TRAINORDERDEDUCTIONGQ, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitQueueMQ_GQTrainDeduction_log") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            JSONObject jsonobject = JSONObject.parseObject(notice);
                            long orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
                            long changeid = jsonobject.containsKey("changeid") ? jsonobject.getLongValue("changeid")
                                    : 0;
                            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
                            new GqTrainorderDeduction(orderid, type, changeid).tongchengDeduction();
                            return notice + "--->改签扣款";
                        }
                    }.start();

                }
            }
        }.start();
        //监控结束
    }

    public void orderNoticeback() {
        String queryName = PropertyUtil.getValue("QueueMQ_TrainorderDeductionGq", "rabbitMQ.properties");
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                new RabbitQueueConsumerGQTrainDeductionV3(queryName, type).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
