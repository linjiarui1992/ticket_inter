package com.ccservice.inter.rabbitmq;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrderPaiDui;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.rabbitmq.util.PropertyUtil;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 
 * 下单排队订单排队
 * @author RRRRRR
 * @time 2016年11月18日 下午8:54:21
 */
public class RabbitMQ_TrainOrderWaitLine extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        super.init();
        System.out.println("订单排队队列RabbitMQ-----开启");
        changeNotice();
    }

    int threadNum = 0;

    private void changeNotice() {
        System.out.println("下单排队队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        String queueName = PropertyUtil.getValue("QueueMQ_trainorder_waitorder_orderid_PaiDui", "rabbitMQ.properties");//
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos
                .getConsumerType(PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者[下单排队业务类型是2]
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI + ":pingtaiType:" + pingtaiType
                + ":consumerType:" + consumerType);
        System.out.println("下单排队队列【" + PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI
                + "】【consumerType:" + consumerType + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType
                + "【空铁1,同程2】 ");
        //初始化参数结束

        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI,
                            pingtaiString, rabbitmqBean.getHost(), rabbitmqBean.getUsername(),
                            rabbitmqBean.getPassword(), "RabbitMQ_TrainOrderWaitLine_log") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            System.out.println(TimeUtil.gettodaydate(5) + "开始处理消息" + notice);
                            if (!"0".equals(notice)) {
                                String jsonData = notice;
                                JSONObject json = JSONObject.parseObject(jsonData); //字符串转为JSONObject
                                long trainorderid = json.getLongValue("trainorderid"); //解析JSON对象获取订单id
                                boolean isLastPaidui = json.getBooleanValue("isLastPaidui");//解析JSONobject获取是否排队
                                TrainCreateOrderPaiDui trainCreateOrderPaiDui = new TrainCreateOrderPaiDui(trainorderid);
                                trainCreateOrderPaiDui.startPaiduiResult(isLastPaidui);
                            }
                            WriteLog.write("RabbitMQ_TrainOrderWaitLine_log", System.currentTimeMillis() + ":"
                                    + " 确认消息 :changeNoticeResult:" + notice);

                            return notice + "--->下单排队";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }

    /**
    * 
    * @author RRRRRR
    * @time 2016年11月16日 下午2:42:12
    * @Description 订单排队消费实现
    */
    private void changeNotice_back() {
        String QueueMQ_trainorder_waitorder_orderid_PaiDui = PropertyUtil.getValue(
                "QueueMQ_trainorder_waitorder_orderid_PaiDui", "rabbitMQ.properties"); // 获取排队占座队列名称
        //        RabbitQueueConsumerWaitLineV3 consumer = null;
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                new RabbitQueueConsumerWaitLineV3(QueueMQ_trainorder_waitorder_orderid_PaiDui, type).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

}
