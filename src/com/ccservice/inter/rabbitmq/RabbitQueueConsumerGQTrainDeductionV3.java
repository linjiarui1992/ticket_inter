package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.qunar.util.ExceptionUtil;
//import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.ccservice.train.qunar.GqTrainorderDeduction;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 改签扣款·真
 * 
 * @author 之至
 * @time 2016年12月28日 上午10:00:22
 */
public class RabbitQueueConsumerGQTrainDeductionV3  extends RabbitMQDefaultCustomer implements IConsumer{//extends RabbitMQCustomer{

    private long orderid;

    private long changeid;
    
    private String queueNameString;
    
    private int type;

    private int typeRabbit;

    //消息内容
    private String orderNoticeResult = "";

    public RabbitQueueConsumerGQTrainDeductionV3(String queueNameString, int typeRabbit) throws IOException {
//        super(queueNameString, typeRabbit);
//        this.queueNameString = queueNameString;
//        this.typeRabbit = typeRabbit;
    }
    
    /**
    * 改签扣款
    * @param name
    * @param pingtaiType
    * @param host
    * @param username
    * @param password
    * @throws IOException
    */
   public RabbitQueueConsumerGQTrainDeductionV3(String name, String pingtaiType, String host, String username, String password) throws IOException {
       super(name, pingtaiType, host, username, password);
   }
   public long tempTime;
   public String logName = this.getClass().getSimpleName();
   @Override
   public void run() {
       
       UUID uuid = UUID.randomUUID(); 
       CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
       this.logName = this.getClass().getSimpleName();
       try {
           initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
       }
       catch (IOException e1) { 
           e1.printStackTrace();
       }
        while (true) {//循环获取消息
            if (!super.connection.isOpen()) {//如果连接已经关闭了就关闭
                willClose = true;
                WriteLog.write(this.logName + "-Exception", "连接关了");
            }
            else {
                this.tempTime = System.currentTimeMillis();
                if (!isCanOrdering(new Date())) {//是否可以下单 
                    try {
                        sleep(1000L);//如果不能下单休息1秒
                    }
                    catch (Exception e) {
                    }
                    continue;
                }
                String msg = "";
                //           String orderNoticeResult="";
                try {
                    msg = getMsgNew();//【第四步】:获取消息
                    orderNoticeResult = msg;//消息内容
                    System.out.println(TimeUtil.gettodaydate(5) + ":接收到消息:" + orderNoticeResult);
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + orderNoticeResult + "】");
                    willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + willClose + "】");
                    AckMsg();//【第五步】:确认消息，已经收到
                }
                catch (ShutdownSignalException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (ConsumerCancelledException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
   }
   
//    @Override
    public void runBack() {
        while (true) {
            try {
                orderNoticeResult = getNewMessageString(queueNameString);
                System.out.println(TimeUtil.gettodaydate(5)+"改签扣款  接收 ：————————>"+orderNoticeResult);
                WriteLog.write("RabbitTrainorderDeductiongq", orderNoticeResult);
                GQDeductionMethod(orderNoticeResult);
            }
            catch (ShutdownSignalException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "");
            }
            catch (ConsumerCancelledException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "");
            }
            catch (IOException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "");
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "");
            }catch (Exception e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "");
            }
            finally {
                //确认消息已经收到.必须
                try {
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                }
                catch (IOException e) {
                    ExceptionUtil.writelogByException("RabbitTrainorderDeductiongq_Exception", e, "确认消息异常");
                }
            }
        }
    }
    
    @Override
    public boolean processTheMessage(String msgInfo) { 
        boolean willClose = false;//是否即将关闭
        try {
            GQDeductionMethod(msgInfo);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }

    private void GQDeductionMethod(String msgInfo) {
        JSONObject jsonobject = JSONObject.parseObject(msgInfo);
        this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
        this.changeid = jsonobject.containsKey("changeid") ? jsonobject.getLongValue("changeid") : 0;
        this.type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
        new GqTrainorderDeduction(this.orderid, type, changeid).tongchengDeduction();
    }
}
