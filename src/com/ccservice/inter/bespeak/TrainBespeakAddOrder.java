package com.ccservice.inter.bespeak;

/**
 * 抢票转线下-下单
 * guozhengju
 * 2016-09-18
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;

public class TrainBespeakAddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TrainBespeakAddOrder() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("content-type", "text/html;charset=UTF-8");
		String jsonStr = request.getParameter("jsonStr");
		JSONObject result = new JSONObject();
		PrintWriter out = null;
		WriteLog.write("抢票转线下-下单", "param：" + jsonStr);
		try {
			if (jsonStr != null && !"".equals(jsonStr)) {
				out = response.getWriter();
				JSONObject json = JSONObject.parseObject(jsonStr);
				if (json == null || json.isEmpty()) {
					result.put("success", false);
					result.put("msg", "参数为空");
				} else {
					JSONArray datas = json.getJSONArray("data");
					WriteLog.write("抢票转线下-下单", "data：" + datas);
					if (datas == null || datas.size() <= 0) {
						result.put("success", false);
						result.put("msg", "参数错误");
					} else {
						for (int i = 0; i < datas.size(); i++) {
							// 入库
							new MyThreadBespeakAddOrder(datas.getJSONObject(i)
									.toString()).start();
						}
						result.put("success", true);
						result.put("msg", "");
					}
				}
			} else {
				result.put("success", false);
				result.put("msg", "参数为空");
			}
		} catch (Exception e) {
			result.put("success", false);
			result.put("msg", "系统错误");
		} finally {
			if (out != null) {
				out.print(result.toString());
				out.flush();
				out.close();
			}
		}
	}

}
