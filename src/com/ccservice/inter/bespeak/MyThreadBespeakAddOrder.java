package com.ccservice.inter.bespeak;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 抢票分线下入库
 * @author guozhengju
 * 2016-09-18
 */
public class MyThreadBespeakAddOrder extends Thread{

    private String arrStationValue, dptStationValue, extSeatValue, seatValue_Key, orderNoValue, certNoValue,
    certTypeValue, nameValue, seatValue, trainEndTimeValue, trainNoValue, trainStartTimeValue,
    arrStationValue_lc, dptStationValue_lc, extSeatValue_lc, seatValue_lc, trainEndTimeValue_lc,
    trainNoValue_lc, trainStartTimeValue_lc, birthday;

    private float ticketPayValue, seatValue_Value = 0;

    private int ticketTypeValue, seqValue;
    
    private DBoperationUtil dt = new DBoperationUtil();
    
    private String jsonStr;
    
    public MyThreadBespeakAddOrder(String jsonStr){
        this.jsonStr=jsonStr;
    }
    
    @Override
    public void run() {
        JSONObject info=JSONObject.parseObject(this.jsonStr);
        com.ccservice.b2b2c.atom.component.WriteLog.write("抢票_线下_JSON", "json:"+this.jsonStr);
        String orderType = info.getString("orderType");// 订单类型 0抢票未出票/1抢票成功
        String arrStationValue = info.getString("arrStation");// 终点站
        String dptStationValue = info.getString("dptStation");// 始发站
        String extSeatValue = info.getString("extSeatMap");// 备选车票
        String extSeatString = getAllExtSeatMap(extSeatValue);//新修改逻辑Map
        String paperType = info.getString("paperType");
        String paperBackup = info.getString("paperBackup");
        String paperLowSeatCount = info.getString("paperLowSeatCount");
        String orderNoValue=info.getString("orderNo");
        String sql1="select * from TrainOrderOffline where OrderNumberOnline='"+orderNoValue+"'";
        List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (list.size()==0) {// 如果未存库
            String seatValue = info.getString("seatMap");// 坐席及其价格
            // 0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
            // TODO 普通订单的坐席类型
            JSONObject seatObject = JSONObject.parseObject(seatValue);
            Set<String> seatSet = seatObject.keySet();

            for (String str : seatSet) {
                 seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
                 seatValue_Key = str;// 得到坐席
            }
            ticketPayValue = (float) info.getDoubleValue("ticketPay");// 应付订单票款
            trainNoValue = info.getString("trainNo");// 火车编号
            String costTime = "";
            trainStartTimeValue = info.getString("trainStartTime");// 火车出发时间
            
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
            Date date_trainStartTime = new Date();
            try {
                date_trainStartTime = df.parse(trainStartTimeValue);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
           if("1".equals(orderType)){//抢到票
               trainEndTimeValue = info.getString("trainEndTime");// 火车到达时间
               Date date_trainEndTime = new Date();
               
               try {
                   date_trainEndTime = df.parse(trainEndTimeValue);
               }
               catch (ParseException e) {
                   e.printStackTrace();
               }
               long endMilliSeconds = date_trainEndTime.getTime();
               DateFormat dfm = new SimpleDateFormat("HH:mm");
               trainEndTimeValue = dfm.format(date_trainEndTime);
               
               
               long startMilliSeconds = date_trainStartTime.getTime();
               int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
               costTime = String.valueOf(costtime / (60 * 60)) + ":" + String.valueOf((costtime % (60 * 60)) / 60);// 历时
           }
            JSONArray passengers = info.getJSONArray("passengers");
            Trainorder trainorder = new Trainorder();// 创建订单
            trainorder.setTaobaosendid(extSeatString);
            trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
            trainorder.setQunarOrdernumber(orderNoValue);
            List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
            for (int i = 0; i < passengers.size(); i++) {
                List<Trainticket> traintickets = new ArrayList<Trainticket>();
                JSONObject infopassengers = passengers.getJSONObject(i);
                certNoValue = infopassengers.getString("certNo");// 乘车人证件号码
                // 1：身份证；C：港澳通行证；G：台湾通行证；B：护照
                certTypeValue = infopassengers.getString("certType");// 乘车人证件种类
                if ("B".equals(certTypeValue)) {
                    certTypeValue = "3";
                }
                else if ("C".equals(certTypeValue)) {
                    certTypeValue = "4";
                }
                else if ("G".equals(certTypeValue)) {
                    certTypeValue = "5";
                }
                if (certNoValue.length() == 18) {
                    String year = certNoValue.substring(6, 10);
                    String month = certNoValue.substring(10, 12);
                    String day = certNoValue.substring(12, 14);
                    birthday = year + "-" + month + "-" + day;
                }
                nameValue = infopassengers.getString("name");// 乘车人姓名
                if (infopassengers.containsKey("ticketType")) {// 票种类型，1：成人票；0：儿童票 2:学生票(如果是去哪儿的老版本，没有ticketType)
                    ticketTypeValue = Integer.parseInt(infopassengers.getString("ticketType"));
                }
                else {
                    ticketTypeValue = 1;
                }
                // TODO 普通订单存入数据库
                Trainticket trainticket = new Trainticket();
                Trainpassenger trainpassenger = new Trainpassenger();
                trainticket.setArrival(arrStationValue);// 存入终点站
                trainticket.setDeparture(dptStationValue);// 存入始发站
                trainticket.setStatus(Trainticket.WAITISSUE);
                trainticket.setTicketno("0");
                // 无法存入剩余票
                trainticket.setSeq(0);// 存入订单类型
                trainticket.setTickettype(ticketTypeValue);// 存入票种类型
                trainticket.setPayprice(seatValue_Value);
                
                trainticket.setArrivaltime(trainEndTimeValue);// 火车到达时间
                if("1".equals(orderType)){
                    trainticket.setTrainno(infopassengers.getString("trainNum")); // 火车编号
                    trainticket.setSeattype(infopassengers.getString("seatType"));// 车票坐席
                    trainticket.setPrice(Float.parseFloat(infopassengers.getString("price")));// 应付火车票价格
                    trainticket.setCoach(infopassengers.getString("coach"));
                    trainticket.setSeatno(infopassengers.getString("seatNo"));
                    trainticket.setTicketno(infopassengers.getString("ticketNo"));
                }else{
                    trainticket.setTrainno(infopassengers.getString("trainNum")); // 火车编号
                    trainticket.setSeattype(infopassengers.getString("seatType"));// 车票坐席
                    trainticket.setPrice(0f);// 应付火车票价格
                }
                trainticket.setDeparttime(trainStartTimeValue);// 火车出发时间
                trainticket.setIsapplyticket(1);
                trainticket.setRefundfailreason(0);
                trainticket.setCosttime(costTime);// 历时
                trainticket.setInsurenum(0);
                trainticket.setInsurprice(0f);// 采购支付价
                trainticket.setInsurorigprice(0f);// 保险原价
                traintickets.add(trainticket);
                trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
                trainpassenger.setIdtype(Integer.parseInt(certTypeValue));// 存入乘车人证件种类
                trainpassenger.setName(nameValue);// 存入乘车人姓名
                trainpassenger.setBirthday(birthday);
                trainpassenger.setChangeid(0);
                trainpassenger.setAduitstatus(1);
                trainpassenger.setTraintickets(traintickets);
                trainplist.add(trainpassenger);
            }
            //纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
            trainorder.setPaymethod(Integer.parseInt(paperType));
            //当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
            trainorder.setSupplypayway(Integer.parseInt(paperBackup));
            //至少接受下铺/靠窗/连坐数量
            trainorder.setRefuseaffirm(Integer.parseInt(paperLowSeatCount));
            trainorder.setContactuser(info.getString("transportName"));
            trainorder.setContacttel(info.getString("transportPhone"));
            trainorder.setTicketcount(trainplist.size());
            trainorder.setCreateuid(68l);
            trainorder.setInsureadreess(info.getString("transportAddress"));

            trainorder.setOrderprice(ticketPayValue);
            trainorder.setIsjointtrip(0);
            trainorder.setAgentid(2);//---------------------------------------
            trainorder.setState12306(Trainorder.WAITORDER);
            trainorder.setPassengers(trainplist);
            trainorder.setOrderstatus(Trainorder.WAITISSUE);
            trainorder.setAgentprofit(0f);
            trainorder.setCommission(0f);
            trainorder.setTcprocedure(0f);
            trainorder.setInterfacetype(2);
            trainorder.setCreateuser("抢票_线下");
            try {
                WriteLog.write("qunartrainorder", trainorder.getQunarOrdernumber() + "(:)" + trainorder.getId());
                if (trainorder.getOrdertype() == 2) {// 去哪儿的线下订单去线下订单下单
                    WriteLog.write("抢票_线下", "线下");
                    trainorderofflineadd(trainorder,orderType);

                }
                else {// 如果是线上订单的话才扔MQ里去下单
                    WriteLog.write("抢票_线下", "线上");
                }
            }
            catch (Exception e) {
                WriteLog.write("MyThreadQunarOrder_LadanException_Error", info.toString());
                ExceptionUtil.writelogByException("MyThreadQunarOrder_LadanException_Error", e);
            }
        }
        
    
    }
    /**
     * 添加火车票线下订单
     */
    public void trainorderofflineadd(Trainorder trainorder,String orderType) {
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        String addresstemp = trainorder.getInsureadreess();
        // 通过订单邮寄地址匹配出票点
        String agentidtemp = distribution2(addresstemp);
        WriteLog.write("线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());
        SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
        Timestamp startTime = new Timestamp(new Date().getTime());
        trainOrderOffline.setOrderTimeOut(startTime);
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        //通过出票点和邮寄地址获取预计到达时间results
        String delieveStr=getDelieveStr(agentidtemp,trainorder.getInsureadreess());//
        String updatesql="UPDATE TrainOrderOffline SET expressDeliver ='"+delieveStr+"' WHERE ID="+orderid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime() + ":00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                trainTicketOffline.setSubOrderId(ticket.getTicketno());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                List listT = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
                if("1".equals(orderType)){
                    Map map3 = (Map) listT.get(0);
                    String trainTid = map3.get("id").toString();
                    String sqlup="update TrainTicketOffline set sealPrice="+ticket.getPrice()+",realSeat='"+ticket.getSeattype()+"' where Id="+trainTid;
                    WriteLog.write("抢票_线下修改真实出票坐席", "sqlup:"+sqlup);
                    Server.getInstance().getSystemService().excuteAdvertisementBySql(sqlup);
                }
//                else {
//                	Map map3 = (Map) listT.get(0);
//                    String trainTid = map3.get("id").toString();
//                    String sqlup="insert into TrainTicketOffline(sealPrice,realSeat) values('"+ticket.getPrice()+"','"+ticket.getSeattype()+"') where Id="+trainTid;
//                    WriteLog.write("抢票_线下修改真实出票坐席", "sqlup:"+sqlup);
//                    Server.getInstance().getSystemService().excuteAdvertisementBySql(sqlup);
//				}
                
            }
        }
    }
    public String getDelieveStr(String agengId,String address){
        String results="";
        String fromcode="010";
        String tocode=getExpressCodes(address);
        String time1="10:00:00";
        String time2="18:00:00";
        
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        String dates=sdf.format(new Date());
        String sql1="SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId="+agengId;
        List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if(list.size()>0){
            Map map=(Map)list.get(0);
            fromcode=map.get("fromcode").toString();
            time1=map.get("time1").toString();
            time2=map.get("time2").toString();
        }
        String realTime=getRealTimes(dates,time1,time2);
        String urlString=PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param="times="+realTime+"&fromcode="+fromcode+"&tocode="+tocode;
         WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->"+urlString+"?"+param);
        String result=SendPostandGet.submitPost(urlString, param,"UTF-8").toString();
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息","address="+address+"---------->result:"+result);
        try {
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if("OK".equals(root.elementText("Head"))){
                if(result.contains("deliver_time")){
                    Element deliverTmResponse = body.element("DeliverTmResponse");
                    Element deliverTm = deliverTmResponse.element("DeliverTm");
                    String business_type_desc=deliverTm.attributeValue("business_type_desc");
                    String deliver_time=deliverTm.attributeValue("deliver_time");
                    String business_type=deliverTm.attributeValue("business_type");
                    results="如果"+realTime+"正常发件。快递类型为:"+business_type_desc+"。快递预计到达时间:"+deliver_time+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                }else{
                    results="获取快递时间失败！请上官网核验快递送达时间。";
                }
            }
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
          
        return results;
    }
    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }
    public Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }
    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public String getExpressCodes(String address){
        String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        String cityCode="010";
        if(list.size()>0){
            Map map=(Map)list.get(0);
            cityCode=map.get("CityCode").toString();
        }
        return cityCode;
    }
    /**
     * 根据数据库设置匹配出票点
     * @param address1
     * @return
     */
    public String distribution2(String address1) {
        boolean flag=false;
        //默认出票点
        String sql1="SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+"52";
        List list1=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        String agentId="378";
        if(list1.size()>0){
            Map map=(Map)list1.get(0);
            agentId=map.get("agentId").toString();
        }
        //程序自动分配出票点
        String sql2="SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid="+"52";
        List list2=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        for(int i=0;i<list2.size();i++){
            Map mapp=(Map)list2.get(i);
            String provinces=mapp.get("provinces").toString();
            String agentid=mapp.get("agentId").toString();
            String[] add=provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                if (address1.startsWith(add[j])) {
                    agentId=agentid;
                    flag=true;
                }
                if(flag){
                    break;
                }
            }
            if(flag){
                break;
            }
        }
        WriteLog.write("去哪qclt新版分配订单", "agentId="+agentId+";address1="+address1);
        return agentId;
    }
    /**
     * 去哪修改坐席类型
     * @param extSeatValueMap
     * @return
     */
    public String getAllExtSeatMap(String extSeatValue) {
        String extString = "";
        JSONArray array = JSONArray.parseArray(extSeatValue);
        for (int i = 0; i < array.size(); i++) {
            JSONObject seatObject = array.getJSONObject(i);
            Set<String> seatSet = seatObject.keySet();
            for (String str : seatSet) {
                float seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
                extString+=str+":￥" + seatValue_Value + ",";
            }

        }
        if (extString.contains(",")) {
            extString = extString.substring(0, extString.length() - 1);
        }
        else {
            extString = "无";
        }
        return extString;

    }

}
