package com.ccservice.inter.bespeak;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

/**
 * 获取京东、顺丰快递实效
 * @author guozhengju
 * 2016-09-18
 */
public class TrainBespeakGetDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TrainBespeakGetDelivery() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    response.setHeader("content-type", "text/html;charset=UTF-8");
	    String address=request.getParameter("address");
	    String timeStamp=request.getParameter("timeStamp");
        String partnerName=request.getParameter("partnerName");
        String messageIdentity=request.getParameter("messageIdentity");
	    WriteLog.write("抢票分线下快递时效接口", "address:"+address+";timeStamp:"+timeStamp+";partnerName:"+partnerName+";messageIdentity:"+messageIdentity);
        String sql = "SELECT keys from TrainOfflineAgentKey where partnerName='"+partnerName+"'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String key="";
        String code="";
        String msg="";
        String deliverDescription="";
        String deliverDescription1="";
        if(list.size()>0){
            Map map=(Map)list.get(0);
            key=map.get("keys").toString();
            String md5s=MD5Util.MD5Encode(partnerName+timeStamp+key, "UTF-8").toUpperCase();
            if(messageIdentity.equals(md5s)){//验证通过
                String agentid=distribution2(address);
                try {
                    deliverDescription=getDelieveStr(agentid,address);
                    deliverDescription1=getJDExpressDelivery(agentid,address);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                WriteLog.write("抢票分线下快递时效接口", "agentid:"+agentid+";Address:"+address+"--->返回信息="+deliverDescription+"<--->"+deliverDescription1);
                code="true";
                msg="";
            }else{
                code="false";
                msg="账号核验失败！";
            }
        }else{
            code="false";
            msg="账号核验失败！";
        }
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        responsejson.put("sf", deliverDescription);
        responsejson.put("jd", deliverDescription1);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
	}
	/**
	 * 获取京东快递实效
	 */
	public String getJDExpressDelivery(String agentId,String address){
		String cityName = getCityByAgentId(agentId);
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	    String url="http://120.26.100.206:12345/JD/jdisacept";//正式jdisacept  jdgetkey
	    int max=10000;
        int min=1000;
        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
	    String orderid="T"+sdf.format(new Date())+""+s;
	    String sendCode = getProvinceCode(cityName.split("_")[0], cityName.split("_")[1]);
	    String senderprovinceid = "1";
	    String sendercityid = "72";
	    if (sendCode != null && !"".equals(sendCode)) {
            senderprovinceid = sendCode.split("_")[0];
            sendercityid = sendCode.split("_")[1];
           
        }
        String param="address="+address+"&orderid="+orderid+"&senderprovinceid="+senderprovinceid+"&sendercityid="+sendercityid;
        WriteLog.write("抢票转线下获取jd实效", "address:"+address+";orderid:"+orderid+";senderprovinceid:"+senderprovinceid+";sendercityid:"+sendercityid);
        String resultjson=SendPostandGet.submitPost(url, param, "UTF-8").toString();
        WriteLog.write("抢票转线下获取jd实效", "address:"+address+";orderid:"+orderid+";senderprovinceid:"+senderprovinceid+";sendercityid:"+sendercityid+";resultjson:"+resultjson);
        JSONObject json=JSONObject.parseObject(resultjson);
        String responses=json.getString("jingdong_etms_range_check_responce");
        JSONObject jsonres=JSONObject.parseObject(responses);
        String resultInfo=jsonres.getString("resultInfo");
        JSONObject jsonone=JSONObject.parseObject(resultInfo);
        String rcode=jsonone.getString("rcode");
        String result="";
        if("100".equals(rcode)){
            String agingName=jsonone.getString("agingName");
            String aging=jsonone.getString("aging");
            if(!"0".equals(aging)){
                SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
                result=sdf1.format(getDateAfter(new Date(),Integer.parseInt(aging)-1))+" 18:00";
            }
        }
        return result;
	}

    public Date getDateAfter(Date d, int day) {  
        Calendar now = Calendar.getInstance();  
        now.setTime(d);  
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);  
        return now.getTime();  
    }
	
	/**
	 * jd获取省市code
	 * @param province
	 * @param city
	 * @param random
	 * @return
	 */
	
	public String getProvinceCode(String province, String city) {
        String provinces = province.replace("省", "").replace("市", "");
        String privinceCode = "";
        String cityCode = "";
        String parnetid = "";
        String sendCode = "";
        String psql = "SELECT areaid FROM JDprovince WITH (NOLOCK) WHERE areaname = '"
                + provinces + "'";
        List plist = Server.getInstance().getSystemService()
                .findMapResultBySql(psql, null);
        if (plist.size() > 0) {
            Map map = (Map) plist.get(0);
            privinceCode = map.get("areaid").toString();
        }

        String csql = "SELECT areaid,parnetid FROM JDprovince WITH (NOLOCK) WHERE areaname = '"
                + city + "'";
        List clist = Server.getInstance().getSystemService()
                .findMapResultBySql(csql, null);
        if (clist.size() > 0) {
            Map map = (Map) clist.get(0);
            cityCode = map.get("areaid").toString();
            parnetid = map.get("parnetid").toString();
        }
        if (!"".equals(privinceCode) && privinceCode.equals(parnetid)) {
            sendCode = privinceCode + "_" + cityCode;
        }
        WriteLog.write("线下快递时效接口", Math.random()*1000 + "--(JD)--province:" + province
                + "--->city:" + city + "--->sendCode:" + sendCode);
        return sendCode;
    }
	/**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public String getExpressCodes(String address){
        String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        String cityCode="010";
        if(list.size()>0){
            Map map=(Map)list.get(0);
            cityCode=map.get("CityCode").toString();
        }
        return cityCode;
    }
	/**
     * 获取出票点ID
     * @param agengId
     * @param address
     * @return
     * @throws ParseException 
     */
    public String getDelieveStr(String agengId,String address) throws ParseException{
        String results="";
        String fromcode="010";
        String tocode=getExpressCodes(address);
        String time1="10:00:00";
        String time2="18:00:00";
        
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        String dates=sdf.format(new Date());
        String sql1="SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId="+agengId;
        List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if(list.size()>0){
            Map map=(Map)list.get(0);
            fromcode=map.get("fromcode").toString();
            time1=map.get("time1").toString();
            time2=map.get("time2").toString();
        }
        String realTime=getRealTimes(dates,time1,time2);
        String urlString=PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param="times="+realTime+"&fromcode="+fromcode+"&tocode="+tocode;
         WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->"+urlString+"?"+param);
        String result=SendPostandGet.submitPost(urlString, param,"UTF-8").toString();
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->result:"+result);
        try {
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if("OK".equals(root.elementText("Head"))&&result.contains("deliver_time")){
                    Element deliverTmResponse = body.element("DeliverTmResponse");
                    Element deliverTm = deliverTmResponse.element("DeliverTm");
                    String business_type_desc=deliverTm.attributeValue("business_type_desc");
                    String deliver_time=deliverTm.attributeValue("deliver_time");
                    String business_type=deliverTm.attributeValue("business_type");
                    results = deliver_time.substring(0, deliver_time.indexOf(","));
            }
            else{
                results="";
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        WriteLog.write("抢票分线下请求时效temp", "results="+results);
        return results;
    }
    
    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }
    public Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }
	/**
     * 根据数据库设置匹配出票点
     * @param address1
     * @return
     */
    public String distribution2(String address1) {
        boolean flag=false;
        //默认出票点
        String sql1="SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=68";
        List list1=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        String agentId="378";
        if(list1.size()>0){
            Map map=(Map)list1.get(0);
            agentId=map.get("agentId").toString();
        }
        //程序自动分配出票点
        String sql2="SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid=68";
        List list2=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        for(int i=0;i<list2.size();i++){
            Map mapp=(Map)list2.get(i);
            String provinces=mapp.get("provinces").toString();
            String agentid=mapp.get("agentId").toString();
            String[] add=provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                if (address1.startsWith(add[j])) {
                    agentId=agentid;
                    flag=true;
                }
                if(flag){
                    break;
                }
            }
            if(flag){
                break;
            }
        }
        WriteLog.write("抢票分线下快递路由分配订单", "agentId="+agentId+";address1="+address1);
        return agentId;
    }
    
    /**
     * 通过agentId拿对应的省市
     * @param agentId
     * @return
     */
    public String getCityByAgentId(String agentId){
    	String provinceName = "北京";
    	String cityName = "朝阳区";
    	String sql = "SELECT ProvinceName,CityName,RegionName FROM T_CUSTOMERAGENT WHERE ID = '" + agentId + "'";
    	List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    	if(list.size()>0){
            Map map=(Map)list.get(0);
            provinceName=map.get("ProvinceName").toString();
            cityName=map.get("CityName").toString();
            String regionName = map.get("RegionName").toString();
            if (provinceName.equals(cityName.replace("市", ""))) {
            	cityName = regionName;
			}
        }
    	WriteLog.write("抢票分线下快递路由分配订单", "agentId=" + agentId + ";city=" + provinceName + "_" + cityName);
    	return provinceName + "_" + cityName;
    }
}
