package com.ccservice.inter.job.tcreport;

import org.apache.commons.httpclient.NameValuePair;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.util.CCSHttpClient;
import com.ccservice.b2b2c.atom.component.util.CCSPostMethod;
import com.ccservice.inter.job.WriteLog;

public class TongChengReportUtil {

    public static void main(String[] args) {

    }

    public JSONObject TongChengReport(ReportDataUtil data, String md5key) {
        JSONObject TongChengReportJson = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            String order_no = data.getOrder_no();
            String ticket_no = data.getTicket_no();
            String amount = data.getAmount();
            String settlement_type = data.getSettlement_type();
            String quantity = data.getQuantity();
            String trade_date = data.getTrade_date();
            String settlement_date = data.getSettlement_date();
            String channel = data.getChannel();
            String account_balance = data.getAccount_balance();
            String ordertype = data.getOrder_type();
            String unique = data.getUnique() == null ? "" : data.getUnique();
            jsonObject.put("order_no", order_no);
            jsonObject.put("ticket_no", ticket_no);
            jsonObject.put("amount", amount);
            jsonObject.put("settlement_type", settlement_type);
            jsonObject.put("quantity", quantity);
            jsonObject.put("trade_date", trade_date);
            jsonObject.put("settlement_date", settlement_date);
            jsonObject.put("channel", channel);
            jsonObject.put("account_balance", account_balance);
            jsonObject.put("order_type", ordertype);
            jsonObject.put("unique", unique);
            String md5Str = "param=" + jsonObject + "&md5key=" + md5key;
            String sign = com.tenpay.util.MD5Util.MD5Encode(md5Str, "UTF-8");
            TongChengReportJson.put("jsonstring", jsonObject);
            TongChengReportJson.put("sign", sign);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return TongChengReportJson;
    }

    /**
     * 
     * @param order_no
     * @param ticket_no
     * @param amount
     * @param settlement_type
     * @param quantity
     * @param settlement_date
     * @param trade_date
     * @param channel
     * @param account_balance
     * @param md5key
     * @return
     */
    public static String TongChengReportResult(String order_no, String ticket_no, String amount, String settlement_type,
            String quantity, String settlement_date, String trade_date, String channel, String account_balance,
            String md5key, String order_type, String UnionId) {
        String result = "";
        try {
            TongChengReportUtil tongChengReportUtil = new TongChengReportUtil();
            ReportDataUtil data = new ReportDataUtil();
            data.setOrder_no(order_no);
            data.setTicket_no(ticket_no);
            data.setAccount_balance(account_balance);
            data.setAmount(amount);
            data.setChannel(channel);
            data.setSettlement_date(settlement_date);
            data.setSettlement_type(settlement_type);
            data.setOrder_type(order_type);
            data.setUnique(UnionId);
            if ("1".equals(settlement_type)) {//出票
                data.setQuantity(quantity);
            }
            else if ("3".equals(settlement_type) || "4".equals(settlement_type)) {//退票
                data.setQuantity("-1");
            }
            else {
                data.setQuantity("0");
            }
            data.setTrade_date(trade_date);
            JSONObject json = tongChengReportUtil.TongChengReport(data, md5key);
            String jsonstring = json.getString("jsonstring");
            String sign = json.getString("sign");
            String cancelOrderUrl = "http://train.17usoft.net/gateway/supplier/bill?param=" + jsonstring + "&sign="
                    + sign;
            result = "";
            WriteLog.write("s上传链接", "请求：" + cancelOrderUrl);
            CCSHttpClient httpclient = new CCSHttpClient(false, 30000L);
            CCSPostMethod post = new CCSPostMethod("http://train.17usoft.net/gateway/supplier/bill");
            NameValuePair[] names1 = new NameValuePair[2];
            NameValuePair nameparam = new NameValuePair("param", jsonstring);
            names1[0] = nameparam;
            NameValuePair signparam = new NameValuePair("sign", sign);
            names1[1] = signparam;
            post.setRequestBody(names1);
            httpclient.executeMethod(post);
            result = post.getResponseBodyAsString().toString();
            WriteLog.write("s上传链接", "响应：" + result);
            if (result.contains("succeed")) {
                result = "succeed";
            }
            else {
                result = "failed";
            }

        }
        catch (org.apache.http.ParseException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
