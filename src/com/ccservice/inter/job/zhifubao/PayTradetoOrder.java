package com.ccservice.inter.job.zhifubao;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.service.ITrainService;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.uniontrade.Uniontrade;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 支付宝交易记录匹配订单
 * @author wzc
 * @version 创建时间：2015年6月9日 下午6:56:01
 */
public class PayTradetoOrder extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String tradeupdateflag = getSysconfigStringbydb("tradeupdateflag");
        if ("1".equals(tradeupdateflag)) {
            changeSystemCofigbyname("tradeupdateflag", "2");
            updateTradeRecord(2, "", "");
            changeSystemCofigbyname("tradeupdateflag", "1");
        }
        else {
            System.out.println("交易记录匹配关闭");
        }
    }

    /**
     * 测试方法
     * @param args
     */
    public static void main(String[] args) {
        //updateTradeRecordTwo();
        //for (int i = 0; i < 100; i++) {
        //updateTradeRecord(4, "2015-06-12", "2015-06-13");
        //        updateTradeRecordTwo();
        //}
        updateGQTradeRecord(2, "2015-12-17", "2015-12-18");
        //        updateTradeRecordStart(2, "2015-12-17", "2015-12-18");
    }

    public static void updateTradeRecordTwo() {
        List list = Server.getInstance().getSystemService().findMapResultByProcedure("sp_UpdateUnionTradeOrderNumber");
    }

    /**
     * 解析空数据
     * @param data
     * @return
     */
    public static Object parseNullData(Object data) {
        if (data == null) {
            return "";
        }
        return data;
    }

    public static Timestamp parseTimestampbyString(String timeString) {
        try {
            if (timeString != null) {
                return new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(
                        timeString.substring(0, timeString.lastIndexOf("."))).getTime());
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUtil.getCurrentTime();
    }

    public static Uniontrade getTrainorderbyMapunion(Map map) {
        Uniontrade trade = new Uniontrade();
        try {
            trade.setTrandnum(parseNullData(map.get("C_TRANDNUM")).toString());
            trade.setPayname(parseNullData(map.get("C_PAYNAME")).toString());
            trade.setAmount(Float.valueOf(map.get("C_AMOUNT").toString()));
            trade.setKey(parseNullData(map.get("C_KEY")).toString());
            trade.setBusstype(Long.valueOf(map.get("C_BUSSTYPE").toString()));
            trade.setLiushiuhao(parseNullData(map.get("C_LIUSHUIHAO")).toString());
            trade.setWnumber(parseNullData(map.get("C_WNUMBER")).toString());
            trade.setOrdertime(parseTimestampbyString(map.get("C_ORDERTIME").toString()));
            trade.setOrdernumber(parseNullData(map.get("C_ORDERNUMBER")).toString());
            trade.setOrderid(map.get("C_ORDERID") == null ? null : Long.valueOf(map.get("C_ORDERID").toString()));
            trade.setBalance(Float.valueOf(map.get("C_BALANCE").toString()));
            trade.setBankTransNo(parseNullData(map.get("C_BANKTRANSNO")).toString());
            trade.setOrderstats(map.get("C_ORDERSTATS") == null ? null : Long.valueOf(map.get("C_ORDERSTATS")
                    .toString()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return trade;
    }

    public static List<Uniontrade> getUnionList(List list) {
        List<Uniontrade> listunion = new ArrayList<Uniontrade>();
        for (int i = 0; i < list.size(); i++) {
            Uniontrade trade = getTrainorderbyMapunion((Map) list.get(i));
            listunion.add(trade);
        }
        return listunion;
    }

    /**
     * 更新记录起点
     * @param type
     */
    public static void updateTradeRecordStart(int type, String timestart, String timeend) {
        if (type == 3) {
            updateTradeRecordTwo();
        }
        else if (type == 2 || type == 1) {
            updateTradeRecord(type, "", "");
        }
        else if (type == 4) {
            updateTradeRecord(type, timestart, timeend);
        }
    }

    /**
     * 改签比对
     */
    public static void updateGQTradeRecord(int type, String timestart, String timeend) {
        String tuniuserviceurl = PropertyUtil.getValue("tuniuserviceurl", "train.properties");
        List<?> list = new ArrayList();
        do {
            String wheresql = "";
            if (type == 1) {//所有记录
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE (C_ORDERNUMBER is null or C_ORDERNUMBER='')  and (C_WNUMBER is not null or C_WNUMBER!='')  ";
            }
            else if (type == 2) {//昨天
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE (C_ORDERNUMBER is null or C_ORDERNUMBER='')  and C_ORDERTIME>'"
                        + TimeUtil.gettodaydatebyfrontandback(1, -1)
                        + "' and (C_WNUMBER is not null or C_WNUMBER!='') and c_busstype!=3 ";
            }
            else if (type == 4) {
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE (C_ORDERNUMBER is null or C_ORDERNUMBER='')  and C_ORDERTIME>'"
                        + timestart
                        + "' and C_ORDERTIME<='"
                        + timeend
                        + "' and (C_WNUMBER is not null or C_WNUMBER!='') and c_busstype!=3 ";
            }
            list = Server.getInstance().getSystemService().findMapResultBySql(wheresql, null);
            List<Uniontrade> unionlist = getUnionList(list);
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService tnservice = null;
            try {
                tnservice = (ISystemService) factory.create(ISystemService.class, "http://" + tuniuserviceurl
                        + "/cn_service/service/" + ISystemService.class.getSimpleName());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < unionlist.size(); i++) {
                Uniontrade trade = unionlist.get(i);
                String sqlT = "select ID,C_TCNUMBER from T_TRAINORDERCHANGE with(nolock) where C_SUPPLYTRADENO ='"
                        + trade.getTrandnum() + "'";
                List listmap = Server.getInstance().getSystemService().findMapResultBySql(sqlT, null);
                //改签退款
                if (listmap != null && listmap.size() == 1) {
                    Map map = (Map) listmap.get(0);
                    long orderId = Long.parseLong(map.get("ID").toString());
                    String tcnumber = map.get("C_TCNUMBER").toString();
                    String sql = "update T_UNIONTRADE set C_SOURCETYPE=1,C_ORDERID=" + orderId + ",C_ORDERNUMBER='"
                            + tcnumber + "' where C_KEY='" + trade.getKey() + "'";
                    int t = Server.getInstance().getSystemService().excuteUniontradeBySql(sql);
                    WriteLog.write("同程匹配GQ", "shou:" + t + ":" + trade.getTrandnum() + ":" + sql);
                }
                else {
                    if (tnservice != null) {
                        String sqlTm = "select ID,C_TCNUMBER from T_TRAINORDERCHANGE with(nolock) where C_SUPPLYTRADENO = '"
                                + trade.getTrandnum() + "'";
                        List listmapt = tnservice.findMapResultBySql(sqlTm, null);
                        if (listmapt != null && listmapt.size() == 1) {//本地查找到订单C_SOURCETYPE=1
                            Map map = (Map) listmapt.get(0);
                            long orderId = Long.parseLong(map.get("ID").toString());
                            String tcnumber = map.get("C_TCNUMBER").toString();
                            String sqltn = "update T_UNIONTRADE set C_SOURCETYPE=2,C_ORDERID=" + orderId
                                    + ",C_ORDERNUMBER='" + tcnumber + "' where C_KEY='" + trade.getKey() + "'";
                            WriteLog.write("途牛匹配GQ", trade.getTrandnum() + ":" + sqltn);
                            Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                        }
                        else {
                            WriteLog.write("订单出票匹配失败GQW", "W:" + trade.getWnumber() + ";T:" + trade.getTrandnum()
                                    + ":A：" + trade.getPayname());
                        }
                    }
                    else {
                        WriteLog.write("订单出票匹配失败GQW", "W:" + trade.getWnumber() + ";T:" + trade.getTrandnum() + ":A："
                                + trade.getPayname());
                    }
                }
            }
        }
        while (list.size() > 0);

    }

    /**
     * 更新支付记录
     */
    public static void updateTradeRecord(int type, String timestart, String timeend) {
        String tuniuserviceurl = PropertyUtil.getValue("tuniuserviceurl", "train.properties");
        List<?> list = new ArrayList();
        int countflag = -1;
        do {
            String wheresql = "";
            if (type == 1) {//所有记录
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE C_ORDERSTATS is null"
                        + " and (C_ORDERNUMBER is null or C_ORDERNUMBER='')  and C_WNUMBER is not null  and c_busstype!=3  order by newid() ";
            }
            else if (type == 2) {//前天
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE (C_ORDERNUMBER is null or C_ORDERNUMBER='')  and C_ORDERTIME>'"
                        + TimeUtil.gettodaydatebyfrontandback(1, -1)
                        + "' and (C_WNUMBER is not null or C_WNUMBER!='') and c_busstype!=3  order by newid() ";
            }
            else if (type == 4) {
                wheresql = "select top 500 *  from T_UNIONTRADE with(nolock) WHERE  C_ORDERNUMBER is null  and C_ORDERTIME>'"
                        + timestart
                        + "' and C_ORDERTIME<='"
                        + timeend
                        + "' and (C_WNUMBER is not null or C_WNUMBER!='')  and c_busstype!=3  order by newid() ";
            }
            list = Server.getInstance().getSystemService().findMapResultBySql(wheresql, null);
            int countflagtemp = list.size();
            if (countflagtemp < 500) {
                if (countflag > 0 && countflag == countflagtemp) {
                    countflag = 0;
                }
                else {
                    countflag = countflagtemp;
                }
            }
            if (countflag != 0) {
                List<Uniontrade> unionlist = getUnionList(list);
                HessianProxyFactory factory = new HessianProxyFactory();
                ITrainService tnservice = null;
                try {
                    tnservice = (ITrainService) factory.create(ITrainService.class, "http://" + tuniuserviceurl
                            + "/cn_service/service/" + ITrainService.class.getSimpleName());
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < unionlist.size(); i++) {
                    Uniontrade trade = unionlist.get(i);
                    Trainform trainform = new Trainform();
                    trainform.setOrderstatus(3);
                    trainform.setSupplytradeno(trade.getTrandnum());
                    List<Trainorder> orders = Server.getInstance().getTrainService().findAllTrainorder(trainform, null);
                    if (orders != null && orders.size() == 1) {//本地查找到订单C_SOURCETYPE=1
                        Trainorder order = orders.get(0);
                        String sql = "update T_UNIONTRADE set C_SOURCETYPE=1,C_ORDERID=" + order.getId()
                                + ",C_ORDERNUMBER='" + order.getOrdernumber() + "' where C_KEY='" + trade.getKey()
                                + "'";
                        int t = Server.getInstance().getSystemService().excuteUniontradeBySql(sql);
                        WriteLog.write("同程匹配", "shou:" + i + ":" + sql);
                    }
                    else {
                        boolean flag = true;
                        if (trade.getWnumber() != null && trade.getWnumber().length() > 0) {
                            trainform.setSupplytradeno(trade.getWnumber());
                            List<Trainorder> orderw = Server.getInstance().getTrainService()
                                    .findAllTrainorder(trainform, null);
                            if (orderw != null && orderw.size() == 1) {//本地查找到订单C_SOURCETYPE=1
                                flag = false;
                                Trainorder ordertn = orderw.get(0);
                                String sqltn = "update T_UNIONTRADE set C_SOURCETYPE=1,C_ORDERID=" + ordertn.getId()
                                        + ",C_ORDERNUMBER='" + ordertn.getOrdernumber() + "' where C_KEY='"
                                        + trade.getKey() + "'";
                                Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                                String sqlt = "  update T_TRAINORDER set C_SUPPLYTRADENO='" + trade.getTrandnum()
                                        + "',C_AUTOUNIONPAYURLSECOND='" + trade.getPayname() + "' where ID= "
                                        + ordertn.getId();
                                WriteLog.write("同程匹配W", sqlt);
                                Server.getInstance().getTrainService().excuteTrainroomBySql(sqlt);
                            }
                        }
                        if (flag) {
                            if (tnservice != null) {
                                trainform.setSupplytradeno(trade.getTrandnum());
                                List<Trainorder> orderstn = tnservice.findAllTrainorder(trainform, null);
                                if (orderstn != null && orderstn.size() == 1) {//本地查找到订单C_SOURCETYPE=1
                                    Trainorder ordertn = orderstn.get(0);
                                    String sqltn = "update T_UNIONTRADE set C_SOURCETYPE=2,C_ORDERID="
                                            + ordertn.getId() + ",C_ORDERNUMBER='" + ordertn.getOrdernumber()
                                            + "' where C_KEY='" + trade.getKey() + "'";
                                    WriteLog.write("途牛匹配", sqltn);
                                    Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                                }
                                else {
                                    if (trade.getWnumber() != null && trade.getWnumber().length() > 0) {
                                        trainform.setSupplytradeno(trade.getWnumber());
                                        List<Trainorder> orderstnw = tnservice.findAllTrainorder(trainform, null);
                                        if (orderstnw != null && orderstnw.size() == 1) {//本地查找到订单C_SOURCETYPE=1
                                            Trainorder ordertn = orderstnw.get(0);
                                            String sqltn = "update T_UNIONTRADE set C_SOURCETYPE=2,C_ORDERID="
                                                    + ordertn.getId() + ",C_ORDERNUMBER='" + ordertn.getOrdernumber()
                                                    + "' where C_KEY='" + trade.getKey() + "'";
                                            Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                                            String sqlt = "  update T_TRAINORDER set C_SUPPLYTRADENO='"
                                                    + trade.getTrandnum() + "',C_AUTOUNIONPAYURLSECOND='"
                                                    + trade.getPayname() + "' where ID= " + ordertn.getId();
                                            WriteLog.write("途牛匹配W", sqlt);
                                            tnservice.excuteTrainroomBySql(sqlt);
                                        }
                                        else {
                                            WriteLog.write("订单出票匹配失败W",
                                                    "W:" + trade.getWnumber() + ";T:" + trade.getTrandnum() + ":A："
                                                            + trade.getPayname());
                                            String sqltn = "update T_UNIONTRADE set C_ORDERSTATS=2 where C_KEY='"
                                                    + trade.getKey() + "'";
                                            Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                                        }
                                    }
                                    else {
                                        WriteLog.write(
                                                "订单出票匹配失败",
                                                "W:" + trade.getWnumber() + ";T:" + trade.getTrandnum() + ":A："
                                                        + trade.getPayname());
                                        String sqltn = "update T_UNIONTRADE set C_ORDERSTATS=2 where C_KEY='"
                                                + trade.getKey() + "'";
                                        Server.getInstance().getSystemService().excuteUniontradeBySql(sqltn);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                System.out.println("关闭了");
            }
        }
        while (list.size() > 0 && countflag != 0);
    }
}
