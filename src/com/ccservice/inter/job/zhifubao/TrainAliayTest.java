package com.ccservice.inter.job.zhifubao;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.ccservice.b2b2c.policy.SendPostandGet;

public class TrainAliayTest {
    public static void main(String[] args) {
        //updateTrade();
        readLog();
        for (int i = 0; i < 300; i++) {
            System.out.println("i:" + i);
            //    updateTradeByDb();
        }
    }

    public static void readLog() {
        String msg = "{'cmd':'logToDatabase'}";
        try {
            msg = SendPostandGet.submitPost("http://121.40.62.200:36958/ticket_inter/tradeMatch",
                    "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(msg);
    }

    public static void updateTradeByDb() {
        String msg = "{'cmd':'PayTradetoOrder','type':'3'}";
        try {
            msg = SendPostandGet.submitPost("http://121.40.62.200:36958/ticket_inter/tradeMatch",
                    "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(msg);
    }

    public static void updateTrade() {
        String msg = "{'cmd':'PayTradetoOrder','type':'1'}";
        try {
            msg = SendPostandGet.submitPost("http://121.40.62.200:36958/ticket_inter/tradeMatch",
                    "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(msg);
    }
}
