package com.ccservice.inter.job.zrate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.xml.sax.InputSource;

import client.JinRiPolicyServerStub;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 今日更新政策
 * usertype 目前只有0,其他的1和2目前没有数据所以不用请求
 * 只有散客和单程有数据
 * @author chend
 * 
 */
public class JobJRUpdateRatePackage extends SupplyMethod implements Job {
    private String JRUSER = new JinRiBook().getJRUSER();

    public static void main(String[] args) {
        JobJRUpdateRatePackage JobJRUpdateRatePackage = new JobJRUpdateRatePackage();
        JobJRUpdateRatePackage.execute();
    }

    public void execute(JobExecutionContext context) {
        execute();
    }

    private void execute() {
        String JINRINUM = "0";
        String sql = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='JINRINUM'";
        List clist = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (clist.size() > 0) {
            Map m = (Map) clist.get(0);
            JINRINUM = m.get("PICKTIME").toString();
            if (JINRINUM == null) {
                System.out.println("今日天下通还没全取");
                return;
            }
        }
        if (JINRINUM.length() == 0) {
            System.out.println("JINRINUM:异常");
            return;
        }
        if (Integer.parseInt(JINRINUM) > getjinrinum()) {
            System.out.println("JINRINUM:" + JINRINUM + ",getjinrinum():" + getjinrinum());
            return;
        }
        if ("479".equals(JINRINUM)) {
            Server.getInstance().getSystemService()
                    .findMapResultBySql("UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRINUM'", null);
        }

        String JINRIISSTAR = "0";
        String sql1 = "SELECT C_VALUE AS PICKTIME FROM T_B2BSEQUENCE WHERE C_NAME='JINRIISSTAR'";
        List clist1 = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (clist1.size() > 0) {
            Map m = (Map) clist1.get(0);
            JINRIISSTAR = m.get("PICKTIME").toString();
        }
        //        if (JINRIISSTAR.equals("1")) {
        //            System.out.println("上次还没更新完");
        //            return;
        //        }
        String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='JINRIISSTAR'";
        Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);
        for (int i = 0; i <= 2; i++) {
            try {
                JinRiPolicyServerStub stub = new JinRiPolicyServerStub();
                JinRiPolicyServerStub.GetPath getpath = new JinRiPolicyServerStub.GetPath();
                StringBuffer str = new StringBuffer();
                str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
                //              str.append("<JIT-Policy-Request><Request username=\""
                //                      + JRUSER
                //                      + "\" rateway=\""
                //                      + i
                //                      + "\" usertype=\"0\" voyagetype=\"0\" basepack=\"1\" uptime=\""
                //                      + getUpdateTime(JINRINUM)
                //                      + "\" tm=\"3\"/></JIT-Policy-Request>");
                str.append("<JIT-Policy-Request><Request username=\"" + JRUSER + "\" rateway=\"" + i
                        + "\" usertype=\"0\" voyagetype=\"0\" basepack=\"1\" uptime=\""
                        + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "\" tm=\"" + JINRINUM
                        + "\"/></JIT-Policy-Request>");
                WriteLog.write("JINRI_UPDATEZRATE", str.toString());
                getpath.setData(str.toString());
                JinRiPolicyServerStub.GetPathResponse response = stub.getPath(getpath);
                String resultURL = response.getGetPathResult();
                WriteLog.write("JINRI_UPDATEZRATE", resultURL);
                if (resultURL.length() <= 6) {
                    System.out.println("jinri:同步:" + str.toString());
                    System.out.println("jinri:同步:" + resultURL);
                    continue;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String path = "jinriupdateZrate\\" + sdf.format(new Date());
                downZip(resultURL, path);
                try {
                    long lasting = System.currentTimeMillis();
                    SAXParserFactory sf = SAXParserFactory.newInstance();
                    SAXParser sp = sf.newSAXParser();
                    String zippath = "D:\\" + path + "\\"
                            + resultURL.substring(resultURL.lastIndexOf("/") + 1, resultURL.length() - 3) + "zip";
                    unZip(zippath);
                    String xmlpath = "D:\\" + path + "\\"
                            + resultURL.substring(resultURL.lastIndexOf("/") + 1, resultURL.length() - 3) + "xml";
                    sp.parse(new InputSource(xmlpath), new AnalyticJinriXml());
                    System.out.println("运行时间：" + (System.currentTimeMillis() - lasting) + "毫秒");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + JINRINUM + "' WHERE C_NAME='JINRINUM'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
                String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            }
        }
        // 今日删除的政策
        deleteZrate(JINRINUM);
        System.out.println("jinRi获得更新包,本次结束t:" + JINRINUM);
        int t = Integer.parseInt(JINRINUM) + 1;

        String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + t + "' WHERE C_NAME='JINRINUM'";
        Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
        String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
        Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    private int getjinrinum() {
        int num = 0;
        Date date = new Date();
        int hours = date.getHours();
        int minutes = date.getMinutes();
        num += hours * 20;
        num += (minutes / 3);
        return num - 2;
    }

    private void deleteZrate(String num) {
        try {
            JinRiPolicyServerStub stub = new JinRiPolicyServerStub();
            JinRiPolicyServerStub.GetRateIdByDel getRateIdByDel = new JinRiPolicyServerStub.GetRateIdByDel();
            num = getUpdateTime(num);
            getRateIdByDel.setDt(num);
            WriteLog.write("JINRIUPDATE_DELETE", num);
            JinRiPolicyServerStub.GetRateIdByDelResponse response = stub.getRateIdByDel(getRateIdByDel);
            String result = response.getGetRateIdByDelResult();
            WriteLog.write("JINRIUPDATE_DELETE", result);
            if (result.indexOf("<Result>") >= 0) {
                int si = result.indexOf("<Result>");
                int ei = result.indexOf("</Result>");
                result = result.substring(si + 8, ei);
            }
            String[] ids = result.split("\\^");
            String where = " delete from " + Zrate.TABLE + " where 1=2 ";
            for (int i = 0; i < ids.length; i++) {
                String deleteid = ids[i];
                where += " or " + Zrate.COL_outid + "='" + deleteid + "'";
            }
            WriteLog.write("JINRIUPDATE_DELETE", where);
            System.out.println("jinri_update_delete:end:"
                    + Server.getInstance().getAirService().excuteZrateBySql(where));
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteZrate_OLD() {
        try {
            JinRiPolicyServerStub stub = new JinRiPolicyServerStub();
            JinRiPolicyServerStub.GetRateIdByDel getRateIdByDel = new JinRiPolicyServerStub.GetRateIdByDel();
            Calendar rentcal = new GregorianCalendar();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
            String rentTime = sdf.format(new Date());
            Date date = new Date();
            Date date1 = new Date();
            date1.setMinutes(date.getMinutes() - 5);
            String rentD = sdf1.format(date1);
            // date.setYear(Integer.parseInt(rentTime.split("[-]")[0]));
            date.setMonth(Integer.parseInt(rentTime.split("[-]")[1]) - 1);
            date.setDate(Integer.parseInt(rentTime.split("[-]")[2]));
            date.setHours(Integer.parseInt(rentD.split("[:]")[0]));
            date.setMinutes(Integer.parseInt(rentD.split("[:]")[1]));
            rentcal.setTime(date);
            System.out.println(rentcal.toString());
            getRateIdByDel.setDt(rentcal.toString());
            JinRiPolicyServerStub.GetRateIdByDelResponse response = stub.getRateIdByDel(getRateIdByDel);
            String result = response.getGetRateIdByDelResult();
            String[] ids = result.split("\\^");
            String where = " delete from " + Zrate.TABLE + " where " + Zrate.COL_outid + "='" + ids[0] + "'";
            for (int i = 1; i < ids.length; i++) {
                String deleteid = ids[i];
                where += " or " + Zrate.COL_outid + "='" + deleteid + "'";
            }
            System.out.println("jinri_update_delete:" + Server.getInstance().getAirService().excuteZrateBySql(where));
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据url下载zip包并且返回zip中xml的内容 cd 2012-5-4 14:18:58
     * 已不用了效率太低,2013-11-15 16:38:25
     * @param zipUrl
     * @return
     */
    public static String getZipDate(String zipUrl) {
        StringBuffer result = new StringBuffer(1024);
        // zipUrl =
        // "http://policy.jinri.cn/CacheRateDataRAR/220001-zhaohe665055-20120504.zip";
        URL url = null;
        FileOutputStream outfile = null;
        InputStream infile = null;
        URLConnection con = null;
        byte[] buffer = null;
        //
        try {
            File tempFile = new File("D:\\jinriUpdateZrate\\" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            if (!tempFile.exists()) {
                tempFile.mkdirs();
            }

            url = new URL(zipUrl);
            outfile = new FileOutputStream("D:\\jinriUpdateZrate\\"
                    + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "\\"
                    + zipUrl.substring(zipUrl.lastIndexOf("/") + 1), true);
            con = url.openConnection();
            infile = con.getInputStream();
            int f = 0;
            while ((f = infile.read()) != -1) {
                outfile.write(f);
            }
            infile.close();
            outfile.close();

            ZipInputStream zin = new ZipInputStream(new FileInputStream("D:\\jinriUpdateZrate\\"
                    + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "\\"
                    + zipUrl.substring(zipUrl.lastIndexOf("/") + 1)));
            ZipEntry en = null;
            if ((en = zin.getNextEntry()) != null) {
                InputStreamReader insr = new InputStreamReader(zin, "utf-8");
                long size = en.getSize();
                if (size > 0) {
                    BufferedReader br = new BufferedReader(insr);
                    String line;
                    while ((line = br.readLine()) != null) {
                        result.append(line);
                    }
                    br.close();
                }
            }

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return result.toString();

    }

    /**
     * 根据数字获取今日同步的时间
     * @param num
     * @return
     */
    public String getUpdateTime(String num) {
        String result = "";
        try {
            Date date = new Date();
            Date date1 = new Date();
            SimpleDateFormat yMd = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat yMdHms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String tempDateString = yMd.format(date1) + " 00:00:00";
            Date tempDate = yMdHms.parse(tempDateString);
            long timeLong = Long.parseLong(num) * 1000 * 180 + tempDate.getTime();
            date.setTime(timeLong);
            result = yMdHms.format(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
}
