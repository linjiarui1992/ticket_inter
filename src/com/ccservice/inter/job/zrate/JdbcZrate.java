package com.ccservice.inter.job.zrate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ccservice.b2b2c.base.zrate.Zrate;

public class JdbcZrate {
    public static void main(String[] args) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        Zrate zrate = new Zrate();
        zrate.setDepartureport("departureport");
        zrate.setArrivalport("arrivalport");
        // zrate.setFlightnumber("flightnumber");
        zrate.setCabincode("cabincode");
        zrate.setRatevalue(10F);
        zrate.setCreateuser("CJOB");
        zrate.setCreatetime(new Timestamp(new Date().getTime()));
        zrate.setModifyuser("MJOB");
        zrate.setModifytime(new Timestamp(new Date().getTime()));
        zrate.setIssuedstartdate(new Timestamp(new Date().getTime()));
        zrate.setIssuedendate(new Timestamp(new Date().getTime()));
        zrate.setRemark("remark");
        zrate.setWeeknum("weeknum");
        zrate.setIsenable(1);
        zrate.setAircompanycode("aircompanycode");
        zrate.setAgentid(5L);
        zrate.setTickettype(1);
        zrate.setAddratevalue(20F);
        zrate.setGeneral(1L);
        zrate.setBegindate(new Timestamp(new Date().getTime()));
        zrate.setEnddate(new Timestamp(new Date().getTime()));
        zrate.setIstype(1L);
        zrate.setOutid("outid");
        zrate.setAfterworktime("afterworktime");
        zrate.setWorktime("worktime");
        zrate.setSpeed("speed");
        zrate.setZtype("ztype");
        zrate.setVoyagetype("voyagetype");
        zrate.setSchedule("schedule");
        zrate.setUsertype("usertype");
        for (int i = 0; i < 10; i++) {
            zrates.add(zrate);
        }
        insertZrate(zrates);
    }

    public static void insertZrate(List<Zrate> zrates) {
        Long start = new Date().getTime();
        System.out.println(start);
        String sql = "insert into T_ZRATE(C_DEPARTUREPORT,C_ARRIVALPORT,C_FLIGHTNUMBER,"
                + "C_CABINCODE,C_RATEVALUE,C_CREATEUSER,C_CREATETIME,C_MODIFYUSER,C_MODIFYTIME,"
                + "C_ISSUEDSTARTDATE,C_ISSUEDENDATE,C_REMARK,C_WEEKNUM,C_ISENABLE,C_AIRCOMPANYCODE,"
                + "C_AGENTID,C_TICKETTYPE,C_ADDRATEVALUE,C_GENERAL,C_BEGINDATE,C_ENDDATE,C_ISTYPE,"
                + "C_OUTID,C_AFTERWORKTIME,C_WORKTIME,C_SPEED,C_ZTYPE,C_VOYAGETYPE,C_SCHEDULE,"
                + "C_USERTYPE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionUtils.getConnection();
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(sql);
            for (int i = 0; i < zrates.size(); i++) {
                Zrate tzrate = zrates.get(i);
                if (tzrate.getDepartureport() != null) {
                    stmt.setString(1, tzrate.getDepartureport());
                }
                else {
                    stmt.setString(1, null);

                }
                if (tzrate.getArrivalport() != null) {
                    stmt.setString(2, tzrate.getArrivalport());
                }
                else {
                    stmt.setString(1, null);
                }
                if (tzrate.getFlightnumber() != null) {
                    stmt.setString(3, tzrate.getFlightnumber());
                }
                else {
                    stmt.setString(3, null);
                }
                if (tzrate.getCabincode() != null) {
                    stmt.setString(4, tzrate.getCabincode());
                }
                else {
                    stmt.setString(4, null);
                }
                if (tzrate.getRatevalue() != null) {
                    stmt.setFloat(5, tzrate.getRatevalue());
                }
                else {
                    stmt.setString(5, null);
                }
                stmt.setString(6, "job");
                if (tzrate.getCreatetime() != null) {
                    stmt.setTimestamp(7, tzrate.getCreatetime());
                }
                else {
                    stmt.setString(7, null);
                }
                stmt.setString(8, "job");
                if (tzrate.getModifytime() != null) {
                    stmt.setTimestamp(9, tzrate.getModifytime());
                }
                else {
                    stmt.setString(9, null);
                }
                if (tzrate.getIssuedstartdate() != null) {
                    stmt.setTimestamp(10, tzrate.getIssuedstartdate());
                }
                else {
                    stmt.setString(10, null);
                }
                if (tzrate.getIssuedendate() != null) {
                    stmt.setTimestamp(11, tzrate.getIssuedendate());
                }
                else {
                    stmt.setString(11, null);
                }
                if (tzrate.getRemark() != null) {
                    stmt.setString(12, tzrate.getRemark());
                }
                else {
                    stmt.setString(12, null);
                }
                if (tzrate.getWeeknum() != null) {
                    stmt.setString(13, tzrate.getWeeknum());
                }
                else {
                    stmt.setString(13, null);
                }
                if (tzrate.getIsenable() != null) {
                    stmt.setInt(14, tzrate.getIsenable());
                }
                else {
                    stmt.setString(14, null);
                }
                if (tzrate.getAircompanycode() != null) {
                    stmt.setString(15, tzrate.getAircompanycode());
                }
                else {
                    stmt.setString(15, null);
                }
                if (tzrate.getAgentid() != null) {
                    stmt.setLong(16, tzrate.getAgentid());
                }
                else {
                    stmt.setString(16, null);
                }
                if (tzrate.getTickettype() != null) {
                    stmt.setInt(17, tzrate.getTickettype());
                }
                else {
                    stmt.setString(17, null);
                }
                if (tzrate.getAddratevalue() != null) {
                    stmt.setFloat(18, tzrate.getAddratevalue());
                }
                else {
                    stmt.setString(18, null);
                }
                if (tzrate.getGeneral() != null) {
                    stmt.setLong(19, tzrate.getGeneral());
                }
                else {
                    stmt.setString(19, null);
                }
                if (tzrate.getBegindate() != null) {
                    stmt.setTimestamp(20, tzrate.getBegindate());
                }
                else {
                    stmt.setString(20, null);
                }
                if (tzrate.getEnddate() != null) {
                    stmt.setTimestamp(21, tzrate.getEnddate());
                }
                else {
                    stmt.setString(21, null);
                }
                if (tzrate.getIstype() != null) {
                    stmt.setLong(22, tzrate.getIstype());
                }
                else {
                    stmt.setString(22, null);
                }
                if (tzrate.getOutid() != null) {
                    stmt.setString(23, tzrate.getOutid());
                }
                else {
                    stmt.setString(23, null);
                }
                if (tzrate.getAfterworktime() != null) {
                    stmt.setString(24, tzrate.getAfterworktime());
                }
                else {
                    stmt.setString(24, null);
                }
                if (tzrate.getWorktime() != null) {
                    stmt.setString(25, tzrate.getWorktime());
                }
                else {
                    stmt.setString(25, null);
                }
                if (tzrate.getSpeed() != null) {
                    stmt.setString(26, tzrate.getSpeed());
                }
                else {
                    stmt.setString(26, null);
                }
                if (tzrate.getZtype() != null) {
                    stmt.setString(27, tzrate.getZtype());
                }
                else {
                    stmt.setString(27, null);
                }
                if (tzrate.getVoyagetype() != null) {
                    stmt.setString(28, tzrate.getVoyagetype());
                }
                else {
                    stmt.setString(28, null);
                }
                if (tzrate.getSchedule() != null) {
                    stmt.setString(29, tzrate.getSchedule());
                }
                else {
                    stmt.setString(29, null);
                }
                if (tzrate.getUsertype() != null) {
                    stmt.setString(30, tzrate.getUsertype());
                }
                else {
                    stmt.setString(30, null);
                }
                stmt.addBatch();// ��sql����������������������е�sql���̫�ࡣ
                if (i + 1 % 50 == 0) { // ÿ10����¼����һ�Ρ�
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
            }
            stmt.executeBatch();// ִ��
            conn.commit();
        }
        catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        finally {
            ConnectionUtils.close(stmt);
            ConnectionUtils.close(conn);
        }
        System.out.println("��ʱ:" + (new Date().getTime() - start));
    }

    public void deleteZrate() {

    }
}
