package com.ccservice.inter.job.zrate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.xml.sax.InputSource;

import client.JinRiPolicyServerStub;

import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 今日全取政策 usertype 目前只有0,其他的1和2目前没有数据所以不用请求
 * 只有散客和单程有数据
 * @author chend
 * 
 */
public class JobJRRatePackage extends SupplyMethod implements Job {
    JinRiBook jinRiBook = new JinRiBook();

    String JRUSER = jinRiBook.getJRUSER();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        String uptime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        System.out.println("jinRi获得基础包开始：" + uptime);
        String updsql1 = "UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='JINRIISSTAR';UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRINUM'";
        Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
        String staus = "1";
        if (staus.equals("1")) {
            String resultURL = "";
            for (int i = 0; i <= 2; i++) {
                try {
                    JinRiPolicyServerStub stub = new JinRiPolicyServerStub();
                    JinRiPolicyServerStub.GetPath getpath = new JinRiPolicyServerStub.GetPath();
                    StringBuffer str = new StringBuffer();
                    str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
                    str.append("<JIT-Policy-Request><Request  username=\"" + JRUSER + "\" rateway=\"" + i
                            + "\" usertype=\"0\" voyagetype=\"0\" basepack=\"0\" uptime=\"" + uptime
                            + "\"/></JIT-Policy-Request>");
                    getpath.setData(str.toString());
                    WriteLog.write("JINRI_BASEZRATE", str.toString());
                    JinRiPolicyServerStub.GetPathResponse response = stub.getPath(getpath);
                    resultURL = response.getGetPathResult();
                    WriteLog.write("JINRI_BASEZRATE", resultURL);
                    if (resultURL.length() <= 6) {
                        System.out.println("jinri:全取:" + resultURL);
                        continue;
                    }
                    //					resultURL = "http://down.policy.jinri.org.cn/CacheRateDataRAR/000000-hthuayou-20131115.zip";
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    String path = "jinriBaseZrate\\" + sdf.format(new Date());
                    downZip(resultURL, path);
                    try {
                        long lasting = System.currentTimeMillis();
                        SAXParserFactory sf = SAXParserFactory.newInstance();
                        SAXParser sp = sf.newSAXParser();
                        String zippath = "D:\\" + path + "\\"
                                + resultURL.substring(resultURL.lastIndexOf("/") + 1, resultURL.length() - 3) + "zip";
                        unZip(zippath);
                        String xmlpath = "D:\\" + path + "\\"
                                + resultURL.substring(resultURL.lastIndexOf("/") + 1, resultURL.length() - 3) + "xml";
                        sp.parse(new InputSource(xmlpath), new AnalyticJinriXml());
                        System.out.println("运行时间：" + (System.currentTimeMillis() - lasting) + "毫秒");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='150' WHERE C_NAME='JINRINUM'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
                    String updsql3 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql3, null);
                }
            }
            String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='140' WHERE C_NAME='JINRINUM'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql, null);
            String updsql3 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql3, null);
        }
        else {
            System.out.println("今日接口被禁用");
        }
        System.out.println("jinRi获得基础包结束");
    }

    /**
     * 根据url下载zip包并且返回zip中xml的内容 cd 2012-5-4 14:18:58
     * 已不用了效率太低,2013-11-15 16:38:25
     * @param zipUrl
     * @return
     */
    public String getZipDate(String zipUrl) {
        StringBuffer result = new StringBuffer(1024);
        // zipUrl =
        // "http://policy.jinri.cn/CacheRateDataRAR/220001-zhaohe665055-20120504.zip";
        URL url = null;
        FileOutputStream outfile = null;
        InputStream infile = null;
        URLConnection con = null;
        //
        try {
            File tempFile = new File("D:\\jinriBaseZrate");
            if (!tempFile.exists()) {
                tempFile.mkdir();
            }
            url = new URL(zipUrl);
            String filepath = "D:\\jinriBaseZrate\\" + zipUrl.substring(zipUrl.lastIndexOf("/") + 1);
            new File(filepath).delete();
            outfile = new FileOutputStream(filepath, true);
            con = url.openConnection();
            infile = con.getInputStream();
            int f = 0;
            while ((f = infile.read()) != -1) {
                outfile.write(f);
            }
            infile.close();
            outfile.close();

            ZipInputStream zin = new ZipInputStream(new FileInputStream("D:\\jinriBaseZrate\\"
                    + zipUrl.substring(zipUrl.lastIndexOf("/") + 1)));
            ZipEntry en = null;
            if ((en = zin.getNextEntry()) != null) {
                InputStreamReader insr = new InputStreamReader(zin, "utf-8");
                long size = en.getSize();
                if (size > 0) {
                    BufferedReader br = new BufferedReader(insr);
                    String line;
                    while ((line = br.readLine()) != null) {
                        // System.out.println(line);
                        result.append(line);
                    }
                    br.close();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            return null;
        }
        return result.toString();
    }

    public static String checkCity(String city, String cityString) {
        if (cityString.indexOf(city) >= 0) {
            cityString = cityString.replaceAll(city, "");
        }
        return cityString;
    }
}
