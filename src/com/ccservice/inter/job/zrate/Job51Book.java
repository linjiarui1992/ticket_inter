package com.ccservice.inter.job.zrate;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import client.GetPolicyServiceImpl_1_0ServiceStub;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.UtilMethod;
import com.ccservice.b2b2c.policy.ben.FiveonBook;
import com.ccservice.inter.job.DBUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.version2_0.getpolicydata.GetPolicyDataService_2_0Stub;
import com.liantuo.webservice.version2_0.getpolicydata.GetPolicyDataService_2_0Stub.PolicyData;

public class Job51Book implements Job {//
    static DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void execute(JobExecutionContext context) {
        fiveone3_0allZrate();
    }

    public static void main(String[] args) {
        fiveone3_0allZrate();
    }

    public static void fiveone3_0allZrate() {
        int rowsPerPage = 600;
        Eaccount eaccout = Server.getInstance().getSystemService().findEaccount(12);
        try {
            GetPolicyServiceImpl_1_0ServiceStub stub = new GetPolicyServiceImpl_1_0ServiceStub();
            GetPolicyServiceImpl_1_0ServiceStub.GetPolicyE getPolicy = new GetPolicyServiceImpl_1_0ServiceStub.GetPolicyE();
            GetPolicyServiceImpl_1_0ServiceStub.GetPolicy param = new GetPolicyServiceImpl_1_0ServiceStub.GetPolicy();
            GetPolicyServiceImpl_1_0ServiceStub.GetPolicyRequest request = new GetPolicyServiceImpl_1_0ServiceStub.GetPolicyRequest();

            //			String agencyCode = "SDFH";//测试账号
            //			String safecode = "vvFsiRcB";
            String agencyCode = eaccout.getUsername();
            String safecode = eaccout.getPassword();

            int needSpePricePolicy = 1;
            int needSpeRulePolicy = 1;
            int pageno = 1;
            request.setAgencyCode(agencyCode);
            request.setAirlineCode("");
            request.setArrAirportCode("");
            request.setDepAirportCode("");
            request.setNeedSpePricePolicy(needSpePricePolicy);
            request.setNeedSpeRulePolicy(needSpeRulePolicy);
            request.setPageNo(pageno);
            request.setRowPerPage(rowsPerPage);
            String sign = agencyCode + needSpePricePolicy + needSpeRulePolicy + pageno + rowsPerPage + safecode;
            WriteLog.write("51BOOK_ALL", "1页:" + sign);
            sign = HttpClient.MD5(sign);
            request.setSign(sign);
            param.setRequest(request);
            getPolicy.setGetPolicy(param);
            //第一页
            GetPolicyServiceImpl_1_0ServiceStub.GetPolicyResponseE responseE = stub.getPolicy(getPolicy);
            String ReturnMessage = responseE.getGetPolicyResponse().get_return().getReturnMessage();
            String Returncode = responseE.getGetPolicyResponse().get_return().getReturnCode();

            WriteLog.write("51BOOK_ALL", "1页:" + ReturnMessage + ":" + Returncode);
            if ("S".equals(Returncode)) {
                GetPolicyServiceImpl_1_0ServiceStub.WsPolicyData[] WsPolicyDatas = responseE.getGetPolicyResponse()
                        .get_return().getPolicyDatas();
                List<Zrate> zrates = new ArrayList<Zrate>();
                String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                if (WsPolicyDatas != null) {
                    for (int i = 0; i < WsPolicyDatas.length; i++) {
                        GetPolicyServiceImpl_1_0ServiceStub.WsPolicyData wsPolicyData = WsPolicyDatas[i];
                        Zrate zrate = getZreatebyOnefive_3_0(wsPolicyData);
                        deleteWhere += " OR C_OUTID ='" + zrate.getOutid() + "'";
                        if (zrate.getIsenable() == 1) {
                            zrates.add(zrate);
                        }
                    }
                }
                Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
                if (zrates.size() > 0) {
                    Server.getInstance().getAirService().createZrateList(zrates);
                }
                int totalPageCount = responseE.getGetPolicyResponse().get_return().getTotalPageCount();
                System.out.println("总页数:" + totalPageCount);
                WriteLog.write("51BOOK_ALL", "总页数:" + totalPageCount);
                //第二页往后
                for (int i = 2; i <= totalPageCount; i++) {
                    pageno = i;
                    request.setAgencyCode(agencyCode);
                    request.setAirlineCode("");
                    request.setArrAirportCode("");
                    request.setDepAirportCode("");
                    request.setNeedSpePricePolicy(needSpePricePolicy);
                    request.setNeedSpeRulePolicy(needSpeRulePolicy);
                    request.setPageNo(i);
                    request.setRowPerPage(rowsPerPage);
                    sign = agencyCode + needSpePricePolicy + needSpeRulePolicy + pageno + rowsPerPage + safecode;
                    WriteLog.write("51BOOK_ALL", pageno + "页:" + sign);
                    System.out.println("页数:" + pageno);
                    sign = HttpClient.MD5(sign);
                    request.setSign(sign);
                    param.setRequest(request);
                    getPolicy.setGetPolicy(param);
                    GetPolicyServiceImpl_1_0ServiceStub.GetPolicyResponseE responseE_I = stub.getPolicy(getPolicy);
                    String ReturnMessage_I = responseE_I.getGetPolicyResponse().get_return().getReturnMessage();
                    String Returncode_I = responseE_I.getGetPolicyResponse().get_return().getReturnCode();
                    WriteLog.write("51BOOK_ALL", pageno + "页:" + ReturnMessage_I + ":" + Returncode_I);
                    if ("S".equals(Returncode_I)) {
                        GetPolicyServiceImpl_1_0ServiceStub.WsPolicyData[] WsPolicyDatas_I = responseE_I
                                .getGetPolicyResponse().get_return().getPolicyDatas();
                        deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                        zrates.clear();
                        for (int j = 0; j < WsPolicyDatas_I.length; j++) {
                            GetPolicyServiceImpl_1_0ServiceStub.WsPolicyData wsPolicyData_I = WsPolicyDatas_I[j];
                            Zrate zrate = getZreatebyOnefive_3_0(wsPolicyData_I);
                            deleteWhere += " OR C_OUTID ='" + zrate.getOutid() + "'";
                            if (zrate.getIsenable() == 1) {
                                zrates.add(zrate);
                            }
                        }
                        Server.getInstance().getAirService().excuteZrateBySql(deleteWhere);
                        if (zrates.size() > 0) {
                            Server.getInstance().getAirService().createZrateList(zrates);
                        }
                    }
                    else {
                        WriteLog.write("51BOOK_ALL", ReturnMessage_I);
                        System.out.println("51book全取失败:" + ReturnMessage_I);
                    }
                }
            }
            else {
                WriteLog.write("51BOOK_ALL", ReturnMessage);
                System.out.println("51book全取失败:" + ReturnMessage);
            }
        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Zrate getZreatebyOnefive_3_0(GetPolicyServiceImpl_1_0ServiceStub.WsPolicyData wsPolicyData) {
        Zrate zrate = new Zrate();
        try {
            //起飞三字码
            zrate.setDepartureport(wsPolicyData.getFlightCourse().split("[-]")[0]);
            //到达三字码
            zrate.setArrivalport(wsPolicyData.getFlightCourse().split("[-]")[1]);
            if (wsPolicyData.getFlightNoIncluding() != null && wsPolicyData.getFlightNoIncluding().length() > 0) {
                //适用航班号
                zrate.setFlightnumber(wsPolicyData.getFlightNoIncluding());
            }
            //仓位代码
            zrate.setCabincode(wsPolicyData.getSeatClass());
            //返点
            zrate.setRatevalue(wsPolicyData.getCommisionPoint());
            zrate.setCreateuser("51BOOK3.0_ALL");
            zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
            zrate.setRemark(wsPolicyData.getComment());
            zrate.setAircompanycode(wsPolicyData.getAirlineCode());
            zrate.setAgentid(5L);
            zrate.setTickettype(wsPolicyData.getPolicyType().equals("B2B") ? 2 : 1);
            if (wsPolicyData.getProductType() == 1) {
                zrate.setGeneral(1L);
            }
            else {
                zrate.setGeneral(2L);
            }
            //开始结束时间
            zrate.setBegindate(new Timestamp(fromat.parse(wsPolicyData.getStartDate()).getTime()));
            zrate.setEnddate(new Timestamp(fromat.parse(wsPolicyData.getExpiredDate()).getTime()));
            zrate.setOutid(wsPolicyData.getPolicyId() + "");

            zrate.setAfterworktime(wsPolicyData.getWorkTime().split("[-]")[1]);
            zrate.setWorktime(wsPolicyData.getWorkTime().split("[-]")[0]);//08:00-23:59
            zrate.setSpeed(wsPolicyData.getTicketSpeed().split("分钟")[0]);
            zrate.setOnetofivewastetime(wsPolicyData.getVtWorkTime());
            zrate.setZtype("1");
            zrate.setVoyagetype(wsPolicyData.getRouteType().equals("OW") ? "1" : "2");
            zrate.setUsertype("1");
            //
            zrate.setIschange((long) wsPolicyData.getNeedSwitchPNR());
            //不适用的航班号
            if (wsPolicyData.getFlightNoExclude() != null && wsPolicyData.getFlightNoExclude().length() > 0) {
                //适用航班号
                zrate.setWeeknum(wsPolicyData.getFlightNoExclude());
            }
            zrate.setIsenable(1);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrate;
    }

    public Zrate createZreatebyOnefive(PolicyData aa) {
        Zrate zrate = new Zrate();
        if (aa != null) {
            try {
                zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                zrate.setCreateuser("51BOOK");
                zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                zrate.setModifyuser("51BOOK");
                zrate.setAgentid(5l);
                zrate.setTickettype(1);
                zrate.setIsenable(1);
                zrate.setOutid(aa.getId() + "");
                if (aa.getAirlineCode() != null) {
                    zrate.setAircompanycode(aa.getAirlineCode());
                }
                if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                        && aa.getFlightCourse().indexOf("-") != -1) {
                    zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
                    if (zrate.getDepartureport() != null && zrate.getDepartureport().equals("999")) {
                        if (aa.getDepartureExclude() != null) {
                            zrate.setDepartureexclude(aa.getDepartureExclude());// 出发不适用
                        }
                    }
                    zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达

                    if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                        if (aa.getArrivalExclude() != null) {
                            zrate.setArrivalexclude(aa.getArrivalExclude());// 到达不适用
                        }
                    }
                }
                if (aa.getCommision() > 0) {
                    zrate.setRatevalue(aa.getCommision());
                }
                if (aa.getSeatClass() != null && aa.getSeatClass().length() >= 1) {
                    zrate.setCabincode(aa.getSeatClass());
                }
                if (aa.getFlightNoIncluding() != null && aa.getFlightNoIncluding().length() > 3) {
                    zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                }
                if (aa.getFlightNoExclude() != null && aa.getFlightNoExclude().length() > 3) {
                    zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                }
                if (aa.getFlightCycle() != null) {
                    zrate.setSchedule(aa.getFlightCycle());// 航班周期
                    // 1234567
                }
                if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                    zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                }
                if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                    zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
                }
                if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                    zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                }
                if (aa.getPrintTicketExpiredDate() != null && aa.getPrintTicketExpiredDate().getTime() != null) {
                    zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间
                }
                if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                    zrate.setTickettype(2);
                }
                else {
                    zrate.setTickettype(1);
                }
                if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程
                    zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返
                }
                else {
                    zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返
                }
                if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0 && aa.getWorkTime().indexOf("-") != -1) {
                    String worktime = aa.getWorkTime().split("-")[0];
                    zrate.setWorktime(worktime);
                    zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);
                }
                if (aa.getAgencyEfficiency() != null) {
                    zrate.setSpeed(aa.getAgencyEfficiency());
                }
                if (aa.getBusinessUnitType() != null) {
                    // System.out.println("类型=="+aa.getBusinessUnitType());
                    if (aa.getBusinessUnitType().equals("0")) {// =0 普通政策 =1
                        // 特殊政策

                        zrate.setGeneral(1l);
                        zrate.setZtype("1");
                    }
                    else {

                        zrate.setGeneral(2l);// 1,普通 2高反
                        zrate.setZtype("2");
                    }

                }
                else {
                    // System.out.println("类型==null");
                    zrate.setGeneral(1l);
                    zrate.setZtype("1");
                }
                zrate.setUsertype("1");
                if (aa.getAgencyEfficiency() != null) {
                    zrate.setSpeed(aa.getAgencyEfficiency());
                }
                if (aa.getComment() != null) {
                    zrate.setRemark(aa.getComment());

                }
                try {
                    if (zrate.getRatevalue() != null) {
                        DBUtil.insertZrate(zrate);
                    }
                }
                catch (RuntimeException e) {
                    UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                    e.printStackTrace();
                }
                zrate = null;
            }
            catch (Exception e) {
                UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                e.printStackTrace();
            }
        }
        return zrate;
    }

    public void fiveone2_0allZrate() {
        FiveonBook fiveonBook = new FiveonBook();
        int RowPerPage = 150;
        try {
            try {
                if (!fiveonBook.getStaus().equals("0")) {

                    String setsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='NULL' WHERE C_NAME='51ZRATETIME'";
                    Server.getInstance().getSystemService().findMapResultBySql(setsql, null);

                    String setsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='51ISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(setsql2, null);

                    Server.getInstance()
                            .getAirService()
                            .excuteZrateBySql(
                                    "delete from " + Zrate.TABLE + " where C_AGENTID=5 and " + Zrate.COL_id + " !=1");

                    GetPolicyDataService_2_0Stub stub = new GetPolicyDataService_2_0Stub();
                    GetPolicyDataService_2_0Stub.GetPolicyData data = new GetPolicyDataService_2_0Stub.GetPolicyData();
                    GetPolicyDataService_2_0Stub.GetPolicyDataRequest re = new GetPolicyDataService_2_0Stub.GetPolicyDataRequest();
                    GetPolicyDataService_2_0Stub.SecurityCredential sec = new GetPolicyDataService_2_0Stub.SecurityCredential();

                    re.setAirline("");
                    re.setDeparture("");
                    re.setArrival("");
                    re.setIsIncludeSpecialPolicy("1");// 特价政策 0不包括 1包括
                    re.setIsBusinessUnitPolicy("1");// 特殊政策 0不包括 1包括
                    re.setPage(1);
                    re.setRowPerPage(RowPerPage);

                    sec.setAgencyCode(fiveonBook.getAgentcode());
                    String sign = HttpClient.MD5(sec.getAgencyCode() + re.getAirline() + re.getDeparture()
                            + re.getArrival() + re.getPage() + re.getRowPerPage() + re.getIsIncludeSpecialPolicy()
                            + re.getIsBusinessUnitPolicy() + fiveonBook.getSafecode());
                    sec.setSign(sign);

                    re.setCredential(sec);
                    data.setIn0(re);

                    GetPolicyDataService_2_0Stub.GetPolicyDataResponse res = stub.getPolicyData(data);
                    System.out.println("---" + res.getOut().getReturnCode() + "--" + res.getOut().getReturnMessage());
                    System.out.println("总页数==" + res.getOut().getPageCount());
                    int p = res.getOut().getPageCount();
                    int index = 0;
                    for (int a = 1; a < p + 1; a++) {
                        System.out.println("当前页数=" + a);
                        re.setAirline("");
                        re.setDeparture("");
                        re.setArrival("");
                        re.setIsIncludeSpecialPolicy("1");// 特价政策 0不包括 1包括
                        re.setIsBusinessUnitPolicy("1");// 特殊政策 0不包括 1包括
                        re.setPage(a);
                        re.setRowPerPage(RowPerPage);
                        sec.setAgencyCode(fiveonBook.getAgentcode());
                        String sign1 = HttpClient.MD5(sec.getAgencyCode() + re.getAirline() + re.getDeparture()
                                + re.getArrival() + re.getPage() + re.getRowPerPage() + re.getIsIncludeSpecialPolicy()
                                + re.getIsBusinessUnitPolicy() + fiveonBook.getSafecode());
                        sec.setSign(sign1);

                        re.setCredential(sec);
                        data.setIn0(re);

                        GetPolicyDataService_2_0Stub.GetPolicyDataResponse res1;
                        try {
                            res1 = stub.getPolicyData(data);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            continue;
                        }
                        // System.out.println("---"+res.getOut().getReturnCode()+"--"+res.getOut().getReturnMessage());
                        // System.out.println("总页数=="+res1.getOut().getPageCount());
                        System.out.println("---" + res.getOut().getReturnCode() + "--"
                                + res.getOut().getReturnMessage());
                        if (res.getOut().getReturnCode().equals("F")) {
                            return;
                        }

                        int len = 0;
                        if (res1.getOut().getPolicyDatasList() != null) {
                            if (res1.getOut().getPolicyDatasList().getPolicyData() != null) {
                                if (res1.getOut().getPolicyDatasList().getPolicyData().length > 0) {
                                    len = res1.getOut().getPolicyDatasList().getPolicyData().length;
                                }
                            }
                        }
                        // writeLog.write("51bookZrate",
                        // "当前页数:"+a+".....总条数:"+len);
                        // System.out.println("ien=="+len);

                        for (int b = 0; b < len; b++) {

                            // writeLog.write("51bookZrate",
                            // "当前页数:"+a+".....当前条数:"+b);
                            // index++;
                            // System.out.println("index=="+index);
                            if (res1.getOut().getPolicyDatasList().getPolicyData()[b] != null) {

                                PolicyData aa = res1.getOut().getPolicyDatasList().getPolicyData()[b];
                                if (aa != null) {

                                    try {
                                        Zrate zrate = new Zrate();
                                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                                        zrate.setCreateuser("51BOOK");
                                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                                        zrate.setModifyuser("51BOOK");
                                        zrate.setAgentid(5l);
                                        zrate.setTickettype(1);
                                        zrate.setIsenable(1);
                                        zrate.setOutid(aa.getId() + "");
                                        if (aa.getAirlineCode() != null) {
                                            zrate.setAircompanycode(aa.getAirlineCode());
                                        }
                                        if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                                                && aa.getFlightCourse().indexOf("-") != -1) {
                                            zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
                                            if (zrate.getDepartureport() != null
                                                    && zrate.getDepartureport().equals("999")) {
                                                if (aa.getDepartureExclude() != null) {
                                                    zrate.setDepartureexclude(aa.getDepartureExclude());// 出发不适用
                                                }
                                            }
                                            zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达

                                            if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                                                if (aa.getArrivalExclude() != null) {
                                                    zrate.setArrivalexclude(aa.getArrivalExclude());// 到达不适用
                                                }
                                            }

                                        }
                                        if (aa.getCommision() > 0) {
                                            zrate.setRatevalue(aa.getCommision());
                                        }
                                        if (aa.getSeatClass() != null && aa.getSeatClass().length() >= 1) {
                                            zrate.setCabincode(aa.getSeatClass());
                                        }
                                        if (aa.getFlightNoIncluding() != null && aa.getFlightNoIncluding().length() > 3) {
                                            zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                                        }
                                        if (aa.getFlightNoExclude() != null && aa.getFlightNoExclude().length() > 3) {
                                            zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                                        }
                                        if (aa.getFlightCycle() != null) {
                                            zrate.setSchedule(aa.getFlightCycle());// 航班周期
                                            // 1234567
                                        }
                                        if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                                            zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                                        }
                                        if (aa.getPrintTicketStartDate() != null
                                                && aa.getPrintTicketStartDate().getTime() != null) {
                                            zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate()
                                                    .getTime().getTime()));
                                        }
                                        if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                                            zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                                        }
                                        if (aa.getPrintTicketExpiredDate() != null
                                                && aa.getPrintTicketExpiredDate().getTime() != null) {
                                            zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime()
                                                    .getTime())); // 政策有效期结束时间

                                        }
                                        if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                                            zrate.setTickettype(2);
                                        }
                                        else {
                                            zrate.setTickettype(1);
                                        }
                                        if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程

                                            zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返

                                        }
                                        else {
                                            zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返

                                        }
                                        if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                                && aa.getWorkTime().indexOf("-") != -1) {
                                            String worktime = aa.getWorkTime().split("-")[0];
                                            zrate.setWorktime(worktime);
                                            zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);

                                        }
                                        if (aa.getAgencyEfficiency() != null) {
                                            zrate.setSpeed(aa.getAgencyEfficiency());
                                        }
                                        if (aa.getBusinessUnitType() != null) {
                                            // System.out.println("类型=="+aa.getBusinessUnitType());
                                            if (aa.getBusinessUnitType().equals("0")) {// =0 普通政策 =1
                                                // 特殊政策

                                                zrate.setGeneral(1l);
                                                zrate.setZtype("1");
                                            }
                                            else {

                                                zrate.setGeneral(2l);// 1,普通 2高反
                                                zrate.setZtype("2");
                                            }

                                        }
                                        else {
                                            // System.out.println("类型==null");
                                            zrate.setGeneral(1l);
                                            zrate.setZtype("1");
                                        }
                                        zrate.setUsertype("1");
                                        if (aa.getAgencyEfficiency() != null) {
                                            zrate.setSpeed(aa.getAgencyEfficiency());
                                        }
                                        if (aa.getComment() != null) {
                                            zrate.setRemark(aa.getComment());

                                        }
                                        try {
                                            if (zrate.getRatevalue() != null) {
                                                DBUtil.insertZrate(zrate);
                                            }
                                        }
                                        catch (RuntimeException e) {
                                            UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                                            e.printStackTrace();
                                        }
                                        zrate = null;
                                    }
                                    catch (Exception e) {
                                        UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }

                    System.out.println("*********************提取完成××××××××××××××××××××××××");

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = Calendar.getInstance();
                    String startDate = sdf.format(calendar.getTime());

                    String lasteZrateID = "1";// 最后条记录ID
                    String lastUpdateTime = startDate + " 00:00:00";// 最后次更新时间

                    String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime
                            + "' WHERE C_NAME='51ZRATETIME'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

                    String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID
                            + "' WHERE C_NAME='51ZRATEID'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

                    // 设置时间开始,设置成正在更新中
                    String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                    Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
                    // 设置结束时间

                    WriteLog.write("51bookZrate", "全取完成,lastUpdateTime:" + lastUpdateTime + ",lasteZrateID:"
                            + lasteZrateID);

                }
                else {
                    System.out.println("*********************51book接口被禁用××××××××××××××××××××××××");
                }

            }
            catch (Exception e) {
                UtilMethod.writeEx(this.getClass().getSimpleName(), e);
                System.out.println("51book赋值异常：");
                e.printStackTrace();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                String startDate = sdf.format(calendar.getTime());
                String lasteZrateID = "1";// 最后条记录ID
                String lastUpdateTime = startDate + " 00:00:00";// 最后次更新时间
                String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

                String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
                Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

                // 设置时间开始,设置成正在更新中
                String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
                Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
                // 设置结束时间
            }
        }
        catch (Exception e) {
            UtilMethod.writeEx(this.getClass().getSimpleName(), e);
            System.out.println("^^^^^^^51book异常^^^^^^^^^");
            e.printStackTrace();
            WriteLog.write("51bookZrateQuanQuERRER", e.getMessage());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            String startDate = sdf.format(calendar.getTime());

            String lasteZrateID = "1";// 最后条记录ID
            String lastUpdateTime = startDate + " 00:00:00";// 最后次更新时间

            String updsql = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lastUpdateTime + "' WHERE C_NAME='51ZRATETIME'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql, null);

            String updsql2 = " UPDATE T_B2BSEQUENCE SET C_VALUE='" + lasteZrateID + "' WHERE C_NAME='51ZRATEID'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql2, null);

            // 设置时间开始,设置成正在更新中
            String updsq = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='51ISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsq, null);
            // 设置结束时间

        }
    }
}
