package com.ccservice.inter.job.train;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.CancelFreeAccountThread;
import com.ccservice.inter.job.train.thread.MyThreadVerification;
import com.ccservice.inter.server.Server;

/**
 * 定时访问12306，保持12306账号在线，并将已通过人数同步到数据库
 * @time 2014年8月30日 上午11:40:17
 * @author yinshubin
 * 
 * 增加如果超过45分钟没有支付取消订单
 * @time 2015年1月7日17:31:11
 * @author chendong
 * 
 * 增加如果超过45分钟没有确认改签取消改签
 * @time 2015年3月2日14:52:25
 * @author WH
 */

/**
 * ！！！！！！！！！！此类弃用，所有逻辑已迁移！！！！！！！！！！
 * @author WH
 * @time 2016年10月26日 上午9:18:43
 * @version 1.0
 */

public class Job12306Verification extends TrainSupplyMethod implements Job {

    public static void main(String[] args) {
        Long l1 = System.currentTimeMillis();
        //        new Job12306Verification().verification12306();
        //        System.out.println("Job12306CheckIsenable:耗时:" + (System.currentTimeMillis() - l1));
        try {
            Job12306Verification.startScheduler("0/2 0/1 7-23 * * ?");
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 说明:定时访问12306,保持12306账号在线,并将已通过人数同步到数据库
     * @time 2014年8月30日 上午11:40:29
     * @author yinshubin
     */
    public void verification12306() {
        int r1 = new Random().nextInt(10000);
        int threadcount = 1;
        int meicicount = 1;//每次处理的账号数量
        String procedureSql = " sp_Customeruser_Job12306Verification_login15Check ";
        String url_cn_service = PropertyUtil.getValue("serviceurl", "train.checkStatus.properties");
        List customerusers = Server.getInstance().getSystemService(url_cn_service)
                .findMapResultByProcedure(procedureSql);
        if (customerusers.size() == 0) {
            System.out.println("没有查到账号");
            return;
        }
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(threadcount);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        for (int i = 0; i < customerusers.size(); i++) {
            Customeruser customeruser = new Customeruser();
            Map map = (Map) customerusers.get(i);
            String loginname = map.get("C_LOGINNAME").toString();
            String logpassword = map.get("C_LOGPASSWORD") == null ? "" : map.get("C_LOGPASSWORD").toString();
            String cardnunber = map.get("C_CARDNUNBER") == null ? "" : map.get("C_CARDNUNBER").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            System.out.println(id + ":开始啦:" + loginname);
            customeruser.setId(id);
            customeruser.setLoginname(loginname);
            customeruser.setLogpassword(logpassword);
            customeruser.setCardnunber(cardnunber);
            String repUrl = "http://localhost:8080/Reptile/traininit";
            repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(true);
            //                        String repUrl = randomIp();
            // 将线程放入池中进行执行
            t1 = new MyThreadVerification(customeruser, repUrl);
            pool.execute(t1);
        }
        pool.shutdown();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        verification12306();//说明:定时访问12306,保持12306账号在线,并将已通过人数同步到数据库            
        try {
            if (!GoAccountSystem()) {
                //                verification12306();//说明:定时访问12306,保持12306账号在线,并将已通过人数同步到数据库            
            }
        }
        catch (Exception e) {
        }
        try {
            //            cancelWAITPAYTrainorder();//取消46分钟未支付的订单并且把账号改为可使用
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            //            cancelWaitConfirmChange();//超过45分钟没有确认改签取消改签
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            //            release_customeruser();//释放当前时间开车的账号
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 超过45分钟没有确认改签取消改签
     */
    @SuppressWarnings({ "rawtypes", "deprecation" })
    private void cancelWaitConfirmChange() {
        Date date = new Date();
        date.setMinutes(date.getMinutes() - 30);
        String timeout = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        //相关ID
        String orderIds = "";
        String changeIds = "";
        //存在改签通过的车票的订单
        String querySql = "select top 60 C_ORDERID, ID from T_TRAINORDERCHANGE with(nolock) where C_TCSTATUS = 4 "
                + "and C_TCISCHANGEREFUND = 0 and C_ISQUESTIONCHANGE = 0 and C_TCCREATETIME < '" + timeout + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(querySql, null);
        int listSize = list == null ? 0 : list.size();
        for (int i = 0; i < listSize; i++) {
            Map map = (Map) list.get(i);
            long changeId = Long.parseLong(map.get("ID").toString());
            long orderId = Long.parseLong(map.get("C_ORDERID").toString());
            //订单ID
            orderIds += orderId + ",";
            //改签ID
            changeIds += changeId + ",";
            //记录日志
            writeRC(orderId, "[取消 - " + changeId + "]超过30分钟未确认，系统自动取消改签，还原车票状态。", "系统", Trainticket.CANTCHANGE, 1);
        }
        //取消改签订单、还原车票状态
        if (changeIds.endsWith(",")) {
            //IDS
            changeIds = changeIds.substring(0, changeIds.length() - 1);
            //SQL
            String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + Trainorderchange.CANTCHANGE
                    + ", C_STATUS12306 = " + Trainorderchange.ORDERFALSE + " where ID in (" + changeIds
                    + "); update T_TRAINTICKET set C_STATUS = " + Trainticket.ISSUED + ", C_STATE12306 = "
                    + Trainticket.ORDEREDPAYED + " where C_CHANGEID in (" + changeIds + ") and C_STATUS = "
                    + Trainticket.THOUGHCHANGE;
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        }
        //释放账号
        if (orderIds.endsWith(",")) {
            orderIds = orderIds.substring(0, orderIds.length() - 1);
            //订单账号
            String accountSql = "select ISNULL(C_SUPPLYACCOUNT, '') C_SUPPLYACCOUNT, ordertype "
                    + "from T_TRAINORDER with(nolock) where ID in (" + orderIds + ")";
            List accountList = Server.getInstance().getSystemService().findMapResultBySql(accountSql, null);
            //账号长度
            int accountListSize = accountList == null ? 0 : accountList.size();
            //账号系统
            boolean GoAccountSystem = GoAccountSystem();
            //线程池
            ExecutorService pool = GoAccountSystem && accountListSize > 0 ? Executors
                    .newFixedThreadPool(accountListSize) : null;
            //循环账号
            String accountNames = "";
            try {
                for (int i = 0; i < accountListSize; i++) {
                    Map tempMap = (Map) accountList.get(i);
                    String supplyaccount = tempMap.get("C_SUPPLYACCOUNT").toString();
                    String ordertype = tempMap.get("ordertype") == null ? "" : tempMap.get("ordertype").toString();
                    if (!ElongHotelInterfaceUtil.StringIsNull(supplyaccount) && !"3".equals(ordertype)
                            && !"4".equals(ordertype)) {
                        supplyaccount = supplyaccount.split("/")[0];
                        accountNames += "'" + supplyaccount + "',";
                        //账号系统
                        if (GoAccountSystem) {
                            pool.execute(new CancelFreeAccountThread(supplyaccount));
                        }
                    }
                }
            }
            catch (Exception e) {
            }
            finally {
                if (pool != null) {
                    pool.shutdown();
                }
            }
            if (!GoAccountSystem && accountNames.endsWith(",")) {
                accountNames = accountNames.substring(0, accountNames.length() - 1);
                String updateSql = "UPDATE T_CUSTOMERUSER SET C_ENNAME = '2', C_STATE = 0 WHERE C_LOGINNAME in ("
                        + accountNames + ")";
                Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
            }
        }
    }

    /**
     * 取消46分钟未支付的订单并且把账号改为可使用
     * 
     * @time 2014年12月31日 下午1:58:27
     * @author chendong
     */
    /**
     * 取消46分钟未支付的订单并且把账号改为今天不可使用
     * 
     * @time 2015年4月8日 下午6:24:43
     * @author chendong
     */
    @SuppressWarnings({ "deprecation", "rawtypes" })
    private void cancelWAITPAYTrainorder() {
        Long l1 = System.currentTimeMillis();
        int r1 = new Random().nextInt(10000);
        Date date = new Date();
        date.setMinutes(date.getMinutes() - 35);
        String t_time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        String trainorderstatus1 = "SELECT TOP 60 ID, C_SUPPLYACCOUNT, ordertype FROM T_TRAINORDER WITH (NOLOCK) "
                + "WHERE C_ORDERSTATUS=1 " + "AND (C_ISQUESTIONORDER =0 OR C_ISQUESTIONORDER=2) AND C_STATE12306=4 "
                + "AND C_EXTORDERCREATETIME<'" + t_time + "' ";
        WriteLog.write("Job12306Verification_cancelWAITPAYTrainorder", r1 + ":" + trainorderstatus1);
        List list = Server.getInstance().getSystemService()
                .findMapResultSortBySql(trainorderstatus1, "ORDER BY ID ", null);
        int listSize = list == null ? 0 : list.size();
        WriteLog.write("Job12306Verification_cancelWAITPAYTrainorder", r1 + ":超时未支付的订单数量:" + listSize + "==");
        //线程池
        boolean GoAccountSystem = GoAccountSystem();//账号系统
        ExecutorService pool = GoAccountSystem && listSize > 0 ? Executors.newFixedThreadPool(listSize) : null;
        String idstring = "";
        String supplyaccountstring = "";
        try {
            for (int i = 0; i < listSize; i++) {
                Map lists_map = (Map) list.get(i);
                String supplyaccount = (String) lists_map.get("C_SUPPLYACCOUNT");
                BigDecimal bigdecimal = (BigDecimal) lists_map.get("ID");
                String ordertype = lists_map.get("ordertype") == null ? "" : lists_map.get("ordertype").toString();
                if (supplyaccount != null && !"3".equals(ordertype) && !"4".equals(ordertype)) {
                    supplyaccount = supplyaccount.split("/")[0];
                    supplyaccountstring += "'" + supplyaccount + "'";
                    if (i != listSize - 1) {
                        supplyaccountstring += ",";
                    }
                    if (GoAccountSystem) {
                        pool.execute(new CancelFreeAccountThread(supplyaccount));
                    }
                }
                idstring += bigdecimal;
                if (i != listSize - 1) {
                    idstring += ",";
                }
                Trainorderrc rc = new Trainorderrc();
                rc.setContent("超过30分钟未支付,系统自动取消订单");
                rc.setCreateuser("系统");//创建者
                rc.setOrderid(bigdecimal.longValue());
                rc.setStatus(8);
                rc.setTicketid(0);
                rc.setYwtype(1);
                Server.getInstance().getTrainService().createTrainorderrc(rc);
            }
        }
        catch (Exception e) {

        }
        finally {
            if (pool != null) {
                pool.shutdown();
            }
        }
        String sql1 = "UPDATE T_CUSTOMERUSER SET C_ENNAME=2,C_STATE=0 WHERE C_LOGINNAME in(" + supplyaccountstring
                + ")"; //enname 1和空代表可用，0代表使用中，2代表已经取消三次今日不可用
        String sql2 = "UPDATE T_TRAINORDER SET C_ORDERSTATUS=8 WHERE ID in(" + idstring + ")";
        int i1 = 0;
        int i2 = 0;
        if (listSize > 0) {
            try {
                if (GoAccountSystem) {
                    i1 = listSize;
                }
                else {
                    i1 = Server.getInstance().getSystemService().excuteGiftBySql(sql1);
                }
            }
            catch (Exception e) {
            }
            try {
                i2 = Server.getInstance().getSystemService().excuteGiftBySql(sql2);
            }
            catch (Exception e) {
            }
        }
        WriteLog.write("Job12306Verification_cancelWAITPAYTrainorder", r1 + i1 + ":sql1:" + sql1);
        WriteLog.write("Job12306Verification_cancelWAITPAYTrainorder", r1 + i2 + ":sql2:" + sql2);
        WriteLog.write("Job12306Verification_cancelWAITPAYTrainorder",
                r1 + ":处理超时未支付的订单耗时:" + (System.currentTimeMillis() - l1));
    }

    /**
     * 释放当前时间开车的账号
     * 
     * @time 2015年3月13日 下午3:44:59
     * @author chendong
     */
    private void release_customeruser() {
        Long l1 = System.currentTimeMillis();
        Date date = new Date();
        date.setMinutes(date.getMinutes() + 119);
        String t_time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        String sql = "SELECT TOP 100 ID FROM T_CUSTOMERUSER with(nolock) WHERE C_TYPE=4 AND C_ISENABLE=3 AND C_BIRTHDAY<'"
                + t_time + "'";
        List list = Server.getInstance().getSystemService().findMapResultSortBySql(sql, "ORDER BY ID DESC", null);
        WriteLog.write("Job12306Verification_release_customeruser", list.size() + ":" + sql);
        WriteLog.write("Job12306Verification_release_customeruser", list.toString());
        String idstring = "";
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String id = map.get("ID").toString();
            idstring += id;
            if (i != list.size() - 1) {
                idstring += ",";
            }
        }
        String sql1 = "";
        int updatecount = 0;
        if (list.size() > 0) {
            sql1 = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=1 WHERE ID in(" + idstring + ") ";
            updatecount = 0;
            try {
                updatecount = Server.getInstance().getSystemService().excuteGiftcatalogBySql(sql1);
            }
            catch (Exception e) {
            }
        }
        WriteLog.write("Job12306Verification_release_customeruser", "耗时:" + (System.currentTimeMillis() - l1) + ":"
                + updatecount + ":" + sql1);
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306Verification", "Job12306VerificationGroup",
                Job12306Verification.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306Verification", "Job12306VerificationGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
