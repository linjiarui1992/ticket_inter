package com.ccservice.inter.job.train.reg;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.Jmail;
import com.ccservice.inter.server.Server;

/**tc000153953@mlzg99.com
 * 自动注册12306账号
 * 开始自动注册12306账号
 * 账号，密码，E-MAIL
 * @time 2014年12月14日 下午3:57:21
 * @author chendong
 * @author yinshubin
 */

public class Job12306Registration_JihuoThread extends Thread {

    /**
     * 
     */
    public Job12306Registration_JihuoThread(ISystemService isystemservice) {
        this.isystemservice = isystemservice;
    }

    private int idx;

    private long id;

    private int jihuoleixing;//1后激活(wh),2注册后激活

    public Job12306Registration_JihuoThread(int idx, long id, ISystemService isystemservice) {
        this.id = id;
        this.idx = idx;
        this.jihuoleixing = 1;
        this.isystemservice = isystemservice;
    }

    String result;

    String email;

    String email_ip;

    String logname;

    Customeruser customeruser;

    String mailname;

    ISystemService isystemservice;

    int r1;

    String useProxy;

    String proxyHost;

    String proxyPort;

    /**
     * @param result
     * @param email
     * @param email_ip
     * @param logname
     * @param r1
     * @param customeruser
     * @param mailname
     * @param isystemservice
     * @param proxyPort 
     * @param proxyHost 
     * @param useProxy 
     * @param r1 
     */
    public Job12306Registration_JihuoThread(String result, String email, String email_ip, String logname,
            Customeruser customeruser, String mailname, ISystemService isystemservice, String useProxy,
            String proxyHost, String proxyPort) {
        this.result = result;
        this.email = email;
        this.email_ip = email_ip;
        this.logname = logname;
        this.customeruser = customeruser;
        this.mailname = mailname;
        this.isystemservice = isystemservice;
        this.jihuoleixing = 2;
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    public void run() {
        if (this.jihuoleixing == 2) {//后来新增的线程激活方法
            tojihuo(result, email, email_ip, logname, this.r1, customeruser, mailname);
        }
        else if (this.jihuoleixing == 1) {//王宏的线程激活方法
            ISystemService isystemservice = Server.getInstance().getSystemService();
            Customeruser customeruser = Server.getInstance().getMemberService().findCustomeruser(id);
            if (customeruser == null || customeruser.getIsenable() == null
                    || customeruser.getIsenable().intValue() != 9) {
                return;
            }
            String loginName = customeruser.getLoginname();
            String email = customeruser.getMemberfax();
            String[] emails = email.split("@");
            String email_ip = PropertyUtil.getValue(emails[1], "train.properties");
            if (ElongHotelInterfaceUtil.StringIsNull(email_ip)) {
                String sql = "update T_CUSTOMERUSER set C_ISENABLE = -9 where ID = " + id;
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
                System.out.println(sql);
                return;
            }
            tojihuo(loginName + "|" + email, email, email_ip, loginName, idx, customeruser, emails[0]);
        }
    }

    /**
     * 发送激活的方法
     * 返回激活次数
     * @param str
     * @return
     * @time 2014年12月28日 下午1:26:30
     * @author chendong
     */
    public int sendjihuourl(String str, int r1) {
        int sendsuccess = 0;
        String resulthtml = "-1";
        if (str.contains("activeAccount")) {
            try {
                for (int i = 0; i < 10; i++) {
                    if ("1".equals(useProxy)) {
                        resulthtml = HttpsUtil.getProxy(str, "", "utf-8", this.proxyHost,
                                Integer.parseInt(this.proxyPort));
                    }
                    else {
                        resulthtml = HttpsUtil.get(str, "", "utf-8");
                    }
                    WriteLog.write("12306jihuohtml", r1 + ":" + resulthtml);
                    //                    Thread.sleep(10000);
                    if (resulthtml.indexOf("账户已成功激活") > 0) {
                        sendsuccess++;
                        break;
                    }
                    else {
                        try {
                            Thread.sleep(1000L);
                        }
                        catch (Exception e) {
                        }
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sendsuccess;
    }

    /**
     * 注册成功后去激活
     * 
     * @param result
     * @param email
     * @param email_ip
     * @param logname
     * @param r1
     * @param customeruser
     * @param mailname
     * @time 2015年5月7日 下午3:02:51
     * @author chendong
     * @param isystemservice 
     */
    public void tojihuo(String result, String email, String email_ip, String logname, int r1,
            Customeruser customeruser, String mailname) {
        String email_ip_temp_1 = email.split("@")[1];
        String email_ip_temp = PropertyUtil.getValue(email_ip_temp_1, "train.properties");
        if (email_ip_temp != null) {
            email_ip = email_ip_temp;
        }
        System.out.println(r1 + "--->激活前的信息:" + result);
        String[] results = result.split("[|]");
        String newlogname = results[0];
        String newemail = results[1];
        email = newemail;
        logname = newlogname;
        String jihuourl = getjihuoyouxiangUrl(email_ip, email, logname, r1);//激活成功后把这个激活链接放到Memberfax里customeruser改成可用
        System.out.println(r1 + "--->激活前的url:" + jihuourl);
        int success = sendjihuourl(jihuourl, r1);
        System.out.println(r1 + "--->访问url激活的结果:" + success);
        WriteLog.write("Job12306Registration_JihuoThread", r1 + ":激活结果:" + success + ":" + logname + ":" + email);
        String sqlupdatecus = "";
        if (success > 0) {
            sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=18,C_MEMBERFAX='" + email + "',C_LOGINNAME='"
                    + newlogname + "',C_TICKETNUM=" + customeruser.getTicketnum() + ",C_MEMBERMOBILE='"
                    + customeruser.getMembermobile() + "',c_membername='" + customeruser.getMembername()
                    + "',C_LINKOTHER='" + jihuourl + "', C_CREATETIME = '" + ElongHotelInterfaceUtil.getCurrentTime()
                    + "',C_MOBILE='" + customeruser.getMobile() + "' WHERE ID=" + customeruser.getId();//激活成功后把这个customeruser改成可用
        }
        else {
            sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_LINKOTHER='" + jihuourl + "',C_ISENABLE=9,C_MEMBERFAX='"
                    + email + "' WHERE ID=" + customeruser.getId();//未获取到激活邮件，来日再激活
        }
        int recount = 0;
        try {
            recount = isystemservice.excuteGiftBySql(sqlupdatecus);
            //            recount = Server.getServer().getSystemService().excuteGiftBySql(sqlupdatecus);
            //            如果注册成功把把哪台vps注册的编码发送到监控服务器便于监控哪个vps出问题了
            String vpsReg12306CountUrl = PropertyUtil.getValue("vpsReg12306CountUrl", "train.properties");
            SendPostandGet.submitGet(vpsReg12306CountUrl);
        }
        catch (Exception e) {
            // TODO: handle exception
        }
        System.out.println(r1 + ":激活后修改数据库=>" + recount + ":" + sqlupdatecus);
        WriteLog.write("Job12306Registration_JihuoThread", r1 + ":激活后修改数据库:" + recount + ":" + sqlupdatecus);
        if ("1".equals(this.useProxy)) {
            WriteLog.write("Job12306Registration_JihuoThread_proxy", r1 + "#" + this.proxyHost + ":" + this.proxyPort
                    + "#" + ":激活后修改数据库:" + recount + ":" + sqlupdatecus);
        }
        if (success > 0) {
            try {
                //                new Job12306Registration_JihuoThread_DeleteMail(mailname, email_ip).start();
                new Job12306Registration_JihuoThread_DeleteMail().deleteMethod(mailname, email_ip);
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * 获取激活邮箱里的12306的激活链接
     * 
     * @param mail_ip 邮箱服务IP
     * @param email 邮箱地址
     * @param loginame 12306用户名
     * @return
     * @time 2014年12月14日 下午5:59:40
     * @author chendong
     */
    private String getjihuoyouxiangUrl(String mail_ip, String email, String loginame, int r1) {
        String mailname = email.split("@")[0];
        String str = "-1";
        for (int i = 0; i < 8; i++) {
            Long sleepTime = 20000L;
            if (i > 0) {
                sleepTime = 5000L;
            }
            try {
                Thread.sleep(sleepTime);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                str = Jmail.getMail(mailname, "123456", mail_ip, 10000L);
            }
            catch (Exception e) {
            }
            if (str.startsWith("https")) {
                break;
            }
            //            String str = JMail.getMail(email.replace("@51dzp.com", ""), "123456");
            WriteLog.write("Job12306Registration_JihuoThread", r1 + ";获取" + i + "次(" + loginame + ":" + email
                    + ")邮箱激活地址:" + str);
        }
        return str;
    }
}
