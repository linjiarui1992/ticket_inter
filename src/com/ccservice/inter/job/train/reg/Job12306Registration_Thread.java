package com.ccservice.inter.job.train.reg;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.Server;
import com.weixin.util.RequestUtil;

/**tc000153953@mlzg99.com
 * 自动注册12306账号
 * 开始自动注册12306账号
 * 账号，密码，E-MAIL
 * @time 2014年12月14日 下午3:57:21
 * @author chendong
 * @author yinshubin
 */

public class Job12306Registration_Thread extends Thread {

    private static final int ZhongWu = 1;

    private static final int YeBan = 2;

    private static final int XiuXi = 3;

    int regIdx;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    //一共使用多少个rep
    private int repTotalSize;

    private String rep_url;

    private String email_ip;

    private String email_host;//形如 hangtian123.com.cn

    private List<String> reg_urls;

    //rep错误总次数

    String useProxy;

    String proxyHost;

    String proxyPort;

    public Job12306Registration_Thread(String rep_url, String useProxy) {
        this.rep_url = rep_url;
        this.useProxy = useProxy;
    }

    public Job12306Registration_Thread(List<String> reg_urls, String useProxy, String proxyHost, String proxyPort) {
        super();
        this.reg_urls = reg_urls;
        this.repTotalSize = this.reg_urls.size();
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    public Job12306Registration_Thread(String reg_rep_urls, String useProxy, String proxyHost, String proxyPort) {
        this.rep_url = reg_rep_urls;
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    @Override
    public void run() {
        //计数
        //        int idx = 0; 
        //循环
        //        String damaRuleUrl = "http://hthyservice.hangtian123.net:808/dama18.html";
        String damaRuleUrl = PropertyUtil.getValue("Job12306Registration_damaRuleUrl", "train.properties");
        while (true) {
            int ri = new Random().nextInt(1000000);
            WriteLog.write("Job12306Registration_Thread_debug", ri + ":start while " + TimeUtil.gettodaydate(5));
            try {
                main_execute_while(damaRuleUrl, ri);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 
     * @time 2015年8月25日 下午5:49:49
     * @author chendong
     * @param damaRuleUrl 
     * @param ri 
     * @param reg_urls 
     * @param proxyPort 
     * @param proxyHost 
     * @param useProxy 
     * @param idx 
     * @param size 
     */
    public void main_execute_while(String damaRuleUrl, int ri) {
        try {
            String config = getConfig(damaRuleUrl);//开@2.5秒一次打码
            String[] array = config.split("@");
            String array0 = array[0];
            String array1 = array[1];
            WriteLog.write("Job12306Registration_Thread_debug", ri + ":config:" + Arrays.toString(array));
            //休息，不打码
            if (TimeControl(XiuXi) || !"1".equals(array0)) {
                long xiuxitime = 10 * 1000;
                System.out.println("TimeControl(XiuXi):" + TimeControl(XiuXi) + ":array0:" + array0);
                System.out.println(TimeUtil.gettodaydate(5) + ":xiuxitime:" + xiuxitime);
                Thread.sleep(xiuxitime);
            }
            else {
                if (regIdx > 0) {
                    Long SleepTime = Long.parseLong(array1);
                    WriteLog.write("Job12306Registration_Thread_debug", ri + ":SleepTime0:" + SleepTime);
                    try {
                        Thread.sleep(SleepTime);
                    }
                    catch (InterruptedException e) {
                        WriteLog.write("Job12306Registration_Thread_debug",
                                ri + ":InterruptedException:" + Arrays.toString(e.getStackTrace()));
                        e.printStackTrace();
                    }
                    catch (Exception e) {
                        WriteLog.write("Job12306Registration_Thread_debug",
                                ri + ":Exception:" + Arrays.toString(e.getStackTrace()));
                        e.printStackTrace();
                    }
                }
                WriteLog.write("Job12306Registration_Thread_debug", ri + ":SleepTime:0:over");
                //加一次
                regIdx++;
                toGoReg(ri);
            }
        }
        catch (Exception e) {
            WriteLog.write("Job12306Registration_Thread_debug", ri + ":e:" + Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        }
    }

    /**
     * 
     * @time 2015年9月7日 上午8:57:30
     * @author chendong
     * @param ri
    * @param array1  睡眠多长时间
     */
    public void toGoReg(int ri) {
        WriteLog.write("Job12306Registration_Thread_debug", ri + ":regIdx:" + regIdx);
        //是否使用adsl
        String job12306Registration_useadsl = PropertyUtil.getValue("Job12306Registration_useadsl", "train.properties");
        String proxyHost = "-1";
        String proxyPort = "-1";
        if ("0".equals(job12306Registration_useadsl)) {//如果不使用adsl
            //不使用代理&&不使用adsl
            if ("0".equals(this.useProxy)) {
                Long canUseTime = Server.RegWlfmURLMapLastUseTime.get(this.rep_url);
                if (canUseTime != null && canUseTime > System.currentTimeMillis()) {
                    String systemString1 = "===>===>被封时间还不到2小时";
                    System.out.println(systemString1);
                    return;
                }
            }
            else if ("1".equals(this.useProxy)) {
                String systemString2 = ":>代理总数>" + Server.ProxyIpList.size();
                System.out.println(systemString2);
                System.out.println("==>" + ("1".equals(this.useProxy) ? "使用代理" : "不使用代理") + "=rep_url>" + this.rep_url);
            }
        }
        String systemString = "=>:正常**准备开始扔注册的线程:" + TimeUtil.gettodaydate(4);
        System.out.println(systemString);
        System.out.println("-->>>>" + "rep开啦:" + this.rep_url);
        WriteLog.write("Job12306Registration_Thread_debug", ri + ":SleepTime:over");
        if ("1".equals(job12306Registration_useadsl)) {
            if (Server.adslIaonLine) {//在线
                System.out.println("已经拨号,正常走....");
            }
            else {
                WriteLog.write("Job12306Registration_Thread_debug", ri + ":正在拨号,请稍等。。。:" + TimeUtil.gettodaydate(4));
                System.out.println("正在拨号,请稍等。。。");
                return;
            }
        }
        String resultString = JobTrainUtil.rep0Method(this.rep_url, this.useProxy, proxyHost, proxyPort);
        if (resultString.contains("铁路客户服务中心")) {
            new Job12306Registration_RegThread(this.rep_url, this.useProxy, proxyHost, proxyPort).start();//线程注册
        }
        else if (resultString.contains("网络繁忙") || "-1".equals(resultString)) {//如果被封了这个不使用代理的rep就等2小时在用
            System.out.println("检测情况:0:" + resultString);
            JobTrainUtil.reConnectAdsl();//断开adsl重新连接
            WriteLog.write("Job12306Registration_Thread_debug", ri + ":resultString0:" + resultString);
        }
        else {
            System.out.println("检测情况:1:" + resultString);
            WriteLog.write("Job12306Registration_Thread_debug", ri + ":resultString1:" + resultString);
        }
        WriteLog.write("Job12306Registration_Thread_debug", ri + ":all over:");
    }

    /**
     * 
     * @param damaRuleUrl
     * @return
     * @time 2015年9月6日 下午10:14:08
     * @author chendong
     */
    public String getConfig(String damaRuleUrl) {
        String config = "1@1000";//开@2.5秒一次打码
        //读配置的
        try {
            //            config = RequestUtil.get(damaRuleUrl, "UTF-8", new HashMap<String, String>(), 3000);
            config = SendPostandGet.submitGet(damaRuleUrl);
            //                                config = "4[0],5[0],1[10],7[0]@1@60000";//打码规则和间隔时间
            String[] array = config.split("@");
            config = array[1] + "@" + array[2];
        }
        catch (Exception e) {
            config = "";
        }
        if (config == null || !config.contains("@")) {
            config = "1@1000";
        }
        else {
            String[] array = config.split("@");
            long configTime = Long.parseLong(array[1]);
            if (configTime < 100) {
                configTime = 1000;
            }
            config = array[0] + "@" + configTime;
        }
        return config;
    }

    /**
     * 获取一个可用的代理ip
     * @return
     * @time 2015年8月28日 下午5:23:31
     * @author chendong
     */
    private String getProxyHost_proxyPort() {
        String proxyHost_proxyPort = "-1";
        if (Server.ProxyIpList.entrySet().iterator().hasNext()) {
            proxyHost_proxyPort = Server.ProxyIpList.entrySet().iterator().next().getKey();
            proxyHost_proxyPort = Server.ProxyIpList.remove(proxyHost_proxyPort);
        }
        return proxyHost_proxyPort;
    }

    /**
     * 查询余票
     * @param repUrl
     * @param time
     * @param startcity
     * @param endcity
     * @return
     */
    public static String rep103Method(String repUrl, String time, String startcity, String endcity) {
        String resultString = "";
        String paramContent = "datatypeflag=103&params={\"from_station\":\"" + startcity + "\",\"to_station\":\""
                + endcity + "\",\"queryDate\":\"" + time + "\"}";
        try {
            //        System.out.println(repUrl + ":" + paramContent);
            resultString = SendPostandGet.submitPost(repUrl + "", paramContent, "utf-8").toString();
        }
        catch (Exception e) {
            resultString = e.getLocalizedMessage();
        }
        return resultString;
    }

    //时间控制
    private static boolean TimeControl(int type) {
        String startTime = "";//开始时间
        String endTime = "";//结束时间
        //中午
        if (type == ZhongWu) {
            startTime = "11:59:00";
            endTime = "13:00:00";
        }
        //夜班
        else if (type == YeBan) {
            startTime = "13:58:00";
            endTime = "23:00:00";
        }
        //休息
        else if (type == XiuXi) {
            startTime = "23:40:00";
            endTime = "07:30:00";
        }
        try {
            Date start = sdf.parse(startTime);
            Date end = sdf.parse(endTime);
            //当前
            Date current = sdf.parse(sdf.format(new Date()));
            //判断
            if (type == ZhongWu && current.after(start) && current.before(end)) {
                return true;
            }
            else if (type == YeBan && current.after(start) && current.before(end)) {
                return true;
            }
            else if (type == XiuXi && (current.after(start) || current.before(end))) {
                return true;
            }
        }
        catch (Exception e) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        try {
            Integer.parseInt("a");
        }
        catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
}
