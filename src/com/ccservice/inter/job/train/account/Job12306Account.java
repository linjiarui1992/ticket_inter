/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.MyThreadVerification;
import com.ccservice.inter.server.Server;

/**
 * 手动跑账号
 * @time 2015年9月30日 下午1:50:14
 * @author chendong
 */
public class Job12306Account extends TrainSupplyMethod implements Job {
    public static void main(String[] args) {
        try {
            Job12306Account.startScheduler("0/30 0/1 7-23 * * ?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //        Job12306Account job12306Account = new Job12306Account();
        //        job12306Account.execute();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        execute();
    }

    private void execute() {
        System.out.println(Job12306Account.class + ":execute:" + TimeUtil.gettodaydate(5));
        String serviceurl = "http://121.40.62.200:40000/cn_service/service/";
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        List customerusers = getCustomeruserCheckMobile(isystemservice);
        System.out.println(Job12306Account.class + ":customerusers:" + customerusers.size());
        if (customerusers.size() == 0) {
            System.out.println("没有账号了,等会");
        }
        else {
            // 创建一个可重用固定线程数的线程池
            ExecutorService pool = Executors.newFixedThreadPool(30);
            // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
            Thread t1 = null;
            for (int i = 0; i < customerusers.size(); i++) {
                Map map = (Map) customerusers.get(i);
                t1 = new Job12306AccountThread(map, isystemservice, i);
                pool.execute(t1);
            }
            pool.shutdown();
        }
    }

    /**
     * 获取到n个准备核验的账号
     * @return
     * @time 2015年9月30日 下午3:10:00
     * @author chendong
     * @param isystemservice2 
     */
    private List getCustomeruserCheckMobile(ISystemService isystemservice) {
        String sql = "select top 1000 ID,C_LOGINNAME,C_CREATETIME,C_MEMBERNAME,C_MOBILE,C_CARDTYPE,C_ISENABLE,C_LOGPASSWORD from T_CUSTOMERUSER with(nolock) "
                //                + " where id=13345614 ";
                + " where c_type=4 and C_ISENABLE in (13,14) and C_CREATETIME>'2015-10-01'";
        List customerusers = isystemservice.findMapResultBySql(sql, null);
        return customerusers;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306Account", "Job12306AccountGroup", Job12306Account.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306Account", "Job12306AccountGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
