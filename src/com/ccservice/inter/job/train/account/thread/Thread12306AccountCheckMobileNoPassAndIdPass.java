package com.ccservice.inter.job.train.account.thread;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.ServerUtil;

/**
 * 核验手机号的Thread
 * 
 * @time 2014年12月27日 上午9:52:42
 * @author chendong
 */
public class Thread12306AccountCheckMobileNoPassAndIdPass extends Thread {

    Long id;//customeruser 的id

    String repUrl;

    String loginname;

    String password;

    String serviceurl;

    ISystemService isystemservice;

    public Thread12306AccountCheckMobileNoPassAndIdPass(Long id, String repUrl, String loginname, String password,
            String serviceurl) {
        super();
        this.id = id;
        this.repUrl = repUrl;
        this.loginname = loginname;
        this.password = password;
        this.serviceurl = serviceurl;
        this.isystemservice = ServerUtil.getISystemService(serviceurl);
    }

    String cookieString;

    @Override
    public void run() {

        runMethod();
        try {
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void runMethod() {
        String cookieString = getCookie(loginname, password, repUrl);
        System.out.println(cookieString);
        if ("-1".equals(cookieString)) {
            System.out.println(loginname + ":登录失败");
            return;
        }
        JSONObject jsonObject = JobTrainUtil.initQueryUserInfo(cookieString);//{"shoujihaoma":"15346174493","shoujihaoma_heyanstatus":"未通过","heyanzhuangtai":"未通过"}
        String shoujihaoma = jsonObject.getString("shoujihaoma");
        System.out.println(jsonObject.toJSONString());
        if ("已通过".equals(jsonObject.getString("heyanzhuangtai"))) {//身份核验通过
            String sql = "";
            if ("已通过".equals(jsonObject.getString("shoujihaoma_heyanstatus"))) {
                System.out.println(loginname + ":身份核验通过,手机【通过】,直接修改状态");
                if (shoujihaoma != null && shoujihaoma.length() == 11) {
                    sql = "update T_CUSTOMERUSER set C_ISENABLE=1,C_CARDTYPE=100,C_MOBILE='" + shoujihaoma + "'  where id="
                            + this.id;
                    int count = isystemservice.excuteAdvertisementBySql(sql);
                    System.out.println(loginname + ":count:" + count + ":" + sql);
                }
            }
            else {
                System.out.println(loginname + ":身份核验通过,手机【未通过】,直接修改状态");
                sql = "update T_CUSTOMERUSER set C_ISENABLE=38,C_MOBILE='" + shoujihaoma + "' where id=" + this.id;
                int count = isystemservice.excuteAdvertisementBySql(sql);
                System.out.println(loginname + ":count:" + count + ":" + sql);
            }
        }
        else if ("未通过".equals(jsonObject.getString("heyanzhuangtai"))) {//身份核验未通过
            System.out.println(loginname + ":身份核验【未通过】" + jsonObject.toJSONString());
            String sql = "";
            if ("已通过".equals(jsonObject.getString("shoujihaoma_heyanstatus"))) {
                System.out.println(loginname + ":身份核验【未通过】,手机【通过】,直接修改状态");
                if (shoujihaoma != null && shoujihaoma.length() == 11) {
                    sql = "update T_CUSTOMERUSER set C_ISENABLE=37,C_MOBILE='" + shoujihaoma + "'  where id=" + this.id;
                    int count = isystemservice.excuteAdvertisementBySql(sql);
                    System.out.println(loginname + ":count:" + count + ":" + sql);
                }
            }
            else {
                System.out.println(loginname + ":身份核验【未通过】,手机【未通过】,直接修改状态");
                sql = "update T_CUSTOMERUSER set C_ISENABLE=35,C_MOBILE='" + shoujihaoma + "' where id=" + this.id;
                int count = isystemservice.excuteAdvertisementBySql(sql);
                System.out.println(loginname + ":count:" + count + ":" + sql);
            }

        }
    }

    /**
     * 
     * @param loginname
     * @param password
     * @return
     * @time 2015年10月19日 下午3:19:24
     * @author chendong
     */
    private String getCookie(String loginname, String password, String repUrl) {
        String cookie = "-1";
        for (int i = 0; i < 5; i++) {
            cookie = JobTrainUtil.getCookie(repUrl, loginname, password);
            System.out.println(loginname + ":登陆:" + i + "次:" + cookie);
            if (cookie.contains("失败") || cookie.contains("-1")) {
                continue;
            }
            else {
                break;
            }
        }
        return cookie;
    }

    /**
    * 如果为空返回 "0"
    * 
    * @param object
    * @return
    * @time 2015年4月15日 下午1:57:54
    * @author chendong
    */
    private String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

}
