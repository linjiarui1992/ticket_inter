/**
 * 
 */
package com.ccservice.inter.job.train.temp;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.TimeUtil;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.train.Trainorderrc;

/**
 * 
 * @time 2015年10月16日 下午9:20:07
 * @author chendong
 */
public class JobShenheThread extends Thread {
    public static void main(String[] args) {
        String qunarorderid = "TGT_SF8356414D825099F";
        String ordernumber = "T1510161727181835129";
        int agentid = 47;
        String callBackUrl = "http://121.199.25.199:35216/cn_interface/tcTrainCallBack";
        JobShenheThread jobShenheThread = new JobShenheThread(36464704, new JobShenhe().getISystemservice(),
                qunarorderid, ordernumber, agentid, callBackUrl);
        jobShenheThread.start();
    }

    long id;

    ISystemService isystemservice;

    String qunarorderid;

    String ordernumber;

    int agentid;

    String callBackUrl;

    public JobShenheThread(long id, ISystemService isystemservice, String qunarorderid, String ordernumber,
            int agentid, String callBackUrl) {
        super();
        this.id = id;
        this.isystemservice = isystemservice;
        this.qunarorderid = qunarorderid;
        this.ordernumber = ordernumber;
        this.agentid = agentid;
        this.callBackUrl = callBackUrl;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        for (int i = 0; i < 30; i++) {
            if (isover) {
                break;
            }
            try {
                if (i > 0) {
                    Thread.sleep(10000L);
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(this.id + ":" + i + "次");
            if (isover) {
            }
            else {
                run1();
            }
        }
    }

    boolean isover = false;

    /**
     * 
     * @time 2015年10月16日 下午9:48:50
     * @author chendong
     */
    private void run1() {
        String sql = "select top 15 * from T_TRAINORDERRC where C_ORDERID=" + id + "  order by id desc";
        List list = isystemservice.findMapResultBySql(sql, null);
        int weizhaodaozhifudingdan12306 = 0;
        boolean isfirst = false;
        for (int i = 0; i < list.size(); i++) {
            //            Map map = (Map) list.get(i);
            Map map = (Map) list.get(i);
            String content = map.get("C_CONTENT").toString();
            System.out.println(this.id + ":isover:" + isover + "-->" + content);
            if ("12306上未找到该已支付订单,请客服到12306官网审核支付情况".equals(content)) {//回调出票失败
                weizhaodaozhifudingdan12306++;
                if (i == 0) {
                    isfirst = true;
                }
            }
            else if ("出票:出票成功".equals(content)) {
                break;
            }
        }

        if (weizhaodaozhifudingdan12306 >= 2 || isfirst) {
            int count = isystemservice.excuteEaccountBySql("UPDATE T_TRAINORDER SET C_ORDERSTATUS=8 WHERE ID=" + id);
            if (count > 0) {
                callBackTongChengOrderedFail();
                isover = true;
            }
        }
    }

    /**
     * 空铁系统回调出票失败
     * @param orderid     空铁订单号
     * @param ordernumber 咱们订单号
     * @param agentid     agentid
     **/
    public void callBackTongChengOrderedFail() {
        String result = "false";
        JSONObject jso = new JSONObject();
        jso.put("orderid", this.qunarorderid);
        jso.put("transactionid", this.ordernumber);
        jso.put("method", "train_pay_callback");
        jso.put("agentid", this.agentid);
        jso.put("iskefu", "1");
        jso.put("isSuccess", "N");
        try {
            //            static String kongtieurl = "http://120.26.100.206:39216/cn_interface/tcTrainCallBack";//空铁
            //            static String tongchengurl = "http://121.199.25.199:35216/cn_interface/tcTrainCallBack";//同程
            result = SendPostandGet.submitPost(callBackUrl, jso.toString(), "UTF-8").toString();
            System.out.println(result + "--->" + callBackUrl + "?" + jso.toString());
            if ("success".equals(result)) {
                createTrainorderrc(id, "下单失败-回调成功", "系统-34", 8);
            }
            else {
                createTrainorderrc(id, "下单失败-回调失败", "系统-34", 8);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId 火车票订单id
     * @param content 内容
     * @param createuser 用户
     * @param status 状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderId);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createuser);
        rc.setYwtype(1);

        String sql = "INSERT INTO [dbo].[T_TRAINORDERRC] ([C_ORDERID]"
                + ",C_CREATETIME,[C_CREATEUSER],[C_CONTENT],[C_STATUS],[C_YWTYPE]) VALUES (" + trainorderId + ",'"
                + TimeUtil.gettodaydate(4) + "','" + createuser + "','" + content + "'," + status + ",1)";
        isystemservice.findMapResultBySql(sql, null);
    }
}
