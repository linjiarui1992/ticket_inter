package com.ccservice.inter.job.train;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.net.URLEncoder;
import com.weixin.util.RequestUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 线程_描述超时拒绝的退票，防止退成功，但拒了
 * @author WH
 * @time 2016年12月13日 上午11:55:28
 * @version 1.0
 */

public class AutoScanRefundThread extends Thread {

    //订单ID
    private long orderId;

    //车票数据
    @SuppressWarnings("rawtypes")
    private List<Map> datas;

    //REP超时时间
    private static final int repTimeOut = 30 * 1000;

    //取账号用
    private TrainSupplyMethod supplyMethod = new TrainSupplyMethod();

    //空请求头
    private static final Map<String, String> NullHeader = new HashMap<String, String>();

    //相关参数赋值
    @SuppressWarnings("rawtypes")
    public AutoScanRefundThread(long orderId, List<Map> datas) {
        this.datas = datas;
        this.orderId = orderId;
    }

    @SuppressWarnings("rawtypes")
    public void run() {
        //订单
        Trainorder order = Server.getInstance().getTrainService().findTrainorder(orderId);
        //没有
        if (order == null || order.getId() != orderId) {
            return;
        }
        //车票
        Map<Long, Trainticket> idMap = new HashMap<Long, Trainticket>();
        //乘客
        for (Trainpassenger trainPassenger : order.getPassengers()) {
            //车票
            for (Trainticket trainTicket : trainPassenger.getTraintickets()) {
                //设置
                trainTicket.setTrainpassenger(trainPassenger);
                //添加
                idMap.put(trainTicket.getId(), trainTicket);
            }
        }
        //已发车、未发车
        boolean yiFaChe = false, weiFaChe = false;
        //人名
        String yiFaChePassanger = "", weiFaChePassanger = "";
        //当前时间
        String current = ElongHotelInterfaceUtil.getCurrentDate();
        //票号对应车票
        Map<String, Trainticket> noMap = new HashMap<String, Trainticket>();
        //遍历
        for (int i = 0; i < datas.size(); i++) {
            try {
                //车票数据
                Map map = datas.get(i);
                //车票ID
                long ticketId = Long.parseLong(map.get("TicketId").toString());
                //对应车票
                Trainticket trainTicket = idMap.get(ticketId);
                //非无法退票
                if (trainTicket == null || trainTicket.getStatus() != Trainticket.NONREFUNDABLE) {
                    delete(ticketId);
                }
                //查询12306数据
                else {
                    //发车
                    String departTime;
                    //改签
                    if (trainTicket.getChangeType() == 1 || trainTicket.getChangeType() == 2) {
                        departTime = trainTicket.getTtcdeparttime();
                        noMap.put(trainTicket.getTcticketno(), trainTicket);
                    }
                    else {
                        departTime = trainTicket.getDeparttime();
                        noMap.put(trainTicket.getTicketno(), trainTicket);
                    }
                    //与当前时间判断，发车日期-当前日期
                    if (ElongHotelInterfaceUtil.getSubDays(current, departTime.split(" ")[0]) >= 0) {
                        weiFaChe = true;
                        weiFaChePassanger = trainTicket.getTrainpassenger().getName();
                    }
                    else {
                        yiFaChe = true;
                        yiFaChePassanger = trainTicket.getTrainpassenger().getName();
                    }
                }
            }
            catch (Exception e) {

            }
        }
        //还有数据
        if (yiFaChe || weiFaChe) {
            try {
                //12306
                Map<String, String> order12306Data = order12306Data(order, yiFaChe, weiFaChe, yiFaChePassanger,
                        weiFaChePassanger);
                //遍历车票
                for (String ticket_no : order12306Data.keySet()) {
                    //本地车票
                    Trainticket trainTicket = noMap.get(ticket_no);
                    //无法退票
                    if (trainTicket != null && trainTicket.getStatus() == Trainticket.NONREFUNDABLE) {
                        //ID
                        long ticketId = trainTicket.getId();
                        //SQL
                        String sql = "update T_TRAINTICKET set C_STATUS = " + Trainticket.APPLYTREFUND
                                + ", C_ISAPPLYTICKET = 2, C_APPLYTICKETFLAG = 2 where ID = " + ticketId
                                + " and C_STATUS = " + Trainticket.NONREFUNDABLE;
                        //UPDATE
                        if (Server.getInstance().getSystemService().excuteAdvertisementBySql(sql) > 0) {
                            delete(ticketId);
                        }
                    }
                }
            }
            catch (Exception e) {

            }
        }
    }

    //删除数据
    private void delete(long ticketId) {
        //SQL
        String deleteSql = "[TrainRefuseRefundData] @Method = 2, @IdNumber = " + ticketId;
        //执行
        Server.getInstance().getSystemService().findMapResultByProcedure(deleteSql);
    }

    //12306数据
    private Map<String, String> order12306Data(Trainorder order, boolean yiFaChe, boolean weiFaChe,
            String yiFaChePassanger, String weiFaChePassanger) throws Exception {
        //未登录
        boolean noLogin = false;
        //结果
        Map<String, String> map = new HashMap<String, String>();
        //获取账号
        Customeruser user = supplyMethod.getCustomerUserBy12306Account(order, true);
        //REP数据
        if (yiFaChe) {
            //已发车结果
            map = repData(order, "H", yiFaChePassanger, weiFaChePassanger, user, map);
            //用户未登录
            noLogin = Boolean.valueOf(map.get("noLogin"));
        }
        if (!noLogin && weiFaChe) {
            //未发车结果
            map = repData(order, "G", yiFaChePassanger, weiFaChePassanger, user, map);
            //用户未登录
            noLogin = Boolean.valueOf(map.get("noLogin"));
        }
        //释放账号
        supplyMethod.FreeUserFromAccountSystem(user, noLogin ? AccountSystem.FreeNoLogin : AccountSystem.FreeNoCare,
                AccountSystem.OneFree, AccountSystem.ZeroCancel, AccountSystem.NullDepartTime);
        //返回结果
        return map;
    }

    /**
     * REP数据
     * @author WH
     * @time 2016年12月13日 下午6:28:36
     * @version 1.0
     * @param query_where G：未发车；H：已发车
     */
    private Map<String, String> repData(Trainorder order, String query_where, String yiFaChePassanger,
            String weiFaChePassanger, Customeruser user, Map<String, String> map) throws Exception {
        //REP
        RepServerBean rep = RepServerUtil.getRepServer(user, false);
        //订单列表
        JSONObject listRequest = new JSONObject();
        //请求参数
        listRequest.put("cookie", user.getCardnunber());
        listRequest.put("sequence_no", order.getExtnumber());
        listRequest.put("query_where", query_where);//G：未发车；H：已发车
        listRequest.put("queryType", "1");//1：按订票日期查询；2：按乘车日期查询
        listRequest.put("queryEndDate", ElongHotelInterfaceUtil.getCurrentDate());//结束时间，取当天
        listRequest.put("queryStartDate", order.getCreatetime().toString().split(" ")[0]);//开始时间
        listRequest.put("come_from_flag", "my_order");//查询类型>>my_order：全部；my_resign：可改签； my_refund：可退票
        listRequest.put("passanger_name", "G".equals(query_where) ? weiFaChePassanger : yiFaChePassanger);//乘客姓名，用于关键字查询
        //请求参数
        String param = "datatypeflag=108&jsonStr=" + URLEncoder.encode(listRequest.toString(), "UTF-8")
                + supplyMethod.JoinCommonAccountInfo(user, rep);
        //请求REP
        String html = RequestUtil.post(rep.getUrl(), param, "UTF-8", NullHeader, repTimeOut);
        //用户未登录
        if (html.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
            //切换REP
            rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
            //类型正确
            if (rep.getType() == 1) {
                //重拼参数
                param = "datatypeflag=108&jsonStr=" + URLEncoder.encode(listRequest.toString(), "UTF-8")
                        + supplyMethod.JoinCommonAccountInfo(user, rep);
                //重新请求
                html = RequestUtil.post(rep.getUrl(), param, "UTF-8", NullHeader, repTimeOut);
            }
        }
        //解析数据
        JSONObject obj = JSONObject.parseObject(html);
        //用户未登录
        boolean noLogin = Account12306Util.accountNoLogin(html, user);
        //获取到订单
        if (obj != null && obj.getBooleanValue("success")) {
            //INFO
            String info = obj.getString("order");
            //JSON
            JSONObject OrderDTOData = JSONObject.parseObject(info);
            //车票数组
            JSONArray tickets = OrderDTOData.getJSONArray("tickets");
            //遍历车票
            for (int i = 0; i < tickets.size(); i++) {
                //车票信息
                JSONObject ticket = tickets.getJSONObject(i);
                //车票状态
                String ticket_status_name = ticket.getString("ticket_status_name");
                //已退车票
                if (ticket_status_name.contains("已退票")) {
                    map.put(ticket.getString("ticket_no"), ticket_status_name);
                }
            }
        }
        //用户未登录
        map.put("noLogin", String.valueOf(noLogin));
        //返回结果
        return map;
    }

}