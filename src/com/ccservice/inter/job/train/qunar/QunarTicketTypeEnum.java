package com.ccservice.inter.job.train.qunar;
/**
 * 去哪票类型枚举类
 * @author Administrator
 *
 */
public enum QunarTicketTypeEnum {

	ADULT_TICKET("1", 1, "成人票"), CHILDREN_TICKET("0", 2, "儿童票"), STUDENT_TICKET("2", 3, "学生票"), RESIDUAL_PERIOD("4", 4,
			"残军票");

	private QunarTicketTypeEnum(String qunarTicketTypeStr, int localTicketTypeInt, String text) {
		this.qunarTicketTypeStr = qunarTicketTypeStr;
		this.localTicketTypeInt = localTicketTypeInt;
		this.text = text;
	}

	/**
	 * qunar票类型
	 */
	private String qunarTicketTypeStr;
	/**
	 * 咱们自己的票类型
	 */
	private int localTicketTypeInt;
	/**
	 * 说明
	 */
	private String text;

	public String getQunarTicketTypeStr() {
		return qunarTicketTypeStr;
	}

	public void setQunarTicketTypeStr(String qunarTicketTypeStr) {
		this.qunarTicketTypeStr = qunarTicketTypeStr;
	}

	public int getLocalTicketTypeInt() {
		return localTicketTypeInt;
	}

	public void setLocalTicketTypeInt(int localTicketTypeInt) {
		this.localTicketTypeInt = localTicketTypeInt;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 通过qunar票类型，返回票类型枚举
	 * 
	 * @param qunarTicketTypeStr
	 * @return
	 * @throws NullPointerException
	 */
	public static QunarTicketTypeEnum getQunarTicketTypeEnumByQunar(String qunarTicketTypeStr)
			throws NullPointerException {
		if (qunarTicketTypeStr == null) {
			throw new NullPointerException("qunarTicketTypeStr is null");
		}
		if ("".equalsIgnoreCase(qunarTicketTypeStr)) {
			throw new NullPointerException("qunarTicketTypeStr is empty");
		}
		for (QunarTicketTypeEnum qunarTicketTypeEnum : QunarTicketTypeEnum.values()) {
			if (qunarTicketTypeEnum.getQunarTicketTypeStr().equals(qunarTicketTypeStr)) {
				return qunarTicketTypeEnum;
			}
		}
		throw new NullPointerException("enhave this qunarTicketTypeStr:[" + qunarTicketTypeStr + "]");
	}

	/**
	 * 通过系统票类型，返回票类型枚举
	 * 
	 * @param localTicketTypeInt
	 * @return
	 * @throws NullPointerException
	 */
	public static QunarTicketTypeEnum getQunarTicketTypeEnumByLocal(int localTicketTypeInt)
			throws NullPointerException {
		for (QunarTicketTypeEnum qunarTicketTypeEnum : QunarTicketTypeEnum.values()) {
			if (qunarTicketTypeEnum.getLocalTicketTypeInt() == localTicketTypeInt) {
				return qunarTicketTypeEnum;
			}
		}
		throw new NullPointerException("enhave this localTicketTypeInt:[" + localTicketTypeInt + "]");
	}
}
