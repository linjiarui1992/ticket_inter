package com.ccservice.inter.job.train.thread;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.jms.JMSException;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtilNew;
import com.ccservice.train.qunar.QunarOrderMethod;

public class HandleQunarOrder {
    private String arrStationValue, dptStationValue, extSeatValue, seatValue_Key, orderNoValue, certNoValue,
            certTypeValue, nameValue, seatValue, trainEndTimeValue, trainNoValue, trainStartTimeValue,
            arrStationValue_lc, dptStationValue_lc, extSeatValue_lc, seatValue_lc, trainEndTimeValue_lc,
            trainNoValue_lc, trainStartTimeValue_lc, birthday;

    private float ticketPayValue, seatValue_Value = 0;

    private int ticketTypeValue, seqValue;

    private long qunar_agentid;

    private QunarOrderMethod qunarordermethod;

    private String qunarPayurl;

    public HandleQunarOrder(QunarOrderMethod qunarordermethod, long qunar_agentid, String qunarPayurl) {
        this.qunarordermethod = qunarordermethod;
        this.qunar_agentid = qunar_agentid;
        this.qunarPayurl = qunarPayurl;
    }

    /**
     * 说明：数据库生成订单
     * @param info
     * @time 2014年8月30日 上午11:06:59
     * @author yinshubin
     */
    @SuppressWarnings("unchecked")
    public void getorder(JSONObject info) {
        arrStationValue = info.getString("arrStation");//终点站
        dptStationValue = info.getString("dptStation");//始发站
        extSeatValue = info.getString("extSeat");//备选车票
        if (info.containsKey("jointTrip") == false) {//如果是普通订单,seq:0
            orderNoValue = info.getString("orderNo");//订单号
            seatValue = info.getString("seat");//坐席及其价格
            //                0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
            //               TODO     普通订单的坐席类型
            JSONObject seatObject = JSONObject.parseObject(seatValue);
            Set<String> seatSet = seatObject.keySet();
            for (String str : seatSet) {
                seatValue_Value = (float) seatObject.getDoubleValue(str);//得到票价
                int i_seat = Integer.parseInt(str);
                seatValue_Key = getSeat(i_seat);//得到坐席
            }
            extSeatValue_lc = info.getString("extSeat");//联程备选车票及其价格
            ticketPayValue = (float) info.getDoubleValue("ticketPay");//应付订单票款
            trainEndTimeValue = info.getString("trainEndTime");//火车到达时间
            Date date_trainEndTime = new Date();
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
            try {
                date_trainEndTime = df.parse(trainEndTimeValue);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            long endMilliSeconds = date_trainEndTime.getTime();
            DateFormat dfm = new SimpleDateFormat("HH:mm");
            trainEndTimeValue = dfm.format(date_trainEndTime);
            trainNoValue = info.getString("trainNo");//火车编号
            trainStartTimeValue = info.getString("trainStartTime");//火车出发时间
            Date date_trainStartTime = new Date();
            try {
                date_trainStartTime = df.parse(trainStartTimeValue);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            long startMilliSeconds = date_trainStartTime.getTime();
            int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
            String costTime = String.valueOf(costtime / (60 * 60)) + ":" + String.valueOf((costtime % (60 * 60)) / 60);//历时
            JSONArray passengers = info.getJSONArray("passengers");
            Trainorder trainorder = new Trainorder();//创建订单 
            trainorder.setQunarOrdernumber(orderNoValue);
            List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
            for (int i = 0; i < passengers.size(); i++) {
                List<Trainticket> traintickets = new ArrayList<Trainticket>();
                JSONObject infopassengers = passengers.getJSONObject(i);
                certNoValue = infopassengers.getString("certNo");//乘车人证件号码
                //                    1：身份证；C：港澳通行证；G：台湾通行证；B：护照
                certTypeValue = infopassengers.getString("certType");//乘车人证件种类
                if ("B".equals(certTypeValue)) {
                    certTypeValue = "3";
                }
                else if ("C".equals(certTypeValue)) {
                    certTypeValue = "4";
                }
                else if ("G".equals(certTypeValue)) {
                    certTypeValue = "5";
                }
                if (certNoValue.length() == 18) {
                    String year = certNoValue.substring(6, 10);
                    String month = certNoValue.substring(10, 12);
                    String day = certNoValue.substring(12, 14);
                    birthday = year + "-" + month + "-" + day;
                }
                nameValue = infopassengers.getString("name");//乘车人姓名
                if (infopassengers.containsKey("ticketType")) {//票种类型，1：成人票；0：儿童票(如果是去哪儿的老版本，没有ticketType)
                    ticketTypeValue = Integer.parseInt(infopassengers.getString("ticketType"));
                }
                else {
                    ticketTypeValue = 1;
                }
                //                      TODO 普通订单存入数据库
                Trainticket trainticket = new Trainticket();
                Trainpassenger trainpassenger = new Trainpassenger();
                trainticket.setArrival(arrStationValue);//存入终点站
                trainticket.setDeparture(dptStationValue);//存入始发站
                trainticket.setStatus(Trainticket.WAITISSUE);
                //无法存入剩余票
                trainticket.setSeq(0);//存入订单类型
                trainticket.setTickettype(ticketTypeValue == 0 ? 2 : ticketTypeValue);//存入票种类型
                trainticket.setPayprice(seatValue_Value);
                trainticket.setPrice(seatValue_Value);//应付火车票价格
                trainticket.setArrivaltime(trainEndTimeValue);//火车到达时间
                trainticket.setTrainno(trainNoValue); //火车编号
                trainticket.setDeparttime(trainStartTimeValue);//火车出发时间
                trainticket.setIsapplyticket(1);
                trainticket.setRefundfailreason(0);
                trainticket.setCosttime(costTime);//历时
                trainticket.setSeattype(seatValue_Key);//车票坐席
                if (extSeatValue_lc != null && !extSeatValue_lc.replace("[", "").replace("]", "").equals("")) {
                    trainticket.setTcseatno(extSeatValue);//备选车次及其价格
                }
                trainticket.setInsurenum(0);
                trainticket.setInsurprice(0f);// 采购支付价
                trainticket.setInsurorigprice(0f);// 保险原价
                traintickets.add(trainticket);
                trainpassenger.setIdnumber(certNoValue);//存入乘车人证件号码
                trainpassenger.setIdtype(Integer.parseInt(certTypeValue));//存入乘车人证件种类
                trainpassenger.setName(nameValue);//存入乘车人姓名
                trainpassenger.setBirthday(birthday);
                trainpassenger.setChangeid(0);
                trainpassenger.setAduitstatus(1);
                trainpassenger.setTraintickets(traintickets);
                trainplist.add(trainpassenger);
            }
            trainorder.setOrderprice(ticketPayValue);
            trainorder.setIsjointtrip(0);
            trainorder.setAgentid(qunar_agentid);
            trainorder.setState12306(Trainorder.WAITORDER);
            trainorder.setPassengers(trainplist);
            trainorder.setOrderstatus(Trainorder.WAITISSUE);
            trainorder.setAgentprofit(0f);
            trainorder.setCommission(0f);
            trainorder.setTcprocedure(0f);
            trainorder.setInterfacetype(2);
            trainorder.setCreateuser("去哪儿网");
            trainorder = Server.getInstance().getTrainService().createTrainorder(trainorder);
            WriteLog.write("qunartrainorder", trainorder.getQunarOrdernumber() + "(:)" + trainorder.getId());
            new JobGenerateOrder().exThreadJSP(trainorder.getId());
        }
        else {
            orderNoValue = info.getString("orderNo");//订单号
            ticketPayValue = (float) info.getDoubleValue("ticketPay");//应付票款
            trainEndTimeValue = info.getString("trainEndTime");//火车到达时间
            trainNoValue = info.getString("trainNo");//火车编号
            trainStartTimeValue = info.getString("trainStartTime");//火车出发时间
            Trainorder trainorder = new Trainorder();//创建订单
            trainorder.setQunarOrdernumber(orderNoValue);
            JSONArray passengers = info.getJSONArray("passengers");
            List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
            for (int j = 0; j < passengers.size(); j++) {
                JSONObject infopassengers = passengers.getJSONObject(j);
                certTypeValue = infopassengers.getString("certType");//乘车人证件种类
                if ("B".equals(certTypeValue)) {
                    certTypeValue = "3";
                }
                else if ("C".equals(certTypeValue)) {
                    certTypeValue = "4";
                }
                else if ("G".equals(certTypeValue)) {
                    certTypeValue = "5";
                }
                certNoValue = infopassengers.getString("certNo");//乘车人证件号码
                //                    1：身份证；C：港澳通行证；G：台湾通行证；B：护照
                if (certNoValue.length() == 18) {
                    String year = certNoValue.substring(6, 10);
                    String month = certNoValue.substring(10, 12);
                    String day = certNoValue.substring(12, 14);
                    birthday = year + "-" + month + "-" + day;
                }
                nameValue = infopassengers.getString("name");//乘车人姓名
                if (infopassengers.containsKey("ticketType")) {
                    ;//票种类型，1：成人票；0：儿童票
                    ticketTypeValue = Integer.parseInt(infopassengers.getString("ticketType"));
                }
                else {
                    ticketTypeValue = 1;
                }
                JSONArray jointTripArray = info.getJSONArray("jointTrip");
                List<Trainticket> Traintlist = new ArrayList<Trainticket>();
                for (int i = 0; i < jointTripArray.size(); i++) {
                    JSONObject infojointTrip = jointTripArray.getJSONObject(i);
                    arrStationValue_lc = infojointTrip.getString("arrStation");//联程终点站
                    dptStationValue_lc = infojointTrip.getString("dptStation");//联程始发站
                    extSeatValue_lc = infojointTrip.getString("extSeat");//联程备选车票及其价格
                    seatValue_lc = infojointTrip.getString("seat");//坐席及其价格
                    JSONObject seatObject = JSONObject.parseObject(seatValue_lc);
                    Set<String> seatSet = seatObject.keySet();
                    for (String str : seatSet) {
                        seatValue_Key = str;
                        seatValue_Value = (float) seatObject.getDoubleValue(str);//得到票价
                        int i_seat = Integer.parseInt(str);
                        seatValue_Key = getSeat(i_seat);//得到坐席
                    }
                    seqValue = infojointTrip.getIntValue("seq");//联程订单值。1：第一程；2：第二程
                    trainEndTimeValue_lc = infojointTrip.getString("trainEndTime");//联程火车到达时间
                    Date date_trainEndTime = new Date();
                    DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
                    try {
                        date_trainEndTime = df.parse(trainEndTimeValue_lc);
                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long endMilliSeconds = date_trainEndTime.getTime();
                    DateFormat dfm = new SimpleDateFormat("HH:mm");
                    trainEndTimeValue_lc = dfm.format(date_trainEndTime);
                    trainNoValue_lc = infojointTrip.getString("trainNo");//联程火车编号
                    trainStartTimeValue_lc = infojointTrip.getString("trainStartTime");//联程出发时间
                    Date date_trainStartTime = new Date();
                    try {
                        date_trainStartTime = df.parse(trainStartTimeValue);
                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long startMilliSeconds = date_trainStartTime.getTime();
                    int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
                    String costTime = String.valueOf(costtime / (60 * 60)) + ":"
                            + String.valueOf((costtime % (60 * 60)) / 60);//历时
                    //                          TODO 联程订单存入数据库
                    Trainticket trainticket = new Trainticket();
                    trainticket.setArrival(arrStationValue_lc);//存入终点站
                    trainticket.setDeparture(dptStationValue_lc);//存入始发站
                    trainticket.setStatus(Trainticket.WAITISSUE);
                    //无法存入剩余票
                    trainticket.setSeq(seqValue);//存入订单类型
                    trainticket.setTickettype(ticketTypeValue == 0 ? 2 : ticketTypeValue);//存入票种类型
                    trainticket.setSeattype(seatValue_Key);//车票坐席
                    trainticket.setPayprice(seatValue_Value);
                    trainticket.setPrice(seatValue_Value);//应付火车票价格
                    trainticket.setArrivaltime(trainEndTimeValue_lc);//火车到达时间
                    trainticket.setTrainno(trainNoValue_lc); //火车编号
                    trainticket.setDeparttime(trainStartTimeValue_lc);//火车出发时间
                    trainticket.setCosttime(costTime);
                    trainticket.setIsapplyticket(1);
                    trainticket.setRefundfailreason(0);
                    //                    trainticket.setTcnewprice(0f);
                    trainticket.setInsurenum(0);
                    trainticket.setInsurprice(0f);// 采购支付价
                    trainticket.setInsurorigprice(0f);// 保险原价
                    if (extSeatValue_lc != null && !extSeatValue_lc.replace("[", "").replace("]", "").equals("")) {
                        trainticket.setTcseatno(extSeatValue_lc);//备选车次及其价格
                    }
                    Traintlist.add(trainticket);
                }
                Trainpassenger trainpassenger = new Trainpassenger();
                trainpassenger.setTraintickets(Traintlist);
                trainpassenger.setIdnumber(certNoValue);//存入乘车人证件号码
                trainpassenger.setIdtype(Integer.parseInt(certTypeValue));//存入乘车人证件种类
                trainpassenger.setName(nameValue);//存入乘车人姓名
                trainpassenger.setBirthday(birthday);
                trainpassenger.setChangeid(0);
                trainplist.add(trainpassenger);
            }
            trainorder.setOrderprice(ticketPayValue);
            trainorder.setPassengers(trainplist);
            trainorder.setIsjointtrip(1);
            trainorder.setAgentid(qunar_agentid);
            trainorder.setOrderstatus(Trainorder.WAITISSUE);
            trainorder.setAgentprofit(0f);
            trainorder.setCommission(0f);
            trainorder.setTcprocedure(0f);
            trainorder.setCreateuser("去哪儿网");
            //                trainorder.setPaysupplystatus(0);
            Server.getInstance().getTrainService().createTrainorder(trainorder);
        }
    }

    /**
     * 说明：坐席
     * @param i_seat
     * @return seat
     * @time 2014年8月30日 上午11:07:38
     * @author yinshubin
     */
    public static String getSeat(int i_seat) {
        String str = "";
        switch (i_seat) {
        case 0:
            str = "站票";
            break;
        case 1:
            str = "硬座";
            break;
        case 2:
            str = "软座";
            break;
        case 3:
            str = "一等软座";
            break;
        case 4:
            str = "二等软座";
            break;
        case 5:
            str = "硬卧";
            break;
        case 6:
            str = "硬卧";
            break;
        case 7:
            str = "硬卧";
            break;
        case 8:
            str = "软卧";
            break;
        case 9:
            str = "软卧";
            break;
        case 10:
            str = "高级软卧";
            break;
        case 11:
            str = "高级软卧";
            break;
        case 12:
            str = "特等座";
            break;
        case 13:
            str = "商务座";
            break;
        default:
            break;
        }
        return str;
    }

    /**
     * qunar代付 
     * @return
     * @time 2015年1月21日 下午5:53:36
     * @author fiend
     */
    public boolean qunarPay() {
        boolean isresult = false;
        //        String url = "http://211.103.207.133/cn_interface/qunarTrainPay";
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", this.qunarordermethod.getId());
        jso.put("method", "qunartrain_pay");
        try {
            WriteLog.write("Trainaction_qunarPay", "qunar拉单时调用代付接口:url:" + this.qunarPayurl + ":" + jso.toString());
            String result = SendPostandGet.submitPost(this.qunarPayurl, jso.toString()).toString();
            WriteLog.write("Trainaction_qunarPay", "qunar拉单时调用代付接口:" + result);
            if ("success".equalsIgnoreCase(result)) {
                isresult = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return isresult;
    }

    /**
     * 处理订单 
     * @time 2015年2月2日 下午4:12:44
     * @author fiend
     */
    public void handleTrainOrder() {
        if (this.qunarordermethod.getId() == 0) {//直接存储
            WriteLog.write("QUNAR火车票接口_3.1获取订单", "订单ID:" + this.qunarordermethod.getQunarordernumber() + "===>直接存储");
            getorder(this.qunarordermethod.getOrderjson());
        }
        else {
            if (this.qunarordermethod.getOrderstatus() == Trainorder.CANCLED
                    || this.qunarordermethod.getOrderjson().containsKey("jointTrip")) {//直接拒单
                WriteLog.write("QUNAR火车票接口_3.1获取订单", "订单ID:" + this.qunarordermethod.getQunarordernumber() + "===>直接拒单");
                refuse();
            }
            else if (this.qunarordermethod.getOrderstatus() == Trainorder.WAITPAY) {
                if (iscanpay()) {//直接支付
                    WriteLog.write("QUNAR火车票接口_3.1获取订单", "订单ID:" + this.qunarordermethod.getQunarordernumber()
                            + "===>直接支付");
                    gotoQunarPay();
                }
                else {//修改订单为等待出票 
                    WriteLog.write("QUNAR火车票接口_3.1获取订单", "订单ID:" + this.qunarordermethod.getQunarordernumber()
                            + "===>直接修改状态为等待出票");
                    changeQunarOrder();
                    try {
                        Trainorder trainorder = new Trainorder();
                        trainorder.setId(this.qunarordermethod.getId());
                        try {
                            new TrainpayMqMSGUtilNew().sendPayMQmsgByUrltype(this.qunarordermethod.getId(), 1);
                        }
                        catch (JMSException e) {
                            WriteLog.write("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error",
                                    this.qunarordermethod.getId() + "");
                            ExceptionUtil.writelogByException("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", e);
                        }
                        WriteLog.write("12306_MyThreadQunarOrder_MQ_Pay",
                                ": qunar确认出票 ： " + this.qunarordermethod.getQunarordernumber());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                WriteLog.write("QUNAR火车票接口_异常", "数据库已有此订单且正在处理:" + this.qunarordermethod.getQunarordernumber());
            }
        }
    }

    /**
     * 修改订单为等待出票 
     * @time 2015年2月2日 下午5:09:53
     * @author fiend
     */
    private void changeQunarOrder() {
        writetrainorderrc("qunar接口确定出票", 2);
        String sql_changeorderstatus = "UPDATE T_TRAINORDER SET C_ORDERSTATUS=2 WHERE ID="
                + this.qunarordermethod.getId();
        Server.getInstance().getSystemService().findMapResultBySql(sql_changeorderstatus, null);

    }

    /**
     * 直接支付 
     * @time 2015年2月2日 下午5:01:27
     * @author fiend
     */
    private void gotoQunarPay() {
        if (qunarPay()) {
            String sql = "UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=0,C_STATE12306=5,C_ORDERSTATUS=2,C_SUPPLYTRADENO='qunarpaying',C_SUPPLYPAYWAY=14,"
                    + "C_SUPPLYACCOUNT=C_SUPPLYACCOUNT+'/QUNAR',C_SUPPLYPRICE=C_TOTALPRICE WHERE ID ="
                    + this.qunarordermethod.getId();
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            writetrainorderrc("qunar代付发送成功", 2);
        }
        else {
            String sql = "UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=0,C_STATE12306=4,C_ORDERSTATUS=2,C_SUPPLYTRADENO='qunarpayfalse',C_SUPPLYPAYWAY=14 WHERE ID ="
                    + this.qunarordermethod.getId();
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            writetrainorderrc("qunar代付发送失败", 2);
        }

    }

    /**
     * 是否可以直接调qunar代付 
     * @return
     * @time 2015年2月2日 下午4:50:18
     * @author fiend
     */
    private boolean iscanpay() {
        try {
            if (this.qunarordermethod.getOrderstatus() != Trainorder.WAITPAY) {//订单状态必须是等待支付
                return false;
            }
            if (this.qunarordermethod.getIsquestionorder() != Trainorder.NOQUESTION
                    || this.qunarordermethod.getIsquestionorder() == Trainorder.CAIGOUQUESTION) {//不能是问题订单 或者是采购问题订单
                return false;
            }
            if (this.qunarordermethod.getState12306() != Trainorder.ORDEREDWAITPAY) {//12306状态必须是下单完成等待支付
                return false;
            }
            if (System.currentTimeMillis() > TrainSupplyMethod.getLongTime(this.qunarordermethod.getCreatetime(), 44)) {//订单创建时间必须在44分钟以内
                return false;
            }
        }
        catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * 拒单 
     * @time 2015年2月2日 下午4:22:01
     * @author fiend
     */
    public void refuse() {
        if (isrefuse(0)) {
            Trainorder trainorder = Server.getInstance().getTrainService()
                    .findTrainorder(this.qunarordermethod.getId());
            trainorder.setOrderstatus(Trainorder.REFUSED);
            try {
                Server.getInstance().getTrainService().refusequnarTrain(trainorder, Trainticket.NONISSUEDABLE);
                writetrainorderrc("拒单-无法出票:占座失败订单直接拒单", Trainticket.NONISSUEDABLE);
            }
            catch (Exception e) {
                try {
                    Server.getInstance().getTrainService().refusequnarTrain(trainorder, Trainticket.NONISSUEDABLE);
                    writetrainorderrc("拒单-无法出票:占座失败订单直接拒单", Trainticket.NONISSUEDABLE);
                }
                catch (Exception e1) {
                    updateTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION
                            + ",C_ORDERSTATUS=" + Trainorder.WAITISSUE + " WHERE ID=" + this.qunarordermethod.getId());
                    writetrainorderrc("拒单-无法出票:占座失败订单直接拒单===>数据库修改失败", Trainticket.NONISSUEDABLE);
                }
            }
        }
        else {
            updateTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION
                    + ",C_ORDERSTATUS=" + Trainorder.WAITISSUE + " WHERE ID=" + this.qunarordermethod.getId());
            writetrainorderrc("拒单-失败:请核对qunar后台订单状态", Trainticket.WAITISSUE);
        }
    }

    /**
     * 通过sql修改订单 
     * @param sql
     * @time 2015年2月2日 下午4:32:29
     * @author fiend
     */
    public void updateTrainorder(String sql) {
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    /**
     * 说明:调取出票、拒单接口 
     * @param orderid
     * @param state
     * @return
     * @time 2014年9月1日 下午2:45:39
     * @author yinshubin
     */
    public boolean isrefuse(int state) {
        boolean result = false;
        result = QunarTrainMethod.trainIssueOrRefuse(this.qunarordermethod.getId(), state);
        return result;
    }

    /**
     * 此类公用生成操作记录方法 
     * @param content
     * @param status
     * @time 2015年1月31日 下午3:37:17
     * @author fiend
     */
    private void writetrainorderrc(String content, int status) {
        createTrainorderrc(this.qunarordermethod.getId(), content, "qunar拉单接口", status, 1);
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId 火车票订单id
     * @param content 内容
     * @param createuser 用户
     * @param status 状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status, int ywtype) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderId);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createuser);
        rc.setYwtype(ywtype);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

}
