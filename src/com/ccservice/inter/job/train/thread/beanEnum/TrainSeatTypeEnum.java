package com.ccservice.inter.job.train.thread.beanEnum;

public enum TrainSeatTypeEnum {

    BOX_CAR("其他", "0", "0", "棚车"), HARD_SEAT("硬座", "1", "1", "硬座"), SOFT_SEATS("软座", "2", "2", "软座"), HARD_SLEEPER(
            "硬卧", "3", "3", "硬卧"), SOFT_SLEEPER("软卧", "4", "4", "软卧"), HARD_SLEEPER_BOX("硬卧", "3", "5", "包厢硬卧"), HIGH_GRADE_SOFT_BERTH(
            "高级软卧", "6", "6", "高级软卧"), A_SOFT_SEAT_LOCAL("一等座", "M", "7", "一等软座"), TWO_SOFT_SEAT_LOCAL("二等座", "O", "8",
            "二等软座"), BUSINESS_SEAT("商务座", "9", "9", "商务座"), ADVANCED_DYNAMIC_LIE_LOCAL("高级软卧", "6", "A", "高级动卧"), MIXED_SEAT(
            "硬座", "1", "B", "混编硬座"), MIXED_SLEEPER("硬卧", "3", "C", "混编硬卧"), THE_BOX_SEAT("软座", "2", "D", "包厢软座"), THE_PRINCIPAL_SEAT(
            "特等座", "P", "E", "特等软座"), PNEUMATIC_HORIZONTAL("软卧", "4", "F", "动卧"), TWO_SOFT("软卧", "4", "G", "二人软包"), A_SOFT_BOX(
            "软卧", "4", "H", "一人软包"), A_PAIR_SOFT_BOX("一等座", "M", "I", "一等双软"), TWO_PAIR_SOFT_BOX("二等座", "O", "J",
            "二等双软"), MIXED_SOFT_SEAT("软座", "2", "K", "混编软座"), MIXED_SOFT_SLEEPER("软卧", "4", "L", "混编软卧"), FIRST_CLASS_SEAT(
            "一等座", "M", "M", "一等座"), SECOND_CLASS_SEAT("二等座", "O", "O", "二等座"), SPECIAL_CLASS_SEAT("特等座", "P", "P",
            "特等座"), SIGHTSEEING_BLOCK("其他", "0", "Q", "观光座"), FIRST_CLASS_PACKING_SEAT("一等座", "M", "S", "一等包座");
    /**
     * 私有构造
     * @param seatTypeCreate
     * @param seatCodeCreate
     * @param seatTypeResult
     * @param seatCodeResult
     */
    private TrainSeatTypeEnum(String seatTypeCreate, String seatCodeCreate, String seatCodeResult, String seatTypeResult) {
        this.seatTypeCreate = seatTypeCreate;
        this.seatCodeCreate = seatCodeCreate;
        this.seatCodeResult = seatCodeResult;
        this.seatTypeResult = seatTypeResult;
    }

    /**
     * 通过DB中的坐席，找到对应的枚举
     * 
     * @param seatTypeResult
     * @return
     * @throws NullPointerException
     * @time 2017年5月31日 下午6:05:39
     * @author fiend
     */
    public static TrainSeatTypeEnum getTrainSeatTypeEnumBySeatTypeResult(String seatTypeResult)
            throws NullPointerException {
        if (seatTypeResult == null) {
            throw new NullPointerException("seatTypeDB is null");
        }
        if ("".equals(seatTypeResult.trim())) {
            throw new NullPointerException("seatTypeDB illegal[" + seatTypeResult + "]");
        }
        for (TrainSeatTypeEnum trainSeatTypeEnum : TrainSeatTypeEnum.values()) {
            if (trainSeatTypeEnum.getSeatTypeResult().equalsIgnoreCase(seatTypeResult)) {
                return trainSeatTypeEnum;
            }
        }
        throw new NullPointerException("No such type[" + seatTypeResult + "]");
    }

    /**
     * 通过12306下单成功后的坐席编码，找到对应的枚举
     * 
     * @param seatTypeDB
     * @return
     * @throws NullPointerException
     * @time 2017年5月31日 下午6:05:39
     * @author fiend
     */
    public static TrainSeatTypeEnum getTrainSeatTypeEnumBySeatCodeResult(String seatCodeResult)
            throws NullPointerException {
        if (seatCodeResult == null) {
            throw new NullPointerException("seatCodeResult is null");
        }
        if ("".equals(seatCodeResult.trim())) {
            throw new NullPointerException("seatCodeResult illegal[" + seatCodeResult + "]");
        }
        for (TrainSeatTypeEnum trainSeatTypeEnum : TrainSeatTypeEnum.values()) {
            if (trainSeatTypeEnum.getSeatCodeResult().equalsIgnoreCase(seatCodeResult)) {
                return trainSeatTypeEnum;
            }
        }
        throw new NullPointerException("No such type[" + seatCodeResult + "]");
    }

    /**
     * 12306下单前的坐席名称
     */
    private String seatTypeCreate;

    /**
     * 12306下单前的坐席编码
     */
    private String seatCodeCreate;

    /**
     * 12306下单结果的坐席编码，DB中也会存储这些
     */
    private String seatCodeResult;

    /**
     * 12306下单结果的坐席名称
     */
    private String seatTypeResult;

    public String getSeatTypeCreate() {
        return seatTypeCreate;
    }

    public void setSeatTypeCreate(String seatTypeCreate) {
        this.seatTypeCreate = seatTypeCreate;
    }

    public String getSeatCodeCreate() {
        return seatCodeCreate;
    }

    public void setSeatCodeCreate(String seatCodeCreate) {
        this.seatCodeCreate = seatCodeCreate;
    }

    public String getSeatCodeResult() {
        return seatCodeResult;
    }

    public void setSeatCodeResult(String seatCodeResult) {
        this.seatCodeResult = seatCodeResult;
    }

    public String getSeatTypeResult() {
        return seatTypeResult;
    }

    public void setSeatTypeResult(String seatTypeResult) {
        this.seatTypeResult = seatTypeResult;
    }

}
