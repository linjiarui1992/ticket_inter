/**
 * 
 */
package com.ccservice.inter.job.train.thread;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.Server;

/**
 * 
 * @time 2015年8月28日 下午4:31:40
 * @author chendong
 */
public class Rep0Thread extends Thread {
    String repUrl;

    String useProxy;

    String proxyHost;

    String proxyPort;

    /**
     * @param rep_url
     * @param useProxy
     * @param proxyHost
     * @param proxyPort
     */
    public Rep0Thread(String rep_url, String useProxy, String proxyHost, String proxyPort) {
        this.repUrl = rep_url;
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    @Override
    public void run() {
        String resultString = "-1";
        String proxyHost_proxyPortKey = proxyHost + ":" + proxyPort;
        try {
            resultString = JobTrainUtil.rep0Method(repUrl, useProxy, proxyHost, proxyPort);
        }
        catch (Exception e) {
            resultString = JobTrainUtil.rep0Method(repUrl, useProxy, proxyHost, proxyPort);
        }

        if (resultString.contains("铁路客户服务中心")) {
            Server.ProxyIpList.put(proxyHost_proxyPortKey, proxyHost_proxyPortKey);
            WriteLog.write("Job12306Registration_proxyHost_proxyPortKey_enable", "1@:" + proxyHost_proxyPortKey);
        }
        else if (resultString.contains("网络繁忙")) {
            WriteLog.write("Job12306Registration_proxyHost_proxyPortKey_enable", "0@:" + proxyHost_proxyPortKey);
            Server.ProxyIpList.remove(proxyHost_proxyPortKey);
        }
        else {
            Server.ProxyIpList.remove(proxyHost_proxyPortKey);
        }
        //        System.out.println("Server.ProxyIpList==========>" + Server.ProxyIpList);
    }

    /**
     * java.net实现 HTTP POST方法提交
     * 
     * @param url
     * @param paramContent
     * @return
     */
    private static StringBuffer submitPost(String url, String paramContent, String codetype) {
        StringBuffer responseMessage = null;
        java.net.URLConnection connection = null;
        java.net.URL reqUrl = null;
        OutputStreamWriter reqOut = null;
        InputStream in = null;
        BufferedReader br = null;
        String param = paramContent;
        try {

            // System.out.println("url=" + url + "?" + paramContent + "\n");
            // System.out.println("===========post method start=========");
            responseMessage = new StringBuffer();
            reqUrl = new java.net.URL(url);
            connection = reqUrl.openConnection();
            connection.setDoOutput(true);
            reqOut = new OutputStreamWriter(connection.getOutputStream());
            reqOut.write(param);
            reqOut.flush();
            int charCount = -1;
            in = connection.getInputStream();

            br = new BufferedReader(new InputStreamReader(in, codetype));
            while ((charCount = br.read()) != -1) {
                responseMessage.append((char) charCount);
            }
            // System.out.println(responseMessage);
            // System.out.println("===========post method end=========");
        }
        catch (Exception ex) {
            System.out.println("url=" + url + "?" + paramContent + "\n e=" + ex);
        }
        finally {
            try {
                br.close();
                in.close();
                reqOut.close();
            }
            catch (Exception e) {
                System.out.println("paramContent=" + paramContent + "|err=" + e);
            }
        }
        return responseMessage;
    }
}
