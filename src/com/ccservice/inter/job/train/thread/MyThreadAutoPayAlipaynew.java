package com.ccservice.inter.job.train.thread;

import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class MyThreadAutoPayAlipaynew extends Thread {
    private Trainorder trainorder;

    private String i;

    private int t;

    private String url;

    public void run() {
        long t1 = System.currentTimeMillis();
        WriteLog.write("12306支付宝自动支付", this.t + ":订单号：" + this.trainorder.getOrdernumber() + ",开始支付");
        boolean resull = autoalipayPay(this.trainorder, this.i, this.url, this.t);
        long t2 = System.currentTimeMillis();
        WriteLog.write("12306支付宝自动支付", this.t + ":用时：" + (t2 - t1) + "毫秒:订单号：" + this.trainorder.getOrdernumber()
                + ",支付结束,请求支付结果：" + resull);
    }

    public MyThreadAutoPayAlipaynew(Trainorder trainorder, String i, int t, String url) {
        this.trainorder = trainorder;
        this.i = i;
        this.t = t;
        this.url = url;
    }

    public boolean autoalipayPay(Trainorder trainorder, String i, String url, int t) {
        WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ":对应支付银联地址：" + url);
        String payurl = trainorder.getChangesupplytradeno();
        if ((payurl != null) && (!("".equals(payurl)))) {
            JSONObject jso = new JSONObject();
            jso.accumulate("username", "trainone");
            jso.accumulate("sign", "FF1E07242CE2C86A");
            jso.accumulate("postdata", payurl);
            jso.accumulate("servid", "1");
            jso.accumulate("ordernum", trainorder.getOrdernumber());
            jso.accumulate("paytype", "1");
            try {
                int paycount = 0;
                try {
                    paycount = (int) trainorder.getTcprocedure();
                    if (paycount > 0) {
                        jso.accumulate("RePay", "true");
                        WriteLog.write("12306支付宝自动支付", t + ":" + paycount + "次支付:" + jso.getString("RePay"));
                    }
                }
                catch (Exception e) {
                }
                String supplyaccount = trainorder.getSupplyaccount();
                WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",请求数据：" + jso.toString()
                        + ",支付前支付账号：" + supplyaccount);
                String result = SendPostandGet.submitPostTimeOut(url, jso.toString(), "UTF-8", 30000).toString();
                WriteLog.write("12306支付宝自动支付", t + ":地址：" + url + ",返回结果：" + result);
                if (("".equals(result)) || (!(result.contains("state")))) {
                    trainorder.setState12306(Integer.valueOf(7));
                    trainorder.setIsquestionorder(Integer.valueOf(2));
                    trainorder.setPaysupplystatus(Integer.valueOf(2));
                    try {
                        trainRC(trainorder.getId(), "网络异常");
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    }
                    catch (Exception e) {
                    }
                    return false;
                }
                JSONObject jsoresult = JSONObject.fromObject(result);
                if (jsoresult.getString("state").equals("1")) {
                    try {
                        try {
                            WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "旧的支付账号"
                                    + trainorder.getSupplyaccount());
                            trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/alipayurl" + i);
                            WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "新的支付账号"
                                    + trainorder.getSupplyaccount());
                        }
                        catch (Exception e) {
                            WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "更改支付账号失败");
                            e.printStackTrace();
                        }
                        trainorder.setState12306(Integer.valueOf(5));
                        String now = TimeUtil.gettodaydatebyfrontandback(4, 0);
                        String sql = "update T_TRAINORDER set C_STATE12306=5,C_SUPPLYACCOUNT='"
                                + trainorder.getSupplyaccount() + "' where ID=" + trainorder.getId();
                        sql = sql + ";delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                                + ";insert T_TRAINREFINFO(C_ORDERID,C_SUPPLYPAYTIME) values(" + trainorder.getId()
                                + ",'" + now + "')";
                        WriteLog.write("12306支付宝自动支付", t + ":更新sql：" + sql);
                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",提交自动支付成功");
                        trainRC(trainorder.getId(), "自动支付已经接收支付信息,支付审核待确认");
                    }
                    catch (Exception e1) {
                        WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",支付宝自动支付异常");
                        e1.printStackTrace();
                    }
                }
                else {
                    trainorder.setState12306(Integer.valueOf(7));
                    trainorder.setIsquestionorder(Integer.valueOf(2));
                    trainorder.setPaysupplystatus(Integer.valueOf(2));
                    try {
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "银联支付失败");
                    return false;
                }
            }
            catch (Exception e) {
                WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "银联支付失败");
                return false;
            }
        }
        return false;
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}