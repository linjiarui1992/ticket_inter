package com.ccservice.inter.job.train.thread;

import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.qunar.util.ExceptionUtil;

public class MyThreadQueueCallBack extends Thread {
    private String interfaceOrderNumber;

    private String jsonStr;

    private String tcTrainCallBack;

    public MyThreadQueueCallBack(String interfaceOrderNumber, String tcTrainCallBack, String jsonStr) {
        this.interfaceOrderNumber = interfaceOrderNumber;
        this.tcTrainCallBack = tcTrainCallBack;
        this.jsonStr = jsonStr;
    }

    @Override
    public void run() {
        WriteLog.write("定时回调排队状态", interfaceOrderNumber + ":callback_url:" + tcTrainCallBack + ":" + jsonStr);
        try {
            String result = SendPostandGet.submitPost(tcTrainCallBack, jsonStr, "UTF-8").toString();
            WriteLog.write("定时回调排队状态", interfaceOrderNumber + "--->" + result + "<---");
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("定时回调排队状态_ERROR", e);
        }
    }
}
