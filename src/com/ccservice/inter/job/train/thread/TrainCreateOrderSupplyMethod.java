package com.ccservice.inter.job.train.thread;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jms.JMSException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.ccervice.util.db.DBHelperAccount;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderMessage;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderReturnBean;
import com.ccservice.b2b2c.base.customerpassenger.Customerpassenger;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.insuruser.Insuruser;
import com.ccservice.b2b2c.base.templet.Templet;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.control.TrainPropertyMessage;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.extseat.TrainOrderExtSeatMethod;
import com.ccservice.inter.job.train.rule.Train3151Rule;
import com.ccservice.inter.job.train.rule.TrainPayTimeRule;
import com.ccservice.inter.job.train.train12306.TrainorderLishiMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.mq.producer.method.MQSendMessageMethod;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.account.CookieLogic;
import com.ccservice.train.mqlistener.TrainActiveMQ;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtilNew;
import com.ccservice.train.mqlistener.Method.KongTieRabbtMQMethod;
import com.ccservice.train.mqlistener.Method.OcsMethod;
import com.ccservice.train.rule.BaiDuTrainRule;
import com.ccservice.train.rule.ElongTrainRule;
import com.ccservice.train.rule.MeituanTrainRule;
import com.ccservice.train.rule.QunarTrainRule;
import com.ccservice.train.rule.SeachTrainRule;
import com.ccservice.train.rule.TaobaoTrainRule;
import com.ccservice.train.util.app.TrainAppSDK.Control;
import com.ccservice.train.util.app.TrainAppSDK.IFindMapFromDb;
import com.ccservice.train.util.app.TrainAppSDK.appMethod.BaseClient;

/**
 */
public class TrainCreateOrderSupplyMethod extends TrainSupplyMethod {
    Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    protected String passengers;

    protected Trainorder trainorder;

    protected com.alibaba.fastjson.JSONObject propertys = new com.alibaba.fastjson.JSONObject();

    /**
     * 是否联程票 true 正常订单,false 联程订单
     */
    protected boolean isjointrip;

    protected String cninterfaceurl;

    // protected long qunar_agentid;

    // protected long dangqianjiekouagentid;

    protected int ordermax;

    protected int ordermaxbefore;

    protected int ordermaxerroe;

    /**
     * 查询信息已过期，走手机端次数
     */
    protected int orderMaxQueryRetryPhone = 3;

    //淘宝客人账号订单未登录最大重试
    protected int taobaoNoLoginMax;

    // protected long tongcheng_agentid;

    protected long trainorderid;

    protected String tcTrainCallBack;

    protected String qunarPayurl;

    protected String qunarPayCallBackurl;

    protected String isbudanstr = "占座";

    protected boolean isbudan;// 是否是补单订单

    int r1;

    protected int interfacetype;// 订单所属接口类型

    protected long order_timeout_time = 15 * 60 * 1000l;// 淘宝占座前给后续操作的预留时间

    protected boolean iscanissue = true;//taobao是否可以出票（比对价格）

    protected boolean isnowaitpayorderneedkillcus = false;//排队干掉账号 true 占有账号 false释放

    protected boolean isreturnonline = false;//同城订单涉及拉萨的车次为true 不能在线退票

    protected List<TrainOrderExtSeatMethod> trainOrderExtSeatMethods = new ArrayList<TrainOrderExtSeatMethod>();//备选坐席list

    protected int ordererrorcodenum;//多次打码失败循环次数

    protected int ordererrorcodemax;//多次打码失败循环最大次数

    protected Customeruser userqu;

    protected Customeruser userqunew;

    protected int queueticket = 0;

    protected TrainOrderReturnBean returnobQueue;

    protected final String ORDER_MAX_ERROE_STR = "ordermaxerroe_";

    private int loopAdd2 = 0;

    protected long loopAddSleepTime = 15000L;

    protected int loopAdd2Max = 3;

    protected boolean loopAdd2ChangeAccountFlag = false;

    protected long loopAddSleepAddTime = 15000L;

    protected boolean gaotieShiftOrderType = false;
    //提交用户过多内存存储默认Key   空铁系统就是kt  同城系统就是tc
    String default_pingtaiStr = PropertyUtil.getValue("too_many_users_submit_default_str_", "train.properties");

    //    protected boolean isotherjiekou;//是否是其他接口用户

    JSONArray accountlist = new JSONArray();//该订单所使用过的下单账号清单[同程]

    JSONArray unmatchedpasslist = new JSONArray();//未匹配到有效账号的乘客列表[同程]

    protected long isReCreateOrderSleepTime = 30000L;

    protected long haveBoughtCount = 4L;

    private com.alibaba.fastjson.JSONObject errorAgainMessageJson = new com.alibaba.fastjson.JSONObject();

    private List<String> rules = new ArrayList<String>();

    /**
     * 说明:拒单公用方法
     *
     * @param orderid
     *            火车票订单id
     * @param state
     *            1:所购买的车次坐席已无票 2:身份证件已经实名制购票,不能再次购买同日期同车次的车票 3:qunar票价和12306不符
     * @param user
     * @param returnmsg
     *            同程订单下单失败回调 具体原因
     * @time 2014年9月1日 下午2:45:18
     * @author yinshubin
     */
    public void refuse(long orderid, int state, Customeruser user, String returnmsg) {
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + orderid + ";12306账号:" + user.getLoginname()
                + ":refuse:准备拒单:拒单原因:" + returnmsg);
        try {
            //            下单失败:姜志强@320621199210092813@姜志强的身份信息涉嫌被他人冒用，
            //            为了保障个人信息安全，请您持本人和姜志强的身份证件原件(未成年人可持户口本)
            //            到就近办理客运售票业务的铁路车站完成身份核验，通过后即可在网上办理购票业务，谢谢。
            //            身份信息涉嫌被他人冒用
            if (returnmsg.contains("@待核验") || returnmsg.contains("@证件号码输入有误")) {
                String[] strs = returnmsg.split("@");
                returnmsg = "添加乘客 未通过身份效验 " + strs[0] + strs[1];
            }
            if (returnmsg.contains("的身份信息涉嫌被他人冒用") && returnmsg.contains("@")) {
                returnmsg = returnmsg.split("@")[2];
                String name = returnmsg.split("的身份信息涉嫌被他人冒用")[0];
                returnmsg = name + "冒用，未通过身份核验，请持本人身份证件原件到就近铁路客运车站办理身份核验";
                com.alibaba.fastjson.JSONObject jsonObject_maoyong = getJsonObject_maoyong(returnmsg);
                unmatchedpasslist.add(jsonObject_maoyong);
            }
        }
        catch (Exception e) {
            WriteLog.write("Error_TrainCreateOrder_refuse", "" + orderid);
            ExceptionUtil.writelogByException("Error_TrainCreateOrder_refuse", e);
        }
        WriteLog.write("CP_产品下单失败最终失败原因",
                this.trainorder.getAgentid() + "--->ordertype:" + this.trainorder.getOrdertype() + "--->customeruser:"
                        + user.getLoginname() + "--->" + returnmsg + "--->orderId:" + this.trainorder.getId()
                        + "--->roundSum:最终结果");
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + orderid + ":refuse:Orderstatus():" + trainorder.getOrderstatus() + ":interfacetype:"
                        + this.interfacetype);
        //拒单原因建表  orderid 订单号、trainorder.getAgentid() 商户id 、 trainorder.getOrdertype 订单类型 teturnmsg 拒单信息
        if (saveRefuseMsg(orderid, trainorder.getAgentid(), trainorder.getOrdertype(), returnmsg)) {
            WriteLog.write("OK_save_Refuse_Msg", "Refuse()__orderid:" + orderid + ";state:" + ";returnmsg:" + returnmsg
                    + ";username:" + user.getLoginname());
        }
        else {
            WriteLog.write("ERROR_save_Refuse_Msg", "Refuse()__orderid:" + orderid + ";state:" + ";returnmsg:"
                    + returnmsg + ";username:" + user.getLoginname());
        }
        if ((trainorder.getOrderstatus() == Trainorder.WAITISSUE)
                || (trainorder.getOrderstatus() == Trainorder.WAITPAY)) {
            if (state != 3) {
                trainorder.setState12306(Trainorder.ORDERFALSE);
            }
            trainorder.setRefundreason(state);
            trainorder.setRefuseaffirm(Integer.valueOf(1));
            if (TrainInterfaceMethod.HTHY == this.interfacetype) {// 易定行拒单
                if (this.trainorder.getOrderstatus() == 2) {
                    trainorder.setOrderstatus(Trainorder.REFUSENOREFUND);
                    trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    Trainorderrc rcrefund = new Trainorderrc();
                    rcrefund.setStatus(Trainticket.NONISSUEDABLE);
                    rcrefund.setContent("拒单-无法出票:" + returnmsg);
                    // rcrefund.setContent("拒单-无法出票:" + refusereason(state));
                    rcrefund.setCreateuser(user.getLoginname());
                    rcrefund.setOrderid(trainorder.getId());
                    rcrefund.setYwtype(1);
                    try {
                        Server.getInstance().getTrainService().createTrainorderrc(rcrefund);
                    }
                    catch (Exception e) {
                        WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:拒单无法出票:" + returnmsg);
                    }
                    Server.getInstance().getTrainService().refuseTrain(orderid, user, this.cninterfaceurl);
                }
                else if (this.trainorder.getOrderstatus() == 1) {
                    trainorder.setOrderstatus(Trainorder.CANCLED);
                    trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    createTrainorderrc(trainorder.getId(), "下单失败自动取消订单:" + returnmsg, "系统", 8);
                }
                insertTrain_Order_Seat(this.trainorder, false);
            }
            else if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                    || TrainInterfaceMethod.MEITUAN == this.interfacetype//美团拒单
                    || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                    || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                    || TrainInterfaceMethod.YILONG1 == this.interfacetype
                    || TrainInterfaceMethod.YILONG2 == this.interfacetype) {// 同程拒单
                if (checkOrderStatusMethod(this.trainorder.getId(), user.getLoginname())) {
                    return;
                }
                createTrainorderrc(trainorder.getId(), returnmsg, "12306", Trainticket.WAITISSUE);
                trainorder.setOrderstatus(Trainorder.CANCLED);// 先改状态,再调接口
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                returnTongcheng(trainorder, returnmsg, user, false);
                insertTrain_Order_Seat(this.trainorder, false);
            }
            else if (TrainInterfaceMethod.QUNAR == this.interfacetype) {
                if (trainorder.getOrderstatus() == Trainorder.WAITPAY) {// 如果订单是等待支付掉去哪儿占座回调接口
                    try {
                        WriteLog.write("TrainCreateOrder_Qunar_占座失败", r1 + ":去哪儿占座回调接口_占座失敗 ==>开始" + ":status:"
                                + trainorder.getOrderstatus());
                        trainorder.setOrderstatus(Trainorder.CANCLED);// 先改状态,再调接口
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        String result = callBackQunarOrdered(trainorder.getId(), returnmsg);
                        if (result != null && !"".equals(result) && !"false".equals(result)
                                && !"success".equalsIgnoreCase(result)) {
                            try {
                                result = JSONObject.fromObject(result).get("msg").toString();
                            }
                            catch (Exception e) {
                            }
                        }
                        WriteLog.write("TrainCreateOrder_Qunar_占座失败", r1 + ":去哪儿占座回调接口_占座失败 ==结束" + ":status:"
                                + trainorder.getOrderstatus() + ":返回结果result:" + result);
                        createTrainorderrc(this.trainorderid, "占座失败==>" + returnmsg + "==>返回结果result==>" + result,
                                user.getLoginname(), 1);
                    }
                    catch (Exception e) {
                        WriteLog.write("TrainCreateOrder_Qunar_占座失败_ERROR", r1 + ":去哪儿占座回调接口_占座失败 ==结束" + ":status:"
                                + trainorder.getOrderstatus());
                        createTrainorderrc(this.trainorderid, "占座失败==>" + returnmsg + "回调异常", user.getLoginname(), 1);
                    }
                }
                else if (trainorder.getOrderstatus() == Trainorder.WAITISSUE) {
                    for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                        JobQunarOrder.changecustomeruser(trainpassenger.getName(), trainpassenger.getIdnumber(), "1");
                    }
                    Trainorderrc rc = new Trainorderrc();
                    WriteLog.write("JobGenerateOrder_MyThread",
                            r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + user.getLoginname()
                                    + ":refuse:准备调用拒单拒单接口");
                    if (isrefuse(trainorder, returnmsg)) {
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ";12306账号:"
                                + user.getLoginname() + ":refuse:拒单成功");
                        trainorder.setOrderstatus(Trainorder.REFUSED);
                        rc.setContent("拒单-无法出票:" + returnmsg);
                        try {
                            Server.getInstance().getTrainService()
                                    .refusequnarTrain(trainorder, Trainticket.NONISSUEDABLE);
                        }
                        catch (Exception e) {
                            try {
                                Server.getInstance().getTrainService()
                                        .refusequnarTrain(trainorder, Trainticket.NONISSUEDABLE);
                            }
                            catch (Exception e1) {
                                rc.setContent("拒单-无法出票:" + returnmsg + ";数据库修改失败");
                                trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                                trainorder.setOrderstatus(Trainorder.WAITISSUE);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            }
                        }
                        rc.setStatus(Trainticket.NONISSUEDABLE);
                        rc.setCreateuser(user.getLoginname());
                    }
                    else {
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单ID:" + trainorder.getId() + ";12306账号:"
                                + user.getLoginname() + ":refuse:拒单失败");
                        trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        rc.setStatus(Trainticket.WAITISSUE);
                        rc.setContent("拒单-失败");
                    }
                    rc.setCreateuser(user.getLoginname());
                    rc.setOrderid(trainorder.getId());
                    rc.setYwtype(1);
                    try {
                        Server.getInstance().getTrainService().createTrainorderrc(rc);
                    }
                    catch (Exception e) {
                        WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:qunar拒单");
                    }
                }
                insertTrain_Order_Seat(this.trainorder, false);
            }
            else if (TrainInterfaceMethod.TAOBAO == this.interfacetype) {
                trainorder.setOrderstatus(Trainorder.CANCLED);// 先改状态,再调接口
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                Trainorderrc rc = new Trainorderrc();
                rc.setStatus(Trainticket.WAITISSUE);
                rc.setContent(returnmsg);
                rc.setCreateuser(user.getLoginname());
                rc.setOrderid(trainorder.getId());
                rc.setYwtype(1);
                try {
                    Server.getInstance().getTrainService().createTrainorderrc(rc);
                }
                catch (Exception e) {
                    WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:淘宝拒单");
                }
                refundTaobao(this.trainorder, user.getLoginname(), returnmsg);
                insertTrain_Order_Seat(this.trainorder, false);
            }
        }
        else {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":ID:" + orderid + ":refuse:状态不对");
        }
    }

    /**
     * @TODO  保存拒单信息
     * <p>
     * @param orderid   订单ID
     * @param agentId   商户id
     * @param ordertype 订单类型
     * @param returnmsg 拒单信息
     * @return
     * <p>
     * @time:2016年4月28日  下午4:54:25
     * <p>
     * @author  fengfh
     */
    private boolean saveRefuseMsg(long orderid, long agentId, int Ordertype, String returnmsg) {
        boolean flag = false;
        String procedure = "exec [dbo].[TrainOrderRefuseMsg_insert] @OrderId=" + orderid + ",@AgentId=" + agentId
                + ",@OrderType=" + Ordertype + ",@Msg='" + returnmsg + "'";

        try {
            int num = Server.getInstance().getTrainService().excuteTrainNoBySql(procedure);
            if (num > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("error_saveRefuseMsg", "addRefuseMsg()_class(TrainCreateOrderSupplyMethod) procedure:"
                    + procedure);
        }
        return flag;
    }

    /**
     * 同程占座回调统一处理方案
     *
     * @param trainorder
     * @param returnmsg
     * @param user
     * @param isordered
     * @time 2015年1月4日 下午2:12:58
     * @author fiend
     */
    public void returnTongcheng(Trainorder trainorder, String returnmsg, Customeruser user, boolean isordered) {
        String str = isordered ? isbudanstr + "成功" : isbudanstr + "失败";
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + user.getLoginname() + ":refuse:准备调用同程:" + isbudanstr
                        + ":结果回调接口====>" + str);
        String callbackordered = "调:" + isbudanstr + ":接口失败";
        if ("占座".equals(isbudanstr)) {
            callbackordered = callBackTongChengOrdered(trainorder, returnmsg);
        }
        else if ("补单成功".equals(str)) {
            if (TrainCreateOrderUtil.isHaveWZ(trainorder)) {
                String WPS = getWPS();
                if (TrainCreateOrderUtil.isThisWP(trainorder, WPS)) {
                    callbackordered = "success";// callBackTongChengBudan(trainorder.getId(),
                    // returnmsg);
                }
                else {
                    callbackordered = callBackTongChengPayedFalse(trainorder);
                    str = "补单失败";
                    isordered = false;
                }
            }
            else {// 订单中有无座,调无法出票
                callbackordered = callBackTongChengPayedFalse(trainorder);
                str = "补单失败";
                isordered = false;
            }
        }
        else if ("补单失败".equals(str)) {
            callbackordered = callBackTongChengPayedFalse(trainorder);
        }

        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":订单ID:" + trainorder.getId() + ";12306账号:" + user.getLoginname() + ":refuse:同程:" + isbudanstr
                        + ":结果回调接口返回====>" + callbackordered);

        if (!"success".equalsIgnoreCase(callbackordered)) {
            trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
            if (!isordered) {
                trainorder.setState12306(Trainorder.ORDERFALSE);
                if ("补单".equals(isbudanstr)) {
                    trainorder.setOrderstatus(Trainorder.WAITISSUE);
                }
                else {
                    trainorder.setOrderstatus(Trainorder.WAITPAY);
                }
            }
            tongchengFalseRc(callbackordered, user.getLoginname(), trainorder, isordered, isbudanstr);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        else {
            tongchengTrueRc(user.getLoginname(), trainorder, isordered, isbudanstr);
            try {
                if ("补单成功".equals(str)) {
                    String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=1 WHERE C_ORDERID =" + trainorder.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    // new
                    // TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
                    //                    toPayMq(trainorder);
                    try {
                        new TrainpayMqMSGUtilNew().sendPayMQmsgByUrltype(this.trainorder.getId(), 1);
                    }
                    catch (JMSException e) {
                        WriteLog.write("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", this.trainorder.getId()
                                + "");
                        ExceptionUtil.writelogByException("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", e);
                    }
                }
                else if ("补单失败".equals(str)) {
                    String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=2 WHERE C_ORDERID =" + trainorder.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    trainorder.setOrderstatus(Trainorder.CANCLED);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 检测订单是否取消
     *
     * @param trainorderId 订单ID
     * @param loginName  帐号用户名
     * @return 取消：true, 其他：false
     * @time 2016年1月26日 下午12:09:43
     * @author wang.cheng.liang
     */
    @SuppressWarnings("rawtypes")
    public boolean checkOrderStatusMethod(long trainorderId, String loginName) {
        String sql = "SELECT C_ORDERSTATUS FROM T_TRAINORDER WITH(NOLOCK) WHERE ID=" + trainorderId;
        try {
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                int orderStatus = Integer.parseInt(map.get("C_ORDERSTATUS").toString());
                if (orderStatus == 8) {
                    createTrainorderrc(trainorder.getId(), "订单已取消", loginName, trainorder.getOrderstatus());
                    return true;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 同程回调占座结果
     *
     * @param i
     * @param orderid
     * @param returnmsg
     *            回调具体内容 占座成功传true
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengOrdered(Trainorder trainorder, String returnmsg) {
        String result = "false";
        String url = this.tcTrainCallBack;
        //错误码
        int returncode = CustomerAccountRealRefused(trainorder, interfacetype, returnmsg);
        //不可返回真实原因
        if (returncode <= 0) {
            returncode = 0;
            returnmsg = changeMsg(returnmsg);
            returnmsg = returnMsgStr(returnmsg);
        }
        //用户未登录判断，针对客人账号
        else if (returncode == 751) {
            returnmsg = "用户未登录";
        }
        String iserror = isTrainorderSystemErrorForCallback(trainorder.getId(), interfacetype);
        if (!"true".equalsIgnoreCase(returnmsg) && !"SUCCESS".equalsIgnoreCase(iserror)) {
            returnmsg = iserror;
        }
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("agentid", trainorder.getAgentid());
        jso.put("trainorderid", trainorder.getId());
        jso.put("method", "train_order_callback");
        jso.put("returnmsg", returnmsg);
        jso.put("returncode", returncode);
        if (isreturnonline) {
            jso.put("refund_online", 1);
        }
        jso.put("unmatchedpasslist", this.unmatchedpasslist);
        try {
            WriteLog.write("JobGenerateOrder_MyThread_callBackTongChengOrdered", r1 + ":callback_url:" + url + ":"
                    + jso.toString());
            result = SendPostandGet.submitPostTimeOutFiend(url, jso.toString(), "UTF-8", 180000).toString();
            if ("".equalsIgnoreCase(result.trim())) {
                result = "false";
            }
            WriteLog.write("JobGenerateOrder_MyThread_callBackTongChengOrdered", r1 + ":" + result);
        }
        catch (Exception e) {
            e.printStackTrace();
            result = "false";
        }
        return result;
    }

    /**
     * 说明:调取出票、拒单接口
     *
     * @param orderid
     * @param state
     * @return
     * @time 2014年9月1日 下午2:45:39
     * @author yinshubin
     */
    public boolean isrefuse(Trainorder trainorder, String msg) {
        boolean result = false;
        result = QunarTrainMethod.trainRefuseByCreateOrder(trainorder, msg);
        return result;
    }

    /**
     * 隐藏下单失败原因，客人账号订单参考CustomerAccountRealRefused()方法
     */
    public String changeMsg(String returnmsg) {
        if ((TrainInterfaceMethod.TONGCHENG == this.interfacetype || TrainInterfaceMethod.MEITUAN == this.interfacetype
                || TrainInterfaceMethod.YILONG1 == this.interfacetype || TrainInterfaceMethod.YILONG2 == this.interfacetype)
                && (returnmsg.contains("秘钥") /*|| returnmsg.contains("多次打码")*/|| returnmsg.contains("插件")
                        || returnmsg.contains("初始化") || returnmsg.contains("存在未完成订单"))) {
            returnmsg = "提交订单失败：没有足够的票!";
        }
        else if ((TrainInterfaceMethod.TONGCHENG == this.interfacetype
                || TrainInterfaceMethod.MEITUAN == this.interfacetype
                || TrainInterfaceMethod.YILONG1 == this.interfacetype || TrainInterfaceMethod.YILONG2 == this.interfacetype)
                && returnmsg.contains("多次打码")) {
            returnmsg = "12306繁忙";
        }
        return returnmsg;
    }

    /**
     * 取消12306订单公共方法
     */
    public String cancel12306Order(Customeruser user, String extnumber, long trainorderid) {
        return cancel12306Order(user, extnumber, trainorderid, true);
    }

    /**
     * 取消12306订单公共方法
     * is2MQ 是否需要取消失败扔MQ
     */
    public String cancel12306Order(Customeruser user, String extnumber, long trainorderid, boolean is2MQ) {
        //结果
        String result = "";
        if (user == null) {
            return result;
        }
        //判断是那个下单途径
        String wayByorderId = new Control().orderWayByorderId(new IFindMapFromDb() {
            @Override
            public List findMapResultByProcedure(String paramString) {
                return Server.getInstance().getSystemService().findMapResultByProcedure(paramString);
            }
        }, trainorderid, "cancel");
        //如果是手机端，走手机端流程
        if ("1".equals(wayByorderId)) {
            return cancel12306OrderPhone(user, extnumber, trainorderid);
        }
        //如果是vps走提供的jar包
        else if ("2".equals(wayByorderId)) {
            JSONObject cancelOrderJson = new JSONObject();
            cancelOrderJson.put("loginName", user.getLoginname());
            cancelOrderJson.put("loginPwd", user.getLogpassword());
            return new BaseClient(1000 * 60).CancelOrder(extnumber, cancelOrderJson.toString(), "");
        }
        //处理
        try {
            //Cookie
            String cookie = user.getCardnunber();
            //非空
            if (!ElongHotelInterfaceUtil.StringIsNull(extnumber) && !ElongHotelInterfaceUtil.StringIsNull(cookie)) {
                //REP
                RepServerBean rep = RepServerUtil.getRepServer(user, false);
                //参数
                String param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                        + trainorderid + JoinCommonAccountInfo(user, rep);
                //请求
                result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                //记录日志
                WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method", trainorderid + "-->" + rep.getUrl() + "-->"
                        + result);
                //未登录
                if (result.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                    //切换REP
                    rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                    //类型正确
                    if (rep.getType() == 1) {
                        //重拼参数
                        param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                                + trainorderid + JoinCommonAccountInfo(user, rep);
                        //重新请求
                        result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                        //记录日志
                        WriteLog.write("TrainCreateOrderSupplyMethod_rep10Method", trainorderid + "-->" + rep.getUrl()
                                + "-->" + result);
                    }
                }
            }
        }
        catch (Exception e) {
        }
        if (is2MQ) {
            if (!result.contains("取消订单成功") && !"无未支付订单".equals(result)) {//取消失败，发MQ
                com.alibaba.fastjson.JSONObject json = new com.alibaba.fastjson.JSONObject();
                json.put("extnumber", extnumber);
                json.put("trainorderid", this.trainorderid);
                json.put("freeAccount", false);
                String activeMQ_url = PropertyUtil.getValue("activeMQ_url", "train.properties");
                String QUEUE_NAME = PropertyUtil.getValue("MQ_CancelOrder", "train.properties");
                WriteLog.write("取消订单失败发mq", json.toString());
                //丢MQ循环取消
                try {
                    new KongTieRabbtMQMethod().cannelOrderMQ(activeMQ_url, QUEUE_NAME, json.toString());
                    //                    ActiveMQUtil.sendMessage(activeMQ_url, QUEUE_NAME, json.toString());
                }
                catch (Exception e) {
                    WriteLog.write("取消订单失败发mq_Exception", this.trainorderid + "");
                    ExceptionUtil.writelogByException("取消订单失败发mq_Exception", e);
                }
            }
        }
        //返回结果
        return result;
    }

    /**
     * 说明:将自动下单成功的订单信息存入数据库
     *
     * @param str
     * @param trainorderid
     * @param loginname
     * @param isjointrip
     * @return 存储结果,电子订单号
     * @time 2014年8月30日 上午11:18:59
     * @author yinshubin
     */
    public String saveOrderInformation(String str, long trainorderid, String loginname, boolean isjointrip,
            TrainOrderReturnBean returnob, Customeruser cus) {
        try {
            JSONObject jsono = JSONObject.fromObject(str);
            if (jsono.has("data")) {
                JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                        JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.toString().contains("待支付")) {// 拿到那个唯一的待支付订单
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid
                                    + ":下单成功,获取待支付订单信息:" + jsonoorderDBList.toString());
                            String sequence_no = jsonoorderDBList.getString("sequence_no");
                            //String order_date = jsonoorderDBList.getString("order_date");
                            Float ticket_total_price_page = Float.valueOf(jsonoorderDBList
                                    .getString("ticket_total_price_page"));
                            String start_time = jsonoorderDBList.getString("start_train_date_page");
                            String arriveDate = conversionTime(returnob.getRuntime(), start_time);
                            String train_code = jsonoorderDBList.getString("train_code_page");
                            String from_station = jsonoorderDBList.getJSONArray("from_station_name_page").get(0)
                                    .toString();
                            String to_station = jsonoorderDBList.getJSONArray("to_station_name_page").get(0).toString();
                            String trainno = "";
                            String traintime = "";
                            String startstation = "";
                            String endstation = "";
                            trainno = trainorder.getPassengers().get(0).getTraintickets().get(0).getTrainno();
                            traintime = trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparttime();
                            startstation = trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparture();
                            endstation = trainorder.getPassengers().get(0).getTraintickets().get(0).getArrival();
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":" + trainorderid + ":train_code:"
                                    + train_code + ":trainno:" + trainno + ":start_time:" + start_time + ":traintime:"
                                    + traintime + ":from_station:" + from_station + ":startstation:" + startstation
                                    + ":to_station:" + to_station + ":endstation:" + endstation + ":arriveDate"
                                    + arriveDate);
                            //是否不需要比对发车时间  , true 不需要比对 false 需要比对  // && this.interfacetype != TrainInterfaceMethod.MEITUAN
                            boolean isEnNeedStarttimeEquils = (this.interfacetype != TrainInterfaceMethod.TAOBAO && this.trainorder
                                    .getOrderstatus() == Trainorder.WAITPAY);//18代表是排队中的订单
                            boolean comparisonTraincode = comparisonTraincode(train_code, trainno);//数据库的车次和12306返回的车次是否一致
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":" + trainorderid + ":train_code:"
                                    + train_code + ":trainno:" + trainno + ":start_time:" + start_time + ":traintime:"
                                    + traintime + ":from_station:" + from_station + ":startstation:" + startstation
                                    + ":to_station:" + to_station + ":endstation:" + endstation
                                    + ":isEnNeedStarttimeEquils:" + isEnNeedStarttimeEquils);
                            //fiend2王成亮 这里时间比对  允许有5分钟时差》》》start_time.equals(traintime) 时间格式去看log
                            if (!comparisonTraincode) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,车次不一致>>>" + train_code + "<<<";
                            }
                            else if (!from_station.replace(" ", "").equals(startstation.replace(" ", ""))) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,出发站不一致>>>" + from_station + "<<<";
                            }
                            else if (!to_station.replace(" ", "").equals(endstation.replace(" ", ""))) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,到达站不一致>>>" + to_station + "<<<";
                            }
                            else if (!isEnNeedStarttimeEquils
                                    && !TrainCreateOrderUtil.thanTimeDifference(start_time, traintime)) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,发车时间不对>>>" + start_time + "<<<";
                            }
                            else if (isEnNeedStarttimeEquils
                                    && !TrainCreateOrderUtil.timeEqualsEachDay(start_time, traintime)) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,发车时间不对>>>" + start_time + "<<<";
                            }
                            //美团要求时间丝毫不差
                            else if (this.interfacetype == TrainInterfaceMethod.MEITUAN
                                    && TrainCreateOrderUtil.isMeiTuan(trainorder.getAgentid())
                                    && !TrainCreateOrderUtil.TimeEnDifference(start_time, traintime)) {
                                cancel12306Order(cus, sequence_no, trainorder.getId());
                                freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                        AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                                return "下单成功,数据库同步失败,发车时间不一致>>>" + start_time + "<<<";
                            }
                            else {
                                if (jsonoorderDBList.has("tickets")) {
                                    //历时
                                    returnob = updatereturnob(returnob, cus);
                                    //运行时间为空
                                    if (returnob != null && "ERROR_运行时间为空".equals(returnob.getRuntime())) {
                                        return "获取历时数据失败";
                                    }
                                    JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                                    updatepassengerfrom12306jsonbypassenger(trainorder.getPassengers(), tickets,
                                            returnob, isjointrip, isEnNeedStarttimeEquils);
                                }
                                WriteLog.write("JobGenerateOrder_MyThread", r1
                                        + ":updatepassengerfrom12306jsonbypassenger:0");
                                if (isjointrip) {
                                    if (trainorder.getExtnumber() == null) {
                                        trainorder.setExtnumber(sequence_no);
                                    }
                                    else {
                                        trainorder.setExtnumber(sequence_no + trainorder.getExtnumber());
                                    }
                                    trainorder.setSupplyaccount(loginname);
                                }
                                else {// 联程票第二段
                                    if (trainorder.getExtnumber() == null) {
                                        trainorder.setExtnumber("," + sequence_no);
                                    }
                                    else {
                                        trainorder.setExtnumber(trainorder.getExtnumber() + "," + sequence_no);
                                    }
                                }
                                if (TrainInterfaceMethod.HTHY != this.interfacetype) {
                                    trainorder.setOrderprice(ticket_total_price_page);
                                }
                                if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                                        || TrainInterfaceMethod.MEITUAN == this.interfacetype
                                        || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                                        || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                                        || TrainInterfaceMethod.TAOBAO == this.interfacetype
                                        || TrainInterfaceMethod.YILONG1 == this.interfacetype
                                        || TrainInterfaceMethod.YILONG2 == this.interfacetype) {// 存入订单总价
                                    // if ((trainorder.getAgentid() ==
                                    // this.tongcheng_agentid &&
                                    // this.dangqianjiekouagentid == 1)
                                    // || isotherjiekou) {//存入订单总价
                                    try {
                                        String sql = "UPDATE T_TRAINORDER SET C_EXTORDERCREATETIME='"
                                                + new Timestamp(System.currentTimeMillis()) + "' WHERE ID ="
                                                + trainorder.getId();
                                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                                    }
                                    catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                // trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                //                                try {
                                //                                    orderpayment(cus);
                                //                                }
                                //                                catch (Exception e) {
                                //                                    e.printStackTrace();
                                //                                }
                                String sql = "";
                                try {
                                    sql = "sp_TrainOrderInfo_updateArriveDate @ArriveDate = '" + arriveDate
                                            + "', @OrderId = '" + trainorder.getQunarOrdernumber() + "'";
                                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                                }
                                catch (Exception e) {
                                    ExceptionUtil.writelogByException(
                                            "TrainCreateOrderSupplyMethod_conversionTime_Exception", e, sql);
                                }
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":updateTrainorder:1");
                                String createuser = "12306";
                                if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                                        || TrainInterfaceMethod.MEITUAN == this.interfacetype
                                        || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                                        || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                                        || TrainInterfaceMethod.YILONG1 == this.interfacetype
                                        || TrainInterfaceMethod.YILONG2 == this.interfacetype) {
                                    // if ((trainorder.getAgentid() ==
                                    // this.tongcheng_agentid &&
                                    // this.dangqianjiekouagentid == 1)
                                    // || isotherjiekou) {
                                }
                                else {
                                    createuser = loginname;
                                }
                                createTrainorderrc(trainorder.getId(), "下单成功,电子单号:" + sequence_no, createuser,
                                        trainorder.getOrderstatus());
                                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid
                                        + ":下单成功,更改订单信息信息:" + trainorder.getId() + "(:)" + sequence_no);
                                return "请尽快支付" + sequence_no;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("JobGenerateOrder_MyThread_error", e);
            WriteLog.write("JobGenerateOrder_MyThread_error", r1 + ":订单号:" + trainorderid + ":下单成功,数据库同步失败");
        }
        cancel12306Order(cus, trainorder.getExtnumber(), trainorder.getId());
        freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                AccountSystem.NullDepartTime);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":订单号:" + trainorderid + ":下单成功,数据库同步失败");
        return "下单成功,数据库同步失败";
    }

    /**
     * 进行日期的换算
     * @time:2017年8月15日上午10:53:53
     * @auto:baozz
     * @param runtime
     * @param start_time
     * @return
     */
    public String conversionTime(String runtime, String start_time) {
        String format = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(simpleDateFormat.parse(start_time));
            calendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(runtime.split(":")[0]));
            calendar.add(Calendar.MINUTE, Integer.parseInt(runtime.split(":")[1]));
            format = simpleDateFormat.format(calendar.getTime());
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_conversionTime_Exception", e,
                    "进行日期转换时出现异常==>历时时间为" + runtime + ",开始时间为");
        }
        return format;
    }

    /**
     * 更新历时数据
     * @return
     * @time 2016年1月4日 下午5:31:15
     * @author chendong
     * @param returnob
     * @param cus
     */
    private TrainOrderReturnBean updatereturnob(TrainOrderReturnBean returnob, Customeruser cus) {
        if ((this.interfacetype == TrainInterfaceMethod.QUNAR && this.trainorder.getOrderstatus() == Trainorder.WAITPAY)
                || this.interfacetype == TrainInterfaceMethod.TONGCHENG
                || this.interfacetype == TrainInterfaceMethod.MEITUAN
                || this.interfacetype == TrainInterfaceMethod.WITHHOLDING_BEFORE
                || this.interfacetype == TrainInterfaceMethod.WITHHOLDING_AFTER
                || TrainInterfaceMethod.YILONG1 == this.interfacetype
                || TrainInterfaceMethod.YILONG2 == this.interfacetype) {
            try {
                if (returnob == null) {
                    returnob = new TrainOrderReturnBean();
                    if (returnob.getRuntime() == null || "".equals(returnob.getRuntime())) {
                        Trainticket trainticket = this.trainorder.getPassengers().get(0).getTraintickets().get(0);
                        String lishiString = new TrainorderLishiMethod().getTrainby12306(trainorderid,
                                trainticket.getDeparture(), trainticket.getArrival(), trainticket.getDeparttime(),
                                trainticket.getTrainno());
                        returnob.setRuntime(lishiString);
                    }
                }
            }
            catch (Exception e1) {
                ExceptionUtil.writelogByException("Error_TrainCreateOrder_lishi", e1);
            }
            finally {
                if (returnob == null) {
                    returnob = new TrainOrderReturnBean();
                }
            }
            if (ElongHotelInterfaceUtil.StringIsNull(returnob.getRuntime())) {
                getlishiFailMethon(cus);//获取失败了先不执行
                returnob.setRuntime("ERROR_运行时间为空");//固定值，不可修改
                createTrainorderrc(this.trainorderid, "获取历时数据失败", "保存订单信息", 1);
                //                return "获取历时数据失败";
            }
        }
        return returnob;
    }

    /**
     * 获取历时失败的老逻辑
     * @time 2015年12月11日 下午4:21:26
     * @author chendong
     * @param cus
     */
    private void getlishiFailMethon(Customeruser cus) {
        refuse(trainorder.getId(), 1, cus, "提交订单失败：没有足够的票!");//如果数据库同步失败就拒单
        cancel12306Order(cus, trainorder.getExtnumber(), trainorder.getId());
        freeCustomeruser(cus, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                AccountSystem.NullDepartTime);
    }

    /**
     * 重新获取未支付订单
     *
     * @param customeruser
     * @return
     */
    public String getstr(Customeruser user, String passengers) {
        //判断是那个下单途径
        String wayByorderId = new Control().orderWayByorderId(new IFindMapFromDb() {
            @Override
            public List findMapResultByProcedure(String paramString) {
                return Server.getInstance().getSystemService().findMapResultByProcedure(paramString);
            }
        }, trainorderid, "create");
        if ("2".equals(wayByorderId)) {
            return new BaseClient(1 * 60 * 1000).getNoCompleteOrder(user.getLoginname(), user.getLoginname());
        }
        //REP
        RepServerBean rep = RepServerUtil.getRepServer(user, false);
        //地址
        String repUrl = rep.getUrl();
        String isPhone12306 = PropertyUtil.getValue("isPhone12306", "train.properties");
        String joinCommonAccountPhoneStr = "";
        String datatypeflag = "18";
        if ("1".equals(isPhone12306)) {
            datatypeflag = "1018";
            joinCommonAccountPhoneStr = JoinCommonAccountPhone(user);
            return "";
        }
        //参数
        String par = "datatypeflag=" + datatypeflag + "&cookie=" + user.getCardnunber() + "&passengers=" + passengers
                + "&trainorderid=" + this.trainorder.getId() + JoinCommonAccountInfo(user, rep)
                + joinCommonAccountPhoneStr;
        WriteLog.write("JobGenerateOrder_MyThread_getstr", r1 + ":订单号:" + this.trainorder.getId()
                + ":获取未完成订单(问题订单使用),调取TrainInit:" + repUrl + "(:)" + par);
        String infodata = "";
        try {
            infodata = SendPostandGet.submitPostTimeOutFiend(repUrl, par, "UTF-8", 1 * 60 * 1000).toString();
            //用户未登录
            if (infodata.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                //切换REP
                rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                //类型正确
                if (rep.getType() == 1) {
                    //REP地址
                    repUrl = rep.getUrl();
                    //重拼参数
                    par = "datatypeflag=" + datatypeflag + "&cookie=" + user.getCardnunber() + "&passengers="
                            + passengers + "&trainorderid=" + this.trainorder.getId()
                            + JoinCommonAccountInfo(user, rep) + joinCommonAccountPhoneStr;
                    //记录日志
                    WriteLog.write("JobGenerateOrder_MyThread_getstr", r1 + ":订单号:" + this.trainorder.getId()
                            + ":获取未完成订单(问题订单使用),changeRepServer:" + repUrl + "(:)" + par);
                    //重新请求
                    infodata = SendPostandGet.submitPostTimeOutFiend(repUrl, par, "UTF-8", 1 * 60 * 1000).toString();
                }
            }
            try {
                if (infodata.contains("\"result\":") && infodata.contains("\"cookie\":")) {
                    com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(infodata);
                    //刷新cookie
                    CookieLogic.getInstance().refresh(infodata, user);
                    infodata = jsonObject.getString("result");
                }
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_getstr_Exception", e, infodata);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("JobGenerateOrder_MyThread_getstr", r1 + ":订单号:" + this.trainorder.getId()
                + ":获取未完成订单(问题订单使用),调取TrainInit:返回:" + infodata);
        return infodata;
    }

    /**
     * 下单成功处理方案
     *
     * @param trainorderid
     * @time 2014年12月26日 上午9:02:06
     * @author fiend
     */
    public void generateSuccess(long trainorderid, Customeruser cuser) {
        trainorder.setIsquestionorder(Trainorder.NOQUESTION);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":generateSuccess:" + this.interfacetype);
        boolean ishave = Account12306Util.haveRegistInfo(cuser);
        WriteLog.write("TrainCreateOrderSupplyMethod", trainorderid + "验证结果为:" + ishave);
        if (ishave) {
            String membersex = cuser.getMembersex();
            WriteLog.write("TrainCreateOrderSupplyMethod", "membersex信息为:" + membersex);
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(membersex);
            String accountRegistName = jsonObject.getString("accountRegistName");
            String accountRegistIDNumber = jsonObject.getString("accountRegistIDNumber");
            String accountRegistMobileNo = (jsonObject.containsKey("accountRegistMobileNo")) ? jsonObject
                    .getString("accountRegistMobileNo") : "";
            try {
                Server.getInstance()
                        .getSystemService()
                        .findMapResultByProcedure(
                                "sp_TrainOrderInfo_insert_RegistInfo @orderId=" + trainorderid
                                        + ",@accountRegistName='" + accountRegistName + "',@accountRegistIDNumber='"
                                        + accountRegistIDNumber + "',@accountRegistMobileNo='" + accountRegistMobileNo
                                        + "'");
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_generateSuccess_Exception", e,
                        "注册信息入口异常");
            }
        }
        //去哪儿订单出票,出票
        if (TrainInterfaceMethod.QUNAR == this.interfacetype) {// 去哪儿订单出票,出票
            if (trainorder.getOrderstatus() == 1) {// 如果订单是等待支付掉去哪儿占座回调接口
                if (ishaveseatthis(trainorder)) {
                    //占座成功，释放REP
                    RepServerUtil.freeRepServerByAccount(cuser);
                    //更新订单
                    try {
                        WriteLog.write("TrainCreateOrder_Qunar_占座成功", r1 + ":去哪儿占座回调接口_占座成功 ==>开始" + ":status:"
                                + trainorder.getOrderstatus());
                        String result = callBackQunarOrdered(trainorder.getId(), "");
                        if (result != null && !"".equals(result) && !"false".equals(result)
                                && !"success".equalsIgnoreCase(result)) {
                            try {
                                result = JSONObject.fromObject(result).get("msg").toString();
                            }
                            catch (Exception e) {
                            }
                        }
                        if (result == null || !"SUCCESS".equalsIgnoreCase(result)) {
                            try {
                                String sql = "UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION
                                        + " WHERE ID =" + trainorder.getId();
                                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        WriteLog.write("TrainCreateOrder_Qunar_占座成功", r1 + ":去哪儿占座回调接口_占座成功 ==结束" + ":status:"
                                + trainorder.getOrderstatus() + ":返回结果result:" + result);
                        createTrainorderrc(this.trainorderid, "占座成功==>返回结果result==>" + result, cuser.getLoginname(), 1);
                    }
                    catch (Exception e) {
                        WriteLog.write("TrainCreateOrder_Qunar_占座成功_ERROR", r1 + ":去哪儿占座回调接口_占座成功 ==结束" + ":status:"
                                + trainorder.getOrderstatus());
                        createTrainorderrc(this.trainorderid, "占座成功==>回调异常", cuser.getLoginname(), 1);
                    }
                    insertTrain_Order_Seat(this.trainorder, true);
                }
                else {
                    boolean iscancel = cancelTrainorder(trainorder.getExtnumber(), cuser, true);
                    String str = "-->取消订单<span style='color:red;'>" + (iscancel ? "成功" : "失败") + "</span>";
                    writeRC("该订单包含<span style='color:red;'>无座</span>且客户不要无座" + str, "自动下单");
                    refuse(trainorderid, 3, cuser, "余票不足");
                    //                    freeCustomeruser(cuser, AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                    //                            AccountSystem.NullDepartTime);
                }
            }
            else if (trainorder.getOrderstatus() == 2) {
                if (isallticketseat(trainorder)) {
                    if (ishaveseatthis(trainorder)) {
                        //Qunar下单成功
                        float differenceprice = getdirrerenceprice(trainorder);
                        if (differenceprice < 0) {
                            //取消订单
                            cancelTrainorder(trainorder.getExtnumber(), cuser, true);
                            //拒绝订单
                            refuse(trainorder.getId(), 3, cuser, "与12306票价不符");
                        }
                        else {
                            boolean success = true;
                            //                            int qunarPayRule = QunarTrainRule.qunarPayRule(this.trainorder);
                            int qunarPayRule = getsp_QunarTrainRule_SELECT(this.trainorder);
                            //代付成功
                            if (!QunarTrainRule.isSysPay(qunarPayRule) && qunarPay()) {
                                //释放账号
                                freeCustomeruser(cuser, AccountSystem.FreeCurrent, AccountSystem.OneFree,
                                        AccountSystem.ZeroCancel, AccountSystem.NullDepartTime);
                                //更新数据库
                                trainorder.setSupplypayway(14);
                                trainorder.setSupplytradeno("qunarpaying");
                                trainorder.setSupplytradeno(trainorder.getSupplytradeno() + "/QUNAR");
                                trainorder.setState12306(Trainorder.ORDEREDPAYING);
                                trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                writeRC("qunar代付发送成功", "qunar代付");
                            }
                            else if (QunarTrainRule.isQunarPayFailRufuse(qunarPayRule)) {
                                success = false;
                                //取消订单
                                boolean iscancel = cancelTrainorder(trainorder.getExtnumber(), cuser, true);
                                String str = "-->12306取消订单<span style='color:red;'>" + (iscancel ? "成功" : "失败")
                                        + "</span>";
                                writeRC("qunar代付<span style='color:red;'>失败，直接拒单</span>" + str, "自动下单");
                                refuse(trainorderid, 3, cuser, "余票不足");
                            }
                            else {
                                //占座成功，释放REP
                                RepServerUtil.freeRepServerByAccount(cuser);
                                //更新订单
                                trainorder.setSupplypayway(14);
                                trainorder.setSupplytradeno("qunarpayfalse");
                                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                writeRC("<span style='color:red;'>qunar代付发送失败==>航天商旅支付</span>", "qunar代付");
                                //                                new TrainpayMqMSGUtil("PayMQ_TrainPay").sendPayMQmsg(trainorder, 1, 0);
                                try {
                                    new TrainpayMqMSGUtilNew().sendPayMQmsgByUrltype(this.trainorder.getId(), 1);
                                }
                                catch (JMSException e) {
                                    WriteLog.write("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error",
                                            this.trainorder.getId() + "");
                                    ExceptionUtil.writelogByException(
                                            "12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", e);
                                }
                            }
                            insertTrain_Order_Seat(this.trainorder, success);
                        }
                        // trainIssue(trainorder,
                        // trainorder.getSupplyaccount().split("/")[0], cuser);
                    }
                    else {
                        boolean iscancel = cancelTrainorder(trainorder.getExtnumber(), cuser, true);
                        String str = "-->12306取消订单<span style='color:red;'>" + (iscancel ? "成功" : "失败") + "</span>";
                        writeRC("该订单包含<span style='color:red;'>无座</span>且客户不要无座" + str, "自动下单");
                        refuse(trainorderid, 3, cuser, "余票不足");
                        //                        freeCustomeruser(cuser, AccountSystem.FreeCurrent, AccountSystem.OneFree,
                        //                                AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                    }
                }
                else {
                    writeRC("因<span style='color:red;'>坐席信息不全</span>,无法直接出票", "自动下单");
                    trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                    trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    //释放账号
                    freeCustomeruser(cuser, AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                            AccountSystem.NullDepartTime);
                }
            }
        }
        //.回调成功接口
        else if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                || TrainInterfaceMethod.MEITUAN == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                || TrainInterfaceMethod.YILONG1 == this.interfacetype
                || TrainInterfaceMethod.YILONG2 == this.interfacetype) {// 同程.回调成功接口
            boolean isCanGenerate = getIsCanGenerate(trainorder, this.interfacetype);//是否拒单 true是  false不   默认不拒单
            WriteLog.write("12306_TrainpayMqMSGUtil_是否拒单", this.trainorder.getId() + "isCanGenerate是否拒单"
                    + isCanGenerate);
            if (isCanGenerate) {
                //票价不符，取消订单并释放账号
                cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                refuse(trainorder.getId(), 3, cuser, "与12306票价不符");
                freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            else {
                //boolean status = ishaveseatthisMeiTuan(trainorder);//留着以后用
                boolean yaowuzuo = true;
                if (TrainInterfaceMethod.YILONG1 == this.interfacetype
                        || TrainInterfaceMethod.YILONG2 == this.interfacetype
                        || TrainInterfaceMethod.MEITUAN == this.interfacetype
                        || TrainInterfaceMethod.TONGCHENG == this.interfacetype) {
                    yaowuzuo = ishaveseatthisMeiTuan(trainorder);
                }
                if (yaowuzuo) {
                    //占座成功，释放REP
                    RepServerUtil.freeRepServerByAccount(cuser);
                    //百度逻辑
                    BaiDuTrainRule baiDuTrainRule = new BaiDuTrainRule();
                    if (baiDuTrainRule.isBaidu(trainorder)) {
                        //锁单成功
                        if (baiDuTrainRule.handleBaiDuOrder(trainorder)) {
                            //更新订单
                            trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                            trainorder.setOrderstatus(Trainorder.WAITISSUE);
                            if (isreturnonline) {
                                trainorder.setEnrefundable(1);
                            }
                            Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            //和淘宝逻辑一致
                            successTaobao(this.trainorder, cuser.getLoginname(), "true");
                            insertTrain_Order_Seat(this.trainorder, true);
                        }
                        else {
                            refuse(trainorderid, interfacetype, cuser, "锁单失败");
                            cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                            //待确定：是否要12306取消订单，释放账号
                            freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                    AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                        }
                    }
                    else {
                        //更新订单
                        trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        boolean guestPay = guestPD(cuser);
                        //TODO 
                        if (guestPay) {
                            returnTongcheng(this.trainorder, "true", cuser, true);
                        }
                        insertTrain_Order_Seat(this.trainorder, true);
                    }
                }
                else {
                    buyaowuzuo(cuser);
                }
            }
        }
        else if (TrainInterfaceMethod.TAOBAO == this.interfacetype) {//淘宝占座成功
            boolean ishaveseat = ishaveseatthisTaoBao(trainorder);
            //淘宝锁单
            boolean status = ishaveseat && new TaobaoTrainRule().handleTaobaoOrder(trainorder);
            //            float differenceprice = getdirrerenceprice(trainorder);
            if (!TaobaoTrainRule.isCanGenerate(trainorder)) {
                //票价不符，取消订单并释放账号
                cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                refuse(trainorder.getId(), 3, cuser, "与12306票价不符");
                freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            else if (!TaobaoTrainRule.isSufficientTime(trainorder)) {
                //距离发车时间不足40分钟，取消订单并释放账号
                cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                refuse(trainorder.getId(), 3, cuser, "出票超时");
                freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            else if (status) {
                //占座成功，释放REP
                RepServerUtil.freeRepServerByAccount(cuser);
                //更新订单
                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                trainorder.setOrderstatus(Trainorder.WAITISSUE);
                if (isreturnonline) {
                    trainorder.setEnrefundable(1);
                }
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                successTaobao(this.trainorder, cuser.getLoginname(), "true");
                insertTrain_Order_Seat(this.trainorder, true);
            }
            else if (!ishaveseat) {
                refuse(trainorderid, interfacetype, cuser, "坐票已售完");//淘宝订单含无座直接拒
                cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                //待确定：是否要12306取消订单，释放账号
                freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            else {
                refuse(trainorderid, interfacetype, cuser, "没有足够的票");//淘宝订单含无座直接拒
                cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
                //待确定：是否要12306取消订单，释放账号
                freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
        }
        else {
            //占座成功，释放REP
            RepServerUtil.freeRepServerByAccount(cuser);
            // 易定行订单只修改12306状态
            if (!isallticketseat(trainorder)) {
                trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
            }
            //            new TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
            try {
                new TrainpayMqMSGUtilNew().sendPayMQmsgByUrltype(this.trainorder.getId(), 1);
                insertTrain_Order_Seat(this.trainorder, true);
            }
            catch (JMSException e) {
                WriteLog.write("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", this.trainorder.getId() + "");
                ExceptionUtil.writelogByException("12306_TrainpayMqMSGUtil_sendOrderPayMQmsgnew_error", e);
            }
        }
    }

    /**
     * 
     * @param cuser
     * @return
     * @time 2017年8月26日 下午7:59:38
     * @author shijialei
     */
    private boolean guestPD(Customeruser cuser) {
        boolean isSuccess = true;
        try {
            String procedure = "[dbo].[sp_TomasPayUrlInfo_selectByOrderId] @OrderId=" + this.trainorder.getId()
                    + ",@ChangeFlag=0";
            List tomasPayUrlList = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
            if ((tomasPayUrlList != null) && (tomasPayUrlList.size() > 0)) {
                Map map = (Map) tomasPayUrlList.get(tomasPayUrlList.size() - 1);
                Map msgMap = new HashMap();
                msgMap.put("orderId", String.valueOf(map.get("OrderId")));
                msgMap.put("changeFlag", String.valueOf(map.get("ChangeFlag")));
                msgMap.put("extnumber", String.valueOf(this.trainorder.getExtnumber()));
                msgMap.put("loginname", String.valueOf(cuser.getLoginname()));
                String msg = JSON.toJSONString(msgMap);
                int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
                MQSendMessageMethod.sendOnemessageUnException(msg, "GuestPayMQ_TrainGetURL", pingtaiType);
                isSuccess = false;
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("客人支付_TrainCreateOrderSupplyMethod_guestPD", e, "客人支付判断异常");
        }
        return isSuccess;
    }

    private void insertTrain_Order_Seat(Trainorder trainorder, boolean success) {
        long end = System.currentTimeMillis();
        long start = trainorder.getCreatetime().getTime();
        double result = (end - start) / 1000.0D;
        String sql = "dbo.Train_Order_Seat_inset @OrderId ='" + trainorder.getId() + "',@Status=" + ((success) ? 1 : 0)
                + "," + "@CreateTime='"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(trainorder.getCreatetime())
                + "',@ConsumptionTime='" + Double.parseDouble(new DecimalFormat("#.00").format(result)) + "'";
        Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
    }

    /**
     * 不要无座
     * @time 2016年1月15日 下午4:39:17
     * @author chendong
     * @param cuser
     */
    private void buyaowuzuo(Customeruser cuser) {
        refuse(trainorderid, interfacetype, cuser, "没有足够的票:不要无座");//美团订单含无座直接拒
        cancel12306Order(cuser, trainorder.getExtnumber(), trainorder.getId());
        //待确定：是否要12306取消订单，释放账号
        freeCustomeruser(cuser, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                AccountSystem.NullDepartTime);
    }

    /**
     * 是否拒单 true 是 false 不   默认不拒单
     * @param trainorder2
     * @param interfacetype2
     * @return
     * @time 2016年1月14日 下午1:42:40
     * @author chendong
     */
    private boolean getIsCanGenerate(Trainorder trainorder2, int interfacetype2) {
        boolean isCanGenerate = false;
        if (MeituanTrainRule.isCanGenerate(trainorder, this.interfacetype)) {//下单成功后是否可以支付规则
        }
        else {
            isCanGenerate = true;
        }
        if (ElongTrainRule.isCanGenerate(trainorder, this.interfacetype)) {//下单成功后是否可以支付规则
        }
        else {
            isCanGenerate = true;
        }
        if (!(new SeachTrainRule().isCanGenerate(this.trainorder, this.interfacetype))) {
            isCanGenerate = true;
        }
        return isCanGenerate;
    }

    /**
     * 获取带超时时间的List<Trainpassenger>
     *
     * @return
     * @time 2015年10月20日 下午2:29:53
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    protected List<Trainpassenger> getTimeOutTrainpassenger() {
        List<Trainpassenger> plistcus = new ArrayList<Trainpassenger>();
        try {
            //获取账号 默认超时时间
            long defultTimeOut = 10 * 60 * 1000;
            //获取账号 默认最大超时时间
            long defultLongTimeOut = 30 * 60 * 1000;
            List list = Server
                    .getInstance()
                    .getSystemService()
                    .findMapResultByProcedure(
                            "[dbo].[sp_TrainOrderTimeOut_TimeOut] @AgentId=" + this.trainorder.getAgentid()
                                    + ",@InterfaceType=" + this.interfacetype + ",@Type=1");
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                defultTimeOut = Long.valueOf(map.get("TimeOut").toString()) * 60 * 1000;
                defultLongTimeOut = Long.valueOf(map.get("TimeOutMax").toString()) * 60 * 1000;
                //如果是淘宝
                if (this.interfacetype == TrainInterfaceMethod.TAOBAO) {
                    //获取淘宝订单超时时间
                    long orderOutTime = this.trainorder.getOrdertimeout().getTime();
                    //如果超时时间没有
                    if (orderOutTime == 0) {
                        plistcus = this.trainorder.getPassengers();
                    }
                    else {
                        //获取当前时间
                        long nowTime = System.currentTimeMillis();
                        //获取账号超时时间点
                        long cusOutTime = 0;
                        //如果订单超时时间-当前时间  > 默认超时时间+5分钟  即可用获取账号超时逻辑
                        if (orderOutTime - nowTime > defultTimeOut + 5 * 60 * 1000) {
                            //如果订单超时时间-当前时间  > 默认最大超时时间   就使用当前时间+最大超时时间 为获取账号超时时间
                            if (orderOutTime - nowTime > defultLongTimeOut) {
                                cusOutTime = System.currentTimeMillis() + defultLongTimeOut;
                            }
                            //如果订单超时时间-当前时间  <= 默认最大超时时间   就使用订单超时时间-默认超时时间 为获取账号超时时间
                            else {
                                cusOutTime = orderOutTime - defultTimeOut;
                            }
                            for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
                                //将获取账号超时时间放入CusOutTime里
                                trainpassenger.setOrderid(cusOutTime);
                                plistcus.add(trainpassenger);
                            }
                        }
                        else {
                            plistcus = this.trainorder.getPassengers();
                        }
                    }
                }
                else {
                    //如果不是淘宝 就当前时间+默认超时时间
                    long cusOutTime = System.currentTimeMillis() + defultTimeOut;
                    for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
                        //将获取账号超时时间放入CusOutTime里
                        trainpassenger.setOrderid(cusOutTime);
                        plistcus.add(trainpassenger);
                    }
                }
            }
            else {
                plistcus = this.trainorder.getPassengers();
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("Error_getTimeOutTrainpassenger", e);
            plistcus = this.trainorder.getPassengers();
        }
        return plistcus;
    }

    /**
     * 说明:判断无座 taobao
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public boolean ishaveseatthisTaoBao(Trainorder trainorder) {
        boolean result = true;
        if (!isNeedStandingSeat()) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    if ("无座".equals(trainticket.getSeatno())) {
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId()
                                + ":ishaveseatthis:包含无座");
                        result = false;

                    }
                }
            }
        }
        return result;
    }

    /**
     *
     * 说明:获取下单所用的参数 并下单到12306
     *
     * @param trainorderid
     * @param customeruserId
     * @param cookieString
     * @param loginname
     * @param isjointrip
     *            是否是联程票
     * @return gotoInit();
     * @time 2014年8月30日 上午11:16:03
     * @author yinshubin
     */
    public String acquisitionParameters(long trainorderid, Customeruser user, String cookieString, String loginname) {
        this.orderType = trainorder.getOrdertype();//订单类型
        this.CustomerAccount = this.orderType == 3 || this.orderType == 4;//客人账号:true:是客人账号;false:不是
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":获取下单所用的参数:" + trainorderid + ":" + cookieString + ":"
                + loginname + ":" + this.isjointrip);
        String start_station_name = "";
        String end_station_name = "";
        this.passengers = getPassengersString(this.trainorder);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":获取下单id:" + trainorderid + ":所用的参数:" + passengers + ":"
                + loginname);
        Long l2 = System.currentTimeMillis();
        String TCTrainOrdering = TCTrainOrdering(user, trainorder, start_station_name, end_station_name);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":TCTrainOrdering:返回:" + TCTrainOrdering
                + ":TCTrainOrdering耗时:" + (System.currentTimeMillis() - l2));
        // Customeruser user =
        // Server.getInstance().getMemberService().findCustomeruser(customeruserId);
        return TCTrainOrdering;
    }

    /**
     * 拼一个乘客信息的字符串
     * @return
     * @time 2015年12月11日 下午3:12:40
     * @author chendong
     * @param trainorder2
     */
    protected String getPassengersString(Trainorder trainorder) {
        if (this.passengers == null) {
            this.passengers = "";
        }
        WriteLog.write("JobGenerateOrder_MyThread_二次校验", "订单号： -->" + trainorder.getId() + " 乘客数  : -- >"
                + trainorder.getPassengers().size());
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            String passenger = "";
            try {
                String pname = trainpassenger.getName();
                String pidnumber = trainpassenger.getIdnumber();
                int pidtype = trainpassenger.getIdtype();
                Trainticket trainticket = trainpassenger.getTraintickets().get(0);
                if (!this.isjointrip) {// 联程第2段的车次信息
                    trainticket = trainpassenger.getTraintickets().get(1);
                }
                String idtype = "1";
                if (1 == pidtype) {
                    idtype = "1";
                }
                if (3 == pidtype) {
                    idtype = "B";
                }
                if (4 == pidtype) {
                    idtype = "C";
                }
                if (5 == pidtype) {
                    idtype = "G";
                }
                String tickettype = trainticket.getTickettype() + "";
                if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                    tickettype = "1";
                }
                else {
                    if ("0".equals(tickettype)) {
                        tickettype = "2";
                    }
                }
                passenger = pname + "|" + idtype + "|" + pidnumber + "|" + tickettype + "|" + trainticket.getSeattype()
                        + "|" + Float.valueOf(trainticket.getPayprice()).toString().replace(".", "");
            }
            catch (Exception e) {
                WriteLog.write("JobGenerateOrder_MyThread_二次校验_Exception", "订单号： -->" + trainorder.getId()
                        + " 乘客数  : -- >" + trainpassenger.getId());
            }
            WriteLog.write("JobGenerateOrder_MyThread_二次校验", "订单号： -->" + trainorder.getId() + " 乘客ID  : -- >"
                    + trainpassenger.getId() + " 当前乘客信息 ：-->" + passenger);
            passengers += passenger + ",";
        }
        this.passengers = passengers.substring(0, passengers.length() - 1);
        WriteLog.write("JobGenerateOrder_MyThread_二次校验", "订单号： -->" + trainorder.getId() + " 最终拼接下单乘客参数  : -- >"
                + passengers);
        return this.passengers;
    }

    /**
     * 原来是返回String暂时把结果放到Changesupplytradeno里 如需要可以使用方法getChangesupplytradeno获取
     *
     * @param trainorder
     * @param from_station
     *            出发站三字码,可不传
     * @param to_station
     *            到达站三字码,可不传
     * @param isjointrip
     *            是否是联程票
     * @return >> 格式如:[{"ticket_type":"票类型","price":票价,"zwcode":"座位编码",
     *         "passenger_id_type_code"
     *         :"证件类型","passenger_name":"乘客姓名","passenger_id_no":"证件号"}] >>
     *         ticket_type:1:成人票,2:儿童票,3:学生票,4:残军票 >> price:float类型 >>
     *         zwcode:9:商务座
     *         ,P:特等座,M:一等座,O:二等座,6:高级软卧,4:软卧,3:硬卧,2:软座,1:硬座,0:站票[无座] >>
     *         passenger_id_type_code:1:二代身份证,C:港澳通行证,G:台湾通 行证,B:护照
     * @time 2014年12月14日 上午10:28:02
     * @author wzc
     */
    public String TCTrainOrdering(Customeruser user, Trainorder trainorder, String from_station, String to_station) {
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":1TCTrainOrdering:" + user);
        String result = "下单接口访问错误";
        List<Trainpassenger> passengerlist = trainorder.getPassengers();
        int ticketmark = this.isjointrip ? 0 : 1;// 0 正常订单,1 联程订单
        String passengersstr = "";
        Trainticket ticket = new Trainticket();
        if (passengerlist.size() > 0) {
            passengersstr = getArrayString(passengerlist, ticketmark);//获取到准备下单的参数
            List<Trainticket> tickets = passengerlist.get(0).getTraintickets();
            if (tickets.size() > 0) {
                ticket = tickets.get(ticketmark);
            }
        }
        WriteLog.write("JobGenerateOrder_MyThread",
                r1 + ":2TCTrainOrdering:ticket:" + com.alibaba.fastjson.JSONObject.toJSONString(ticket) + ":"
                        + ":passengersstr:" + passengersstr);
        if (ticket != null && ticket.getDeparttime().length() >= 10) {
            long t1 = System.currentTimeMillis();
            result = orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, 1, 0);//循环下单
            long t2 = System.currentTimeMillis();
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":trainorderid:" + trainorder.getId()
                    + ":TCTrainOrdering:orderRound:下单用时:" + (t2 - t1) + ":" + result);
        }
        else {
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":乘客票有问题");
        }
        return result;
    }

    /**
     * 乘客已添加，针对客人账号订单，如淘宝、美团
     */
    private boolean PassengersHaveBeenAdded(Trainorder order, int interfacetype, boolean CustomerAccount) {
        //6>>淘宝、7>>美团[参见TrainInterfaceMethod类]
        return CustomerAccount && (interfacetype == 6 || interfacetype == 7);
    }

    //订单类型
    public int orderType = 1;

    /**
     * 客人账号:true:是客人账号;false:不是
     */
    public boolean CustomerAccount = (orderType == 3 || orderType == 4);

    /**
     * 循环下单
     *
     * @param ticket
     * @param passengersstr
     * @param user
     * @param result
     * @param r1
     * @param t1
     * @param from_station
     * @param to_station
     * @param i
     * @return
     * @time 2015年1月3日 下午5:41:26
     * @author chendong
     */
    public String orderRound(Trainticket ticket, String passengersstr, Customeruser user, String result, long t1,
            String from_station, String to_station, int i, int phonesum) {
        if (TaobaoTrainRule.isTimeout(this.trainorder, this.interfacetype, this.order_timeout_time)) {//淘宝判断是否下单超时
            refuse(trainorder.getId(), 1, user, "下单超时[" + this.order_timeout_time + "]");
            freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
            return "下单超时";
        }
        //订单类型
        int orderType = trainorder.getOrdertype();
        //客人账号
        boolean CustomerAccount = orderType == 3 || orderType == 4;
        //淘宝托管
        boolean TaoBaoCustomerAccount = CustomerAccount && interfacetype == 6;
        //途牛托管
        boolean TuNiuCustomerAccount = CustomerAccount
                && String.valueOf(trainorder.getAgentid()).equals(
                        PropertyUtil.getValue("TuNiu_OnlineAgentId", "train.properties"));
        //针对客人账号已绑定乘客，12306添加乘客未通过时继续下单，目前只有淘宝
        if (user != null && PassengersHaveBeenAdded(trainorder, this.interfacetype, CustomerAccount)) {
            user.setWorkphone("DontBindingPassengersIsTrue");//固定值，勿修改
        }
        String train_date = ticket.getDeparttime().substring(0, 10);//发车日期
        String departure = ticket.getDeparture();//出发站
        String arrival = ticket.getArrival();//到达站
        String trainno = ticket.getTrainno();//车次
        Long l1 = System.currentTimeMillis();
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":" + trainorder.getId() + ":开始下单:" + train_date + ":"
                + from_station + ":" + to_station + ":" + departure + ":" + arrival + ":" + trainno + ":"
                + passengersstr + ":" + user + ":" + CustomerAccount);
        TrainOrderMessage trainOrderMessage = new TrainOrderMessage(trainorder.getId(), train_date, from_station,
                to_station, departure, arrival, trainno, passengersstr, propertys);
        TrainOrderReturnBean returnob = Server.getInstance().getTrain12306Service()
                .create12306OrderNew(trainOrderMessage, user);
        //12306下单结果返回的信息
        WriteLog.write("JobGenerateOrder_MyThread_TrainOrderReturnBean", r1 + ":trainorderid:" + trainorder.getId()
                + ":TrainOrderReturnBean:下单耗时[" + (System.currentTimeMillis() - l1) + "]:"
                + com.alibaba.fastjson.JSONObject.toJSONString(returnob));
        String code = returnob.getCode();
        String jsonmsg = returnob.getJson();
        String msg = returnob.getMsg();
        WriteLog.write("TrainCreateOrder_refund_online", "" + returnob.getRefundOnline());
        if (returnob.getRefundOnline() == 1) {
            isreturnonline = true;
        }
        result = msg;
        boolean success = returnob.getSuccess();
        if (!success) {
            WriteLog.write("CP_产品下单失败", this.trainorder.getAgentid() + "--->customeruser:" + user.getLoginname()
                    + "--->" + msg + "--->orderId:" + this.trainorder.getId() + "--->roundSum:" + (i + 1));
        }
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":TCTrainOrdering:code:" + code + ":msgmsg:" + msg
                + ":orderRound_interface:下单用时:" + (System.currentTimeMillis() - l1) + ":jsonmsg:" + jsonmsg);
        if (success) {//下单成功！！！
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i + ":下单耗时:"
                    + (System.currentTimeMillis() - t1) + ":jsonmsg:" + jsonmsg);
            for (int j = 0; j < 5; j++) {
                boolean isAll = TrainCreateOrderUtil.isall(this.trainorder, jsonmsg, this.trainorder.getPassengers(),
                        false);
                if (!isAll) {
                    jsonmsg = getstr(user, this.passengers);
                }
                else {
                    break;
                }
            }
            boolean isAll = TrainCreateOrderUtil
                    .isall(this.trainorder, jsonmsg, this.trainorder.getPassengers(), false);
            if (isAll) {
                if (TrainInterfaceMethod.QUNAR == this.interfacetype && jsonmsg.contains("无座")
                        && isCanExtCreateOrderByStandingSeat() && cancelTrainorderByStandingSeat(jsonmsg, user, false)) {
                    //无座重新下单
                    return orderRoundByAlternativeAgents(ticket, passengersstr, user, result, t1, from_station,
                            to_station);
                }
                else {
                    /**从TrainOrderReturnBean中把支付链接拿出来，存到订单里
                     */
                    trainorder.setChangesupplytradeno(returnob.getPayUrl());
                    return saveOrderInformation(jsonmsg, trainorder.getId(), user.getLoginname(), this.isjointrip,
                            returnob, user);
                }
            }
            else {
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":订单 :" + i + ":下单耗时:"
                        + (System.currentTimeMillis() - t1) + ":jsonmsg:" + jsonmsg);
                //取消订单
                cancelTrainorderByStandingSeat(jsonmsg, user, true);
                //更新订单
                trainorder.setSupplyaccount(user.getLoginname());
                trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                return "12306订单信息不全";
            }
        }
        //下面是下单失败或者排队的哦逻辑开始###
        //根据返回的信息判断这个订单是否需要重新下单【需要返回true;不需要返回false】
        //true的情况是重新下单可能成功,false就算重新下单也不会成功
        disposeMsgCheckAcount(user, msg);
        boolean isReCreateOrderBymsg = TrainCreateOrderUtil.getisReCreateOrderBymsg(msg, this.trainorderid,
                this.trainorder.getAgentid());//根据msg判断是否需要重新下单
        WriteLog.write("JobGenerateOrder_MyThread", this.r1 + ":" + this.trainorder.getId() + ":orderRound|code:"
                + code + ":isReCreateOrderBymsg:" + isReCreateOrderBymsg + ":result:" + result);
        errorAgainMessageJson.putAll(TrainPropertyMessage.getErrorAgainCount((isPhoneOrder(trainorderid) ? 2 : 1), 1,
                trainorder.getAgentid(), interfacetype, orderType, result, trainorderid));
        boolean canRefuseOrder = isCanRefuseOrder(errorAgainMessageJson, rules, i, result);
        errorAgainMessageJson.remove("externalAgainRule");
        long errorResultSleepTime = errorAgainMessageJson.containsKey("externalAgainSleepTime") ? errorAgainMessageJson
                .getLongValue("externalAgainSleepTime") : 0;
        if (isReCreateOrderBymsg && canRefuseOrder) {
            if (isReCreateOrderBymsg && ((msg.contains("已订") || msg.contains("已购买")))) {
                if (i >= this.haveBoughtCount) {
                    result = msg;
                    saveTrainorderdc(result, this.trainorder, user, i);
                    refuse(this.trainorder.getId(), 1, user, result);
                    freeCustomeruser(user, 1, 1, 0, AccountSystem.NullDepartTime);
                    return result;
                }
                try {
                    Thread.sleep(this.isReCreateOrderSleepTime);
                }
                catch (InterruptedException e) {
                    ExceptionUtil.writelogByException("JobGenerateOrder_MyThread_Exception", e, "睡觉不老实,出异常了");
                }
            }
            try {
                Thread.sleep(errorResultSleepTime);
            }
            catch (InterruptedException e1) {
            }
            String Runtime = returnob.getRuntime();
            if (Runtime == null) {
                Runtime = "";
            }

            //判断是否走排队机制
            Trainticket tk = this.trainorder.getPassengers().get(0).getTraintickets().get(0);
            boolean isPaidui = TrainCreateOrderUtil.ispaidui(this.trainorder.getOrdertype(), interfacetype, result, tk);//根据Interfacetype和result判断是正在排队排队
            if (isPaidui) {
                queueticket = queueticket + 1;
                insertTrainOrderPaiduiData(trainorder.getId(), user, passengers, Runtime);
                try {
                    int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
                    com.alibaba.fastjson.JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
                    jsonObject.put("trainorderid", Long.valueOf(this.trainorderid));
                    jsonObject.put("isLastPaidui", Boolean.valueOf(false));
                    MQSendMessageMethod.sendOnemessageUnException(jsonObject.toJSONString(),
                            "QueueMQ_trainorder_waitorder_orderid_PaiDui", pingtaiType);
                    WriteLog.write("TrainCreateOrderSupplyMethod_paidui", "订单号:" + this.trainorderid + ",平台类型: "
                            + pingtaiType + ",进入排队消费者");
                }
                catch (Exception e) {
                    ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_orderRound_Exception", e,
                            "发送排队消息时出现异常" + this.trainorderid);
                }
                return "订单已走排队机制";
            }
            if (msg.indexOf("当前提交订单用户过多") > -1) {
                addTooManyUsersSubmit();
            }
            if ((!CustomerAccount && (result.contains("存在未完成订单") || msg.indexOf("已订") > -1))
                    || (TuNiuCustomerAccount && result.contains("存在未完成订单"))) {
                //途牛存在未完成订单
                if (TuNiuCustomerAccount && result.contains("存在未完成订单")) {
                    WriteLog.write("TrainCreateOrderSupplyMethod_CustomerAccountExistsOrder",
                            r1 + ":" + trainorder.getId() + ":jsonmsg:" + jsonmsg + ":result:" + result);
                }
                jsonmsg = getstr(user, this.passengers);
                WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:0:"
                        + ":jsonmsg:" + jsonmsg + ":result:" + result);
                if (jsonmsg.indexOf("改签待支付") < 0 && jsonmsg.indexOf("变更到站待支付") < 0
                        && !Account12306Util.accountNoLogin(jsonmsg, user)) {
                    // 290573:390070:进入排队:0::jsonmsg:无未支付订单:result:提交订单成功，程序自动排队，未获取到订单号，可能情况：1、占座失败；2：超过程序排队时间设定，还在排队中。
                    if ("".equals(jsonmsg)) {
                        jsonmsg = getstr(user, this.passengers);
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:1:"
                                + ":jsonmsg:" + jsonmsg + ":result:" + result);
                    }
                    if ("".equals(jsonmsg)) {
                        jsonmsg = getstr(user, this.passengers);
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":进入排队:2:"
                                + ":jsonmsg:" + jsonmsg + ":result:" + result);
                    }
                    if (jsonmsg.contains("没有足够的票")) {
                        result = jsonmsg;
                        saveTrainorderdc(result, trainorder, user, i);
                        refuse(trainorder.getId(), 1, user, result);
                        freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                AccountSystem.ZeroCancel, AccountSystem.NullDepartTime);
                        //没有足够的票拒单后就直接返回了 chendong 2015年11月7日16:20:04
                        //                        return result;
                    }
                    //存在未完成订单,并且排队的乘客和当前订单匹配,走排队机制
                    else if (jsonmsg.contains("订单排队中")
                            && jsonmsg.contains(this.trainorder.getPassengers().get(0).getName())) {
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId()
                                + ":存在未完成订单:进入排队:0:" + ":jsonmsg:" + jsonmsg + ":result:" + result);
                        insertTrainOrderPaiduiData(trainorder.getId(), user, passengers, Runtime);
                        return "订单已走排队机制";
                    }
                    else {
                        WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId() + ":未进入排队:0:"
                                + ":jsonmsg:" + this.trainorder.getPassengers().get(0).getName());
                        jsonmsg = getIsAllByjsonMsg(jsonmsg, user, code);
                        //isPaiDui:true-->勿修改
                        boolean isall = TrainCreateOrderUtil.isall(this.trainorder, jsonmsg,
                                this.trainorder.getPassengers(), true);
                        if (isall) {
                            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":" + trainorder.getId()
                                    + ":排队成功了:" + jsonmsg);
                            if (TrainInterfaceMethod.QUNAR == this.interfacetype && jsonmsg.contains("无座")
                                    && isCanExtCreateOrderByStandingSeat()
                                    && cancelTrainorderByStandingSeat(jsonmsg, user, false)) {
                                //无座重新下单
                                return orderRoundByAlternativeAgents(ticket, passengersstr, user, result, t1,
                                        from_station, to_station);
                            }
                            else if (!TuNiuCustomerAccount || (TuNiuCustomerAccount && rightPayLimitTime(jsonmsg))) {
                                String saveOrderInformation_result = saveOrderInformation(jsonmsg, trainorder.getId(),
                                        user.getLoginname(), this.isjointrip, returnob, user);
                                return saveOrderInformation_result;
                            }
                        }
                        else {
                            //                            this.isnowaitpayorderneedkillcus = true;
                        }
                    }
                }
            }
            //客人账号未登录，并且是账号密码错误等原因
            boolean customerAccountEnNeedLoginAgain = customerAccountEnNeedLoginAgain(CustomerAccount, msg);
            //客人账号未登录
            boolean customerAccountNoLogin = customerAccountNoLogin(CustomerAccount, msg);
            //记录日志
            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":trainorderid:" + trainorder.getId() + ":i:" + i
                    + ":ordermax:" + ordermax + ":msg:" + msg + ":" + jsonmsg);
            jsonmsg = jsonmsg == null ? "" : jsonmsg;

            // 重复订单、多次打码、当前最大数<系统最大数+错误最大数
            if ((/*jsonmsg.contains("sequence_no") || msg.contains("多次打码失败")|| msg.contains("第三方")
                    || msg.contains("获取动态秘钥失败") || msg.contains("取消次数过多") ||*/msg.contains("已满")
                    || (!TaoBaoCustomerAccount && msg.contains("用户未登录")) || msg.equals("车票预订页，提交预订请求失败。") || msg
                        .contains("车票信息已过期，请重新查询最新车票信息")) && this.ordermax < (this.ordermaxbefore + this.ordermaxerroe)) {
                WriteLog.write("ordermaxerroe", r1 + ":错误原因:" + msg);
                WriteLog.write("ordermaxerroe", r1 + ":当前最大数:" + this.ordermax);
                WriteLog.write("ordermaxerroe", r1 + ":系统最大数:" + this.ordermaxbefore);
                WriteLog.write("ordermaxerroe", r1 + ":错误最大数:" + this.ordermaxerroe);
                this.ordermax++;
            }
            //淘宝托管，账号未登录
            else if (TaoBaoCustomerAccount && customerAccountNoLogin && this.ordermax < this.taobaoNoLoginMax) {
                this.ordermax++;
            }
            //如果超过循环次数还是这个失败原因的话 &&不是淘宝托管，走手机端，并且走3次
            else if (msg.contains("车票信息已过期，请重新查询最新车票信息") && !TaoBaoCustomerAccount
                    && this.ordermax >= (this.ordermaxbefore + this.ordermaxerroe)) {
                //是否可以走手机端
                if (canUsePhone()) {
                    //加3次循环
                    this.ordermax += this.orderMaxQueryRetryPhone;
                    //将附加次数清零
                    this.orderMaxQueryRetryPhone = 0;
                }
            }
            if (result.contains("存在未完成订单") && isnowaitpayorderneedkillcus) {
                freeCustomeruser(user, AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
                isnowaitpayorderneedkillcus = false;
            }
            //如果是高铁托管订单出现账号问题不直接进行拒单
            if (gaoTeiShiftOrderType(result, CustomerAccount, customerAccountNoLogin, customerAccountEnNeedLoginAgain)) {
                WriteLog.write("高铁托管订单下单账号问题", "订单id:" + trainorder.getId() + "返回结果:" + result + "");
                //转换订单类型为代购去下单
                trainorder.setOrdertype(1);
                // TODO 准备转换之前将标识存入数据库
                Server.getInstance()
                        .getSystemService()
                        .findMapResultByProcedure(
                                "GT_ShiftOrderTypeFlag @orderNumber =" + trainorder.getQunarOrdernumber());
                user = Server.getInstance().getTrain12306Service().getcustomeruser(this.trainorder);
                return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, i, phonesum);
            }
            //客人账号未登录，并且是账号密码错误等原因
            if (customerAccountEnNeedLoginAgain) {
                saveTrainorderdc(result, trainorder, user, i);
                refuse(trainorder.getId(), 1, user, result);
                FreeNoCare(user);
            }
            else if (isnowaitpayorderneedkillcus) {
                saveTrainorderdc(result, trainorder, user, i);
                refuse(trainorder.getId(), 1, user, result);
                freeCustomeruser(user, AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            else if (msg.indexOf("多次打码失败") > 0) {
                if (this.ordererrorcodenum < this.ordererrorcodemax) {
                    this.ordererrorcodenum = this.ordererrorcodenum + 1;
                    return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, i + 1,
                            phonesum);
                }
                else {
                    saveTrainorderdc(result, trainorder, user, i);
                    refuse(trainorder.getId(), 1, user, result);
                    freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                            AccountSystem.NullDepartTime);
                }
            }
            //同程的才去核验
            else if (i < this.ordermax && msg.indexOf("手机核验") > 0 && "tc".equals(default_pingtaiStr)) {
                boolean isFalsely = false;//#----------------------手机核验s
                if (phonesum == 0) {
                    isFalsely = getisFalsely_isMaoyong(user);//挨个乘客去ocs中判断是否是冒用
                    WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname()
                            + "--->isFalsely:" + isFalsely);
                    if (isFalsely) { //如果有冒用的
                        String isPhoneVer = getSysconfigStringbydb("isPhoneVer");//是否去手机核验
                        if ("true".equals(isPhoneVer)) {
                            mobilePhoneVerification(user);//去手机核验
                            int mobilePhoneVerificationResult = -3;
                            long vertimestart = System.currentTimeMillis();
                            int versum = 1;
                            do {
                                try {
                                    Thread.sleep(10000L);//每次核验间隔10秒
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                //获取核验结果
                                mobilePhoneVerificationResult = mobilePhoneVerificationResult(user);
                                WriteLog.write("TrainCreateOrder_手机核验",
                                        this.trainorderid + "--->" + user.getLoginname() + "--->获取核验结果次数:" + versum
                                                + "--->" + mobilePhoneVerificationResult);
                                long vertimesMax = getVertimesMax();
                                if (System.currentTimeMillis() - vertimestart >= vertimesMax) {
                                    break;
                                }
                                versum++;
                            }
                            while (mobilePhoneVerificationResult != 2 && mobilePhoneVerificationResult != 3
                                    && mobilePhoneVerificationResult != 4);
                            WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname()
                                    + "--->最终结果:" + mobilePhoneVerificationResult);
                            //-2,-1,0,1 重试 2成功 3失败 4放弃
                            if (mobilePhoneVerificationResult == 2) {
                                i = 0;
                                phonesum = phonesum + 1;
                                WriteLog.write("TrainCreateOrder_手机核验",
                                        this.trainorderid + "--->" + user.getLoginname() + "--->核验通过重新下单");
                                return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, i,
                                        phonesum);
                            }
                            else {
                                if (mobilePhoneVerificationResult == 0) {
                                    mobilePhoneVerificationFree(user);
                                }
                                isEnableCustomeruserByType(user, msg, 33);
                                WriteLog.write("TrainCreateOrder_手机核验",
                                        this.trainorderid + "--->" + user.getLoginname());
                                WriteLog.write("TrainCreateOrder_手机核验",
                                        this.trainorderid + "--->" + user.getLoginname() + "--->核验失败拒单");
                                refuse(trainorder.getId(), 1, user, result);
                            }
                        }
                        else {
                            //放弃核验逻辑
                            isEnableCustomeruserByType(user, msg, 33);
                            WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname()
                                    + "--->放弃核验拒单");
                            refuse(trainorder.getId(), 1, user, result);
                        }
                    }
                    else {
                        //没有冒用 正常换账号重试
                        isEnableCustomeruserByType(user, msg, 33);
                        WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname());
                        List<Trainpassenger> plistcus = getTimeOutTrainpassenger();
                        this.trainorder.setPassengers(plistcus);
                        Customeruser customeruser = Server.getInstance().getTrain12306Service()
                                .getcustomeruser(this.trainorder);
                        if (customeruser != null && customeruser.getId() > 0
                                && TrainCreateOrderUtil.isCanUseByIsenable(customeruser.getIsenable())) {
                            WriteLog.write("TrainCreateOrder_手机核验", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i
                                    + ":切换账号成功:" + user.getLoginname() + "=========新账号>" + customeruser.getLoginname());
                            user = customeruser;
                        }
                        WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname()
                                + "--->没有冒用重新下单");
                        return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, i + 1,
                                phonesum);
                    }
                }
                else {
                    isEnableCustomeruserByType(user, msg, 33);
                    WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname());
                    WriteLog.write("TrainCreateOrder_手机核验", this.trainorderid + "--->" + user.getLoginname()
                            + "--->超过核验次数,不核验了");
                    refuse(trainorder.getId(), 1, user, result);
                }
                //#----------------------手机核验e
            }
            //小于最大循环次数或者失败原因是取消次数过多&&不是客人账号
            else if (i < this.ordermax || ((msg.indexOf("取消次数过多") > 0) && !CustomerAccount)) {
                //是否,换账号重试
                boolean isChangeAccount = TrainCreateOrderUtil.isChangeAccount(jsonmsg, msg, customerAccountNoLogin);
                WriteLog.write("JobGenerateOrder_MyThread", r1 + ":" + trainorder.getId() + ":isChangeAccount:"
                        + isChangeAccount + ":jsonmsg:" + jsonmsg + ":msg:" + msg + ":customerAccountNoLogin:"
                        + customerAccountNoLogin);
                if (isChangeAccount) {
                    //冒用处理
                    maoYongChuLi(msg, user, passengersstr);
                    //客人账号未登录，刷新Cookie
                    String dontRetryLoginMsg = refreshCookie(trainorder, interfacetype, customerAccountNoLogin,
                            orderType);
                    //不进行下单重试
                    if (!ElongHotelInterfaceUtil.StringIsNull(dontRetryLoginMsg)) {
                        //真实原因
                        result = dontRetryLoginMsg;
                        //记录日志、拒单操作
                        saveTrainorderdc(result, trainorder, user, i);
                        refuse(trainorder.getId(), 1, user, "获取下单账户失败");
                        //释放账号
                        FreeNoCare(user);
                        //记录日志
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i
                                + ":dontRetryLoginMsg:" + dontRetryLoginMsg);
                        return result;
                    }
                    //fiend 客人账号密码登陆失败,直接拒单,后续抓全12306信息后加判断是否可以二次登陆
                    //密码输入错误。如果输错次数超过4次，用户将被锁定。
                    //非客人账号 或 客人账号未登录并且密码不能错
                    if (!CustomerAccount || customerAccountNoLogin || msg.contains("存在未完成订单")) {//切换账号的逻辑
                        //客人账号
                        if (CustomerAccount && msg.contains("存在未完成订单")) {
                            try {
                                Thread.sleep(3000);
                            }
                            catch (Exception e) {
                                System.out.println("存在未完成订单，等待异常.");
                            }
                        }
                        boolean havefree = isHavefree(msg, user);//账号是否释放
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i
                                + ":准备切换账号");
                        List<Trainpassenger> plistcus = getTimeOutTrainpassenger();
                        this.trainorder.setPassengers(plistcus);
                        do {
                            Customeruser customeruser = Server.getInstance().getTrain12306Service()
                                    .getcustomeruser(this.trainorder);
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:"
                                    + i + ":切换的新账号:" + customeruser.getLoginname() + ":原因:" + msg);
                            WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:"
                                    + i + ":切换账号成功:" + user.getLoginname() + "=========>"
                                    + com.alibaba.fastjson.JSONObject.toJSONString(customeruser));
                            //账号可用于下单>>账号未释放、isenable符合下单>>此逻辑不可修改
                            if (customeruser != null && customeruser.getId() > 0 && !customeruser.isCanCreateOrder()
                                    && TrainCreateOrderUtil.isCanUseByIsenable(customeruser.getIsenable())) {
                                //获取到有效账号了
                                //切换账号，释放老账号
                                if (!havefree) {
                                    freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                            AccountSystem.ZeroCancel, AccountSystem.NullDepartTime);
                                }
                                user = customeruser;
                                break;
                            }
                            else {
                                //账号不可用于下单、账号未释放
                                if (customeruser != null && !customeruser.isCanCreateOrder()) {
                                    FreeNoCare(customeruser);
                                }
                                //切换账号后未获取到有效账号;再获取账号
                                //不要登录重试，针对第三方传账号和密码，防止重试锁账号等
                                if (customeruser != null && customeruser.isDontRetryLogin()) {
                                    result = customeruser.getNationality();
                                    saveTrainorderdc(result, trainorder, user, i);
                                    refuse(trainorder.getId(), 1, user, result);
                                    return result;
                                }
                                else {
                                    if (i < this.ordermax
                                            && (customeruser == null || customeruser.getLoginname() == null || (!"falsely_enable"
                                                    .equals(customeruser.getLoginname()) && !"falsely_no"
                                                    .equals(customeruser.getLoginname())))) {
                                        i = i + 1;
                                        try {
                                            Thread.sleep(10000);
                                        }
                                        catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        if (!havefree) {
                                            freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                                    AccountSystem.ZeroCancel, AccountSystem.NullDepartTime);
                                        }
                                        if (result.contains("的身份信息涉嫌被他人冒用") && result.contains("@")) {
                                            //冒用3151
                                            if ("falsely_enable".equals(customeruser.getLoginname())) {
                                                if (Train3151Rule.satisfyRule(this.trainorder)
                                                        && IsTaobaoBackcall(customeruser, trainorder.getAgentid(),
                                                                result)) {
                                                    return "该订单常旅对应的可用账号使用中，请等待";
                                                }
                                            }
                                        }
                                        saveTrainorderdc(result, trainorder, user, i);
                                        refuse(trainorder.getId(), 1, user, result);
                                        return result;
                                    }
                                }
                            }
                        }
                        while (true);
                    }
                    else {
                        saveTrainorderdc(result, trainorder, user, i);
                        refuse(trainorder.getId(), 1, user, result);
                        FreeNoCare(user);
                        return result;
                    }
                }
                if (msg.indexOf("联系人尚未通过身份信息核验") > 0) {
                    try {
                        Thread.sleep(loopAddSleepTime);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (i == this.ordermax - 1) {
                        //如果还未换账号,
                        if (!loopAdd2ChangeAccountFlag && loopAdd2 < loopAdd2Max) {
                            i--;
                            loopAdd2++;
                            try {
                                Thread.sleep(loopAdd2 * loopAddSleepAddTime);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, i + 1, phonesum);
            }
            else {
                saveTrainorderdc(result, trainorder, user, i);
                refuse(trainorder.getId(), 1, user, result);
                freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                        AccountSystem.NullDepartTime);
            }
        }
        else if (TrainInterfaceMethod.QUNAR == this.interfacetype
                && (msg.indexOf("余票不足") > -1 || msg.indexOf("没有足够的票") > -1 || msg.indexOf("已无余票") > -1)) {
            orderRoundByAlternativeAgents(ticket, passengersstr, user, result, t1, from_station, to_station);
        }
        else {
            saveTrainorderdc(result, trainorder, user, i);
            refuse(trainorder.getId(), 1, user, result);
            freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
        return result;
    }

    /**
     * 判断是否是高铁的代购订单并且是因为账号问题失败的
     * @time:2017年10月12日上午11:22:25
     * @author:baozz
     * @param result
     * @param customerAccountNoLogin
     * @param customerAccountEnNeedLoginAgain
     * @return
     */
    private boolean gaoTeiShiftOrderType(String result, boolean CustomerAccount, boolean customerAccountNoLogin,
            boolean customerAccountEnNeedLoginAgain) {
        boolean falg = gaotieShiftOrderType && (CustomerAccount && ((result.contains("冒用")
                || result.contains("账号未通过核验") || result.contains("手机未核验") || result.contains("已满")
                || customerAccountEnNeedLoginAgain || customerAccountNoLogin) && trainorder.getAgentid() == 86));
        if (trainorder.getAgentid() == 86 && CustomerAccount) {
            WriteLog.write("高铁托管订单下单账号问题test", "是否进行转换:"+gaotieShiftOrderType+"结果:" + result + "是否是客人账号:" + CustomerAccount + "是否是账号有问题:"
                    + customerAccountNoLogin + "是否是账号原因失败" + customerAccountEnNeedLoginAgain + "返回结果:" + falg);
        }
        return falg;
    }

    /**
     * 淘宝3151逻辑
     *
     * @param customeruser
     * @param Agentid
     * @param result
     * @return
     * @time 2017年3月29日 上午9:55:35
     * @author yuanzc
     */
    public boolean IsTaobaoBackcall(Customeruser customeruser, long Agentid, String result) {
        boolean is = false;
        try {
            com.alibaba.fastjson.JSONArray jsonArray = com.alibaba.fastjson.JSONArray.parseArray(customeruser
                    .getChinaaddress());
            com.alibaba.fastjson.JSONArray accountNames = new com.alibaba.fastjson.JSONArray();
            WriteLog.write("淘宝_3151", this.trainorderid + "--->" + "获得可用账号集合:" + jsonArray.toString());
            if (jsonArray.size() > 0) {
                for (int j = 0; j < jsonArray.size(); j++) {
                    com.alibaba.fastjson.JSONObject json12306Account = jsonArray.getJSONObject(j);
                    if (json12306Account.getBooleanValue("using")) {
                        accountNames.add(json12306Account.getString("accountName"));
                    }
                }
                Map<String, String> backup = new HashMap<String, String>();
                backup.put("timeout", "" + System.currentTimeMillis());
                backup.put("uniqueId", "" + this.trainorderid);
                backup.put("callBackUrl", PropertyUtil.getValue("3151callbackUrl", "train.properties"));
                backup.put("accountNames", "" + accountNames);
                WriteLog.write("淘宝_3151", "请求map:" + backup);
                Customeruser customerusers = Account12306Util.get12306Account(AccountSystem.LoginNameAccount, jsonArray
                        .getJSONObject(0).getString("accountName"), false, backup);
                if (customerusers != null && !"".equals(customerusers.getId())) {//预约账号不为空
                    String sql = "sp_AccountWait_insert_TaobaoCallBack @orderId=" + this.trainorderid + ",@account='"
                            + accountNames.toString() + "',@status=0,@result='" + result + "'";
                    try {
                        Server.getInstance().getSystemService().findMapResultByProcedure(sql);
                        WriteLog.write("淘宝_3151", this.trainorderid + "--->" + sql);
                    }
                    catch (Exception e) {
                        ExceptionUtil.writelogByException("淘宝_3151_Exception", e, this.trainorderid + "--->" + sql);
                    }
                    WriteLog.write(
                            "淘宝_3151",
                            this.trainorderid + "--->" + "请求返回:"
                                    + com.alibaba.fastjson.JSONObject.toJSONString(customerusers));
                    is = true;
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("淘宝_3151_Exception", e, this.trainorderid + "");
        }
        return is;
    }

    /**
     * @time:2017年9月20日下午4:13:48
     * @auto:baozz
     * @param jsonObject 内存中的失败重试次数以及睡眠时间
     *  
     * @param rules 失败匹配的正则集合
     * @param result 
     * @return
     */
    private boolean isCanRefuseOrder(com.alibaba.fastjson.JSONObject jsonObject, List<String> rules, int i,
            String result) {
        boolean falg = true;
        try {
            String dbRule = jsonObject.containsKey("externalAgainRule") ? jsonObject.getString("externalAgainRule")
                    : "";
            int dbCount = jsonObject.containsKey("externalAgainCount") ? jsonObject.getIntValue("externalAgainCount")
                    : 5;
            rules.add(dbRule);
            if (rules.size() > 1) {
                String string = rules.get(rules.size() - 2);
                if (!"".equals(string) && dbRule.equals(string)) {
                    jsonObject.put("count", jsonObject.getIntValue("count") + 1);
                }
                else {
                    jsonObject.put("count", 1);
                }
            }
            else {
                jsonObject.put("count", 1);
            }
            if (jsonObject.getIntValue("count") >= dbCount) {
                falg = false;
            }
            WriteLog.write("JobGenerateOrder_MyThread_下单失败判断是否可以再次下单", "订单号:" + trainorderid + ",传入参数:" + jsonObject
                    + "失败结果正则:==>" + dbRule + "<==,失败结果数据库配置数:" + dbCount + ",失败结果是否可以下单:==>"
                    + (falg ? "可以进行下单" : "不可以进行下单") + ",当前失败次数" + i + ",连续失败次数:" + jsonObject.getIntValue("count"));
            WriteLog.write("JobGenerateOrder_MyThread_下单失败判断是否可以再次下单", "订单号:" + trainorderid + ",本次失败原因:" + result);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("JobGenerateOrder_MyThread_Exception", e,
                    "订单号:" + trainorderid + ",失败结果是否可以下单:==>" + (falg ? "可以进行下单" : "不可以进行下单") + ",当前失败次数" + i
                            + ",连续失败次数:" + jsonObject.getIntValue("count"));
        }
        return falg;
    }

    /**
     * //客人账号未登录，刷新Cookie
     * @param trainorder2
     * @param customerAccountNoLogin
     * @param orderType
     * @time 2016年1月6日 下午12:31:46
     * @author chendong
     */
    private String refreshCookie(Trainorder trainorder, int interfacetype, boolean customerAccountNoLogin, int orderType) {
        //非空表示不重试，暂针对淘宝
        String dontRetryLoginMsg = "";
        //客人账号未登录，刷新Cookie
        if (customerAccountNoLogin && orderType == 4) {
            //淘宝、暂停一会
            if (interfacetype == 6) {
                try {
                    //系统配置
                    String wait = PropertyUtil.getValue("fresh_cookie_taobao_waittime", "train.properties");
                    //配置为空
                    if (ElongHotelInterfaceUtil.StringIsNull(wait)) {
                        wait = "60";
                    }
                    //等待一会
                    if (Long.parseLong(wait) > 0) {
                        Thread.sleep(Long.parseLong(wait) * 1000);
                    }
                }
                catch (Exception e) {
                    System.out.println("刷新Cookie等待异常.");
                }
            }
            //刷新Cookie
            String refreshResult = refreshCookieFromInterface(trainorder);
            //淘宝托管、不重试判断
            //{"session_info":{"error_code":"2114","error_msg":"用户名或者密码错误","items":{},"success":false},"request_id":"zq6lg3roepah"}}
            //{"session_info":{"error_code":"2115","error_msg":"登陆频繁，请稍后重试","items":{},"success":false},"request_id":"15q0mir9sv7cd"}}
            try {
                if (interfacetype == 6 && !ElongHotelInterfaceUtil.StringIsNull(refreshResult)) {
                    //不重试判断
                    if (refreshResult.contains("登录名不存在") || refreshResult.contains("邮箱不存在")
                            || refreshResult.contains("用户名不能全为数字") || refreshResult.contains("手机号码尚未进行核验，目前暂无法用于登录")
                            || refreshResult.contains("请重新在网上注册新的账户") || refreshResult.contains("用户已被暂停使用")
                            || refreshResult.contains("密码输入错误") || refreshResult.contains("12306用户名格式不正确")
                            || refreshResult.contains("用户名或者密码错误") || refreshResult.contains("用户状态为2不能获取")) {
                        try {
                            dontRetryLoginMsg = com.alibaba.fastjson.JSONObject.parseObject(refreshResult)
                                    .getJSONObject("train_agent_session_get_response").getJSONObject("session_info")
                                    .getString("error_msg");
                        }
                        catch (Exception e) {
                        }
                        finally {
                            dontRetryLoginMsg = ElongHotelInterfaceUtil.StringIsNull(dontRetryLoginMsg) ? refreshResult
                                    : dontRetryLoginMsg;
                        }
                    }
                }
            }
            catch (Exception e) {
            }
        }
        return dontRetryLoginMsg;
    }

    /**
     * 冒用处理
     * @param msg
     * @time 2016年1月6日 下午12:22:08
     * @author chendong
     */
    private void maoYongChuLi(String msg, Customeruser user, String passengersstr) {
        if (msg.contains("的身份信息涉嫌被他人冒用")) {
            try {
                if (msg.split("@").length > 1) {
                    //                    String key = "falsely_" + msg.split("@")[1];
                    //                    OcsMethod.getInstance().add(key, "falsely");
                    //fiend2wcl 这里new Thread 写你的逻辑  trainpassenger就是那个冒用的乘客
                    checkAccountMethod(user, passengersstr, msg.split("@")[1]);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断下单超时时间
     */
    private boolean rightPayLimitTime(String json) {
        boolean right = false;
        //解析数据判断
        try {
            //配置
            String setTime = PropertyUtil.getValue("TuNiu_CustomerNoCompleteOrderPayTimeLimit", "train.properties");
            //解析
            com.alibaba.fastjson.JSONObject data = com.alibaba.fastjson.JSONObject.parseObject(json).getJSONObject(
                    "data");
            //订单
            com.alibaba.fastjson.JSONArray orderDBList = data.getJSONArray("orderDBList");
            //非唯一
            if (orderDBList == null || orderDBList.size() != 1) {
                return right;
            }
            //第一个
            com.alibaba.fastjson.JSONObject firstOrder = orderDBList.getJSONObject(0);
            //车票
            com.alibaba.fastjson.JSONArray tickets = firstOrder.getJSONArray("tickets");
            //支付时限>>2016-02-29 16:03:07
            String pay_limit_time = tickets.getJSONObject(0).getString("pay_limit_time");
            //时间转换
            long pay_limit_longTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(pay_limit_time).getTime();
            //时间判断
            right = pay_limit_longTime - System.currentTimeMillis() > Long.parseLong(setTime) * 60 * 1000;
        }
        catch (Exception e) {
        }
        return right;
    }

    /**
     * 根据jsonmsg判断是返回的json信息是否全
     * @return
     * @time 2015年12月16日 下午12:54:08
     * @author chendong
     * @param jsonmsg
     * @param code
     * @param user
     */
    private String getIsAllByjsonMsg(String jsonmsg, Customeruser user, String code) {
        for (int j = 0; j < 5; j++) {
            //isPaiDui:true-->勿修改
            boolean isAll = TrainCreateOrderUtil.isall(this.trainorder, jsonmsg, this.trainorder.getPassengers(), true);
            if (isAll) {
                break;
            }
            else {
                WriteLog.write("JobGenerateOrder_MyThread_paidui",
                        r1 + ":" + trainorder.getId() + ":" + user.getLoginname() + ":排队:" + j + ":次:" + code
                                + ":jsonmsg:" + jsonmsg);
                try {
                    Thread.sleep(100L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                jsonmsg = getstr(user, this.passengers);
            }
        }
        return jsonmsg;
    }

    /**
     * 走排队机制
     * 插入需要排队的数据,并写上操作记录
     * @time 2015年12月11日 下午2:27:58
     * @author chendong
     * @param passengers2
     * @param string
     * @param l
     */
    private void insertTrainOrderPaiduiData(long TrainOrderId, Customeruser user, String passengers, String Runtime) {
        String Loginname = user.getLoginname();
        String CookieString = user.getCardnunber();
        //释放REP
        RepServerUtil.freeRepServerByAccount(user);
        //操作记录
        createTrainorderrc(trainorderid, "订单已走排队机制1", "下单系统", 1);
        try {
            //修改12306状态为排队状态
            String sql = "UPDATE T_TRAINORDER SET c_state12306=18 WHERE ID =" + trainorder.getId();
            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":insertTrainOrderPaiduiData:sql:" + sql);
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("JobGenerateOrder_MyThread_paidui", r1 + ":insertTrainOrderPaiduiData:count:" + count);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        String insertSql = "INSERT INTO [TrainOrderPaiduiData] ([TrainOrderId],[Loginname],[CookieString],[Runtime]) "
                + "VALUES (" + TrainOrderId + ",'" + Loginname + "','" + CookieString + "','" + Runtime + "')";
        WriteLog.write("历时_排队", "OrderId:" + TrainOrderId + " ;Runtime:" + Runtime);
        WriteLog.write("历时_排队", "OrderId:" + TrainOrderId + " ;Sql:" + insertSql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(insertSql);

    }

    /**
     * 说明:自动下单失败的书写操作记录
     *
     * @param result
     * @param loginname
     * @param trainorder
     * @param isjointrip
     *            是否是联程票
     * @param customeruserId
     * @param orderno
     *            第几次下单
     * @time 2014年8月30日 上午11:42:19
     * @author yinshubin
     */
    public void saveTrainorderdc(String result, Trainorder trainorder, Customeruser user, int orderno) {
        // trainorder =
        // Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
        trainorder.setId(trainorder.getId());
        // if (orderno == this.ordermax) {
        // trainorder.setIsquestionorder(Trainorder.ORDERINGQUESTION);
        // trainorder.setState12306(Trainorder.ORDERFALSE);
        // Server.getInstance().getTrainService().updateTrainorder(trainorder);
        // }
        if (!this.isjointrip) {
            trainorder.setChangesupplypayway(1);
        }
        String content = "";
        String createuser = "";
        if (this.isjointrip) {// true 正常订单,false 联程订单
            if (TrainInterfaceMethod.TONGCHENG == this.interfacetype
                    || TrainInterfaceMethod.MEITUAN == this.interfacetype
                    || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                    || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                    || TrainInterfaceMethod.YILONG1 == this.interfacetype
                    || TrainInterfaceMethod.YILONG2 == this.interfacetype) {//判断是否重复下单
                // if ((trainorder.getAgentid() == this.tongcheng_agentid &&
                // this.dangqianjiekouagentid == 1) || isotherjiekou) {
                content = "12306:第" + orderno + "次:下单失败:" + result;
            }
            else {
                content = user.getLoginname() + "第" + orderno + "次:下单失败:" + result;
            }
        }
        else {
            content = user.getLoginname() + ":第二程下单失败:" + result;
        }
        if (TrainInterfaceMethod.TONGCHENG == this.interfacetype || TrainInterfaceMethod.MEITUAN == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype
                || TrainInterfaceMethod.WITHHOLDING_BEFORE == this.interfacetype
                || TrainInterfaceMethod.YILONG1 == this.interfacetype
                || TrainInterfaceMethod.YILONG2 == this.interfacetype) {
            createuser = "12306";
        }
        else {
            createuser = user.getLoginname();
        }
        createTrainorderrc(trainorder.getId(), content, createuser, 1);
    }

    /**
     *
     * @return
     * @time 2015年11月10日 下午2:32:00
     * @author chendong
     * @param passengerlist
     * @param ticketmark
     */
    private String getArrayString(List<Trainpassenger> passengerlist, int ticketmark) {
        String passengersstr = "";
        JSONArray array = new JSONArray();
        for (int i = 0; i < passengerlist.size(); i++) {
            Trainpassenger p = passengerlist.get(i);
            List<Trainticket> tickets = p.getTraintickets();
            if (tickets != null && tickets.size() > 0) {
                JSONObject obj = new JSONObject();
                obj.put("passenger_id_no", p.getIdnumber());
                obj.put("passenger_name", p.getName());
                obj.put("ticket_type", tickets.get(ticketmark).getTickettype() == 0 ? 2 : tickets.get(ticketmark)
                        .getTickettype());
                obj.put("price", tickets.get(ticketmark).getPrice());
                if ("无座".equals(tickets.get(ticketmark).getSeattype())) {
                    String codeNum = "";
                    if (tickets.get(ticketmark).getTrainno().startsWith("D")) {
                        codeNum = "O"; //二等座
                    }
                    else if (tickets.get(ticketmark).getTrainno().startsWith("C")) {
                        codeNum = "2";//软座
                    }
                    else {
                        codeNum = "1";//硬座
                    }
                    obj.put("zwcode", codeNum);
                    createTrainOrderExtSeat(trainorderid, "[{\"0\":" + tickets.get(ticketmark).getPrice() + "}]");
                }
                else {
                    obj.put("zwcode", TrainSupplyMethod.getzwname(tickets.get(ticketmark).getSeattype()));
                }
                obj.put("passenger_id_type_code", TrainSupplyMethod.getIdtype12306(p.getIdtype()));

                if (TrainInterfaceMethod.QUNAR == this.interfacetype) {
                    // 是否是qunar订单
                    obj.put("isqunarorder", true);
                }
                //座席转换
                String seatChange = PropertyUtil.getValue("seatChange", "train.properties");
                //空即转换
                obj.put("seatChange",
                        ElongHotelInterfaceUtil.StringIsNull(seatChange) || "true".equalsIgnoreCase(seatChange));
                array.add(obj);
            }
        }
        passengersstr = array.toString();
        return passengersstr;
    }

    /**
     * QUNAR回调占座结果
     *
     * @param i
     * @param orderid
     * @param returnmsg
     *            回调具体内容 占座成功传true
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackQunarOrdered(long orderid, String returnmsg) {
        String result = "false";
        String url = PropertyUtil.getValue("QunarCallBack", "train.properties");
        String merchantCode = PropertyUtil.getValue("merchantCode", "train.properties");
        // //qunar回调地址
        // String QunarCallBack = getSysconfigString("QunarCallBack");
        // //qunar配置环境
        // String merchantCode = "hangt";
        returnmsg = returnMsgStr(returnmsg);
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_callback");
        jso.put("returnmsg", returnmsg);
        jso.put("merchantCode", merchantCode);
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 处理加工12306返回结果，客人账号订单参考CustomerAccountRealRefused()方法
     */
    public String returnMsgStr(String returnmsg) {
        if (returnmsg.contains("获取12306账号失败") || returnmsg.contains("您的账号尚未通过身份信息核验")
                || returnmsg.contains("获取下单服务器失败")) {
            return "下单失败";
        }
        return returnmsg;
    }

    /**
     * 创建火车票操作记录
     *
     * @param trainorderId
     *            火车票订单id
     * @param content
     *            内容
     * @param createuser
     *            用户
     * @param status
     *            状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    protected void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        try {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(trainorderId);
            rc.setContent(content);
            rc.setStatus(status);
            rc.setCreateuser(createuser);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorderId + ":content:" + content);
        }
    }

    /**
     * 同程回调补单结果
     *
     * @param orderid
     * @param returnmsg
     *            回调具体内容 补单成功传true
     * @return
     * @time 2014年01月11日 下午6:20:30
     * @author fiend
     */
    public String callBackTongChengBudan(Trainorder trainorder, String returnmsg) {
        String result = "false";
        String url = this.tcTrainCallBack;
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("agentid", trainorder.getAgentid());
        jso.put("trainorderid", trainorder.getId());
        jso.put("method", "train_order_budan_callback");
        jso.put("returnmsg", returnmsg);
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 回调支付失败
     *
     * @param trainorder
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengPayedFalse(Trainorder trainorder) {
        String result = "false";
        String url = this.tcTrainCallBack;
        JSONObject jso = new JSONObject();
        jso.put("agentid", trainorder.getAgentid());
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getOrdernumber());
        jso.put("method", "train_pay_callback");
        jso.put("isSuccess", "N");
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 同程回调失败后 生成操作记录
     *
     * @param result
     * @param loginname
     * @param trainorder
     * @param isordered
     * @time 2015年1月4日 下午2:05:46
     * @author fiend
     */
    public void tongchengFalseRc(String result, String loginname, Trainorder trainorder, boolean isordered,
            String isbudanstr) {
        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(str + "----回调失败:" + result);
        rc.setCreateuser("12306");
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:" + result);
        }
    }

    /**
     * 同程回调成功后 生成操作记录
     *
     * @param loginname
     * @param trainorder
     * @param isordered
     * @time 2015年1月4日 下午2:05:46
     * @author fiend
     */
    public void tongchengTrueRc(String loginname, Trainorder trainorder, boolean isordered, String isbudanstr) {
        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(str + "----回调成功");
        if ("---补单成功".equals(str)) {
            rc.setContent(str + "----等待支付后回调");
        }
        rc.setCreateuser("12306");
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:同程回调成功");
        }
    }

    /**
     * 获取原订单的WPS
     *
     * @return
     * @time 2015年1月18日 下午8:07:56
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public String getWPS() {
        String sql = "SELECT C_WPS FROM T_TRAINORDERTIMEOUT WHERE C_ORDERID =" + this.trainorder.getId();
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            return map.get("C_WPS").toString();
        }
        return null;
    }

    /**
     * 火车票下单系统是否正常
     *
     * @param orderid
     * @param interfacetype
     * @return
     * @time 2015年4月16日 下午3:49:47
     * @author fiend
     */
    public String isTrainorderSystemErrorForCallback(long orderid, int interfacetype) {
        WriteLog.write("TongchengSupplyMethod_isTrainorderSystemErrorForCallback", orderid + ":当前接口类型:" + interfacetype);
        try {
            if (TrainInterfaceMethod.TONGCHENG == interfacetype || TrainInterfaceMethod.MEITUAN == interfacetype
                    || TrainInterfaceMethod.YILONG1 == this.interfacetype
                    || TrainInterfaceMethod.YILONG2 == this.interfacetype) {
                String isError = getSysconfigStringbydb("isTrainorderSystemErrorForCallback");
                WriteLog.write("TongchengSupplyMethod_isTrainorderSystemErrorForCallback", orderid
                        + ":isTrainorderSystemErrorForCallback:" + isError);
                if (!"-1".equals(isError)) {
                    return isError;
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("TongchengSupplyMethod_isTrainorderSystemErrorForCallback", orderid + ":异常");
        }
        WriteLog.write("TongchengSupplyMethod_isTrainorderSystemErrorForCallback", orderid + ":系统正常");
        return "SUCCESS";
    }

    /**
     * 获取到同程冒用的乘客的json
     * @param description
     * @return
     * @time 2015年11月23日 下午10:07:25
     * @author chendong
     */
    public com.alibaba.fastjson.JSONObject getJsonObject_maoyong(String description) {
        com.alibaba.fastjson.JSONObject jsonObject_maoyong = new com.alibaba.fastjson.JSONObject();
        for (int i = 0; i < this.trainorder.getPassengers().size(); i++) {
            Trainpassenger trainpassenger = this.trainorder.getPassengers().get(i);
            if (description.contains(trainpassenger.getName())) {
                String passengersename = trainpassenger.getName();
                try {
                    passengersename = URLEncoder.encode(passengersename, "utf-8");
                }
                catch (Exception e) {
                }
                jsonObject_maoyong.put("passengersename", passengersename);
                jsonObject_maoyong.put("passportseno", trainpassenger.getIdnumber());
                break;
            }
        }
        return jsonObject_maoyong;
    }

    /**
     * 各种情况下重新把消息扔进队列里等待下次消费
     *
     * @time 2015年4月20日 上午10:00:32
     * @author chendong
     */
    public void reSendTraincreateOrdermessage() {
        try {// 等待7秒重新扔进队列里等待下次去下单 chendong 2015年4月20日09:57:05
            Thread.sleep(7000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        sendMQmessage("QueueMQ_trainorder_waitorder_orderid", this.trainorderid + "");
    }

    /**
     * 区分补单或者是占座
     *
     * @time 2015年1月19日 下午2:29:33
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public boolean isbudanorder() {
        boolean isbudan = false;
        try {
            String sql = "SELECT C_ORDERID FROM T_TRAINORDERTIMEOUT with(nolock) WHERE C_STATE=0 AND C_ORDERID="
                    + trainorder.getId();
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                isbudan = true;
                this.isbudanstr = "补单";
                toBudanRemoveId();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return isbudan;
    }

    public boolean isIsjointrip() {
        return isjointrip;
    }

    public void setIsjointrip(boolean isjointrip) {
        this.isjointrip = isjointrip;
    }

    /**
     * 说明:判断无座 meituan
     *
     * @param trainorder
     * @return true 不下单
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public boolean ishaveseatthisMeiTuan(Trainorder trainorder) {
        boolean result = true;
        if (isNeedStandingSeat()) {
        }
        else {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    if ("无座".equals(trainticket.getSeatno())) {
                        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId()
                                + ":ishaveseatthis:包含无座");
                        result = false;
                    }
                }
            }

        }
        return result;
    }

    /**
     * 淘宝是否需要无座
     * 去备选坐席表里查如果有返回true 没有说明没有备选坐席返回false
     * @return  要 true false 不要
     * @time 2015年12月11日 下午9:29:31
     * @author chendong
     */
    @SuppressWarnings("rawtypes")
    public boolean isNeedStandingSeat() {
        try {
            String sql = "select ExtSeat from TrainOrderExtSeat WITH (NOLOCK) WHERE ORDERID=" + this.trainorderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                return true;
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("Error_TrainCreateOrder_isNeedStandingSeat", e);
        }
        return false;
    }

    /**
     * 说明:判断无座
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public boolean ishaveseatthis(Trainorder trainorder) {
        boolean result = true;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if ("无座".equals(trainticket.getSeatno())) {
                    WriteLog.write("ishaveseatthis", r1 + ":当前订单:" + trainorder.getId() + "包含无座");
                    if (this.trainOrderExtSeatMethods != null) {
                        for (TrainOrderExtSeatMethod trainOrderExtSeatMethod : trainOrderExtSeatMethods) {
                            WriteLog.write("ishaveseatthis", r1 + ":当前订单:" + trainorder.getId() + "---"
                                    + trainOrderExtSeatMethod.getSeat());
                            if ("无座".equals(trainOrderExtSeatMethod.getSeat())) {
                                return true;
                            }
                        }
                    }
                    result = false;
                }
            }
        }
        return result;
    }

    /**
     * 订单票信息是否完整
     *
     * @param trainorder
     * @return
     * @time 2015年1月5日 上午10:41:13
     * @author fiend
     */
    public boolean isallticketseat(Trainorder trainorder) {
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno() == null) {
                    WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前订单:" + trainorder.getId()
                            + ";ishaveseat:因坐席信息不全,无法直接出票");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 说明:得到订单差价
     *
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午3:29:14
     * @author yinshubin
     */
    public float getdirrerenceprice(Trainorder trainorder) {
        float differenceprice = 0f;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                differenceprice += trainticket.getPayprice() - trainticket.getPrice();
            }
        }
        return differenceprice;
    }

    /**
     * 说明:通过12306账号比对未完成订单
     *
     * @param customeruserId
     * @param pid
     * @param pname
     * @param pidtype
     * @return boolean
     * @time 2014年8月30日 上午11:15:55
     * @author yinshubin
     */
    @SuppressWarnings("unchecked")
    public boolean isalignmentTrue(long customeruserId, String pid, String pname, int pidtype) {
        boolean result = false;
        String sql = "SELECT * FROM T_CUSTOMERPASSENGER a JOIN T_CUSTOMERCREDIT b ON a.ID=b.C_REFID WHERE a.C_CUSTOMERUSERID="
                + customeruserId
                + " AND a.C_USERNAME='"
                + pname
                + "' AND b.C_CREDITTYPEID="
                + pidtype
                + " AND b.C_CREDITNUMBER='" + pid + "' AND b.C_STAUS=1 AND a.C_STATE=1";
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前的乘客是否在该12306中:SQL:" + sql);
        List<Customerpassenger> customerpassengerList = Server.getInstance().getMemberService()
                .findAllCustomerpassengerBySql(sql, -1, 0);
        WriteLog.write("JobGenerateOrder_MyThread", r1 + ":当前的乘客是否在该12306中:" + customerpassengerList.size());
        if (1 == customerpassengerList.size()) {
            result = true;
        }
        return result;
    }

    /**
     * 账号是否释放
     * @param msg
     * @param user
     * @return
     * @time 2015年11月10日 下午8:35:12
     * @author chendong
     */
    private boolean isHavefree(String msg, Customeruser user) {
        boolean havefree = false;
        if (msg.indexOf("取消次数过多") > 0) {// 取消次数过多换账号
            havefree = true;
            isenableTodaycustomeruser(user);
        }
        else if (msg.indexOf("账号尚未通过身份信息核验") > 0) {// 您的账号尚未通过身份信息核验
            havefree = true;
            isEnableForeverCustomeruser(user, msg);
        }
        else if (msg.indexOf("您在12306网站注册时填写信息有误") > 0) {//您在12306网站注册时填写信息有误
            WriteLog.write("您在12306网站注册时填写信息有误", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 31);
        }
        else if (msg.indexOf("您注册的信息与其他用户重复") > 0 || (msg.indexOf("您账户中的信息与其他用户重复") > 0)) {//您注册的信息与其他用户重复
            WriteLog.write("您注册的信息与其他用户重复", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 32);
        }
        else if (msg.indexOf("手机核验") > 0) {//手机核验
            WriteLog.write("手机核验", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 33);
        }
        else if (msg.indexOf("您注册时的手机号码被多个用户使用") > 0) {//您注册时的手机号码被多个用户使用
            WriteLog.write("您注册时的手机号码被多个用户使用", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 34);
            //                        isEnableForeverCustomeruser(user, msg);
        }
        else if (msg.indexOf("您需要提供真实、准确的本人资料，为了保障您的个人信息安全，请您到就近办理客运售票业务的铁路车站完成身份核验") > 0) {//您需要提供真实、准确的本人资料，为了保障您的个人信息安全，请您到就近办理客运售票业务的铁路车站完成身份核验
            WriteLog.write("需要提供真实&准确的本人资料", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 39);
            //                        isEnableForeverCustomeruser(user, msg);
        }
        else if (msg.indexOf("您的身份信息涉嫌被他人冒用，为了保障您的个人信息安全，请您到就") > 0) {//您的身份信息涉嫌被他人冒用，为了保障您的个人信息安全，请您到就近办理客运售票业务的铁路车站完成注册用户身份核验，通过后即可在网上购票，谢谢您的理解和支持！
            WriteLog.write("您的身份信息涉嫌被他人冒用", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, 43);
            //                        isEnableForeverCustomeruser(user, msg);
        }
        else if (msg.contains("已满")) {//已满
            WriteLog.write("已满", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, AccountSystem.FreePassengerFull);
        }
        else if (msg.contains("未登录")) {//未登录
            WriteLog.write("未登录", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, AccountSystem.FreeNoLogin);
        }
        else if (msg.contains("操作乘车人过于频繁")) {//对不起，由于您操作乘车人过于频繁，请明日再试！
            WriteLog.write("操作乘车人过于频繁", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, AccountSystem.FreeCurrent);
        }
        else if (msg.contains("您填写的身份信息有误") && msg.contains("未能通过国家身份信息管理权威部门核验")) {
            //您填写的身份信息有误，未能通过国家身份信息管理权威部门核验，请检查您的姓名和身份证件号码填写是否正确。如有疑问，可致电12306客服咨询。
            WriteLog.write("未能通过国家身份信息管理权威部门核验", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            FreeNoPassState(user);
        }
        else if (msg.contains("验证码输入错误")) {//登录失败|验证码输入错误
            WriteLog.write("验证码输入错误", this.trainorderid + "--->" + user.getLoginname());
            havefree = true;
            isEnableCustomeruserByType(user, msg, AccountSystem.FreeNoCare);
        }
        return havefree;
    }

    /**
     *
     * @param QUEUE_NAME
     * @param message
     *
     * @time 2015年1月27日 上午11:48:04
     * @author chendong
     */
    public void sendMQmessage(String QUEUE_NAME, String message) {
        String url = getSysconfigString("activeMQ_url");
        // String url = "tcp://192.168.0.5:61616";
        try {
            new KongTieRabbtMQMethod().activeMQroordering(this.trainorderid);
            //            ActiveMQUtil.sendMessage(url, QUEUE_NAME, message);
        }
        catch (Exception e) {
        }
    }

    /**
     * 火车票系统投保
     *
     * @param ticket
     *            火车票bean
     * @return Map.Entry<Boolean, String>:key{true/false},value:{备注信息}
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map.Entry<Boolean, String> insure(long ticketid) {
        Trainticket ticket = Server.getInstance().getTrainService().findTrainticket(ticketid);
        boolean success = true;
        String msg = "";
        try {
            Trainpassenger passenger = ticket.getTrainpassenger();
            Trainorder order = passenger.getTrainorder();
            if (ticket.getInsurorigprice() > 0) {
                // / type 1。易订行火车票5元(未成年人)2。易订行火车票5元(成年人)3
                // 。易订行火车票20元(未成年人)4。易订行火车票20元(成年人)
                int type = ticket.getInsurorigprice() == 20f ? 4 : 2;
                Insuruser insur = new Insuruser();
                if (ticket.getInsureno() != null && ticket.getInsureno().indexOf("失败") > -1) {
                    String extno = ticket.getInsureno().split("\\|")[1];
                    if (!"null".equals(extno))
                        insur.setExtorderno(extno);
                }
                insur.setAgentid(order.getAgentid());
                insur.setCodetype(Long.valueOf(passenger.getIdtype()));
                insur.setCode(passenger.getIdnumber());
                insur.setFlytime(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm"));
                insur.setOrdernum(order.getOrdernumber());
                insur.setBirthday(formatStringToTime(passenger.getBirthday(), "yyyy-MM-dd"));
                insur.setName(passenger.getName());
                insur.setFlyno(ticket.getTrainno());
                insur.setMobile(order.getContacttel());
                List insurlist = new ArrayList();
                insurlist.add(insur);
                // 购买保险
                List<Insuruser> ins = Server.getInstance().getAtomService2()
                        .saveTrainOrderAplylist(null, insurlist, type);
                Insuruser in = ins.get(0);
                String extorderno = in.getExtorderno();
                if (isNotNullOrEpt(in.getPolicyno())) {
                    WriteLog.write("trainorder_insure", r1 + ":insure:" + passenger.getName() + "火车票购买保险投保成功,保单号::"
                            + in.getPolicyno());
                    msg = in.getPolicyno();
                    if (!isNotNullOrEpt(msg)) {
                        success = false;
                        msg = "投保保单号为空";
                    }
                }
                else {
                    success = false;
                    WriteLog.write("trainorder_insure",
                            r1 + ":insure:" + passenger.getName() + "火车票购买保险投保失败:" + in.getPolicyno() + in.getRemark());
                    msg = "旅客" + passenger.getName() + "投保失败:" + in.getRemark();
                    Trainticket tticket = new Trainticket();
                    tticket.setInsureno("失败|" + extorderno);
                    tticket.setId(ticket.getId());
                    Server.getInstance().getTrainService().updateTrainticket(tticket);
                }

            }
        }
        catch (Exception e) {
            success = false;
            msg = "系统投保异常";
            WriteLog.write("trainorder_insure", r1 + ":insure:" + "火车票购买保险异常:" + e.fillInStackTrace());
        }
        Map<Boolean, String> m = new HashMap<Boolean, String>();
        m.put(success, msg);
        Map.Entry<Boolean, String> me = m.entrySet().iterator().next();
        return me;
    }

    /**
     * 时间转换
     *
     * @param date
     * @param format
     * @return
     * @time 2014年10月9日 下午6:45:34
     * @author yinshubin
     */
    public Timestamp formatStringToTime(String date, String format) {
        try {
            SimpleDateFormat simplefromat = new SimpleDateFormat(format);
            return new Timestamp(simplefromat.parse(date).getTime());

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 日期转换
     *
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatchinaMMdd(Timestamp date) {
        return (new SimpleDateFormat("MM月dd日").format(date));
    }

    /**
     * 日期转换
     *
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatTimestampHHmm(Timestamp date) {
        try {
            return (new SimpleDateFormat("HH:mm").format(date));

        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取短信模板(写死的17,获取火车票模板)
     *
     * @param dnsmaintenance
     * @return
     * @time 2014年10月9日 下午6:46:30
     * @author yinshubin
     */
    @SuppressWarnings("unchecked")
    public String getSMSTemplet(Dnsmaintenance dnsmaintenance) {
        try {
            List<Templet> list = Server
                    .getInstance()
                    .getMemberService()
                    .findAllTemplet("where id=" + 17 + " and c_state=1 and  c_agentid=" + dnsmaintenance.getAgentid(),
                            "", -1, 0);
            if (list != null && list.size() > 0) {
                return list.get(0).getTempletmess();
            }
            else {
                WriteLog.write("trainorder_insure", r1 + ":dnsmaintenance.getAgentid():" + dnsmaintenance.getAgentid()
                        + ";smstype:" + 17);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {

        }
        return "";
    }

    /**
     * 获取分销商对象
     *
     * @param agentid
     * @return
     * @time 2014年10月9日 下午6:47:06
     * @author yinshubin
     */
    @SuppressWarnings("rawtypes")
    public Dnsmaintenance getSmsDnsByAgentid(long agentid) {
        String sql = "SELECT  C_AGENTID agentid, C_B2BSMSCOUNTER smscounter,C_B2BSMSPWD smspwd,C_AGENTSMSENABLE agentsmsenable FROM T_DNSMAINTENANCE WHERE C_AGENTID= "
                + agentid
                + "OR CHARINDEX(','+CONVERT(NVARCHAR,C_AGENTID)+',',(SELECT ','+C_PARENTSTR+',' FROM T_CUSTOMERAGENT WHERE ID="
                + agentid + "))>0 " + "ORDER BY C_AGENTID DESC";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            Map m = (Map) list.get(0);
            try {
                return (Dnsmaintenance) setFiledfrommap(Dnsmaintenance.class, m);
            }
            catch (Exception e) {
                return null;
            }

        }
        return null;
    }

    /**
     * 数据库RESULTMAP转换成类对象
     *
     * @param t
     * @param map
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws NoSuchFieldException
     * @time 2014年10月9日 下午6:47:29
     * @author yinshubin
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T> T setFiledfrommap(Class t, Map map) throws SecurityException, NoSuchMethodException,
            IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException,
            NoSuchFieldException {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        T tt = (T) t.newInstance();
        for (Map.Entry<String, String> entry = null; iterator.hasNext();) {
            entry = iterator.next();
            String paraname = entry.getKey();
            Object val = entry.getValue();
            paraname = paraname.substring(0, 1).toUpperCase() + paraname.substring(1);
            Method getm = t.getMethod("get" + paraname);
            String type = getm.getReturnType().getSimpleName();
            if (type.equals("Integer") || type.equals("int")) {
                try {
                    val = Integer.valueOf(val.toString());
                }
                catch (Exception ex) {
                    // System.out.println(ex.getMessage());
                    val = 0;
                }

            }
            else if (type.equals("long") || type.equals("Long")) {
                try {
                    val = Long.valueOf(converNull(val, '0').toString());
                }
                catch (Exception ex) {
                    // System.out.println(ex.getMessage());
                    val = 0l;
                }

            }
            else if (type.equals("float") || type.equals("Float")) {
                try {
                    val = Float.valueOf(val.toString());
                }
                catch (Exception ex) {
                    // System.out.println(ex.getMessage());
                    val = 0f;
                }
            }
            else if (type.equals("byte") || type.equals("Byte")) {
                try {
                    val = Byte.valueOf(val.toString());
                }
                catch (Exception ex) {
                    // System.out.println(ex.getMessage());
                    val = 0f;
                }
            }
            Method method = t.getMethod("set" + paraname, getm.getReturnType());

            method.invoke(tt, val);

        }
        return tt;
    }

    public <T> T converNull(T t, T v) {
        if (t != null) {
            return t;
        }
        return v;
    }

    /**
     * 出票后发送短信和投保
     *
     * @param trainorderid
     * @time 2014年10月9日 上午10:12:23
     * @author yinshubin
     */
    public void sendmessage(long trainorderid, String loginname) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        try {
            Dnsmaintenance dns = getSmsDnsByAgentid(trainorder.getAgentid());
            String smstemple = getSMSTemplet(dns);// 单程
            if (isNotNullOrEpt(smstemple)) {
                for (Trainpassenger passenger : trainorder.getPassengers()) {
                    for (Trainticket ticket : passenger.getTraintickets()) {
                        // 订单号[订单号],[联系人]您已购[日期][车次][车厢][席位][出发站][出发时间]开。请尽快换取纸质车票。
                        String sms = smstemple.replace("[订单号]", trainorder.getExtnumber());
                        sms = sms.replace("[联系人]", passenger.getName() + "先生/女士");
                        sms = sms.replace("[日期]",
                                formatchinaMMdd(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm")));
                        sms = sms.replace("[车次]", ticket.getTrainno());
                        sms = sms.replace("[车厢]", ticket.getCoach());
                        sms = sms.replace("[席位]", ticket.getSeatno());
                        sms = sms.replace("[出发站]", ticket.getDeparture());
                        sms = sms.replace("[到达站]", ticket.getArrival());
                        sms = sms.replace("[出发时间]",
                                formatTimestampHHmm(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm")));
                        WriteLog.write("火车票短信", r1 + ":" + trainorder.getId() + ":火车票出票短信:" + sms);
                        String mobiles[] = { trainorder.getAgentcontacttel() };
                        Server.getInstance().getAtomService2()
                                .sendSms(mobiles, sms, trainorder.getId(), trainorder.getAgentid(), dns, 3);
                    }
                }
            }
            else {
                WriteLog.write("火车票短信", r1 + ":" + trainorder.getId() + ":火车票出票短信模板不存在");
            }
        }
        catch (Exception e) {
            WriteLog.write("火车票短信", r1 + ":" + trainorder.getId() + "短信异常:" + e.fillInStackTrace());
        }
        // 投保
        List<Map.Entry<Boolean, String>> issuelist = new ArrayList<Map.Entry<Boolean, String>>();
        for (Trainpassenger passenger : trainorder.getPassengers()) {
            for (Trainticket ticket : passenger.getTraintickets()) {
                if (ticket.getInsurorigprice() > 0) {
                    passenger.setTrainorder(trainorder);
                    ticket.setTrainpassenger(passenger);
                    Map.Entry<Boolean, String> m = insure(ticket.getId());
                    if (m.getKey()) {
                        String insureno = m.getValue();
                        Trainticket insurticket = new Trainticket();// 因Hessian
                        // 报异常。故此处重new
                        insurticket.setId(ticket.getId());
                        insurticket.setInsureno(insureno);
                        Server.getInstance().getTrainService().updateTrainticket(insurticket);
                    }
                    issuelist.add(m);
                }
            }
        }
        JSONArray array = JSONArray.fromObject(issuelist);
        createTrainorderrc(
                trainorderid,
                array.toString().replace("\"key\"", "").replace("\"value\"", "").replace(":", "")
                        .replace("true", "投保成功").replace("false", "投保失败"), loginname, 3);
    }

    /**
     * 判断数据库的车次和12306返回的车次是否一致
     * 说明:保存订单信息比对车次
     *
     * @param traincode
     * @param trainno
     * @return
     * @time 2014年10月6日 上午8:26:05
     * @author yinshubin
     */
    public boolean comparisonTraincode(String traincode, String trainno) {
        boolean result = false;
        if (trainno.contains("/")) {
            String[] trainnos = trainno.split("/");
            for (int i = 0; i < trainnos.length; i++) {
                result = traincode.equals(trainnos[i]);
                if (result) {
                    break;
                }
            }
        }
        else {
            result = traincode.equals(trainno);
        }
        return result;
    }

    /**
     * 说明:根据12306要求,将数据库中日期转变格式
     *
     * @param date
     * @return date
     * @time 2014年8月30日 上午11:18:41
     * @author yinshubin
     */
    public String changeDate(String date) {
        try {
            Date date_result = new Date();
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
            date_result = df.parse(date);
            DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
            date = dfm.format(date_result);
        }
        catch (ParseException e1) {
            e1.printStackTrace();
        }
        return date;
    }

    /**
     * 说明:得到拒单原因
     *
     * @param state
     * @return
     * @time 2014年9月1日 下午2:50:26
     * @author yinshubin
     */
    public static String refusereason(int state) {
        if (state == 1) {
            return "所购买的车次坐席已无票";
        }
        else if (state == 2) {
            return "身份证件已经实名制购票,不能再次购买同日期同车次的车票";
        }
        else if (state == 3) {
            return "qunar票价和12306不符";
        }
        else {
            return "";
        }
    }

    //    public boolean isrefuse(long orderid, int state) {
    //        boolean result = false;
    //        result = QunarTrainMethod.trainIssueOrRefuse(orderid, state);
    //        return result;
    //    }

    // /**
    // * 拒单原因-状态: 0 其他
    // 1 所购买的车次坐席已无票
    // 2 身份证件已经实名制购票,不能再次购买同日期同车次的车票
    // 3 qunar票价和12306不符
    // 4 车次数据与12306不一致
    // 5 乘客信息错误
    // 6 12306乘客身份信息核验失败
    // */
    // private Integer refundreason;

    /**
     * 说明:生成下单失败的解决办法
     *
     * @param result
     * @param loginname
     * @param trainorder
     * @return
     * @time 2014年8月30日 上午11:42:56
     * @author yinshubin
     */
    public String saveSolution(String result, String loginname, Trainorder trainorder) {
        String solution = TrainCreateOrderUtil.getSolution(result);
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(loginname + "下单失败:" + solution);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:" + result);
        }
        return "";
    }

    /**
     * 根据12306返回的tickets,获取到对应人的票的信息,并且把人放进去 暂不用
     *
     * @param trainpassengers
     * @param tickets
     * @param isjointrip
     * @time 2014年12月29日 下午5:49:51
     * @author chendong
     */
    private void updatepassengerfrom12306jsonbypassenger(List<Trainpassenger> trainpassengers, JSONArray tickets,
            TrainOrderReturnBean returnob, boolean isjointrip, boolean isEnNeedStarttimeEquils) {
        int k = isjointrip ? 0 : 1;
        List<Long> ticketidList = new ArrayList<Long>();
        String costtime = "";
        String arrivaltime = "";
        String departtime = "";
        //存储支付超时时间
        boolean savePayTimeOut = true;
        for (int i = 0; i < tickets.size(); i++) {
            JSONObject jsonoticket = tickets.getJSONObject(i);
            int ticket_type_code = jsonoticket.getInt("ticket_type_code");// 乘客类型(票种)
            JSONObject jsonopassengerDTO = jsonoticket.getJSONObject("passengerDTO");
            String ticket_no = jsonoticket.getString("ticket_no");
            String coach_no = jsonoticket.getString("coach_no");
            String seat_name = jsonoticket.getString("seat_name");
            String seat_no = jsonoticket.getString("seat_no");
            String seat_type_name = jsonoticket.getString("seat_type_name");
            Float str_ticket_price_page = Float.valueOf(jsonoticket.getString("str_ticket_price_page"));
            String passenger_id_no = jsonopassengerDTO.getString("passenger_id_no");
            departtime = jsonoticket.getString("start_train_date_page");
            try {
                costtime = returnob.getRuntime();
                arrivaltime = getArrivalTime(departtime, costtime);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("updatepassengerfrom12306jsonbypassenger_err", e);
            }
            if (ticket_type_code == 1 || ticket_type_code == 3 || ticket_type_code == 4) {//成人、学生  ，残军
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);// 获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if (trainticket.getTickettype() == ticket_type_code && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        // trainticket =
                        // Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        //12306支付超时时间,存储一次即可
                        if (savePayTimeOut) {
                            String pay_limit_time = jsonoticket.getString("pay_limit_time");
                            TrainPayTimeRule.savePayTimeOut(this.trainorder.getQunarOrdernumber(), pay_limit_time);
                            savePayTimeOut = false;
                        }
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        if ("一等软座".equals(seat_type_name) || "二等软座".equals(seat_type_name)
                                || seat_type_name.contains("动卧") || seat_type_name.contains("动软")
                                || seat_type_name.contains("高级动软")) {
                            trainticket.setSeattype(seat_type_name);
                        }
                        if (!ElongHotelInterfaceUtil.StringIsNull(seat_name)) {
                            trainticket.setSeatno(seat_name);
                        }
                        else {
                            if (seat_type_name.contains("卧")) {
                                if (seat_no.endsWith("3")) {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "上铺");
                                }
                                else if (seat_no.endsWith("2")) {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "中铺");
                                }
                                else {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "下铺");
                                }
                            }
                            else {
                                if (seat_no.startsWith("3")) {
                                    trainticket.setSeatno("无座");
                                }
                                else {
                                    trainticket.setSeatno(seat_no + "号");
                                }
                            }
                        }
                        trainticket.setPrice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        ticketidList.add(trainticket.getId());
                        break;// 找到这个人了就不往下走了,继续找
                    }
                }
            }
            else if (ticket_type_code == 2) {// 儿童
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);// 获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if ((trainticket.getTickettype() == ticket_type_code || trainticket.getTickettype() == 0)
                            && (trainticket.getSeatno() == null || "".equals(trainticket.getSeatno()))
                            && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        // trainticket =
                        // Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        if ("一等软座".equals(seat_type_name) || "二等软座".equals(seat_type_name)
                                || seat_type_name.contains("动卧") || seat_type_name.contains("动软")
                                || seat_type_name.contains("高级动软")) {
                            trainticket.setSeattype(seat_type_name);
                        }
                        if (!ElongHotelInterfaceUtil.StringIsNull(seat_name)) {
                            trainticket.setSeatno(seat_name);
                        }
                        else {
                            if (seat_type_name.contains("卧")) {
                                if (seat_no.endsWith("3")) {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "上铺");
                                }
                                else if (seat_no.endsWith("2")) {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "中铺");
                                }
                                else {
                                    trainticket.setSeatno(seat_no.substring(0, seat_no.length() - 1) + "下铺");
                                }
                            }
                            else {
                                if (seat_no.startsWith("3")) {
                                    trainticket.setSeatno("无座");
                                }
                                else {
                                    trainticket.setSeatno(seat_no + "号");
                                }
                            }
                        }
                        trainticket.setPrice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        ticketidList.add(trainticket.getId());
                        break;// 找到这个人了就不往下走了,继续找
                    }
                    else {
                        continue;// 找不到这个人就继续走
                    }
                }
            }

        }
        String ticketidsString = "";
        for (int i = 0; i < ticketidList.size(); i++) {
            ticketidsString += ticketidList.get(i);
            if (i != ticketidList.size() - 1) {
                ticketidsString += ",";
            }
        }
        WriteLog.write("历时", this.trainorderid + ":isEnNeedStarttimeEquils:" + isEnNeedStarttimeEquils
                + ":ticketidsString:" + ticketidsString + ":isEnNeedStarttimeEquils:" + isEnNeedStarttimeEquils
                + ":costtime:" + costtime + ":arrivaltime:" + arrivaltime + ":departtime:" + departtime);
        //        isEnNeedStarttimeEquils:true:ticketidsString:62287108true:costtime:-1:arrivaltime::departtime:2015-12-14 12:16
        if (isEnNeedStarttimeEquils && isNotNullOrEpt(ticketidsString) && isNotNullOrEpt(costtime)
                && isNotNullOrEpt(arrivaltime) && isNotNullOrEpt(departtime)) {
            String sql = "UPDATE T_TRAINTICKET SET C_ARRIVALTIME='" + arrivaltime + "',C_DEPARTTIME='" + departtime
                    + "',C_COSTTIME='" + costtime + "' WHERE ID IN (" + ticketidsString + ")";
            WriteLog.write("历时", this.trainorderid + ":" + sql);
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }

    }

    /**
     * qunar代付
     *
     * @return
     * @time 2015年1月21日 下午5:53:36
     * @author fiend
     */
    public boolean qunarPay() {
        boolean isresult = false;
        String name = "qunarpay";
        int open = 0;//去哪代付开关，DB配置。0：关闭，1：开启。
        try {
            String sql = "[dbo].[sp_T_qunarpay_select] @name=" + name;
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                String qunarpay = map.get("C_VALUE").toString().trim();
                if (qunarpay != null && !"".equals(qunarpay)) {
                    open = Integer.parseInt(qunarpay);
                }
            }
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        if (open == 1) {
            String url = this.qunarPayurl;
            JSONObject jso = new JSONObject();
            jso.put("trainorderid", this.trainorderid);
            jso.put("method", "qunartrain_pay");
            try {
                WriteLog.write("JobGenerateOrder_MyThread_qunarPay", r1 + ":url:" + url + ":" + jso.toString());
                String result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
                WriteLog.write("JobGenerateOrder_MyThread_qunarPay", r1 + ":" + result);
                if ("success".equalsIgnoreCase(result)) {
                    isresult = true;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return isresult;
    }

    /**
     * 书写操作记录
     *
     * @param content
     * @param createurser
     * @time 2015年1月21日 下午7:05:04
     * @author fiend
     */
    public void writeRC(String content, String createurser) {
        try {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(this.trainorder.getId());
            rc.setContent(content);
            rc.setStatus(Trainticket.ISSUED);
            rc.setCreateuser(createurser);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", this.trainorder.getId() + ":content:" + content);
        }
    }

    /**
     * 补单订单进入下单队列后，调清除缓存中补单ID队列
     *
     * @time 2015年3月13日 下午4:10:53
     * @author fiend
     */
    public void toBudanRemoveId() {
        try {
            new TrainActiveMQ(this.trainorderid, TrainInterfaceMethod.WITHHOLDING_AFTER == this.interfacetype ? 2 : 1)
                    .sendBudanRemoveIdMQ();
            WriteLog.write("id_仍入清除缓存中订单ID队列", "仍入成功--->" + this.trainorderid);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getArrivalTime(String time1, String time2) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat sdf_arrival = new SimpleDateFormat("HH:mm");
        String[] dates = time2.split(":");
        int i = Integer.parseInt(dates[0]);
        int j = Integer.parseInt(dates[1]);
        Date a = sdf.parse(time1);
        long millis = a.getTime() + i * 60 * 60 * 1000 + j * 60 * 1000;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        return sdf_arrival.format(cal.getTime());
    }

    //=========================================================备选坐席下单================================================
    /**
     * 初始化备选坐席
     */
    @SuppressWarnings("rawtypes")
    public void initTrainOrderExtSeat() {
        String sql = "SELECT ExtSeat FROM TrainOrderExtSeat WITH(NOLOCK) WHERE OrderId=" + this.trainorderid;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            String string = map.get("ExtSeat").toString();
            if (string != null && !"".equals(string)) {
                JSONArray jsonArray = JSONArray.fromObject(string);
                List<TrainOrderExtSeatMethod> trainOrderExtSeatMethodList = new ArrayList<TrainOrderExtSeatMethod>();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    TrainOrderExtSeatMethod trainOrderExtSeatMethod = new TrainOrderExtSeatMethod();
                    Iterator it = jsonObject.keys();
                    // 遍历jsonObject数据，添加到Map对象
                    while (it.hasNext()) {
                        String key = String.valueOf(it.next());
                        Float value = (float) jsonObject.getDouble(key);
                        trainOrderExtSeatMethod.setSeat(key);
                        trainOrderExtSeatMethod.setPrice(value);
                        trainOrderExtSeatMethod.setCreated(false);
                        trainOrderExtSeatMethodList.add(trainOrderExtSeatMethod);
                    }
                }
                changeTrainOrderExtSeatMethods(trainOrderExtSeatMethodList);
            }
        }
    }

    /**
     * 将坐席转换
     * @param trainOrderExtSeatMethodList
     */
    private void changeTrainOrderExtSeatMethods(List<TrainOrderExtSeatMethod> trainOrderExtSeatMethodList) {
        for (TrainOrderExtSeatMethod trainOrderExtSeatMethod : trainOrderExtSeatMethodList) {
            if (isCanChangeSeat(trainOrderExtSeatMethod)) {
                trainOrderExtSeatMethod.setSeat(QunarTrainRule.changeSeatQunar2DB(trainOrderExtSeatMethod.getSeat()));
                if ("无座".equals(trainOrderExtSeatMethod.getSeat())) {
                    trainOrderExtSeatMethod.setCreated(true);
                }
                this.trainOrderExtSeatMethods.add(trainOrderExtSeatMethod);
            }
        }
    }

    /**
     * 是否可以将qunar坐席转换成DB坐席
     * 硬卧上5 硬卧中6 软卧上8 高级软卧上10 不允许转换
     * @param trainOrderExtSeatMethod
     * @return
     */
    private boolean isCanChangeSeat(TrainOrderExtSeatMethod trainOrderExtSeatMethod) {
        String[] disChangeSeat = { "5", "6", "8", "10" };
        for (int i = 0; i < disChangeSeat.length; i++) {
            if (disChangeSeat[i].equals(trainOrderExtSeatMethod.getSeat())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 下单为无座票后是否可以继续使用备选坐席下单
     * @return
     */
    private boolean isCanExtCreateOrderByStandingSeat() {
        if (this.trainOrderExtSeatMethods != null) {
            for (TrainOrderExtSeatMethod trainOrderExtSeatMethod : trainOrderExtSeatMethods) {
                if ("无座".equals(trainOrderExtSeatMethod.getSeat())) {
                    return false;
                }
            }
            for (TrainOrderExtSeatMethod trainOrderExtSeatMethod : trainOrderExtSeatMethods) {
                if (!trainOrderExtSeatMethod.isCreated()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param ticket
     * @param passengersstr
     * @param user
     * @param result
     * @param t1
     * @param from_station
     * @param to_station
     * @return
     */
    private String orderRoundByAlternativeAgents(Trainticket ticket, String passengersstr, Customeruser user,
            String result, long t1, String from_station, String to_station) {
        try {
            if (this.trainOrderExtSeatMethods != null) {
                for (TrainOrderExtSeatMethod trainOrderExtSeatMethod : this.trainOrderExtSeatMethods) {
                    if (!trainOrderExtSeatMethod.isCreated()) {
                        trainOrderExtSeatMethod.setCreated(true);
                        JSONArray jsonArray = JSONArray.fromObject(passengersstr);
                        JSONArray array = new JSONArray();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            jsonObject.put("price", trainOrderExtSeatMethod.getPrice());
                            jsonObject.put("zwcode", TrainSupplyMethod.getzwname(trainOrderExtSeatMethod.getSeat()));
                            array.add(jsonObject);
                        }
                        passengersstr = array.toString();
                        changeTicketSeatAndPrice(trainOrderExtSeatMethod);
                        this.ordererrorcodenum = 0;
                        return orderRound(ticket, passengersstr, user, result, t1, from_station, to_station, 1, 0);
                    }
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("Error_TrainCreateOrder_orderRoundByAlternativeAgents", this.trainorderid + "");
            ExceptionUtil.writelogByException("Error_TrainCreateOrder_orderRoundByAlternativeAgents", e);
        }
        refuse(trainorder.getId(), 1, user, result);
        freeCustomeruser(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                AccountSystem.NullDepartTime);
        return result;
    }

    /**
     * 修改DB中票种类型和价格
     * @param trainOrderExtSeatMethod
     * @throws Exception
     */
    private void changeTicketSeatAndPrice(TrainOrderExtSeatMethod trainOrderExtSeatMethod) throws Exception {
        String ticketids = "";
        for (int i = 0; i < this.trainorder.getPassengers().size(); i++) {
            Trainpassenger trainpassenger = this.trainorder.getPassengers().get(i);
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                ticketids += trainticket.getId();
                if (i != (this.trainorder.getPassengers().size() - 1)) {
                    ticketids += ",";
                }
            }
        }
        String sqlString = "UPDATE T_TRAINTICKET SET C_PAYPRICE=" + trainOrderExtSeatMethod.getPrice() + ",C_PRICE="
                + trainOrderExtSeatMethod.getPrice() + ",C_SEATTYPE='" + trainOrderExtSeatMethod.getSeat()
                + "' WHERE ID in(" + ticketids + ")";
        Server.getInstance().getSystemService().findMapResultBySql(sqlString, null);
    }

    /**
     * 无座时取消未支付订单>>包含释放账号逻辑
     */
    private boolean cancelTrainorderByStandingSeat(String str, Customeruser user, boolean freeAccount) {
        try {
            //取消订单
            return cancelTrainorder(extnumberByStandingSeat(str), user, freeAccount);
        }
        catch (Exception e) {
            //日志
            WriteLog.write("ERROR_TrainCreateOrder_cancelTrainorderByStandingSeat", "" + this.trainorderid);
            ExceptionUtil.writelogByException("ERROR_TrainCreateOrder_cancelTrainorderByStandingSeat", e);
            //释放账号
            if (freeAccount) {
                freeCustomeruser(user, AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                        AccountSystem.NullDepartTime);
            }
            //返回结果
            return false;
        }
    }

    /**
     * 无座时获取12306订单号
     * @param str
     * @return
     */
    private String extnumberByStandingSeat(String str) throws Exception {
        JSONObject jsono = JSONObject.fromObject(str);
        if (jsono.has("data")) {
            JSONObject jsonodata = jsono.getJSONObject("data");
            if (jsonodata.has("orderDBList")) {
                JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                    JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                    if (jsonoorderDBList.toString().contains("待支付")) {// 拿到那个唯一的待支付订单
                        WriteLog.write("TrainCreateOrder_extnumberByStandingSeat", r1 + ":订单号:" + trainorderid
                                + ":下单成功,获取待支付订单信息:" + jsonoorderDBList.toString());
                        return jsonoorderDBList.getString("sequence_no");
                    }
                }
            }
        }
        return "";
    }

    /**
     * 取消未支付订单>>包含释放账号逻辑
     * @param freeAccount true：释放账号
     */
    private Boolean cancelTrainorder(String extnumber, Customeruser user, boolean freeAccount) {
        //结果
        boolean cancelSuccess = false;
        //非空
        if (!ElongHotelInterfaceUtil.StringIsNull(extnumber)) {
            String result = cancel12306Order(user, extnumber, this.trainorderid);
            if (result.contains("取消订单成功") || "无未支付订单".equals(result)) {
                cancelSuccess = true;
            }
        }
        //释放账号
        if (freeAccount) {
            freeCustomeruser(user, cancelSuccess ? AccountSystem.FreeNoCare : AccountSystem.FreeCurrent,
                    AccountSystem.OneFree, AccountSystem.OneCancel, AccountSystem.NullDepartTime);
        }
        //不释放账号>>取消成功>>非客人订单，重取账号再释放
        else if (cancelSuccess && this.trainorder.getOrdertype() != 3 && this.trainorder.getOrdertype() != 4) {
            Trainorder trainorderelse = new Trainorder();
            trainorderelse.setId(this.trainorderid);
            trainorderelse.setSupplyaccount(user.getLoginname());
            trainorderelse.setAgentid(this.trainorder.getAgentid());
            trainorderelse.setOrdertype(this.trainorder.getOrdertype());
            trainorderelse.setInterfacetype(this.trainorder.getInterfacetype());
            trainorderelse.setQunarOrdernumber(this.trainorder.getQunarOrdernumber());
            Customeruser temp = getCustomerUserBy12306Account(trainorderelse, false);
            freeCustomeruser(temp, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.OneCancel,
                    AccountSystem.NullDepartTime);
        }
        //返回结果
        return cancelSuccess;
    }

    /**
     * 是否可以下单
     * @return
     */
    public String isCanCreate() {
        String modifyresult = "-1";
        boolean iscancreate = true;
        try {
            List<String> keyList = new ArrayList<String>();
            WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "");
            for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
                Trainticket trainticket = trainpassenger.getTraintickets().get(0);
                if (trainticket.getTickettype() != 2) {
                    String keyString = trainticket.getDeparture() + trainticket.getArrival()
                            + trainticket.getDeparttime().substring(0, 10) + trainticket.getTrainno()
                            + trainticket.getSeattype() + trainpassenger.getIdnumber();
                    String key = ElongHotelInterfaceUtil.MD5(keyString);
                    WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "--->MD5==>" + keyString + ":"
                            + key);
                    String valueString = OcsMethod.getInstance().get(key);
                    if ("".equals(valueString)) {
                        boolean isadd = OcsMethod.getInstance().add(key, "true", 12 * 60);
                        WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "--->isadd==>key:" + key
                                + ":" + isadd);
                        if (isadd) {
                            keyList.add(key);
                        }
                    }
                    else {
                        modifyresult = "提交订单失败：" + trainpassenger.getName() + "(" + trainpassenger.getIdtypestr() + "-"
                                + trainpassenger.getIdnumber() + ")已订" + trainticket.getDeparttime().substring(0, 4)
                                + "年" + trainticket.getDeparttime().substring(5, 7) + "月"
                                + trainticket.getDeparttime().substring(8, 10) + "日" + trainticket.getTrainno()
                                + "次的车票!!";
                        iscancreate = false;
                        break;
                    }
                }
            }
            if (!iscancreate && keyList.size() > 0) {
                for (String removeKey : keyList) {
                    boolean isremove = OcsMethod.getInstance().remove(removeKey);
                    WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "--->isremove==>key:"
                            + removeKey + ":" + isremove);
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("ERROR_TrainCreateOrder_isCanCreate", this.trainorderid + "");
            ExceptionUtil.writelogByException("ERROR_TrainCreateOrder_isCanCreate", e);
        }
        return modifyresult;
    }

    /**
     * 下单后移除信息
     */
    public void removeCanCreate() {
        try {
            WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + ":end");
            for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
                Trainticket trainticket = trainpassenger.getTraintickets().get(0);
                if (trainticket.getTickettype() != 2) {
                    String keyString = trainticket.getDeparture() + trainticket.getArrival()
                            + trainticket.getDeparttime().substring(0, 10) + trainticket.getTrainno()
                            + trainticket.getSeattype() + trainpassenger.getIdnumber();
                    String key = ElongHotelInterfaceUtil.MD5(keyString);
                    WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "--->endMD5==>" + keyString
                            + ":" + key);
                    boolean isremove = OcsMethod.getInstance().remove(key);
                    WriteLog.write("TrainCreateOrder_isCanCreate", this.trainorderid + "--->endisremove==>key:" + key
                            + ":" + isremove);
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("ERROR_TrainCreateOrder_isCanCreate", this.trainorderid + "");
            ExceptionUtil.writelogByException("ERROR_TrainCreateOrder_isCanCreate", e);
        }
    }

    /**
     * 说明:获取支付宝支付订单页面
     * @param isjointrip 是否为联程
     * @return
     * @time 2014年8月30日 上午11:20:49
     * @author yinshubin
     */
    public boolean orderpayment(Customeruser user) {
        boolean resultmsg = false;
        int randomnum = new Random().nextInt(10000);
        WriteLog.write(
                "12306支付宝自动支付链接_下单成功后",
                randomnum + ":" + user.getLoginname() + ":cookie:" + user.getCardnunber() + ":"
                        + trainorder.getOrdernumber() + ",");
        if (user == null || user.getCardnunber() == null || user.getCardnunber().equals("")) {
            return false;
        }
        String loginname = user.getLoginname();
        int isjointrip = trainorder.getIsjointtrip() == null ? 0 : trainorder.getIsjointtrip();//是否为联程
        String extnumber = trainorder.getExtnumber();// 12306订单号
        float priceAll = 0;
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                priceAll += trainticket.getPrice();
            }
        }
        try {
            if (isjointrip == 0) {
                long t1 = System.currentTimeMillis();
                String result = "";
                for (int i = 0; i < 5; i++) {
                    //Cookie
                    String cookieString = user.getCardnunber();
                    //为空
                    boolean cookieIsNull = ElongHotelInterfaceUtil.StringIsNull(cookieString);
                    //非空
                    if (!cookieIsNull) {
                        cookieString = URLEncoder.encode(cookieString, "UTF-8");
                        //REP
                        RepServerBean rep = RepServerUtil.getRepServer(user, false);
                        //地址
                        String url = rep.getUrl();
                        //参数
                        String par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber
                                + "&trainorderid=" + trainorder.getId() + "&price=" + priceAll
                                + JoinCommonAccountInfo(user, rep);
                        WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":超时机制循环次数：" + i + ":par:" + par + ""
                                + trainorder.getOrdernumber() + ":支付宝访问地址:" + url);
                        result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 60000).toString();
                        result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                        //                    result = HttpsClientUtils.gethttpclientdata(url + "?" + par, 30000l);
                        WriteLog.write("12306支付宝自动支付链接_下单成功后",
                                randomnum + ":循环次数：" + i + ":" + trainorder.getOrdernumber() + "支付宝返回数据:" + result);
                        //切换REP
                        if (result.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                            //切换REP
                            rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                            //类型正确
                            if (rep.getType() == 1) {
                                //REP地址
                                url = rep.getUrl();
                                //重拼参数
                                par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber
                                        + "&trainorderid=" + trainorder.getId() + "&price=" + priceAll
                                        + JoinCommonAccountInfo(user, rep);
                                //记录日志
                                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":超时机制循环次数：" + i + ":par:" + par
                                        + "" + trainorder.getOrdernumber() + ":切换支付宝访问地址:" + url);
                                //重新请求
                                result = SendPostandGet.submitPostTimeOutFiend(url, par, "UTF-8", 60000).toString();
                                result = ElongHotelInterfaceUtil.StringIsNull(result) ? "" : result;
                                //记录日志
                                WriteLog.write("12306支付宝自动支付链接_下单成功后",
                                        randomnum + ":循环次数：" + i + ":" + trainorder.getOrdernumber() + "支付宝返回数据:"
                                                + result);
                            }
                        }
                    }
                    if (cookieIsNull || Account12306Util.accountNoLogin(result, user)) {
                        //释放
                        if (cookieIsNull) {
                            FreeNoCare(user);
                        }
                        else {
                            FreeNoLogin(user);
                        }
                        //等待
                        Thread.sleep(1000);
                        //重拿
                        user = getCustomerUserBy12306Account(trainorder, true);
                    }
                    else if (result.indexOf("gateway.do") >= 0) {
                        break;
                    }
                    else if (result.indexOf("价格不正确") >= 0) {
                        break;
                    }
                }
                long t2 = System.currentTimeMillis();
                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":获取支付链接用时:" + (t2 - t1) + "--->result:" + result);
                if (result.indexOf("gateway.do") >= 0) {
                    float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                    trainorder.setSupplyprice(supplyprice);
                    trainorder.setSupplypayway(7);
                    trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                    trainorder.setChangesupplytradeno(result);
                    WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + "存储支付信息正确" + trainorder.getOrdernumber()
                            + "," + loginname + "," + extnumber);
                    resultmsg = true;
                }
            }
            else if (isjointrip == 2) {//联程暂时不处理
                WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + trainorder.getOrdernumber() + "联程订单不处理");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("12306支付宝自动支付链接_下单成功后", randomnum + ":" + "存储支付信息错误:" + e);
        }
        return resultmsg;
    }

    /**
     * 很抱歉！当前提交订单用户过多，请您稍后重试。
     * 订单号放入内存
     *
     * @time 2015年11月19日 下午2:04:09
     * @author fiend
     */
    private void addTooManyUsersSubmit() {
        try {
            //默认放15分钟
            OcsMethod.getInstance().add(default_pingtaiStr + this.trainorderid, "true", 60 * 15);
            WriteLog.write("too_many_users_submit_add", "订单号:" + this.trainorderid + "--->" + default_pingtaiStr
                    + this.trainorderid);
        }
        catch (Exception e) {
            WriteLog.write("error_too_many_users_submit_add", "订单号:" + this.trainorderid);
            ExceptionUtil.writelogByException("error_too_many_users_submit_add", e);
        }
    }

    /**
     * getsp_QunarTrainRule_SELECT这个方法访问数据库了不能放到静态qunarTrainRule用静态方法使用
     * @time 2015年11月26日 下午2:24:36
     * @author chendong
     */
    @SuppressWarnings("rawtypes")
    private int getsp_QunarTrainRule_SELECT(Trainorder trainorder) {
        int payRule = QunarTrainRule.QUNAR_PAY_FAIL_SYS_PAY;
        String sql = " sp_QunarTrainRule_SELECT @AgentId=" + trainorder.getAgentid();
        WriteLog.write("qunar支付规则", trainorder.getId() + ":" + Server.getInstance().getSystemService() + "--->" + sql);
        try {
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            if (list.size() > 0) {
                payRule = Integer.valueOf(((Map) list.get(0)).get("PayRule").toString());
            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        WriteLog.write("qunar支付规则", trainorder.getId() + ":--->" + payRule);
        return payRule;
    }

    /**
     *  获取手机核验结果
     *
     * @param cus
     * @return  -2,-1,0,1 重试  2成功  3失败   4放弃
     * @time 2015年9月30日 下午2:15:17
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private int mobilePhoneVerificationResult(Customeruser cus) {
        String insersql = "[sp_CustomeruserCheckMobile_Select] @loginname='" + cus.getLoginname() + "'";
        try {
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(insersql);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                String status = map.get("status") == null ? "-1" : map.get("status").toString();
                try {
                    return Integer.valueOf(status);
                }
                catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("ERROR_mobilePhoneVerificationResult", insersql);
            ExceptionUtil.writelogByException("ERROR_mobilePhoneVerificationResult", e);
        }
        return -2;
    }

    /**
     * 放弃手机核验
     *
     * @time 2015年9月30日 下午1:28:59
     * @author fiend
     */
    private void mobilePhoneVerificationFree(Customeruser cus) {
        String insersql = "[sp_CustomeruserCheckMobile_Free] @loginname='" + cus.getLoginname() + "' ";
        try {
            Server.getInstance().getSystemService().findMapResultByProcedure(insersql);
            WriteLog.write("mobilePhoneVerificationFree", insersql);
        }
        catch (Exception e) {
            WriteLog.write("ERROR_mobilePhoneVerificationFree", insersql);
            ExceptionUtil.writelogByException("ERROR_mobilePhoneVerificationFree", e);
        }
    }

    /**
     * 手机核验
     *
     * @time 2015年9月30日 下午1:28:59
     * @author fiend
     */
    private void mobilePhoneVerification(Customeruser cus) {
        String insersql = "[sp_CustomeruserCheckMobile_INSERT] @UserId=" + cus.getId() + ", @loginname='"
                + cus.getLoginname() + "' ,@loginPass='" + cus.getLogpassword() + "' ,@cookieString='"
                + cus.getCardnunber() + "'";
        try {
            Server.getInstance().getSystemService().findMapResultByProcedure(insersql);
        }
        catch (Exception e) {
            WriteLog.write("ERROR_mobilePhoneVerification", insersql);
            ExceptionUtil.writelogByException("ERROR_mobilePhoneVerification", e);
        }
    }

    /**
     * 获取核验的最长的时间
     * @return
     * @time 2015年11月26日 下午4:58:28
     * @author chendong
     */
    private long getVertimesMax() {
        long vertimesMax = 10 * 60 * 1000;//最多核验10分钟
        try {
            //去手机核验的最大时长
            String phoneVerTime = getSysconfigStringbydb("phoneVerTime");
            if (phoneVerTime != null && Integer.valueOf(phoneVerTime) > 0) {
                vertimesMax = Integer.valueOf(phoneVerTime) * 60 * 1000;
            }
        }
        catch (NumberFormatException e) {
        }
        return vertimesMax;
    }

    /**
     * 挨个乘客去ocs中判断是否是冒用
     * @return
     * @time 2015年11月26日 下午4:46:02
     * @author chendong
     * @param user
     */
    private boolean getisFalsely_isMaoyong(Customeruser user) {
        boolean isFalsely = false;
        //挨个乘客去ocs中判断是否是冒用
        for (Trainpassenger trainpassenger : this.trainorder.getPassengers()) {
            //            String key = "falsely_" + trainpassenger.getIdnumber();
            //            String value = OcsMethod.getInstance().get(key);
            try {
                long idNumber = Long.parseLong(trainpassenger.getIdnumber().toUpperCase().replace("X", "10"));
                String sql = "SELECT * FROM FalselyPassenger WITH(NOLOCK) WHERE IdNumber=" + idNumber;
                DataTable result = DBHelperAccount.GetDataTable(sql);
                List<DataRow> dataRows = result.GetRow();
                boolean value = false;
                if (dataRows.size() > 0) {
                    value = true;
                }
                if (value) {
                    WriteLog.write("手机核验_判断是否乘客有冒用嫌疑", this.trainorderid + "--->" + user.getLoginname() + "--->"
                            + idNumber + "--->" + value);
                    isFalsely = true;
                    break;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return isFalsely;
    }

    /**
     * 初始化ordermaxerroe
     *
     * @time 2015年12月3日 下午2:59:04
     * @author fiend
     */
    public void initOrderMaxErroe() {
        String key = ORDER_MAX_ERROE_STR + default_pingtaiStr;
        String str = OcsMethod.getInstance().get(key);
        if (str == null || "".equals(str)) {
            this.ordermaxerroe = Integer.valueOf(getSysconfigStringbydb("ordermaxerroe"));
            OcsMethod.getInstance().add(key, this.ordermaxerroe + "");
        }
        else {
            int sum = -2;
            try {
                sum = Integer.valueOf(str);
            }
            catch (Exception e) {
            }
            if (sum == -2) {
                WriteLog.write("false_initOrderMaxErroe", str);
                this.ordermaxerroe = Integer.valueOf(getSysconfigStringbydb("ordermaxerroe"));
                OcsMethod.getInstance().add(key, this.ordermaxerroe + "");
            }
            else {
                this.ordermaxerroe = sum;
            }
        }
    }

    /**
     * 初始化是否下单过程中绑定乘客到DB中
     *
     * @time 2015年12月4日 下午5:28:14
     * @author fiend
     */
    public void initBindingPassengers() {
        try {
            List<Trainpassenger> passengers = this.trainorder.getPassengers();
            for (int i = 0; i < passengers.size(); i++) {
                Trainticket tk = passengers.get(i).getTraintickets().get(0);
                if (tk.getTickettype() == 3) {
                    //包含学生票、儿童的不走下单过程中绑定乘客
                    return;
                }
            }
            Server.getInstance().getSystemService()
                    .findMapResultByProcedure(" [sp_TrainOrderBinding_Insert] @OrderId=" + this.trainorderid);
        }
        catch (Exception e) {
            WriteLog.write("Error_initBindingPassengers", "" + this.trainorderid);
            ExceptionUtil.writelogByException("Error_initBindingPassengers", e);
        }
    }

    /**
     * 将315的乘客同步推送给同程
     *
     * @param user
     * @param trainpassenger
     * @time 2015年11月17日 下午5:49:28
     * @author w.c.l
     */
    public static void checkAccountMethod(Customeruser user, String passengers, String idNumber) {
        WriteLog.write("TongchengSupplyMethod_checkAccountMethod", "user:" + user.getId() + ":passengers:" + passengers);
        Trainpassenger trainpassenger = new Trainpassenger();
        List<Trainticket> traintickets = new ArrayList<Trainticket>();
        Trainticket trainticket = new Trainticket();
        com.alibaba.fastjson.JSONArray passengerList = com.alibaba.fastjson.JSONArray.parseArray(passengers);
        int passengerSize = passengerList.size();
        for (int i = 0; i < passengerSize; i++) {
            com.alibaba.fastjson.JSONObject passenger = passengerList.getJSONObject(i);
            if (idNumber.equals(passenger.get("passenger_id_no"))) {
                trainpassenger.setName(passenger.getString("passenger_name"));
                trainpassenger.setIdnumber(idNumber);
                trainpassenger.setIdtype(Integer.parseInt(passenger.getString("passenger_id_type_code")));
                trainticket.setTickettype(Integer.parseInt(passenger.getString("ticket_type")));
                traintickets.add(trainticket);
                trainpassenger.setTraintickets(traintickets);
                //将冒用的放入挡板库中
                try {
                    idNumber = trainpassenger.getIdnumber().toUpperCase().replace("X", "10");
                    String sql = "INSERT INTO FalselyPassenger (name,IdNumber) VALUES ('"
                            + passenger.getString("passenger_name") + "'," + Long.parseLong(idNumber) + ")";
                    DBHelperAccount.executeSql(sql);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        Customeruser temp = new Customeruser();
        temp.setId(user.getId());
        temp.setIsenable(user.getIsenable());
        temp.setLogpassword("315");

        List<Trainpassenger> trainpassengers = new ArrayList<Trainpassenger>();
        trainpassengers.add(trainpassenger);
        CallBackPassengerUtil.callBackTongcheng(temp, trainpassengers, 2);
    }

    /**
     * 取消12306订单公共方法
     */
    private String cancel12306OrderPhone(Customeruser user, String extnumber, long trainorderid) {
        //结果
        String result = "";
        //处理
        try {
            //Cookie
            String cookie = user.getCardnunber();
            //非空
            if (!ElongHotelInterfaceUtil.StringIsNull(extnumber)) {
                //REP
                RepServerBean rep = RepServerUtil.getRepServer(user, false);
                //参数
                String param = "datatypeflag=1010&cookie=" + cookie + "&extnumber=" + extnumber + "&trainorderid="
                        + trainorderid + JoinCommonAccountInfo(user, rep) + JoinCommonAccountPhone(user);
                //请求
                result = SendPostandGet.submitPost(rep.getUrl(), param, "UTF-8").toString();
                //记录日志
                WriteLog.write("TrainCreateOrderSupplyMethod_rep1010Method", trainorderid + "-->" + rep.getUrl()
                        + "-->" + result);
                //取消订单成功，一天只能取消3次
                if (result.contains("取消订单成功")) {
                    result = "取消订单成功，一天只能取消3次";
                }
                WriteLog.write("TrainCreateOrderSupplyMethod_rep1010Method", trainorderid + "-->" + result);
            }
        }
        catch (Exception e) {
        }
        //返回结果
        return result;
    }

    /**
     * DB控制是否走手机端下单
     *
     * @param orderid
     * @return
     * @time 2016年3月10日 下午2:06:27
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public boolean isPhoneOrder(long orderid) {
        try {
            List list = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(" [Select_TrainOrderIsPhone] @OrderId=" + orderid);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                if ("1".equals(map.get("IsPhone").toString())) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("PHONE_ERROR_isPhoneOrder", e);
        }
        return false;
    }

    /**
     * 请求REP账号手机端数据
     */
    public String JoinCommonAccountPhone(Customeruser user) {
        return "&accountPhone=" + CommonAccountPhone(user);
    }

    /**
     * 请求REP账号手机端数据
     */
    public String CommonAccountPhone(Customeruser user) {
        try {
            JSONObject obj = new JSONObject();
            //账号相关
            obj.put("account", user.getLoginname());
            obj.put("password", user.getLogpassword());
            obj.put("Cookie", user.getCardnunber());
            obj.put("WL-Instance-Id", user.getSessionid());
            obj.put("__wl_deviceCtxSession", user.getWldevicectxsession());
            obj.put("baseDTO.device_no", user.getDeviceno());
            obj.put("startTime", System.currentTimeMillis());
            return URLEncoder.encode(obj.toString(), "UTF-8");
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("PHONE_ERROR_请求REP账号手机端数据", e);
            return "";
        }
    }

    /**
     * 将帐号被封的乘客同步给美团
     *
     * @param user
     * @param msg
     * @time 2016年3月14日 下午3:52:16
     * @author wang.cheng.liang
     */
    public void disposeMsgCheckAcount(Customeruser user, String msg) {
        if (msg.contains("您填写的身份信息有误") || msg.contains("未能通过国家身份信息管理权威部门核验") || msg.indexOf("账号尚未通过身份信息核验") > -1
                || msg.indexOf("您的身份信息未经核验") > -1 || msg.contains("手机核验") || msg.contains("您在12306网站注册时填写信息有误")
                || msg.indexOf("您需要提供真实、准确的本人资料，为了保障您的个人信息安全，请您到就近办理客运售票业务的铁路车站完成身份核验") > -1) {
            new Thread(new CheckAccountAndSendMeiTuanPassengerThread(user)).start();
        }
    }

    /**
     * 创建火车票备选坐席
     *
     * @param orderid
     * @param extseats
     */
    private void createTrainOrderExtSeat(long orderid, String extseats) {
        try {
            String sql = "INSERT INTO TrainOrderExtSeat (OrderId ,ExtSeat ,ReMark) VALUES ( " + orderid + ",'"
                    + extseats + "' ,'')";
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
        catch (Exception e) {
            WriteLog.write("Error_MyThreadQunarOrder_createTrainOrderExtSeat", orderid + "");
            ExceptionUtil.writelogByException("Error_MyThreadQunarOrder_createTrainOrderExtSeat", e);
        }
    }

    /**
     * 賬號密碼模式，不再重試，直接拒單
     *
     * @param CustomerAccount
     * @param result
     * @return
     * @time 2016年3月17日 上午10:32:35
     * @author fiend
     */
    private boolean customerAccountEnNeedLoginAgain(boolean CustomerAccount, String result) {
        if (CustomerAccount) {
            if (result.contains("登录名不存在") || result.contains("密码输入错误") || result.contains("邮箱不存在")
                    || result.contains("用户名不能全为数字") || result.contains("手机号码尚未进行核验，目前暂无法用于登录")
                    || result.contains("注册的信息与其他用户重复") || result.contains("手机核验") || result.contains("您需要提供真实、准确的本人资料")
                    || result.contains("您账户中的信息与其他用户重复") || result.contains("您注册时的手机号码被多个用户使用")
                    || result.contains("您填写的身份信息有误，未能通过国家身份信息管理权威部门核验") || result.contains("手机号码被多个用户使用")
                    || result.contains("您的身份信息未通过核验") || result.contains("该用户已被暂停使用")) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否可以走手机端
     *
     * @return
     * @time 2017年3月27日 下午5:38:04
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private boolean canUsePhone() {
        boolean canUse = false;
        try {
            List list = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(" [sp_TrainOrderIsPhone_update] @OrderId=" + this.trainorderid);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                if (map.containsKey("IsPhone") && "1".equals(map.get("IsPhone").toString())) {
                    canUse = true;
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainCreateOrderSupplyMethod_canUsePhone_Exception", e,
                    this.trainorderid + "");
        }
        WriteLog.write("TrainCreateOrderSupplyMethod_canUsePhone", this.trainorderid + "--->" + canUse);
        return canUse;
    }

}
