package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;

//create table T_TRAINORDERTIMEOUT
//(  
//  C_ORDERID  decimal(18, 0),  订单号
//  C_STATE int 当前处理状态 0 尚未处理 1 下单成功已经处理 2 下单失败已经处理 
//)  
public class TongchengTrainOrderTimeout extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub
        String ids = getTrainorderIDs();
        if (!"".equals(ids.trim())) {
            createTrainorderTimeout(ids);
            changeToWaitorder(ids);
        }
    }

    /**
     * 获取满足条件的IDs 
     * @return 如果没有满足的 返回""；
     * @time 2015年1月10日 下午5:25:20
     * @author fiend
     */
    public String getTrainorderIDs() {
        String sql = "SELECT ID FROM T_TRAINORDER WHERE C_ORDERSTATUS=2 AND C_ISQUESTIONORDER=2 AND C_CONTROLNAME IS NULL AND C_CREATETIME<'"
                + getCreateTime(45) + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String ids = "";
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            ids += map.get("ID").toString() + ",";
        }
        if (!"".equals(ids.trim())) {
            ids = ids.substring(0, ids.length() - 1);
        }
        return ids;
    }

    /**
     * 修改订单，将订单转为可以下单
     * @time 2015年1月10日 下午5:26:24
     * @author fiend
     */
    public void changeToWaitorder(String ids) {
        String sql = "UPDATE T_TRAINORDER SET C_STATE12306=1,C_ORDERSTATUS=1,C_ISQUESTIONORDER=0,C_AUTOUNIONPAYURL=null,C_EXTNUMBER=null WHERE ID IN ("
                + ids + ")";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    /**
     * 创建超时记录(先删除再创建) 
     * @param ids
     * @time 2015年1月10日 下午5:57:44
     * @author chendong
     */
    public void createTrainorderTimeout(String ids) {
        String sql = "DELETE FROM T_TRAINORDERTIMEOUT WHERE C_ORDERID IN (" + ids + ")";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        sql = "INSERT INTO T_TRAINORDERTIMEOUT(C_ORDERID,C_STATE) VALUES(" + ids.replace(",", ",0),(") + ",0)";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    public static void main(String[] args) {
        TongchengTrainOrderTimeout ttot = new TongchengTrainOrderTimeout();
        String ids = ttot.getTrainorderIDs();
        if (!"".equals(ids.trim())) {
            ttot.createTrainorderTimeout(ids);
            ttot.changeToWaitorder(ids);
        }
    }
}
