package com.ccservice.inter.job.train;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.insuruser.Insuruser;
import com.ccservice.b2b2c.base.rebaterecord.Rebaterecord;
import com.ccservice.b2b2c.base.templet.Templet;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.ben.Payresult;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrderOtherMothed;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtil;

public class TrainSupplyMethod extends SupplyMethod {

    /**
     * 客人cookie模式
     * @author WH
     * @time 2017年7月12日 上午10:04:17
     * @param orderType 订单类型>>4（线上订单）或6（约票订单）：客人cookie模式
     * @version 1.0
     */
    private boolean customerCookieModel(int orderType) {
        return orderType == 4 || orderType == 6;
    }

    /**
     * 判断走APP端
     * @param goApp 业务逻辑传来的标识
     */
    private boolean isGoApp(boolean... goApp) {
        return goApp != null && goApp.length == 1 && goApp[0];
    }

    /**
     * 判断走APP端
     * @author WH
     * @time 2017年7月24日 下午3:59:30
     * @version 1.0
     * @param trainOrder 订单（使用其订单类型和代理ID）
     * @param businessType 业务类型，如改签、退票等，暂退票、改签使用，参考AppBusinessType类！！！
     * @param oldGoApp 原来是否走APP端
     * @return true：走APP
     */
    @SuppressWarnings("rawtypes")
    public boolean goApp(Trainorder trainOrder, int businessType, Boolean oldGoApp) {
        //走APP
        boolean goApp = false;
        //原来走APP
        if (oldGoApp != null && oldGoApp) {
            //结果赋值
            goApp = true;//切到APP后一直是APP
        }
        //实时取DB数据
        else {
            try {
                //代理ID
                long agentId = trainOrder.getAgentid();
                //订单类型
                int orderType = trainOrder.getOrdertype();
                //非客人cookie模式、代理ID大于0
                if (!customerCookieModel(orderType) && agentId > 0) {
                    //查询SQL
                    String sql = "[TrainGoWebOrApp_Query] @orderType = " + orderType + ", @agentId = " + agentId
                            + ", @businessType = " + businessType;
                    //执行查询
                    List<Map<String, Object>> list = executeSQLReturn(sql);
                    //结果存在
                    if (list.size() > 0) {
                        //转换
                        Map map = (Map) list.get(0);
                        //取值
                        Object temp = map.get("goApp");
                        //判断
                        goApp = temp != null && "1".equals(temp.toString());
                    }
                }
            }
            catch (Exception e) {

            }
        }
        //返回结果
        return goApp;
    }

    /**
     * 执行sql返回 list<map>
     *  改签审核消费者db安全包
     * @param sql
     * @return
     */
    public static List<Map<String, Object>> executeSQLReturn(String sql) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            DataTable getDataTable = DBHelper.GetDataTable(sql, dbSourceEnum);
            List<DataRow> getRow = getDataTable.GetRow();
            for (DataRow dataRow : getRow) {
                Map<String, Object> map = new HashMap<String, Object>();
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    map.put(dataColumn.GetKey(), dataColumn.GetValue());
                }
                list.add(map);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainSupplyMethod_Exception", e, "executeSQLReturn方法错误>>>>>sql=" + sql);
        }
        return list;
    }

    /**
     * 请求REP账号公共参数
     */
    private String CommonAccountInfo(Customeruser user, RepServerBean rep) {
        try {
            JSONObject obj = new JSONObject();
            //REP相关
            obj.put("repType", rep.getType());
            obj.put("serverIp", rep.getServerIp());
            obj.put("serverPort", rep.getServerPort());
            obj.put("serverPassword", rep.getServerPassword());
            //账号相关
            obj.put("loginName", user.getLoginname());
            obj.put("loginPwd", user.getLogpassword());
            //取接口数据
            if (ElongHotelInterfaceUtil.StringIsNull(rep.getSpecial12306Ip())) {
                obj.put("login12306Ip", user.getPostalcode());
            }
            //取账号系统配置数据
            else {
                obj.put("login12306Ip", rep.getSpecial12306Ip());
            }
            //时间，手机端有用
            obj.put("startTime", System.currentTimeMillis());
            //参数编码
            return URLEncoder.encode(obj.toString(), "UTF-8");
        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 请求REP账号公共参数
     */
    public String JoinCommonAccountInfo(Customeruser user, RepServerBean rep) {
        //避免传递字符串的时候出现 "null"
        String browserValue = rep.getBrowserValue();
        if (browserValue == null) {
            browserValue = "";
        }
        return "&accountInfo=" + CommonAccountInfo(user, rep) + "&browserVersion=" + browserValue;
    }

    /**
     * 走账号系统
     * @author WH
     */
    public boolean GoAccountSystem() {
        return true;
        //String AccountOpen = getSysconfigString("12306AccountOpen");
        //return "1".equals(AccountOpen) || "-1".equals(AccountOpen);
    }

    /**
     * 账号系统系统获取账号
     * @author WH
     * @param type 1:获取下单账号；2:获取身份验证账号；3:账号名获取
     * @param name 12306账号名，type为3时有效，其他type可为空
     * @param waitWhenNoAccount 无账号的时候是否等待，type为1、2时有效
     * @param backup 备用字段
     */
    public Customeruser GetUserFromAccountSystem(int type, String name, boolean waitWhenNoAccount,
            Map<String, String> backup) {
        return Account12306Util.get12306Account(type, name, waitWhenNoAccount, backup);
    }

    /**
     * 账号系统释放12306账号
     * @author WH
     * @param user 12306账号
     * @param freeType 释放类型 1:NoCare；2:仅当天使用；3:发车时间后才可使用；4:分配给其他业务(暂未用)；其他详见AccountSystem类
     * @param freeCount 释放次数，1或2次
     * @param cancalCount 取消次数，用于取消时释放账号，其他业务必须传0
     * @param departTime 发车时间，freeType为3时有效，其他请设为空
     */
    public void FreeUserFromAccountSystem(Customeruser user, int freeType, int freeCount, int cancalCount,
            Timestamp departTime) {
        Account12306Util.free12306Account(user, freeType, freeCount, cancalCount, departTime,
                !AccountSystem.checkPassenger);
    }

    /**
     * 账号系统释放:不关心
     */
    public void FreeNoCare(Customeruser user) {
        if (user != null && user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
    }

    /**
     * 账号系统释放:未登录
     */
    public void FreeNoLogin(Customeruser user) {
        if (user != null && user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, AccountSystem.FreeNoLogin, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
    }

    /**
     * 账号系统释放:您填写的身份信息有误，未能通过国家身份信息管理权威部门核验，请检查您的姓名和身份证件号码填写是否正确。如有疑问，可致电12306客服咨询。  
     */
    public void FreeNoPassState(Customeruser user) {
        if (user != null && user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, 35, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
    }

    /**
     * 释放账号
     */
    public void freeCustomeruser(Customeruser user, int freeType, int freeCount, int cancalCount, Timestamp departTime) {
        if (user == null) {
            return;
        }
        //账号系统释放账号
        if (user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, freeType, freeCount, cancalCount, departTime);
        }
        else {
            try {
                String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME = '1' where id = " + user.getId();
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            }
            catch (Exception e) {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("customeruserObject", user);
            }
        }
    }

    /**
     * 这个账号取消三次后今天不能使用了 支付完成等调用
     * @author WH
     */
    public void isenableTodaycustomeruser(Customeruser user) {
        if (user == null) {
            return;
        }
        if (user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ThreeCancel,
                    AccountSystem.NullDepartTime);
        }
        else {
            try {
                String loginName = user.getLoginname();
                String sql = "UPDATE T_CUSTOMERUSER SET C_ISENABLE = 4 where C_LOGINNAME = '" + loginName + "' ";
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * 这个账号由于不能使用了把 isenable改为type表示禁用了
     * 31身份信息未通过核验
     * 32注册的用户与其他用户重复
     * 33手机核验
     * 34您注册时的手机号码被多个用户使用
     * 41备用1
     * 42备用2
     */
    public void isEnableCustomeruserByType(Customeruser user, String description, int type) {
        if (user == null) {
            return;
        }
        if (user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, type, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
        else {
            try {
                String sql = "UPDATE T_CUSTOMERUSER C_ISENABLE = " + type + " , C_STATE = 0, C_DESCRIPTION = '"
                        + description + "' where ID = " + user.getId();
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * 这个账号由于不能使用了把 isenable改为0表示禁用了
     */
    public void isEnableForeverCustomeruser(Customeruser user, String description) {
        if (user == null) {
            return;
        }
        if (user.isFromAccountSystem()) {
            FreeUserFromAccountSystem(user, AccountSystem.FreeNoCheck, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
        else {
            try {
                String sql = "UPDATE T_CUSTOMERUSER C_ISENABLE = 0 , C_STATE = 0, C_DESCRIPTION = '" + description
                        + "' where C_LOGINNAME = '" + user.getLoginname() + "'";
                Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * 确定订单归属
     */
    public int getOrderAttribution(long orderId) {
        try {
            long C_AGENTID = 0;
            int r1 = (int) (Math.random() * 10000);
            String sql = "[sp_T_TRAINORDER_selectOrderInterfaceInformationByOrderID] @ID=" + orderId;
            WriteLog.write("接口用户判断", r1 + "--->" + sql);

            List<Map<String, Object>> list1 = executeSQLReturn(sql);
            if (list1 != null && list1.size() > 0) {
                Map map = list1.get(0);
                //接口类型
                String C_INTERFACETYPE = map.get("C_INTERFACETYPE").toString();
                if (!"0".equals(C_INTERFACETYPE)) {
                    return Integer.valueOf(C_INTERFACETYPE);
                }
                else {
                    C_AGENTID = Long.parseLong(map.get("C_AGENTID").toString());
                }
            }

            String sql1 = "[sp_T_INTERFACEACCOUNT_SelectInterfaceTypeByAgentId] @C_AGENTID = " + C_AGENTID;
            WriteLog.write("接口用户判断", r1 + "--->" + sql1);

            List<Map<String, Object>> list = executeSQLReturn(sql);
            WriteLog.write("接口用户判断", r1 + "--->" + list.size());
            if (list.size() > 0) {
                Map map = list.get(0);
                WriteLog.write("接口用户判断", r1 + "--->" + map.get("C_INTERFACETYPE").toString());
                return Integer.valueOf(map.get("C_INTERFACETYPE").toString());
            }
            else {
                return 1;
            }
        }
        catch (Exception e) {
            return 1;
        }
    }

    /**
     * @param scid
     * @param scvalue
     * @time 2014年9月25日 下午12:07:03
     * @author yinshubin
     */
    //    public void changeSystemCofig(long scid, String scvalue) {
    //        Sysconfig sysconfig;
    //        if (scid > 0) {
    //            sysconfig = Server.getInstance().getSystemService().findSysconfig(scid);
    //            sysconfig.setId(sysconfig.getId());
    //            sysconfig.setValue(scvalue);
    //            Server.getInstance().getSystemService().updateSysconfig(sysconfig);
    //        }
    //    }
    //
    //    public long getSystemConfigId(String name) {
    //        List<Sysconfig> configs = Server.getInstance().getSystemService()
    //                .findAllSysconfig(" where c_name='" + name + "'", "", -1, 0);
    //        if (configs != null && configs.size() == 1) {
    //            Sysconfig config = configs.get(0);
    //            return config.getId();
    //        }
    //        return 0;
    //    }

    public String getDataString(String name, String value) {
        if (Server.getInstance().getDateHashMap().get(name) != null) {
            return Server.getInstance().getDateHashMap().get(name);
        }
        else {
            Server.getInstance().getDateHashMap().put(name, value);
            return value;
        }
    }

    public String setDataString(String name, String value) {
        Server.getInstance().getDateHashMap().put(name, value);
        return value;
    }

    /**
     * 公共发短信方法
     * @param content 内容
     * @param tetctelphonenum 手机号
     * @param agentid 1 
     */
    public boolean sendSmspublic(String content, String tetctelphonenum, long agentid) {
        Dnsmaintenance dns = Server.getInstance().getSystemService().findDnsmaintenance(1);
        WriteLog.write("tc提醒短信sendSmspublic", content);
        return Server.getInstance().getAtomService2()
                .sendSms(new String[] { "" + tetctelphonenum + "" }, content, 0, agentid, dns, 0);
    }

    /**
     * 刷新自动支付页面 
     * @param i
     * @time 2014年11月27日 下午2:18:53
     * @author fiend
     */
    public void fresh(int i) {
        String url = getSysconfigString("Reptile_traininit_url" + i);
        String par = "datatypeflag=25";
        try {
            SendPostandGet.submitPost(url, par, "UTF-8").toString();
        }
        catch (Exception e) {
            System.out.println("刷新支付宝失败");
        }
    }

    //    /**
    //     *  随机调取下单接口，获取12306-cookie所在IP
    //     * @return cookie所在IP
    //     * @time 2014年12月16日 下午12:07:53
    //     * @author fiend
    //     */
    //    public String randomIp() {
    //        //公共模块获取
    //        RepServerBean common = GetCommonRep(false);
    //        //获取到REP地址
    //        if (common != null && !ElongHotelInterfaceUtil.StringIsNull(common.getUrl())) {
    //            return common.getUrl();
    //        }
    //        //返回REP地址
    //        else {
    //            return OldRandomIp();
    //        }
    //    }
    //
    //    /**
    //     * 原随机REP，不走公共获取REP模块
    //     */
    //    private String OldRandomIp() {
    //        String RefundTicketRepUrl = getSysconfigString("RefundTicketRepUrl");
    //        if (!ElongHotelInterfaceUtil.StringIsNull(RefundTicketRepUrl) && !"-1".equals(RefundTicketRepUrl.trim())) {
    //            //原REP序列号
    //            long oldIdx = Server.getInstance().getTrainRepIdx();
    //            //REP序号+1
    //            Server.getInstance().setTrainRepIdx(oldIdx + 1);
    //            //取REP
    //            String[] urls = RefundTicketRepUrl.split("@");
    //            //返回REP
    //            return urls[(int) (oldIdx % urls.length)];
    //        }
    //        String canorderips = getSysconfigString("Reptile_traininit_strs");
    //        if (canorderips.equals("-1") || canorderips.trim().equals("")) {
    //            return getSysconfigString("Reptile_traininit_url");
    //        }
    //        String[] iscanorderip = canorderips.split(",");
    //        int i = (int) (Math.random() * iscanorderip.length);
    //        return getSysconfigString("Reptile_traininit_url" + iscanorderip[i]);
    //    }

    /**
     * 如果为空返回 "0"
     * 
     * @param object
     * @return
     * @time 2015年4月15日 下午1:57:54
     * @author chendong
     */
    public String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    /**
     * 根据12306账号的用户名获取获取CustomerUser
     * @param refreshCookie 是否刷新Cookie，客人Cookie方式有效
     * @param goApp 是否走手机端（不传则视为false），最多传一个，如果为true表示只取账号名称和密码（账号系统不走登录PC逻辑）
     */
    @SuppressWarnings("unchecked")
    public Customeruser getCustomerUserBy12306Account(Trainorder order, boolean refreshCookie, boolean... goApp) {
        //客人账号名和密码
        if (order.getOrdertype() == 3) {
            //取客人数据
            Customeruser temp = TrainCreateOrderOtherMothed.getTrainAccountSrcById(order.getId());
            //走账号系统
            return GetCustomerAccount(temp.getLoginname(), temp.getLogpassword());
        }
        //客人账号的Cookie
        else if (order.getOrdertype() == 4) {
            //刷新Cookie
            if (refreshCookie) {
                refreshCookieFromInterface(order);
            }
            //取客人数据
            Customeruser temp = TrainCreateOrderOtherMothed.getTrainAccountSrcById(order.getId());
            //虚拟一个账号
            return GetCustomerAccountByCookieWay(temp);
        }
        String AccountName = order.getSupplyaccount();
        AccountName = AccountName.split("/")[0];
        //走账号系统
        if (GoAccountSystem()) {
            //备用
            Map<String, String> backup;
            //非手机端
            if (!isGoApp(goApp)) {
                backup = AccountSystem.NullMap;
            }
            //走手机端
            else {
                //初始
                backup = new HashMap<String, String>();
                //虚拟
                backup.put("AccountVirtualCookie", "true");
            }
            //公共获取
            return GetUserFromAccountSystem(AccountSystem.LoginNameAccount, AccountName,
                    !AccountSystem.waitWhenNoAccount, backup);
        }
        Customeruser user = new Customeruser();
        String sql = "[sp_T_CUSTOMERUSER_selectTCustomeruserByLoginname] @C_LOGINNAME = '" + AccountName + "'";
        //        String sql = "SELECT top 1 * FROM T_CUSTOMERUSER WHERE C_LOGINNAME = '" + AccountName + "'";
        List<Customeruser> users = executeSQLReturnListCustomeruser(sql);
        if (users != null && users.size() > 0) {
            user = users.get(0);
            //重新登陆
            if (user.getState() == 0) {
                user = login12306(user);
            }
        }
        return user;
    }

    /**
     * 通过sql查找list<Customeruser>
     * @param sql
     * @return
     */
    private List<Customeruser> executeSQLReturnListCustomeruser(String sql) {

        List<Customeruser> list = new ArrayList<Customeruser>();
        try {

            DataTable getDataTable = DBHelper.GetDataTable(sql, dbSourceEnum);
            List<DataRow> getRow = getDataTable.GetRow();
            for (DataRow dataRow : getRow) {
                Customeruser customeruser = new Customeruser();
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    String key = dataColumn.GetKey();
                    Object getValue = dataColumn.GetValue();

                    if ("ID".equals(key) && getValue != null) {
                        customeruser.setId(Long.parseLong(getValue.toString()));
                    }
                    if ("C_MEMBERNAME".equals(key) && getValue != null) {
                        customeruser.setMembername(getValue.toString());
                    }
                    if ("C_LOGINNAME".equals(key) && getValue != null) {
                        customeruser.setLoginname(getValue.toString());
                    }
                    if ("C_LOGPASSWORD".equals(key) && getValue != null) {
                        customeruser.setLogpassword(getValue.toString());
                    }
                    if ("C_MEMBERSEX".equals(key) && getValue != null) {
                        customeruser.setMembersex(getValue.toString());
                    }
                    if ("C_MEMBEREMAIL".equals(key) && getValue != null) {
                        customeruser.setMemberemail(getValue.toString());
                    }
                    if ("C_STATE".equals(key) && getValue != null) {
                        customeruser.setState(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_TYPE".equals(key) && getValue != null) {
                        customeruser.setType(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_ISADMIN".equals(key) && getValue != null) {
                        customeruser.setIsadmin(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_BIRTHDAY".equals(key) && getValue != null) {
                        customeruser.setBirthday((Timestamp) getValue);
                    }
                    if ("C_MOBILE".equals(key) && getValue != null) {
                        customeruser.setMobile(getValue.toString());
                    }
                    if ("C_MEMBERFAX".equals(key) && getValue != null) {
                        customeruser.setMemberfax(getValue.toString());
                    }
                    if ("C_ISWEB".equals(key) && getValue != null) {
                        customeruser.setIsweb(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_MEMBERMOBILE".equals(key) && getValue != null) {
                        customeruser.setMembermobile(getValue.toString());
                    }
                    if ("C_ISENABLE".equals(key) && getValue != null) {
                        customeruser.setIsenable(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_MEMBERTYPE".equals(key) && getValue != null) {
                        customeruser.setMembertype(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_AGENTID".equals(key) && getValue != null) {
                        customeruser.setAgentid(Long.parseLong(getValue.toString()));
                    }
                    if ("C_MODIFYTIME".equals(key) && getValue != null) {
                        customeruser.setModifytime((Timestamp) getValue);
                    }
                    if ("C_MODIFYUSER".equals(key) && getValue != null) {
                        customeruser.setModifyuser(getValue.toString());
                    }
                    if ("C_CREATETIME".equals(key) && getValue != null) {
                        customeruser.setCreatetime((Timestamp) getValue);
                    }
                    if ("C_CREATEUSER".equals(key) && getValue != null) {
                        customeruser.setCreateuser(getValue.toString());
                    }
                    if ("C_DEPTID".equals(key) && getValue != null) {
                        customeruser.setDeptid(Long.parseLong(getValue.toString()));
                    }
                    if ("C_CARDTYPE".equals(key) && getValue != null) {
                        customeruser.setCardtype(Long.parseLong(getValue.toString()));
                    }
                    if ("C_CARDNUNBER".equals(key) && getValue != null) {
                        customeruser.setCardnunber(getValue.toString());
                    }
                    if ("C_WORKPHONE".equals(key) && getValue != null) {
                        customeruser.setWorkphone(getValue.toString());
                    }
                    if ("C_DESCRIPTION".equals(key) && getValue != null) {
                        customeruser.setDescription(getValue.toString());
                    }
                    if ("C_ENNAME".equals(key) && getValue != null) {
                        customeruser.setEnname(getValue.toString());
                    }
                    if ("C_NATIONALITY".equals(key) && getValue != null) {
                        customeruser.setNationality(getValue.toString());
                    }
                    if ("C_CHINAADDRESS".equals(key) && getValue != null) {
                        customeruser.setChinaaddress(getValue.toString());
                    }
                    if ("C_POSTALCODE".equals(key) && getValue != null) {
                        customeruser.setPostalcode(getValue.toString());
                    }
                    if ("C_TOTALSCORE".equals(key) && getValue != null) {
                        customeruser.setTotalscore(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_LEVEL".equals(key) && getValue != null) {
                        customeruser.setLevel(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_TICKETNUM".equals(key) && getValue != null) {
                        customeruser.setTicketnum(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_LIMITTICKETNUM".equals(key) && getValue != null) {
                        customeruser.setLimitticketnum(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_INTERTICKETNUM".equals(key) && getValue != null) {
                        customeruser.setInterticketnum(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_LIMITINTERTICKETNUM".equals(key) && getValue != null) {
                        customeruser.setLimitinterticketnum(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_LOGINNUM".equals(key) && getValue != null) {
                        customeruser.setLoginnum(Integer.parseInt(getValue.toString()));
                    }
                    if ("C_TICKETDISCOUNT".equals(key) && getValue != null) {
                        customeruser.setTicketDiscount(Float.parseFloat(getValue.toString()));
                    }
                    if ("C_INTERTICKETDISCOUNT".equals(key) && getValue != null) {
                        customeruser.setInterticketDiscount(Float.parseFloat(getValue.toString()));
                    }

                }
                list.add(customeruser);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainSupplyMethod_Exception", e,
                    "executeSQLReturnListCustomeruser方法错误>>>>>sql=" + sql);
        }
        return list;
    }

    //    /**
    //     * 公共模块获取REP地址
    //     * @param isDama 是否是打码
    //     */
    //    public RepServerBean GetCommonRep(boolean isDama) {
    //        return Server.getInstance().getTrain12306Service().commonRepServer(isDama);
    //    }

    /**
     * 重新登录12306的公用方法
     */
    public Customeruser login12306(Customeruser customeruser) {
        if (customeruser == null || customeruser.getId() <= 0) {
            return new Customeruser();
        }
        String url = "";
        //公共模块获取
        RepServerBean common = RepServerUtil.getRepServer(customeruser, true);
        //取值成功
        if (common != null && !ElongHotelInterfaceUtil.StringIsNull(common.getUrl())) {
            url = common.getUrl();
        }
        else {
            return customeruser;
            /*
            //获取REP服务器
            String RefundTicketRepUrl = getSysconfigString("RefundTicketRepUrl");
            //退票REP非空
            if (!ElongHotelInterfaceUtil.StringIsNull(RefundTicketRepUrl) && !"-1".equals(RefundTicketRepUrl.trim())) {
                //原REP序列号
                long oldIdx = Server.getInstance().getDamaRepIdx();
                //REP序号+1
                Server.getInstance().setDamaRepIdx(oldIdx + 1);
                //取REP
                String[] urls = RefundTicketRepUrl.split("@");
                //REP URL
                url = urls[(int) (oldIdx % urls.length)];
            }
            else {
                url = OldRandomIp();
            }
            */
        }
        //用户信息
        String logname = customeruser.getLoginname();
        String logpassword = customeruser.getLogpassword();
        //参数
        String param = "datatypeflag=12&logname=" + logname + "&logpassword=" + logpassword;
        //请求REP
        String result = SendPostandGet.submitPost(url, param, "UTF-8").toString();
        //结果
        if (result != null && result.contains("JSESSIONID")) {
            customeruser.setState(1);
            customeruser.setCardnunber(result);
            //Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
        }
        else {
            customeruser.setState(0);
            //Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
        }
        return customeruser;
    }

    /**
     * 火车票订单通过下单时间进行升序
     * @param list
     * @return
     * @time 2014年12月16日 下午4:02:22
     * @author fiend
     */
    public static List<Trainorder> sortUP(List<Trainorder> list) {
        TrainOrderChangeCompare sort = new TrainOrderChangeCompare();
        TrainOrderChangeCompare.sortASC = true;//升序   
        TrainOrderChangeCompare.sortBychangeordertime = true; //设置排序属性生效 
        Collections.sort(list, sort);
        return list;
    }

    /**
     * 修改Trainorder公用方法， 
     * @param trainorder    对象里面放其他修改信息
     * @param paysupplystatus   审核支付分类 可以为NULL
     * @param state12306    12306订单状态 可以为NULL
     * @param questionorder 问题订单类型 可以为NULL
     * @time 2014年12月24日 下午5:59:13
     * @author fiend
     */
    public void trainorderUpdate(Trainorder trainorder, Integer paysupplystatus, Integer state12306,
            Integer questionorder) {
        trainorder.setId(trainorder.getId());
        if (paysupplystatus != null) {//审核支付供应的时候，传审核错误原因
            trainorder.setPaysupplystatus(paysupplystatus);
        }
        if (state12306 != null) {//订单12306状态
            trainorder.setState12306(state12306);
        }
        if (questionorder != null) {//如果改为问题订单，传问题订单类型，否则传null
            trainorder.setIsquestionorder(questionorder);
        }
        Server.getInstance().getTrainService().updateTrainorder(trainorder);
    }

    /**
     * 座位代码转换12306
     * @param zwcode
     * @return
     * @time 2014年12月24日 上午11:15:33
     * @author wzc
     */
    public static String getzwname(String codename) {
        String str = "";
        if ("商务座".equals(codename)) {
            str = "9";
        }
        else if ("特等座".equals(codename)) {
            str = "P";
        }
        else if ("一等软座".equals(codename) || "一等座".equals(codename)) {
            str = "M";
        }
        else if ("二等软座".equals(codename) || "二等座".equals(codename)) {
            str = "O";
        }
        else if ("高级软卧".equals(codename)) {
            str = "6";
        }
        else if ("动卧".equals(codename)) {
            str = "F";
        }
        else if ("软卧".equals(codename)) {
            str = "4";
        }
        else if ("硬卧".equals(codename)) {
            str = "3";
        }
        else if ("软座".equals(codename)) {
            str = "2";
        }
        else if ("硬座".equals(codename)) {
            str = "1";
        }
        else if ("无座".equals(codename)) {
            str = "0";
        }
        return str;
    }

    /**
     * 证件类型本地转换12306代码
     * @param idtype
     * @return
     * @time 2014年12月24日 上午11:21:59
     * @author wzc
     */
    public static String getIdtype12306(int idtype) {
        switch (idtype) {
        case 1:
            return "1";
        case 3:
            return "B";
        case 4:
            return "C";
        case 5:
            return "G";
        }
        return "";
    }

    //    public static <T> T converNull(T t, T v) {
    //        if (t != null) {
    //            return t;
    //        }
    //        return v;
    //    }

    /**
     * 说明：判断无座 
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public static boolean ishaveseat(Trainorder trainorder) {
        boolean result = true;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno() != null && trainticket.getSeatno().equals("无座")) {
                    result = false;
                    WriteLog.write("JobGenerateOrder", "当前订单：" + trainorder.getId() + ";ishaveseat：因有无座票，无法直接出票");
                }
            }
        }
        return result;
    }

    /**
     * 时间转换 
     * @param date
     * @param format
     * @return
     * @time 2014年10月9日 下午6:45:34
     * @author yinshubin
     */
    public Timestamp formatStringToTime(String date, String format) {
        try {
            SimpleDateFormat simplefromat = new SimpleDateFormat(format);
            return new Timestamp(simplefromat.parse(date).getTime());

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatchinaMMdd(Timestamp date) {
        return (new SimpleDateFormat("MM月dd日").format(date));
    }

    /**
     * 获取短信模板（写死的17，获取火车票模板） 
     * @param dnsmaintenance
     * @return
     * @time 2014年10月9日 下午6:46:30
     * @author yinshubin
     */
    public String getSMSTemplet(Dnsmaintenance dnsmaintenance) {
        try {
            List<Templet> list = Server
                    .getInstance()
                    .getMemberService()
                    .findAllTemplet("where id=" + 17 + " and c_state=1 and  c_agentid=" + dnsmaintenance.getAgentid(),
                            "", -1, 0);
            if (list != null && list.size() > 0) {
                return list.get(0).getTempletmess();
            }
            else {
                WriteLog.write("trainorder_insure", "dnsmaintenance.getAgentid():" + dnsmaintenance.getAgentid()
                        + ";smstype:" + 17);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {

        }
        return "";
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatTimestampHHmm(Timestamp date) {
        try {
            return (new SimpleDateFormat("HH:mm").format(date));

        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 说明：联程票绑定第二段的12306账号 
     * @param name
     * @param idno
     * @param idtype
     * @time 2014年9月16日 下午5:44:54
     * @author yinshubin
     */
    public long join12306() {
        try {
            int agentid = Integer.valueOf(getSysconfigString("qunar_agentid"));
            String sql = "SELECT * FROM T_CUSTOMERUSER WHERE C_AGENTID= " + agentid
                    + " AND C_LOGINNUM<80 AND C_STATE= 1 AND (C_ENNAME IS NULL OR C_ENNAME='1')";
            List<Customeruser> customerusers = Server.getInstance().getMemberService()
                    .findAllCustomeruserBySql(sql, -1, 0);
            if (customerusers.size() > 0) {
                Customeruser customeruser = customerusers.get(new Random().nextInt(customerusers.size()));
                customeruser.setEnname("0");
                Server.getInstance().getMemberService().updateCustomeruser(customeruser);
                return customeruser.getId();
            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 说明：获取订单的JSONObject对象
     * @param url
     * @return  jsonObject
     * @time 2014年8月30日 上午11:05:53
     * @author yinshubin
     */
    public JSONObject getJson(URL url) {
        JSONObject jsonObject = null;
        URLConnection con = null;
        try {
            con = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String result = "";
            jsonObject = null;
            while (true) {
                result = br.readLine();
                if (result == null) {
                    break;
                }
                else {
                    //                    jsonObject = JSONObject.fromObject(result);
                    jsonObject = JSONObject.parseObject(result);
                    WriteLog.write("qunartrainorder", jsonObject + "");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                con.getInputStream().close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    /**
     * 获取分销商对象 
     * @param agentid
     * @return
     * @time 2014年10月9日 下午6:47:06
     * @author yinshubin
     */
    public Dnsmaintenance getSmsDnsByAgentid(long agentid) {
        String sql = "SELECT  C_AGENTID agentid, C_B2BSMSCOUNTER smscounter,C_B2BSMSPWD smspwd,C_AGENTSMSENABLE agentsmsenable FROM T_DNSMAINTENANCE WHERE C_AGENTID= "
                + agentid
                + "OR CHARINDEX(','+CONVERT(NVARCHAR,C_AGENTID)+',',(SELECT ','+C_PARENTSTR+',' FROM T_CUSTOMERAGENT WHERE ID="
                + agentid + "))>0 " + "ORDER BY C_AGENTID DESC";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            Map m = (Map) list.get(0);
            try {
                return (Dnsmaintenance) setFiledfrommap(Dnsmaintenance.class, m);
            }
            catch (Exception e) {
                return null;
            }

        }
        return null;
    }

    /**
     * 数据库RESULTMAP转换成类对象 
     * @param t
     * @param map
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws NoSuchFieldException
     * @time 2014年10月9日 下午6:47:29
     * @author yinshubin
     */
    public <T> T setFiledfrommap(Class t, Map map) throws SecurityException, NoSuchMethodException,
            IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException,
            NoSuchFieldException {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        T tt = (T) t.newInstance();
        for (Map.Entry<String, String> entry = null; iterator.hasNext();) {
            entry = iterator.next();
            String paraname = entry.getKey();
            Object val = entry.getValue();
            paraname = paraname.substring(0, 1).toUpperCase() + paraname.substring(1);
            Method getm = t.getMethod("get" + paraname, null);
            String type = getm.getReturnType().getSimpleName();
            if (type.equals("Integer") || type.equals("int")) {
                try {
                    val = Integer.valueOf(val.toString());
                }
                catch (Exception ex) {
                    //                    System.out.println(ex.getMessage());
                    val = 0;
                }

            }
            else if (type.equals("long") || type.equals("Long")) {
                try {
                    val = Long.valueOf(converNull(val, '0').toString());
                }
                catch (Exception ex) {
                    //                    System.out.println(ex.getMessage());
                    val = 0l;
                }

            }
            else if (type.equals("float") || type.equals("Float")) {
                try {
                    val = Float.valueOf(val.toString());
                }
                catch (Exception ex) {
                    //                    System.out.println(ex.getMessage());
                    val = 0f;
                }
            }
            else if (type.equals("byte") || type.equals("Byte")) {
                try {
                    val = Byte.valueOf(val.toString());
                }
                catch (Exception ex) {
                    //                    System.out.println(ex.getMessage());
                    val = 0f;
                }
            }
            Method method = t.getMethod("set" + paraname, getm.getReturnType());

            method.invoke(tt, val);

        }
        return tt;
    }

    public <T> T converNull(T t, T v) {
        if (t != null) {
            return t;
        }
        return v;
    }

    /**
     * 出票后发送短信和投保
     * @param trainorderid
     * @time 2014年10月9日 上午10:12:23
     * @author yinshubin
     */

    public void sendmessage(long trainorderid, String loginname) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        try {
            Dnsmaintenance dns = getSmsDnsByAgentid(trainorder.getAgentid());
            String smstemple = getSMSTemplet(dns);// 单程
            if (isNotNullOrEpt(smstemple)) {
                for (Trainpassenger passenger : trainorder.getPassengers()) {
                    for (Trainticket ticket : passenger.getTraintickets()) {
                        // 订单号[订单号],[联系人]您已购[日期][车次][车厢][席位][出发站][出发时间]开。请尽快换取纸质车票。
                        String sms = smstemple.replace("[订单号]", trainorder.getExtnumber());
                        sms = sms.replace("[联系人]", passenger.getName() + "先生/女士");
                        sms = sms.replace("[日期]",
                                formatchinaMMdd(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm")));
                        sms = sms.replace("[车次]", ticket.getTrainno());
                        sms = sms.replace("[车厢]", ticket.getCoach());
                        sms = sms.replace("[席位]", ticket.getSeatno());
                        sms = sms.replace("[出发站]", ticket.getDeparture());
                        sms = sms.replace("[到达站]", ticket.getArrival());
                        sms = sms.replace("[出发时间]",
                                formatTimestampHHmm(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm")));
                        WriteLog.write("火车票短信", trainorder.getId() + ":火车票出票短信:" + sms);
                        String mobiles[] = { trainorder.getAgentcontacttel() };
                        Server.getInstance().getAtomService2()
                                .sendSms(mobiles, sms, trainorder.getId(), trainorder.getAgentid(), dns, 3);
                    }
                }
            }
            else {
                WriteLog.write("火车票短信", trainorder.getId() + ":火车票出票短信模板不存在");
            }
        }
        catch (Exception e) {
            WriteLog.write("火车票短信", trainorder.getId() + "短信异常:" + e.fillInStackTrace());
        }
        // 投保
        List<Map.Entry<Boolean, String>> issuelist = new ArrayList<Map.Entry<Boolean, String>>();
        for (Trainpassenger passenger : trainorder.getPassengers()) {
            for (Trainticket ticket : passenger.getTraintickets()) {
                if (ticket.getInsurorigprice() > 0) {
                    passenger.setTrainorder(trainorder);
                    ticket.setTrainpassenger(passenger);
                    Map.Entry<Boolean, String> m = insure(ticket.getId());
                    if (m.getKey()) {
                        String insureno = m.getValue();
                        Trainticket insurticket = new Trainticket();// 因Hessian
                        // 报异常。故此处重new
                        insurticket.setId(ticket.getId());
                        insurticket.setInsureno(insureno);
                        Server.getInstance().getTrainService().updateTrainticket(insurticket);
                    }
                    issuelist.add(m);
                }
            }
        }
        //        JSONArray array = JSONArray.parseArray(issuelist);
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(JSONObject.toJSONString(issuelist).replace("\"key\"", "").replace("\"value\"", "")
                .replace(":", "").replace("true", "投保成功").replace("false", "投保失败"));
        rc.setCreateuser(loginname);
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 火车票系统投保
     * @param ticket  火车票bean
     * @return Map.Entry<Boolean, String>:key{true/false},value:{备注信息}
     */
    public Map.Entry<Boolean, String> insure(long ticketid) {
        Trainticket ticket = Server.getInstance().getTrainService().findTrainticket(ticketid);
        boolean success = true;
        String msg = "";
        try {
            Trainpassenger passenger = ticket.getTrainpassenger();
            Trainorder order = passenger.getTrainorder();
            if (ticket.getInsurorigprice() > 0) {
                // / type 1。易订行火车票5元（未成年人）2。易订行火车票5元（成年人）3
                // 。易订行火车票20元（未成年人）4。易订行火车票20元（成年人）
                int type = ticket.getInsurorigprice() == 20f ? 4 : 2;
                Insuruser insur = new Insuruser();
                if (ticket.getInsureno() != null && ticket.getInsureno().indexOf("失败") > -1) {
                    String extno = ticket.getInsureno().split("\\|")[1];
                    if (!"null".equals(extno))
                        insur.setExtorderno(extno);
                }
                insur.setAgentid(order.getAgentid());
                insur.setCodetype(Long.valueOf(passenger.getIdtype()));
                insur.setCode(passenger.getIdnumber());
                insur.setFlytime(formatStringToTime(ticket.getDeparttime(), "yyyy-MM-dd HH:mm"));
                insur.setOrdernum(order.getOrdernumber());
                insur.setBirthday(formatStringToTime(passenger.getBirthday(), "yyyy-MM-dd"));
                insur.setName(passenger.getName());
                insur.setFlyno(ticket.getTrainno());
                insur.setMobile(order.getContacttel());
                List insurlist = new ArrayList();
                insurlist.add(insur);
                // 购买保险
                List<Insuruser> ins = Server.getInstance().getAtomService2()
                        .saveTrainOrderAplylist(null, insurlist, type);
                Insuruser in = ins.get(0);
                String extorderno = in.getExtorderno();
                if (isNotNullOrEpt(in.getPolicyno())) {
                    WriteLog.write("trainorder_insure",
                            "insure:" + passenger.getName() + "火车票购买保险投保成功，保单号::" + in.getPolicyno());
                    msg = in.getPolicyno();
                    if (!isNotNullOrEpt(msg)) {
                        success = false;
                        msg = "投保保单号为空";
                    }
                }
                else {
                    success = false;
                    WriteLog.write("trainorder_insure",
                            "insure:" + passenger.getName() + "火车票购买保险投保失败:" + in.getPolicyno() + in.getRemark());
                    msg = "旅客" + passenger.getName() + "投保失败:" + in.getRemark();
                    Trainticket tticket = new Trainticket();
                    tticket.setInsureno("失败|" + extorderno);
                    tticket.setId(ticket.getId());
                    Server.getInstance().getTrainService().updateTrainticket(tticket);
                }

            }
        }
        catch (Exception e) {
            success = false;
            msg = "系统投保异常";
            WriteLog.write("trainorder_insure", "insure:" + "火车票购买保险异常:" + e.fillInStackTrace());
        }
        Map<Boolean, String> m = new HashMap<Boolean, String>();
        m.put(success, msg);
        Map.Entry<Boolean, String> me = m.entrySet().iterator().next();
        return me;
    }

    /**
     * 易订行出票 
     * @param trainorderId
     * @param customeruserid
     * @time 2014年10月9日 上午11:58:38
     * @author yinshubin
     */
    public void yeeIssue(long trainorderId, long customeruserid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderId);
        Customeruser customeruser = Server.getInstance().getMemberService().findCustomeruser(customeruserid);
        List<Trainticket> tickets = new ArrayList<Trainticket>();
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                Trainticket ticket = new Trainticket();
                ticket.setId(trainticket.getId());
                ticket.setCoach(trainticket.getCoach());
                ticket.setSeatno(trainticket.getSeatno());
                ticket.setPrice(trainticket.getPrice());
                ticket.setPayprice(trainticket.getPayprice());
                ticket.setStatus(Trainticket.ISSUED);
                tickets.add(ticket);
            }
        }
        String iscj = "cp";
        trainorder.setOrderstatus(Trainorder.ISSUED);//出票状态
        trainorder.setState12306(Trainorder.ORDEREDPAYED);
        String cninterfaceurl = getSysconfigString("cninterfaceurl");
        Server.getInstance().getTrainService()
                .issueTrainticket(trainorder, tickets, customeruser, cninterfaceurl, iscj, 0f, null);
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorder.getId());
        rc.setContent("出票完成");
        rc.setStatus(Trainticket.ISSUED);
        rc.setCreateuser(customeruser.getLoginname());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
        sendmessage(trainorderId, customeruser.getLoginname());
    }

    /**
     * 获取当前时间前46分钟的数据库时间格式 
     * @return   yyyy-MM-dd HH:mm:ss        
     * @throws ParseException
     * @time 2015年1月6日 下午1:35:12
     * @author fiend
     */
    public String getCreateTime(int timeoutfenzhong) {
        String timeFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
        long mins = timeoutfenzhong * 1000 * 60;
        long createtime = new Date().getTime() - mins;
        return sdf.format(new Date(createtime));
    }

    /**
     * 是否是补单订单 
     * @param trainorderid
     * @return
     * @time 2015年1月18日 下午6:28:05
     * @author fiend
     */
    public boolean isBudanOrder(long trainorderid) {
        String sql = "SELECT C_ORDERID FROM T_TRAINORDERTIMEOUT WHERE C_ORDERID=" + trainorderid;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 支付完毕，修改临时表
     * @param trainorderid
     * @return
     * @time 2015年1月18日 下午6:28:05
     * @author fiend
     */
    public void changeBudanOrder(long trainorderid) {
        String sql = "UPDATE T_TRAINORDERTIMEOUT SET C_STATE=3 WHERE C_ORDERID =" + trainorderid;
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    /**
     * 同程回调补单结果
     * @param orderid
     * @param tcTrainCallBack
     * @param returnmsg 回调具体内容  补单成功传true
     * @return
     * @time 2014年01月11日 下午6:20:30
     * @author fiend
     */
    public String callBackTongChengBudan(long orderid, String tcTrainCallBack) {
        String result = "false";
        String url = tcTrainCallBack;
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_budan_callback");
        jso.put("returnmsg", "true");
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 书写操作记录
     * @param trainorderid
     * @param content
     * @param createurser
     * @param status
     * @param ywtype
     * @time 2015年1月21日 下午7:05:04
     * @author fiend
     */
    public void writeRC(long trainorderid, String content, String createurser, int status, int ywtype) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createurser);
        rc.setYwtype(ywtype);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 回调支付成功 
     * @param i
     * @param orderid
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengPayed(Trainorder trainorder, String tongchengcallbackurl) {
        String url = tongchengcallbackurl;
        JSONObject jso = new JSONObject();
        String result = "false";
        jso.put("pkid", trainorder.getId());
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getTradeno());
        jso.put("method", "train_pay_callback");
        try {
            WriteLog.write("审核回调同程出票", trainorder.getId() + ":tongcheng_tongzhi_chupiao:" + url + ":" + jso.toString());
            long t1 = System.currentTimeMillis();
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            long t2 = System.currentTimeMillis();
            WriteLog.write("审核回调同程出票", trainorder.getId() + ":返回:" + result + ":用时：" + (t2 - t1));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 修改订单
     * @param sql
     * @time 2015年1月25日 下午4:10:50
     * @author fiend
     */
    public void upTrainorder(String sql) {
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    /**
     * 获取当前时间-change min
     * @param change    min
     * @return yyyy-MM-dd HH:mm:ss
     * @time 2015年1月27日 下午2:39:47
     * @author fiend
     */
    public static String getdateTimp(long change) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        Date afterDate = new Date(now.getTime() - change);
        return sdf.format(afterDate);
    }

    /**
     * 获取数据库格式时间+修改时间 
     * @param dbtime  yyyy-MM-dd HH:mm:ss:SSS     
     * @param changetime  min
     * @return long
     * @time 2015年1月27日 下午2:39:47
     * @author fiend
     * @throws ParseException 
     */
    public static long getLongTime(String dbtime, long changetime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = sdf.parse(dbtime);
        return date.getTime() + changetime * 60 * 1000;
    }

    /**
     * 判断是不是同程类型的接口订单 
     * @return
     * @time 2015年3月10日 下午7:07:05
     * @author fiend
     */
    //    @SuppressWarnings("rawtypes")
    //    public boolean isOtherJiekou(long trainorderid) {
    //        try {
    //            int r1 = (int) (Math.random() * 10000);
    //            String sql = "SELECT ID FROM T_CALLBACK_TRAIN WHERE C_AGENTID=(SELECT TOP 1 C_AGENTID FROM T_TRAINORDER WHERE ID="
    //                    + trainorderid + ")";
    //            WriteLog.write("接口用户判断", r1 + "--->" + sql);
    //            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    //            WriteLog.write("接口用户判断", r1 + "--->" + list.size());
    //            if (list.size() > 0) {
    //                return true;
    //            }
    //            else {
    //                return false;
    //            }
    //        }
    //        catch (Exception e) {
    //            return false;
    //        }
    //    }

    /**
     * 获取MQ的地址
     * type 代表类型
     * @return
     * @time 2015年3月30日 下午5:30:38
     * @author chendong
     */
    protected String getActiveMQUrl(int type) {
        String url = getActiveMQUrl();
        return url;
    }

    /**
     * 获取MQ的地址
     * 
     * @return
     * @time 2015年3月30日 下午5:30:38
     * @author chendong
     */
    protected String getActiveMQUrl() {
        String url = getSysconfigString("activeMQ_url");
        return url;
    }

    /**
     * 是否是超时订单，淘宝适用
     * @param trainorder
     * @param interfacetype
     * @param order_timeout_time
     * @return
     * @time 2015年3月31日 上午5:05:06
     * @author fiend
     */
    public boolean isTimeout(Trainorder trainorder, int interfacetype, long order_timeout_time) {
        if (TrainInterfaceMethod.TAOBAO != interfacetype) {
            return false;
        }
        if (trainorder.getOrdertimeout() == null) {
            //创建订单时间+30分钟
            long ordertimeout = trainorder.getCreatetime().getTime() + 30 * 60 * 1000l;
            //创建订单30分钟后减淘宝发起支付预留时间,如果小于当前时间判定为超时返回true
            if ((ordertimeout - order_timeout_time) < System.currentTimeMillis()) {
                return true;
            }
            else {
                return false;
            }
        }
        else if ((trainorder.getOrdertimeout().getTime() - order_timeout_time) < System.currentTimeMillis()) {
            return false;
        }
        return true;
    }

    /**
     * 回调淘宝拒单
     * @time 2015年3月31日 上午3:48:11
     * @author fiend
     */
    public void refundTaobao(Trainorder trainorder, String loginname, String result) {
        //        其他  0
        //        无票  1
        //        价格不符    2
        //        高昕(二代身份证-142701199611093620)已订2015年06月20日K1084次的车票!!    3
        //        出票超时    4
        //        身份验证未通过 5
        //        发车时间不符  6
        //        车次未找到   7
        //        因网络繁忙或12306系统故障导致订单未提交/支付未成功/订单未找到  8
        //        学生票信息错误 9
        //        被限制消费   10
        //        无座票 11
        //        尊敬的旅客，您的证件-邹本强(371322198704058354)已订2015年05月08日K1196次车票，与本次购票行程冲突，请将已购车票办理改签（或变更到站），或办理退票后重新购票；如您确认此身份信息被他人冒用，请点击“网上举报”并确认后，可以继续购票。谢谢您的合作。  12
        //        乘客信息有错  13
        //        预售期不符   14
        //        乘客xxx已改签（或变更到站），不能改签（或变更到站）（线下） 15
        //        非法席别    16
        //淘宝回调地址
        WriteLog.write("TAOBAO_refund_reason", trainorder.getId() + "--->" + result);
        String Taobao_TrainCallBack = getSysconfigString("Taobao_TrainCallBack");
        if (result == null) {
            result = "0";
        }
        else if (result.contains("票价")) {
            result = "2";

        }
        else if (result.contains("身份信息涉嫌被他人冒用")) {
            result = "10";

        }
        else if (result.contains("没有足够的票") || result.contains("余票")) {
            result = "1";

        }
        else if (result.contains("已购")) {
            result = "12";

        }
        else if (result.contains("已订")) {
            result = "3";

        }
        else if (result.contains("超时")) {
            result = "4";

        }
        else if (result.contains("身份验证失败")) {
            result = "5";
        }
        else if (result.contains("发车")) {
            result = "6";
        }
        else if (result.contains("车次")) {
            result = "7";
        }
        else if (result.contains("不能购买学生票")) {
            result = "9";
        }
        else if (result.contains("网络繁忙")) {
            result = "8";
        }
        else if (result.contains("被限制消费")) {
            result = "10";
        }
        else if (result.contains("无座")) {
            result = "11";
        }
        else if (result.contains("乘客信息有错")) {
            result = "13";
        }
        else if (result.contains("非法席别 ")) {
            result = "16";
        }
        else {
            result = "0";
        }

        try {
            result = URLEncoder.encode(result, "utf-8");
        }
        catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        String taobao_callbackstr = "";
        try {
            String pameString = "?order_id=" + trainorder.getId() + "&fail_msg=" + result;
            WriteLog.write("TrainCreateOrder_refundTaobao", "Taobao_TrainCallBack:" + Taobao_TrainCallBack
                    + ":pameString:" + pameString);
            taobao_callbackstr = SendPostandGet.submitGet(Taobao_TrainCallBack + pameString, "UTF-8").toString();
            WriteLog.write("TrainCreateOrder_refundTaobao", trainorder.getId() + ":" + taobao_callbackstr);

        }
        catch (Exception e) {
            WriteLog.write("淘宝出票拒单异常", e.getMessage() + " url " + result);
        }

        if ("SUCCESS".equals(taobao_callbackstr)) {
            taobaoRc(trainorder, loginname, false, true);
        }
        else {
            taobaoRc(trainorder, loginname, false, false);
        }
    }

    /**
     * 淘宝下单成功,直接调支付
     * @time 2015年3月31日 上午3:48:11
     * @author fiend
     */
    public void successTaobao(Trainorder trainorder, String loginname, String result) {
        //TODO 淘宝下单成功
        try {
            toPayMq(trainorder);
            WriteLog.write("TrainCreateOrder_successTaobao", trainorder.getId() + ":占座成功---调用支付");
            taobaoRc(trainorder, loginname, true, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 淘宝回调后 生成操作记录
     * @param loginname
     * @param trainorder
     * @param isordered
     * @param isbudanstr
     * @time 2015年3月31日 上午4:01:05
     * @author fiend
     */
    public void taobaoRc(Trainorder trainorder, String loginname, boolean isordered, boolean iscallbacktrue) {
        String contentstr = isordered ? "---占座成功---调用支付" : (iscallbacktrue ? "---占座失败----回调成功" : "---占座失败---回调失败");
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(contentstr);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorder.getId() + ":content:" + contentstr);
        }
    }

    /**
     * 补单成功后调用支付队列
     * 
     * @time 2015年3月13日 下午4:17:07
     * @author fiend
     */
    public void toPayMq(Trainorder trainorder) {
        try {
            new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(trainorder, 2, 0);
            WriteLog.write("12306_TrainCreateOrder_MQ", ": 补单请求支付 ： " + trainorder.getOrdernumber());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 淘宝出票回调后 生成操作记录
     * @param orderid
     * @param issuccess
     * @time 2015年4月8日 下午5:20:03
     * @author fiend
     */
    public void taobaoSuccessRc(long orderid, boolean issuccess) {
        String contentstr = issuccess ? "---出票成功---回调成功" : "---出票成功---回调失败";
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(contentstr);
        rc.setCreateuser("系统接口");
        rc.setOrderid(orderid);
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", orderid + ":content:" + contentstr);
        }
    }

    /**
     * 顺序获取一个手机号用来注册
     * 
     * @return
     * @time 2015年4月23日 下午12:44:34
     * @author chendong
     */
    public String getmobile() {
        String mobile = "15514070462";
        try {
            String mobileString = PropertyUtil.getValue("reg_mobile", "train.properties");
            String[] mobileStrings = mobileString.split(",");
            int xuhao = new Random().nextInt(mobileStrings.length);
            if (Server.getInstance().getDateHashMap().get("reg_mobile_xuhao") == null) {
                Server.getInstance().getDateHashMap().put("reg_mobile_xuhao", "0");
            }
            else {
                xuhao = Integer.parseInt(Server.getInstance().getDateHashMap().get("reg_mobile_xuhao")) + 1;
                if (xuhao >= mobileStrings.length) {
                    xuhao = 0;
                }
                Server.getInstance().getDateHashMap().put("reg_mobile_xuhao", xuhao + "");
            }
            xuhao = Integer.parseInt(Server.getInstance().getDateHashMap().get("reg_mobile_xuhao"));
            mobile = mobileStrings[xuhao];
            System.out.println("length:" + mobileStrings.length + ";xuhao:" + xuhao + ";mobile:" + mobile);
        }
        catch (Exception e) {
        }
        return mobile;
    }

    /**
     * 全自动登录,返回cookie
     * 
     * @param logname 12306账号  
     * @param logpassword 12306密码
     * @return
     * @time 2014年12月19日 下午7:41:51
     * @author chendong
     */
    public static String rep12Method(String logname, String logpassword, String repUrl) {
        String damarule = "5,5,5,5,5";//打码规则,逗号分隔顺序,数字详见DaMaCommon类
        String paramContent = "";
        paramContent = "logname=" + logname + "&logpassword=" + logpassword + "&damarule=" + damarule
                + "&datatypeflag=12";
        String resultString = "";
        //        System.out.println(paramContent);
        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        return resultString;
    }

    /**
     * 修改手机号
     * 
     * @param _loginPwd
     * @param mobile_no
     * @param cookieString
     * @return
     * @time 2015年5月12日 下午2:17:33
     * @author chendong
     */
    public static String rep30Method(String _loginPwd, String mobile_no, String cookieString, String repUrl) {
        //        String repUrl = "http://localhost:9016/Reptile/traininit";//300
        String resultString = "";
        JSONObject new_data = new JSONObject();
        new_data.put("_loginPwd", _loginPwd);
        new_data.put("mobile_no", mobile_no);
        new_data.put("edittype", "1");//修改类型 1修改手机号
        String paramContent = "datatypeflag=30&new_data=" + new_data.toJSONString() + "&cookie=" + cookieString;
        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        return resultString;
    }

    /**
     * 通过订单ID查询接口类型
     * @param orderid
     * @return
     */
    @SuppressWarnings("rawtypes")
    public int interfacetypeByOrderid(long orderid) {
        int interfacetype = 0;
        try {
            String sql = "SELECT ISNULL(C_INTERFACETYPE, 0) C_INTERFACETYPE "
                    + "FROM T_TRAINORDER WITH (NOLOCK) WHERE ID = " + orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            //唯一
            if (list != null && list.size() == 1) {
                Map map = (Map) list.get(0);
                interfacetype = Integer.valueOf(map.get("C_INTERFACETYPE").toString());
            }
            //失败
            if (interfacetype == 0) {
                interfacetype = Server.getInstance().getInterfaceTypeService().getTrainInterfaceType(orderid);
            }
        }
        catch (NumberFormatException e) {
        }
        return interfacetype;
    }

    /**
     * 
     * 扣除虚拟账号里面的钱
     * 
     * @param orderid 订单ID
     * @param ordernumber 订单号
     * @param orderprice 订单支付金额
     * @param ywtype
     *            业务类型。 1，机票，2，酒店。3.火车票（已无支付操作），4,手机,QQ充值，5，短信充值。 6.保险购买，7，
     *            8.行程单支付 9.签证。10.商旅手伴 21：酒店补开发票    11 线下火车票结账
     * @param refordernum  同城关联订单号
     */
    public Payresult vmonyPay(long changeId, String ordernumber, float orderprice, int ywtype, String refordernum,
            long agentid) {
        WriteLog.write("tc虚拟账户支付", "订单号ID：" + changeId + ":订单号：" + ordernumber + ":订单价格：" + orderprice + ":业务类型："
                + ywtype);
        Payresult payresult = new Payresult();
        payresult.setPaysuccess(true);
        payresult.setResultmessage("支付成功");
        String msg = "订单";
        if (ywtype == 21) {
            msg = "发票";
            ywtype = 2;
        }
        if (orderprice == 0) {
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败,支付金额不能等于0！");
            WriteLog.write("tc虚拟账户支付", "订单号ID：" + changeId + ",订单号：" + ordernumber + "," + payresult.getResultmessage());
            return payresult;
        }
        //升级针对虚拟账户的扣款操作
        try {
            String memo = msg + "支付扣除" + orderprice + "元";
            String sql = "TrainorderDeduction @orderid=" + changeId + ",@ordernumber='" + ordernumber + "',@yewutye="
                    + ywtype + ",@rebateagentid=" + agentid + ",@rebatemoney=" + (0D - orderprice)
                    + ",@vmenble=1,@rebatetype=" + Rebaterecord.PINGTAIXIAOFEI + ",@rebatememo='" + memo
                    + "',@customerid=0,@refordernumber='" + refordernum + "',@paymethod=10,@paystate=0";
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            if (null != list && list.size() > 0) {
                Map map = (Map) list.get(0);
                String result = map.get("result").toString();
                String error = map.get("message").toString();
                if (!"success".equals(result)) {// 失败
                    payresult.setPaysuccess(false);
                    payresult.setResultmessage(msg + " " + error);
                }
                else if ("success".equals(result)) { // 成功
                    payresult.setPaysuccess(true);
                    System.out.println("扣款成功" + result);
                }
                WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + "--->result:" + result
                        + "--->error:" + error);
            }
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + ":支付成功--->sql:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败！");
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + "," + e.getMessage());
        }
        return payresult;
    }

    /**
     * 创建 返佣记录表
     * @param record
     * @return
     */
    private Rebaterecord createRebaterecord(Rebaterecord rebaterecord) {
        if (rebaterecord.getRebatemoney() == null) {
            rebaterecord.setRebatemoney(0d);
        }
        Integer yewutype = rebaterecord.getYewutype();
        if (yewutype == null) {
            yewutype = 0;
        }
        String ordernumber = rebaterecord.getOrdernumber();
        Long rebateagentid = rebaterecord.getRebateagentid();
        if (rebateagentid == null) {
            rebateagentid = 0L;
        }
        Double rebatemoney = rebaterecord.getRebatemoney();
        if (rebatemoney == null) {
            rebatemoney = 0.0;
        }
        Integer rebatetype = rebaterecord.getRebatetype();
        if (rebatetype == null) {
            rebatetype = 0;
        }
        Integer paystate = rebaterecord.getPaystate();
        if (paystate == null) {
            paystate = 0;
        }
        Timestamp rebatetime = rebaterecord.getRebatetime();
        String rebatememo = rebaterecord.getRebatememo();
        Long customerid = rebaterecord.getCustomerid();
        if (customerid == null) {
            customerid = 0L;
        }
        Long orderid = rebaterecord.getOrderid();
        if (orderid == null) {
            orderid = 0L;
        }
        Double rebate = rebaterecord.getRebate();
        if (rebate == null) {
            rebate = 0.0;
        }
        Integer vmenable = rebaterecord.getVmenable();
        if (vmenable == null) {
            vmenable = 0;
        }
        Double vmbalance = rebaterecord.getVmbalance();
        if (vmbalance == null) {
            vmbalance = 0.0;
        }
        Integer paymethod = rebaterecord.getPaymethod();
        if (paymethod == null) {
            paymethod = 0;
        }
        Long optagentid = rebaterecord.getOptagentid();
        if (optagentid == null) {
            optagentid = 0L;
        }
        Float disablemoneyval = rebaterecord.getDisablemoneyval();
        if (disablemoneyval == null) {
            disablemoneyval = 0f;
        }
        Long disablemoneystatus = rebaterecord.getDisablemoneystatus();
        if (disablemoneystatus == null) {
            disablemoneystatus = 0L;
        }
        Float gusmoneyval = rebaterecord.getGusmoneyval();
        if (gusmoneyval == null) {
            gusmoneyval = 0f;
        }
        String refordernum = rebaterecord.getRefordernum();

        String insertSql = "[sp_T_REBATERECORD_createRebaterecord] " + "               @C_YEWUTYPE='" + yewutype + "',"
                + "               @C_ORDERNUMBER='" + ordernumber + "'," + "               @C_REBATEAGENTID='"
                + rebateagentid + "'," + "               @C_VMENABLE='" + vmenable + "',"
                + "               @C_REBATEMONEY='" + rebatemoney + "'," + "               @C_REBATETYPE='"
                + rebatetype + "'," + "               @C_PAYSTATE='" + paystate + "',"
                + "               @C_REBATETIME='" + rebatetime + "'," + "               @C_REBATEMEMO='" + rebatememo
                + "'," + "               @C_CUSTOMERID='" + customerid + "'," + "               @C_ORDERID='" + orderid
                + "'," + "               @C_REBATE='" + rebate + "'," +
                /*"               @C_TRADENO="+tradeno+","+*/
                "               @C_VMBALANCE='" + vmbalance + "'," + "               @C_OPTAGENTID='" + optagentid
                + "'," + "               @C_PAYMETHOD='" + paymethod + "'," + "               @C_DISABLEMONEYVAL='"
                + disablemoneyval + "'," + "               @C_DISABLEMONEYSTATUS='" + disablemoneystatus + "',"
                + "               @C_GUSMONEYVAL='" + gusmoneyval + "'," + "               @C_REFORDERNUM='"
                + refordernum + "'";

        try {
            Integer insertGeneratedKey = 0;
            DataTable getDataTable = DBHelper.GetDataTable(insertSql, dbSourceEnum);
            List<DataRow> getRow2 = getDataTable.GetRow();
            for (DataRow dataRow : getRow2) {
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    insertGeneratedKey = Integer.parseInt(dataColumn.GetValue().toString());
                }
            }
            rebaterecord.setId(insertGeneratedKey);
            String selectSql = "[sp_T_REBATERECORD_selectTradenoById] @ID = " + insertGeneratedKey;
            DataTable table = DBHelper.GetDataTable(selectSql, dbSourceEnum);
            List<DataRow> getRow = table.GetRow();
            for (DataRow dataRow : getRow) {
                List<DataColumn> getColumn = dataRow.GetColumn();
                for (DataColumn dataColumn : getColumn) {
                    rebaterecord.setTradeno((String) dataColumn.GetValue());
                }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainSupplyMethod_err", e, "createRebaterecordf方法错误>>>>insertSql="
                    + insertSql + ">>>>>>rebaterecord=" + rebaterecord.toString());
        }
        return rebaterecord;
    }

    @SuppressWarnings("rawtypes")
    public static Long getuseridbyagentid(Long agentid) {
        String sql = " select id from T_CUSTOMERUSER where C_AGENTID=" + agentid + " and C_ISADMIN=1";
        Map map = (Map) executeSQLReturn(sql).get(0);
        return Long.valueOf(map.get("id").toString());

    }

    /**
     * 获取当前时间
     */
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 操作虚拟账户钱
     */
    public static float addAgentvmoney(long agentid, float money) {
        //        String sql = "UPDATE T_CUSTOMERAGENT SET C_VMONEY=C_VMONEY+" + money + " WHERE ID=" + agentid + ";"
        //                + "SELECT C_VMONEY FROM T_CUSTOMERAGENT WHERE ID=" + agentid;
        String sql = "[sp_T_CUSTOMERAGENT_updateVMONEYById] @C_VMONEY=" + money + ", @ID=" + agentid + ";";
        WriteLog.write("tc支付sql", sql);
        DBHelper.executeSql(sql, dbSourceEnum);
        WriteLog.write("tc支付sql", "成功：" + money);
        return 0.0f;
    }

    /**
     * 根据agentid获取余额
     */
    @SuppressWarnings("rawtypes")
    public static float getTotalVmoney(long agentid) {
        String sql = "SELECT C_VMONEY AS VMONEY FROM T_CUSTOMERAGENT WHERE ID=" + agentid;
        List list = executeSQLReturn(sql);
        if (list.size() > 0) {
            Map m = (Map) list.get(0);
            float vmoney = Float.valueOf(m.get("VMONEY").toString());
            return vmoney;
        }
        return 0;
    }

    public static void main(String[] args) {
        /*List<Map.Entry<Boolean, String>> list = new ArrayList<Map.Entry<Boolean, String>>();

        Map mapo = new HashMap<Boolean, String>();
        mapo.put(true, "");

        Iterator<Map.Entry<Boolean, String>> iterator = mapo.entrySet().iterator();
        Map.Entry<Boolean, String> item = iterator.next();

        try {
            list.add(item);
            System.out.println(JSONObject.toJSONString(list));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
        //        InitServer.start();

        /*Customeruser user = new Customeruser();
        String sql = "SELECT top 1 ID,C_MEMBERNAME,C_LOGINNAME,C_LOGPASSWORD,C_MEMBERSEX,C_MEMBEREMAIL,C_MOBILE,"+
        "C_STATE,C_TYPE,C_ISADMIN,C_BIRTHDAY,C_MEMBERFAX,C_ISWEB,C_MEMBERMOBILE,C_ISENABLE,C_MEMBERTYPE,"+
        "C_AGENTID,C_MODIFYTIME,C_MODIFYUSER,C_CREATETIME,C_CREATEUSER,C_DEPTID,C_CARDTYPE,C_CARDNUNBER,"+
        "C_WORKPHONE,C_DESCRIPTION,C_ENNAME,C_NATIONALITY,C_CHINAADDRESS,C_POSTALCODE,C_TOTALSCORE,C_LEVEL,"+
        "C_TICKETNUM,C_LIMITTICKETNUM,C_INTERTICKETNUM,C_LIMITINTERTICKETNUM,C_LOGINNUM,C_TICKETDISCOUNT,"+
        "C_INTERTICKETDISCOUNT FROM T_CUSTOMERUSER WHERE C_LOGINNAME = '" + "admin" + "'";
        //        String sql = "SELECT top 1 * FROM T_CUSTOMERUSER WHERE C_LOGINNAME = '" + AccountName + "'";
        List<Customeruser> users = new TrainSupplyMethod().executeSQLReturnListCustomeruser(sql);
        if (users != null && users.size() > 0) {
            user = users.get(0);
            //重新登陆
            if (user.getState() == 0) {
                //user = login12306(user);
                System.out.println(user.getState());
            }else{
                System.out.println(user.getState());
            }
        }*/
        /*
        Rebaterecord rebaterecord = new Rebaterecord();
        rebaterecord.setYewutype(1);
        rebaterecord.setOrdernumber("12314");
        rebaterecord.setRebateagentid(11234L);
        rebaterecord.setRebatemoney(1.2d);
        rebaterecord.setRebatetype(1);
        rebaterecord.setPaystate(1);
        rebaterecord.setRebatetime(Timestamp.valueOf("2018-02-06 14:21:00"));
        rebaterecord.setRebatememo("12312");
        rebaterecord.setCustomerid(123123L);
        rebaterecord.setOrderid(123123L);
        rebaterecord.setRebate(2.3d);
        rebaterecord.setVmenable(1231);
        rebaterecord.setVmbalance(2.5d);
        rebaterecord.setPaymethod(123123);
        rebaterecord.setOptagentid(123131L);
        rebaterecord.setDisablemoneyval(12.4f);
        rebaterecord.setDisablemoneystatus(12312L);
        rebaterecord.setGusmoneyval(123.4f);
        rebaterecord.setRefordernum("123112");
        
        rebaterecord = new TrainSupplyMethod().createRebaterecord(rebaterecord);
        System.out.println(rebaterecord.getId());
        
        
        System.out.println(rebaterecord.getCustomername());
        System.out.println(rebaterecord.getTradeno());
        
        System.out.println("ok");*/
        /*String sql = "[sp_T_TRAINORDER_selectOrderInterfaceInformationByOrderID] @ID = " + 47046;
        List<Map<String, Object>> list = executeSQLReturn(sql);
        for (Map<String, Object> map : list) {
            Set<String> keySet = map.keySet();
            for (String string : keySet) {
                Object object = map.get(string);
                System.out.println(object);
            }
        }*/
        Rebaterecord rebaterecord = new Rebaterecord();
        rebaterecord.setYewutype(1);
        rebaterecord.setOrdernumber("12314");
        rebaterecord.setRebateagentid(11234L);
        rebaterecord.setRebatemoney(1.2d);
        rebaterecord.setRebatetype(1);
        rebaterecord.setPaystate(1);
        rebaterecord.setRebatetime(Timestamp.valueOf("2018-02-06 14:21:00"));
        rebaterecord.setRebatememo("12312");
        rebaterecord.setCustomerid(123123L);
        rebaterecord.setOrderid(123123L);
        rebaterecord.setRebate(2.3d);
        rebaterecord.setVmenable(1231);
        rebaterecord.setVmbalance(2.5d);
        rebaterecord.setPaymethod(123123);
        rebaterecord.setOptagentid(123131L);
        rebaterecord.setDisablemoneyval(12.4f);
        rebaterecord.setDisablemoneystatus(12312L);
        rebaterecord.setGusmoneyval(123.4f);
        rebaterecord.setRefordernum("123112");

        rebaterecord = new TrainSupplyMethod().createRebaterecord(rebaterecord);
        System.out.println(rebaterecord.toString());
        System.out.println("ok");
    }

    /**
     * 获取客户账号Cookie
     * @author WH
     * @param name 账号名称
     * @param password 账号密码
     */
    public Customeruser GetCustomerAccount(String name, String password) {
        if (ElongHotelInterfaceUtil.StringIsNull(name) || ElongHotelInterfaceUtil.StringIsNull(password)) {
            return new Customeruser();
        }
        else {
            Map<String, String> backup = new HashMap<String, String>();
            backup.put("password", password);
            Customeruser user = Account12306Util.get12306Account(4, name, true, backup);
            user.setCustomerAccount(true);
            return user;
        }
    }

    /**
     * 客人Cookie方式
     */
    private Customeruser GetCustomerAccountByCookieWay(Customeruser temp) {
        Customeruser user = new Customeruser();
        //虚拟值
        user.setId(1);
        user.setState(1);
        user.setIsenable(1);
        user.setMemberemail("");
        user.setNationality("");
        user.setLoginname("Cookie");
        user.setLogpassword("Cookie");
        user.setCustomerAccount(true);
        user.setFromAccountSystem(true);
        user.setCardnunber(temp.getCardnunber());
        user.setPostalcode(temp.getPostalcode());
        return user;
    }

    /**
     * TODO  
     * 使用哪个数据库
     */
    private static DBSourceEnum dbSourceEnum = DBSourceEnum.TONGCHENG_DB;

    /**
     * Cookie无效时，调用接口刷新Cookie
     * @author WH
     */
    @SuppressWarnings("rawtypes")
    public String refreshCookieFromInterface(Trainorder order) {
        //刷新结果
        String refreshResult = "";
        //捕捉异常
        try {
            if (ElongHotelInterfaceUtil.StringIsNull(order.getQunarOrdernumber()) || order.getAgentid() == 0) {
                String sql = "[sp_T_TRAINORDER_selectOrderInterfaceInformationByOrderID] @ID = " + order.getId();
                List<Map<String, Object>> list = executeSQLReturn(sql);
                //查询成功
                if (list.size() > 0) {
                    Map map = (Map) list.get(0);
                    order.setAgentid(Long.valueOf(map.get("C_AGENTID").toString()));
                    order.setQunarOrdernumber(map.get("C_QUNARORDERNUMBER").toString());
                    order.setInterfacetype(Integer.valueOf(map.get("C_INTERFACETYPE").toString()));
                }
            }
            int interfaceType = order.getInterfacetype() == null ? 0 : order.getInterfacetype();
            interfaceType = interfaceType > 0 ? interfaceType : getOrderAttribution(order.getId());
            //淘宝
            if (interfaceType == TrainInterfaceMethod.TAOBAO) {
                //参数
                JSONObject param = new JSONObject();
                param.put("orderid", order.getId());
                param.put("agentid", order.getAgentid());
                param.put("interfaceOrderNumber", order.getQunarOrdernumber());
                //地址
                String url = PropertyUtil.getValue("fresh_cookie_taobao_url", "train.properties");
                //请求
                refreshResult = SendPostandGet.submitGet(url + "?jsonStr=" + param, "UTF-8").toString();
            }
            //其他
            else {
                //参数
                JSONObject json = new JSONObject();
                json.put("orderid", order.getId());
                json.put("interfaceType", order.getInterfacetype());
                json.put("agentid", order.getAgentid());
                json.put("interfaceOrderNumber", order.getQunarOrdernumber());
                //地址
                String url = PropertyUtil.getValue("TongChengCallBackServletURL", "train.properties");
                //请求
                refreshResult = SendPostandGet.submitPost(url, json.toString(), "UTF-8").toString();
            }
        }
        catch (Exception exception) {
            ExceptionUtil.writelogByException("RefreshCookieFromInterface_Error", exception);
        }
        return ElongHotelInterfaceUtil.StringIsNull(refreshResult) ? "" : refreshResult;
    }

    /**
     * TongChengCancelThread类，取消订单判断
     */
    public boolean GoCancelOrder(Trainorder order, Customeruser customeruser) {
        String extNum = order == null ? "" : order.getExtnumber();
        String cookie = customeruser == null ? "" : customeruser.getCardnunber();
        //电子单号、Cookie非空
        return !ElongHotelInterfaceUtil.StringIsNull(extNum) && !ElongHotelInterfaceUtil.StringIsNull(cookie);
    }
}