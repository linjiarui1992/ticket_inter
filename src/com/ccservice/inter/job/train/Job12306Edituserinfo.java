package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 定时任务修改12306账号的信息
 * 
 * @time 2015年4月29日 上午11:41:59
 * @author chendong
 */
public class Job12306Edituserinfo extends TrainSupplyMethod implements Job {
    public static void main(String[] args) {
        Job12306Edituserinfo job12306edituserinfo = new Job12306Edituserinfo();
        job12306edituserinfo.execute();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String edit12306userinfo_isstart_url = PropertyUtil.getValue("edit12306userinfo_isstart_url",
                "train.properties");
        String isstart = SendPostandGet.submitGet(edit12306userinfo_isstart_url);
        Long l1 = System.currentTimeMillis();
        if ("1".equals(isstart)) {
            System.out.println("s:" + l1);
            execute();
            System.out.println("耗时:" + (System.currentTimeMillis() - l1));
        }
        else {
            System.out.println("不修改12306账号信息了");
        }
    }

    private void execute() {
        Long l1 = System.currentTimeMillis();
        String meicicount = "1";
        //获取一个需要修改12306信息的账号，并把这个账号的C_ISENABLE改为999
        List customerusers = Server.getInstance().getSystemService()
                .findMapResultByProcedure("sp_CustomerUser_getoneuserToeditUserinfo");

        for (int i = 0; i < customerusers.size(); i++) {
            Map map = (Map) customerusers.get(i);
            String loginname = map.get("C_LOGINNAME").toString();
            String password = map.get("C_LOGPASSWORD").toString();
            String cardnunber = map.get("C_CARDNUNBER") == null ? "" : map.get("C_CARDNUNBER").toString();
            String mobile = map.get("C_MOBILE") == null ? "" : map.get("C_MOBILE").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            if (/*new_mobile_no.length() == 11 && */checkmobileiskeyong(mobile)) {
                String repUrl = getrepurl(2).getUrl();
                WriteLog.write("Job12306Edituserinfo", l1 + ":" + id + ":" + loginname + ":" + password + ":" + repUrl);
                cardnunber = rep12Method(loginname, password, repUrl);//登录获取cookie
                WriteLog.write("Job12306Edituserinfo", l1 + ":" + loginname + ":new_cookie====" + cardnunber);
                chuludenglujieguo(loginname, password, cardnunber, id);
            }
            else {
                String sql11 = "";
                sql11 = "update T_CUSTOMERUSER  set C_DEPTID=0,C_ISENABLE='1' where id=" + id;
                int count = Server.getInstance().getSystemService().excuteEaccountBySql(sql11);
                System.out.println("存在了:result:" + "loginname:" + loginname + ":count:" + count);
                WriteLog.write("Job12306Edituserinfo", l1 + ":" + loginname + ":count:" + count);
                //                execute();
            }
        }
    }

    private void chuludenglujieguo(String loginname, String password, String cardnunber, Long id) {
        String sql11;
        if (Server.getInstance().getDateHashMap().get("edituserinfo_count_1") == null) {
            Server.getInstance().getDateHashMap().put("edituserinfo_count_1", "0");
        }
        Integer edituserinfo_count_1 = Integer.parseInt(Server.getInstance().getDateHashMap()
                .get("edituserinfo_count_1"));
        int isexecute = 1;//是否每次都执行
        if (!"登录失败".equals(cardnunber) && !"失败".equals(cardnunber)) {
            String new_mobile_no = getmobile();
            String result = "";
            try {
                //                String repUrl = "http://localhost:9016/Reptile/traininit";//300
                String repUrl = PropertyUtil.getValue("edit12306userinfo_rep30Method_repUrl", "train.properties");
                result = rep30Method(password, new_mobile_no, cardnunber, repUrl);
            }
            catch (Exception e) {
            }
            System.out.println(loginname + ":id:" + id + ":修改结果result:" + result);
            sql11 = "";
            if ("true".equals(result)) {
                isexecute = 0;
                edituserinfo_count_1 += 1;
                Server.getInstance().getDateHashMap().put("edituserinfo_count_1", edituserinfo_count_1 + "");
                sql11 = "update T_CUSTOMERUSER  set C_DEPTID=0,C_MOBILE ='" + new_mobile_no
                        + "',C_ISENABLE='1' where id=" + id;
                if (Server.getInstance().getDateHashMap().get("edituserinfo_sql_1") == null) {
                    Server.getInstance().getDateHashMap().put("edituserinfo_sql_1", sql11);
                }
                else {
                    String hashmapSql = Server.getInstance().getDateHashMap().get("edituserinfo_sql_1") + ";" + sql11;
                    Server.getInstance().getDateHashMap().put("edituserinfo_sql_1", hashmapSql);
                }
            }
            else {
                sql11 = "update T_CUSTOMERUSER set C_ISENABLE='1' where id=" + id;
            }
        }
        else if ("失败".equals(cardnunber)) {
            sql11 = "update T_CUSTOMERUSER set C_ISENABLE='0' where id=" + id;

        }
        else {
            sql11 = "update T_CUSTOMERUSER set C_ISENABLE='2' where id=" + id;
        }
        String hashmapSql = "";
        if (isexecute == 1) {
            int count = Server.getInstance().getSystemService().excuteEaccountBySql(sql11);
            System.out.println("result:" + "loginname:" + loginname + ":count:" + count);
            WriteLog.write("Job12306Edituserinfo", loginname + ":count:" + count + ":sql:" + sql11);
        }
        else {
            if (Integer.parseInt(Server.getInstance().getDateHashMap().get("edituserinfo_count_1")) >= 1) {
                Server.getInstance().getDateHashMap().put("edituserinfo_count_1", "0");//重置
                hashmapSql = Server.getInstance().getDateHashMap().get("edituserinfo_sql_1");
                int count = Server.getInstance().getSystemService().excuteEaccountBySql(hashmapSql);
                System.out.println("result:" + "loginname:" + loginname + ":count:" + count);
                Server.getInstance().getDateHashMap().remove("edituserinfo_sql_1");
            }
        }
        WriteLog.write("Job12306Edituserinfo", loginname + ":edituserinfo_count_1:"
                + Server.getInstance().getDateHashMap().get("edituserinfo_count_1") + ":hashmapSql:" + hashmapSql);
    }

    /**
     * 
     * 判断手机号是否存在使用的之中如果存在返回false否则返回true
     * @param mobile
     * @return
     * @time 2015年5月1日 上午11:57:35
     * @author chendong
     */

    private boolean checkmobileiskeyong(String mobile) {
        try {
            if (mobile.length() > 0) {
                String mobileString = PropertyUtil.getValue("reg_mobile", "train.properties");
                if (mobileString.indexOf(mobile) >= 0) {
                    return false;
                }
            }
        }
        catch (Exception e) {
        }
        return true;
    }

    /**
     * type 1=登录（打码）2=保持在线（不打码）
     * 此方法是根据 C_USENUMBER 使用次数排序低的排到前面
     * @param type
     * @return
     * @time 2015年3月20日 下午4:45:58
     * @author chendong
     */
    public RepServerBean getrepurl(int type) {
        List list = new ArrayList();
        RepServerBean rep = new RepServerBean();
        String url1 = "http://192.168.0.5:9001/cn_service/service/";
        try {
            ISystemService iSystemService = (ISystemService) new HessianProxyFactory().create(ISystemService.class,
                    url1 + ISystemService.class.getSimpleName());
            iSystemService = Server.getInstance().getSystemService();
            list = iSystemService.findMapResultByProcedure("sp_RepServer_GetOneRep");
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;
    }

    /**
     * 修改手机号
     * 
     * @param _loginPwd
     * @param mobile_no
     * @param cookieString
     * @return
     * @time 2015年5月12日 下午2:17:33
     * @author chendong
     */
    public static String rep30Method(String _loginPwd, String mobile_no, String cookieString, String repUrl) {
        //        String repUrl = "http://localhost:9016/Reptile/traininit";//300
        String resultString = "";
        JSONObject new_data = new JSONObject();
        new_data.put("_loginPwd", _loginPwd);
        new_data.put("mobile_no", mobile_no);
        new_data.put("edittype", "1");//修改类型 1修改手机号
        String paramContent = "datatypeflag=30&new_data=" + new_data.toJSONString() + "&cookie=" + cookieString;
        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        return resultString;
    }
}
