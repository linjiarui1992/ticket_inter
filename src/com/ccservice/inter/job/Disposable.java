package com.ccservice.inter.job;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelper2;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;

public class Disposable {

    public void findUser() {
        List<com.ccervice.util.db.DataRow> dataRows = null;
        long id = 0;
        String Accounts = "";
        String password = "";
        Integer status = -2;
        String sql = "select top 1 * from T_Customeruser with(nolock) where C_Isenable = 37";
        Customeruser customeruser = null;
        try {
            com.ccervice.util.db.DataTable result = DBHelper2.GetDataTable(sql);
            dataRows = result.GetRow();
            if (dataRows.size() > 0) {
                com.ccervice.util.db.DataRow dataRow = dataRows.get(0);
                Accounts = dataRow.GetColumnString("C_LOGINNAME");
                password = dataRow.GetColumnString("C_LOGPASSWORD");
                status = dataRow.GetColumnInt("C_ISENABLE");
                id = dataRow.GetColumnInt("PKId");
                System.out.println("id:" + id + ":帐号:" + Accounts);
            }
        }
        catch (Exception e1) {
        }

        String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
        String cookieString = rep12Method(Accounts, password, repUrl);
        JSONObject json = JobTrainUtil.initQueryUserInfo(cookieString);
        if (json.containsKey("loginStatus")) {
            System.out.println("未登录");
            return;
        }
        String heyanzhuangtai = json.getString("heyanzhuangtai");
        if ("已通过".contains(heyanzhuangtai)) {
            try {
                WriteFile("ID:" + id + "===>帐号:" + Accounts + "===>状态:" + status, "D:/USER");
                System.out.println("拿到=============》》》");
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    /**
     * 
     * 获取cookie
     * @param logname
     * @param logpassword
     * @param repUrl
     * @return cookie
     * @time 2016年1月6日 下午6:59:56
     * @author ZhiHong
     */
    public String rep12Method(String logname, String logpassword, String repUrl) {
        String damarule = "5,5,5,5,5";
        String paramContent = "";
        paramContent = "logname=" + logname + "&logpassword=" + logpassword + "&damarule=" + damarule
                + "&datatypeflag=12";
        String resultString = "";
        System.out.println(repUrl + paramContent);
        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        System.out.println("拿到Cookie：" + resultString);
        return resultString;
    }

    public void WriteFile(String content, String path) throws IOException {
        File file = new File(path);
        FileWriter fw = null;
        if (!file.exists()) {
            file.createNewFile();
        }
        fw = new FileWriter(file, true);
        BufferedWriter bufferWritter = new BufferedWriter(fw);
        bufferWritter.write(content);
        bufferWritter.close();
    }

    public static void main(String[] args) {
        Disposable disposable = new Disposable();
        do {
            disposable.findUser();
        }
        while (true);
    }
}
