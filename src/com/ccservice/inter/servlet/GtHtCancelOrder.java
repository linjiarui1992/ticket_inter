package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GtHtCancelOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GtHtCancelOrder() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String timeStamp=request.getParameter("timeStamp");
		String partnerName=request.getParameter("partnerName");
		String messageIdentity=request.getParameter("messageIdentity");
		String orderNumber=request.getParameter("orderNumber");
		String sql = "SELECT keys from TrainOfflineAgentKey where partnerName='"+partnerName+"'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String key="";
        String code="";
        String msg="";
        String sql2="select lockedStatus from TrainOrderOffline where OrderNumberOnline='"+orderNumber+"'";
        List list2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(list.size()>0 && list2.size()>0){
            Map map=(Map)list.get(0);
            key=map.get("keys").toString();
            String md5s=MD5Util.MD5Encode(partnerName+timeStamp+key, "UTF-8").toUpperCase();
            Map map2=(Map)list2.get(0);
            if(messageIdentity.equals(md5s) && !"1".equals(map2.get("lockedStatus").toString())){//验证通过
            	String updatesql="UPDATE TrainOrderOffline SET AgentId=2,orderstatus=3 WHERE OrderNumberOnline='"+orderNumber+"'";
            	Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
            	//加操作记录
            	String sqlorder="SELECT id from TrainOrderOffline where OrderNumberonline='"+orderNumber+"'";
        		List listorder = Server.getInstance().getSystemService().findMapResultBySql(sqlorder, null);
        		if(listorder.size()>0){
        			Map maporder=(Map)listorder.get(0);
        			String updatesql1 = "INSERT TrainOrderOfflineRecord(FKTrainOrderOfflineId,ProviderAgentid,DistributionTime,DealResult,RefundReasonStr) "
    						+ " VALUES("
    						+ maporder.get("id").toString()
    						+ ",0,'"
    						+ sdf.format(new Date())
    						+ "',15,'" + "--------高铁管家取消订单成功！--------')";
        			WriteLog.write("高铁管家取消订单sql", "updatesql1:"+updatesql1);
        			Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql1);
        		}
            	
        		code="0";
            	msg="取消订单成功";
            }else if(messageIdentity.equals(md5s) && "1".equals(map2.get("lockedStatus").toString())){
            	code="2";
            	msg="取消订单失败,订单已经锁单成功!";
            }else{
                code="1";
                msg="账号核验失败！";
            }
        }else{
            code="1";
            msg="账号核验失败！";
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        WriteLog.write("高铁管家请求取消线下票_log", "orderNumber="+orderNumber+";timeStamp="+timeStamp+";partnerName="+partnerName+";messageIdentity="+messageIdentity+";responsejson="+responsejson);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
	}

}
