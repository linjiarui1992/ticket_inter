package com.ccservice.inter.servlet.Customeruser;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.server.Server;

/**
 * 实时核验手机号接口
 */
public class CustomeruserCheckMobileServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 68779843216117L;

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");
        int r1 = new Random().nextInt(10000000);
        PrintWriter out = null;
        String type = req.getParameter("type");//类型1核验手机号
        if (type == null) {
            type = "1";
        }
        JSONObject jsonobject = new JSONObject();
        try {
            out = resp.getWriter();
            if ("1".equals(type)) {
                Map<String, String> dateHashMap = Server.getInstance().getDateHashMap();
                Iterator<String> keys = dateHashMap.keySet().iterator();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    String value = dateHashMap.get(key);
                    jsonobject.put(key, value);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (out != null) {
                out.print(jsonobject.toJSONString());
                out.flush();
                out.close();
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
