package com.ccservice.inter.servlet;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.qunar.QunarOrderMethod;

public class TrainTeamAddOrderDB {

    private DBoperationUtil dt = new DBoperationUtil();  
    
    public void exThread(QunarOrderMethod qunarordermethod) {
        JSONObject info=qunarordermethod.getOrderjson();

        String ispaper = info.getString("isPaper");// 是否纸质票
        String arrStationValue = info.getString("arrStation");// 终点站
        String dptStationValue = info.getString("dptStation");// 始发站
        String extSeatValue = info.getString("extSeatMap");// 备选车票
        String extSeatString = getAllExtSeatMap(extSeatValue);//新修改逻辑Map
        String paperType = info.getString("paperType");
        String selectAgentid=info.getString("selectAgentid");
        String paperBackup = info.getString("paperBackup");
        String paperLowSeatCount = info.getString("paperLowSeatCount");
        if (info.containsKey("jointTrip") == false) {// 如果是普通订单,seq:0
            String orderNoValue = info.getString("orderNo");// 订单号
            String seatValue = info.getString("seatMap");// 坐席及其价格
            // 0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
            // TODO 普通订单的坐席类型
            JSONObject seatObject = JSONObject.parseObject(seatValue);
            Set<String> seatSet = seatObject.keySet();
            float seatValue_Value = 0 ;
            String seatValue_Key = "";
            for (String str : seatSet) {
                seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
                seatValue_Key = str;// 得到坐席
          }
//            extSeatValue_lc = info.getString("extSeat");// 联程备选车票及其价格
            float ticketPayValue = (float) info.getDoubleValue("ticketPay");// 应付订单票款
//            String trainEndTimeValue = info.getString("trainEndTime");// 火车到达时间
            Date date_trainEndTime = new Date();
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
//            try {
//                date_trainEndTime = df.parse(trainEndTimeValue);
//            }
//            catch (ParseException e) {
//                e.printStackTrace();
//            }
            long endMilliSeconds = date_trainEndTime.getTime();
            DateFormat dfm = new SimpleDateFormat("HH:mm");
//            trainEndTimeValue = dfm.format(date_trainEndTime);
            String trainNoValue = info.getString("trainNo");// 火车编号
            String trainStartTimeValue = info.getString("trainStartTime");// 火车出发时间
            Date date_trainStartTime = new Date();
//            try {
//                date_trainStartTime = df.parse(trainStartTimeValue);
//            }
//            catch (ParseException e) {
//                e.printStackTrace();
//            }
//            long startMilliSeconds = date_trainStartTime.getTime();
//            int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
//            String costTime = String.valueOf(costtime / (60 * 60)) + ":" + String.valueOf((costtime % (60 * 60)) / 60);// 历时
            JSONArray passengers = info.getJSONArray("passengers");
            Trainorder trainorder = new Trainorder();// 创建订单
            trainorder.setTaobaosendid(extSeatString);
            if ("1".equals(ispaper)) {// 如果去哪儿订单是线下订单
                trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
            }
            trainorder.setQunarOrdernumber(orderNoValue);
            List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
            for (int i = 0; i < passengers.size(); i++) {
                List<Trainticket> traintickets = new ArrayList<Trainticket>();
                JSONObject infopassengers = passengers.getJSONObject(i);
                String certNoValue = infopassengers.getString("certNo");// 乘车人证件号码
                // 1：身份证；C：港澳通行证；G：台湾通行证；B：护照
                String certTypeValue = infopassengers.getString("certType");// 乘车人证件种类
                if ("B".equals(certTypeValue)) {
                    certTypeValue = "3";
                }
                else if ("C".equals(certTypeValue)) {
                    certTypeValue = "4";
                }
                else if ("G".equals(certTypeValue)) {
                    certTypeValue = "5";
                }
                String birthday="";
                if (certNoValue.length() == 18) {
                    String year = certNoValue.substring(6, 10);
                    String month = certNoValue.substring(10, 12);
                    String day = certNoValue.substring(12, 14);
                    birthday = year + "-" + month + "-" + day;
                }
                String nameValue = infopassengers.getString("name");// 乘车人姓名
                int ticketTypeValue=1;
                if (infopassengers.containsKey("ticketType")) {// 票种类型，1：成人票；0：儿童票 2:学生票(如果是去哪儿的老版本，没有ticketType)
                   ticketTypeValue = Integer.parseInt(infopassengers.getString("ticketType"));
                }
                else {
                    ticketTypeValue = 1;
                }
                if(ticketTypeValue==1){
                    ticketTypeValue=1;
                }else if(ticketTypeValue==2){
                    ticketTypeValue=0;
                }else if(ticketTypeValue==3){
                    ticketTypeValue=2;
                }
                // TODO 普通订单存入数据库
                Trainticket trainticket = new Trainticket();
                Trainpassenger trainpassenger = new Trainpassenger();
                trainticket.setArrival(arrStationValue);// 存入终点站
                trainticket.setDeparture(dptStationValue);// 存入始发站
                trainticket.setStatus(Trainticket.WAITISSUE);
                trainticket.setTicketno(infopassengers.getString("passengerId"));
//                trainticket.setticket
                // 无法存入剩余票
                trainticket.setSeq(0);// 存入订单类型
                trainticket.setTickettype(ticketTypeValue);// 存入票种类型
                trainticket.setPayprice(seatValue_Value);
                trainticket.setPrice(seatValue_Value);// 应付火车票价格
//                trainticket.setArrivaltime(trainEndTimeValue);// 火车到达时间
                trainticket.setArrivaltime("");// 火车到达时间
                trainticket.setTrainno(trainNoValue); // 火车编号
                trainticket.setDeparttime(trainStartTimeValue);// 火车出发时间
                trainticket.setIsapplyticket(1);
                trainticket.setRefundfailreason(0);
//                trainticket.setCosttime(costTime);// 历时
                trainticket.setCosttime("");// 历时
                //                Map<String, String> seatmap = extSeat(seatValue_Key, extSeatValue);
                //                seatValue_Key = seatmap.get("seatCode");
                //                extSeatValue = seatmap.get("extSeat");
                trainticket.setSeattype(seatValue_Key);// 车票坐席
                trainticket.setInsurenum(0);
                trainticket.setInsurprice(0f);// 采购支付价
                trainticket.setInsurorigprice(0f);// 保险原价
                traintickets.add(trainticket);
                trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
                trainpassenger.setIdtype(Integer.parseInt(certTypeValue));// 存入乘车人证件种类
                trainpassenger.setName(nameValue);// 存入乘车人姓名
                trainpassenger.setBirthday(birthday);
                trainpassenger.setChangeid(0);
                trainpassenger.setAduitstatus(1);
                trainpassenger.setTraintickets(traintickets);
                trainplist.add(trainpassenger);
            }
            //纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
            trainorder.setPaymethod(Integer.parseInt(paperType));
            //当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
            trainorder.setSupplypayway(Integer.parseInt(paperBackup));
            //至少接受下铺/靠窗/连坐数量
            trainorder.setRefuseaffirm(Integer.parseInt(paperLowSeatCount));
            trainorder.setContactuser(info.getString("transportName"));
            trainorder.setContacttel(info.getString("transportPhone"));
            trainorder.setTicketcount(trainplist.size());
            trainorder.setCreateuid(66l);
            trainorder.setInsureadreess(info.getString("transportAddress"));

            trainorder.setOrderprice(ticketPayValue);
            trainorder.setIsjointtrip(0);
            trainorder.setAgentid(66);
            trainorder.setState12306(Trainorder.WAITORDER);
            trainorder.setPassengers(trainplist);
            trainorder.setOrderstatus(Trainorder.WAITISSUE);
            trainorder.setAgentprofit(0f);
            trainorder.setCommission(0f);
            trainorder.setTcprocedure(0f);
            trainorder.setInterfacetype(2);
            trainorder.setCreateuser("团队票");
            try {
                WriteLog.write("qunartrainorder", trainorder.getQunarOrdernumber() + "(:)" + trainorder.getId());
                if (trainorder.getOrdertype() == 2) {// 去哪儿的线下订单去线下订单下单
                    WriteLog.write("去哪儿火车票类型", "线下");
                    trainorderofflineadd(trainorder,selectAgentid);

                }
            }
            catch (Exception e) {
                WriteLog.write("MyThreadQunarOrder_LadanException_Error", info.toString());
                ExceptionUtil.writelogByException("MyThreadQunarOrder_LadanException_Error", e);
            }
        }
    }
 
    public static String getAllExtSeatMap(String extSeatValue) {
        String extString = "";
        JSONArray array = JSONArray.parseArray(extSeatValue);
        float seatValue_Value;
        for (int i = 0; i < array.size(); i++) {
            JSONObject seatObject = array.getJSONObject(i);
            Set<String> seatSet = seatObject.keySet();
            for (String str : seatSet) {
                seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
                extString+=str+":￥" + seatValue_Value + ",";
            }

        }
        if (extString.contains(",")) {
            extString = extString.substring(0, extString.length() - 1);
        }
        else {
            extString = "无";
        }
        return extString;

    }
    public void trainorderofflineadd(Trainorder trainorder,String selectAgentid) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        String addresstemp = trainorder.getInsureadreess();
        // 通过订单邮寄地址匹配出票点
        String agentidtemp = selectAgentid;
        WriteLog.write("线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());
        SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
        Timestamp startTime = new Timestamp(new Date().getTime());
        trainOrderOffline.setOrderTimeOut(startTime);
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        //通过出票点和邮寄地址获取预计到达时间results
//        String delieveStr=getDelieveStr(agentidtemp,trainorder.getInsureadreess());//
        String delieveStr="无";
        String updatesql="UPDATE TrainOrderOffline SET expressDeliver ='"+delieveStr+"' WHERE ID="+orderid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        // String insureaddress=trainorder.getInsureadreess();
        //        String insureaddress = "郑宇凤^^^河北^^^石家庄市^^^长安区^^^河北省石家庄市长安区裕华东路99号就业服务大厦^^^050000^^^18819411570^^^";
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        //        address.setProvinceName(splitadd[0]);
        //        address.setCityName(splitadd[1]);
        //        address.setRegionName(splitadd[2]);
        //        address.setTownName("桥东区政府");
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date2=new Date();
                String sss=ticket.getDeparttime();
                try {
                    date2=sdf2.parse(sss);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                sss=sdf2.format(date2);
                trainTicketOffline.setDepartTime(Timestamp.valueOf(sss+":00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(10, 15));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                trainTicketOffline.setSubOrderId(ticket.getTicketno());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }
}
