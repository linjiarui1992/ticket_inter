package com.ccservice.inter.servlet;

import javax.jms.Session;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.ConnectionFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.ActiveMQConnectionFactory;
import com.ccservice.train.mqlistener.TrainConfirmChangeMessageListener;

@SuppressWarnings("serial")
public class QueueMQ_TrainConfirmChange extends HttpServlet {

    private String mqaddress = "";//MQ地址

    private String mqusername = "";//MQ用户名

    private String isstart = "";//是否开启

    private int confirmchangenum = 0;

    public void init() throws ServletException {
        super.init();
        mqaddress = getInitParameter("mqaddress");
        mqusername = getInitParameter("mqusername");
        isstart = getInitParameter("isstart");
        confirmchangenum = Integer.parseInt(getInitParameter("confirmchangenum"));
        if ("1".equals(isstart)) {
            System.out.println("改签确认队列-----开启");
            changeNotice();
        }
    }

    private void changeNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < this.confirmchangenum; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainConfirmChangeMessageListener());
            }
            conn.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}