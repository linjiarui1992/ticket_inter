package com.ccservice.inter.servlet;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.ccervice.huamin.update.FileCopyJob;
import com.ccervice.huamin.update.HotelImageUpdateJob;
import com.ccervice.huamin.update.HuaMinFileUpdateJob;
import com.ccervice.huamin.update.UpdateContract;
import com.ccervice.huamin.update.UpdateHotelInfoJob;
import com.ccervice.huamin.update.UpdateInternationalHotelInfoJob;
import com.ccervice.huamin.update.UpdateLastDayPrice;
import com.ccervice.huamin.update.UpdateOneMonthPrice;
import com.ccservice.b2b2c.atom.servlet.job.JobAlitripTraindetails;
import com.ccservice.bussinessman.UpdateHotelInfoBus;
import com.ccservice.bussinessman.UpdateHotelPriceBus;
import com.ccservice.compareprice.ComPareHotelGoodDataJob;
import com.ccservice.compareprice.FindRoomStausJob;
import com.ccservice.compareprice.HMchangePriceUpdate;
import com.ccservice.compareprice.HmFileParseJob;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.compareprice.UpdatePriceJob;
import com.ccservice.compareprice.UpdatePriceOneDayJob;
import com.ccservice.compareprice.travelsky.SuppliersCloseRoomJob;
import com.ccservice.compareprice.travelsky.SuppliersComparePriceWithQunarJob;
import com.ccservice.compareprice.travelsky.SuppliersUpdatePriceForQunarJob;
import com.ccservice.compareprice.travelsky.sync.TravelSkyDataJob;
import com.ccservice.elong.hoteldb.HotelImageDown;
import com.ccservice.elong.hoteldb.UpdateBrandDB;
import com.ccservice.elong.hoteldb.UpdateHotelDB;
import com.ccservice.elong.hotelorderupdate.HotelOrderUpdate;
import com.ccservice.inter.job.Findinternational;
import com.ccservice.inter.job.Findspecialprice;
import com.ccservice.inter.job.JobElongHotelBaseUpdate;
import com.ccservice.inter.job.Tourism.JobCtripDemosticTourism;
import com.ccservice.inter.job.Visa.JobCtripVisa;
import com.ccservice.inter.job.tcreport.JobTongChengReport;
import com.ccservice.inter.job.train.ChangeReturnTicketJob;
import com.ccservice.inter.job.train.CheckReturnTicketJob;
import com.ccservice.inter.job.train.Job12306CheckIsenable;
import com.ccservice.inter.job.train.Job12306Edituserinfo;
import com.ccservice.inter.job.train.Job12306GetPayUrl;
import com.ccservice.inter.job.train.Job12306Registration;
import com.ccservice.inter.job.train.Job12306Registration_addMailUser;
import com.ccservice.inter.job.train.Job12306Verification;
import com.ccservice.inter.job.train.Job12306index;
import com.ccservice.inter.job.train.JobAlipaytradenum;
import com.ccservice.inter.job.train.JobAutoPayAlipay;
import com.ccservice.inter.job.train.JobAutoPayAlipayNew;
import com.ccservice.inter.job.train.JobAutoPayUnion;
import com.ccservice.inter.job.train.JobBacklogOfOrders;
import com.ccservice.inter.job.train.JobDeletePerson;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.JobIndexAgain;
import com.ccservice.inter.job.train.JobMonitoringOfPayments;
import com.ccservice.inter.job.train.JobQunarGenerateOrder;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.job.train.JobQunarOrderRefund;
import com.ccservice.inter.job.train.JobTaobaoPersonateOrderCallback;
import com.ccservice.inter.job.train.JobTongchengTrainOrderTimeout;
import com.ccservice.inter.job.train.JobUnionTradenum;
import com.ccservice.inter.job.train.account.Job12306AccountCheckMobileNew;
import com.ccservice.inter.job.zhifubao.Job12306PayRecord;
import com.ccservice.inter.job.zhifubao.PayLogToDatabase;
import com.ccservice.inter.job.zhifubao.PayTradetoOrder;
import com.ccservice.inter.job.zrate.Job51Book;
import com.ccservice.inter.job.zrate.Job51BookUpdate;
import com.ccservice.inter.job.zrate.Job8000yi;
import com.ccservice.inter.job.zrate.JobJRRatePackage;
import com.ccservice.inter.job.zrate.JobJRUpdateRatePackage;
import com.ccservice.inter.job.zrate.Yi9eUpdate;
import com.ccservice.inter.server.Server;
import com.ccservice.jielv.hotel.QunarRoomType;
import com.ccservice.jielv.hotel.UpdateJielvModifyPrice;
import com.ccservice.jielv.hotelupdate.UpdateDataJob1;
import com.ccservice.jielv.hotelupdate.UpdateJlPrice2;
import com.ccservice.jielv.hotelupdate.UpdateJllastDayPriceJob5;
import com.ccservice.jielv.hotelupdate.UpdateJlpriceJob4;
import com.ccservice.jielv.hotelupdate.UpdatejlhrJob3;
import com.ccservice.mixdata.FindQunarHotel;
import com.ccservice.mixdata.FindQunarHotelIdJob;
import com.ccservice.mixdata.UpdateHotelallPayTypeJob;
import com.ccservice.newelong.hoteldb.HotelLowestPriceAndStatusDB;
import com.ccservice.newelong.hoteldb.NewGeoCNDB;
import com.ccservice.newelong.hoteldb.NewHotelDB;
import com.ccservice.newjltour.updatejob.NewJlHotelJob;
import com.ccservice.newjltour.updatejob.NewJlPriceJob;
import com.ccservice.qunar.cache.CatchQunarHotelPriceJob;
import com.ccservice.report.train.TrainExpReportUpdate;
import com.ccservice.report.train.TrainReportUpdate;
import com.ccservice.taobao.hotelupdate.TaoBaoBuyerUpdateJob;
import com.ccservice.taobao.hotelupdate.TaoBaoLiuLiangJob;
import com.ccservice.train.order.TrainDownOrderJob;
import com.ccservice.train.station.StationUpdateJob;

/**
 * Servlet implementation class for Servlet: InitServerlet
 * 
 */
public class InitServerlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
    static final long serialVersionUID = 1L;

    private Scheduler scheduler;

    /*
     * (non-Java-doc)
     * 
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public InitServerlet() {
        super();

    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        //初始化的时候清空里面的数据
        Server.getInstance().setDateHashMap(new HashMap<String, String>());
        super.init(config);
        System.out.println("Start rate inter...");
        // FiveonBook fiveonBook = new FiveonBook();
        Server.getInstance().setUrl(config.getInitParameter("service_url"));
        Server.getInstance().seturlAtom(config.getInitParameter("inter_url"));
        Server.getInstance().setCnserviceurl(config.getInitParameter("51bookseach_url"));
        // 自动扣款
        Server.getInstance().setIsAutoPay(config.getInitParameter("isAutoPay"));
        // 是否创建pnr
        Server.getInstance().setCreatePnr(config.getInitParameter("createPNR"));
        // 在哪创建订单
        Server.getInstance().setIsCreateExtOrder(config.getInitParameter("isCreateExtOrder"));
        initAir(config);

        String TaoBaoChangetime = config.getInitParameter("TaoBaoChangetime");//淘宝车站数据没日1点
        String timeof8000yi = config.getInitParameter("8000yitime");// 8000翼同步时间
        String time51book = config.getInitParameter("51bookupdate");// 51book同步时间
        String timeYXbook = config.getInitParameter("YXTXupdatetime");// 51book同步时间
        String jielvTime = config.getInitParameter("jielvTime");// 捷旅同步时间
        String ylTime = config.getInitParameter("ylTime");// 艺龙更新时间
        String ylWeekTime = config.getInitParameter("ylWeekTime");// 艺龙周更新时间
        String elLowestPriceAndStatusTime = config.getInitParameter("elLowestPriceAndStatusTime");// 艺龙酒店最低价格、状态更新
        String zsHotelPayTypeTime = config.getInitParameter("zsHotelPayTypeTime");// 正式酒店支付方式更新
        String FindQunarHotelUpdateTime = config.getInitParameter("FindQunarHotelUpdateTime");// 抓取所有去哪儿酒店
        String rtTime = config.getInitParameter("rtTime");
        String hmupdatefiletime = PropertyUtil.getValue("hmupdatefiletime");// 华闽同步时间
        String timespecialprice = config.getInitParameter("specialpricestarttime");
        String timeinternationalstart = config.getInitParameter("internationalstarttime");
        String timeofqunarorder = config.getInitParameter("qunarorderTime");//去哪儿火车票订单同步时间
        String timeofindex12306 = config.getInitParameter("index12306Time");//12306账号自动登录时间
        String JobUnionTradenumtime = config.getInitParameter("JobUnionTradenumtime");//自动更新银联流水号控制
        String JobAlipaytradenumtime = config.getInitParameter("JobAlipaytradenumtime");//支付宝自动更新流水号控制
        String JobgeturlTradenumtime = config.getInitParameter("JobgeturlTradenumtime");//自动更新银联流水号控制
        String timeofgenerateorder = config.getInitParameter("generateorderTime");//12306账号自动下单时间
        String timeofqunargenerateorder = config.getInitParameter("qunargenerateorderTime");//QUNAR占座自动下单时间
        String timeofdeleteperson = config.getInitParameter("deletepersonTime");//删除12306账号中不是已通过的乘客
        String timeofverification = config.getInitParameter("verificationTime");//访问12306保持12306账号在线时间
        String timeofregistration = config.getInitParameter("registrationTime");//自动注册12306账号
        String timeofautopay = config.getInitParameter("autopayTime");//火车票自动支付
        String timeofindexagain = config.getInitParameter("indexagainTime");//检查登录12306账号
        String timeofqunarorderrefund = config.getInitParameter("qunarorderrefundTime");//同步去哪儿退票
        String timeofbacklogoforders = config.getInitParameter("backlogofordersTime");//自动获取未支付订单
        String timeofautopayunion = config.getInitParameter("autopayunionTime");//银联自动支付
        String timeofautopayalipaynew = config.getInitParameter("Autopayalipaynewtime");//支付宝自动支付
        String timeoftongchengtrainordertimeout = config.getInitParameter("tongchengtrainordertimeouttime");//支付宝自动支付

        String AutoChangeRefundFlag = config.getInitParameter("AutoChangeRefundFlag");//自动改签退
        String AutoChangeRefundTime = config.getInitParameter("AutoChangeRefundTime");//自动改签退
        String AutoCheckRefundFlag = config.getInitParameter("AutoCheckRefundFlag");//自动审核、退款
        String AutoCheckRefundTime = config.getInitParameter("AutoCheckRefundTime");//自动审核、退款
        String TaoBaoChange = "0";

        String AutoCheckTrainreport = config.getInitParameter("AutoCheckTrainreport");//自动导出火车票报表开关
        String AutoCheckTrainreportTime = config.getInitParameter("AutoCheckTrainreportTime");//自动导出报表时间

        String AutoCheck12306Trainreport = config.getInitParameter("AutoCheck12306Trainreport");//自动导出12306火车票报表开关
        String AutoCheck12306TrainreportTime = config.getInitParameter("AutoCheck12306TrainreportTime");//自动12306导出报表时间

        String TrainDownOrderflag = config.getInitParameter("TrainDownOrderflag");//自动下单开关
        String PayTradetoOrderflag = config.getInitParameter("PayTradetoOrderflag");//自动匹配交易记录开关
        String PayLogToDatabaseflag = config.getInitParameter("PayLogToDatabaseflag");//自动读取交易记录入库
        String TrainDownOrderTime = config.getInitParameter("TrainDownOrderTime");//自动下单时间
        String TrainExptionReportJobflag = config.getInitParameter("TrainExptionReportJobflag");//自动导出异常报表
        String TrainExptionReportJobtime = config.getInitParameter("TrainExptionReportJobtime");//自动导出异常报表时间
        String PayTradetoOrderTime = config.getInitParameter("PayTradetoOrderTime");//自动匹配交易记录时间
        String PayLogToDatabaseTime = config.getInitParameter("PayLogToDatabaseTime");//自动读取交易记录入库

        String StationName12306UpdateTime = config.getInitParameter("StationName12306UpdateTime");//12306更新交易记录时间
        String StationName12306Flag = config.getInitParameter("StationName12306Flag");//12306更新交易记录时间
        String jobTaobaoCallBack3151Flag = config.getInitParameter("jobTaobaoCallBack3151Flag");//淘宝3151回调是否超时查询

        String baqianyiisstar = "0";
        String qunarOrderstar = "0";
        String verificationstar = "0";
        String index12306star = "0";
        String JobUnionTradenumstat = "0";
        String JobAlipaytradenumstat = "0";
        String Job12306GetPayUrltat = "0";
        String generateorderstar = "0";
        String qunargenerateorderstar = "0";
        String deletepersonstar = "0";
        String registrationstar = "0";
        String autopaystar = "0";
        String indexagainstar = "0";

        String qunarorderrefundstar = "0";
        String backlogofordersstar = "0";
        String autopayunionstar = "0";
        String autopayalipaynew = "0";
        String tongchengtrainordertimeoutstar = "0";
        String wuyibookisstar = "0";
        String jielvstar = "0";
        String rtstar = "0";
        String jinriisstar = "0";
        String jinritongbu = "";
        String wuyibooktongbu = "0";
        String hotelstar = "0";
        String hotelgeo = "0";
        String hotelbrand = "0";
        String specialpricestart = "0";
        String internationalstart = "0";
        String ylstar = "0";
        String weekylstar = "0";
        String elLowestPriceAndStatusUpdate = "0";
        String zsHotelPayTypeUpdate = "0";
        String FindQunarHotelUpdate = "0";
        String YXTXisstar = "0";
        String YXTXtongbu = "0";
        String synchroCtripDomesticTourism = "0";
        String synchroCtripVisa = "0";

        YXTXisstar = config.getInitParameter("YXTXisstar");
        YXTXtongbu = config.getInitParameter("YXTXtongbu");

        internationalstart = config.getInitParameter("internationalstart");

        specialpricestart = config.getInitParameter("specialpricestart");

        TaoBaoChange = config.getInitParameter("TaoBaoChange");//淘宝车站是否开启
        qunarOrderstar = config.getInitParameter("qunarOrderstar");//去哪儿是否开启更新
        verificationstar = config.getInitParameter("verificationstar");//12306账号是否开启保持在线
        index12306star = config.getInitParameter("index12306star");//12306账号是否开启自动登录
        JobUnionTradenumstat = config.getInitParameter("JobUnionTradenumstat");//银联自动更新开启控制
        JobAlipaytradenumstat = config.getInitParameter("JobAlipaytradenumstat");//支付宝自动更新开启控制
        Job12306GetPayUrltat = config.getInitParameter("Job12306GetPayUrltat");//银联抓取链接
        generateorderstar = config.getInitParameter("generateorderstar");//12306账号是否开启自动下单
        qunargenerateorderstar = config.getInitParameter("qunargenerateorderstar");//12306账号是否开启自动下单QUNAR占座
        deletepersonstar = config.getInitParameter("deletepersonstar");//删除12306中不是已通过乘客是否开启
        registrationstar = config.getInitParameter("registrationstar");//自动支付是否开启
        autopaystar = config.getInitParameter("autopaystar");//自动支付是否开启
        indexagainstar = config.getInitParameter("indexagainstar");//检查登录12306是否开启
        qunarorderrefundstar = config.getInitParameter("qunarorderrefundstar");//自动同步qunar退票
        autopayunionstar = config.getInitParameter("autopayunionstar");//银联自动支付
        autopayalipaynew = config.getInitParameter("Autopayalipaynewstat");//支付宝自动支付
        tongchengtrainordertimeoutstar = config.getInitParameter("tongchengtrainordertimeoutstar");//同程超时订单转变
        baqianyiisstar = config.getInitParameter("8000yiisstar");// 8000翼是否开启更新
        // 1,更新
        // 0,不更新
        wuyibookisstar = config.getInitParameter("51bookisstar");// 51book是否开启更新

        jielvstar = config.getInitParameter("JLUpdate");// 捷旅是否更新

        ylstar = config.getInitParameter("ylUpdate");// 艺龙是否更新

        rtstar = config.getInitParameter("JLRMT");// 房型对应去哪儿

        weekylstar = config.getInitParameter("ylWeekUpdate");// 艺龙周更新是否开启

        elLowestPriceAndStatusUpdate = config.getInitParameter("elLowestPriceAndStatusUpdate");// 艺龙酒店最低价格、状态更新

        zsHotelPayTypeUpdate = config.getInitParameter("zsHotelPayTypeUpdate");// 正式酒店支付方式更新

        FindQunarHotelUpdate = config.getInitParameter("FindQunarHotelUpdate");// 抓取所有去哪儿酒店

        rtstar = config.getInitParameter("JLRMT");// 房型对应去哪儿

        // 1,更新
        // 0,不更新
        jinriisstar = config.getInitParameter("jinriisstar");// 今日是否开启全取 1,更新

        jinritongbu = config.getInitParameter("jinriistongbu");

        wuyibooktongbu = config.getInitParameter("51bookistongbu");// 51book同步
        // 1,更新
        // 0,不更新
        hotelstar = config.getInitParameter("hotelStar");
        hotelgeo = config.getInitParameter("hotelgeo");
        hotelbrand = config.getInitParameter("hotelbrand");
        // 同步携程国内旅游数据 0不同步 1同步
        synchroCtripDomesticTourism = config.getInitParameter("synchroCtripDomesticTourism");
        // 同步携程签证数据 0不同步 1同步
        synchroCtripVisa = config.getInitParameter("synchroCtripVisa");

        String hmchangepriceupdate = "0";
        String compareprice = "0";
        String findroomstatus = "0";
        String findroomstatustime = "";
        String hmfileupdate = "";
        String hmfiletime = "";

        String updateonemonth = "";
        String updateonemonthtime = "";
        String updateonemonthlastday = "";
        String updateonemonthtimelastday = "";
        updateonemonth = PropertyUtil.getValue("updateonemonth");
        updateonemonthtime = PropertyUtil.getValue("updateonemonthtime");
        updateonemonthlastday = PropertyUtil.getValue("updateonemonthlastday");
        updateonemonthtimelastday = PropertyUtil.getValue("updateonemonthtimelastday");

        String comparetimetime = PropertyUtil.getValue("comparetimetime");
        hmchangepriceupdate = PropertyUtil.getValue("hmchangepriceupdate");
        compareprice = PropertyUtil.getValue("compareprice");
        findroomstatus = PropertyUtil.getValue("findroomstatus");
        findroomstatustime = PropertyUtil.getValue("findroomstatustime");
        hmfileupdate = PropertyUtil.getValue("hmfileupdate");
        hmfiletime = PropertyUtil.getValue("hmfiletime");

        String huaminhotelinfoopenclose = "";
        String huaminInternationalHotelInfoOpenClose = "";// 华闽国际酒店
        String huamincontractopenclose = "";
        String huaminpriceopenclose = "";
        String huaminlastdayopenclose = "";
        String filecopyopenclose = "";// 文件复制job
        String fileupdateydxopenclose = "";
        String Imageopenclose = "";
        String elongorderupdateopenclose = "";

        String filecopytime = "";
        String huaminhotelinfotime = "";
        String huaminInternationalHotelInfoTime = "";
        String huamincontractshijian = "";
        String huaminpricetime = "";
        String huaminlastdaytime = "";
        String fileupdateydxtime = "";
        String ImageopencloseTime = "";
        String elongimageopenclose = "";
        String elongimagetime = "";
        String elongorderupdatetime = "";

        // 捷旅更新模块start
        String updatedatajobopenclose = "";
        String updateJlPriceopenclose = "";
        String updatejlhrJobopenclose = "";
        String updateJlpriceJobopenclose = "";
        String updateJllastDayPriceopenclose = "";

        String updatedatajobopentime = "";
        String updateJlPriceopentime = "";
        String updatejlhrJobopentime = "";
        String updateJlpriceJobopentime = "";
        String updateJllastDayPricetime = "";

        updatedatajobopenclose = PropertyUtil.getValue("updatedatajobopenclose");
        updateJlPriceopenclose = PropertyUtil.getValue("updateJlPriceopenclose");
        updatejlhrJobopenclose = PropertyUtil.getValue("updatejlhrJobopenclose");
        updateJlpriceJobopenclose = PropertyUtil.getValue("updateJlpriceJobopenclose");
        updateJllastDayPriceopenclose = PropertyUtil.getValue("updateJllastDayPriceopenclose");

        updatedatajobopentime = PropertyUtil.getValue("updatedatajobopentime");
        updateJlPriceopentime = PropertyUtil.getValue("updateJlPriceopentime");
        updatejlhrJobopentime = PropertyUtil.getValue("updatejlhrJobopentime");
        updateJlpriceJobopentime = PropertyUtil.getValue("updateJlpriceJobopentime");
        updateJllastDayPricetime = PropertyUtil.getValue("updateJllastDayPricetime");
        getstaus(updatedatajobopenclose, "整体更新捷旅基础数据");
        getstaus(updateJlPriceopenclose, "整体更新捷旅价格==");
        getstaus(updateJlPriceopenclose, "整体更新捷旅价格==");
        getstaus(updatejlhrJobopenclose, "持续更新酒店房型动更新程序==");
        getstaus(updateJlpriceJobopenclose, "持续更新价格更新程序==");
        getstaus(updateJllastDayPriceopenclose, "捷旅更新最后一天价格==");

        // 捷旅更新模块over

        //深捷旅JSON接口更新数据
        String jlUpdateBaseFlag = PropertyUtil.getValue("jlUpdateBaseFlag");
        String jlUpdateBaseTime = PropertyUtil.getValue("jlUpdateBaseTime");
        getstaus(jlUpdateBaseFlag, "~~~~~~~~~~JSON接口更新深捷旅酒店及房型基础信息~~~~~~~~~~");

        String jlUpdatePriceFlag = PropertyUtil.getValue("jlUpdatePriceFlag");
        String jlUpdatePriceTime = PropertyUtil.getValue("jlUpdatePriceTime");
        getstaus(jlUpdatePriceFlag, "~~~~~~~~~~JSON接口整体更新深捷旅价格~~~~~~~~~~");

        //淘宝酒店买家价格、房量更新
        String TaoBaoBuyerUpdate = PropertyUtil.getValue("TaoBaoBuyerUpdate");
        String TaoBaoBuyerUpdateTime = PropertyUtil.getValue("TaoBaoBuyerUpdateTime");
        getstaus(TaoBaoBuyerUpdate, "~~~~~~~~~~淘宝酒店买家价格、房量持续更新~~~~~~~~~~");

        String TaoBaoLiuLiangUpdate = PropertyUtil.getValue("TaoBaoLiuLiangUpdate");
        String TaoBaoLiuLiangUpdateTime = PropertyUtil.getValue("TaoBaoLiuLiangUpdateTime");
        getstaus(TaoBaoLiuLiangUpdate, "~~~~~~~~~~淘宝酒店买家流量统计、重置~~~~~~~~~~");

        //酒店比价上去哪儿
        String jlComparePriceUpdate = PropertyUtil.getValue("jlComparePriceUpdate");
        String jlComparePriceUpdateTime = PropertyUtil.getValue("jlComparePriceUpdateTime");
        getstaus(jlComparePriceUpdate, "~~~~~~~~~~酒店比价上去哪儿、同步供应商数据~~~~~~~~~~");

        String jlUpdatePriceForQunarUpdate = PropertyUtil.getValue("jlUpdatePriceForQunarUpdate");
        String jlUpdatePriceForQunarUpdateTime = PropertyUtil.getValue("jlUpdatePriceForQunarUpdateTime");
        getstaus(jlUpdatePriceForQunarUpdate, "~~~~~~~~~~酒店比价上去哪儿、更新供应商数据~~~~~~~~~~");

        String qunarCloseRoom = PropertyUtil.getValue("qunarCloseRoom");
        String qunarCloseRoomTime = PropertyUtil.getValue("qunarCloseRoomTime");
        getstaus(qunarCloseRoom, "~~~~~~~~~~酒店比价上去哪儿、定时关当日房间~~~~~~~~~~");

        String OpenCatchQunarHotelPrice = PropertyUtil.getValue("OpenCatchQunarHotelPrice");
        String OpenCatchQunarHotelPriceTime = PropertyUtil.getValue("OpenCatchQunarHotelPriceTime");
        getstaus(OpenCatchQunarHotelPrice, "~~~~~~~~~~酒店缓存去哪儿价格~~~~~~~~~~");

        String SyncQunarHotelStatus = PropertyUtil.getValue("SyncQunarHotelStatus");
        String SyncQunarHotelStatusTime = PropertyUtil.getValue("SyncQunarHotelStatusTime");
        getstaus(SyncQunarHotelStatus, "~~~~~~~~~~同步去哪儿酒店状态~~~~~~~~~~");

        //中航信酒店
        String loadAllTravelSkyHotel = PropertyUtil.getValue("loadAllTravelSkyHotel");
        String loadAllTravelSkyHotelTime = PropertyUtil.getValue("loadAllTravelSkyHotelTime");
        getstaus(loadAllTravelSkyHotel, "中航信酒店同步==");

        // 生意人更新模块
        String updateHotelInfoBusopenclose = "";
        String updateHotelInfoBustime = "";
        String updateHotelPriceBusopenclose = "";
        String updateHotelPriceBustime = "";

        updateHotelInfoBusopenclose = PropertyUtil.getValue("updateHotelInfoBusopenclose");
        updateHotelInfoBustime = PropertyUtil.getValue("updateHotelInfoBustime");
        updateHotelPriceBusopenclose = PropertyUtil.getValue("updateHotelPriceBusopenclose");
        updateHotelPriceBustime = PropertyUtil.getValue("updateHotelPriceBustime");

        getstaus(updateHotelInfoBusopenclose, "生意人更新酒店价格==");
        getstaus(updateHotelPriceBusopenclose, "生意人更新价格==");

        String updatequnarIdopenclose = "";
        String updatequnarIdtime = "";
        updatequnarIdopenclose = PropertyUtil.getValue("updatequnarIdopenclose");
        updatequnarIdtime = PropertyUtil.getValue("updatequnarIdtime");
        getstaus(updatequnarIdopenclose, "更新去哪Id==");
        elongimageopenclose = PropertyUtil.getValue("elongimageopenclose");
        fileupdateydxopenclose = PropertyUtil.getValue("fileupdateydxopenclose");
        filecopyopenclose = PropertyUtil.getValue("filecopyopenclose");
        huaminhotelinfoopenclose = PropertyUtil.getValue("huaminhotelinfoopenclose");
        huaminInternationalHotelInfoOpenClose = PropertyUtil.getValue("huaminInternationalHotelInfoOpenClose");
        huaminlastdayopenclose = PropertyUtil.getValue("huaminlastdayopenclose");
        huaminpriceopenclose = PropertyUtil.getValue("huaminpriceopenclose");
        huamincontractopenclose = PropertyUtil.getValue("huamincontractopenclose");
        Imageopenclose = PropertyUtil.getValue("Imageopenclose");
        elongorderupdateopenclose = PropertyUtil.getValue("elongorderupdateopenclose");
        elongimagetime = PropertyUtil.getValue("elongimagetime");
        fileupdateydxtime = PropertyUtil.getValue("fileupdateydxtime");
        filecopytime = PropertyUtil.getValue("filecopytime");
        huaminhotelinfotime = PropertyUtil.getValue("huaminhotelinfotime");
        huaminInternationalHotelInfoTime = PropertyUtil.getValue("huaminInternationalHotelInfoTime");
        huaminpricetime = PropertyUtil.getValue("huaminpricetime");
        huamincontractshijian = PropertyUtil.getValue("huamincontractshijian");
        huaminlastdaytime = PropertyUtil.getValue("huaminlastdaytime");
        ImageopencloseTime = PropertyUtil.getValue("ImageopencloseTime");
        elongorderupdatetime = PropertyUtil.getValue("elongorderupdatetime");
        //        System.out.println("****************酒店相关********************");
        getstaus(elongorderupdateopenclose, "艺龙订单状态自动更新程序==");
        getstaus(Imageopenclose, "图片抓去更新==");
        getstaus(fileupdateydxopenclose, "本地华闽价格房态更新==");
        getstaus(filecopyopenclose, "文件复制==");
        getstaus(huaminhotelinfoopenclose, "更新酒店信息==");
        getstaus(huaminInternationalHotelInfoOpenClose, "初始化华闽国际酒店信息==");
        getstaus(huamincontractopenclose, "更新合同附属信息==");
        getstaus(huaminpriceopenclose, "更新华闽所有一个月的价格==");
        getstaus(huaminlastdayopenclose, "更新华闽最后一天的价格==");
        getstaus(updateonemonth, "更新一个月的价格==");
        getstaus(hmfileupdate, "华闽文件更新==");
        getstaus(findroomstatus, "华闽房态更新==");
        getstaus(compareprice, "华闽比价==");
        getstaus(hmchangepriceupdate, "华闽变价文件更新：");
        getstaus(jielvstar, "捷旅更新状态==");
        getstaus(hotelgeo, "艺龙地理位置更新初始化==");
        getstaus(ylstar, "艺龙每天更新状态==");
        getstaus(weekylstar, "艺龙每周更新状态==");
        getstaus(elLowestPriceAndStatusUpdate, "艺龙酒店最低价格、状态更新==");
        getstaus(zsHotelPayTypeUpdate, "正式酒店支付方式更新==");
        getstaus(FindQunarHotelUpdate, "抓取所有去哪儿酒店==");
        getstaus(rtstar, "去哪儿房型对比：");
        getstaus(YXTXisstar, "易行天下基础数据包状态==");
        getstaus(YXTXtongbu, "易行天下更新数据包状态==");
        getstaus(hotelstar, "艺龙酒店更新==");
        getstaus(synchroCtripDomesticTourism, "同步携程国内旅游数据==");
        getstaus(synchroCtripVisa, "同步携程签证数据==");
        System.out.println("****************机票相关********************");
        getstaus(specialpricestart, "国内特价机票==");
        getstaus(internationalstart, "国际特价机票==");
        getstaus(jobTaobaoCallBack3151Flag,"淘宝3151回调自动查询是否超时=========");

        getstaus(TaoBaoChange, "淘宝次详情更新状态==");
        getstaus(baqianyiisstar, "8000翼更新状态==");
        getstaus(wuyibookisstar, "51book全取状态==");
        getstaus(wuyibooktongbu, "51book同步状态==");
        getstaus(jinriisstar, "今日基础数据包状态==");
        getstaus(jinritongbu, "今日更新数据包状态==");
        getstaus(Server.getInstance().getIsAutoPay(), "机票自动支付供应是否开启==");
        System.out.println("****************火车票相关********************");
        getstaus(qunarOrderstar, "去哪儿火车票订单==");
        getstaus(index12306star, "早上一次自动登录12306=======================");
        getstaus(verificationstar, "定时访问12306,保持在线,并将已通过人数同步到数据库===");
        getstaus(indexagainstar, "检查登录12306,定时保证12306在线账号数量=========");
        getstaus(JobUnionTradenumstat, "银联自动更新流水号===========================");
        getstaus(autopayalipaynew, "支付宝自动支付===============================");
        getstaus(JobAlipaytradenumstat, "支付宝自动更新流水号===========================");
        getstaus(Job12306GetPayUrltat, "自动抓取支付链接============================");
        getstaus(autopayunionstar, "银联自动支付===============================");
        getstaus(tongchengtrainordertimeoutstar, "同程超时订单转换=========================");
        getstaus(generateorderstar, "12306账号全自动下单=========================");
        getstaus(qunargenerateorderstar, "QUNAR占座全自动下单=========================");
        getstaus(deletepersonstar, "删除12306中不是已通过乘客=====================");
        getstaus(registrationstar, "自动注册12306账号===========================");
        getstaus(autopaystar, "火车票自动支付===============================");
        getstaus(backlogofordersstar, "自动获取未支付订单============================");
        getstaus(qunarorderrefundstar, "同步qunar退票==============================");
        getstaus(AutoChangeRefundFlag, "火车票，自动改签退============================");
        getstaus(AutoCheckRefundFlag, "火车票，自动审核、退款============================");
        getstaus(AutoCheckTrainreport, "火车票自动生成报表============================");
        getstaus(AutoCheck12306Trainreport, "火车票12306自动生成报表============================");
        getstaus(TrainDownOrderflag, "火车票收单订单自动下单============================");
        getstaus(PayTradetoOrderflag, "火车票匹配支付宝交易============================");
        getstaus(PayLogToDatabaseflag, "火车票支付宝交易入库============================");
        getstaus(TrainExptionReportJobflag, "火车票自动导出异常报表============================");
        getstaus(StationName12306Flag, "12306站点更新============================");
        try {
            try {
                scheduler = StdSchedulerFactory.getDefaultScheduler();
                scheduler.getContext().put("jrurl", config.getInitParameter("jrurl"));
            }
            catch (SchedulerException e1) {
                e1.printStackTrace();
            }

            JobDetail jobqunarorder = new JobDetail("qunarorder", JobQunarOrder.class);
            JobDetail jobindex12306 = new JobDetail("index12306", Job12306index.class);
            JobDetail JobUnionTradenum = new JobDetail("JobUnionTradenum", JobUnionTradenum.class);
            JobDetail JobAlipaytradenum = new JobDetail("JobAlipaytradenum", JobAlipaytradenum.class);
            JobDetail jobgenerateorder = new JobDetail("generateorder", JobGenerateOrder.class);
            JobDetail jobqunargenerateorder = new JobDetail("qunargenerateorder", JobQunarGenerateOrder.class);

            JobDetail jobdeleteperson = new JobDetail("deleteperson", JobDeletePerson.class);
            JobDetail jobregistration = new JobDetail("registration", Job12306Registration.class);
            JobDetail jobautopay = new JobDetail("autopay", JobAutoPayAlipay.class);
            JobDetail jobindexagain = new JobDetail("indexagain", JobIndexAgain.class);

            JobDetail jobqunarorderrefund = new JobDetail("qunarorderrefund", JobQunarOrderRefund.class);
            JobDetail jobbacklogoforders = new JobDetail("backlogoforders", JobBacklogOfOrders.class);
            JobDetail jobautopayunion = new JobDetail("autopayunion", JobAutoPayUnion.class);
            JobDetail jobtongchengtrainordertimeout = new JobDetail("tongchengtrainordertimeout",
                    JobTongchengTrainOrderTimeout.class);
            JobDetail jobautopayalipaynew = new JobDetail("jobautopayalipaynew", JobAutoPayAlipayNew.class);
            JobDetail jobgeturlunion = new JobDetail("jobgeturlunion", Job12306GetPayUrl.class);
            JobDetail jobAutoChangeRefund = new JobDetail("jobAutoChangeRefund", ChangeReturnTicketJob.class);
            JobDetail job8000yi = new JobDetail("8000yi", Job8000yi.class);

            JobDetail taobaocc = new JobDetail("taobao", JobAlitripTraindetails.class);
            JobDetail job51book = new JobDetail("51bookup", Job51BookUpdate.class);
            JobDetail jobjielv = new JobDetail("jielvup", UpdateJielvModifyPrice.class);
            JobDetail jobRt = new JobDetail("RT", QunarRoomType.class);
            JobDetail jobhmfileupdate = new JobDetail("Hmchangepriceup", HMchangePriceUpdate.class);
            JobDetail jobcomparepice = new JobDetail("hmcompareprice", ComPareHotelGoodDataJob.class);
            JobDetail jobspecialprice = new JobDetail("specialprice", Findspecialprice.class);
            JobDetail jobinternationalSpecialprice = new JobDetail("internationalSpecialpriceJob",
                    Findinternational.class);
            JobDetail jobyl = new JobDetail("ylup", JobElongHotelBaseUpdate.class);
            JobDetail jobweekyl = new JobDetail("weekylup", NewHotelDB.class);
            JobDetail jobElLowestPriceAndStatus = new JobDetail("jobElLowestPriceAndStatus",
                    HotelLowestPriceAndStatusDB.class);
            JobDetail jobZsHotelPayType = new JobDetail("jobZsHotelPayType", UpdateHotelallPayTypeJob.class);
            JobDetail jobFindQunarHotel = new JobDetail("jobFindQunarHotel", FindQunarHotel.class);
            JobDetail jobroomstatus = new JobDetail("roomstatus", FindRoomStausJob.class);
            JobDetail jobhmfile = new JobDetail("jobhmfile", HmFileParseJob.class);
            JobDetail jobupdatehmmonth = new JobDetail("updateonemonth", UpdatePriceJob.class);
            JobDetail jobupdatehmday = new JobDetail("updateoneday", UpdatePriceOneDayJob.class);
            JobDetail jobhuaminhotelinfo = new JobDetail("jobhuaminhotelinfo", UpdateHotelInfoJob.class);
            JobDetail jobHuaMinInternationalHotelInfo = new JobDetail("jobHuaMinInternationalHotelInfo",
                    UpdateInternationalHotelInfoJob.class);
            JobDetail jobcontract = new JobDetail("jobcontract", UpdateContract.class);
            JobDetail jobhuaminprice = new JobDetail("jobhuaminprice", UpdateOneMonthPrice.class);
            JobDetail jobhuaminlastday = new JobDetail("jobhuaminlastday", UpdateLastDayPrice.class);
            JobDetail jobfilecopy = new JobDetail("filecopy", FileCopyJob.class);
            JobDetail jobfileupdateydx = new JobDetail("fileupdateydx", HuaMinFileUpdateJob.class);
            JobDetail jobImageupdate = new JobDetail("imageupdate", HotelImageUpdateJob.class);
            JobDetail jobelongtime = new JobDetail("elongimage", HotelImageDown.class);
            //#TODO 等待把这些对象去掉 chendong 时间
            JobDetail jobTaobaoCallBack3151 = new JobDetail("TaobaoCallBack3151", JobTaobaoPersonateOrderCallback.class);
            if ("1".equals(jobTaobaoCallBack3151Flag)) {
                try {
                    scheduler.scheduleJob(jobTaobaoCallBack3151, new CronTrigger("jobTaobaoCallBack3151",
                            "jobTaobaoCallBack3151Two", config.getInitParameter("jobTaobaoCallBack3151FlagTime")));
                }
                catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
            CronTrigger updatedatajobinfo;
            CronTrigger updateJlPriceinfo;
            CronTrigger updatejlhrjobinfo;
            CronTrigger updateJlpricejobinfo;
            CronTrigger updatejllastdayjobinfo;
            CronTrigger updateHotelInfoBusinfo;
            CronTrigger updateHotelPriceBusinfo;
            CronTrigger updatequnarIdinfo;
            if (updateJllastDayPriceopenclose.equals("1")) {
                try {
                    updatejllastdayjobinfo = new CronTrigger("updatejllastdayjobinfo", "updatejllastdayjobinfo",
                            updateJllastDayPricetime);
                    scheduler.scheduleJob(new JobDetail("updatejllastdayjobdesc", UpdateJllastDayPriceJob5.class),
                            updatejllastdayjobinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updatequnarIdopenclose.equals("1")) {
                try {
                    JobDetail updatequnarIddes = new JobDetail("updatequnarIddes", FindQunarHotelIdJob.class);
                    updatequnarIdinfo = new CronTrigger("updatequnarIdinfo", "updatequnarIdinfo", updatequnarIdtime);
                    scheduler.scheduleJob(updatequnarIddes, updatequnarIdinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updateHotelInfoBusopenclose.equals("1")) {
                try {
                    updateHotelInfoBusinfo = new CronTrigger("updateHotelInfoBusinfo", "updateHotelInfoBusinfo",
                            updateHotelInfoBustime);
                    scheduler.scheduleJob(new JobDetail("updateHotelInfoBusdes", UpdateHotelInfoBus.class),
                            updateHotelInfoBusinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updateHotelPriceBusopenclose.equals("1")) {
                try {
                    updateHotelPriceBusinfo = new CronTrigger("updateHotelPriceBusinfo", "updateHotelPriceBusinfo",
                            updateHotelPriceBustime);
                    scheduler.scheduleJob(new JobDetail("updateHotelPriceBusdes", UpdateHotelPriceBus.class),
                            updateHotelPriceBusinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updatedatajobopenclose.equals("1")) {
                try {
                    updatedatajobinfo = new CronTrigger("updatedatajobinfo", "updatedatajobinfo", updatedatajobopentime);
                    scheduler.scheduleJob(new JobDetail("updatedatajobDec", UpdateDataJob1.class), updatedatajobinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (updateJlPriceopenclose.equals("1")) {
                try {
                    updateJlPriceinfo = new CronTrigger("updateJlPriceinfo", "updateJlPriceinfo", updateJlPriceopentime);
                    scheduler.scheduleJob(new JobDetail("updateJlPriceDec", UpdateJlPrice2.class), updateJlPriceinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (updatejlhrJobopenclose.equals("1")) {
                try {
                    updatejlhrjobinfo = new CronTrigger("updatejlhrjobinfo", "updatejlhrjobinfo", updatejlhrJobopentime);
                    scheduler.scheduleJob(new JobDetail("updatejlhrjobDec", UpdatejlhrJob3.class), updatejlhrjobinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (updateJlpriceJobopenclose.equals("1")) {
                try {
                    updateJlpricejobinfo = new CronTrigger("updateJlpricejobinfo", "updateJlpricejobinfo",
                            updateJlpriceJobopentime);
                    scheduler.scheduleJob(new JobDetail("updateJlpricejobDec", UpdateJlpriceJob4.class),
                            updateJlpricejobinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if ("1".equals(jlUpdateBaseFlag)) {
                try {
                    CronTrigger jlUpdateBaseCronTrigger = new CronTrigger("jlUpdateBaseCronTrigger",
                            "jlUpdateBaseCronTrigger", jlUpdateBaseTime);
                    JobDetail jlUpdateBaseJobDetail = new JobDetail("jlUpdateBaseJobDetail", NewJlHotelJob.class);
                    scheduler.scheduleJob(jlUpdateBaseJobDetail, jlUpdateBaseCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(jlUpdatePriceFlag)) {
                try {
                    CronTrigger jlUpdatePriceCronTrigger = new CronTrigger("jlUpdatePriceCronTrigger",
                            "jlUpdatePriceCronTrigger", jlUpdatePriceTime);
                    JobDetail jlUpdatePriceJobDetail = new JobDetail("jlUpdatePriceJobDetail", NewJlPriceJob.class);
                    scheduler.scheduleJob(jlUpdatePriceJobDetail, jlUpdatePriceCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(TaoBaoBuyerUpdate)) {
                try {
                    CronTrigger TaoBaoBuyerCronTrigger = new CronTrigger("TaoBaoBuyerCronTrigger",
                            "TaoBaoBuyerCronTrigger", TaoBaoBuyerUpdateTime);
                    JobDetail TaoBaoBuyerJobDetail = new JobDetail("TaoBaoBuyerJobDetail", TaoBaoBuyerUpdateJob.class);
                    scheduler.scheduleJob(TaoBaoBuyerJobDetail, TaoBaoBuyerCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(TaoBaoLiuLiangUpdate)) {
                try {
                    CronTrigger TaoBaoLiuLiangCronTrigger = new CronTrigger("TaoBaoLiuLiangCronTrigger",
                            "TaoBaoLiuLiangCronTrigger", TaoBaoLiuLiangUpdateTime);
                    JobDetail TaoBaoLiuLiangJobDetail = new JobDetail("TaoBaoLiuLiangJobDetail",
                            TaoBaoLiuLiangJob.class);
                    scheduler.scheduleJob(TaoBaoLiuLiangJobDetail, TaoBaoLiuLiangCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(jlComparePriceUpdate)) {
                try {
                    CronTrigger jlComparePriceCronTrigger = new CronTrigger("jlComparePriceCronTrigger",
                            "jlComparePriceCronTrigger", jlComparePriceUpdateTime);
                    JobDetail jlComparePriceJobDetail = new JobDetail("jlComparePriceJobDetail",
                            SuppliersComparePriceWithQunarJob.class);
                    scheduler.scheduleJob(jlComparePriceJobDetail, jlComparePriceCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(jlUpdatePriceForQunarUpdate)) {
                try {
                    CronTrigger jlUpdatePriceForQunarCronTrigger = new CronTrigger("jlUpdatePriceForQunarCronTrigger",
                            "jlUpdatePriceForQunarCronTrigger", jlUpdatePriceForQunarUpdateTime);
                    JobDetail jlUpdatePriceForQunarJobDetail = new JobDetail("jlUpdatePriceForQunarJobDetail",
                            SuppliersUpdatePriceForQunarJob.class);
                    scheduler.scheduleJob(jlUpdatePriceForQunarJobDetail, jlUpdatePriceForQunarCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(loadAllTravelSkyHotel)) {
                try {
                    CronTrigger loadAllTravelSkyHotelCronTrigger = new CronTrigger("loadAllTravelSkyHotelCronTrigger",
                            "loadAllTravelSkyHotelCronTrigger", loadAllTravelSkyHotelTime);
                    JobDetail loadAllTravelSkyHotelJobDetail = new JobDetail("loadAllTravelSkyHotelJobDetail",
                            TravelSkyDataJob.class);
                    scheduler.scheduleJob(loadAllTravelSkyHotelJobDetail, loadAllTravelSkyHotelCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(qunarCloseRoom)) {
                try {
                    CronTrigger qunarCloseRoomCronTrigger = new CronTrigger("qunarCloseRoomCronTrigger",
                            "qunarCloseRoomCronTrigger", qunarCloseRoomTime);
                    JobDetail qunarCloseRoomJobDetail = new JobDetail("qunarCloseRoomJobDetail",
                            SuppliersCloseRoomJob.class);
                    scheduler.scheduleJob(qunarCloseRoomJobDetail, qunarCloseRoomCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(OpenCatchQunarHotelPrice)) {
                try {
                    CronTrigger CatchQunarHotelPriceCronTrigger = new CronTrigger("CatchQunarHotelPriceCronTrigger",
                            "CatchQunarHotelPriceCronTrigger", OpenCatchQunarHotelPriceTime);
                    JobDetail CatchQunarHotelPriceJobDetail = new JobDetail("CatchQunarHotelPriceJobDetail",
                            CatchQunarHotelPriceJob.class);
                    scheduler.scheduleJob(CatchQunarHotelPriceJobDetail, CatchQunarHotelPriceCronTrigger);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(SyncQunarHotelStatus)) {
                try {
                    CronTrigger SyncQunarHotelStatusCronTrigger = new CronTrigger("SyncQunarHotelStatusCronTrigger",
                            "SyncQunarHotelStatusCronTrigger", SyncQunarHotelStatusTime);
                    JobDetail SyncQunarHotelStatusJobDetail = new JobDetail("SyncQunarHotelStatusJobDetail",
                            "SyncQunarHotelStatusJobDetail", com.ccservice.qunar.update.SyncQunarHotelStatus.class);
                    scheduler.scheduleJob(SyncQunarHotelStatusJobDetail, SyncQunarHotelStatusCronTrigger);
                }
                catch (Exception e) {
                }
            }

            CronTrigger conhuaminhotelinfo;
            CronTrigger conHuaMinInternationalHotelInfo;
            CronTrigger concontract;
            CronTrigger conhuaminprice;
            CronTrigger conhuaminlastday;
            CronTrigger confilecopy;
            CronTrigger conjobfileupdateydx;
            CronTrigger elongorderupdate;

            CronTrigger conimageupdate;// 图片抓取
            CronTrigger conhmfileupdate;// 华闽变价问价更新
            CronTrigger comparepriceupdate;// 华闽变价问价更新
            CronTrigger roomstatusupdate;// 房态更新程序
            CronTrigger conhmfile;// 华敏更新方式更新
            CronTrigger conupdateonemonth;
            CronTrigger conupdateoneday;
            CronTrigger conelongimage;
            CronTrigger crontaobao;//淘宝

            CronTrigger cron8000yi;// 8000翼同步
            CronTrigger cronqunarorder;//去哪儿火车票同步
            CronTrigger cronverification;//12306账号保持在线
            CronTrigger cronindex12306;//自动登录12306账号
            CronTrigger cronJobUnionTradenum;//银联交易
            CronTrigger cronJobAlipaytradenum;//支付宝自动获取交易号
            CronTrigger cronJobgeturlTradenum;//自动抓取银联链接
            CronTrigger cronJobgetpayTradenum;//自动抓取银联链接
            CronTrigger crongenerateorder;//12306账号全自动下单
            CronTrigger cronqunargenerateorder;//12306账号全自动下单
            CronTrigger cronquerymyorder;//审核12306订单支付状态
            CronTrigger crondeleteperson;//删除12306账号中不是已通过的乘客.
            CronTrigger cronregistration;//自动注册12306账号
            CronTrigger cronautopay;//自动支付火车票
            CronTrigger cronindexagain;//自动支付火车票
            CronTrigger cronmonitoringofpayments;//自动支付警报
            CronTrigger cronqunarorderrefund;//自动同步qunar退票
            CronTrigger cronbacklogoforders;//自动获取未支付订单
            CronTrigger cronautopayunion;//银联自动支付
            CronTrigger cronautopayalipay;//支付宝自动支付
            CronTrigger crontongchengtrainordertimeout;//同程超时订单转换
            CronTrigger cronAutoChangeRefund;//自动改签退
            CronTrigger cron51book;// 51book同步
            CronTrigger cronjielv;// 捷旅同步

            CronTrigger cronyl;// 艺龙同步

            CronTrigger cronweekyl;// 艺龙周同步

            CronTrigger cronElLowestPriceAndStatusTime;// 艺龙酒店最低价格、状态更新

            CronTrigger cronZsHotelPayType;// 正式酒店支付方式更新

            CronTrigger cronFindQunarHotel;// 抓取所有去哪儿酒店

            CronTrigger cronRT;

            CronTrigger cronYXbook;// 易行天下同步
            CronTrigger croninternationalSpecialprice;// 国际特价机票
            CronTrigger cronSpecialprice;// 国内特价机票

            if (elongorderupdateopenclose.equals("1")) {
                try {
                    System.out.println("艺龙订单状态更新初始化……");
                    elongorderupdate = new CronTrigger("elongorderupdate", "elongorderupdate", "0 0/"
                            + elongorderupdatetime + " * * * ?");
                    scheduler.scheduleJob(new JobDetail("elongorderupdate", HotelOrderUpdate.class), elongorderupdate);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (elongimageopenclose.equals("1")) {
                try {
                    System.out.println("艺龙图片抓取初始化……");
                    conelongimage = new CronTrigger("elongimageupdatet", "elongimageupdatettt", elongimagetime);
                    scheduler.scheduleJob(jobelongtime, conelongimage);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (Imageopenclose.equals("1")) {
                try {
                    System.out.println("图片抓取初始化……");
                    conimageupdate = new CronTrigger("imageupdatet", "imageupdatettt", ImageopencloseTime);
                    scheduler.scheduleJob(jobImageupdate, conimageupdate);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (fileupdateydxopenclose.equals("1")) {
                try {
                    System.out.println("本地文件更新房态，房价初始化……");
                    conjobfileupdateydx = new CronTrigger("conjobfileupdateydx", "conjobfileupdateydx", "0 0/"
                            + fileupdateydxtime + " * * * ?");
                    scheduler.scheduleJob(jobfileupdateydx, conjobfileupdateydx);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (filecopyopenclose.equals("1")) {
                try {
                    System.out.println("文件复制工作初始化");
                    confilecopy = new CronTrigger("filecopyjob", "filecopyjob", "0 0/" + filecopytime + " * * * ?");
                    scheduler.scheduleJob(jobfilecopy, confilecopy);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (huaminhotelinfoopenclose.equals("1")) {
                try {
                    System.out.println("更新华闽酒店基本信息");
                    conhuaminhotelinfo = new CronTrigger("huaminhotelinfo", "huaminhotelinfo", huaminhotelinfotime);
                    scheduler.scheduleJob(jobhuaminhotelinfo, conhuaminhotelinfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (huaminInternationalHotelInfoOpenClose.equals("1")) {
                try {
                    System.out.println("更新华闽国际酒店信息");
                    conHuaMinInternationalHotelInfo = new CronTrigger("huaminInternationalHotelinfo",
                            "huaminInternationalHotelinfo", huaminInternationalHotelInfoTime);
                    scheduler.scheduleJob(jobHuaMinInternationalHotelInfo, conHuaMinInternationalHotelInfo);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (huamincontractopenclose.equals("1")) {
                try {
                    System.out.println("更新合同附属信息");
                    concontract = new CronTrigger("huamincontractshijian", "huamincontractshijian",
                            huamincontractshijian);
                    scheduler.scheduleJob(jobcontract, concontract);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (huaminpriceopenclose.equals("1")) {
                try {
                    System.out.println("更新华闽一个月初始化");
                    conhuaminprice = new CronTrigger("conhuaminprice", "conhuaminprice", huaminpricetime);
                    scheduler.scheduleJob(jobhuaminprice, conhuaminprice);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (huaminlastdayopenclose.equals("1")) {
                try {
                    System.out.println("更新最后一天价格初始化");
                    conhuaminlastday = new CronTrigger("conhuaminlast", "conhuaminlast", huaminlastdaytime);
                    scheduler.scheduleJob(jobhuaminlastday, conhuaminlastday);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updateonemonth.equals("1")) {
                try {
                    System.out.println("更新一个月初始化");
                    conupdateonemonth = new CronTrigger("updateonemonth", "updateonemonth", updateonemonthtime);
                    scheduler.scheduleJob(jobupdatehmmonth, conupdateonemonth);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (updateonemonthlastday.equals("1")) {
                try {
                    conupdateoneday = new CronTrigger("updateoneday", "updateoneday", updateonemonthtimelastday);
                    scheduler.scheduleJob(jobupdatehmday, conupdateoneday);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (hmfileupdate.equals("1")) {
                try {
                    conhmfile = new CronTrigger("hmfileupdate", "jobhmfile", "0 0/" + hmfiletime + " * * * ?");
                    scheduler.scheduleJob(jobhmfile, conhmfile);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // 房态更新
            if (findroomstatus.equals("1")) {
                try {
                    roomstatusupdate = new CronTrigger("roomstatusupdate", "roomstatus", "0 0/" + findroomstatustime
                            + " * * * ?");
                    System.out.println("房态初始化更新");
                    roomstatusupdate = new CronTrigger("roomstatusupdate", "roomstatus", "0 0/" + findroomstatustime
                            + " * * * ?");
                    scheduler.scheduleJob(jobroomstatus, roomstatusupdate);
                }
                catch (Exception e) {
                    System.out.println("房态出现异常");
                    e.printStackTrace();
                }
            }

            if (jielvstar.equals("1")) {
                // 捷旅 同步政策
                try {
                    cronjielv = new CronTrigger("Jielvupdate", "Jielvup", jielvTime);
                    scheduler.scheduleJob(jobjielv, cronjielv);
                }
                catch (Exception e) {
                    System.out.println("捷旅同步出现异常");
                    e.printStackTrace();
                }
            }

            if (ylstar.equals("1")) {
                // 艺龙 同步政策
                try {
                    cronyl = new CronTrigger("ylupdate", "ylup", ylTime);
                    scheduler.scheduleJob(jobyl, cronyl);
                }
                catch (Exception e) {
                    System.out.println("艺龙同步出现异常");
                    e.printStackTrace();
                }
            }

            if (weekylstar.equals("1")) {
                // 艺龙 同步政策
                try {
                    cronweekyl = new CronTrigger("ylweekupdate", "ylweekup", ylWeekTime);
                    scheduler.scheduleJob(jobweekyl, cronweekyl);
                }
                catch (Exception e) {
                    System.out.println("艺龙同步出现异常");
                    e.printStackTrace();
                }
            }

            if ("1".equals(elLowestPriceAndStatusUpdate)) {
                // 艺龙酒店最低价格、状态更新
                try {
                    cronElLowestPriceAndStatusTime = new CronTrigger("ellowestpriceandstatusupdate",
                            "ellowestpriceandstatusup", elLowestPriceAndStatusTime);
                    scheduler.scheduleJob(jobElLowestPriceAndStatus, cronElLowestPriceAndStatusTime);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(zsHotelPayTypeUpdate)) {
                // 正式酒店支付方式更新
                try {
                    cronZsHotelPayType = new CronTrigger("zshotelpaytypeupdate", "zshotelpaytypeup", zsHotelPayTypeTime);
                    scheduler.scheduleJob(jobZsHotelPayType, cronZsHotelPayType);
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(FindQunarHotelUpdate)) {
                // 抓取所有去哪儿酒店
                try {
                    cronFindQunarHotel = new CronTrigger("findqunarhotelupdate", "findqunarhotelup",
                            FindQunarHotelUpdateTime);
                    scheduler.scheduleJob(jobFindQunarHotel, cronFindQunarHotel);
                }
                catch (Exception e) {
                }
            }

            if (hmchangepriceupdate.equals("1")) {
                // 华闽变价文件更新
                try {
                    conhmfileupdate = new CronTrigger("Hmchangepriceupdate", "Hmchangepriceup", "0 0/"
                            + hmupdatefiletime + " * * * ?");
                    scheduler.scheduleJob(jobhmfileupdate, conhmfileupdate);
                }
                catch (Exception e) {
                    System.out.println("华闽同步出现异常");
                    e.printStackTrace();
                }
            }
            if (compareprice.equals("1")) {
                // 华闽比价更新
                try {//
                    System.out.println("华闽比价job初始化……");
                    comparepriceupdate = new CronTrigger("hmcompareprice", "Hmcompricepriceup", comparetimetime);
                    scheduler.scheduleJob(jobcomparepice, comparepriceupdate);
                }
                catch (Exception e) {
                    System.out.println("华闽变价文件异常");
                    e.printStackTrace();
                }
            }

            if (internationalstart.equals("1")) {
                System.out.println(new Date());
                croninternationalSpecialprice = new CronTrigger("internationalSpecialpricetriger",
                        "internationalSpecialpriceJob", timeinternationalstart);
                try {
                    scheduler.scheduleJob(jobinternationalSpecialprice, croninternationalSpecialprice);
                }
                catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }

            if (jielvstar.equals("1")) {
                // 捷旅 同步政策
                try {
                    cronjielv = new CronTrigger("Jielvupdate", "Jielvup", jielvTime);
                    scheduler.scheduleJob(jobjielv, cronjielv);
                }
                catch (Exception e) {
                    System.out.println("捷旅同步出现异常");
                    e.printStackTrace();
                }
            }

            if (rtstar.equals("1")) {
                // 房型 同步政策
                try {
                    cronRT = new CronTrigger("RTX", "RT", rtTime);
                    scheduler.scheduleJob(jobRt, cronRT);
                }
                catch (Exception e) {
                    System.out.println("捷旅同步出现异常");
                    e.printStackTrace();
                }
            }
            if (hmchangepriceupdate.equals("1")) {
                // 华闽变价文件更新
                try {
                    conhmfileupdate = new CronTrigger("Hmchangepriceupdate", "Hmchangepriceup", "0 0/"
                            + hmupdatefiletime + " * * * ?");
                    scheduler.scheduleJob(jobhmfileupdate, conhmfileupdate);
                }
                catch (Exception e) {
                    System.out.println("华闽同步出现异常");
                    e.printStackTrace();
                }
            }

            if (specialpricestart.equals("1")) {
                try {
                    cronSpecialprice = new CronTrigger("specialprice", "specialprice", timespecialprice);
                    scheduler.scheduleJob(jobspecialprice, cronSpecialprice);
                }
                catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }

            if (qunarOrderstar.equals("1")) {
                try {
                    cronqunarorder = new CronTrigger("qunarordertriger", "qunarorder", timeofqunarorder + " * * ?");
                    scheduler.scheduleJob(jobqunarorder, cronqunarorder);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }

            if (verificationstar.equals("1")) {
                try {
                    cronverification = new CronTrigger("verificationtriger", "verification", timeofverification
                            + " * * ?");
                    JobDetail jobverification = new JobDetail("verification", Job12306Verification.class);
                    cronverification = new CronTrigger("verificationtriger", "verification", timeofverification
                            + " * * ?");
                    scheduler.scheduleJob(jobverification, cronverification);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }
            if (index12306star.equals("1")) {
                try {
                    cronindex12306 = new CronTrigger("index12306triger", "index12306", timeofindex12306 + " * * ?");
                    scheduler.scheduleJob(jobindex12306, cronindex12306);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }

            /**
             * 自动更新交易号
             */
            if ("1".equals(JobUnionTradenumstat)) {
                try {
                    cronJobUnionTradenum = new CronTrigger("JobUnionTradenums", "JobUnionTradenum",
                            JobUnionTradenumtime + " * * ?");
                    scheduler.scheduleJob(JobUnionTradenum, cronJobUnionTradenum);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }
            /**
             * 支付宝自动更新交易号
             */
            if ("1".equals(JobAlipaytradenumstat)) {
                try {
                    cronJobAlipaytradenum = new CronTrigger("cronJobAlipaytradenum", "cronJobAlipaytrade",
                            JobAlipaytradenumtime + " * * ?");
                    scheduler.scheduleJob(JobAlipaytradenum, cronJobAlipaytradenum);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }
            /**
             * 获取支付链接
             */
            if ("1".equals(Job12306GetPayUrltat)) {
                try {
                    cronJobgeturlTradenum = new CronTrigger("cronJobgeturlTradenum", "cronJobgeturlTradenum",
                            JobgeturlTradenumtime + " * * ?");
                    scheduler.scheduleJob(jobgeturlunion, cronJobgeturlTradenum);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }

            if ("1".equals(generateorderstar)) {
                try {
                    crongenerateorder = new CronTrigger("generateordertriger", "generateorder", timeofgenerateorder
                            + " * * ?");
                    scheduler.scheduleJob(jobgenerateorder, crongenerateorder);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }
            if ("1".equals(qunargenerateorderstar)) {
                try {
                    cronqunargenerateorder = new CronTrigger("qunargenerateordertriger", "qunargenerateorder",
                            timeofqunargenerateorder + " * * ?");
                    scheduler.scheduleJob(jobqunargenerateorder, cronqunargenerateorder);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }

            if ("1".equals(deletepersonstar)) {
                try {
                    crondeleteperson = new CronTrigger("deletepersontriger", "deleteperson", timeofdeleteperson
                            + " * * ?");
                    scheduler.scheduleJob(jobdeleteperson, crondeleteperson);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }

            if ("1".equals(registrationstar)) {
                try {
                    cronregistration = new CronTrigger("registrationtriger", "registration", timeofregistration);
                    scheduler.scheduleJob(jobregistration, cronregistration);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }

            }
            if ("1".equals(autopaystar)) {
                try {
                    cronautopay = new CronTrigger("autopaytriger", "autopay", timeofautopay + " * * ?");
                    scheduler.scheduleJob(jobautopay, cronautopay);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            if ("1".equals(indexagainstar)) {
                try {
                    cronindexagain = new CronTrigger("indexagaintriger", "indexagain", timeofindexagain + " * * ?");
                    scheduler.scheduleJob(jobindexagain, cronindexagain);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            String monitoringofpaymentsstar = "0";
            //            monitoringofpaymentsstar = config.getInitParameter("monitoringofpaymentsstar");//自动支付警报是否开启
            //定时检测这个12306账号是否可用
            monitoringofpaymentsstar = PropertyUtil.getValue("JobMonitoringOfPayments_isstart", "train.properties");
            getstaus(monitoringofpaymentsstar, "发送邮件预警================================");
            if ("1".equals(monitoringofpaymentsstar)) {
                JobDetail jobmonitoringofpayments = new JobDetail("monitoringofpayments", JobMonitoringOfPayments.class);
                try {
                    String JobMonitoringOfPayments_cronExpression = PropertyUtil.getValue(
                            "JobMonitoringOfPayments_cronExpression", "train.properties");
                    cronmonitoringofpayments = new CronTrigger("monitoringofpaymentstriger", "monitoringofpayments",
                            JobMonitoringOfPayments_cronExpression);
                    scheduler.scheduleJob(jobmonitoringofpayments, cronmonitoringofpayments);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if ("1".equals(qunarorderrefundstar)) {
                try {
                    cronqunarorderrefund = new CronTrigger("qunarorderrefundtriger", "qunarorderrefund",
                            timeofqunarorderrefund + " * * ?");
                    scheduler.scheduleJob(jobqunarorderrefund, cronqunarorderrefund);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            if ("1".equals(backlogofordersstar)) {
                try {
                    cronbacklogoforders = new CronTrigger("backlogoforderstriger", "backlogoforders",
                            timeofbacklogoforders + " * * ?");
                    scheduler.scheduleJob(jobbacklogoforders, cronbacklogoforders);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            /**
             * 银联自动支付
             */
            if ("1".equals(autopayalipaynew)) {
                try {
                    cronautopayalipay = new CronTrigger("autopayalipaynewtriger", "autopayalipaynew",
                            timeofautopayalipaynew + " * * ?");
                    scheduler.scheduleJob(jobautopayalipaynew, cronautopayalipay);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            /**
             * 银联自动支付
             */
            if ("1".equals(autopayunionstar)) {
                try {
                    cronautopayunion = new CronTrigger("autopayuniontriger", "autopayunion", timeofautopayunion
                            + " * * ?");
                    scheduler.scheduleJob(jobautopayunion, cronautopayunion);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }
            /**
             * 同程超时订单转换
             */
            if ("1".equals(tongchengtrainordertimeoutstar)) {
                try {
                    crontongchengtrainordertimeout = new CronTrigger("tongchengtrainordertimeouttriger",
                            "tongchengtrainordertimeout", timeoftongchengtrainordertimeout + " * * ?");
                    scheduler.scheduleJob(jobtongchengtrainordertimeout, crontongchengtrainordertimeout);
                }
                catch (Exception e) {

                    e.printStackTrace();
                }
            }

            //自动改签退
            if ("1".equals(AutoChangeRefundFlag)) {
                try {
                    cronAutoChangeRefund = new CronTrigger("cronAutoChangeRefund", "cronAutoChangeRefund",
                            AutoChangeRefundTime);
                    scheduler.scheduleJob(jobAutoChangeRefund, cronAutoChangeRefund);
                }
                catch (Exception e) {
                }
            }
            //自动导出报表
            if ("1".equals(AutoCheckTrainreport)) {
                try {
                    scheduler.scheduleJob(new JobDetail("jobAutoCheckTrainreport", TrainReportUpdate.class),
                            new CronTrigger("cronAutoCheckTrainreport", "cronAutoCheckTrainreport",
                                    AutoCheckTrainreportTime));
                }
                catch (Exception e) {
                }
            }
            //自动导出12306报表
            if ("1".equals(AutoCheck12306Trainreport)) {
                try {
                    scheduler.scheduleJob(new JobDetail("jobAutoCheck12306Trainreport", Job12306PayRecord.class),
                            new CronTrigger("cronAutoCheck12306Trainreport", "cronAutoCheck12306Trainreport",
                                    AutoCheck12306TrainreportTime));
                }
                catch (Exception e) {
                }
            }

            //自动下单
            if ("1".equals(TrainDownOrderflag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("jobTrainDownOrderflag", TrainDownOrderJob.class),
                            new CronTrigger("cronTrainDownOrderflag", "cronTrainDownOrderflag", TrainDownOrderTime));
                }
                catch (Exception e) {
                }
            }

            //自动匹配支付宝交易
            if ("1".equals(PayTradetoOrderflag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("PayTradetoOrderflagJob", PayTradetoOrder.class),
                            new CronTrigger("PayTradetoOrderflagJobflag", "PayTradetoOrderflagJobflag",
                                    PayTradetoOrderTime));
                }
                catch (Exception e) {
                }
            }
            //自动匹配支付宝交易
            if ("1".equals(PayLogToDatabaseflag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("PayLogToDatabaseflagJob", PayLogToDatabase.class),
                            new CronTrigger("PayLogToDatabaseflagJobflag", "PayLogToDatabaseflagJobflag",
                                    PayLogToDatabaseTime));
                }
                catch (Exception e) {
                }
            }

            //12306站点更新
            if ("1".equals(StationName12306Flag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("StationName12306FlagJob", StationUpdateJob.class),
                            new CronTrigger("StationName12306FlagJobflag", "StationName12306Flagflag",
                                    StationName12306UpdateTime));
                }
                catch (Exception e) {
                }
            }

            //异常报表
            if ("1".equals(TrainExptionReportJobflag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("TrainExptionReportJob", TrainExpReportUpdate.class),
                            new CronTrigger("cronTrainExptionReportJobflag", "cronTrainExptionReportJobflag",
                                    TrainExptionReportJobtime));
                }
                catch (Exception e) {
                }
            }

            if ("1".equals(AutoCheckRefundFlag)) {
                try {
                    scheduler.scheduleJob(new JobDetail("jobAutoCheckRefund", CheckReturnTicketJob.class),
                            new CronTrigger("cronAutoCheckRefund", "cronAutoCheckRefund", AutoCheckRefundTime));
                }
                catch (Exception e) {
                }
            }

            if (baqianyiisstar.equals("1")) {
                try {
                    cron8000yi = new CronTrigger("8000yitriger", "8000yi", timeof8000yi + " * * * ?");
                    scheduler.scheduleJob(job8000yi, cron8000yi);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (wuyibooktongbu.equals("1")) {
                // 51book 同步政策
                try {
                    cron51book = new CronTrigger("51bookupdate", "51bookup", time51book + " * * * ?");
                    scheduler.scheduleJob(job51book, cron51book);
                }
                catch (Exception e) {
                    System.out.println("51book同步出现异常");
                    e.printStackTrace();
                }
            }

            if ("1".equals(TaoBaoChange)) {
                try {
                    crontaobao = new CronTrigger("taobaoupdate", "taobaocc", TaoBaoChangetime);
                    scheduler.scheduleJob(taobaocc, crontaobao);

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 8000yi政策，每半小时执行一次 特殊政策
            // CronTrigger cron8000yi_s = new
            // CronTrigger("8000yitriger_s","8000yi_s","0 0/30 * * * ?");
            // scheduler.scheduleJob(job8000yi_s, cron8000yi_s);
            //
            // // 8000yi政策，每半小时执行一次 实时更新的的政策
            // CronTrigger cron8000yi_n = new
            // CronTrigger("8000yitriger_n","8000yi_n","0 0/30 * * * ?");
            // scheduler.scheduleJob(job8000yi_n, cron8000yi_n);

            if (jinritongbu.equals("1")) {
                try {
                    JobDetail jrUpdatedetail = new JobDetail("jrUpdatejob", JobJRUpdateRatePackage.class);
                    scheduler.scheduleJob(
                            jrUpdatedetail,
                            new CronTrigger("jrjobtriger", "jrjobg", "0 0/"
                                    + config.getInitParameter("jinriupdatetime") + " * * * ?"));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }

            if (jinriisstar.equals("1")) {
                try {
                    JobDetail jrdetail = new JobDetail("jrjob", JobJRRatePackage.class);
                    scheduler.scheduleJob(
                            jrdetail,
                            new CronTrigger("jrupdatejobtriger", "jrupdatejobg", config
                                    .getInitParameter("jinriquanquTime")));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }

                // 今日政策,每天2点执行一次--高反政策
                // try {
                // JobDetail jrdetailgao = new JobDetail("jrjobgao",
                // JobJRRatePackage_GaoFan.class);
                // scheduler.scheduleJob(jrdetailgao, new CronTrigger(
                // "jrjobtrigergao", "jrjobgao", config
                // .getInitParameter("jrcrongao")));
                // } catch (SchedulerException e1) {
                // // TODO Auto-generated catch block
                // e1.printStackTrace();
                // }
            }

            // 同步携程旅游数据
            if (synchroCtripDomesticTourism.equals("1")) {
                try {
                    JobDetail jrdetail = new JobDetail("ctripjob", JobCtripDemosticTourism.class);
                    scheduler.scheduleJob(
                            jrdetail,
                            new CronTrigger("ctripjobtriger", "ctripjobg", config
                                    .getInitParameter("synchroCtripDomesticTourismTime")));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }
            // 同步携程签证数据
            if (synchroCtripVisa.equals("1")) {
                try {
                    JobDetail jrdetail = new JobDetail("ctripvisajob", JobCtripVisa.class);
                    scheduler.scheduleJob(
                            jrdetail,
                            new CronTrigger("ctripvisajobtriger", "ctripvisajobg", config
                                    .getInitParameter("synchroCtripVisaTime")));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }
            CronTrigger cronHotel;// hotel
            if (hotelstar.equals("1")) {

                try {
                    System.out.println("艺龙酒店初始化更新……");
                    JobDetail hoteljob = new JobDetail("hoteldb", UpdateHotelDB.class);
                    cronHotel = new CronTrigger("hoteldate", "hoteldb", config.getInitParameter("hotelupdateTime"));
                    scheduler.scheduleJob(hoteljob, cronHotel);
                }
                catch (Exception e) {
                    System.out.println("酒店数据更新 出现错误了.....");
                    e.printStackTrace();
                }

            }
            CronTrigger cronGeo;// geo
            if (hotelgeo.equals("1")) {
                try {
                    System.out.println("艺龙地理位置更新初始化……");
                    JobDetail geojob = new JobDetail("geodb", NewGeoCNDB.class);
                    cronGeo = new CronTrigger("geodate", "geodb", config.getInitParameter("geoupdateTime"));
                    scheduler.scheduleJob(geojob, cronGeo);
                }
                catch (Exception e) {
                    System.out.println("geo更新 出现错误了.....");
                    e.printStackTrace();
                }
            }
            CronTrigger cronBrand;// geo
            if (hotelbrand.equals("1")) {

                try {
                    JobDetail brandjob = new JobDetail("branddb", UpdateBrandDB.class);
                    cronBrand = new CronTrigger("branddate", "branddb", config.getInitParameter("brandupdateTime"));
                    scheduler.scheduleJob(brandjob, cronBrand);
                }
                catch (Exception e) {
                    System.out.println("geo更新 出现错误了.....");
                    e.printStackTrace();
                }

            }
            if (wuyibookisstar.equals("1")) {
                // 51book 政策
                try {
                    JobDetail bookzrate = new JobDetail("51book", Job51Book.class);
                    scheduler.scheduleJob(bookzrate,
                            new CronTrigger("51bookzrate", "51book", config.getInitParameter("51book")));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }

            String isstart12306edituserinfo = PropertyUtil.getValue("isstart12306edituserinfo", "train.properties");//定时任务是否开启修改12306账号信息
            getstaus(isstart12306edituserinfo, "修改火车票账号注册信息============================");
            if ("1".equals(isstart12306edituserinfo)) {
                String cronExpression_edit12306userinfo = PropertyUtil.getValue("edit12306userinfo_cronExpression",
                        "train.properties");
                // 定时任务是否开启修改12306账号信息
                try {
                    JobDetail edit12306userinfo = new JobDetail("edit12306userinfo", Job12306Edituserinfo.class);
                    scheduler.scheduleJob(edit12306userinfo, new CronTrigger("edit12306userinfo_job",
                            "edit12306userinfo", cronExpression_edit12306userinfo + " * * ?"));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }
            String isstartJob12306Registration_addMailUser = PropertyUtil.getValue(
                    "isstartJob12306Registration_addMailUser", "train.properties");//定时任务是否开启修改12306账号信息
            getstaus(isstartJob12306Registration_addMailUser, "在邮件系统里加入账号并把这个账号添加到customerusr==========");
            if ("1".equals(isstartJob12306Registration_addMailUser)) {
                String cronExpression_Job12306Registration_addMailUser = PropertyUtil.getValue(
                        "Job12306Registration_addMailUser_cronExpression", "train.properties");
                // 定时任务是否开启修改12306账号信息
                try {
                    JobDetail Job12306Registration_addMailUser_JobDetail = new JobDetail("edit12306userinfo",
                            Job12306Registration_addMailUser.class);
                    scheduler.scheduleJob(Job12306Registration_addMailUser_JobDetail, new CronTrigger(
                            "Job12306Registration_addMailUser_job", "Job12306Registration_addMailUser",
                            cronExpression_Job12306Registration_addMailUser));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }

            //定时检测这个12306账号是否可用
            String isstart12306CheckIsenable = PropertyUtil.getValue("isstart12306CheckIsenable", "train.properties");
            getstaus(isstart12306CheckIsenable, "定时检测所有符合条件的12306账号是否可用==========");
            if ("1".equals(isstart12306CheckIsenable)) {
                String checkIsenable_cronExpression = PropertyUtil.getValue("checkIsenable_cronExpression",
                        "train.properties");
                try {
                    JobDetail Job12306CheckIsenable_JobDetail = new JobDetail("checkIsenable",
                            Job12306CheckIsenable.class);
                    scheduler.scheduleJob(Job12306CheckIsenable_JobDetail, new CronTrigger("Job12306CheckIsenable_job",
                            "Job12306CheckIsenable", checkIsenable_cronExpression));
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            scheduler.start();
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
        Job12306AccountCheckMobileNewMethod();
        TcReport();//同程对账定时job
    }

    /**
     * 
     * @time 2015年12月2日 上午11:45:53
     * @author chendong
     * @param config 
     */
    private void initAir(ServletConfig config) {
        String timeofYi9E = config.getInitParameter("timeofYi9E");// 19E同步时间
        String yi9Eisstar = "0";
        yi9Eisstar = config.getInitParameter("yi9Eisstar");// 19E是否开启更新
        getstaus(yi9Eisstar, "19E更新状态==");
        if ("1".equals(yi9Eisstar)) {
            CronTrigger cronYi9E;// 19E
            try {
                cronYi9E = new CronTrigger("Yi9Etriger", "Yi9E", timeofYi9E);
                scheduler.scheduleJob(new JobDetail("19E", Yi9eUpdate.class), cronYi9E);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 
     * @time 2015年9月22日 下午2:23:03
     * @author chendong
     */
    private void Job12306AccountCheckMobileNewMethod() {
        try {
            String checi_isstart = PropertyUtil.getValue("isstart", "train.checkMobile.properties");
            getstaus(checi_isstart, "************单独核验手机号JOB 开!************");
            if ("1".equals(checi_isstart)) {
                Job12306AccountCheckMobileNew.startScheduler(PropertyUtil.getValue("cronExpression",
                        "train.checkMobile.properties"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 同程
     * @time 2015年10月26日 下午7:37:21
     * @author zcn
     */
    private void TcReport() {
        try {
            String TCreport_isstart = PropertyUtil.getValue("TCreport_isstart", "Tcreport.properties");
            if ("1".equals(TCreport_isstart)) {
                JobTongChengReport.startScheduler(PropertyUtil.getValue("TCreport_expr", "Tcreport.properties"));
                System.out.println("************同程对账发布定时开!************");
            }
            else {
                System.out.println("************同程对账发布定时关！************");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        System.out.println("Start rate stop...");
        try {
            scheduler.shutdown();
        }
        catch (SchedulerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getstaus(String staus, String infoString) {
        if ("0".equals(staus)) {
            return "禁用";
        }
        else if ("1".equals(staus)) {
            System.out.println(infoString + ":启用");
            return "启用";
        }
        else {
            return "空";
        }
    }

    public String getstaus1(String staus) {
        if ("0".equals(staus)) {
            return "禁用";
        }
        else if ("1".equals(staus)) {
            return "启用";
        }
        else {
            return "空";
        }
    }

    //  <!-- 审核订单支付状态 1开启 0关闭 -->
    //  <init-param>
    //      <param-name>querymyorderstar</param-name>
    //      <param-value>0</param-value>
    //  </init-param>
    //  <init-param>
    //      <param-name>querymyorderTime</param-name>
    //      <param-value>0 0/1 7-23</param-value>
    //  </init-param>
    //  <!-- 审核订单支付状态结束 1开启 0关闭 -->
    //            if ("1".equals(querymyorderstar)) {
    //                try {
    //                    JobDetail jobquerymyorder = new JobDetail("querymyorder", JobQueryMyOrder.class);
    //                    cronquerymyorder = new CronTrigger("querymyordertriger", "querymyorder", timeofquerymyorder
    //                            + " * * ?");
    //                    scheduler.scheduleJob(jobquerymyorder, cronquerymyorder);
    //                }
    //                catch (Exception e) {
    //
    //                    e.printStackTrace();
    //                }
    //
    //            }

}