package com.ccservice.qunar.train;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import com.callback.SendPostandGet;

public class JobQunarOrderOfflineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public JobQunarOrderOfflineServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String agengId=request.getParameter("agengId");
		String address=request.getParameter("address");
		logistics(agengId, address);
		
		}
	public void logistics (String agengId,String address){
		String urlString="http://121.40.241.126:9038/SfExpressHthy/DeliverTmServiceServlet";
	       // String realTime="2016-04-14 10:00:00";
		     String realTime = "";
	        String fromcode="010";
	        String tocode="755";
	        String param="times="+realTime+"&fromcode="+fromcode+"&tocode="+tocode;
//	         WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->"+urlString+"?"+param);
	        String result=SendPostandGet.submitPost(urlString, param,"UTF-8").toString();
	        System.out.println("顺丰返回："+result);
	        String results="";
	        try {
				Document document = DocumentHelper.parseText(result);
	            Element root = document.getRootElement();
	            Element head = root.element("Head");
	            Element body = root.element("Body");
	            if("OK".equals(root.elementText("Head"))){
	            	Element deliverTmResponse = body.element("DeliverTmResponse");
	            	Element deliverTm = deliverTmResponse.element("DeliverTm");
	            	String business_type_desc=deliverTm.attributeValue("business_type_desc");
	            	String deliver_time=deliverTm.attributeValue("deliver_time");
	            	String business_type=deliverTm.attributeValue("business_type");
	            	results="如果"+realTime+"正常发件。快递类型为:"+business_type_desc+"。快递预计到达时间:"+deliver_time+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
	            }
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        System.out.println(results);
	}

}
