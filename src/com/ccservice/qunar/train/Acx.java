package com.ccservice.qunar.train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Acx {

    public static void main(String[] args) {
        Date datenew = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            int d = daysBetween(sdf.format(datenew), "2015-11-21 18:10:00");
            System.out.println(d);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static String newdistribution(String date, String address) {
        String result = "";
        Date datenew = new Date();
        int d = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            d = daysBetween(sdf.format(datenew), date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        if (2 >= d && address.contains("北京")) {
            result = "378";
        }
        else if (2 >= d && !address.contains("北京")) {
            if (isContainCity(1, address)) {
                result = "379";
            }
            else {
                result = "1";
            }
        }
        else if (d > 2 && isContainCity(2, address)) {
            result = "378";
        }
        else if (d > 2 && isContainCity(3, address)) {
            result = "379";
        }
        return result;
    }

    public static boolean isContainCity(int type, String address) {
        boolean flag = false;
        String resultcity = "";
        //72城市
        String citieshn = "天津 ,石家庄,唐山,保定,廊坊,沧州,呼和浩特,沈阳,大连,长春,上海,南京,苏州,无锡,常州,南通,昆山,太仓,扬州,吴江,泰州,常熟,镇江,张家港,杭州,金华,宁波,义乌,绍兴,嘉州,湖州,合肥,芜湖,福州,厦门,莆田,泉州,济南,青岛,淄博,烟台,潍坊,济宁,郑州,武汉,长沙,广州,深圳,珠海,东莞,中山,江门,佛山,成都,重庆,昆明,西安";
        //72-13城市
        String citieshn2 = "苏州,无锡,常州,南通,昆山,太仓,扬州,吴江,泰州,常熟,镇江,张家港,杭州,金华,宁波,义乌,绍兴,嘉州,湖州,合肥,芜湖,福州,厦门,莆田,泉州,济南,青岛,淄博,烟台,潍坊,济宁,郑州,武汉,长沙,广州,深圳,珠海,东莞,中山,江门,佛山,成都,重庆,昆明,西安";
        //13城市
        String citiestqString = "北京,天津 ,石家庄,唐山,保定,廊坊,沧州,呼和浩特,沈阳,大连,长春,上海,南京";
        if (type == 1) {
            resultcity = citieshn;
        }
        else if (type == 2) {
            resultcity = citiestqString;
        }
        else if (type == 3) {
            resultcity = citieshn2;
        }
        String[] citys = resultcity.split(",");
        for (int i = 0; i < citys.length; i++) {
            if (address.contains(citys[i])) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    /** 
    *字符串的日期格式的计算 
     * @throws ParseException 
    */
    public static int daysBetween(String smdate, String bdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between_days));
    }
}
