package com.ccservice.qunar.train;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.qunar.QunarOrderMethod;

public class MyThreadQunarOrderOfflineQunarOne extends Thread {

    private String arrStationValue, dptStationValue, extSeatValue, seatValue_Key, orderNoValue, certNoValue,
            certTypeValue, nameValue, seatValue, trainEndTimeValue, trainNoValue, trainStartTimeValue,
            arrStationValue_lc, dptStationValue_lc, extSeatValue_lc, seatValue_lc, trainEndTimeValue_lc,
            trainNoValue_lc, trainStartTimeValue_lc, birthday;

    private float ticketPayValue, seatValue_Value = 0;

    private int ticketTypeValue, seqValue;

    private long qunar_agentid;

    private QunarOrderMethod qunarordermethod;

    private String qunarPayurl;

    // 创建去哪儿线下订单
    private CreateQunarTrainOrder ctrainorderoffline;

    public MyThreadQunarOrderOfflineQunarOne(QunarOrderMethod qunarordermethod, long qunar_agentid, String qunarPayurl) {
        this.qunarordermethod = qunarordermethod;
        this.qunar_agentid = qunar_agentid;
        this.qunarPayurl = qunarPayurl;
    }

    @Override
    public void run() {
        handleTrainOrder();
    }

    /**
     * 处理订单
     * 
     * @time 2015年2月2日 下午4:12:44
     * @author fiend
     */
    public void handleTrainOrder() {
        //        if (this.qunarordermethod.getId() != 0) {// 直接存储
        //            WriteLog.write("QUNAR火车票接口_3.1获取订单", "订单ID:" + this.qunarordermethod.getQunarordernumber() + "===>直接存储");
        getorder(this.qunarordermethod.getOrderjson());
        //        }

    }

    /**
     * 说明：数据库生成订单
     * 
     * @param info
     * @time 
     * @author 
     */
    @SuppressWarnings("unchecked")
    public void getorder(JSONObject info) {
        String ispaper = info.getString("isPaper");// 是否纸质票
        arrStationValue = info.getString("arrStation");// 终点站
        dptStationValue = info.getString("dptStation");// 始发站
//        extSeatValue = info.getString("extSeat");// 备选车票
        extSeatValue = info.getString("extSeatMap");// 备选车票
//        String extSeatString = getAllExtSeat(extSeatValue);
        String extSeatString = getAllExtSeatMap(extSeatValue);//新修改逻辑Map
        String paperType = info.getString("paperType");
        String paperBackup = info.getString("paperBackup");
        String paperLowSeatCount = info.getString("paperLowSeatCount");
        if (info.containsKey("jointTrip") == false) {// 如果是普通订单,seq:0
//            seatValue = info.getString("seat");// 坐席及其价格
            try {
            	orderNoValue = info.getString("orderNo");// 订单号
            	Thread.sleep((long) (1000 * Math.random()));
				String sql1="select * from TrainOrderOffline with(nolock) where OrderNumberOnline='"+orderNoValue+"'";
				List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
				WriteLog.write("MyThreadQunarOrderOfflineQunarOne",  "OrderNumberOnline=" + orderNoValue
                        + ";防止订单重复list=" + list.size());
				if (list.size()==0) {//如果未存库
				seatValue = info.getString("seatMap");// 坐席及其价格
				// 0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
				// TODO 普通订单的坐席类型
				JSONObject seatObject = JSONObject.parseObject(seatValue);
				Set<String> seatSet = seatObject.keySet();

				for (String str : seatSet) {
				    seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
//              int i_seat = Integer.parseInt(str);
//              seatValue_Key = getSeat(i_seat);// 得到坐席
				  seatValue_Key = str;// 得到坐席
        }
//            extSeatValue_lc = info.getString("extSeat");// 联程备选车票及其价格
				ticketPayValue = (float) info.getDoubleValue("ticketPay");// 应付订单票款
				trainEndTimeValue = info.getString("trainEndTime");// 火车到达时间
				Date date_trainEndTime = new Date();
				DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
				try {
				    date_trainEndTime = df.parse(trainEndTimeValue);
				}
				catch (ParseException e) {
				    e.printStackTrace();
				}
				long endMilliSeconds = date_trainEndTime.getTime();
				DateFormat dfm = new SimpleDateFormat("HH:mm");
				trainEndTimeValue = dfm.format(date_trainEndTime);
				trainNoValue = info.getString("trainNo");// 火车编号
				trainStartTimeValue = info.getString("trainStartTime");// 火车出发时间
				Date date_trainStartTime = new Date();
				try {
				    date_trainStartTime = df.parse(trainStartTimeValue);
				}
				catch (ParseException e) {
				    e.printStackTrace();
				}
				long startMilliSeconds = date_trainStartTime.getTime();
				int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
				String costTime = String.valueOf(costtime / (60 * 60)) + ":" + String.valueOf((costtime % (60 * 60)) / 60);// 历时
				JSONArray passengers = info.getJSONArray("passengers");
				Trainorder trainorder = new Trainorder();// 创建订单
				trainorder.setTaobaosendid(extSeatString);
				if ("1".equals(ispaper)) {// 如果去哪儿订单是线下订单
				    trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
				}
				trainorder.setQunarOrdernumber(orderNoValue);
				List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
				for (int i = 0; i < passengers.size(); i++) {
				    List<Trainticket> traintickets = new ArrayList<Trainticket>();
				    JSONObject infopassengers = passengers.getJSONObject(i);
				    certNoValue = infopassengers.getString("certNo");// 乘车人证件号码
				    // 1：身份证；C：港澳通行证；G：台湾通行证；B：护照
				    certTypeValue = infopassengers.getString("certType");// 乘车人证件种类
				    if ("B".equals(certTypeValue)) {
				        certTypeValue = "3";
				    }
				    else if ("C".equals(certTypeValue)) {
				        certTypeValue = "4";
				    }
				    else if ("G".equals(certTypeValue)) {
				        certTypeValue = "5";
				    }
				    if (certNoValue.length() == 18) {
				        String year = certNoValue.substring(6, 10);
				        String month = certNoValue.substring(10, 12);
				        String day = certNoValue.substring(12, 14);
				        birthday = year + "-" + month + "-" + day;
				    }
				    nameValue = infopassengers.getString("name");// 乘车人姓名
				    if (infopassengers.containsKey("ticketType")) {// 票种类型，1：成人票；0：儿童票 2:学生票(如果是去哪儿的老版本，没有ticketType)
				        ticketTypeValue = Integer.parseInt(infopassengers.getString("ticketType"));
				    }
				    else {
				        ticketTypeValue = 1;
				    }
				    // TODO 普通订单存入数据库
				    Trainticket trainticket = new Trainticket();
				    Trainpassenger trainpassenger = new Trainpassenger();
				    trainticket.setArrival(arrStationValue);// 存入终点站
				    trainticket.setDeparture(dptStationValue);// 存入始发站
				    trainticket.setStatus(Trainticket.WAITISSUE);
				    trainticket.setTicketno("0");
				    // 无法存入剩余票
				    trainticket.setSeq(0);// 存入订单类型
				    trainticket.setTickettype(ticketTypeValue);// 存入票种类型
				    trainticket.setPayprice(seatValue_Value);
				    trainticket.setPrice(seatValue_Value);// 应付火车票价格
				    trainticket.setArrivaltime(trainEndTimeValue);// 火车到达时间
				    trainticket.setTrainno(trainNoValue); // 火车编号
				    trainticket.setDeparttime(trainStartTimeValue);// 火车出发时间
				    trainticket.setIsapplyticket(1);
				    trainticket.setRefundfailreason(0);
				    trainticket.setCosttime(costTime);// 历时
				    //                Map<String, String> seatmap = extSeat(seatValue_Key, extSeatValue);
				    //                seatValue_Key = seatmap.get("seatCode");
				    //                extSeatValue = seatmap.get("extSeat");
				    trainticket.setSeattype(seatValue_Key);// 车票坐席
				    trainticket.setInsurenum(0);
				    trainticket.setInsurprice(0f);// 采购支付价
				    trainticket.setInsurorigprice(0f);// 保险原价
				    traintickets.add(trainticket);
				    trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
				    trainpassenger.setIdtype(Integer.parseInt(certTypeValue));// 存入乘车人证件种类
				    trainpassenger.setName(nameValue);// 存入乘车人姓名
				    trainpassenger.setBirthday(birthday);
				    trainpassenger.setChangeid(0);
				    trainpassenger.setAduitstatus(1);
				    trainpassenger.setTraintickets(traintickets);
				    trainplist.add(trainpassenger);
				}
				//纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
				trainorder.setPaymethod(Integer.parseInt(paperType));
				//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
				trainorder.setSupplypayway(Integer.parseInt(paperBackup));
				//至少接受下铺/靠窗/连坐数量
				trainorder.setRefuseaffirm(Integer.parseInt(paperLowSeatCount));
				trainorder.setContactuser(info.getString("transportName"));
				trainorder.setContacttel(info.getString("transportPhone"));
				trainorder.setTicketcount(trainplist.size());
				trainorder.setCreateuid(59l);
				String address=info.getString("transportAddress");
				trainorder.setInsureadreess(address.replaceAll("'", ""));
				trainorder.setOrderprice(ticketPayValue);
				trainorder.setIsjointtrip(0);
				trainorder.setAgentid(qunar_agentid);
				trainorder.setState12306(Trainorder.WAITORDER);
				trainorder.setPassengers(trainplist);
				trainorder.setOrderstatus(Trainorder.WAITISSUE);
				trainorder.setAgentprofit(0f);
				trainorder.setCommission(0f);
				trainorder.setTcprocedure(0f);
				trainorder.setInterfacetype(2);
				trainorder.setCreateuser("七彩沈阳_去哪儿");
				//                        trainorder.setCreateuid(createuid);
				try {
//                if (extSeatValue_lc != null && !extSeatValue_lc.replace("[", "").replace("]", "").equals("")) {
//                    //                    createTrainOrderExtSeat(trainorder.getId(), extSeatValue);// 备选车次及其价格
//                }
				    WriteLog.write("qunartrainorder", trainorder.getQunarOrdernumber() + "(:)" + trainorder.getId());
				    if (trainorder.getOrdertype() == 2) {// 去哪儿的线下订单去线下订单下单
				        WriteLog.write("去哪儿1火车票类型", "线下");
				        trainorderofflineadd(trainorder);

				    }
				    else {// 如果是线上订单的话才扔MQ里去下单
				        WriteLog.write("去哪儿1火车票类型", "线上");
				    }
				}
				catch (Exception e) {
				    WriteLog.write("MyThreadQunarOrderOfflineQunarOne_LadanException_Error", info.toString());
				    ExceptionUtil.writelogByException("MyThreadQunarOrderOfflineQunarOne_LadanException_Error", e);
				}
				}
			} 
            catch (Exception e) {
				  WriteLog.write("MyThreadQunarOrderOfflineQunarOne_LadanException_Error", info.toString());
				    ExceptionUtil.writelogByException("MyThreadQunarOrderOfflineQunarOne_LadanException_Error", e);
				} 
       }
        else {
            //            refuse();
        }
    }
    /**
     * 去哪修改坐席类型
     * @param extSeatValueMap
     * @return
     */
    public static String getAllExtSeatMap(String extSeatValue) {
        String extString = "";
        JSONArray array = JSONArray.parseArray(extSeatValue);
        for (int i = 0; i < array.size(); i++) {
            JSONObject seatObject = array.getJSONObject(i);
            Set<String> seatSet = seatObject.keySet();
            for (String str : seatSet) {
                float seatValue_Value = (float) seatObject.getDoubleValue(str);// 得到票价
                extString+=str+":￥" + seatValue_Value + ",";
            }

        }
        if (extString.contains(",")) {
            extString = extString.substring(0, extString.length() - 1);
        }
        else {
            extString = "无";
        }
        return extString;

    }

    /**
     * 说明：坐席
     * 
     * @param i_seat
     * @return seat
     * @time 2014年8月30日 上午11:07:38
     * @author yinshubin
     */
    //0：站票；1：硬座；2：软座；3：一等软座；4：二等软座；5-7：硬卧上、中、下；8-9：软卧上、下；10-11：高级软卧上、下
    public static String getSeat(int i_seat) {
        String str = "";
        switch (i_seat) {
        case 0:
            str = "无座";
            break;
        case 1:
            str = "硬座";
            break;
        case 2:
            str = "软座";
            break;
        case 3:
            str = "一等软座";
            break;
        case 4:
            str = "二等软座";
            break;
        case 5:
            str = "硬卧上";
            break;
        case 6:
            str = "硬卧中";
            break;
        case 7:
            str = "硬卧下";
            break;
        case 8:
            str = "软卧上";
            break;
        case 9:
            str = "软卧下";
            break;
        case 10:
            str = "高级软卧上";
            break;
        case 11:
            str = "高级软卧下";
            break;
        case 12:
            str = "特等座";
            break;
        case 13:
            str = "商务座";
            break;
        default:
            break;
        }
        return str;
    }

    public static String getAllExtSeat(String extSeatValue) {
        JSONArray extArray = JSONArray.parseArray(extSeatValue);
        String extString = "";
        for (int i = 0; i < extArray.size(); i++) {
            String seat0 = extArray.getJSONObject(i).getString("0");
            String seat1 = extArray.getJSONObject(i).getString("1");
            String seat2 = extArray.getJSONObject(i).getString("2");
            String seat3 = extArray.getJSONObject(i).getString("3");
            String seat4 = extArray.getJSONObject(i).getString("4");
            String seat5 = extArray.getJSONObject(i).getString("5");
            String seat6 = extArray.getJSONObject(i).getString("6");
            String seat7 = extArray.getJSONObject(i).getString("7");
            String seat8 = extArray.getJSONObject(i).getString("8");
            String seat9 = extArray.getJSONObject(i).getString("9");
            String seat10 = extArray.getJSONObject(i).getString("10");
            String seat11 = extArray.getJSONObject(i).getString("11");
            String seat12 = extArray.getJSONObject(i).getString("12");
            String seat13 = extArray.getJSONObject(i).getString("13");
            if (!StringIsNull(seat0)) {
                extString += "无座:￥" + seat0 + ",";
            }
            else if (!StringIsNull(seat1)) {
                extString += "硬座:￥" + seat1 + ",";
            }
            else if (!StringIsNull(seat2)) {
                extString += "软座:￥" + seat2 + ",";
            }
            else if (!StringIsNull(seat3)) {
                extString += "一等软座:￥" + seat3 + ",";
            }
            else if (!StringIsNull(seat4)) {
                extString += "二等软座:￥" + seat4 + ",";
            }
            else if (!StringIsNull(seat5)) {
                extString += "硬卧上:￥" + seat5 + ",";
            }
            else if (!StringIsNull(seat6)) {
                extString += "硬卧中:￥" + seat6 + ",";
            }
            else if (!StringIsNull(seat7)) {
                extString += "硬卧下:￥" + seat7 + ",";
            }
            else if (!StringIsNull(seat8)) {
                extString += "软卧上:￥" + seat8 + ",";
            }
            else if (!StringIsNull(seat9)) {
                extString += "软卧下:￥" + seat9 + ",";
            }
            else if (!StringIsNull(seat10)) {
                extString += "高级软卧上:￥" + seat10 + ",";
            }
            else if (!StringIsNull(seat11)) {
                extString += "高级软卧下:￥" + seat11 + ",";
            }
            else if (!StringIsNull(seat12)) {
                extString += "特等座:￥" + seat12 + ",";
            }
            else if (!StringIsNull(seat13)) {
                extString += "商务座:￥" + seat13 + ",";
            }

        }
        if (extString.contains(",")) {
            extString = extString.substring(0, extString.length() - 1);
        }
        else {
            extString = "无";
        }
        System.out.println(extString);
        return extString;

    }

    /** 字符串是否为空 */
    public static boolean StringIsNull(String str) {
        if (str == null || "".equals(str.trim())) {
            return true;
        }
        return false;
    }

    // 当无座为主坐席时调用备选坐席
    private Map<String, String> extSeat(String seatCode, String extSeat) {
        JSONArray extArray = JSONArray.parseArray(extSeat);
        WriteLog.write("Error_MyThreadQunarOrderOfflineQunarOne_createTrainOrderExtSeat", "替换前extArray:" + extArray.toString());
        Map<String, String> map = new HashMap<String, String>();
        if (seatCode.equals("0")) {
            JSONObject json = new JSONObject();
            for (int i = 0; i < extArray.size(); i++) {
                String seatY = extArray.getJSONObject(i).getString("1");
                String seat2 = extArray.getJSONObject(i).getString("4");
                String seatR = extArray.getJSONObject(i).getString("2");
                WriteLog.write("Error_MyThreadQunarOrderOfflineQunarOne_createTrainOrderExtSeat", "硬座,seatY:" + seatY + "二等座,seat2:"
                        + seat2);
                if (!StringIsNull(seatY)) {

                    extArray.remove(i);
                    json.put(seatCode, Float.valueOf(seatY));
                    extArray.add(json);
                    seatCode = "1";
                    break;

                }
                else if (!StringIsNull(seatR)) {
                    extArray.remove(i);
                    json.put(seatCode, Float.valueOf(seatR));
                    extArray.add(json);
                    seatCode = "2";
                    break;
                }
                else if (!StringIsNull(seat2)) {
                    extArray.remove(i);

                    json.put(seatCode, Float.valueOf(seat2));
                    extArray.add(json);
                    seatCode = "4";
                    break;
                }
            }
        }
        map.put("seatCode", seatCode);
        map.put("extSeat", extArray.toString());
        return map;

    }

    /**
     * 创建火车票备选坐席
     * 
     * @param orderid
     * @param extseats
     */
    private void createTrainOrderExtSeat(long orderid, String extseats) {
        try {
            String sql = "INSERT INTO TrainOrderExtSeat (OrderId ,ExtSeat ,ReMark) VALUES ( " + orderid + ",'"
                    + extseats + "' ,'')";
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
        catch (Exception e) {
            WriteLog.write("Error_MyThreadQunarOrderOfflineQunarOne_createTrainOrderExtSeat", orderid + "");
            ExceptionUtil.writelogByException("Error_MyThreadQunarOrderOfflineQunarOne_createTrainOrderExtSeat", e);
        }

    }

    private DBoperationUtil dt = new DBoperationUtil();

    public void createTrainOrderOffline(Trainorder trainorder) {
        WriteLog.write("CreateQunarTrainOrder", "CreateQunarTrainOrder");
        trainorderofflineadd(trainorder);
    }

    /**
     * 添加火车票线下订单
     */
    public void trainorderofflineadd(Trainorder trainorder) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        //        String addresstemp = "河北省,石家庄市,长安区,就业服务大厦";
        String addresstemp = trainorder.getInsureadreess();
        if (addresstemp.contains("省") && addresstemp.contains("市")) {
            String province = addresstemp.substring(0, addresstemp.indexOf("省"));
            String city = addresstemp.substring(addresstemp.indexOf("省") + 1, addresstemp.indexOf("市"));
            String region = addresstemp.substring(addresstemp.indexOf("市") + 1);
            addresstemp = province + "," + city + "," + region;
        }
        String agentidtemp="";
        String star="2017-01-26 00:00:00";//开始时间
		String end="2017-02-03 00:00:00";//结束时间
		SimpleDateFormat localTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try{
	        Date sdate = localTime.parse(star);
	        Date edate=localTime.parse(end);
	        long time = System.currentTimeMillis();
	        if(time>=sdate.getTime()&& time<edate.getTime()){
	            if (addresstemp.contains("县") || addresstemp.contains("镇")|| addresstemp.contains("乡")) {
	            	agentidtemp="414";
	            	WriteLog.write("春节部分地区拦截",  "trainorderofflineadd:" + "线下火车票分单log记录:地址:"
	            			+ addresstemp + "----->agentId:" + agentidtemp);
	            }
	        }
	        else{
	        	agentidtemp = distribution2(addresstemp);
	        }
	    }catch(Exception e){
	    	ExceptionUtil.writelogByException("春节顺丰无法邮寄的地区拦截异常", e);
	    	e.printStackTrace();
	    }
        // 通过订单邮寄地址匹配出票点
        BaiDuMapApi bd = new BaiDuMapApi();
        //        String agentidtemp = bd.getAgentidByInsuraddress(getAllListByAdd(addresstemp, "-1"), addresstemp);
//        String agentidtemp = distribution1(addresstemp);
//        String agentidtemp = distribution2(addresstemp);
        WriteLog.write("线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());
        SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
        Timestamp startTime = new Timestamp(new Date().getTime());
        trainOrderOffline.setOrderTimeOut(startTime);
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        
        //通过出票点和邮寄地址获取预计到达时间results
        String delieveStr=getDelieveStr(agentidtemp,trainorder.getInsureadreess());//
        String updatesql="UPDATE TrainOrderOffline SET expressDeliver ='"+delieveStr+"' WHERE ID="+orderid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime() + ":00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                trainTicketOffline.setSubOrderId(ticket.getTicketno());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }

    public static List getAllListByAdd(String address, String orderId) {
        // address = "北京,北京市,昌平区,沙河镇";
        String[] ss = address.split(",");
        String procedure = "sp_findAgentid_byAddress @ProvinceName='" + ss[0] + "',@CityName='" + ss[1]
                + "',@RegionName='" + ss[2] + "',@TownName='',@OrderId='" + orderId + "'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        return list;
    }
    /**
     * 根据数据库设置匹配出票点
     * @param address1
     * @return
     */
    public static String distribution2(String address1) {
    	boolean flag=false;
		//默认出票点
    	String sql1="SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+"59";
    	List list1=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
    	String agentId="378";
    	if(list1.size()>0){
    		Map map=(Map)list1.get(0);
    		agentId=map.get("agentId").toString();
    	}
    	//程序自动分配出票点
    	String sql2="SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid="+"59";
    	List list2=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
    	for(int i=0;i<list2.size();i++){
    		Map mapp=(Map)list2.get(i);
    		String provinces=mapp.get("provinces").toString();
    		String agentid=mapp.get("agentId").toString();
    		String[] add=provinces.split(",");
    		for (int j = 0; j < add.length; j++) {
				if (address1.startsWith(add[j])) {
					agentId=agentid;
					flag=true;
				}
				if(flag){
					break;
				}
			}
			if(flag){
				break;
			}
    	}
    	WriteLog.write("去哪1MyThreadQunarOrderOfflineQunarOne新版分配订单", "agentId="+agentId+";address1="+address1);
    	return agentId;
	}
    
    public static String distribution1(String address1) {
		String resultp="";
		String agentp="378";
		int num=Integer.parseInt(PropertyUtil.getValue("allAgentNum", "train.properties"));
		boolean flag=false;
		for (int i = 1; i <= num; i++) {
			String agents=PropertyUtil.getValue("AgentId"+i, "train.properties");
			String provinces=PropertyUtil.getValue("MailProvince"+i, "train.properties");
			String resultNames="";
			try {
				resultNames = new String(provinces.getBytes("ISO-8859-1"),"utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String[] add=resultNames.split(",");
			for (int j = 0; j < add.length; j++) {
//				if (address1.contains(add[j])) {
//					resultp=add[j];
//					agentp=agents;
//					flag=true;
//				}
				if (address1.startsWith(add[j])) {
					resultp=add[j];
					agentp=agents;
					flag=true;
				}
				if(flag){
					break;
				}
			}
			if(flag){
				break;
			}
		}
		return agentp;
    }



    public static String getDelieveStr(String agengId,String address){
    	String results="";
    	String fromcode="010";
    	String tocode=getExpressCodes(address);
    	String time1="10:00:00";
    	String time2="18:00:00";
    	
    	SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        String dates=sdf.format(new Date());
    	String sql1="SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId="+agengId;
    	List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
    	if(list.size()>0){
    		Map map=(Map)list.get(0);
    		fromcode=map.get("fromcode").toString();
    		time1=map.get("time1").toString();
    		time2=map.get("time2").toString();
    	}
    	String realTime=getRealTimes(dates,time1,time2);
    	String urlString=PropertyUtil.getValue("expressDeliverUrl", "train.properties");
    	String param="times="+realTime+"&fromcode="+fromcode+"&tocode="+tocode;
    	 WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->"+urlString+"?"+param);
    	String result=SendPostandGet.submitPost(urlString, param,"UTF-8").toString();
    	WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息","address="+address+"---------->result:"+result);
		try {
			Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if("OK".equals(root.elementText("Head"))){
                if(result.contains("deliver_time")){
                    Element deliverTmResponse = body.element("DeliverTmResponse");
                    Element deliverTm = deliverTmResponse.element("DeliverTm");
                    String business_type_desc=deliverTm.attributeValue("business_type_desc");
                    String deliver_time=deliverTm.attributeValue("deliver_time");
                    String business_type=deliverTm.attributeValue("business_type");
                    results="如果"+realTime+"正常发件。快递类型为:"+business_type_desc+"。快递预计到达时间:"+deliver_time+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                }else{
                    results="获取快递时间失败！请上官网核验快递送达时间。";
                }
            }
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          
    	return results;
    }
    
    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public static String getExpressCodes(String address){
    	String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
    	List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
    	String cityCode="010";
    	if(list.size()>0){
    		Map map=(Map)list.get(0);
    		cityCode=map.get("CityCode").toString();
    	}
    	return cityCode;
    }
    
    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public static String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }
    
    public static Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

}
