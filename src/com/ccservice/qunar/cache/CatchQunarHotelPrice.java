package com.ccservice.qunar.cache;

import java.util.*;
import net.sf.json.JSONArray;

import com.ccservice.compareprice.PropertyUtil;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;

import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.QunarCookieUtil;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class CatchQunarHotelPrice {

    //是否是更新缓存
    private static boolean isupdate = false;

    //代理IP
    private static String AgentIp = PropertyUtil.getValue("AgentIp");

    //即时爬网络IP
    private static List<String> AgentIpList = new ArrayList<String>();

    //如家品牌酒店是否更新，0：不更新，1：更新
    private static boolean UpdateRuJiaHotel = "1".equals(PropertyUtil.getValue("UpdateRuJiaHotel")) ? true : false;

    /**
     * @param type 缓存数据类型，0：抓取新数据，有缓存直接跳过；1：抓取所有数据并更新缓存
     */
    @SuppressWarnings("unchecked")
    public static void cache(int type) {
        isupdate = type == 1 ? true : false;
        //Hotel
        String sql = "where C_STATE = 3 and C_STARTPRICE > 0";
        //缓存城市
        String QunarCacheDataCity = PropertyUtil.getValue("QunarCacheDataCity");
        if (!ElongHotelInterfaceUtil.StringIsNull(QunarCacheDataCity)) {
            sql += " and C_CITYID in (" + QunarCacheDataCity + ")";
        }
        List<Hotelall> hotels = Server.getInstance().getHotelService().findAllHotelall(sql, "", -1, 0);
        int size = hotels.size();
        System.out.println("酒店合计：" + size + "个。");
        //循环
        for (int i = 0; i < size; i++) {
            foreach(hotels.get(i), type, 0);
        }
    }

    private static void foreach(Hotelall h, int type, int ErrorCount) {
        String qid = h.getQunarId();
        String hname = h.getName().trim();
        //不更新如家快捷
        if (!UpdateRuJiaHotel) {
            long chaininfoid = h.getChaininfoid() == null ? 0 : h.getChaininfoid().longValue();
            //如家酒店
            if (chaininfoid == 129) {
                System.out.println(hname + "，如家酒店，跳过。");
                return;
            }
        }
        try {
            //获取缓存
            Map<String, JSONArray> map = Server.getInstance().getHotelCacheService()
                    .getQunarHotelPrice(qid, new HashMap<String, JSONArray>(), 0);
            if (map == null || map.size() == 0) {
                System.out.println("==========" + hname + "，无缓存，请求去哪儿==========");
            }
            else if (isupdate) {
                System.out.println("----------" + hname + "，更新缓存，请求去哪儿----------");
            }
            else {
                System.out.println(hname + "，有缓存，跳过。");
                return;
            }
            //网络爬取代理IP
            if (ElongHotelInterfaceUtil.StringIsNull(AgentIp)) {
                if (AgentIpList.size() == 0) {
                    AgentIpList = CatchAgentIp.getIp();
                }
                AgentIp = AgentIpList.get(new Random().nextInt(AgentIpList.size()));
            }
            //暂停休息
            Thread.sleep(1000 * 10);
            //请求去哪儿
            new ReqQunar(hname, qid, type, AgentIp, AgentIpList).start();
        }
        catch (Exception e) {
            System.out.println("**********" + hname + "，" + AgentIp + "，" + ElongHotelInterfaceUtil.errormsg(e)
                    + "**********");
        }
    }

    public static List<String> getAgentIpList() {
        return AgentIpList;
    }

    public static void setAgentIpList(List<String> agentIpList) {
        CatchQunarHotelPrice.AgentIpList = agentIpList;
    }

    public static String getAgentIp() {
        return AgentIp;
    }

    public static void setAgentIp(String agentIp) {
        CatchQunarHotelPrice.AgentIp = agentIp;
    }
}

class ReqQunar extends Thread {
    private String hotelName;

    private String qunarHotelId;

    private int type;

    private String AgentIp;

    private List<String> agentIpList;

    public ReqQunar(String hotelName, String qunarHotelId, int type, String AgentIp, List<String> agentIpList) {
        this.hotelName = hotelName;
        this.qunarHotelId = qunarHotelId;
        this.type = type;
        this.AgentIp = AgentIp;
        this.agentIpList = agentIpList;
    }

    @Override
    public void run() {
        request(hotelName, qunarHotelId, type, AgentIp, agentIpList, 0);
    }

    private void request(String hotelName, String qunarHotelId, int type, String AgentIp, List<String> agentIpList,
            int ErrorCount) {
        long start = System.currentTimeMillis();

        //关闭日志输出
        org.apache.commons.logging.LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");

        //实例化WebClient
        WebClient webClient = null;
        if ("localhost".equals(AgentIp)) {
            webClient = new WebClient(BrowserVersion.getDefault());
        }
        else {
            webClient = new WebClient(BrowserVersion.getDefault(), AgentIp.split(":")[0], Integer.parseInt(AgentIp
                    .split(":")[1]));
        }

        //设置
        webClient.getOptions().setTimeout(1000 * 30);
        webClient.getOptions().setRedirectEnabled(true);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getCookieManager().setCookiesEnabled(true);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());

        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setAppletEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

        //去哪儿ID
        String QunarCityCode = qunarHotelId.substring(0, qunarHotelId.lastIndexOf("_"));
        String QunarHotelId = qunarHotelId.substring(qunarHotelId.lastIndexOf("_") + 1);

        //请求头
        webClient.addRequestHeader("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        webClient.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
        webClient.addRequestHeader("Cache-Control", "max-age=0");
        webClient.addRequestHeader("Connection", "keep-alive");
        webClient.addRequestHeader("Referer", "http://hotel.qunar.com/city/" + QunarCityCode);
        String UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";
        webClient.addRequestHeader("User-Agent", UserAgent);

        //获取Cookie
        String cookie = QunarCookieUtil.getCookie("localhost".equals(AgentIp) ? AgentIp : AgentIp.split(":")[0]);
        if (!ElongHotelInterfaceUtil.StringIsNull(cookie)) {
            webClient.getCookieManager().addCookie(new Cookie("qunar.com", "VCD", cookie));
        }

        //日期
        String current = ElongHotelInterfaceUtil.getCurrentDate();
        String fromDate = "";
        String toDate = "";
        try {
            fromDate = ElongHotelInterfaceUtil.getAddDate(current, 1);
            toDate = ElongHotelInterfaceUtil.getAddDate(fromDate, 7);
        }
        catch (Exception e) {
        }

        //URL
        String url = "http://hotel.qunar.com/city/" + QunarCityCode + "/dt-" + QunarHotelId + "/?tag=" + QunarCityCode
                + "#fromDate=" + fromDate + "&toDate=" + toDate;

        HtmlPage page = null;
        try {
            page = webClient.getPage(url);
            //等待加载JS
            webClient.waitForBackgroundJavaScript(1000 * 10);
            if (page.asText().contains("由于您的查询频率过高，请输入验证码后继续")) {
                throw new Exception("由于您的查询频率过高，请输入验证码后继续。");
            }
        }
        catch (Exception e) {
            agentIpList.remove(AgentIp);
            String msg = e.getMessage() == null ? "" : e.getMessage();
            System.out.println("**********" + hotelName + "，" + AgentIp + "，" + msg + "，重新请求去哪儿**********");
            //重新请求
            if (agentIpList.size() == 0) {
                agentIpList = CatchAgentIp.getIp();
            }
            AgentIp = agentIpList.get(new Random().nextInt(agentIpList.size()));
            CatchQunarHotelPrice.setAgentIp(AgentIp);
            CatchQunarHotelPrice.setAgentIpList(agentIpList);
            ErrorCount++;
            if (ErrorCount < 5) {
                request(hotelName, qunarHotelId, type, AgentIp, agentIpList, ErrorCount);
            }
            return;
        }

        long end = System.currentTimeMillis();
        System.out.println(hotelName + "，获取源文件消耗时间：" + (end - start) + "毫秒，开始解析源文件...");

        //线程解析
        if (page != null) {
            new SaveCacheThread(webClient, page, hotelName, qunarHotelId, start, type).run();
        }
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getQunarHotelId() {
        return qunarHotelId;
    }

    public void setQunarHotelId(String qunarHotelId) {
        this.qunarHotelId = qunarHotelId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAgentIp() {
        return AgentIp;
    }

    public void setAgentIp(String agentIp) {
        AgentIp = agentIp;
    }

    public List<String> getAgentIpList() {
        return agentIpList;
    }

    public void setAgentIpList(List<String> agentIpList) {
        this.agentIpList = agentIpList;
    }
}