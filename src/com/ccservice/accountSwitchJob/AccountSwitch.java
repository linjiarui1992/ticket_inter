package com.ccservice.accountSwitchJob;

public class AccountSwitch {

	private static boolean accountSwitch = false;

	public static boolean isAccountSwitch() {
		return accountSwitch;
	}

	public static void setAccountSwitch(boolean accountSwitch) {
		AccountSwitch.accountSwitch = accountSwitch;
	}
	
	
}
