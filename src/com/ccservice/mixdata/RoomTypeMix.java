package com.ccservice.mixdata;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.roomtypeofficial.RoomTypeOfficial;
import com.ccservice.huamin.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 房型融合
 *
 */
public class RoomTypeMix {
	public static void main(String[] args) {
		mixRoomTypeOfiFirst();
	}
	/**
	 * 首次融合都在T_ROOMTYPEOFFICIAL中进行
	 */
	public static void mixRoomTypeOfiFirst(){
		String sql="select distinct o.C_ZSHOTELID from T_ROOMTYPEOFFICIAL o "+
					"join T_HOTEL h on h.C_ZSHOTELID=o.C_ZSHOTELID "+
					"join T_ROOMTYPE  r on r.C_HOTELID=h.ID "+
					"where o.c_state!=5 and o.C_ZSHOTELID is not null " +
					"and o.c_hotelid is not null and h.C_SOURCETYPE!=1 " +
					"and r.C_ZSROOMTYPEID is null";
		System.out.println(sql);
		List hotelids=Server.getInstance().getSystemService().findMapResultBySql(sql, null);
		for (int i = 0; i < hotelids.size(); i++) {
			System.out.println("当前剩余酒店："+(hotelids.size()-i));
			Map map =(Map)hotelids.get(i);
			String hotelid=map.get("C_ZSHOTELID").toString();
			System.out.println(hotelid);
			List<RoomTypeOfficial> ofi=Server.getInstance().getHotelService().findAllRoomTypeOfficial("where C_ZSHOTELID="+hotelid+"", " order by id asc ", -1, 0);
			Map<String, Long> roominfo=new HashMap<String, Long>();
			for (RoomTypeOfficial roomTypeOfficial : ofi) {
				if(roomTypeOfficial.getName()!=null){
					String ofiname=roomTypeOfficial.getName();
					if(ofiname.contains("（")&&ofiname.contains("）")){
						ofiname=ofiname.replaceAll("（.*）", "");
					}
					if(ofiname.contains("(")&&ofiname.contains(")")){
						ofiname=ofiname.replaceAll("\\(.*\\)", "");
					}
					if(ofiname.contains("双床")){
						ofiname=ofiname.replaceAll("双床", "");
					}
					if(ofiname.contains("大床")){
						ofiname=ofiname.replaceAll("大床", "");
					}
					if(ofiname.contains("间")){
						ofiname=ofiname.replaceAll("间", "房");
					}
					if(roominfo.containsKey(ofiname)){
						//如果该正式酒店下面有对应房型--删除正式表数据更新临时表数据
						roomTypeOfficial.setState(5);
						Server.getInstance().getHotelService().updateRoomTypeOfficialIgnoreNull(roomTypeOfficial);
						Roomtype r=new Roomtype();
						r.setId(roomTypeOfficial.getId());
						r.setZsroomtypeid(roominfo.get(ofiname));
						Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(r);
						System.out.println("融合："+ofiname+"==="+roominfo.get(ofiname));
					}else{
						//如果没有的话就添加该数据
						roominfo.put(ofiname, roomTypeOfficial.getId());
						System.out.println("正式房型："+ofiname+"==="+roominfo.get(ofiname));
					}
				}
			}
			
			//对选出作为正式房型的集合进行关联
			Collection<Long> idvals=roominfo.values();
			Iterator<Long> iditer= idvals.iterator();
			while(iditer.hasNext()){
				Long id=iditer.next();
				String sqltemp="select count(c_zsroomtypeid) as countid from t_roomtype where  c_zsroomtypeid="+id.intValue()+" having count(c_zsroomtypeid)>0 ";
				List idcounts=Server.getInstance().getSystemService().findMapResultBySql(sqltemp, null);
				for (int k = 0; k < idcounts.size(); k++) {
					Roomtype r=new Roomtype();
					r.setId(id.intValue());
					r.setZsroomtypeid(id);
					Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(r);
					System.out.println("更新融合融合数据："+id.intValue());
				}
			}
		}
	}
	/**
	 * 正式与临时表房型进行融合
	 */
	public static void mixRoomtype(){
		String sql="select distinct t.ID from T_ROOMTYPE  r join T_HOTEL t on t.ID=r.C_HOTELID " +
				   "where r.C_ZSROOMTYPEID is null and t.C_ZSHOTELID is not null and t.C_SOURCETYPE!=1 ";
		List hotelids=Server.getInstance().getSystemService().findMapResultBySql(sql, null);
		for (int i = 0; i < hotelids.size(); i++) {
			System.out.println("当前剩余酒店："+(hotelids.size()-i));
			Map map =(Map)hotelids.get(i);
			String hotelid=map.get("ID").toString();
			Hotel hotel=Server.getInstance().getHotelService().findHotel(Long.valueOf(hotelid));
			System.out.println(hotelid);
			//临时房型
			List<Roomtype> lin=Server.getInstance().getHotelService().findAllRoomtype("where C_HOTELID="+hotelid+" and c_zsroomtypeid is null", " order by id asc ", -1, 0);
			//正式房型
			List<RoomTypeOfficial> ofi=Server.getInstance().getHotelService().findAllRoomTypeOfficial("where C_ZSHOTELID="+hotel.getZshotelid()+"", " order by id asc ", -1, 0);
			Map<String, Long> roominfo=new HashMap<String, Long>();
			for (RoomTypeOfficial roomTypeOfficial : ofi) {
				if(roomTypeOfficial.getName()!=null){
					String ofiname=roomTypeOfficial.getName();
					if(ofiname.contains("（")&&ofiname.contains("）")){
						ofiname=ofiname.replaceAll("（.*）", "");
					}
					if(ofiname.contains("(")&&ofiname.contains(")")){
						ofiname=ofiname.replaceAll("\\(.*\\)", "");
					}
					if(ofiname.contains("双床")){
						ofiname=ofiname.replaceAll("双床", "");
					}
					if(ofiname.contains("大床")){
						ofiname=ofiname.replaceAll("大床", "");
					}
					if(ofiname.contains("间")){
						ofiname=ofiname.replaceAll("间", "房");
					}
					if(!roominfo.containsKey(ofiname)){
						//如果没有的话就添加该数据
						roominfo.put(ofiname, roomTypeOfficial.getId());
					}
				}
			}
			for (Roomtype room : lin) {
				if(room.getName()!=null){
					String lis=room.getName();
					System.out.println(lis+"----------------------------------------");
					if(lis.contains("（")&&lis.contains("）")){
						lis=lis.replaceAll("（.*）", "");
					}
					if(lis.contains("(")&&lis.contains(")")){
						lis=lis.replaceAll("\\(.*\\)", "");
					}
					if(lis.contains("双床")){
						lis=lis.replaceAll("双床", "");
					}
					if(lis.contains("大床")){
						lis=lis.replaceAll("大床", "");
					}
					if(lis.contains("间")){
						lis=lis.replaceAll("间", "房");
					}
					if(roominfo.containsKey(lis)){
						//如果该正式酒店下面有对应房型--删除正式表数据更新临时表数据
						WriteLog.write("临时-->正式", lis+",正式房型id："+roominfo.get(lis)+"----临时房型id："+room.getId());
						room.setZsroomtypeid(roominfo.get(lis));
						Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(room);
					}
				}
			}
		}
	}
}
