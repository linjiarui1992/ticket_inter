package com.ccservice.mixdata;

/**
 * @author wzc
 * 酒店融合
 */
import java.sql.SQLException;
import java.util.List;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelall.Hotelall;

public class HotelMix {
	
	/**
	 * @param 融合酒店填充正式酒店不足信息
	 */
	public static void mixData(Hotelall zshotel,Hotel lshotel){
		
		int temp=0;
		
		//填充enname
		if(zshotel.getEnname()==null||"".equals(zshotel.getEnname())){
			if(lshotel.getEnname()!=null&&!"".equals(zshotel.getEnname())){
				zshotel.setEnname(lshotel.getEnname());
				temp++;
				System.out.println("填充enname");
			}
		}
		//填充星级
		if(zshotel.getStar()==null||zshotel.getStar()==0){
			if(lshotel.getStar()!=null&&lshotel.getStar()!=0){
				zshotel.setStar(lshotel.getStar());
				temp++;
				System.out.println("星级更新");
			}
		}
		//填充城市
		if(zshotel.getCityid()==null||zshotel.getCityid()==0){
			if(lshotel.getCityid()!=null&&lshotel.getCityid()!=0){
				zshotel.setCityid(lshotel.getCityid());
				temp++;
				System.out.println("填充城市更新");
			}
		}
		//填充行政区
		if(zshotel.getRegionid1()==null||zshotel.getRegionid1()==0){
			if(lshotel.getRegionid1()!=null&&lshotel.getRegionid1()!=0){
				zshotel.setRegionid1(lshotel.getRegionid1());
				temp++;
				System.out.println("行政区更新");
			}
		}
		//填充商业区
		if(zshotel.getRegionid2()==null||zshotel.getRegionid2()==0){
			if(lshotel.getRegionid2()!=null&&lshotel.getRegionid2()!=0){
				zshotel.setRegionid2(lshotel.getRegionid2());
				temp++;
				System.out.println("填充商业区更新");
			}
		}
		//餐饮信息
		if(zshotel.getFootitem()==null||"".equals(zshotel.getFootitem())){
			if(lshotel.getFootitem()!=null&&!"".equals(lshotel.getFootitem())){
				zshotel.setFootitem(lshotel.getFootitem());
				temp++;
				System.out.println("餐饮信息更新");
			}
		}
		//服务信息
		if(zshotel.getServiceitem()==null||"".equals(zshotel.getFootitem())){
			if(lshotel.getServiceitem()!=null&&!"".equals(lshotel.getServiceitem())){
				zshotel.setServiceitem(lshotel.getServiceitem());
				temp++;
				System.out.println("服务信息更新");
			}
		}
		//会议信息
		if(zshotel.getMeetingitem()==null||"".equals(zshotel.getMeetingitem())){
			if(lshotel.getMeetingitem()!=null&&!"".equals(lshotel.getMeetingitem())){
				zshotel.setMeetingitem(lshotel.getMeetingitem());
				temp++;
				System.out.println("会议信息更新");
			}
		}
		//娱乐信息
		if(zshotel.getPlayitem()==null||"".equals(zshotel.getPlayitem())){
			if(lshotel.getPlayitem()!=null&&!"".equals(lshotel.getPlayitem())){
				zshotel.setPlayitem(lshotel.getPlayitem());
				temp++;
				System.out.println("娱乐信息更新");
			}
		}
		//卡信息
		if(zshotel.getCarttype()==null||"".equals(zshotel.getCarttype())){
			if(lshotel.getCarttype()!=null&&!"".equals(lshotel.getCarttype())){
				zshotel.setCarttype(lshotel.getCarttype());
				temp++;
				System.out.println("卡信息更新");
			}
		}
		
		//邮政编码
		if(zshotel.getPostcode()==null||"".equals(zshotel.getPostcode())){
			if(lshotel.getPostcode()!=null&&!"".equals(lshotel.getPostcode())){
				zshotel.setPostcode(lshotel.getPostcode());
				temp++;
				System.out.println("邮政编码更新");
			}
		}
		
		//酒店传真
		if(zshotel.getFax1()==null||"".equals(zshotel.getFax1())){
			if(lshotel.getFax1()!=null&&!"".equals(lshotel.getFax1())){
				zshotel.setFax1(lshotel.getFax1());
				temp++;
				System.out.println("酒店传真更新");
			}
		}
		
		
		//经纬度
		if(zshotel.getLat()==null||"".equals(zshotel.getLat())){
			if(lshotel.getLat()!=null&&!"".equals(lshotel.getLat())){
				zshotel.setLat(lshotel.getLat());
				temp++;
				System.out.println("经纬度更新");
			}
		}		
		if(zshotel.getLng()==null||"".equals(zshotel.getLng())){
			if(lshotel.getLng()!=null&&!"".equals(lshotel.getLng())){
				zshotel.setLng(lshotel.getLng());
				temp++;
				System.out.println("经纬度更新");
			}
		}
		
		
		//注意事项
		if(zshotel.getAvailPolicy()==null||"".equals(zshotel.getAvailPolicy())){
			if(lshotel.getAvailPolicy()!=null&&!"".equals(lshotel.getAvailPolicy())){
				zshotel.setAvailPolicy(lshotel.getAvailPolicy());
				temp++;
				System.out.println("注意事项更新");
			}
		}
		
		//房间设施
		if(zshotel.getRoomAmenities()==null||"".equals(zshotel.getRoomAmenities())){
			if(lshotel.getRoomAmenities()!=null&&!"".equals(lshotel.getRoomAmenities())){
				zshotel.setRoomAmenities(lshotel.getRoomAmenities());
				temp++;
				System.out.println("房间设施更新");
			}
		}
		if(temp>0){
			Server.getInstance().getHotelService().updateHotelallIgnoreNull(zshotel);//更新正式酒店信息
		}
	}
	
	//酒店信息填充
	public  void fixData(){
		List<Hotelall> zshotels = Server.getInstance().getHotelService().findAllHotelall("where  C_CITYID!=101 and id in (select distinct c_zshotelid from t_hotel where c_sourcetype in (3,5,6) and c_zshotelid is not null)", "order by c_cityid asc ", -1, 0);//
		for (int i = 0; i < zshotels.size(); i++) {
			List<Hotel> lshotels=Server.getInstance().getHotelService().findAllHotel("where c_zshotelid="+zshotels.get(i).getId(), " order by c_sourcetype desc ", -1, 0);
			for (int j = 0; j < lshotels.size(); j++) {
				mixData(zshotels.get(i), lshotels.get(j));
			}
		}
	}
	public static void main(String[] args) throws Exception {
		new HotelMix().mixHotelPro();
	}	
	/**
	 * 酒店融合---关联
	 * @throws SQLException 
	 */
	public  void mixHotelPro() throws SQLException {
		List<City> cityes=Server.getInstance().getHotelService().findAllCity("where c_type=1", "order by id asc", -1, 0);
		for (City city : cityes) {
			List<Hotel> lsHotels = Server.getInstance().getHotelService().findAllHotel("where c_sourcetype in (3,6) and c_zshotelid is null and c_cityid="+city.getId(),	"", -1, 0);
			int count = 0;
			List<Long> ids = new java.util.ArrayList<Long>();
			label:for (Hotel lshotel : lsHotels) {// 华闽
				System.out.println("遍历："+lshotel.getName());
				Long cityid = lshotel.getCityid();
				String name = lshotel.getName();
				String tortell = lshotel.getTortell();
				String address = lshotel.getAddress();
				String qunarId = lshotel.getQunarId();
				boolean flag=true;
				if (cityid != null && cityid > 0 && name != null&& !"".equals(name.trim())) {
					List<Hotelall> zshotels = Server.getInstance().getHotelService().findAllHotelall("where  C_CITYID="+city.getId()+" and c_name like '%"+name.trim()+"%'", "order by id ", -1, 0);//
					if(zshotels.size()>0){
						for (Hotelall el : zshotels) {
							if (name.trim().equals(el.getName().trim())) {
								System.out.println("--" + (++count) + "--酒店名--"+ lshotel.getName());
								lshotel.setZshotelid(el.getId());
								Server.getInstance().getHotelService().updateHotelIgnoreNull(lshotel);
								ids.add(el.getId());
								//el.setPaytype(2l);
								//el.setState(3);
								flag=false;
								//Server.getInstance().getHotelService().updateHotelallIgnoreNull(el);
								continue label;
							}
						}
					}
				}
				// 城市+去哪ID
				if (cityid != null && cityid > 0 && qunarId != null&& !"".equals(qunarId.trim())) {
					List<Hotelall> zshotels = Server.getInstance().getHotelService().findAllHotelall("where  C_CITYID="+city.getId()+" and c_qunarid='"+qunarId.trim()+"'", "order by id ", -1, 0);//
					if(zshotels.size()>0){
						for (Hotelall el : zshotels) {
							if (qunarId.trim().equals(el.getQunarId().trim())) {
								System.out.println("--" + (++count) + "--去哪ID--"+ lshotel.getName());
								lshotel.setZshotelid(el.getId());
								Server.getInstance().getHotelService().updateHotelIgnoreNull(lshotel);
								ids.add(el.getId());
								el.setPaytype(2l);
								el.setState(3);
								//Server.getInstance().getHotelService().updateHotelallIgnoreNull(el);
								flag=false;
								continue label;
							}
						}
					}
				
				}
				if(false){
					Hotelall zs=new Hotelall();
					zs.setMixflag("1");
					zs.setQunarId(lshotel.getQunarId());
					zs.setRoomAmenities(lshotel.getRoomAmenities());
					zs.setAvailPolicy(lshotel.getAvailPolicy());
					zs.setEnname(lshotel.getEnname());
					zs.setStar(lshotel.getStar());
					zs.setHot(lshotel.getHot());
					zs.setRepair(lshotel.getRepair());
					zs.setName(lshotel.getName());
					zs.setProvinceid(lshotel.getProvinceid());
					zs.setCityid(lshotel.getCityid());
					zs.setRegionid1(lshotel.getRegionid1());
					zs.setRegionid2(lshotel.getRegionid2());
					zs.setAddress(lshotel.getAddress());
					zs.setDescription(lshotel.getDescription());
					zs.setType(lshotel.getType());
					zs.setRooms(lshotel.getRooms());
					zs.setFootitem(lshotel.getFootitem());
					zs.setServiceitem(lshotel.getServiceitem());
					zs.setMeetingitem(lshotel.getMeetingitem());
					zs.setPlayitem(lshotel.getPlayitem());
					zs.setCarttype(lshotel.getCarttype());
					zs.setRepaildate(lshotel.getRepaildate());
					zs.setNearhotel(lshotel.getNearhotel());
					zs.setPostcode(lshotel.getPostcode());
					zs.setState(lshotel.getState());
					zs.setPyname(lshotel.getPyname());
					zs.setJpname(lshotel.getJpname());
					zs.setMainfloor(lshotel.getMainfloor());
					zs.setOpendate(lshotel.getOpendate());
					zs.setFaxdesc(lshotel.getFaxdesc());
					zs.setMarkettell(lshotel.getMarkettell());
					zs.setTortell(lshotel.getTortell());
					zs.setPrespec(lshotel.getPrespec());
					zs.setAppendlever(lshotel.getAppendlever());
					zs.setMainlevel(lshotel.getMainlevel());
					zs.setStatedesc(lshotel.getStatedesc());
					zs.setSellpoint(lshotel.getSellpoint());
					zs.setFullname(lshotel.getFullname());
					zs.setFax1(lshotel.getFax1());
					zs.setFax2(lshotel.getFax2());
					zs.setLat(lshotel.getLat());
					zs.setLng(lshotel.getLng());
					zs.setLanguage(lshotel.getLanguage());
					zs.setAirportservice(lshotel.getAirportservice());
					zs.setTrafficinfo(lshotel.getTrafficinfo());
					zs.setChaininfoid(lshotel.getChaininfoid());
					zs.setLowestPrice(lshotel.getLowestPrice());
					System.out.println("创建新的酒店");
					zs=Server.getInstance().getHotelService().createHotelall(zs);
					lshotel.setZshotelid(zs.getId());
					Server.getInstance().getHotelService().updateHotelIgnoreNull(lshotel);
					System.out.println("更新临时的酒店");
				}
			}
		}
		}
}
