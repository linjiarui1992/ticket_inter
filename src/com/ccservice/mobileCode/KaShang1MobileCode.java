/**
 * 
 */
package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;

/**
 * 卡商1
 * QQ 250616068;527111205
 * 
 * @time 2015年9月26日 下午12:54:13
 * @author chendong
 */
public class KaShang1MobileCode implements IMobileCode {
    //获取手机号码
    final static String GETMOBILENUMURL = "http://tc12306smsmsg.hangtian123.com/SelectCellPhoneInfo?action=getMobilenum&agentid=1";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://tc12306smsmsg.hangtian123.com/SelectCellPhoneInfo?action=getVcodeAndReleaseMobile";

    //加黑手机号码
    final static String ADDIGNORELISTURL = "http://tc12306smsmsg.hangtian123.com/SelectCellPhoneInfo?action=addIgnoreList";

    /**
     * 
     * 
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static KaShang1MobileCode getInstance(String pid) {
        //        String pid = "4036";//12306项目项目编号
        String uid = "";//12306
        KaShang1MobileCode kaShang1MobileCode = new KaShang1MobileCode();
        return kaShang1MobileCode;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String, java.lang.String)
     */
    @Override
    public String LoginIn(String uid, String pwd) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        String paramContent = "";
        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("TongHangMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        String[] results = result.split("[|]");
        if (results.length == 2) {
            result = results[0];
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#getVcodeAndReleaseMobile(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String paramContent = "mobile=" + mobile;
        String result = "-1";
        result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
        WriteLog.write("TongHangMobileCode_getVcodeAndReleaseMobile",
                paramContent + ":getVcodeAndReleaseMobile:" + result);
        if (result.contains("not_receive")) {
            result = "no_data";
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        String paramContent = "&mobile=" + mobiles;
        String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("TongHangMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
