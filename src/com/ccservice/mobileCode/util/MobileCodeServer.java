/**
 * 
 */
package com.ccservice.mobileCode.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @time 2015年10月15日 下午4:43:28
 * @author chendong
 */
public class MobileCodeServer {

    public static String qtdsCookieString;

    /**
     * key 手机号
     * value 订单号
     */
    public static HashMap<String, String> mapQtdsMobile2OrderId = new HashMap<String, String>();

    /**
     * 齐天大圣的获取号码的订单id
     */
    public static String qtdsoid;

    /*
     * 放齐天大圣获取到的手机号
     */
    public static List<String> mapQtdsMobileList = new ArrayList<String>();

}
