/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;

/**
 * 同程的验证码接口
 * @time 2015年12月3日15:17:31
 * @author chendong
 */
public class TongChengMobileCode implements IMobileCode {
    public static void main(String[] args) {
        String result = "";
        String mobile = "";
        TongChengMobileCode tongChengMobileCode = TongChengMobileCode.getInstance("");
        mobile = tongChengMobileCode.GetMobilenum("", "", "");
        //                System.out.println(mobile);
        //        result = tongChengMobileCode.send(mobile, "999");
        //        main_getVcodeAndReleaseMobile(tongChengMobileCode, mobile);//获取验证码
        //        System.out.println(result);
        //        result = tongChengMobileCode.AddIgnoreList("", mobile, "", "");
        //        tongChengMobileCode.ReleaseMobile("", "13468761853", "", "");
        //        mobile = tongChengMobileCode.GetMobilenum("", "", "");
        //        System.out.println(mobile);
        //        System.out.println(DBHelperTongchengMobile.GetSingle(" exec sp_TongChengPhoneSim_getOneMobileNoToReg "));
        //        System.out.println(DBHelperTongchengMobile.GetSingle(" exec sp_TongChengPhoneSim_getOneMobileNoToRegTest"));
        //        System.out.println(DBHelperTongchengMobile.GetSingle(" exec sp_TongChengPhoneSim_getOneMobileNoToRegTest"));
        String mobileString = "17355648668";
        long l1 = System.currentTimeMillis();
        tongChengMobileCode.insertDb(mobileString, l1);
    }

    /**
     * 
     * 
     * @param TongChengMobileCode
     * @param mobile
     * @time 2015年12月3日 下午3:41:17
     * @author chendong
     */
    private static void main_getVcodeAndReleaseMobile(TongChengMobileCode TongChengMobileCode, String mobile) {
        for (int i = 0; i < 200; i++) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception e) {
            }
            String vocde_mobile = TongChengMobileCode.getVcodeAndReleaseMobile("", mobile, "", "", "");
            System.out.println(i + ":" + mobile + ":" + vocde_mobile);
            if ("no_data".equals(vocde_mobile)) {
                continue;
            }
            else {
                break;
            }
        }
    }

    TongChengSmsMethod tongChengSmsMethod;

    public TongChengMobileCode() {
        super();
        String channel = "hs";
        String password = "Btdvg0rRtBQBc1Q7";
        tongChengSmsMethod = new TongChengSmsMethod(channel, password);
    }

    /**
     * 
     * 
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static TongChengMobileCode getInstance(String pid) {
        //        String pid = "4036";//12306项目项目编号
        String uid = "";//12306
        TongChengMobileCode TongchengMobileCode = new TongChengMobileCode();

        return TongchengMobileCode;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String, java.lang.String)
     */
    @Override
    public String LoginIn(String uid, String pwd) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        String result = "";
        long l1 = System.currentTimeMillis();
        //String result = "17730090265";
        try {
            Object MobileObject = DBHelperTongchengMobile.GetSingle(" exec sp_TongChengPhoneSim_getOneMobileNoToReg ");
            if (MobileObject != null) {
                result = (String) MobileObject;
            }
            WriteLog.write("TongChengMobileCode_GetMobilenum", l1 + ":fromDB:" + result);
        }
        catch (Exception e) {
        }

        if (result == null || "".equals(result) || result.length() != 11) {
            result = tongChengSmsMethod.get();
            WriteLog.write("TongChengMobileCode_GetMobilenum", l1 + ":fromtc:" + result);
            if (result != null && result.length() == 11) {
                insertDb(result, l1);
            }
        }
        WriteLog.write("TongChengMobileCode_GetMobilenum", l1 + ":result:" + result);
        return result;
    }

    private void insertDb(String Mobile, long l1) {
        boolean isSuccess = true;
        String sql = " exec sp_TongChengPhoneSim_Insert @PhoneNumber = N'" + Mobile + "'";
        try {
            for (int i = 0; i < 10; i++) {
                isSuccess = DBHelperTongchengMobile.executeSql(sql);
                if (isSuccess) {
                    Thread.sleep(5000L);
                    continue;
                }
                else {
                    break;
                }
            }
            WriteLog.write("TongChengMobileCode_GetMobilenum", l1 + ":" + isSuccess + ":fromDB:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("TongChengMobileCode_GetMobilenum_insert_err", l1 + ":" + e.getMessage() + ":fromDB:" + sql);
        }
    }

    private void addBlack(String PhoneNumber, int Status) {
        try {
            DBHelperTongchengMobile.executeSql(" exec sp_TongChengPhoneSim_updateStatus @PhoneNumber='" + PhoneNumber
                    + "',@Status = " + Status);
        }
        catch (Exception e) {
            // TODO: handle exception
        }
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#getVcodeAndReleaseMobile(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String result = tongChengSmsMethod.query(mobile);
        WriteLog.write("TongChengMobileCode_getVcodeAndReleaseMobile", mobile + ":" + result);
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */

    /**
     * token  "0".equals(token)  注册失败               0注册失败，1注册成功
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        addBlack(mobiles, 2);
        String register = "1";
        if (token != null && token.startsWith("0")) {
            register = "0";
        }
        else if (token.contains("weishoudao")) {
            register = "2";
        }
        else {
            register = "1";
        }
        String remark = "";

        //        register 0注册失败，1注册成功
        //        remark备注  "无法注册"

        String result = tongChengSmsMethod.update(mobiles, register, remark);
        WriteLog.write("TongChengMobileCode_AddIgnoreList", mobiles + ":" + result);
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        String result = tongChengSmsMethod.send(mobile, content);
        WriteLog.write("TongChengMobileCode_send", mobile + ":" + result);
        return result;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        //        String paramContent = "";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", "FREE_SIM_CARD");
        jsonObject.put("phonenumber", mobiles);
        String paramContent = "jsonStr=" + jsonObject.toJSONString();// 
        //        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        String result = "";
        WriteLog.write("TongChengMobileCode_ReleaseMobile", paramContent + ":GetMobilenum:" + result);
        JSONObject jsonObject2 = JSONObject.parseObject(result);
        if ("100".equals(jsonObject2.getString("code"))) {
            boolean content1 = jsonObject2.getBooleanValue("success");
            if (content1) {
                result = "true";
            }
            else {
                result = "false";
            }
        }
        else {
            result = "false";
        }
        return result;
    }

}
