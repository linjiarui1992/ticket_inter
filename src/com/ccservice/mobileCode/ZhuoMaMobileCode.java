/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 卓码
 * http://api.zmyzm.com
 * @time 2015年9月14日 下午1:58:11
 * @author chendong
 */
public class ZhuoMaMobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "4799";//12306项目项目编号
        String uid = "";//12306
        String author_uid = "cd1989929";//开发者用户名
        String author_pwd = "cd1989929123";//开发者用户名
        ZhuoMaMobileCode zhuoMaMobileCode = new ZhuoMaMobileCode(pid, uid, author_uid, author_pwd);
        String mobile = zhuoMaMobileCode.GetMobilenum(pid, author_uid, "");
        System.out.println("mobile:" + mobile);
        String result = "";
        //        System.out.println(yanzhengma);
        //        String black = zhuoMaMobileCode.AddIgnoreList(author_uid, mobile, pid, "");
        //        System.out.println(black);
        result = zhuoMaMobileCode.send(mobile, "999");
        System.out.println(result);

        //        for (int i = 0; i < args.length; i++) {
        //        result = zhuoMaMobileCode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        //            if ("no_data".equals(result)) {
        //                continue;
        //            }
        //            else {
        //                System.out.println(result);
        //                break;
        //            }
        //        }
        //添加黑名单
        //        result = zhuanMamobilecode.AddIgnoreList("", mobile, "", "");
        //获取当前用户正在使用的号码列表
        //        System.out.println(result);

    }

    /**
     * 
     * 
     * @param pid 8403(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static ZhuoMaMobileCode getInstance(String pid) {
        //        String pid = "8403";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("ZhuoMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("ZhuoMaAuthor_pwd", "MobileCode.properties");//"cd1989929123";//开发者用户名
        ZhuoMaMobileCode zhuomamobilecode = new ZhuoMaMobileCode(pid, uid, author_uid, author_pwd);
        return zhuomamobilecode;
    }

    //卓码平台登陆
    final static String LOGININURL = "http://api.zmyzm.com/apiGo.do?action=loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://api.zmyzm.com/apiGo.do?action=getMobilenum";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://api.zmyzm.com/apiGo.do?action=getVcodeAndReleaseMobile";

    //加黑手机号码
    final static String ADDIGNORELISTURL = "http://api.zmyzm.com/apiGo.do?action=addIgnoreList";

    String pid;//项目编号

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public ZhuoMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = PropertyUtil.getValue("ZhuoMaAuthor_pid", "MobileCode.properties");
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        //        this.token = LoginIn(author_uid, author_pwd);
        this.token = PropertyUtil.getValue("ZhuoMaAuthor_token", "MobileCode.properties");
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 优码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        if (uid == null || pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + uid + "&pwd=" + pwd;
            String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("ZhuoMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            String[] results = result.split("[|]");
            if (results.length == 2) {
                result = results[1];
            }
            return result;
        }
    }

    /**
     * 
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String pid, String uid, String token) {
        if (this.author_uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String[] results = null;
            String pids = PropertyUtil.getValue("ZhuoMaAuthor_pids", "MobileCode.properties");
            String[] pidss = pids.split(",");
            String paramContent;
            String result = "no_data";// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            for (int i = 0; i < pidss.length; i++) {
                this.pid = pidss[i];
                paramContent = "pid=" + this.pid + "&uid=" + this.author_uid + "&token=" + this.token
                        + "&mobile=&size=1";
                result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();
                System.out.println(result + ":" + this.pid);
                WriteLog.write("ZhuoMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
                if ("no_data".equals(result)) {
                    continue;
                }
                else {
                    results = result.split("[|]");
                    if (results.length == 2) {
                        result = results[0];
                    }
                    break;
                }
            }
            return result;
        }
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&token=" + this.token + "&mobile=" + mobile;
            String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "mobile=" + mobile + "&token=" + this.token + "&uid=" + this.author_uid
                    + "&author_uid=" + this.author_uid;
            String result = "-1";
            result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
            WriteLog.write("ZhuoMaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":getVcodeAndReleaseMobile:" + result);
            if (result.contains("not_receive")) {
                result = "no_data";
            }
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (mobiles == null) {
            return "mobiles参数传入有误zhuoma";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&token=" + this.token + "&pid=" + this.pid + "&mobiles="
                    + mobiles;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            System.out.println("AddIgnoreList.pid:" + this.pid + ":" + result);
            WriteLog.write("ZhuoMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            //            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            //            WriteLog.write("ZhuoMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return "";
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    @Override
    public String send(String mobile, String content) {
        if (mobile == null) {
            return "mobile参数传入有误zhuomasend";
        }
        else {
            //            ?action=send&uid=用户名&token=登录时返回的令牌&pid=项目ID&mobile=号码&content=短信内容
            String paramContent = "action=send&uid=" + this.author_uid + "&token=" + this.token + "&pid=" + this.pid
                    + "&mobile=" + mobile + "&content=" + content;
            String result = SendPostandGet.submitPost("http://api.zmyzm.com/apiGo.do", paramContent, "utf-8")
                    .toString();
            System.out.println("send.pid:" + this.pid + ":" + result);
            WriteLog.write("ZhuoMaMobileCode_send", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }
}
