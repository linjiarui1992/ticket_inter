package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hmhotelprice.PriceResult;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
/**
 * 
 * @author wzc
 * 华闽，去哪比价实现，数据插入优势数据库
 *
 */
public class TestHMComPrice extends Utils {
	// 城市id
	private String cityid = "";
	// 入住日期
	private String startDate = "";
	// 离店日期
	private String endDate = "";
	// 本地城市id
	private Long profit = 20l;
	//在去哪比其他代理商低1元
	private Long lower=1l;
	// 返回去哪对应的数据集合
	private Map<String, List<HotelData>> roomDatas;
	// 返回的酒店集合
	private List<String> roomnames;
	// 返回酒店列表qunar数据集合
	private Map<Long, Map<String, List<HotelData>>> qunarDatas;

	private Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas;

	private Map<Long, PriceResult> dayHMData = new HashMap<Long, PriceResult>();
	
	ComProcPublic pro=null;

	public TestHMComPrice() {
		pro=new ComProcPublic();
	}
	public static void main(String[] args) throws Exception {
		new TestHMComPrice().getHotelDatas();
	}
		
	public void getHotelDatas() throws Exception {
		while(true){
			String where =PropertyUtil.getValue("sql");
			List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(where, "order by id desc ", -1, 0);
			if (hotels.size() > 0) {
				int i=hotels.size();
				for (Hotel hotel : hotels) {
					System.out.println("剩余更新数量:"+i--);
					updateOneHotel(hotel);
				}
			}
		}
	}
	/**
	 * 更新单个酒店
	 * @param hotel
	 * @param startt
	 * @param endt
	 * @throws Exception
	 */	
	public void updateOneHotel(Hotel hotel) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar start = GregorianCalendar.getInstance();
		start.add(Calendar.DAY_OF_MONTH, 0);
		Calendar end = GregorianCalendar.getInstance();
		end.add(Calendar.DAY_OF_MONTH,1);
		dayqunardatas = new HashMap<String, Map<Long, Map<String, List<HotelData>>>>();
//		pro.loadroomtype(hotel, start.getTime(), end.getTime(), dayHMData);
		List<HotelGoodData> hotelGoodDataList= Server.getInstance().getIHMHotelService().getQrateHotelData(hotel.getHotelcode(), start.getTime(),  end.getTime(), "", "", "", "", "", "", "");

		System.out.println(hotel.getHotelcode()+"----"+hotelGoodDataList);
		
		if(hotelGoodDataList!=null&&hotelGoodDataList.size()>0){
//			while (start.before(end)) {
//				qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
//				loadQunarData(hotel, start.getTime());
//				dayqunardatas.put(sdf.format(start.getTime()), qunarDatas);
//				start.add(Calendar.DAY_OF_MONTH, 1);
//			}
//			
//			pro.comparequnar(dayqunardatas,hotelGoodDataList,profit,lower);
			
			//pro.comparequnar(pro.loadQunarData(hotel, start.getTime(), end.getTime()),hotelGoodDataList,profit,lower,"3");
		}
	}



	// http://hotel.qunar.com/price/detail.jsp?fromDate=2012-08-14&toDate=2012-08-15&cityurl=beijing_city&HotelSEQ=beijing_city_377
	// http://hotel.qunar.com/price/detail.jsp?fromDate=2012-08-11&toDate=2012-08-12&cityurl=shanghai_city&HotelSEQ=shanghai_city_534
	// 加载去哪酒店数据
	public void loadQunarData(Hotel hotel, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String citycode = "";
		citycode = hotel.getQunarId().substring(0,
				hotel.getQunarId().lastIndexOf("_"));
		String url = "http://hotel.qunar.com/price/detail.jsp?fromDate="
				+ sdf.format(date) + "&toDate=" + sdf.format(cal.getTime())
				+ "&cityurl=" + citycode + "&HotelSEQ="
				+ hotel.getQunarId().trim();
//		System.out.println(url);
		String str = HttpClient.httpget(url, "utf-8");
		roomnames = new ArrayList<String>();
		if (str!=null&&!str.equals("")) {
			String key = str.trim();
			if (!key.equals("")) {
				String strstr = key.substring(key.indexOf("(") + 1, key.trim().indexOf("/*"));
				JSONObject datas = JSONObject.fromObject(strstr + "}");
				JSONObject result = datas.getJSONObject("result");
				Iterator<String> strs = result.keys();
				HotelData hoteldata;
				roomDatas = new HashMap<String, List<HotelData>>();
				while (strs.hasNext()) {
					hoteldata = new HotelData();
					String keys = strs.next();
					JSONArray arra = JSONArray.fromObject(result.get(keys));
					if (arra.get(0) != null) {
						hoteldata.setSealprice(Integer.parseInt(arra.get(0)
								.toString()));
					}
					hoteldata.setAgentNo(keys.substring(0, keys.trim().indexOf(
							"|")));
					hoteldata.setHotelid(hotel.getId() + "");
					hoteldata.setHotelname(hotel.getName());
					if (arra.get(2) != null) {
						String strtemp = arra.getString(2);
						hoteldata.setRoomnameBF(getString(strtemp));
					}
					// 开关房
					if (arra.get(9) != null) {
						String strtemp = arra.getString(9);
						hoteldata.setRoomstatus(Integer.parseInt(strtemp));
					}
					// 现预付
					if (arra.get(14) != null) {
						String strtemp = arra.getString(14);
						hoteldata.setType(Integer.parseInt(strtemp));
					}
					if (arra.get(3) != null) {
						hoteldata.setRoomname(arra.getString(3));
						if (!roomnames.contains(arra.getString(3))) {
							roomnames.add(arra.getString(3));
						}
						if (roomDatas.containsKey(arra.getString(3))) {
							List<HotelData> datastemp = roomDatas.get(arra
									.getString(3));
							datastemp.add(hoteldata);
							Collections.sort(datastemp);
							roomDatas.put(arra.getString(3), datastemp);
						} else {
							List<HotelData> hoteldatas = new ArrayList<HotelData>();
							hoteldatas.add(hoteldata);
							Collections.sort(hoteldatas);
							roomDatas.put(arra.getString(3), hoteldatas);
						}
					}
				}
				qunarDatas.put(hotel.getId(), roomDatas);
				JSONObject detailBasic = datas.getJSONObject("detailBasic");
				JSONObject vendors = detailBasic.getJSONObject("vendors");
				Iterator<String> names = vendors.keys();
				Map<String, String> agentnos = new HashMap<String, String>();
	//			 while (names.hasNext()) {
	//			 String name = names.next();
	//			 JSONObject obj = vendors.getJSONObject(name);
//				 if (!session.containsKey("name")) {
//				 session.put(name, obj.get("name"));
//				 System.out.println("name:" + name + "," + "id:"
//				 + obj.get("name"));
//				 }
//				 if (!agentnos.containsKey(name)) {
//				 agentnos.put(name, obj.get("name").toString());
//				 }
//				 }
				// Set<String> keys= agentnos.keySet();
				// for (String k : keys) {
				// WriteLog.write("去哪代理商大全", k+" - "+agentnos.get(k));
				// }

			}
		}

	}




	// 根据床型id查找房名

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}


	public Map<String, List<HotelData>> getRoomDatas() {
		return roomDatas;
	}

	public void setRoomDatas(Map<String, List<HotelData>> roomDatas) {
		this.roomDatas = roomDatas;
	}

	public List<String> getRoomnames() {
		return roomnames;
	}

	public void setRoomnames(List<String> roomnames) {
		this.roomnames = roomnames;
	}


	public Map<Long, Map<String, List<HotelData>>> getQunarDatas() {
		return qunarDatas;
	}

	public void setQunarDatas(Map<Long, Map<String, List<HotelData>>> qunarDatas) {
		this.qunarDatas = qunarDatas;
	}


	public Long getProfit() {
		return profit;
	}

	public void setProfit(Long profit) {
		this.profit = profit;
	}



	public Map<String, Map<Long, Map<String, List<HotelData>>>> getDayqunardatas() {
		return dayqunardatas;
	}

	public void setDayqunardatas(
			Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas) {
		this.dayqunardatas = dayqunardatas;
	}

	public Map<Long, PriceResult> getDayHMData() {
		return dayHMData;
	}

	public void setDayHMData(Map<Long, PriceResult> dayHMData) {
		this.dayHMData = dayHMData;
	}


	public Long getLower() {
		return lower;
	}


	public void setLower(Long lower) {
		this.lower = lower;
	}

}
