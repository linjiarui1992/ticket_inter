package com.ccservice.compareprice;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.util.Util;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 
 * @author wzc 华闽最新更新数据方式
 * 
 */
public class HmFileParse {
	
	public HmFileParse() {
		
	}
	private Float profit3 = 0.025f;//1-1000  3个点
	private Float profit25 = 0.02f;//1000-2000 2.5个点
	private Float profit2 = 0.015f;//2000以上  2 个点
	
	public Long getSealPrice(Long baseprice){
		int fee=0;
		if(baseprice<=1000){
			//fee=(int)(baseprice*0.01+baseprice*profit3)+12;
			fee=23;
		}else if(baseprice>1000&&baseprice<=2000){
			//fee=(int)(baseprice*0.01+baseprice*profit25)+12;
			fee=25;
		}else if(baseprice>2000){
			//fee=(int)(baseprice*0.01+baseprice*profit2)+12;
			fee=30;
		}
		return baseprice+fee;
	}
	public void test(){
		new HmFileParse().parselockfile();
		new HmFileParse().parseUpdateprice();
	}
	public void parselockfile(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		File files = new File("D:\\酒店价格\\qunar");
		File over= new File("D:\\酒店价格\\qunar\\over\\"+sdf.format(new Date()));
		File exp= new File("D:\\酒店价格\\qunar\\exp\\"+sdf.format(new Date()));
		if(!over.exists()){
			over.mkdirs();
		}
		if(!exp.exists()){
			exp.mkdirs();
		}
		if (files.exists()) {
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_PUSHBLOCKHT_") == 0) {
						return true;
					}
					return false;
				}
			});
			for (File f : file) {
				SAXBuilder sb = new SAXBuilder();
				try {
					Document doc=sb.build(f);
					Util.copyfile(f, new File(over,f.getName()));
					f.delete();
					
					Element root=doc.getRootElement();
					Element contracts=root.getChild("CONTRACTS");
					List<Element> hotels=contracts.getChildren("CONTRACT_LIST");
					for (Element temp : hotels) {
						String htcode=temp.getChildText("HT_CODE");
						String startdate=temp.getChildText("FROM_DATE");
						String todata=temp.getChildText("TO_DATE");
						Server.getInstance().getSystemService().findMapResultBySql(
								"delete from t_hotelgooddata where c_hotelid in " +
								"(select id from t_hotel where c_sourcetype=3 and c_hotelcode='"+htcode+"')  and C_DATENUM>='"+startdate+"' and C_DATENUM<='"+todata+"'", null);
						System.out.println("ov^^^^^"+htcode);
					}
				} catch (Exception e) {
					e.printStackTrace();
					Util.copyfile(new File(over,f.getName()), new File(exp,f.getName()));
				}
			}
		}
	}
	
	/**
	 * 更新价格数据/解析房态变化文件
	 */
	public void parseUpdateprice(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		File files = new File("D:\\酒店价格\\qunar");
		File over= new File("D:\\酒店价格\\qunar\\over\\"+sdf.format(new Date()));
		File exp= new File("D:\\酒店价格\\qunar\\exp\\"+sdf.format(new Date()));
		if(!over.exists()){
			over.mkdirs();
		}
		if(!exp.exists()){
			exp.mkdirs();
		}
		Calendar cal=Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 28);
		if (files.exists()) {
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_PUSHRATE_CN00839_") == 0||filename.indexOf("HMC_PUSHALLOT_") == 0) {
						return true;
					}
					return false;
				}
			});
			if (file.length > 0) {
				Arrays.sort(file, new Comparator<File>(){
					@Override
					public int compare(File file1, File file2) {
						// TODO Auto-generated method stub
						String s="";
						String s1="";
						if (file1.getName().indexOf("HMC_PUSHRATE_CN00839_") == 0){
							String[] str=file1.getName().split("_");
							s=str[5]+str[6];
						}else{
							String[] str=file1.getName().split("_");
							s=str[4]+str[5];
						}
						if(file2.getName().indexOf("HMC_PUSHALLOT_") == 0){
							String[] str=file2.getName().split("_");
							s1=str[4]+str[5];
						}else{
							String[] str=file2.getName().split("_");
							s1=str[5]+str[6];
						}
						return s.compareTo(s1);
					}
					
				});
				for (File f : file) {
					System.out.println("开始更新此文件："+f.getName());
					if(f.getName().indexOf("HMC_PUSHRATE_CN00839_") == 0){
						SAXBuilder sb = new SAXBuilder();
						try {
							Document doc=sb.build(f);
							
							Util.copyfile(f, new File(over,f.getName()));
							f.delete();
							
							System.out.println("删除");
							Element root=doc.getRootElement();
							Element result=root.getChild("XML_RESULT");
							if(result!=null){
								List<Element> options=result.getChildren();
								if(options!=null&&options.size()>0){
									for (Element opt : options){
										String optname=opt.getName();
										if(optname.equals("DELETE")){
											parseDom(opt, optname);
										}
									}
									for (Element opt : options) {
										String optname=opt.getName();
										if(optname.equals("INSERT")){
											parseDom(opt, optname);
										}else if(optname.equals("UPDATE")){
											parseDom(opt, optname);
										}
									}
								}
							}
						}catch (FileNotFoundException e) {
							System.out.println("文件找不到了……");
						}catch (Exception e) {
							e.printStackTrace();
							Util.copyfile(new File(over,f.getName()), new File(exp,f.getName()));
						}
					
					}else{
						SAXBuilder sb = new SAXBuilder();
						try {
							SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
							Util.copyfile(f, new File(over,f.getName()));
							Document doc = sb.build(f);
							
							System.out.println("已经复制过去了……");
							f.delete();
							
							Element root = doc.getRootElement();
							Element xmlresult = root.getChild("XML_RESULT");
							Element update = xmlresult.getChild("UPDATE");
							String contract = update.getChildText("CONTRACT");
							String ver = update.getChildText("VER");
							String hotelID = update.getChildText("HOTEL");
							String cur = update.getChildText("CUR");
							List<Element> products = update.getChildren("PRODUCT");
							for (Element product : products) {
								String prod = product.getChildText("PROD");
								List<Element> rooms = product.getChildren("ROOM");
								for (Element room : rooms) {
									String cat = room.getChildText("CAT");
									String type = room.getChildText("TYPE");
									if (!type.equals("S")) {
										String serv = room.getChildText("SERV");
										String bf = room.getChildText("BF");
										List<Element> stays = room.getChildren("STAY");
										for (Element stay : stays) {
											String statedate = stay.getChildText("STAYDATE");
											String date = sdf.format(sd.parse(statedate));
											if(sd.parse(statedate).before(cal.getTime())){
												System.out.println("更新房态……");
												String isallot = stay.getChildText("IS_ALLOT");
												String str="[dbo].[updateAllot]@hotelid = N'"+hotelID+"'," +
												"@contractid = N'"+contract+"'," +
												"@contractver = N'"+ver+"'," +
												"@prodid = N'"+prod+"'," +
												"@cat = N'"+cat+"'," +
												"@type = N'"+type+"'," +
												"@bf = "+bf+"," +
												"@date = N'"+date+"'," +
												"@isallot = N'"+isallot+"'";
												System.out.println(str);
												synchronized (this) {
													List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
												}
											}
										}
									}
								}
							}
						}catch (FileNotFoundException e) {
							System.out.println("文件找不到了……");
						}catch (Exception e) {
							System.out.println("异常复制……………………");
							Util.copyfile(new File(over,f.getName()), new File(exp,f.getName()));
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * 文档临时节点解析
	 * @throws Exception 
	 * 
	 */
	public void parseDom(Element opt,String typeopt) throws Exception{
		Calendar cal=Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 28);
		SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String contract = opt.getChildText("CONTRACT");
		String ver = opt.getChildText("VER");
		String hotelID = opt.getChildText("HOTEL");
		Hotel hotel=null;
		List<HotelGoodData> gooddatas=new ArrayList<HotelGoodData>();
		List<Hotel> hotels=Server.getInstance().getHotelService().findAllHotel("where c_hotelcode='"+hotelID+"' and id in (select distinct id from t_hotel where c_sourcetype=3 and c_push=1 and c_state=3)", "", -1, 0);
		if(hotels.size()>0){
			hotel=hotels.get(0);
			String cur = opt.getChildText("CUR");
			String hotelname=opt.getChildText("HOTELNAME");
			List<Element> products=opt.getChildren("PRODUCT");
			for (Element product : products) {
				String prod=product.getChildText("PROD");
				String rorate=product.getChildText("RORATE");
				String nation=product.getChildText("NATION");
				if(!nation.equals("ALL")){
					continue;
				}
				String nationname=product.getChildText("NATIONNAME");
				String min=product.getChildText("MIN");
				String max=product.getChildText("MAX");
				String advance=product.getChildText("ADVANCE");
				String ticket=product.getChildText("TICKET");
				String cancel=product.getChildText("CANCEL");
				String cutoff=product.getChildText("CUTOFF");
				List<Element> rooms=product.getChildren("ROOM");
				for (Element room : rooms) {
					String cat=room.getChildText("CAT");
					String type=room.getChildText("TYPE");
					if(type.equals("S")){
						continue;
					}
					String serv=room.getChildText("SERV");
					Roomtype roomtype=null;
					String where="where c_hotelid="+hotel.getId()+" and C_ROOMCODE='"+cat+"' and  C_BED in (select ID from T_BEDTYPE where C_TYPE='"+type+"')";
					List<Roomtype> roomtypes=Server.getInstance().getHotelService().findAllRoomtype(where, "", -1, 0);
					if(roomtypes.size()>0){
						roomtype=roomtypes.get(0);
						String bedypename="";
						try {
							bedypename=Server.getInstance().getHotelService().findBedtype(roomtype.getBed()).getTypename();
						} catch (Exception e) {
							
						}
						String bf=room.getChildText("BF");
						String deadline=room.getChildText("DEADLINE");
						List<Element> stays=room.getChildren("STAY");
						for (Element stay : stays) {
							String staydate=stay.getChildText("STAYDATE");
							String date=sdf.format(sd.parse(staydate));
							if(sd.parse(staydate).before(cal.getTime())){
								if(typeopt.equals("INSERT")||typeopt.equals("UPDATE")){
									HotelGoodData gooddata=new HotelGoodData();
									String price=stay.getChildText("PRICE");
									gooddata.setHotelid(hotel.getId());
									gooddata.setHotelname(hotel.getName());
									gooddata.setRoomtypeid(roomtype.getId());
									gooddata.setBedtypeid(roomtype.getBed().longValue());
									if(roomtype.getQunarname()!=null){
										gooddata.setQunarName(roomtype.getQunarname());
									}
									gooddata.setCityid(hotel.getCityid()+"");
									gooddata.setRoomtypename(roomtype.getName()+bedypename);
									gooddata.setBaseprice(Long.parseLong(price));
									gooddata.setSealprice(getSealPrice(Long.parseLong(price)));
									gooddata.setShijiprice(getSealPrice(Long.parseLong(price)));
									gooddata.setDatenum(date);
									gooddata.setBeforeDay(Long.parseLong(advance));
									gooddata.setMinday(Long.parseLong(min));
									String allot=stay.getChildText("ALLOT");
									gooddata.setAllot(allot);
									String isallot=stay.getChildText("IS_ALLOT");
									if("Y".equals(isallot)){
										gooddata.setYuliunum(1L);// IS_ALLOT  为Y时 预留房数目设置1  
									}  else {//满房
										gooddata.setYuliunum(0l);
									}
									gooddata.setRoomstatus(0l);
									gooddata.setSorucetype("3");
									gooddata.setContractid(contract);
									gooddata.setContractver(ver);
									gooddata.setBfcount(Long.parseLong(bf));
									gooddata.setProdid(prod);
									gooddata.setRoomflag("-1");
									gooddata.setProfit(gooddata.getShijiprice()-gooddata.getBaseprice());
									gooddatas.add(gooddata);
								}else if(typeopt.equals("DELETE")){
									String str="[dbo].[deleteHmData]@hotelid = N'"+hotel.getId()+"'," +
									"@contractid = N'"+contract+"'," +
									"@contractver = N'"+ver+"'," +
									"@prodid = N'"+prod+"'," +
									"@cat = N'"+roomtype.getId()+"'," +
									"@bf = "+bf+"," +
									"@date = N'"+date+"'";
									synchronized (this) {
										List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
									}
								}
							}
							}
					}
				}
		}
		}
	 writeDataDB(gooddatas, "3");
	
	}
	public void writeDataDB(List<HotelGoodData> gooddata,String sorucetype){
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if("3".equals(sorucetype)){//华闽
		Set<Long> hotelids=new HashSet<Long>();
		int version=0;
		for (int i = 0; i < gooddata.size() ; i++) {
			HotelGoodData good=gooddata.get(i);
			if(!hotelids.contains(good.getHotelid())){
				hotelids.add(good.getHotelid());
				List versionids=Server.getInstance().getSystemService().findMapResultBySql("select MAX(C_HIDECLOSE) as C_HIDECLOSE from T_HOTELGOODDATA where C_HOTELID="+good.getHotelid(), null);
				if(versionids!=null&&versionids.size()>0){
					if(((Map)versionids.get(0)).get("C_HIDECLOSE")!=null){
						System.out.println("版本提升……");
						version=Integer.parseInt(((Map)versionids.get(0)).get("C_HIDECLOSE").toString());
						//version=version+1;
					}
				}
			}
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			Integer coloseopen=Integer.parseInt(PropertyUtil.getValue("openclose"));
			good.setOpenclose(coloseopen);
			good.setSorucetype("3");
			// 特殊处理
			if(good.getProfit()>0&&good.getProfit()<20){
				good.setRoomflag("2");
				 good.setRoomstatus(0l);//开房
				 good.setShijiprice(good.getBaseprice()+20);
				 good.setProfit(20l);
			}
			if(good.getProfit()>=20){
				good.setRoomflag("1");
			}
			if(good.getProfit()<=0){
				good.setRoomflag("-1");
				good.setShijiprice(good.getBaseprice()+20);
				good.setRoomstatus(0l);//开房
				good.setProfit(20l);
			}
			System.out.println(good);
			String str="[dbo].[sp_updateHmData] @hotelid = "+
			good.getHotelid()+",@hotelname = N'"+
			good.getHotelname()+"',@roomtypeid = "+
			good.getRoomtypeid()+",@roomtypename = N'"+
			good.getRoomtypename()+"',@shijiprice = "+
			good.getShijiprice()+",@baseprice = "+
			good.getBaseprice()+",	@sealprice = "+
			good.getSealprice()+",@profit = "+
			good.getProfit()+",@roomstatus = "+
			good.getRoomstatus()+",@yuliunum = "+
			good.getYuliunum()+",@datenum = N'"+
			good.getDatenum()+"',@minday = "+
			good.getMinday()+",@openclose = "+
			good.getOpenclose()+",@beforeday = "+
			good.getBeforeday()+",@contractid = N'"+
			good.getContractid()+"',@contractver = N'"+
			good.getContractver()+"',@prodid = N'"+
			good.getProdid()+"',@bfcount = "+
			good.getBfcount()+",@sorucetype = N'"+
			good.getSorucetype()+"',@agentname = N'"+
			good.getAgentname()+"',@updatetime = N'"+
			good.getUpdatetime()+"',@roomflag=N'"+
			good.getRoomflag()+"',@cityid=N'"+
			good.getCityid()+"',@bedtypeid="+
			good.getBedtypeid()+",@version="+
			version+",@qunarname=N'"+
			good.getQunarName()+"',@allot="+
			good.getAllot();
			synchronized (this) {
				List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
			}
			}
		}else if("6".equals(sorucetype)){//捷旅
			Set<Long> hotelids=new HashSet<Long>();
			for (int i = 0; i < gooddata.size() ; i++) {
				HotelGoodData good=gooddata.get(i);
				good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
				good.setId(0l);
				if(hotelids.contains(good.getHotelid())){
				}else{
					hotelids.add(good.getHotelid());
					Server.getInstance().getSystemService().findMapResultBySql(
							"delete from T_HOTELGOODDATA where c_hotelid=" + good.getHotelid(), null);
					System.out.println("删除………………");
				}
				
				good.setOpenclose(1);
				good.setSorucetype("6");
				//特殊处理
				if(good.getProfit()!=null){
					if(good.getProfit()>0&&good.getProfit()<20){
						good.setRoomflag("2");
					}
					if(good.getProfit()>=20){
						good.setRoomflag("1");
					}
				}else{
					System.out.println(good.getProfit());
				}
				
				if(good.getShijiprice()<20){
					good.setShijiprice(0l);
				}
				System.out.println(good);
				String str="[dbo].[sp_insertgooddata] @hotelid = "+
				good.getHotelid()+",@hotelname = N'"+
				good.getHotelname()+"',@roomtypeid = "+
				good.getRoomtypeid()+",@roomtypename = N'"+
				good.getRoomtypename()+"',@shijiprice = "+
				good.getShijiprice()+",@baseprice = "+
				good.getBaseprice()+",	@sealprice = "+
				good.getSealprice()+",@profit = "+
				good.getProfit()+",@roomstatus = "+
				good.getRoomstatus()+",@yuliunum = "+
				good.getYuliunum()+",@datenum = N'"+
				good.getDatenum()+"',	@minday = "+
				good.getMinday()+",@openclose = "+
				good.getOpenclose()+",@beforeday = "+
				good.getBeforeday()+",@contractid = N'"+
				good.getContractid()+"',@contractver = N'"+
				good.getContractver()+"',@prodid = N'"+
				good.getProdid()+"',@bfcount = "+
				good.getBfcount()+",@sorucetype = N'"+
				good.getSorucetype()+"',@agentname = N'"+
				good.getAgentname()+"',@updatetime = N'"+
				good.getUpdatetime()+"',@roomflag=N'"+
				good.getRoomflag()+"',@cityid=N'"+
				good.getCityid()+"',@jlkeyid=N'"+
				good.getJlkeyid()+"',@jltime=N'"+
				good.getJltime()+"',@jlft=N'"+
				good.getJlft()+"',@jlf=N'"+
				good.getJlf()+"',@jlroomtypeid=N'"+
				good.getJlroomtypeid()+"',@allotmenttype=N'"+
				good.getAllotmenttype()+"',@ratetype=N'"+
				good.getRatetype()+"',@ratetypeid=N'"+
				good.getRatetypeid()+"'@qunarname=N'"+
				good.getQunarName()+"'";
				synchronized (this) {
					List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
				}
			}
		}else{
			System.out.println("来源不存在........");
		}
		
	}

	public Float getProfit3() {
		return profit3;
	}
	public void setProfit3(Float profit3) {
		this.profit3 = profit3;
	}
	public Float getProfit25() {
		return profit25;
	}
	public void setProfit25(Float profit25) {
		this.profit25 = profit25;
	}
	public Float getProfit2() {
		return profit2;
	}
	public void setProfit2(Float profit2) {
		this.profit2 = profit2;
	}
	
}
