package com.ccservice.compareprice.jielv;

import java.util.Map;
import java.util.List;
import java.util.Date;

import org.quartz.Job;

import java.util.HashMap;
import java.text.SimpleDateFormat;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class JlUpdatePriceForQunarJob implements Job {

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private int catchdays = Integer.parseInt(PropertyUtil.getValue("catchdays").trim());

    private int timeInterval = Integer.parseInt(PropertyUtil.getValue("jlUpdatePriceForQunarUpdateInterval").trim());

    private String updateDataTime = PropertyUtil.getValue("updateDataTime").trim();

    private int closeRoomFlag = Integer.parseInt(PropertyUtil.getValue("closeRoomFlag").trim());

    //private String ReloadQunarGetHotelInfo = PropertyUtil.getValue("ReloadQunarGetHotelInfo");

    public void execute(JobExecutionContext context) throws JobExecutionException {
        loadHotel();
    }

    public static void main(String[] args) {
        new JlUpdatePriceForQunarJob().loadHotel();
    }

    @SuppressWarnings("unchecked")
    private void loadHotel() {
        while (true) {
            try {
                //TIME
                Date current = format.parse(format.format(new Date()));
                Date start = format.parse(updateDataTime.split("-")[0]);
                Date end = format.parse(updateDataTime.split("-")[1]);
                boolean waitflag = false;
                if (current.before(start)) {
                    waitflag = true;
                    long wait = start.getTime() - current.getTime();
                    System.out.println("更新时间未到，等待" + wait / 1000 + "秒。");
                    //关房
                    if (closeRoomFlag == 1) {
                        String time = sdf.format(new Date());
                        String sql = "update t_hotel set c_goqunarupdatetime = '" + time + "' where c_push in (1,2)";
                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    }
                    Thread.sleep(wait);
                }
                if (current.after(end)) {
                    waitflag = true;
                    Date _24 = format.parse("24:00:00");
                    Date _00 = format.parse("00:00:00");
                    long wait = _24.getTime() - current.getTime() + start.getTime() - _00.getTime();
                    System.out.println("更新时间未到，等待" + wait / 1000 + "秒。");
                    //关房
                    if (closeRoomFlag == 1) {
                        String time = sdf.format(new Date());
                        String sql = "update t_hotel set c_goqunarupdatetime = '" + time + "' where c_push in (1,2)";
                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    }
                    Thread.sleep(wait);
                }
                //更新时间，开房
                if (waitflag && closeRoomFlag == 1) {
                    String time = sdf.format(new Date());
                    String sql = "update t_hotel set c_goqunarupdatetime = '" + time + "' where c_push in (1,2)";
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                }
            }
            catch (Exception e) {
            }
            //Start
            long start = System.currentTimeMillis();
            try {
                String sql = "h where h.C_PUSH = 2 and h.C_SOURCETYPE = 6 and h.C_HOTELCODE != '' and exists (select 'YES' from T_HOTELGOODDATA where C_HOTELID = h.ID)";
                List<Hotel> list = Server.getInstance().getHotelService().findAllHotel(sql, "", -1, 0);
                for (Hotel h : list) {
                    try {
                        loadJlPrice(h);
                    }
                    catch (Exception e) {
                        StackTraceElement ele = e.getStackTrace()[0];
                        System.out.println("酒店 [" + h.getName().trim() + " ]上去哪儿，更新数据异常：" + e.getMessage() + "，类名："
                                + ele.getFileName() + "，方法名：" + ele.getMethodName() + "，行数：" + ele.getLineNumber());
                    }
                }
            }
            catch (Exception e) {
                StackTraceElement ele = e.getStackTrace()[0];
                System.out.println("酒店上去哪儿，更新数据异常：" + e.getMessage() + "，类名：" + ele.getFileName() + "，方法名："
                        + ele.getMethodName() + "，行数：" + ele.getLineNumber());
            }
            finally {
                //更新缓存
                //SendPostandGet.submitGet(ReloadQunarGetHotelInfo).toString();
                //小于两分钟
                long end = System.currentTimeMillis();
                long interval = end - start;
                long time = timeInterval * 1000;
                if (interval < time) {
                    try {
                        Thread.sleep(time - interval);
                    }
                    catch (Exception ex) {
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void loadJlPrice(Hotel hotel) throws Exception {
        long hotelid = hotel.getId();
        //时间
        String startDate = ElongHotelInterfaceUtil.getCurrentDate();
        String endDate = ElongHotelInterfaceUtil.getAddDate(startDate, catchdays);
        //请求深捷旅
        List<JLPriceResult> jlResults = Server.getInstance().getIJLHotelService()
                .getHotelPriceByRoom(hotel.getHotelcode().trim(), "", "", startDate, endDate);
        //本地房型
        String where = "where C_ROOMCODE != '' and C_HOTELID  = " + hotel.getId();
        List<Roomtype> localRoomList = Server.getInstance().getHotelService().findAllRoomtype(where, "", -1, 0);
        //删除、更新时间
        if (jlResults == null || jlResults.size() == 0 || localRoomList == null || localRoomList.size() == 0
                || hotel.getState() == null || hotel.getState().intValue() != 3) {
            String delSql = "delete from t_hotelgooddata where c_hotelid = " + hotelid
                    + ";update t_hotel set c_push = null , c_goqunarupdatetime = '" + sdf.format(new Date())
                    + "' where id = " + hotelid;
            Server.getInstance().getSystemService().findMapResultBySql(delSql, null);
            return;
        }
        Map<String, Roomtype> localRoomMap = new HashMap<String, Roomtype>();
        for (Roomtype r : localRoomList) {
            localRoomMap.put(r.getRoomcode().trim(), r);
        }
        //本地数据
        Map<String, HotelGoodData> localDataMap = new HashMap<String, HotelGoodData>();
        String sql = "where C_HOTELID = " + hotel.getId();
        List<HotelGoodData> localDataList = Server.getInstance().getHotelService().findAllHotelGoodData(sql, "", -1, 0);
        for (HotelGoodData d : localDataList) {
            localDataMap.put(d.getJlkeyid(), d);
        }
        //深捷旅数据
        int count = 0;
        for (JLPriceResult jl : jlResults) {
            try {
                String keyid = jl.getJlkeyid();
                String time = jl.getJltime();
                //更新
                HotelGoodData old = localDataMap.get(keyid);
                if (old != null) {
                    //无变化
                    if (time.equals(old.getJltime())) {
                        localDataMap.remove(keyid);
                        continue;
                    }
                    else {
                        System.out.println("~~~~~数据变化，需更新~~~~~" + hotel.getName() + "~~~~~" + old.getRoomtypename()
                                + "~~~~~" + old.getDatenum());
                        String jlRoomId = jl.getJlroomtype().trim();
                        Roomtype local = localRoomMap.get(jlRoomId);
                        long newprice = 0l;
                        try {
                            newprice = (long) Double.parseDouble(jl.getPprice());
                        }
                        catch (Exception e) {
                            newprice = 0l;
                        }
                        if (local == null || ElongHotelInterfaceUtil.StringIsNull(jl.getJlroomtype())
                                || "null".equalsIgnoreCase(jl.getJlroomtype().trim())
                                || ElongHotelInterfaceUtil.StringIsNull(jl.getRatetypeid())
                                || "null".equalsIgnoreCase(jl.getRatetypeid().trim())
                                || ElongHotelInterfaceUtil.StringIsNull(jl.getStayDate())
                                || "null".equalsIgnoreCase(jl.getStayDate().trim()) || "外宾".equals(jl.getRatetype())
                                || ElongHotelInterfaceUtil.StringIsNull(jl.getPprice())
                                || "null".equalsIgnoreCase(jl.getPprice().trim()) || newprice <= 0) {
                            continue;
                        }
                        old.setHotelid(hotel.getId());
                        old.setHotelname(hotel.getName());
                        old.setRoomtypeid(local.getId());
                        old.setRoomtypename(jl.getRoomtype().trim());
                        //去哪儿房型 0：大床； 1：双床；2：大/双床；3：三床；5：单人床
                        old.setBedtypeid(getBed(local.getBed()));
                        old.setJlroomtypeid(jlRoomId);
                        old.setJlf(jl.getFangliang());
                        old.setJlft(jl.getRoomstate());
                        old.setJlkeyid(jl.getJlkeyid().trim());
                        old.setJltime(jl.getJltime());
                        old.setAllotmenttype(jl.getAllotmenttype());
                        old.setRatetype(jl.getRatetype());
                        old.setRatetypeid(jl.getRatetypeid().trim());
                        old.setDatenum(jl.getStayDate());
                        //最少连住
                        try {
                            old.setMinday(Long.parseLong(jl.getMinDay()));
                        }
                        catch (Exception e) {
                            old.setMinday(1l);
                        }
                        //提前天数
                        try {
                            old.setBeforeday(Long.parseLong(jl.getLeadTime()));
                        }
                        catch (Exception e) {
                            old.setBeforeday(0l);
                        }
                        //判断即时确认
                        boolean isNowFlag = false;
                        //房态 12:Open 13:良好 14:紧张 15:不可超 16:满房
                        if (ElongHotelInterfaceUtil.StringIsNull(jl.getAllot())
                                || "null".equalsIgnoreCase(jl.getAllot().trim())) {
                            jl.setAllot("0");//用于判断房量
                        }
                        String jlRoomStatus = jl.getAllot().trim();
                        //Open
                        if ("12".equals(jlRoomStatus)) {
                            isNowFlag = true;
                        }
                        if ("16".equals(jlRoomStatus)) {
                            old.setYuliunum(0l);
                        }
                        else {
                            try {
                                old.setYuliunum(Long.parseLong(jl.getFangliang()));
                            }
                            catch (Exception e) {
                                old.setYuliunum(0l);
                            }
                        }
                        if (old.getYuliunum().longValue() > 0) {
                            isNowFlag = true;
                        }
                        //原价格
                        long oldprice = old.getBaseprice().longValue();
                        //变价
                        if (newprice != oldprice) {
                            isNowFlag = false;//概率小，暂定关房
                        }
                        //0：开房  ；1：关房
                        old.setRoomstatus(isNowFlag ? 0l : 1l);
                        //捷旅房态
                        old.setJlft(jlRoomStatus);
                        old.setContractid(hotel.getHotelcode2());
                        //深捷旅早餐转去哪儿早餐
                        String bf = getBf(jl.getBreakfast());
                        old.setBfcount(Long.parseLong(bf));
                        //深捷旅宽带转去哪儿宽带
                        old.setWeb(getWeb(jl.getNetfee()));
                        old.setSorucetype("6");
                        old.setCityid(String.valueOf(hotel.getCityid()));
                        Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(old);
                        localDataMap.remove(keyid);
                        count++;
                    }
                }
            }
            catch (Exception e) {
                StackTraceElement ele = e.getStackTrace()[0];
                System.out.println("更新数据异常：" + e.getMessage() + "，类名：" + ele.getFileName() + "，方法名："
                        + ele.getMethodName() + "，行数：" + ele.getLineNumber());
            }
        }
        if (localDataMap.size() > 0) {
            String ids = "";
            for (String keyid : localDataMap.keySet()) {
                ids += "'" + keyid + "',";
                System.out.println("=====关房=====" + keyid);
            }
            ids = ids.substring(0, ids.length() - 1);
            //关房
            String closesql = "update T_HOTELGOODDATA set C_ROOMSTATUS = 1 where C_JLKEYID in (" + ids + ")";
            Server.getInstance().getSystemService().findMapResultBySql(closesql, null);
            count++;
        }
        if (count > 0) {
            String updatesql = "update t_hotel set c_goqunarupdatetime = '" + sdf.format(new Date()) + "' where id = "
                    + hotel.getId();
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
        }
    }

    //深捷旅房型转去哪儿房型 0：大床； 1：双床；2：大/双床；5：单人床
    private long getBed(Integer bedid) {
        long bed = 2;//大/双床
        if (bedid == null || bedid == 0) {
            return bed;
        }
        else if (bedid == 1) {
            bed = 5;//单人床
        }
        else if (bedid == 2) {
            bed = 0;//大床
        }
        else if (bedid == 3) {
            bed = 1;//双床
        }
        return bed;
    }

    //深捷旅早餐
    private String getBf(String bf) {
        String ret = "0";//不含早
        if (ElongHotelInterfaceUtil.StringIsNull(bf) || "null".equalsIgnoreCase(bf.trim())) {
            ret = "0";//不含早
        }
        else if (bf.equals("11") || bf.equals("12") || bf.equals("13") || bf.equals("1")) {
            ret = "1";//单早
        }
        else if (bf.equals("21") || bf.equals("22") || bf.equals("23") || bf.equals("2")) {
            ret = "2";//双早
        }
        else if (bf.equals("31") || bf.equals("32") || bf.equals("33") || bf.equals("3")) {
            ret = "3";//三早
        }
        else if (bf.equals("34") || bf.equals("6")) {
            ret = "-1";//含早
        }
        return ret;
    }

    //深捷旅宽带
    private int getWeb(String net) {
        int web = 0;
        if (ElongHotelInterfaceUtil.StringIsNull(net) || "null".equalsIgnoreCase(net.trim())) {
            web = 0;//无
        }
        else if (net.trim().equals("0")) {
            web = 2;//免费
        }
        else if (Integer.parseInt(net.trim()) > 0) {
            web = 3;//收费
        }
        return web;
    }
}
