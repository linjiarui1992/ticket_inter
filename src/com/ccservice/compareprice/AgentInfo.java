package com.ccservice.compareprice;

/**
 * 
 * @author wzc qunar代理商的基本信息
 * 
 */
public class AgentInfo implements Comparable<AgentInfo> {
	private String agentno;// 代理商号
	private String name;// 代理商名称
	private Double orderAvailable;// 代理商订单预订率
	private Double serviceScore;// 代理商服务星级
	private Double userSatisfaction;// 代理商用户体验度
	private String responseTimeDesc;// 代理商响应时间
	private boolean onSale;// 是否在线

	@Override
	public String toString() {
		String str = "代理商号：" + agentno + ",代理商名称：" + name + ",代理商订单预订率:"
				+ orderAvailable + ",代理商服务星级：" + serviceScore + ",代理商用户体验度:"
				+ userSatisfaction + ",代理商响应时间:" + responseTimeDesc + ",是否在线："
				+ onSale;
		return str;
	}

	public String getAgentno() {
		return agentno;
	}

	public void setAgentno(String agentno) {
		this.agentno = agentno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getOrderAvailable() {
		return orderAvailable;
	}

	public void setOrderAvailable(Double orderAvailable) {
		this.orderAvailable = orderAvailable;
	}

	public Double getServiceScore() {
		return serviceScore;
	}

	public void setServiceScore(Double serviceScore) {
		this.serviceScore = serviceScore;
	}

	public Double getUserSatisfaction() {
		return userSatisfaction;
	}

	public void setUserSatisfaction(Double userSatisfaction) {
		this.userSatisfaction = userSatisfaction;
	}

	public String getResponseTimeDesc() {
		return responseTimeDesc;
	}

	public void setResponseTimeDesc(String responseTimeDesc) {
		this.responseTimeDesc = responseTimeDesc;
	}

	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}


	@Override
	public int compareTo(AgentInfo o) {
		try {
			return (int) (o.getOrderAvailable() - this.getOrderAvailable());
		} catch (RuntimeException e) {
			return 0;
		}
	}

}
