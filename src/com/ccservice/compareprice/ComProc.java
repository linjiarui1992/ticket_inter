package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.hmhotelprice.PriceResult;
import com.ccservice.b2b2c.base.hmhotelprice.ResultProduct;
import com.ccservice.b2b2c.base.hmhotelprice.ResultRoom;
import com.ccservice.b2b2c.base.hmhotelprice.ResultStay;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc 比价实体工具类
 * 
 */
public class ComProc extends Utils {
	/**
	 * 加载华闽数据
	 * 
	 * @param hotel
	 *            酒店对象
	 * @param start
	 *            加载数据起始日期
	 * @param end
	 *            加载数据结束日期
	 * @param dayHMData
	 *            承载数据的每天数据
	 * @throws Exception
	 */
	public void loadroomtype(Hotel hotel, Date start, Date end,
			Map<Long, PriceResult> dayHMData) throws Exception {
		System.out.println("加载华闽数数据:" + hotel.getName());
		PriceResult result = Server.getInstance().getIHMHotelService()
				.getQrate(hotel.getHotelcode(), start, end, "", "", "", "", "",
						"", "");
		if(result==null||result.getContract() == null){
			Calendar cal=Calendar.getInstance();
			cal.setTime(start);
			cal.add(Calendar.DAY_OF_MONTH, 3);
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(end);
			cal1.add(Calendar.DAY_OF_MONTH, 3);
			result = Server.getInstance().getIHMHotelService().getQrate(hotel.getHotelcode(), cal.getTime(), cal1.getTime(), "", "", "", "", "",
						"", "");
		}
		if (result != null) {
			if (result.getContract() != null) {
				dayHMData.put(hotel.getId(), result);
			} else {
				Server.getInstance().getSystemService().findMapResultBySql(
						"delete from T_HOTELGOODDATA where c_hotelid="
								+ hotel.getId(), null);
				System.out.println("删除成功……");
			}
		} else {
			Server.getInstance().getSystemService().findMapResultBySql(
					"delete from T_HOTELGOODDATA where c_hotelid="
							+ hotel.getId(), null);
			System.out.println("删除成功……");
		}
	}

	/**
	 * 比价代码程序
	 * 
	 * @param startDate
	 *            起始日期
	 * @param endDate
	 *            结束日期
	 * @param dayqunardatas
	 *            去哪数据集合
	 * @param dayHMData
	 *            华闽数据集合
	 * @param profit
	 *            最低利润点
	 * @param lower
	 *            比别人家低的价格
	 * @throws Exception
	 */
	public void comparequnarhm(String startDate, String endDate,
			Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas,
			Map<Long, PriceResult> dayHMData, Long profit, Long lower)
			throws Exception {
		System.out.println("开始比价……");
		List<HotelGoodData> result0 = new ArrayList<HotelGoodData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(startDate));
		Date start = sdf.parse(startDate);
		Date end = sdf.parse(endDate);
		while (start.before(end)) {
			System.out.println("比价日期：" + sdf.format(start));
			Map<Long, Map<String, List<HotelData>>> tem = dayqunardatas.get(sdf
					.format(start));
			if (tem != null) {
				Set<Long> hotelids = tem.keySet();
				for (Long hotelid : hotelids) {
					PriceResult result = dayHMData.get(hotelid);
					if (result.getHotelid() != null) {
						List<ResultProduct> product = result.getProduct();
						if (product.size() > 0) {
							Hotel hotel = Server.getInstance().getHotelService().findHotel(hotelid);
							String contractid = result.getContract();
							String contractver = result.getVer();
							String hotelname = result.getHotelname();
							Map<String, List<HotelData>> rooms = tem.get(hotelid);
							for (ResultProduct resultProduct : product) {
								List<ResultRoom> roomstemp = resultProduct.getRooms();
								if (roomstemp.size() > 0) {
									String countryarea = resultProduct.getNationname();// 试用区域
									String prod = resultProduct.getProd();
									if (countryarea.contains("不适用于中国")
											|| countryarea.contains("非中国大陆市场")
											|| countryarea.equals("香港及台湾市场")
											|| countryarea.equals("香港市场")
											|| countryarea.equals("只限香港身份证")) {
										System.out.println("不使用区域过滤……");
										continue;
									}
									int beforeday = resultProduct.getAdvance();// 提前几天
									int minday = 0;
									if (resultProduct.getMin() > 0) {
										minday = resultProduct.getMin();// 最少多少天
									}
									label: for (ResultRoom resultRoom : roomstemp) {
										String cat = resultRoom.getCat();
										Roomtype roomtype = null;
										String where = "where c_hotelid="+ hotelid+ " and c_roomcode='"+ cat
										+ "' and c_bed!=58 and c_bed in(select id from t_bedtype where c_type='"
										+ resultRoom.getType() + "')";
										List<Roomtype> roomtemps = Server.getInstance().getHotelService().findAllRoomtype(where," order by id ", -1, 0);
										if (roomtemps.size() > 0) {
											roomtype = roomtemps.get(0);
										}
										if (roomtype != null) {
											if (roomtype.getQunarname() != null&& !"".equals(roomtype.getQunarname())) {
												List<HotelData> hoteltdatas = rooms.get(roomtype.getQunarname());
												if (hoteltdatas != null&& hoteltdatas.size() > 0) {
													List<ResultStay> stays = resultRoom.getStays();
													if (stays.size() > 0) {
														Map<String, ResultStay> staysdatetemp=resultRoom.getStaystemp();
														if(staysdatetemp.containsKey(sdf.format(start))){
															Bedtype bedtype = null;
															String type = resultRoom.getType();
															if ("S".equals(type)) {
																continue label;
															}
															List<Bedtype> bedtypes = Server.getInstance().getHotelService().findAllBedtype("where c_type='"+ type+ "'", "",-1, 0);
															if (bedtypes.size() > 0) {
																bedtype = bedtypes.get(0);
															}
															//String serv = resultRoom.getServ();
															String bf = resultRoom.getBf();
															//String deadline = resultRoom.getDeadline();
															ResultStay resultStay=staysdatetemp.get(sdf.format(start));
																for (HotelData hotelData : hoteltdatas) {
																	HotelGoodData good = new HotelGoodData();
																	good.setHotelid(hotelid);
																	good.setRoomtypeid(roomtype.getId());
																	good.setRoomtypename(roomtype.getName());
																	good.setHotelname(hotelname);
																	good.setContractid(contractid);
																	good.setContractver(contractver);
																	good.setBfcount(Long.parseLong(bf));
																	good.setProdid(prod);
																	good.setMinday((long) minday);
																	good.setCityid(hotel.getCityid().toString());
																	if (bedtype != null) {
																		String bedtypename = bedtype.getTypename();
																		good.setRoomtypename(good.getRoomtypename()+ "-"+ bedtypename);
																	}
																	// good.setCityid(hotel.getCityid());
																	// good.setRegion(countryarea);
																	good.setBeforeday((long) beforeday);
																	Double sp = Double.parseDouble(resultStay.getPrice())+ profit;
																	good.setAgentname(hotelData.getAgentName(hotelData.getAgentNo()));
																	String status = resultStay.getIsallot();
																	if (status.equals("Y")) {
																		good.setYuliunum(0l);// 暂时写为0
																									// 以后稳定了在为1（及时确认）
																	} else {
																		good.setYuliunum(0l);
																	}
																	good.setDatenum(resultStay.getStaydate());
																	good.setBaseprice(Long.parseLong(resultStay.getPrice()));
																	good.setSealprice(hotelData.getSealprice().longValue());
																	if (bf.equals("0")) {
																		good.setBfcount(0l);
																	} else if (bf.equals("1")) {
																		good.setBfcount(0l);
																	} else if (bf.equals("2")) {
																		good.setBfcount(2l);
																	} else if (bf.equals("3")) {
																		good.setBfcount(3l);
																	} else if (bf.equals("4")) {
																		good.setBfcount(4l);
																	}
																	good.setShijiprice(hotelData.getSealprice().longValue());
																	good.setProfit(hotelData.getSealprice().longValue()- Integer.parseInt(resultStay.getPrice()));
																	if (good.getProfit() == profit) {// 如果利润为profit则不用再减，如果大于prodfit则比其他代理商少lower元

																	} else {
																		if (good.getProfit() >= (profit + lower)) {
																			good.setShijiprice(hotelData.getSealprice()- lower);
																			good.setProfit(hotelData.getSealprice()- Integer.parseInt(resultStay.getPrice())- lower);
																		}
																	}

																	if (hotelData.getAgentNo().equals("wiotappb065")) {// 易订行过滤
																		continue;
																	}
																	if (hotelData.getRoomstatus() == -1) {// 关房过滤
																		continue;
																	}
																	if (resultStay.getIsallot().equals("C")) {
																		good.setRoomflag("0");
																		good.setRoomstatus(1l);
																		result0.add(good);
																		continue label;
																	}
																	Double profittemp = good.getProfit().doubleValue();
																	if ("0".equals(bf)) {
																		if (sp > hotelData.getSealprice()) {
																			good.setRoomstatus(1l);
																			result0.add(good);
																			continue label;
																		} else {
																			if (hotelData.getType() == 0&& profittemp / 2 >= profit) {
																				good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
																				good.setProfit(hotelData.getSealprice()- Integer.parseInt(resultStay.getPrice())
																								- (long) (profittemp / 2));
																			} else if (hotelData.getType() == 0&& profittemp / 2 < profit) {
																				good.setRoomstatus(1l);
																				result0.add(good);
																				continue label;
																			}
																			if (good.getProfit() <profit) {
																				good.setRoomstatus(1l);
																				result0.add(good);
																				continue label;
																			}
																			if (hotelData.getRoomnameBF().contains("早")&& !hotelData.getRoomnameBF()
																							.equals("无早")&& !hotelData.getRoomnameBF().equals("不含早")) {
																				if (good.getProfit() > (profit + 10)) {
																					good.setShijiprice(good.getShijiprice() - 10);
																					good.setProfit(good.getProfit() - 10);
																					good.setRoomstatus(0l);
																					System.out.println("当前利润："+ good.getProfit());
																					result0.add(good);
																					continue label;
																				} else if (good.getProfit() > profit + 5) {
																					good.setShijiprice(good.getShijiprice() - 5);
																					good.setProfit(good.getProfit() - 5);
																					good.setRoomstatus(0l);
																					System.out.println("当前利润："+ good.getProfit());
																					result0.add(good);
																					continue label;
																				} else {
																					good.setRoomstatus(0l);
																					result0.add(good);
																					continue label;
																				}
																			} else {
																				good.setRoomstatus(0l);
																				result0.add(good);
																				continue label;
																			}
																		}
																	} else if ("1"
																			.equals(bf)) {
																		if (sp > hotelData
																				.getSealprice()) {
																			good
																					.setRoomstatus(1l);
																			result0
																					.add(good);
																			continue label;
																		} else {
																			if (hotelData
																					.getRoomnameBF()
																					.contains(
																							"早")
																					&& !hotelData
																							.getRoomnameBF()
																							.contains(
																									"无早")) {
																				if (hotelData
																						.getType() == 0
																						&& (profittemp / 2) >= profit) {
																					good
																							.setShijiprice(hotelData
																									.getSealprice()
																									- (long) (profittemp / 2));
																					good
																							.setProfit(good
																									.getProfit()
																									- (long) (profittemp / 2));
																				} else if (hotelData
																						.getType() == 0
																						&& profittemp / 2 < profit) {
																					good
																							.setRoomstatus(1l);
																					result0
																							.add(good);
																					continue label;
																				}
																				if (good
																						.getProfit() < profit) {
																					good
																							.setRoomstatus(1l);
																					result0
																							.add(good);
																					continue label;
																				}
																				if (hotelData
																						.getRoomnameBF()
																						.contains(
																								"单早")
																						|| hotelData
																								.getRoomnameBF()
																								.contains(
																										"1份早餐")) {
																					good
																							.setRoomstatus(0l);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					result0
																							.add(good);
																					continue label;
																				} else if (hotelData
																						.getRoomnameBF()
																						.equals(
																								"")
																						|| hotelData
																								.getRoomnameBF()
																								.contains(
																										"无早")
																						|| hotelData
																								.getRoomnameBF()
																								.contains(
																										"不含早")) {
																					good
																							.setRoomstatus(0l);
																					result0
																							.add(good);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					continue label;
																				} else {
																					if (good
																							.getProfit() > (profit + 10)) {
																						good
																								.setShijiprice(good
																										.getShijiprice() - 10);
																						good
																								.setProfit(good
																										.getProfit() - 10);
																						good
																								.setRoomstatus(0l);
																						System.out
																								.println("当前利润："
																										+ good
																												.getProfit());
																						result0
																								.add(good);
																						continue label;
																					} else if (good
																							.getProfit() >= (profit + 5)) {
																						good
																								.setShijiprice(good
																										.getShijiprice() - 5);
																						good
																								.setProfit(good
																										.getProfit() - 5);
																						good
																								.setRoomstatus(0l);
																						System.out
																								.println("当前利润："
																										+ good
																												.getProfit());
																						result0
																								.add(good);
																						continue label;
																					} else {
																						good
																								.setRoomstatus(0l);
																						System.out
																								.println("当前利润："
																										+ good
																												.getProfit());
																						result0
																								.add(good);
																						continue label;
																					}
																				}
																			}
																		}
																	} else if ("2".equals(bf)) {
																		if (sp > hotelData.getSealprice()) {
																			good.setRoomstatus(1l);
																			result0.add(good);
																			continue label;
																		} else {
																			if (hotelData.getType() == 0&& (profittemp / 2 >= profit)) {
																				good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
																				good.setProfit(good.getProfit()- (long) (profittemp / 2));
																			} else if (hotelData.getType() == 0&& (profittemp / 2 < profit)) {
																				good.setRoomstatus(1l);
																				result0.add(good);
																				continue label;
																			}
																			if (good.getProfit() < profit) {
																				good.setRoomstatus(1l);
																				result0.add(good);
																				continue label;
																			}

																			if (hotelData.getRoomnameBF().contains("双早")
																					|| hotelData.getRoomnameBF().equals("")
																					|| hotelData.getRoomnameBF().equals("无早")
																					|| hotelData.getRoomnameBF().equals("不含早")
																					|| hotelData.getRoomnameBF().equals("单早")
																					|| hotelData.getRoomnameBF().equals("1份早餐")
																					|| hotelData.getRoomnameBF().contains("2份早餐")
																					|| hotelData.getRoomnameBF().contains("含早")) {
																				good.setRoomstatus(0l);
																				System.out.println("当前利润："+ good.getProfit());
																				result0.add(good);
																				continue label;
																			} else {
																				if (good.getProfit() >= (profit + 10)) {
																					good.setShijiprice(good.getShijiprice() - 10);
																					good.setProfit(good.getProfit() - 10);
																				} else if (good.getProfit() >= (profit + 5)) {
																					good.setShijiprice(good.getShijiprice() - 5);
																					good.setProfit(good.getProfit() - 5);
																				}
																				System.out.println("当前利润："+ good.getProfit());
																				good.setRoomstatus(0l);
																				result0.add(good);
																				continue label;
																			}
																		}
																	} else if ("3"
																			.equals(bf)) {
																		if (sp > hotelData
																				.getSealprice()) {
																			good
																					.setRoomstatus(1l);
																			result0
																					.add(good);
																			continue label;
																		} else {
																			if (hotelData
																					.getType() == 0
																					&& (profittemp / 2 >= profit)) {
																				good
																						.setShijiprice(hotelData
																								.getSealprice()
																								- (long) (profittemp / 2));
																				good
																						.setProfit(good
																								.getProfit()
																								- (long) (profittemp / 2));
																			} else if (hotelData
																					.getType() == 0
																					&& (profittemp / 2 < profit)) {
																				good
																						.setRoomstatus(1l);
																				result0
																						.add(good);
																				continue label;
																			}
																			if (hotelData
																					.getRoomnameBF()
																					.contains(
																							"三早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"双早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"1份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"2份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"3份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"无早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"不含早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"单早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"含早")) {
																				good
																						.setRoomstatus(0l);
																				System.out
																						.println("当前利润："
																								+ good
																										.getProfit());
																				result0
																						.add(good);
																				continue label;
																			} else {
																				if (good
																						.getProfit() >= (profit + 10)) {
																					good
																							.setShijiprice(good
																									.getShijiprice() - 10);
																					good
																							.setProfit(good
																									.getProfit() - 10);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					good
																							.setRoomstatus(0l);
																					result0
																							.add(good);
																					continue label;
																				} else if (good
																						.getProfit() >= (profit + 5)) {
																					good
																							.setShijiprice(good
																									.getShijiprice() - 5);
																					good
																							.setProfit(good
																									.getProfit() - 5);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					good
																							.setRoomstatus(0l);
																					result0
																							.add(good);
																					continue label;
																				} else {
																					good
																							.setRoomstatus(0l);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					result0
																							.add(good);
																					continue label;
																				}
																			}
																		}
																	} else if ("4"
																			.equals(bf)) {
																		if (sp > hotelData
																				.getSealprice()) {
																			good
																					.setRoomstatus(1l);
																			result0
																					.add(good);
																			continue label;
																		} else {
																			if (hotelData
																					.getType() == 0
																					&& (profittemp / 2 >= profit)) {
																				good
																						.setShijiprice(hotelData
																								.getSealprice()
																								- (long) (profittemp / 2));
																				good
																						.setProfit(good
																								.getProfit()
																								- (long) (profittemp / 2));
																			} else if (hotelData
																					.getType() == 0
																					&& (profittemp / 2 < profit)) {
																				good
																						.setRoomstatus(1l);
																				result0
																						.add(good);
																				continue label;
																			}
																			if (good
																					.getProfit() < profit) {
																				good
																						.setRoomstatus(1l);
																				result0
																						.add(good);
																				continue label;
																			}
																			if (hotelData
																					.getRoomnameBF()
																					.contains(
																							"四早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"双早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"2份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"4份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"三早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"3份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"无早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"不含早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"单早")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"1份早餐")
																					|| hotelData
																							.getRoomnameBF()
																							.contains(
																									"含早")) {
																				good
																						.setRoomstatus(0l);
																				System.out
																						.println("当前利润："
																								+ good
																										.getProfit());
																				result0
																						.add(good);
																				continue label;
																			} else {
																				if (good
																						.getProfit() >= (profit + 10)) {
																					good
																							.setShijiprice(good
																									.getShijiprice() - 10);
																					good
																							.setProfit(good
																									.getProfit() - 10);
																					System.out
																							.println("当前利润："
																									+ good
																											.getProfit());
																					good
																							.setRoomstatus(0l);
																					result0
																							.add(good);
																					continue label;
																				} else if (good.getProfit() >= (profit + 5)) {
																					good.setShijiprice(good.getShijiprice() - 5);
																					good.setProfit(good.getProfit() - 5);
																					System.out.println("当前利润："+ good.getProfit());
																					good.setRoomstatus(0l);
																					result0.add(good);
																					continue label;
																				} else {
																					good
																							.setRoomstatus(0l);
																					result0
																							.add(good);
																					continue label;
																				}
																			}
																		}
																	}
																}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			cal.add(Calendar.DAY_OF_MONTH, 1);
			start = cal.getTime();
		}
		writeDataDB(result0);
	}

	/**
	 * 写入势数据写入数据库
	 * 
	 * @param gooddata
	 */
	public void writeDataDB(List<HotelGoodData> gooddata){
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Set<Long> hotelids=new HashSet<Long>();
		int version=0;
		for (int i = 0; i < gooddata.size() ; i++) {
			HotelGoodData good=gooddata.get(i);
			if(i==gooddata.size()-1){
				Server.getInstance().getSystemService().findMapResultBySql(
						"delete from T_HOTELGOODDATA where c_hotelid="+ good.getHotelid()+" and C_HIDECLOSE<"+version, null);
				System.out.println("删除版本低的……");
			}
			if(!hotelids.contains(good.getHotelid())){
				hotelids.add(good.getHotelid());
				List versionids=Server.getInstance().getSystemService().findMapResultBySql("select MAX(C_HIDECLOSE) as C_HIDECLOSE from T_HOTELGOODDATA where C_HOTELID="+good.getHotelid(), null);
				if(versionids!=null&&versionids.size()>0){
					if(((Map)versionids.get(0)).get("C_HIDECLOSE")!=null){
						System.out.println("版本提升……");
						version=Integer.parseInt(((Map)versionids.get(0)).get("C_HIDECLOSE").toString());
						version=version+1;
					}
				}
			}
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			good.setOpenclose(1);
			good.setSorucetype("3");
			// 特殊处理
			if(good.getProfit()<20){
				good.setRoomstatus(1l);//关房
				good.setRoomflag("2");
				good.setShijiprice(good.getBaseprice()+20);
			}
			if(good.getProfit()>=20){
				good.setRoomflag("1");
			}
			System.out.println(good);
			String str="[dbo].[sp_inserthmgooddata] @hotelid = "+
			good.getHotelid()+",@hotelname = N'"+
			good.getHotelname()+"',@roomtypeid = "+
			good.getRoomtypeid()+",@roomtypename = N'"+
			good.getRoomtypename()+"',@shijiprice = "+
			good.getShijiprice()+",@baseprice = "+
			good.getBaseprice()+",	@sealprice = "+
			good.getSealprice()+",@profit = "+
			good.getProfit()+",@roomstatus = "+
			good.getRoomstatus()+",@yuliunum = "+
			good.getYuliunum()+",@datenum = N'"+
			good.getDatenum()+"',@minday = "+
			good.getMinday()+",@openclose = "+
			good.getOpenclose()+",@beforeday = "+
			good.getBeforeday()+",@contractid = N'"+
			good.getContractid()+"',@contractver = N'"+
			good.getContractver()+"',@prodid = N'"+
			good.getProdid()+"',@bfcount = "+
			good.getBfcount()+",@sorucetype = N'"+
			good.getSorucetype()+"',@agentname = N'"+
			good.getAgentname()+"',@updatetime = N'"+
			good.getUpdatetime()+"',@roomflag=N'"+
			good.getRoomflag()+"',@cityid=N'"+
			good.getCityid()+"',@bedtypeid="+
			good.getBedtypeid()+",@version="+
			version+",@allot="+
			good.getAllot()+",@qunarname=N'"+
			good.getQunarName()+"'";
			synchronized (this) {
				List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
			}
		}
	}

}
