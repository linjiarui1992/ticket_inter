/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tuniu.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;

import org.eclipse.jetty.util.UrlEncoded;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.service.TrainTuNiuOfflineLockOrder
 * @description: TODO - 途牛线下票锁票请求发起接口-直接定义在相关的方法中
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:58:14 
 * @version: v 1.0
 * @since 
 *
 */
public class TuNiuOfflineLockOvertimeCallback implements Job {
    private static final String LOGNAME = "途牛线下票锁票请求启动定时任务-5分钟后自动回调出票失败";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        Integer useridi = dataMap.getInt("useridi");
        String LockWait = dataMap.getString("LockWait");
        
        WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-进入到自动回调出票失败方法-OrderId-->"+OrderId+"-useridi-->"+useridi+"-LockWait-->"+LockWait);

        if (OrderId == null || OrderId == 0L) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-OrderId不存在，请排查");
            return;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(OrderId);
        }
        catch (Exception e) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline == null) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-对象无法获取，请排查");
            return;
        }
        
        if (trainOrderOffline.getIsLockCallback() == null) {
            trainOrderOffline.setIsLockCallback(false);
        }
        
        if (!trainOrderOffline.getIsLockCallback() && trainOrderOffline.getOrderStatus() == 1) {//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            
            //记录日志 - 刷新界面 - 
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(OrderId, LOGNAME);
            
            //此处目前需要调用 - 出票回调接口，回调出票失败
            //?orderId=1575&userid=382&expressNum=616284889097&expressCompanyName=0&orderSuccess=true

            //途牛拒单回调
            String result = "false";//反馈 true - false
            
            /*String param = "";
            try {
                param = "?orderId="+OrderId+"&userid="+useridi+"&refundReason="+108
                        +"&refundreasonstr="+URLEncoder.encode("途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败", "UTF-8")+"&orderSuccess="+false;
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/

            String tuniuOrderCallbackServlet = PropertyUtil.getValue("TuniuOrderCallbackServlet", "Train.GuestAccount.properties");

            JSONObject resObj = new JSONObject();
            resObj.put("orderId", OrderId);
            resObj.put("userid", useridi);
            resObj.put("refundReason", 108);
            resObj.put("refundreasonstr", "途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败");
            resObj.put("orderSuccess", false);

            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-回调出票失败-resObj:"+resObj);
            
            try {
                result = new HttpUtil().doPost(tuniuOrderCallbackServlet, resObj.toJSONString());
            }
            catch (Exception e1) {
                //记录操作记录
                WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-回调出票失败的接口无法访问，请联系技术排查");
                return;
            }

            JSONObject resResultTemp = JSONObject.parseObject(result);

            String resultTemp = "false";//反馈 true - false

            JSONObject resResult = new JSONObject();

            //这个锁票失败的原因，需要做特殊处理
            String success = resResultTemp.getString("success");
            if (success.equals("true")) {
                resultTemp = "true";//出票成功，回调成功
            } else {
                resultTemp = "false";//未知原因导致出票回调失败，请联系客服处理
                String msg = resResult.getString("msg");
                if (msg.contains("网络原因")) {
                    resultTemp = "false-网络原因";//由于网络原因，导致途牛的出票回调请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理
                } else if (msg.contains("出票超时")) {
                    result = "false-出票超时";//该订单已出票超时，请选择拒单
                }
            }
            
            if (resultTemp.contains("resultTemp")) {
                resResult.put("success", "false");
                resResult.put("msg", "锁票超时失败，由于网络原因，导致途牛的出票回调请求接口无法访问，请您选择拒单");
                resResult.put("orderid", trainOrderOffline.getOrderNumber());
            } else {
                resResult.put("success", "false");
                resResult.put("msg", "锁票超时失败，已经回调出票失败，该单已拒单"); 
                resResult.put("orderid", trainOrderOffline.getOrderNumber());
            }

            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-resResult");
            
        }
    }

}
