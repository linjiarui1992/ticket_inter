/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tuniu.service;

import java.util.Random;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.service.TrainTuNiuOfflineLockOrder
 * @description: TODO - 途牛线下票锁票请求发起接口-直接定义在相关的方法中
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:58:14 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTuNiuOfflineCancelLockTicket implements Job {
    private static final String LOGNAME = "途牛线下票锁票回调请求定时取消任务";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        
        /*DateFormat df = new SimpleDateFormat("yyyy年MM月dd日  HH时mm分ss秒");  
        System.out.println("任务启动");
        System.out.println(jobName + "在" + df.format(new Date()) + "时，输出了：Hello World!!!--:--:>"+OrderId);  
        System.out.println("任务结束");*/
        

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-进入到取消方法-OrderId-->"+OrderId);

        if (OrderId == null || OrderId == 0L) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-OrderId不存在，请排查");
            return;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(OrderId);
        }
        catch (Exception e) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline == null) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-对象无法获取，请排查");
            return;
        }

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-OrderStatus-->"+trainOrderOffline.getOrderStatus());

        if (trainOrderOffline.getOrderStatus() == 1 && trainOrderOffline.getLockedStatus() == 1) {//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            //如果是锁票状态，则变为非锁票的状态，并能给出相关的提示 - 只处理等待出票
            try {
                WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-进入到取消方法-开始修改-->"+4);

                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(OrderId, 4);//0表示未被锁定，1表示被锁定
            }
            catch (Exception e) {
                //记录操作记录
                WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-无法修改锁票状态");
                return;
            }
        }

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调请求定时取消任务-进入到取消方法-修改成功");

    }

    public static void main(String[] args) throws Exception {
        //白名单限制
        //http://121.40.226.72:9007/ticket_inter/TrainTuNiuOfflineLockOrderServletTest
        
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        String data = "{\"orderId\":\"test17081800133473\",\"msg\":\"请求锁票\"}";
        String timestamp = TrainOrderOfflineUtil.getTuNiuTimestamp();
        String url = PropertyUtil.getValue("TrainTuNiuOfflineLockTicket", "Train.GuestAccount.properties");

        System.out.println(httpPostJsonUtil.getTuNiuRes(data, timestamp, url));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
