package com.ccservice.tuniu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.tuniu.service.TuNiuOfflineLockOvertimeCallback;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineLockOrderCallbackServlet
 * @description: TODO - 途牛线下票锁票请求接口 - 
 * 
 * cn_home平台 请求该接口
 * 
 * 将异步的方式转换为同步的结果反馈
 * 
 * 向cn_home平台 进行 模拟的 - 同步反馈
 * 
 * @author: 郑州-技术-郭伟强  E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:57:36
 * @version: v 1.0
 * @since 
 * 
 */
public class TrainTuNiuOfflineLockTicketServletCN extends HttpServlet {
    private static final String LOGNAME = "途牛线下票锁票请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求信息-orderId-->"+orderIdl+",userid-->"+useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起锁票请求");
        
        JSONObject result = lockTicketCN(orderIdl, useridi);
        
        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求结果-result" + result);
        
        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"途牛线下票锁票请求完成");
        out.flush();
        out.close();
    }

    private JSONObject lockTicketCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求-进入到方法-lockTicketCN:orderIdl-->"+orderIdl+",useridi-->"+useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        resResult.put("orderid", trainOrderOffline.getOrderNumber());
        
        if (trainOrderOffline.getLockedStatus()!=0) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已发起过锁单请求，请勿重复操作");
            return resResult;
        }

        String orderId = trainOrderOffline.getOrderNumberOnline();

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        JSONObject dataJson = new JSONObject();
        dataJson.put("orderId", orderId);
        dataJson.put("msg", "请求锁票");

        String errorMsg = "请求锁票";

        String url = PropertyUtil.getValue("TrainTuNiuOfflineLockTicket", "Train.GuestAccount.properties");

        //String data = "{\"orderId\":\"test17081800133473\",\"msg\":\"请求锁票\"}";
        String data = dataJson.toJSONString();
        
        //System.out.println(data);
        
        String res = "";
        
        for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
            res = lockTicket(httpPostJsonUtil, errorMsg, url, data);
            //System.out.println(res);//{"success":true} - 此处只是请求的发起

            //记录请求信息日志
            WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求反馈信息-res" + res);

            if (res != null && res.contains("true")) {//锁单请求成功
                //更新锁单状态为锁单中 - 点击一次之后
                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderIdl, 3);//锁单中
                
                /**
                 * 如果想模拟成为同步反馈的结果，需要判定是否收到异步的锁单反馈 - 且不允许再次锁单 - 需要加上一个标志位
                 * 
                 * 途牛的锁票的异步转同步的内部等待时间 - 以s为单位
                 */
                String LockWait = PropertyUtil.getValue("TrainTuNiuOfflineLockWait", "Train.GuestAccount.properties");
                
                //毫秒数
                Long overtime = new Date().getTime() + Integer.valueOf(LockWait)*1000;

                String lockWaitDateTime = TrainOrderOfflineUtil.getTimestrByTime(overtime);
                //记录日志
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票请求成功，等待途牛异步反馈结果，目前等待超时的设置时间是:"+LockWait+"s，超时后自动回调出票失败");
                
                //此处另起JOB，完成5分钟之后的拒单操作

                String year = lockWaitDateTime.substring(0,4);
                String month = lockWaitDateTime.substring(5,7);
                String day = lockWaitDateTime.substring(8,10);
                String hour = lockWaitDateTime.substring(11,13);
                String minute = lockWaitDateTime.substring(14,16);
                String second = lockWaitDateTime.substring(17,19);
                
                //"40 17 14 25 08 ? 2017"
                String cronExpression = second+" "+minute+" "+hour+" "+day+" "+month+" ? "+year;
                
                //System.out.println(cronExpression);

                WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求启动定时任务-cronExpression" + cronExpression);

                try {
                    JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail("TuNiuOfflineLockOvertimeCallback"+"Job"+trainOrderOffline.getId(), "TuNiuOfflineLockOvertimeCallback"+"JobGroup");
                    if (jobDetail!=null) {
                        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求启动定时任务-jobDetail:" + jobDetail.getFullName());
                        
                        resResult.put("success", "true");
                        resResult.put("msg", "锁票请求成功");
                        return resResult;
                    } else {
                        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票请求启动定时任务-jobDetail:" + null);

                        //创建一个定时任务，并在指定的时间点进行启动
                        try {
                            SchedulerUtil.startLockScheduler(trainOrderOffline.getId(), useridi, LockWait, cronExpression, "TuNiuOfflineLockOvertimeCallback", TuNiuOfflineLockOvertimeCallback.class);
                        }
                        catch (Exception e) {
                            return ExceptionTNUtil.handleTNException(e);
                        }

                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票请求成功");
                        
                        resResult.put("success", "true");
                        resResult.put("msg", "锁票请求成功");
                        return resResult;
                    }
                }
                catch (SchedulerException e1) {
                    e1.printStackTrace();
                }
                
                /*while (true) {
                    //隔5s扫描一次标志位 - 判断是否锁单反馈成功 - 超时等待时间 - 1min - 可配置 - 先写在文件里面
                    Boolean isLockCallback = false;
                    try {
                        isLockCallback = trainOrderOfflineDao.findIsLockCallbackById(orderIdl);
                    }
                    catch (Exception e) {
                        ExceptionTNUtil.handleTNException(e);
                    }
                    
                    if (isLockCallback) {
                        //已经有了回调结果之后，根据取出的锁单字段的状态值，判断是成功还是失败
                        
                        Integer lockedStatus = 0;//0表示未被锁定，1表示被锁定
                        try {
                            lockedStatus = trainOrderOfflineDao.findLockedStatusById(orderIdl);
                        }
                        catch (Exception e) {
                            ExceptionTNUtil.handleTNException(e);
                        }
                        
                        if (lockedStatus == 1) {
                            //记录日志
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票成功");
                            
                            resResult.put("success", "true");
                            resResult.put("msg", "锁票成功，请尽快出票");
                            resResult.put("orderid", trainOrderOffline.getOrderNumber());
                            return resResult;
                        } else {//锁票异步反馈失败
                            //记录日志
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票成功");
                            
                            resResult.put("success", "false");
                            resResult.put("msg", "锁票失败，请拒单");
                            resResult.put("orderid", trainOrderOffline.getOrderNumber());
                            return resResult;
                        }
                        
                        //break;
                    } else {
                        //超时无反馈，判断-锁单失败
                        if (overtime > new Date().getTime()) {
                            TrainOrderOfflineUtil.sleep(5*1000);
                            continue;
                        } else {
                            //记录日志 - 刷新界面 - 
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "途牛端的锁票请求异步反馈时间超时，锁票失败，请稍后刷新界面或者联系客服处理");
                            
                            //此处目前需要调用 - 出票回调接口，回调出票失败
                            //?orderId=1575&userid=382&expressNum=616284889097&expressCompanyName=0&orderSuccess=true

                            //途牛拒单回调
                            String result = "false";//反馈 true - false
                            
                            String param = "?orderId="+orderIdl+"&userid="+useridi+"&refundReason="+108
                                    +"&refundreasonstr=途牛锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败&orderSuccess="+false;

                            String tuniuOrderCallbackServlet = PropertyUtil.getValue("TuniuOrderCallbackServlet", "Train.GuestAccount.properties");
                                           
                            try {
                                result = new HttpUtil().doGet(tuniuOrderCallbackServlet, param);
                            }
                            catch (Exception e1) {
                                e1.printStackTrace();
                            }

                            JSONObject resResultTemp = JSONObject.parseObject(result);

                            String resultTemp = "false";//反馈 true - false
                            
                            //这个锁票失败的原因，需要做特殊处理
                            String success = resResultTemp.getString("success");
                            if (success.equals("true")) {
                                resultTemp = "true";//出票成功，回调成功
                            } else {
                                resultTemp = "false";//未知原因导致出票回调失败，请联系客服处理
                                String msg = resResult.getString("msg");
                                if (msg.contains("网络原因")) {
                                    resultTemp = "false-网络原因";//由于网络原因，导致途牛的出票回调请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理
                                } else if (msg.contains("出票超时")) {
                                    result = "false-出票超时";//该订单已出票超时，请选择拒单
                                }
                            }
                            
                            if (resultTemp.contains("resultTemp")) {
                                resResult.put("success", "false");
                                resResult.put("msg", "锁票超时失败，由于网络原因，导致途牛的出票回调请求接口无法访问，请您选择拒单");
                                resResult.put("orderid", trainOrderOffline.getOrderNumber());
                            } else {
                                resResult.put("success", "false");
                                resResult.put("msg", "锁票超时失败，已经回调出票失败，该单已拒单"); 
                                resResult.put("orderid", trainOrderOffline.getOrderNumber());
                            }
                            
                            return resResult;
                        }
                    }
                }*/
                
                //break;
            } else {//锁单请求失败 - 后续可以再次锁单 - 不更改任何内容 - 系统问题
                /**
                 * 
    success:null不会有，false会存在
    false的话，你可以重试
    这种情况比较少，只有网络故障的时候才会反馈false

    重试3次还不成功的话，这个单子会先放着不处理，等后期人工再次介入，如果无法请求成功，单子应该会拒单
                 * 
                 */

                if (res == null || res.equals("")) {
                    if (i == (TrainOrderOfflineUtil.TNRETRY-1)) {
                        //记录日志
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "由于网络原因，锁票请求反馈失败，稍后重试或者联系技术处理");
                        
                        resResult.put("success", "false");
                        resResult.put("msg", "由于网络原因，导致途牛的锁票请求接口无法访问，请您暂且不要拒单，稍后重试或者联系客服处理");
                        return resResult;
                    }
                    //记录操作记录
                    
                    TrainOrderOfflineUtil.sleep(10*1000);
                    continue;
                }

                resResult.put("success", "false");
                resResult.put("msg", "未知原因");
                
                /*if (res != null && !res.equals("")) {
                    JSONObject resJson = JSONObject.parseObject(res);
                    resResult.put("msg", resJson.getString("msgInfo"));
                }*/
                return resResult;
            }
        }
        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票失败，原因异常，请联系技术处理");
        
        resResult.put("success", "false");
        resResult.put("msg", "异常原因，请联系客服处理");
        return resResult;
    }

    private String lockTicket(HttpPostJsonUtil httpPostJsonUtil, String errorMsg, String url, String data) {
        String timestamp = TrainOrderOfflineUtil.getTuNiuReqTimestamp();
        String res = httpPostJsonUtil.getTuNiuReqRes(data, timestamp, url, errorMsg);
        return res;
    }
    
}
