package com.ccservice.jielv.hotel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;

public class UpdateJielvModifyPrice implements Job {
	
	private JLComProcUp proc = null;// 比价工具对象
	
	
	
	private Map<String, List<JLPriceResult>> dayJLData = new HashMap<String, List<JLPriceResult>>();
	
	public static void main(String[] args) {
		UpdateJielvModifyPrice dp=new UpdateJielvModifyPrice();
		dp.test();
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		UpdateJielvModifyPrice dp=new UpdateJielvModifyPrice();
		dp.test();
	}

	/**
	 * 构造器，实例化比价对象
	 */
	public UpdateJielvModifyPrice() {
		proc = new JLComProcUp();
	}


	public void test() {
		Calendar start = GregorianCalendar.getInstance();
		start.add(Calendar.DAY_OF_MONTH, -1);
		Calendar end = GregorianCalendar.getInstance();
		end.add(Calendar.DAY_OF_MONTH, 30);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfx = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String started = sdf.format(start.getTime());
		String ended = sdf.format(end.getTime());
		
		PropertyUtil pu=new PropertyUtil();
		Float profit = Float.parseFloat(pu.getValue("profit1"));// 1000-
		
		while(true){	
			long time1 = System.currentTimeMillis();
			long time = Long.parseLong(pu.getValue("jielvtime"));// 更新间隔时间（秒）
			//更新酒店
			Server.getInstance().getIJLHotelService().getUpHotel(time);
			//更新房型
			Server.getInstance().getIJLHotelService().getUpRoomType(time);
			//删除表
			List<JLPriceResult> jldel=Server.getInstance().getIJLHotelService().getDelHotel(time);
			del(jldel);
			
			String where=" where C_JLCODE is not null";
			List<City> citys=Server.getInstance().getHotelService().findAllCity(where, "", -1, 0);
			int cnum = citys.size();
			for (City city : citys) {
				System.out.println("城市数量："+cnum--);
				long time3 = System.currentTimeMillis();
				// 更新
				update(started, ended, dayJLData, profit, city.getId(),city.getJlcode());
				long time4 = System.currentTimeMillis();
				System.out.println("用时：" + (time4 - time3) / 1000 + "s");
		
				
			}
			long time2 = System.currentTimeMillis();
			System.out.println("用时：" + (time2 - time1) / 1000 / 60+"分钟");
			System.out.println("这次已经跑完，可以跑下一次了");
			String timess = sdfx.format(System.currentTimeMillis());
			String time5=sdfx.format(time1);
			System.out.println("开始时间："+time5);
			System.out.println("结束时间："+timess);
		}
	}

	/**
	 * 删除表删除数据
	 */
	
	public void del(List<JLPriceResult> jldel){
		for (int i = 0; i < jldel.size(); i++) {
			JLPriceResult jprs=jldel.get(i);
			String jlkeyid = jprs.getJlkeyid();
			String where = " where C_JLKEYID='"+jlkeyid+"'";
			List<HotelGoodData> hd = Server.getInstance().getHotelService().findAllHotelGoodData(where, "", -1, 0);
			if(hd.size()>0){
				Server.getInstance().getSystemService().findMapResultBySql(
						"delete from T_HOTELGOODDATA where c_jlkeyid='" + jlkeyid+"'", null);
				System.out.println("删除………………");
			}else{
				System.out.println("没对应");
			}
		}
	}
	/**
	 * 固定利润业务代码实现
	 * 
	 * @param hotel
	 * @param start
	 * @param end
	 * @param dayHMData
	 * @param profit
	 */

	public void update(String started, String ended,
			Map<String, List<JLPriceResult>> dayJLData, Float profit,
			Long cityid,String citycode) {
		PropertyUtil pu=new PropertyUtil();
		Float profit25 = Float.parseFloat(pu.getValue("profit2"));// 1000-2000
		Float profit2 = Float.parseFloat(pu.getValue("profit3"));// 2000+
		List<HotelGoodData> HotelGoodDatas = new ArrayList<HotelGoodData>();
		try {
			long time = Long.parseLong(pu.getValue("jielvtime"));// 更新间隔时间（秒）
			proc.loadUpdateRoomtype(started, ended, dayJLData, time, citycode);
			if (dayJLData != null) {
				List<JLPriceResult> result = dayJLData.get(citycode);// 华闽加载的数据结果
				int num=result.size();
				if (result != null) {

					label: for (JLPriceResult priceResult : result) {
						String hid = priceResult.getHotelid();
						String where = " where c_hotelcode='" + hid + "' and C_SOURCETYPE=6";
						List<Hotel> hotel = Server.getInstance().getHotelService().findAllHotel(where, "", -1,0);
						
						if (hotel.size() > 0) {
							
							String allot = priceResult.getAllot();
							String bf = priceResult.getBreakfast();
							String hname = priceResult.getHotelName();
							String pp = priceResult.getPprice();
							String rt = priceResult.getRoomtype();
							String sdate = priceResult.getStayDate();
							HotelGoodData good = new HotelGoodData();
							good.setAllotmenttype(priceResult.getAllotmenttype());
							good.setRatetype(priceResult.getRatetype());
							good.setRatetypeid(priceResult.getRatetypeid());
							good.setJlroomtypeid(priceResult.getRoomtypeid());
							good.setHotelid(hotel.get(0).getId());
							good.setBfcount(Long.valueOf(bf));
							good.setJlkeyid(priceResult.getJlkeyid());
							good.setCityid(String.valueOf(cityid));
							good.setJltime(priceResult.getJltime());
							good.setContractid(hotel.get(0).getHotelcode2());
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyy-MM-dd");
							Date qdate = sdf.parse(sdate);
							String xdate = sdf.format(qdate);
							good.setDatenum(xdate);
							good.setHotelname(hname);
							good.setSorucetype("6");
							if (priceResult.getLeadTime() != null
									&& !"".equals(priceResult.getLeadTime())
									&& !"null"
											.equals(priceResult.getLeadTime())) {
								good.setBeforeday(Long.parseLong(priceResult
										.getLeadTime()));
							} else {
								good.setBeforeday(0l);
							}
							if (priceResult.getMinDay() != null
									&& !"".equals(priceResult.getMinDay())
									&& !"null".equals(priceResult.getMinDay())) {
								good.setMinday(Long.parseLong(priceResult
										.getMinDay()));
							} else {
								good.setMinday(1l);
							}

							good.setOpenclose(Integer.parseInt(pu.getValue("openclose")));
							
							String wheres = "  where C_HOTELID="+ hotel.get(0).getId() + " and C_NAME='"+ rt + "'";
							List<Roomtype> typeid = Server.getInstance()
									.getHotelService().findAllRoomtype(wheres,
											"", -1, 0);
							good.setRoomtypename(rt+"("+priceResult.getRatetype()+")");
							if (typeid.size() > 0) {
								good.setRoomtypeid(typeid.get(0).getId());
								good.setYuliunum(0l);
								Double baseprice = Double.parseDouble(pp);
								int fee = 0;
								int lirun = 0;
								Double shijiprice = 0d;
								if (profit < 1) {
									if (baseprice < 1000) {
										fee = (int) (baseprice * 0.01 + baseprice * profit) + 12;
										lirun = (int) (baseprice * profit);
										shijiprice = baseprice + fee;
									} else if (baseprice > 1000 && baseprice < 2000) {
										fee = (int) (baseprice * 0.01 + baseprice * profit25) + 12;
										lirun = (int) (baseprice * profit25);
										shijiprice = baseprice + fee;
									} else if (baseprice > 2000) {
										fee = (int) (baseprice * 0.01 + baseprice * profit2) + 12;
										lirun = (int) (baseprice * profit2);
										shijiprice = baseprice + fee;
									}
								} else {
									fee = (int) (baseprice * 0.01)
											+ profit.intValue() + 12;
									shijiprice = baseprice + profit + fee;
								}
								good.setBaseprice((long) (Double.parseDouble(pp)));
								good.setProfit((long) lirun);
								good.setShijiprice(shijiprice.longValue());
								
								// 11为现付
								if ("11".equals(priceResult.getPricetypr())) {
									System.out.println("现付的房型");
									continue label;
								}
								//状态为查的数据
								if("null".equals(allot)&&Integer.parseInt(priceResult.getFangliang())<=0||
										"".equals(allot)&&Integer.parseInt(priceResult.getFangliang())<=0||
										allot==null&&Integer.parseInt(priceResult.getFangliang())<=0){
									good.setRoomstatus(1l);
									HotelGoodDatas.add(good);
									good.setJlft(allot);
									good.setJlf(priceResult.getFangliang());
									System.out.println("查");
									continue label;
								}

								//没有价格
								if ("0".equals(pp)) {
									good.setRoomstatus(1l);
									HotelGoodDatas.add(good);
									good.setJlft(allot);
									good.setJlf(priceResult.getFangliang());
									System.out.println("没有价格");
									continue label;
								}
								
								//待确认的
								if(!"16".equals(allot)
										&&!"12".equals(allot)
										&&Integer.parseInt(priceResult.getFangliang())==0
										){
									good.setRoomstatus(1l);
									good.setJlft(allot);
									good.setJlf(priceResult.getFangliang());
									System.out.println("待确认房");
									continue label;
								}
								//及时确认
								if (!"16".equals(allot)
										&&Integer.parseInt(priceResult.getFangliang())>0
										||"12".equals(allot)) {
									good.setYuliunum(1l);
									good.setRoomstatus(0l);
									good.setJlft(allot);
									good.setJlf(priceResult.getFangliang());
									System.out.println("及时确认房");
								} 
								//满房
								if("16".equals(allot)){
									good.setRoomstatus(1l);
									HotelGoodDatas.add(good);
									good.setJlft(allot);
									good.setJlf(priceResult.getFangliang());
									continue label;
								}
								good.setJlft(allot);
								good.setJlf(priceResult.getFangliang());
								System.out.println("正常确认房");
								good.setRoomstatus(0l);
								HotelGoodDatas.add(good);
								
							}			
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		proc.writeDataDB(HotelGoodDatas);
	}

	
	public static void copyFile(File sourceFile, File targetFile)
			throws IOException {
		BufferedInputStream inBuff = null;
		BufferedOutputStream outBuff = null;
		try {
			// 新建文件输入流并对它进行缓冲
			inBuff = new BufferedInputStream(new FileInputStream(sourceFile));

			// 新建文件输出流并对它进行缓冲
			outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));

			// 缓冲数组
			byte[] b = new byte[1024 * 5];
			int len;
			while ((len = inBuff.read(b)) != -1) {
				outBuff.write(b, 0, len);
			}
			// 刷新此缓冲的输出流
			outBuff.flush();
		} finally {
			// 关闭流
			if (inBuff != null)
				inBuff.close();
			if (outBuff != null)
				outBuff.close();
		}
	}



	public static int getXmlCount(String xmlstr) {
		SAXBuilder sb = new SAXBuilder();
		Document doc;
		int count = 0;
		try {
			doc = sb.build(new StringReader(xmlstr));
			Element root = doc.getRootElement();
			Element result = root.getChild("XML_RESULT");
			count = result.getChildren().size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}


}
