package com.ccservice.jielv.hotelupdate;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 持续更新价格job-4
 */
public class UpdateJlpriceJob4 implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新-捷旅-持续更新酒店价格job...");
		JLUpdateJob.updateJlpriceJob();
		System.out.println("结束更新--捷旅-持续更新酒店价格job...");
	}

}
