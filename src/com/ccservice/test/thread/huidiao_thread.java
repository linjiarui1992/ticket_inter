package com.ccservice.test.thread;

import java.net.URLEncoder;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.inter.server.Server;

/**
 * 重新回调采购问题订单
 * 
 * @time 2015年4月3日 下午7:00:42
 * @author chendong
 */
public class huidiao_thread extends Thread {
    Long orderid;

    String url;

    public huidiao_thread(Long orderid, String url) {
        super();
        this.url = url;
        this.orderid = orderid;
    }

    @Override
    public void run() {
        String result = "false";
        //        String url = "http://121.199.25.199:49216/cn_interface/tcTrainCallBack";
        String returnmsg = "true";
        //        returnmsg = "";
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_callback");
        jso.put("returnmsg", returnmsg);
        try {
            Server.getInstance().getSystemService()
                    .excuteAdvertisementBySql("update T_TRAINORDER set C_ORDERSTATUS=8 where ID=" + orderid);
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            System.out.println(orderid + ":" + result);
            if ("success".equals(result)) {
                createTrainorderrc(orderid, "回调成功", "chendong", 8);
                //                Server.getInstance().getSystemService()
                //                        .excuteAdvertisementBySql("update T_TRAINORDER set C_ISQUESTIONORDER=0 where ID=" + orderid);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId 火车票订单id
     * @param content 内容
     * @param createuser 用户
     * @param status 状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private static void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderId);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createuser);
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }
}
