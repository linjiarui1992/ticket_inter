package com.ccservice.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ccervice.util.db.DBHelperAccount;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.util.db.DBHelper;
import com.ccservice.b2b2c.util.db.DataRow;
import com.ccservice.b2b2c.util.db.DataTable;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;

public class CallBackAccountAndPassengerDemoByDb {

    public static void main(String[] args) {
        //        System.out.println(getNowTime());
        selectLocalAccount();
        //        String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
        //        String cookie = JobTrainUtil.getCookie(repUrl, "zhouxiehinijg474", "asd123456");
        //        String result = JobTrainUtil.initQueryUserInfo(cookie).toString();
        //        System.out.println(result);

        //        System.out.println(isNumeric("100012w"));

    }

    public static void selectLocalAccount() {
        String loginName = "";
        int customeruserid = 0;
        boolean isExists = false;
        do {
            if (getNowTime()) {
                try {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else {
                try {
                    Thread.sleep(10000L);
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            String sql2 = "select top 100 Id,AccountId from TongchengAccountDataManage where Flag=0";
            DataTable result = DBHelper.GetDataTable(sql2);
            List<DataRow> dataRows = result.GetRow();
            if (dataRows.size() > 0) {
                for (int i = 0; i < dataRows.size(); i++) {
                    long r1 = System.currentTimeMillis();
                    DataRow dataRow = dataRows.get(i);
                    customeruserid = isNumeric(dataRow.GetColumnString("AccountId")) == true ? Integer.parseInt(dataRow
                            .GetColumnString("AccountId")) : 0;
                    //                    loginName = dataRow.GetColumnString("LoginName");
                    int PKId = dataRow.GetColumnInt("Id");
                    System.out.println(r1 + ":---》" + customeruserid);
                    //                    updateExcuteStatus(customeruserid);
                    try {
                        //                        Thread.sleep(500L);
                        checkCustomeruserEnble(customeruserid, PKId);
                        ;

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            else {

                isExists = true;
            }

        }
        while (!isExists);

    }

    public static boolean isNumeric(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 查询线上帐号
     * 
     * @param loginNameId  帐号id
     * @throws Exception
     * @time 2016年1月7日 下午9:10:41
     * @author Administrator
     */
    public static void checkCustomeruserEnble(int loginNameId, int pkid) throws Exception {

        String sql = "select C_LOGINNAME,C_ISENABLE from T_CUSTOMERUSER WITH(NOLOCK) where ID=" + loginNameId;
        com.ccervice.util.db.DataTable result = DBHelperAccount.GetDataTable(sql);
        List<com.ccervice.util.db.DataRow> dataRows = result.GetRow();
        if (dataRows.size() > 0) {
            com.ccervice.util.db.DataRow dataRow = dataRows.get(0);
            //            String loginName = dataRow.GetColumnString("C_LOGINNAME");
            int isEnable = dataRow.GetColumnInt("C_ISENABLE");
            System.out.println(":DB---->" + isEnable);
            callBacktongchengMethod(loginNameId, isEnable, pkid);
        }
        else {
            Customeruser customeruser = new Customeruser();
            customeruser.setId(loginNameId);
            customeruser.setIsenable(51);
            CallBackPassengerUtil.callBackTongcheng(customeruser, new ArrayList<Trainpassenger>(), 2);
            updateExcuteStatus(pkid, 1);
        }
    }

    private static void callBacktongchengMethod(int loginNameId, int isEnable, int pkid) {
        if (isEnable == 1 || isEnable == 3 || isEnable == 4 || isEnable == 8 || isEnable == 10) {
            updateExcuteStatus(pkid, 2);
        }
        else {
            Customeruser customeruser = new Customeruser();
            customeruser.setId(loginNameId);
            customeruser.setIsenable(51);
            CallBackPassengerUtil.callBackTongcheng(customeruser, new ArrayList<Trainpassenger>(), 2);
            updateExcuteStatus(pkid, 1);
        }

    }

    /**
     * 
     * 
     * @param customeruserid
     * @time 2016年1月15日 下午7:46:21
     * @author Administrator
     */
    public static void updateExcuteStatus(int customeruserid, int flag) {
        String sql = "UPDATE TongchengAccountDataManage SET Flag=" + flag + " WHERE Id=" + customeruserid;
        WriteLog.write("updateExcuteStatus", sql);
        com.ccervice.util.db.DBHelper.executeSql(sql);
    }

    /**
     * 说明:接口可调用时间:早7晚11
     * 
     * @param date
     * @return
     * @time 2014年8月30日 下午4:23:20
     * @author yinshubin
     */
    public static boolean getNowTime() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("07:00:00");
            Date dateAfter = df.parse("23:00:00");
            Date time = df.parse(df.format(new Date()));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
            WriteLog.write("ID_身份验证接口异常", "获取时间出错");
        }
        return false;// 现在24小时,以后有需要再改为FALSE
    }
}
