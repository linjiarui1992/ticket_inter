/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.dao.TrainOfflineMatchAgentDao;
import com.ccservice.offline.dao.TrainOfflineTomAgentAddDao;
import com.ccservice.offline.domain.TrainOfflineMatchAgent;

/**
 * @className: com.ccservice.tuniu.util.TrainOrderOfflineUtil
 * @description: TODO - //线上测试使用的临时分单逻辑
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:51:24 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineUtilCS {
    private static TrainOfflineMatchAgentDao trainOfflineMatchAgentDao = new TrainOfflineMatchAgentDao();

    private static TrainOfflineTomAgentAddDao trainOfflineTomAgentAddDao = new TrainOfflineTomAgentAddDao();

    public static String distributionZS(String deliveryAddress, Integer createUid) throws Exception {
        //默认出票点
        String agentId = "";

        //既然目前的分单逻辑需要和淘宝保持一致-那么直接采用淘宝的匹配规则就好了-无需再次添加！！！
        //String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+createUid;
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=56";

        DataTable dataTable = DBHelperOffline.GetDataTable(sql1, null);
        if (dataTable.GetRow().size() == 0) {
            return "途牛的默认出票点的信息不存在-agentId" + agentId;
        }
        agentId = String.valueOf(dataTable.GetRow().get(0).GetColumn("agentId").GetValue());

        //中间差一个判断逻辑

        //程序自动分配出票点
        List<TrainOfflineMatchAgent> trainOfflineMatchAgentList = trainOfflineMatchAgentDao
                .findTrainOfflineMatchAgentListDetailByRandom();
        List listAgents = new ArrayList();
        for (int i = 0; i < trainOfflineMatchAgentList.size(); i++) {
            TrainOfflineMatchAgent trainOfflineMatchAgent = trainOfflineMatchAgentList.get(i);
            String provinces = trainOfflineMatchAgent.getProvinces();
            Integer agentid = trainOfflineMatchAgent.getAgentId();
            String[] add = provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                boolean flag = false;
                if (deliveryAddress.startsWith(add[j])) {
                    listAgents.add(agentid);
                    flag = true;
                }
                if (flag) {
                    break;
                }
            }
        }

        /**
         * 目前走的是随机分配的逻辑，
         * 
         * 后期的分单的方式和比例以及排名问题的处理可以采用之前的可操作的部分代码来处理
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.trainorderofflineadd(Trainorder)
         * 
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.offlineTicketShunt(String, String)
         * 之前列出的逻辑，但是并未使用
         * 
         */
        if (listAgents.size() > 0) {//如果不存在，会分配到默认的出票点
            int max = listAgents.size();
            int min = 1;
            Random random = new Random();
            int s = random.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= listAgents.size()) {
                agentId = listAgents.get(s - 1) + "";
            }
        }
        WriteLog.write("途牛线下新版分配订单", "agentId=" + agentId + ";deliveryAddress=" + deliveryAddress);

        //agentId = "382";//测试环境的测试账号
        /*qicailvtu1
        123456*/

        agentId = "414";//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentId;
    }

    public static String distributionCS(String deliveryAddress, Integer createUid) throws Exception {
        //默认出票点
        String agentId = "";

        //既然目前的分单逻辑需要和淘宝保持一致-那么直接采用淘宝的匹配规则就好了-无需再次添加！！！
        //String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+createUid;
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=56";

        DataTable dataTable = DBHelperOffline.GetDataTable(sql1, null);
        if (dataTable.GetRow().size() == 0) {
            return "途牛的默认出票点的信息不存在-agentId" + agentId;
        }
        agentId = String.valueOf(dataTable.GetRow().get(0).GetColumn("agentId").GetValue());

        //中间差一个判断逻辑

        //程序自动分配出票点
        List<TrainOfflineMatchAgent> trainOfflineMatchAgentList = trainOfflineMatchAgentDao
                .findTrainOfflineMatchAgentListDetailByRandom();
        List listAgents = new ArrayList();
        for (int i = 0; i < trainOfflineMatchAgentList.size(); i++) {
            TrainOfflineMatchAgent trainOfflineMatchAgent = trainOfflineMatchAgentList.get(i);
            String provinces = trainOfflineMatchAgent.getProvinces();
            Integer agentid = trainOfflineMatchAgent.getAgentId();
            String[] add = provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                boolean flag = false;
                if (deliveryAddress.startsWith(add[j])) {
                    listAgents.add(agentid);
                    flag = true;
                }
                if (flag) {
                    break;
                }
            }
        }

        /**
         * 目前走的是随机分配的逻辑，
         * 
         * 后期的分单的方式和比例以及排名问题的处理可以采用之前的可操作的部分代码来处理
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.trainorderofflineadd(Trainorder)
         * 
         * com.ccservice.b2b2c.atom.taobao.thread.MyThreadTaoBaoOrderOfflineAdd.offlineTicketShunt(String, String)
         * 之前列出的逻辑，但是并未使用
         * 
         */
        if (listAgents.size() > 0) {//如果不存在，会分配到默认的出票点
            int max = listAgents.size();
            int min = 1;
            Random random = new Random();
            int s = random.nextInt(max) % (max - min + 1) + min;
            if (s > 0 && s <= listAgents.size()) {
                agentId = listAgents.get(s - 1) + "";
            }
        }
        WriteLog.write("途牛线下新版分配订单", "agentId=" + agentId + ";deliveryAddress=" + deliveryAddress);

        agentId = "382";//测试环境的测试账号
        /*qicailvtu1
        123456*/

        //agentId="414";//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentId;
    }

    //配送到站的分单逻辑
    public static Integer distributionStationZS(String fromStationName) {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        Integer agentidTemp = null;
        try {
            agentidTemp = trainOfflineTomAgentAddDao.findAgentIdByTrainStation(fromStationName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (agentidTemp != null) {
            agentid = agentidTemp;
        }

        WriteLog.write("线下票配送到站分配订单", "agentid=" + agentid + ";fromStationName=" + fromStationName);

        //agentid = 382;//测试环境的测试账号
        /*qicailvtu1
        123456*/

        agentid = 414;//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentid;
    }

    //配送到站的分单逻辑
    public static Integer distributionStationCS(String fromStationName) {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        Integer agentidTemp = null;
        try {
            agentidTemp = trainOfflineTomAgentAddDao.findAgentIdByTrainStation(fromStationName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (agentidTemp != null) {
            agentid = agentidTemp;
        }

        WriteLog.write("线下票配送到站分配订单", "agentid=" + agentid + ";fromStationName=" + fromStationName);

        agentid = 382;//测试环境的测试账号
        /*qicailvtu1
        123456*/

        //agentid= 414;//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentid;
    }

    //配送到家的抢票订单的分单逻辑
    public static Integer distributionGrabTicketZS() {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        /**
         * 抢票的代售点的账号是有限的
         * 
         * 黄牛的账号 - 正式环境 - 分抢票单的逻辑 - 530
         * 
         * 其它抢票的代售点的账号
         */

        //agentid = 382;//测试环境的测试账号
        /*qicailvtu1
        123456*/

        agentid = 414;//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentid;
    }

    //配送到家的抢票订单的分单逻辑
    public static Integer distributionGrabTicketCS() {
        Integer agentid = 1;//agentId="1"; 默认的话，直接分给 正式环境的订单未分配的账号 - 进行操作

        agentid = 382;//测试环境的测试账号
        /*qicailvtu1
        123456*/

        //agentid= 414;//正式环境的测试账号
        /*guozhengju123
        123456*/

        return agentid;
    }

    //新增闪送的测试的四个分单逻辑方法 - 包含全部地址，与不包含全部地址

    /**
     * @description: TODO - 获取快递类型 - 包含闪送 - 此方法修正为判定是否是闪送的快递类型
     * 
     * @author liujun
     * @createTime: 2017年10月24日 上午10:54:20
     * @param totalAddress - 全部邮寄地址
     * @param cityName - 邮寄地址中的城市的名称
     * @param departTime - 发车时间,格式:2017-08-24 13:15:00
     * @return
     */
    public static JSONObject isRapidSendCS(String orderNumber, String totalAddress, String cityName,
            String departTime) {
        JSONObject result = new JSONObject();
        result.put("isRapidSend", false);
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "----发车时间：" + ":" + departTime + "----收件城市：" + ":" + cityName);
        if (cityName == null || "".equals(cityName)) {
            if (totalAddress.startsWith("北京") || totalAddress.startsWith("天津") || totalAddress.startsWith("上海")
                    || totalAddress.startsWith("重庆")) {
                cityName = totalAddress.substring(0, 2);
            }
            else if (totalAddress.contains("省")) {
                cityName = totalAddress.substring(totalAddress.indexOf("省") + 1, totalAddress.indexOf("市") + 1);
            }
            else {
                String[] provinces = PropertyUtil.getValue("default_uupt_express_province", "train.properties")
                        .split("/");
                for (int i = 0; i < provinces.length; i++) {
                    if (totalAddress.contains(provinces[i])) {
                        totalAddress = totalAddress.replace(provinces[i], "");
                        cityName = totalAddress.substring(0, totalAddress.indexOf("市") + 1);
                        break;
                    }
                }
            }
        }
        // 判断闪送城市
        boolean isRapidSendCity = false;
        String[] citys = PropertyUtil.getValue("rapidSend_city", "train.properties").split("/");
        for (int i = 0; i < citys.length; i++) {
            if (cityName.contains(citys[i])) {
                isRapidSendCity = true;
                break;
            }
        }
        // 如果是闪送城市
        if (isRapidSendCity) {
            // 当前时间
            Date departTimeDate = null;
            try {
                departTimeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(departTime);
            }
            catch (ParseException e) {
                WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "日期格式转换错误：departTime = " + departTime);
                // 记录异常日志
                ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
            }
            // 时间符合闪送条件
            if (TrainOrderOfflineUtil.isRapidSendDepartTime(departTimeDate)) {
                // 判断预算
                JSONObject jsonObject = TrainOrderOfflineUtil.distributionRapidSend(random, orderNumber, totalAddress,
                        cityName);
                double need_paymoney = Double.valueOf(jsonObject.getString("need_paymoney"));
                cityName = cityName.endsWith("市") ? cityName.substring(0, 2) : cityName;
                // 根据闪送城市匹配最低预算
                double budget = Double.valueOf(PropertyUtil.getValue(cityName, "train.properties"));
                if (need_paymoney <= budget) {
                    String agentId = jsonObject.getString("agentId");
                    result.put("isRapidSend", true);
                    result.put("need_paymoney", need_paymoney);

                    agentId = "382";//测试环境的测试账号
                    /*qicailvtu1
                    123456*/

                    //agentId= "414";//正式环境的测试账号
                    /*guozhengju123
                    123456*/

                    result.put("agentId", agentId);
                }
            }
        }
        return result;
    }

    public static JSONObject isRapidSendZS(String orderNumber, String totalAddress, String cityName,
            String departTime) {
        JSONObject result = new JSONObject();
        result.put("isRapidSend", false);
        int random = new Random().nextInt(9000000) + 1000000;
        WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "----发车时间：" + ":" + departTime + "----收件城市：" + ":" + cityName);
        if (cityName == null || "".equals(cityName)) {
            if (totalAddress.startsWith("北京") || totalAddress.startsWith("天津") || totalAddress.startsWith("上海")
                    || totalAddress.startsWith("重庆")) {
                cityName = totalAddress.substring(0, 2);
            }
            else if (totalAddress.contains("省")) {
                cityName = totalAddress.substring(totalAddress.indexOf("省") + 1, totalAddress.indexOf("市") + 1);
            }
            else {
                String[] provinces = PropertyUtil.getValue("default_uupt_express_province", "train.properties")
                        .split("/");
                for (int i = 0; i < provinces.length; i++) {
                    if (totalAddress.contains(provinces[i])) {
                        totalAddress = totalAddress.replace(provinces[i], "");
                        cityName = totalAddress.substring(0, totalAddress.indexOf("市") + 1);
                        break;
                    }
                }
            }
        }
        // 判断闪送城市
        boolean isRapidSendCity = false;
        String[] citys = PropertyUtil.getValue("rapidSend_city", "train.properties").split("/");
        for (int i = 0; i < citys.length; i++) {
            if (cityName.contains(citys[i])) {
                isRapidSendCity = true;
                break;
            }
        }
        // 如果是闪送城市
        if (isRapidSendCity) {
            // 当前时间
            Date departTimeDate = null;
            try {
                departTimeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(departTime);
            }
            catch (ParseException e) {
                WriteLog.write("线下火车票_采购快递判断_闪送", random + "：" + "日期格式转换错误：departTime = " + departTime);
                // 记录异常日志
                ExceptionUtil.writelogByException("线下火车票_采购快递判断_闪送", e, String.valueOf(random));
            }
            // 时间符合闪送条件
            if (TrainOrderOfflineUtil.isRapidSendDepartTime(departTimeDate)) {
                // 判断预算
                JSONObject jsonObject = TrainOrderOfflineUtil.distributionRapidSend(random, orderNumber, totalAddress,
                        cityName);
                double need_paymoney = Double.valueOf(jsonObject.getString("need_paymoney"));
                cityName = cityName.endsWith("市") ? cityName.substring(0, 2) : cityName;
                // 根据闪送城市匹配最低预算
                double budget = Double.valueOf(PropertyUtil.getValue(cityName, "train.properties"));
                if (need_paymoney <= budget) {
                    String agentId = jsonObject.getString("agentId");
                    result.put("isRapidSend", true);
                    result.put("need_paymoney", need_paymoney);

                    //agentId = "382";//测试环境的测试账号
                    /*qicailvtu1
                    123456*/

                    agentId = "414";//正式环境的测试账号
                    /*guozhengju123
                    123456*/

                    result.put("agentId", agentId);
                }
            }
        }
        return result;
    }

    /**
     * 根据地址判断发送快递
     * 
     * @param address 邮寄地址
     * @return 发送快递
     * @time 2017年10月13日 上午10:12:18
     * @author liujun
     */
    private static JSONObject splitAddress(String totaladdress, int random) {
        JSONObject result = new JSONObject();
        String province = "";
        String city = "";
        String county = "";
        String town = "";
        String address = "";
        try {
            totaladdress = totaladdress.replaceAll("-", "");
            address = totaladdress;
            if (totaladdress.startsWith("北京") || totaladdress.startsWith("天津") || totaladdress.startsWith("上海")
                    || totaladdress.startsWith("重庆")) {
                province = totaladdress.substring(0, 2);
                if (totaladdress.indexOf("市") == 4) {
                    city = totaladdress.substring(2, totaladdress.indexOf("市") + 1);
                    totaladdress = totaladdress.substring((province + city).length(), totaladdress.length());
                }
                else {
                    city = totaladdress.substring(3, totaladdress.indexOf("市", totaladdress.indexOf("市") + 1) + 1);
                    totaladdress = totaladdress.substring((province + "市" + city).length(), totaladdress.length());
                }
            }
            else {
                if (totaladdress.startsWith("新疆") || totaladdress.startsWith("西藏") || totaladdress.startsWith("广西")
                        || totaladdress.startsWith("内蒙古") || totaladdress.startsWith("宁夏 ")) {
                    province = totaladdress.substring(0, totaladdress.indexOf("自治区") + 3);
                }
                else if (totaladdress.contains("省")) {
                    province = totaladdress.substring(0, totaladdress.indexOf("省") + 1);
                }
                totaladdress = totaladdress.substring((province).length(), totaladdress.length());
                // 二级地址
                if (totaladdress.indexOf("自治州") > 0) {
                    city = totaladdress.substring(0, totaladdress.indexOf("自治州") + 3);
                }
                if (totaladdress.indexOf("盟") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("盟") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("盟") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("盟") + 1);
                        }
                    }
                }
                if (totaladdress.indexOf("区") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("区") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
                        }
                    }
                }
                if (totaladdress.indexOf("市") > 0) {
                    if (city.length() == 0) {
                        city = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                    }
                    else {
                        if (totaladdress.substring(0, totaladdress.indexOf("市") + 1).length() < city.length()) {
                            city = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                        }
                    }
                }
                totaladdress = totaladdress.substring((city).length(), totaladdress.length());
            }
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "省：" + ":" + province + "----市：" + ":" + city);
            // 三级地址
            if (totaladdress.indexOf("区") > 0) {
                county = totaladdress.substring(0, totaladdress.indexOf("区") + 1);
            }
            if (totaladdress.indexOf("市") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("市") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("市") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("县") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("县") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("县") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("县") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("旗") > 0) {
                if (county.length() == 0) {
                    county = totaladdress.substring(0, totaladdress.indexOf("旗") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("旗") + 1).length() < county.length()) {
                        county = totaladdress.substring(0, totaladdress.indexOf("旗") + 1);
                    }
                }
            }
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "区县：" + ":" + county);
            totaladdress = totaladdress.substring((county).length(), totaladdress.length());
            if (totaladdress.indexOf("街道") > 0) {
                town = totaladdress.substring(0, totaladdress.indexOf("街道") + 2);
            }
            if (totaladdress.indexOf("镇") > 0) {
                if (county.length() == 0) {
                    town = totaladdress.substring(0, totaladdress.indexOf("镇") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("镇") + 1).length() < town.length()) {
                        town = totaladdress.substring(0, totaladdress.indexOf("镇") + 1);
                    }
                }
            }
            if (totaladdress.indexOf("乡") > 0) {
                if (town.length() == 0) {
                    town = totaladdress.substring(0, totaladdress.indexOf("乡") + 1);
                }
                else {
                    if (totaladdress.substring(0, totaladdress.indexOf("乡") + 1).length() < town.length()) {
                        town = totaladdress.substring(0, totaladdress.indexOf("乡") + 1);
                    }
                }
            }

            WriteLog.write("线下火车票_采购快递判断", random + "：" + "乡镇街道：" + ":" + town);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
        }
        result.put("province", province);
        result.put("city", city);
        result.put("county", county);
        result.put("town", town);
        result.put("address", address.replace(province + city + county + town, ""));
        return result;
    }

    /**
     * 日期天数差计算
     * 
     * @param startdate 开始日期
     * @param enddate 结束日期
     * @return
     * @time 2017年10月12日 下午3:12:43
     * @author liujun
     */
    private static Double daysBetween(String startdate, String enddate, int random) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        try {
            cal1.setTime(sdf1.parse(startdate));
            if (cal1.get(Calendar.HOUR_OF_DAY) < 12) {
                cal1.setTime(sdf2.parse(startdate));
                cal1.add(Calendar.HOUR_OF_DAY, 12);
            }
            else {
                cal1.setTime(sdf2.parse(startdate));
                cal1.add(Calendar.DATE, 1);
            }
            cal2.setTime(sdf1.parse(enddate));
            if (cal2.get(Calendar.HOUR_OF_DAY) < 12) {
                cal2.setTime(sdf2.parse(enddate));
            }
            else {
                cal2.setTime(sdf2.parse(enddate));
                cal2.add(Calendar.HOUR_OF_DAY, 12);
            }
        }
        catch (ParseException e) {
            WriteLog.write("线下火车票_采购快递判断", random + "：" + "日期格式转换错误：" + ":" + startdate + "→" + enddate);
            // 记录异常日志
            ExceptionUtil.writelogByException("线下火车票_采购快递判断", e, String.valueOf(random));
            return new Double(0);
        }
        BigDecimal time1 = new BigDecimal(cal1.getTimeInMillis());
        BigDecimal time2 = new BigDecimal(cal2.getTimeInMillis());
        BigDecimal between_days = (time2.subtract(time1)).divide(new BigDecimal(1000 * 3600 * 24));
        return Double.valueOf(between_days.toString());
    }

}
