package com.ccservice.offline.util;

import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;

public class ExceptionCtripUtil {
    private static final String LOGNAME = "携程线下票异常信息集中处理工具类";

    private static int r1 = new Random().nextInt(10000000);

    //途牛的异常的集中处理类
    public static JSONObject handleCtripException(Exception e) {
        Boolean isSuccess = false;
        Integer msgCode = 231099;//success true用231000，false用231099

        JSONObject resResult = new JSONObject();
        
        resResult.put("isSuccess", isSuccess);
        resResult.put("msgCode", msgCode);

        WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-e-->"+e);

        return resResult;
    }
}
