package com.ccservice.offline.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MapSortUtil {

    /**
     * map 排序
     * 
     * @param map
     * @return
     * @time 2017年10月30日 上午10:05:29
     * @author liujun
     */
    public static ArrayList<Map.Entry<String, Double>> sortMap(Map<String, Double> map) {
        List<Map.Entry<String, Double>> entries = new ArrayList<Map.Entry<String, Double>>(map.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> obj1, Map.Entry<String, Double> obj2) {
                return obj1.getValue().compareTo(obj2.getValue());
            }
        });
        return (ArrayList<Entry<String, Double>>) entries;
    }
}
