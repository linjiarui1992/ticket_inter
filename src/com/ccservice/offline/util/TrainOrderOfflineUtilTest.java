/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.offline.dao.TrainOfflineExpressCodeDao;
import com.ccservice.offline.dao.TrainOfflineMatchAgentDao;
import com.ccservice.offline.dao.TrainOrderAgentTimesDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.domain.TrainOfflineMatchAgent;
import com.ccservice.offline.domain.TrainOrderAgentTimes;
import com.ccservice.offline.domain.TrainOrderOfflineRecord;

/**
 * @className: com.ccservice.tuniu.util.TrainOrderOfflineUtil
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:51:24 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineUtilTest {

    public void testTuNiuDesUtil() throws Exception {
        //String data = "LU3QL+qzsKbD1Su5mCvmn0S87Vg3QVfq5skFPjg3DMyUVDJYJpaXsuBNZX1WbkAC17eN9gxjsbJWsnZSmJPGgi6MxZE5ho1m17eN9gxjsbJZ/AQkQIlzq4+G20lPWey1kci2J+pI3L54hD7LHb9g4NbWefUcLlByYf0HnZL7H41YeyCBhI6LR1CP+SgPu8ILGmvQckSPc1xykdiZWXJ4nAZI+rlleJDWQVWTPDqf3vcE2OXLhasGkTSfW+myzhicimM7v9GvhADt/p0UhBc/AtbkQOk7ocgKvI3GQzNaFBcw0PlbJEX5ej+V2/czjTojjnY8SvHka7N/iwOrLWhsWkj58G/Q2vUZXe1EYA/MR6sdNhf3J7Vdi191H013skA5I5mLG9S/ym7CXlF4ZUVwqGxtjjELOdpIMmWBDOnCKwdk8pohMoZa494WMO06pUnnz5rwhI3JvamZaE0mMQavRlGPjkXHxjGT3uZX6ph08bOO1lpJST3a684XQT9JpDuHTvfDh40D3IZo0kQPE+JAMyFZhK8i091/7Zl8cRTAzXiVv5ad7xrG/KLE/LtpLy4D3hYw7TqlSecBbOMG4FbXVd81KT37/HeTfuv/6GsC2zPyQBD/WNM8+RqB13q+w9NaS2sEv1rrPOVk5CQVHj12GI6WgvY1ZousVkbE5r3IcdRHNaa7Q1lsCtFu/QhierNV2L33hKdHcD9MdvTGseZ3QdZ1nn/oK+m7THb0xrHmd0GTgbd1AU/jV0x29Max5ndBMl25/tftNxTtJL0qsPTtQiXnSZTDLj2HqDO96/INuBgLBjtpKotAwnT7Hj8fPD9cSM/y1qaWlMqbn7n0cRzfSTFkV+vliu2qpkQNhpSkbl3ZuFeKZKxpvYVd9CPEqHNz+nheZF0UgvcpL3NugV/9kbM3XXQ3HmKVUAuX4ocELEYJlDD8pgIAXC2MrZzUYVc4Q5BM1YKX19NdYK0xzpwIWag/MYKlf5hDEF3aK/JvNGTCD+HGMYSgNgm9KEqqaflAhdpT2rWP5+RdYK0xzpwIWXmx95HTe/oA5CKFyrojxQqSABQzCjhJC4YihUFa2UrEXcYakDh9z5+t/656EKhCHdollJXJrycPYZ+KRvkLD1XbwsotGEWxF1SZHUg6CJdvi24PKu/z9ZaNqnKr++AxpyW37U3257WyhBOcHhCS2My82/ftysYVVRgvjdw2DxPsGbwzjo/f/xBPjk44S1EJiF8VWLHdWMyQKJdcjIODIFTt/sy/afouBp/F4ufvoFN1KY8/9JLm41inPFyyPRLDDcVN2v+ElvCY7c43FQmxf0w5rWvp5RzXVjEisLkZd4HsYP08hE5gw6brhPp328zkv6WGQNlIr2tFlnH/iIFpIqrMxkDOqDKKv86KVlMeKRjqGUb9zTT0df736QHW7R1+sWD9PIROYMOm64T6d9vM5L/byru/UF/4w3iEPssdv2DgGC+N3DYPE+wJsfeyOi8Co6p2+ySg5B1DBh9cJ4HGQAhMzBMDKyjrRTdEWSo+gvu0R3mleMW/mVJZoo8XdbqzO2h6lFtmoN6WuH+mT8Kqn6noH7ewVB+OkINuNngVbwvESloyE3R5a3Dj0HXCWGmhplFf//8wzqgTlmS6W6KL4vrNE+Xko8ZPbAT8JrdmOFI35mR3LaUEunT6X4r3gRtBhxi5Jf+nIb9SxNmmWT7JAiCIZa7aTpk9/vwKUhcxOJIryaaEiJGmd2PXtpcju5xmRQ==";
        //String data = "LU3QL-qzsKbD1Su5mCvmn0S87Vg3QVfqE1JBqlOWWCs1D5GV5vSJmQ";
        /*String data = "HDBYG6RbHm5rNG-p-WkJSDutB1VYufsEDDOCwnHonnP-vMRYf7ynIsfIZo4m-xom0Z_zVpBKgoCwtG68yJ0qhUx1WcFM5yQi3icAMz6XGjyaZIccpP6eQHJ2a6hlNgXCQJtDHbrtq05MgzjFokmvbX8_6swHaY3g9MDur-iu0U0nV09IJ9mxlhiEe5cQ-_x9ja-XCUh61NB4yKkEvbE5Ewfn4zd9nIRCO_XzK7By_hbhHOFzYI9XWOlEg6R7gc7K3GomccKtQCRKAZGmAxTHCbDLYojqbD301XfG1GdeQbL4gykqHub0XK4Zqh8VrotDayPctzsvkM2WBSY3l4Gy3D1WlDy7Recfwg_hxjGEoDYaKZ4on2L0zlyKWq1f2Yf04PRkyj7Pall36ZtJkKBd6IOD67FHH8g6_YI622DASTa4ckCy3N-iFWKMFERcr984hxLGVnDfYfCgwIGLxnnagIidqFaPgbouQHJK1Pvca7snkTBBZILK_6ZBooQxbX04f8sqCbqZHuiVvjPW_26H2XcM_zdIYpgXTlSyS8drP0Xnutun-Oo1jWmkAJp8ZlQnzEXZKmyaqgFw8iN1Z1FfcGg1H7NrZBLu9NaDH0gz_QLn9V3IJdIBJtSiquvD2iF7lJnWRLGtVUfD1Su5mCvmn7O6559_Tbyl9S8DpWbGGqk1D5GV5vSJmQ";
        //身份校验 - 数据解密，签名校验
        //String dataStr = TuNiuDesUtil.decrypt(data);
        
        String account = "tuniulvyou";
        String timestamp = "2017-08-1810:34:29";
        String key = "v66r9ogtcvtxv3v4xq3gog8fqdbhwmt0";
        
        //传的是什么，就用什么解密
        String checkSign = SignUtil.generateSign(account, data, timestamp, key);*/

        //System.out.println(checkSign);//E185A7FDA6B7F04364FEBE0FFD22FE46
        
        //test17081825257714
        //System.out.println(TuNiuDesUtil.encrypt("{\"orderId\":\"test17081825257715\"}"));
        
        System.out.println(TuNiuDesUtil.decrypt("HDBYG6RbHm5rNG-p-WkJSDutB1VYufsEDDOCwnHonnP-vMRYf7ynIsfIZo4m-xom0Z_zVpBKgoCwtG68yJ0qhUx1WcFM5yQi3icAMz6XGjyaZIccpP6eQHJ2a6hlNgXCQJtDHbrtq05MgzjFokmvbX8_6swHaY3g9MDur-iu0U0nV09IJ9mxlhiEe5cQ-_x9ja-XCUh61NB4yKkEvbE5Ewfn4zd9nIRCO_XzK7By_hbhHOFzYI9XWOlEg6R7gc7K3GomccKtQCRKAZGmAxTHCbDLYojqbD301XfG1GdeQbL4gykqHub0XK4Zqh8VrotDayPctzsvkM2WBSY3l4Gy3D1WlDy7Recfwg_hxjGEoDYaKZ4on2L0zlyKWq1f2Yf04PRkyj7Pall36ZtJkKBd6IOD67FHH8g6_YI622DASTa4ckCy3N-iFWKMFERcr984hxLGVnDfYfCgwIGLxnnagIidqFaPgbouQHJK1Pvca7snkTBBZILK_6ZBooQxbX04f8sqCbqZHuiVvjPW_26H2XcM_zdIYpgXTlSyS8drP0Xnutun-Oo1jWmkAJp8ZlQnzEXZKmyaqgFw8iN1Z1FfcGg1H7NrZBLu9NaDH0gz_QLn9V3IJdIBJtSiquvD2iF7lJnWRLGtVUfD1Su5mCvmn7O6559_Tbyl9S8DpWbGGqk1D5GV5vSJmQ"));
    }

    public void getCancelOrderParam() throws Exception {
        //String data = "LU3QL+qzsKbD1Su5mCvmn0S87Vg3QVfq5skFPjg3DMyUVDJYJpaXsuBNZX1WbkAC17eN9gxjsbJWsnZSmJPGgi6MxZE5ho1m17eN9gxjsbJZ/AQkQIlzq4+G20lPWey1kci2J+pI3L54hD7LHb9g4NbWefUcLlByYf0HnZL7H41YeyCBhI6LR1CP+SgPu8ILGmvQckSPc1xykdiZWXJ4nAZI+rlleJDWQVWTPDqf3vcE2OXLhasGkTSfW+myzhicimM7v9GvhADt/p0UhBc/AtbkQOk7ocgKvI3GQzNaFBcw0PlbJEX5ej+V2/czjTojjnY8SvHka7N/iwOrLWhsWkj58G/Q2vUZXe1EYA/MR6sdNhf3J7Vdi191H013skA5I5mLG9S/ym7CXlF4ZUVwqGxtjjELOdpIMmWBDOnCKwdk8pohMoZa494WMO06pUnnz5rwhI3JvamZaE0mMQavRlGPjkXHxjGT3uZX6ph08bOO1lpJST3a684XQT9JpDuHTvfDh40D3IZo0kQPE+JAMyFZhK8i091/7Zl8cRTAzXiVv5ad7xrG/KLE/LtpLy4D3hYw7TqlSecBbOMG4FbXVd81KT37/HeTfuv/6GsC2zPyQBD/WNM8+RqB13q+w9NaS2sEv1rrPOVk5CQVHj12GI6WgvY1ZousVkbE5r3IcdRHNaa7Q1lsCtFu/QhierNV2L33hKdHcD9MdvTGseZ3QdZ1nn/oK+m7THb0xrHmd0GTgbd1AU/jV0x29Max5ndBMl25/tftNxTtJL0qsPTtQiXnSZTDLj2HqDO96/INuBgLBjtpKotAwnT7Hj8fPD9cSM/y1qaWlMqbn7n0cRzfSTFkV+vliu2qpkQNhpSkbl3ZuFeKZKxpvYVd9CPEqHNz+nheZF0UgvcpL3NugV/9kbM3XXQ3HmKVUAuX4ocELEYJlDD8pgIAXC2MrZzUYVc4Q5BM1YKX19NdYK0xzpwIWag/MYKlf5hDEF3aK/JvNGTCD+HGMYSgNgm9KEqqaflAhdpT2rWP5+RdYK0xzpwIWXmx95HTe/oA5CKFyrojxQqSABQzCjhJC4YihUFa2UrEXcYakDh9z5+t/656EKhCHdollJXJrycPYZ+KRvkLD1XbwsotGEWxF1SZHUg6CJdvi24PKu/z9ZaNqnKr++AxpyW37U3257WyhBOcHhCS2My82/ftysYVVRgvjdw2DxPsGbwzjo/f/xBPjk44S1EJiF8VWLHdWMyQKJdcjIODIFTt/sy/afouBp/F4ufvoFN1KY8/9JLm41inPFyyPRLDDcVN2v+ElvCY7c43FQmxf0w5rWvp5RzXVjEisLkZd4HsYP08hE5gw6brhPp328zkv6WGQNlIr2tFlnH/iIFpIqrMxkDOqDKKv86KVlMeKRjqGUb9zTT0df736QHW7R1+sWD9PIROYMOm64T6d9vM5L/byru/UF/4w3iEPssdv2DgGC+N3DYPE+wJsfeyOi8Co6p2+ySg5B1DBh9cJ4HGQAhMzBMDKyjrRTdEWSo+gvu0R3mleMW/mVJZoo8XdbqzO2h6lFtmoN6WuH+mT8Kqn6noH7ewVB+OkINuNngVbwvESloyE3R5a3Dj0HXCWGmhplFf//8wzqgTlmS6W6KL4vrNE+Xko8ZPbAT8JrdmOFI35mR3LaUEunT6X4r3gRtBhxi5Jf+nIb9SxNmmWT7JAiCIZa7aTpk9/vwKUhcxOJIryaaEiJGmd2PXtpcju5xmRQ==";
        //String data = "LU3QL-qzsKbD1Su5mCvmn0S87Vg3QVfqE1JBqlOWWCs1D5GV5vSJmQ";
        String data = TuNiuDesUtil.encrypt("{\"orderId\":\"test17081825257714\"}");
        //身份校验 - 数据解密，签名校验
        //String dataStr = TuNiuDesUtil.decrypt(data);
        
        String account = "tuniulvyou";
        String timestamp = "2017-08-1810:34:29";
        String key = "v66r9ogtcvtxv3v4xq3gog8fqdbhwmt0";
        
        //传的是什么，就用什么解密
        String checkSign = SignUtil.generateSign(account, data, timestamp, key);

        System.out.println(checkSign);//E185A7FDA6B7F04364FEBE0FFD22FE46
        
        //test17081825257714
        System.out.println("{\"account\":\""+account+"\",\"sign\":\""+checkSign
                +"\",\"timestamp\":\""+timestamp+"\",\"data\":\""+data+"\"}");
        
        //System.out.println(TuNiuDesUtil.decrypt("LU3QL+qzsKbD1Su5mCvmn0S87Vg3QVfq5skFPjg3DMyUVDJYJpaXsuBNZX1WbkAC17eN9gxjsbJWsnZSmJPGgi6MxZE5ho1m17eN9gxjsbJZ/AQkQIlzq4+G20lPWey1kci2J+pI3L54hD7LHb9g4NbWefUcLlByYf0HnZL7H41YeyCBhI6LR1CP+SgPu8ILGmvQckSPc1xykdiZWXJ4nAZI+rlleJDWQVWTPDqf3vcE2OXLhasGkTSfW+myzhicimM7v9GvhADt/p0UhBc/AtbkQOk7ocgKvI3GQzNaFBcw0PlbJEX5ej+V2/czjTojjnY8SvHka7N/iwOrLWhsWkj58G/Q2vUZXe1EYA/MR6sdNhf3J7Vdi191H013skA5I5mLG9S/ym7CXlF4ZUVwqGxtjjELOdpIMmWBDOnCKwdk8pohMoZa494WMO06pUnnz5rwhI3JvamZaE0mMQavRlGPjkXHxjGT3uZX6ph08bOO1lpJST3a684XQT9JpDuHTvfDh40D3IZo0kQPE+JAMyFZhK8i091/7Zl8cRTAzXiVv5ad7xrG/KLE/LtpLy4D3hYw7TqlSecBbOMG4FbXVd81KT37/HeTfuv/6GsC2zPyQBD/WNM8+RqB13q+w9NaS2sEv1rrPOVk5CQVHj12GI6WgvY1ZousVkbE5r3IcdRHNaa7Q1lsCtFu/QhierNV2L33hKdHcD9MdvTGseZ3QdZ1nn/oK+m7THb0xrHmd0GTgbd1AU/jV0x29Max5ndBMl25/tftNxTtJL0qsPTtQiXnSZTDLj2HqDO96/INuBgLBjtpKotAwnT7Hj8fPD9cSM/y1qaWlMqbn7n0cRzfSTFkV+vliu2qpkQNhpSkbl3ZuFeKZKxpvYVd9CPEqHNz+nheZF0UgvcpL3NugV/9kbM3XXQ3HmKVUAuX4ocELEYJlDD8pgIAXC2MrZzUYVc4Q5BM1YKX19NdYK0xzpwIWag/MYKlf5hDEF3aK/JvNGTCD+HGMYSgNgm9KEqqaflAhdpT2rWP5+RdYK0xzpwIWXmx95HTe/oA5CKFyrojxQqSABQzCjhJC4YihUFa2UrEXcYakDh9z5+t/656EKhCHdollJXJrycPYZ+KRvkLD1XbwsotGEWxF1SZHUg6CJdvi24PKu/z9ZaNqnKr++AxpyW37U3257WyhBOcHhCS2My82/ftysYVVRgvjdw2DxPsGbwzjo/f/xBPjk44S1EJiF8VWLHdWMyQKJdcjIODIFTt/sy/afouBp/F4ufvoFN1KY8/9JLm41inPFyyPRLDDcVN2v+ElvCY7c43FQmxf0w5rWvp5RzXVjEisLkZd4HsYP08hE5gw6brhPp328zkv6WGQNlIr2tFlnH/iIFpIqrMxkDOqDKKv86KVlMeKRjqGUb9zTT0df736QHW7R1+sWD9PIROYMOm64T6d9vM5L/byru/UF/4w3iEPssdv2DgGC+N3DYPE+wJsfeyOi8Co6p2+ySg5B1DBh9cJ4HGQAhMzBMDKyjrRTdEWSo+gvu0R3mleMW/mVJZoo8XdbqzO2h6lFtmoN6WuH+mT8Kqn6noH7ewVB+OkINuNngVbwvESloyE3R5a3Dj0HXCWGmhplFf//8wzqgTlmS6W6KL4vrNE+Xko8ZPbAT8JrdmOFI35mR3LaUEunT6X4r3gRtBhxi5Jf+nIb9SxNmmWT7JAiCIZa7aTpk9/vwKUhcxOJIryaaEiJGmd2PXtpcju5xmRQ=="));
    }

    public void testGetOverTimeDateByStr() throws Exception {
        System.out.println(TrainOrderOfflineUtil.getOverTimeDateByStr("2017-08-28 17:35:56.000").getTime());
        System.out.println(new Date().getTime());
        System.out.println(TrainOrderOfflineUtil.getOverTimeDateByStr("2017-08-28 17:35:56.000").getTime() > new Date().getTime());
    }

    public void testGetDelieveStr() throws Exception {
        System.out.println(TrainOrderOfflineUtil.getDelieveStr(1317196L, 437, "湖北省-宜昌市-西陵区-城东大道715306"));//
        System.out.println(DelieveUtils.getDelieveStr("437", "湖北省-宜昌市-西陵区-城东大道715306"));//
        //如果2017-08-31 18:00:00正常发件。快递类型为:标准快递。快递预计到达时间:2017-09-02 12:00:00,2017-09-02 12:00:00。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。
        
        /*String agentidtemp = TrainOrderOfflineUtil.distribution("郑州市", 81);
        System.out.println(agentidtemp);
        
        String aaa = TrainOrderOfflineUtil.getDelieveStr2(382, "陕西西安碑林区XXX路XXX号");
        
        System.out.println(aaa);*/
        
    }

    public void test1() throws Exception {
        String string = "false-出票回调超时=TN20170831153300744953";
        System.out.println(string.substring(0, string.indexOf("=")));//
        System.out.println(string.substring(string.indexOf("=")+1));//
        //如果2017-08-31 18:00:00正常发件。快递类型为:标准快递。快递预计到达时间:2017-09-02 12:00:00,2017-09-02 12:00:00。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。
    }
    
    public void test2() throws Exception {
        Long orderIdl = 1669L;
        String tongChengOfflineSendToStationSettlementURL = PropertyUtil.getValue("TongChengOfflineSendToStationSettlementURL", "Train.GuestAccount.properties");
        
        try {
            System.out.println(new HttpUtil().doGet(tongChengOfflineSendToStationSettlementURL, "?orderid="+orderIdl));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOrderOfflineUtilTest trainOrderOfflineUtilTest = new TrainOrderOfflineUtilTest();
        trainOrderOfflineUtilTest.testGetDelieveStr();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
