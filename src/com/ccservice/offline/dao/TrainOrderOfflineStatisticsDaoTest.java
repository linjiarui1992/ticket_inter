/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;


import com.ccservice.offline.domain.TrainOrderOfflineStatistics;

/**
 * @className: com.ccservice.tuniu.train.dao.T_CUSTOMERAGENTDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月04日 下午5:20:11 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineStatisticsDaoTest {
    private TrainOrderOfflineStatisticsDao trainOrderOfflineStatisticsDao = new TrainOrderOfflineStatisticsDao();

    public void testAddTrainOrderOfflineStatistics() throws Exception {
        //出票点出票统计
    	TrainOrderOfflineStatistics trainOrderOfflineStatistics = new TrainOrderOfflineStatistics();
    	trainOrderOfflineStatistics.setRanking(101);
        System.out.println(trainOrderOfflineStatisticsDao.addTrainOrderOfflineStatistics(trainOrderOfflineStatistics));
    }
    public void testFindrankingById() throws Exception {
        System.out.println(trainOrderOfflineStatisticsDao.findrankingById(5));
    }
    public void testDelTrainOfflineExpRecById() throws Exception {
       // System.out.println(trainOfflineExpRecDao.delTrainOfflineExpRecById(22));
    }
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOrderOfflineStatisticsDaoTest trainOrderOfflineStatisticsDaoTest = new TrainOrderOfflineStatisticsDaoTest();
        //trainOrderOfflineStatisticsDaoTest.testAddTrainOrderOfflineStatistics();
        trainOrderOfflineStatisticsDaoTest.testFindrankingById();
        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
