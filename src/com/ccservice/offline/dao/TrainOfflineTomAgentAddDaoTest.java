/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.util.ArrayList;
import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOrderOffline;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineTomAgentAddDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午6:23:17 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineTomAgentAddDaoTest {
    private TrainOfflineTomAgentAddDao trainOfflineTomAgentAddDao = new TrainOfflineTomAgentAddDao();

    public void testFindTrainStationList() throws Exception {
        List<String> trainStationList = trainOfflineTomAgentAddDao.findTrainStationList();
        System.out.println(trainStationList);
        System.out.println(trainStationList.contains("北京"));
        System.out.println(trainStationList.contains("北"));
        System.out.println(trainStationList.contains("南京"));
    }

    public void testFindAgentIdByTrainStation() throws Exception {
        System.out.println(trainOfflineTomAgentAddDao.findAgentIdByTrainStation("北"));
    }
    
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOfflineTomAgentAddDaoTest trainOfflineTomAgentAddDaoTest = new TrainOfflineTomAgentAddDaoTest();
        trainOfflineTomAgentAddDaoTest.testFindAgentIdByTrainStation();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
