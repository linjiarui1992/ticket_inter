/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ccervice.util.db.DataTable;

/**
 * @className: com.ccservice.yunku.app.train.ticket.dao.BeanHanlder
 * @description: TODO - 单独一个Bean的映射 - 遵循约定 - 得到数据库字段名，也就得到了JavaBan的字段名一致
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年3月2日 下午4:21:55 
 * @version: v 1.0
 * @since 
 *
 */
public class BeanListHanlder {

    private Class clazz;//目标类型

    public BeanListHanlder(Class clazz) {
        this.clazz = clazz;
    }

    public Object handle(DataTable dataTable) {
        try {
            List list = new ArrayList();
            for (int j = 0; j < dataTable.GetRow().size(); j++) {//有记录

                Object bean = clazz.newInstance();//目标对象

                int count = dataTable.GetRow().get(0).GetColumn().size();//列数

                for (int i = 0; i < count; i++) {
                    String fieldName = dataTable.GetRow().get(j).GetColumn().get(i).GetKey();//得到数据库字段名，也就得到了JavaBan的字段名

                    //判断该字段，在类中是否有对应的属性
                    Field[] fields = clazz.getDeclaredFields();

                    //使用新的方式
                    List listTmp = new ArrayList();

                    for (int k = 0; k < fields.length; k++) {
                        String tmp = fields[k].toString();
                        tmp = tmp.substring(tmp.lastIndexOf(".") + 1);
                        listTmp.add(tmp);
                    }
                    if (!listTmp.contains(fieldName)) {
                        continue;
                    }

                    Object fieldValue = dataTable.GetRow().get(j).GetColumn().get(i).GetValue();//字段值

                    if (fieldValue == null) {
                        continue;
                    }

                    if (BigDecimal.class.isAssignableFrom(fieldValue.getClass())) {
                        Double fieldValuedo = ((BigDecimal) fieldValue).doubleValue();

                        //通过字段反射
                        Field f = clazz.getDeclaredField(fieldName);

                        f.setAccessible(true);
                        f.set(bean, fieldValuedo);

                        continue;
                    }

                    if (Timestamp.class.isAssignableFrom(fieldValue.getClass())) {
                        String fieldValuedo = fieldValue.toString();
                        
                        //通过字段反射
                        Field f = clazz.getDeclaredField(fieldName);

                        f.setAccessible(true);
                        f.set(bean, fieldValuedo);

                        continue;
                    }

                    //通过字段反射
                    Field f = clazz.getDeclaredField(fieldName);

                    f.setAccessible(true);
                    f.set(bean, fieldValue);
                }
                list.add(bean);
            }
            return list;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
