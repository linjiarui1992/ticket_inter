package com.ccservice.offline.dao;

import java.sql.Timestamp;
import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainOrderOfflineRecord;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineRecordDao
 * @description: TODO -
 * @author: 郑州-技术-陈亚峰
 * @createTime: 2017年8月16日 下午03:36:02
 * @version: v 1.0
 * @since
 *
 */
public class TrainOrderOfflineRecordDao {
    /**
     * 创建管理员日志
     */
    public static Integer CreateLogAdmin(TrainOrderOfflineRecord trainOrderOfflineRecord) {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        Timestamp nowTimestamp = null;
        try {
            nowTimestamp = TrainOrderOfflineUtil.getTimestampByStr(nowDateStr);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        if (nowTimestamp == null) {
            return -10;
        }
        
        //显示手动加入的操作记录
        String sql = "OFFLINE_CreateLogAdmin @FKTrainOrderOfflineId="
                + trainOrderOfflineRecord.getFKTrainOrderOfflineId() 
                + ",@ProviderAgentid=" + trainOrderOfflineRecord.getProviderAgentid() 
                + ",@DistributionTime='" +  nowTimestamp 
                + "',@DealResult=" +  13 
                + ",@RefundReasonStr='" + trainOrderOfflineRecord.getRefundReasonStr() + "'";
        return DBHelperOffline.UpdateData(sql);
    }

    /**
     * @description: TODO - 
     * 
     * dealResult 0 - 订单已发放 - 刚接受订单
     * 
     * dealResult 1 - 订单出票完成
     * 
     * dealResult 2 - 订单被拒绝
     * 
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年9月28日 下午4:56:33
     * @param trainOrderOfflineRecord
     * @param dealResult
     * @return
     */
    public static Integer CreateLogAdminOtherDealResult(TrainOrderOfflineRecord trainOrderOfflineRecord, Integer dealResult) {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        Timestamp nowTimestamp = null;
        try {
            nowTimestamp = TrainOrderOfflineUtil.getTimestampByStr(nowDateStr);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        if (nowTimestamp == null) {
            return -10;
        }
        
        //显示手动加入的操作记录
        String sql = "OFFLINE_CreateLogAdmin @FKTrainOrderOfflineId="
                + trainOrderOfflineRecord.getFKTrainOrderOfflineId() 
                + ",@ProviderAgentid=" + trainOrderOfflineRecord.getProviderAgentid() 
                + ",@DistributionTime='" +  nowTimestamp 
                + "',@DealResult=" +  dealResult 
                + ",@RefundReasonStr='" + trainOrderOfflineRecord.getRefundReasonStr() + "'";
        return DBHelperOffline.UpdateData(sql);
    }

	// 插入订单详情页的操作记录
	public String addTrainOrderOfflineRecord(TrainOrderOfflineRecord trainOrderOfflineRecord) throws Exception {
		String sql = "OFFLINE_addTrainOrderOfflineRecord @FKTrainOrderOfflineId='"
				+ trainOrderOfflineRecord.getFKTrainOrderOfflineId() + "',@ProviderAgentid="
				+ trainOrderOfflineRecord.getProviderAgentid() + ",@DistributionTime='"
				+ trainOrderOfflineRecord.getDistributionTime() + "',@ResponseTime='"
				+ trainOrderOfflineRecord.getResponseTime() + "',@DealResult=" + trainOrderOfflineRecord.getDealResult()
				+ ",@RefundReason='" + trainOrderOfflineRecord.getRefundReason() + "',@RefundReasonStr='"
				+ trainOrderOfflineRecord.getRefundReasonStr() + "'";

		DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
		if (dataTable.GetRow().size() == 0) {
			return "订单详情页插入失败";
		}
		return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
	}

	// 根据ID获取订单对象
	public TrainOrderOfflineRecord findTrainOrderOfflineRecordById(Long Id) throws Exception {
		String sql = "OFFLINE_findTrainOrderOfflineRecordById @Id=" + Id;
		DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

		// SQLServerUtil.printTable(dataTable);

		if (dataTable.GetRow().size() == 0) {
			return null;
		}
		return (TrainOrderOfflineRecord) new BeanHanlder(TrainOrderOfflineRecord.class).handle(dataTable);
	}

    // 根据ID获取订单对象
    public Integer findDealResultCountByOrderId(Long FKTrainOrderOfflineId, Integer DealResult) throws Exception {
        String sql = "SELECT COUNT(0) AS num FROM TrainOrderOfflineRecord WHERE FKTrainOrderOfflineId = " + FKTrainOrderOfflineId + " AND DealResult = " + DealResult;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        // SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        if (dataTable.GetRow().get(0).GetColumn("num").GetValue() == null) {
            return null;
        }
        return (Integer) dataTable.GetRow().get(0).GetColumn("num").GetValue();
    }

    public Integer findLastRefuseProviderAgentidByOrderId(Long orderId) throws Exception {
        String sql = "SELECT ProviderAgentid FROM TrainOrderOfflineRecord WHERE FKTrainOrderOfflineId = " + orderId + " AND DealResult=2 AND ProviderAgentid!=46 ORDER BY PKId DESC";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        if (dataTable.GetRow().get(0).GetColumn("ProviderAgentid").GetValue() == null) {
            return null;
        }
        return (Integer) dataTable.GetRow().get(0).GetColumn("ProviderAgentid").GetValue();
    }

	// 根据ID删除订单对象-本地测试
	public Integer delTrainTicketOfflineById(Long Id) throws Exception {
		String sql = "DELETE FROM TrainOrderOfflineRecord WHERE Id=" + Id;
		return DBHelperOffline.UpdateData(sql);
	}

    // 根据ID删除订单对象-本地测试
    public Integer updateTrainOrderOfflineRecordResponseTimeByOrderIdAndProviderAgentid(Long FKTrainOrderOfflineId, Integer ProviderAgentid) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "UPDATE TrainOrderOfflineRecord SET ResponseTime='"+TrainOrderOfflineUtil.getTimestampByStr(nowDateStr)+
                "' WHERE FKTrainOrderOfflineId=" + FKTrainOrderOfflineId +"AND ProviderAgentid=" + ProviderAgentid;
        return DBHelperOffline.UpdateData(sql);
    }

    public List<TrainOrderOfflineRecord> findTrainOrderOfflineRecordListByOrderId(Long orderId) throws Exception {
        String sql = "SELECT * FROM TrainOrderOfflineRecord WHERE FKTrainOrderOfflineId=" + orderId +" AND ProviderAgentid>46 AND ProviderAgentid<1000  ORDER BY PKId ASC ";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainOrderOfflineRecord>) new BeanListHanlder(TrainOrderOfflineRecord.class).handle(dataTable);
    }

    public Integer updateTrainOrderOfflineRecordProviderAgentidByDealResult(Long orderId, Integer ProviderAgentid) throws Exception {
        String sql = "UPDATE TrainOrderOfflineRecord SET ProviderAgentid="+ProviderAgentid+" WHERE FKTrainOrderOfflineId=" + orderId
                +"AND DealResult=0";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineRecordProviderAgentidByProviderAgentid(Long orderId, Integer ProviderAgentid) throws Exception {
        String sql = "UPDATE TrainOrderOfflineRecord SET ProviderAgentid="+ProviderAgentid+" WHERE FKTrainOrderOfflineId=" + orderId
                +"AND ProviderAgentid>1000";
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer initOrderDealResult(Long orderId) {
        String sql = "UPDATE TrainOrderOfflineRecord SET DealResult=13 WHERE DealResult=0 AND FKTrainOrderOfflineId=" + orderId;
        return DBHelperOffline.UpdateData(sql);
    }

}
