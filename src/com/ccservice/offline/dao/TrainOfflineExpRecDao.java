/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineExpRec;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.T_CUSTOMERAGENTDao
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午10:18:36 
 * @version: v 1.0
 * @since E
 *
 */
public class TrainOfflineExpRecDao {

    //插入快递单号修改维护表信息，并返回订单号
    public String addTrainOfflineExpRec(TrainOfflineExpRec trainOfflineExpRec) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        //超时时间是从数据库中配置的？
        String sql = "OFFLINENEW_addTrainOfflineExpRec " + 
                "@orderid=" + trainOfflineExpRec.getOrderid()
                +",@newExpnum='" + trainOfflineExpRec.getNewExpnum()
                +"',@historyExpnum='" + trainOfflineExpRec.getHistoryExpnum()
                +"',@updateTime='" + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr)
                +"',@expressAgent='" + trainOfflineExpRec.getExpressAgent()
                +"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "快递单号修改维护表信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }
    /**
     * 根据Id查询newExpnum
     * @param Id
     * @return
     * @throws Exception
     */
    public String findnewExpnumById(int Id) throws Exception {
        String sql = "OFFLINENEW_findnewExpnumById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("newExpnum").GetValue());
    }
    
    //根据ID删除快递单号修改维护表信息-本地测试
    public Integer delTrainOfflineExpRecById(int Id) {
        String sql = "DELETE FROM TrainOfflineExpRec WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }
    
}
