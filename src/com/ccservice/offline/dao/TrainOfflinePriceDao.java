/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflinePrice;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.offline.dao.T_CUSTOMERAGENTDao
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午10:18:36 
 * @version: v 1.0
 * @since E
 *
 */
public class TrainOfflinePriceDao {
    
    //根据代售点的ID获取到其对应的省会的简称
    public String findAlternative1ByAgentId(String AgentId) throws Exception {
        String sql = "OFFLINENEW_findAlternative1ByAgentId @AgentId=" + AgentId;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Alternative1").GetValue());
    }

    //插入出票点手续费快递费维护信息，并返回订单号
    public String addTrainOfflinePrice(TrainOfflinePrice trainOfflinePrice) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        //超时时间是从数据库中配置的？
        String sql = "OFFLINENEW_addTrainOfflinePrice " + 
                "@AgentId=" + trainOfflinePrice.getAgentId()
                +",@Express1=" + trainOfflinePrice.getExpress1()
                +",@Express2=" + trainOfflinePrice.getExpress2()
                +",@shouxuPrice=" + trainOfflinePrice.getShouxuPrice()
                +",@Alternative1='" + trainOfflinePrice.getAlternative1()
                +"',@Alternative2='" + trainOfflinePrice.getAlternative2()
                +"',@Alternative3='" + trainOfflinePrice.getAlternative3()
                +"',@Express3=" + trainOfflinePrice.getExpress3();
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "出票点手续费快递费维护信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }
    
    //根据ID删除出票点手续费快递费维护信息-本地测试
    public Integer delTrainOfflinePriceById(int Id) {
        String sql = "DELETE FROM TrainOfflinePrice WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

}
