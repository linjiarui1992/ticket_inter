/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineConfigDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月23日 下午12:56:53 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineConfigDaoTest {
    private TrainOfflineConfigDao trainOfflineConfigDao = new TrainOfflineConfigDao();

    public void testAddTrainOfflineConfig() throws Exception {
        String KeyName = "CtripOfflineCookieStr222";
        System.out.println(trainOfflineConfigDao.addTrainOfflineConfig(KeyName, "2379245", "fjw1"));//返回的是主键
    }

    public void testFindTrainOfflineConfigByKeyName() throws Exception {
        String KeyName = "CtripOfflineCookieStr";
        System.out.println(trainOfflineConfigDao.findValueByKeyName(KeyName));
        System.out.println();
        System.out.println(trainOfflineConfigDao.findTrainOfflineConfigByKeyName(KeyName));

        System.out.println();
        System.out.println(trainOfflineConfigDao.updateValueByKeyName(KeyName, "1111", "fjw2"));
        
        System.out.println();
        System.out.println(trainOfflineConfigDao.findValueByKeyName(KeyName));
        System.out.println();
        System.out.println(trainOfflineConfigDao.findTrainOfflineConfigByKeyName(KeyName));
    }

    public void testDelTrainOfflineConfigByKeyName() throws Exception {
        String KeyName = "CtripOfflineCookieStr222";
        System.out.println(trainOfflineConfigDao.delTrainOfflineConfigByKeyName(KeyName));//
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOfflineConfigDaoTest trainOfflineConfigDaoTest = new TrainOfflineConfigDaoTest();
        
        //trainOfflineConfigDaoTest.testAddTrainOfflineConfig();
        trainOfflineConfigDaoTest.testDelTrainOfflineConfigByKeyName();
        //trainOfflineConfigDaoTest.testFindTrainOfflineConfigByKeyName();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
