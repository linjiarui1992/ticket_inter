/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainPassengerOffline
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午12:06:51 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTicketOffline {
    private Long Id;//
    private Long TrainPid;//火车票线下乘客ID
    private Long OrderId;//系统的订单号
    private String DepartTime;//出发日期 - 2016-06-28 18:10:00.000
    private String Departure;//出发站
    private String Arrival;//目的站
    private String TrainNo;//车次
    private Integer TicketType;//票类型:成人，小孩。儿童 1:成人票，2:儿童票，3:学生票，4:残军票
    private String SeatType;//席别
    private String SeatNo;//座位号 - 一般情况下不区分 这两个字段 - 统一放在一起 - //14车厢，19座上铺 - //15车 008
    private String Coach;//车厢
    private Double Price;//价格
    private String CostTime;//历时
    private String StartTime;//开车时间
    private String ArrivalTime;//到达时间
    /**
     * 实际出票信息
     */
    private Double sealPrice;//实际出票价格
    private String ticketNo;//出票成功之后回调的票号
    private String realSeat;//实际出票的座位类型
    private String subOrderId;//淘宝火车票子订单id
    public Long getId() {
        return Id;
    }
    public void setId(Long id) {
        Id = id;
    }
    public Long getTrainPid() {
        return TrainPid;
    }
    public void setTrainPid(Long trainPid) {
        TrainPid = trainPid;
    }
    public Long getOrderId() {
        return OrderId;
    }
    public void setOrderId(Long orderId) {
        OrderId = orderId;
    }
    public String getDepartTime() {
        return DepartTime;
    }
    public void setDepartTime(String departTime) {
        DepartTime = departTime;
    }
    public String getDeparture() {
        return Departure;
    }
    public void setDeparture(String departure) {
        Departure = departure;
    }
    public String getArrival() {
        return Arrival;
    }
    public void setArrival(String arrival) {
        Arrival = arrival;
    }
    public String getTrainNo() {
        return TrainNo;
    }
    public void setTrainNo(String trainNo) {
        TrainNo = trainNo;
    }
    public Integer getTicketType() {
        return TicketType;
    }
    public void setTicketType(Integer ticketType) {
        TicketType = ticketType;
    }
    public String getSeatType() {
        return SeatType;
    }
    public void setSeatType(String seatType) {
        SeatType = seatType;
    }
    public String getSeatNo() {
        return SeatNo;
    }
    public void setSeatNo(String seatNo) {
        SeatNo = seatNo;
    }
    public String getCoach() {
        return Coach;
    }
    public void setCoach(String coach) {
        Coach = coach;
    }
    public Double getPrice() {
        return Price;
    }
    public void setPrice(Double price) {
        Price = price;
    }
    public String getCostTime() {
        return CostTime;
    }
    public void setCostTime(String costTime) {
        CostTime = costTime;
    }
    public String getStartTime() {
        return StartTime;
    }
    public void setStartTime(String startTime) {
        StartTime = startTime;
    }
    public String getArrivalTime() {
        return ArrivalTime;
    }
    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }
    public Double getSealPrice() {
        return sealPrice;
    }
    public void setSealPrice(Double sealPrice) {
        this.sealPrice = sealPrice;
    }
    public String getTicketNo() {
        return ticketNo;
    }
    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }
    public String getRealSeat() {
        return realSeat;
    }
    public void setRealSeat(String realSeat) {
        this.realSeat = realSeat;
    }
    public String getSubOrderId() {
        return subOrderId;
    }
    public void setSubOrderId(String subOrderId) {
        this.subOrderId = subOrderId;
    }
    
    @Override
    public String toString() {
        return "TrainTicketOffline [Id=" + Id + ", TrainPid=" + TrainPid + ", OrderId=" + OrderId + ", DepartTime="
                + DepartTime + ", Departure=" + Departure + ", Arrival=" + Arrival + ", TrainNo=" + TrainNo
                + ", TicketType=" + TicketType + ", SeatType=" + SeatType + ", SeatNo=" + SeatNo + ", Coach=" + Coach
                + ", Price=" + Price + ", CostTime=" + CostTime + ", StartTime=" + StartTime + ", ArrivalTime="
                + ArrivalTime + ", sealPrice=" + sealPrice + ", ticketNo=" + ticketNo + ", realSeat=" + realSeat
                + ", subOrderId=" + subOrderId + "]";
    }
    
}
