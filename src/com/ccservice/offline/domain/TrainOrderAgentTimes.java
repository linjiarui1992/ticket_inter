/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainOrderAgentTimes
 * @description: TODO - 不同代售点的发送快递的时效的点位信息
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月29日 上午9:55:31 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderAgentTimes {
    private Integer id;
    private String agentId;
    private String fromcode;//
    private String time1;//
    private String time2;//
    private String remark1;//
    private String remark2;//
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAgentId() {
        return agentId;
    }
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
    public String getFromcode() {
        return fromcode;
    }
    public void setFromcode(String fromcode) {
        this.fromcode = fromcode;
    }
    public String getTime1() {
        return time1;
    }
    public void setTime1(String time1) {
        this.time1 = time1;
    }
    public String getTime2() {
        return time2;
    }
    public void setTime2(String time2) {
        this.time2 = time2;
    }
    public String getRemark1() {
        return remark1;
    }
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }
    public String getRemark2() {
        return remark2;
    }
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }
    @Override
    public String toString() {
        return "TrainOrderAgentTimes [id=" + id + ", agentId=" + agentId + ", fromcode=" + fromcode + ", time1=" + time1
                + ", time2=" + time2 + ", remark1=" + remark1 + ", remark2=" + remark2 + "]";
    }
    
}
