package com.ccservice.ctripoffsts.crack;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.ccservice.crack.ctrippffsts.TrainCtripOfflineUtil;

/**
 * @className: com.ccservice.component.BusSXLWSP.BusSXLWSPUtil
 * @description: TODO - 自测用户是否登录的程序
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年6月19日 下午3:01:15 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainCtripOfflineCheckIsLogin {
    private TrainCtripOfflineUtil trainCtripOfflineUtil = new TrainCtripOfflineUtil();

    public boolean checkIsLogin(String username) throws Exception {
        CloseableHttpClient defaultClient = HttpClients.createDefault();
        return trainCtripOfflineUtil.checkIsLogin(defaultClient, username);
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        System.out.println(new TrainCtripOfflineCheckIsLogin().checkIsLogin("TJZ01"));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
