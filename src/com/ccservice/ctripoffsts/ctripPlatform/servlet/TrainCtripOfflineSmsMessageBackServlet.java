package com.ccservice.ctripoffsts.ctripPlatform.servlet;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.util.RequestStreamUtil;
import com.ccservice.offlineExpress.util.CommonUtil;

public class TrainCtripOfflineSmsMessageBackServlet extends HttpServlet implements Servlet {

    private static final long serialVersionUID = 7875921500303712967L;

    private static final String logName = "线下火车票_客服获取携程验证码_ticket_inter";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //随机
        int random = CommonUtil.randomNum();
        //编码
        request.setCharacterEncoding("utf-8");
        //结果
        JSONObject responseJson = new JSONObject();
        boolean success = false;
        String msg = "";
        String code = "";
        try {
            // 请求参数转换
            JSONObject param = RequestStreamUtil.reqToJson(request);
            //日志
            WriteLog.write(logName, random + "---回调信息---" + param);
            if (param != null) {
                String remark1 = param.getString("message");
                String remark2 = param.getString("phone");
                String sql = "SELECT * FROM TrainOfflineConfig WHERE remark2='" + remark2 + "'";
                DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
                WriteLog.write(logName, random + "---根据手机号查询用户信息---" + dataTable);
                if (dataTable.GetRow().size() != 0) {
                    String uptSql = "UPDATE TrainOfflineConfig SET codeState = 1, remark1='" + remark1
                            + "' WHERE remark2='" + remark2 + "'";
                    int result = DBHelperOffline.UpdateData(uptSql);
                    if (result != 0) {
                        success = true;
                        msg = "获取成功";
                        code = "200";
                    }
                    else {
                        msg = "验证码获取失败";
                        code = "201";
                    }
                }
                else {
                    msg = "用户手机号错误";
                    code = "202";
                }
            }
            else {
                msg = "请求参数为空";
                code = "203";
            }
        }
        catch (Exception e) {
            msg = "请求参数解析失败";
            code = "204";
            //记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        // 统一返回
        responseJson.put("success", success);
        responseJson.put("msg", msg);
        responseJson.put("code", code);
        WriteLog.write(logName, random + "---回调结果---" + responseJson.toJSONString());
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/json; charset=UTF-8");
        response.getOutputStream().write(responseJson.toJSONString().getBytes(Charset.forName("UTF-8")));
    }
}
