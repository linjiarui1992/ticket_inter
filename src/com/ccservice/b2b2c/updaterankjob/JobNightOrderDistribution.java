/**
 * 
 */
package com.ccservice.b2b2c.updaterankjob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.offline.util.DistributionUtil;

/**
 * 扫描夜间单收录表中的订单
 * @time 2017年11月28日 下午19:14:29
 * @author zpy
 */
public class JobNightOrderDistribution implements Job {

    /* (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        execute();
    }

    //    public static void main(String[] args) {
    //        try {
    //            startScheduler("0 * 18 * * ?");
    //        }
    //        catch (Exception e) {
    //            // TODO Auto-generated catch block
    //            e.printStackTrace();
    //        }
    //    }

    /**
     * 
     * @time 2017年11月28日 下午2:16:52
     * @author zpy
     */
    private void execute() {
        long l1 = System.currentTimeMillis();
        WriteLog.write("JobNightOrderDistribution__expr", "l1" + l1 + ",夜间单JOB单次Start");
        //拿到夜间单收录agentid
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent with(nolock) WHERE status=3 AND createUid=" + "56";
        //        List list1 = Server.getSystemServiceOldDB().findMapResultBySql(sql1, null);
        try {
            DataTable list1 = DBHelperOffline.GetDataTable(sql1, null);
            String agentIdDefault = "7";
            if (list1.GetRow().size() > 0) {
                //            Map map = (Map) list1.get(0);
                DataRow map = list1.GetRow().get(0);
                agentIdDefault = map.GetColumnString("agentId");
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date as = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(as);
            cal.add(cal.DATE, -1);
            String today = sdf.format(as);
            String lastDay = sdf.format(cal.getTime());
            String beginTime = lastDay + " 22:50:00";
            String endTime = today + " 06:10:00";
            String now = sdfTime.format(as);
            //        System.out.println(beginTime + "," + endTime);
            //            if (true) {
            //                return;
            //            }
            //取出所有夜间单仓库中的订单
            String sql2 = " select DISTINCT a.id,a.ordernumber,a.agentid,c.departtime,b.address from TrainOrderOffline a with(nolock)"
                    + " join TrainTicketOffline c with(nolock) on c.OrderId=a.id "
                    + " join mailaddress b with(nolock) on a.id=b.orderid " + "where a.AgentId=" + agentIdDefault
                    + " and orderstatus = 1 and a.createtime between '" + beginTime + "' and '" + endTime + "'";
            //        List list2 = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql2, null);
            DataTable list2 = DBHelperOffline.GetDataTable(sql2, null);
            //判断夜间单表中是否有数据？
            if (list1.GetRow().size() > 0) {
                WriteLog.write("JobNightOrderDistribution__expr", "l1" + l1 + ",夜间单JOB当次数量=" + list1.GetRow().size());
                DistributionUtil distributionUtil = new DistributionUtil();
                for (int i = 0; i < list2.GetRow().size(); i++) {
                    //----拿到夜间单所需的邮寄信息和发车时间
                    //                Map map = (Map) list2.get(i);
                    DataRow map = list2.GetRow().get(i);
                    String orderId = map.GetColumnString("id");//订单id
                    String departTime = map.GetColumnString("departtime");//发车时间
                    String address = map.GetColumnString("address");//邮寄地址

                    //----调用分单逻辑方法获得重新分配的代售点id
                    String agentId = distributionUtil.distribution(l1, address, departTime, 2);//夜间收录轮询type=2
                    if (!agentId.equals(agentIdDefault)) {

                        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderId
                                + ",@ProviderAgentid=" + agentId + ",@DistributionTime='" + now
                                + "',@ResponseTime='',@DealResult=4,@RefundReason=0,@RefundReasonStr='夜间单自动分配'";
                        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表重新分配存储过程", procedureRecord);
                        DBHelperOffline.getResultByProc(procedureRecord, null);

                        //----修改仓库种的代售点id
                        String updateAgent = "update TrainOrderOffline set agentid=" + agentId + " where id=" + orderId
                                + " and orderstatus=1 ";
                        WriteLog.write("JobNightOrderDistribution__expr", "l1" + l1 + ",updateGradeSql=" + updateAgent);
                        //                    DBHelpUtil.getSystemServiceOldDB().excuteAdvertisementBySql(updateGrade);
                        DBHelperOffline.UpdateData(updateAgent);
                    }
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("JobNightOrderDistribution__expr", "l1" + l1 + ",Exception!!" + e.toString());
            e.printStackTrace();
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("JobNightOrderDistribution", "JobNightOrderDistributionGroup",
                JobNightOrderDistribution.class);// 任务名，任务组名，任务执行类
        WriteLog.write("JobNightOrderDistribution__expr", "expr=" + expr);
        CronTrigger trigger = new CronTrigger("JobNightOrderDistribution", "JobNightOrderDistributionGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
