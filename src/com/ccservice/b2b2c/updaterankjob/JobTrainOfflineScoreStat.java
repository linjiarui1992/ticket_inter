package com.ccservice.b2b2c.updaterankjob;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.updaterankjob.util.JobUtils;
import com.ccservice.inter.server.Server;
import com.ccservice.offline.util.DateUtil;

/**
 * 2017-11-15 9:14:56
 * @author zpy
 * 供应商得分计算
 *
 */
public class JobTrainOfflineScoreStat implements Job {
    private static int r1 = new Random().nextInt(10000000);

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //----写入日志
        WriteLog.write("JobTrainOfflineSubmenuStat_expr", r1 + "-begin...");
        Date as = new Date();

        try {
            //当天日期
            String dates = DateUtil.getNowDateStr();

            //提前三天的日期(包括今天)
            String threeDayDate = DateUtil.getDayBefore(as, 2);

            //提前七天的日期（包括今天）
            String sevenDayDate = DateUtil.getDayBefore(as, 6);
            //七天前的开始日期
            String sevenBeginDate = sevenDayDate + " 00:00:00";
            //七天前的截止日期
            String sevenEndDate = sevenDayDate + " 23:59:59";

            // 得出七天订单总数
            String allOrdersSql = "select allOrderSum from TrainOrderOfflineStatistics with(nolock) where staticDate between'"
                    + sevenDayDate + "' and '" + dates + "'";
            List allOrderList = Server.getInstance().getSystemService().findMapResultBySql(allOrdersSql, null);

            Map map = new HashMap();
            map = (Map) allOrderList.get(0);
            double sevenOrderNum = Double.parseDouble(map.get("allOrderSum").toString());

            // 取到当天存在效率的agentid
            String todayDatasql = "select agentId from TrainOrderOfflineStatistics with(nolock) where staticDate between'"
                    + threeDayDate + "' and '" + dates + "' group by agentid";
            List todayDatalist = Server.getInstance().getSystemService().findMapResultBySql(todayDatasql, null);

            // 计算近七天各供应商问题订单的总数
            String sevenQuestionSql = "  select ProviderAgentid as agentid,COUNT(PKId) as questionnum from TrainOrderOfflineRecord with (nolock)  "
                    + "where DistributionTime between '" + sevenBeginDate + "' and '" + sevenEndDate
                    + "'  and DealResult=223 group by ProviderAgentid";
            List sevenQuestionList = Server.getInstance().getSystemService().findMapResultBySql(sevenQuestionSql, null);// 得出供应商的问题订单
            //供应商近七天问题订单数量 
            Map<String, Double> agentQuestionMap = new HashMap<String, Double>();
            for (int i = 0; i < sevenQuestionList.size(); i++) {
                Map questionMap = (Map) sevenQuestionList.get(i);
                agentQuestionMap.put(questionMap.get("agentid").toString(),
                        Double.parseDouble(questionMap.get("questionnum").toString()));
            }

            Map<String, Double> maprank = new HashMap<String, Double>();

            System.out.println("共" + todayDatalist.size() + "代售点");
            //当天的概率统计表中有值时进行操作
            if (todayDatalist.size() > 0) {
                //遍历当天供应商效率附加排名
                for (int i = 0; i < todayDatalist.size(); i++) {
                    Map map2 = (Map) todayDatalist.get(i);

                    String agentId = map2.get("agentId").toString();

                    //为了减少出票时间和成功率不足三天的概率，所以取七天的最近三天数据做处理
                    String threeDataSql = "select top 3 finishTime,finishAvg from  TrainOrderOfflineStatistics  with (nolock)  where agentid="
                            + agentId + " and staticDate between'" + sevenDayDate + "' and '" + dates
                            + "' order by staticDate desc";
                    List threeDatalist = Server.getInstance().getSystemService().findMapResultBySql(threeDataSql, null);
                    Map<String, Double> map4 = new HashMap<String, Double>();

                    long finishTime = 0; // 出票时间
                    double finishAvg = 0; // 出票成功率

                    //得出近三天完成时间与成功率的平均值
                    if (threeDatalist.size() > 0) {
                        long tempFinishTime = 0; //临时完成时间储存
                        double tempFinishAvg = 0; ////临时成功率储存
                        for (int j = 0; j < threeDatalist.size(); j++) {
                            map = (Map) threeDatalist.get(j);
                            //完成时长
                            tempFinishTime += Long.parseLong(map.get("finishTime").toString());
                            //成功率
                            tempFinishAvg += Double.parseDouble(map.get("finishAvg").toString());
                        }
                        finishTime = tempFinishTime / threeDatalist.size();
                        finishAvg = tempFinishAvg / threeDatalist.size();
                    }
                    else {
                        //当不具有时长与排名时
                        continue;
                    }

                    //转换时长得分
                    int timeeff = JobUtils.getTimeSwitchScore(finishTime);
                    //成功率
                    double avgeff = finishAvg;

                    // 问题订单率  = 代售点七天问题数，比上出单总数
                    double questionRate = (1
                            - (agentQuestionMap.containsKey(agentId) ? agentQuestionMap.get(agentId) : 0)
                                    / sevenOrderNum)
                            * 100;

                    //得出成功率=时长*50%+成功率*25%+问题订单率*25%
                    double efficiency = 1 / (timeeff * 0.50 + avgeff * 0.25 + questionRate * 0.25);
                    BigDecimal bd = new BigDecimal(Double.parseDouble(efficiency + ""));
                    efficiency = bd.setScale(4, BigDecimal.ROUND_CEILING).doubleValue();

                    maprank.put(agentId, efficiency);
                }

                //根据值排序
                Map<String, Double> m = JobUtils.sortByValue(maprank);

                Set<String> keys = m.keySet();
                Iterator<String> it = keys.iterator();
                int k = 0;
                //排序后更新代售点排名
                while (it.hasNext()) {
                    //名次
                    k++;
                    //拿到key
                    String key = it.next();
                    WriteLog.write("---得分计算" + dates, "k=" + k + "-->" + key + "-->" + maprank.get(key));
                    //得分
                    double douGrade = maprank.get(key);
                    BigDecimal gradeBd = new BigDecimal(douGrade);
                    //保留两位小数
                    douGrade = gradeBd.setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue();
                    String updaterank = "update TrainOrderOfflineStatistics set grade=" + douGrade + " where agentid="
                            + key + " and Staticdate='" + dates + "'";

                    WriteLog.write("updateScoreStat_sql_" + dates, "updaterank=" + updaterank);
                    Server.getInstance().getSystemService().excuteAdvertisementBySql(updaterank);
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("JobTrainOfflineScoreStat_expr", r1 + "-供应商效率统计main异常-e-->" + e);
            e.printStackTrace();
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        System.out.println("开始UpdateRanking_StartJobTrainOfflineSubmenuStat");
        JobDetail jobDetail = new JobDetail("JobTrainOfflineSubmenuStat", "JobTrainOfflineSubmenuStatGroup",
                JobTrainOfflineScoreStat.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobTrainOfflineSubmenuStat", "JobTrainOfflineSubmenuStatGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}