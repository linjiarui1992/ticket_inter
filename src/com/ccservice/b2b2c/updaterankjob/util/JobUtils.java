package com.ccservice.b2b2c.updaterankjob.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 2017年11月15日10:39:21
 * @author zpy
 * 上级包内job所用的公共方法
 */
public class JobUtils {

    //map 按照值排序，降序
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static void main(String[] args) {
        Map map = new HashMap<>();
        map.put("aa", "52");
        map.put("bb", "56");
        map.put("cc", "54");
        map.put("dd", "51");
        map.put("ee", "50");
        Map m = sortByValue(map);

        Set set = m.keySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            System.out.println("key--" + key + ",value--" + m.get(key));

        }

    }

    /**
     * 出票成功时长与得分的转换
     * 
     * @param second
     * @return
     */
    public static int getTimeSwitchScore(long second) {
        int score = 0;// 得分
        if (second >= 0 && second < 180) {
            score = 100;
        }
        else if (second >= 180 && second < 300) {
            score = 90;
        }
        else if (second >= 300 && second < 480) {
            score = 80;
        }
        else if (second >= 480 && second < 720) {
            score = 70;
        }
        else if (second >= 720) {
            score = 60;
        }
        return score;

    }

}
