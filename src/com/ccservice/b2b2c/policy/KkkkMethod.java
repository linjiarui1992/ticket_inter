package com.ccservice.b2b2c.policy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.KkkkBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.webservice.kkkk.PlatformInterface;

/**
 * 
 * @author 贾建磊
 * 
 */
public class KkkkMethod extends SupplyMethod  implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final KkkkBook kkkkbook = new KkkkBook();// KKKK账号相关信息

	/**
	 * 根据航班获取政策
	 * 
	 * @param segmentInfos
	 *            行程list
	 * @param passengerType
	 *            乘客类型 0-成人 1-儿童
	 * @return
	 */
	public static List<Zrate> getPolicysRebateList(
			List<Segmentinfo> segmentInfos, String passengerType) {
		List<Zrate> zrateList = new ArrayList<Zrate>();
		try {
			StringBuffer flightSegements = new StringBuffer();
			if (segmentInfos != null && segmentInfos.size() > 0) {
				for (int i = 0; i < segmentInfos.size(); i++) {
					if (i == segmentInfos.size() - 1) {// 拼flightSegements的最后是不能带分号的，否则会报错
						Segmentinfo segmentinfo = segmentInfos.get(i);
						flightSegements.append(segmentinfo.getAircomapnycode()
								+ ","
								+ segmentinfo.getFlightnumber().substring(2,segmentinfo.getFlightnumber().length())
								+ ","
								+ segmentinfo.getDepartureCtiy()
								+ ","
								+ segmentinfo.getArriveCity()
								+ ","
								+ new SimpleDateFormat("HHmm")
										.format(segmentinfo.getDeparttime())
								+ ","
								+ new SimpleDateFormat("HHmm")
										.format(segmentinfo.getArrivaltime())
								+ "," + segmentinfo.getDepartureDate() + ","
								+ segmentinfo.getCabincode());
					} else {
						Segmentinfo segmentinfo = segmentInfos.get(i);
						flightSegements.append(segmentinfo.getAircomapnycode()
								+ ","
								+ segmentinfo.getFlightnumber().substring(2,segmentinfo.getFlightnumber().length())
								+ ","
								+ segmentinfo.getDepartureCtiy()
								+ ","
								+ segmentinfo.getArriveCity()
								+ ","
								+ new SimpleDateFormat("HHmm")
										.format(segmentinfo.getDeparttime())
								+ ","
								+ new SimpleDateFormat("HHmm")
										.format(segmentinfo.getArrivaltime())
								+ "," + segmentinfo.getDepartureDate() + ","
								+ segmentinfo.getCabincode() + ";");
					}
				}
			}
			WriteLog.write("根据航班获取政策（KKKK）", "根据航班获取政策向接口中传入的数据[行程信息:"
					+ flightSegements.toString() + "-----乘客类型:" + passengerType
					+ "]");
			if (kkkkbook.getKkkkStaus().equals("1")) {
				PlatformInterface pfi = new PlatformInterface();
				String result = pfi.getPlatformInterfaceSoap()
						.getPolicysRebateList(kkkkbook.getKkkkUnitID(),
								flightSegements.toString(), passengerType, "",
								"0", kkkkbook.getKkkkSaftNo(), "2");
				//WriteLog.write("KKKK接口日志", "根据航班获取政策从接口中返回的数据" + result + "]");
				Document document = DocumentHelper.parseText(result);
				Element root = document.getRootElement();
				if (root.attributeValue("ecode") != null
						&& root.attributeValue("ecode").equals("0000")) {
					List pList = (List) root.elements("p");
					Iterator it = pList.iterator();
					while (it.hasNext()) {
						Element pElement = (Element) it.next();
						Zrate zrate = new Zrate();
						zrate.setIsenable(1);
						if (pElement.attributeValue("a0") != null) {
							zrate.setOutid(pElement.attributeValue("a0"));
						}
						if (pElement.attributeValue("a1") != null) {
							zrate.setRatevalue(100 * Float.valueOf(pElement
									.attributeValue("a1")));
						}
						if (pElement.attributeValue("a2") != null) {
							zrate.setWorktime(pElement.attributeValue("a2"));
						}
						if (pElement.attributeValue("a3") != null) {
							zrate.setAfterworktime(pElement
									.attributeValue("a3"));
						}
						if (pElement.attributeValue("a4") != null) {
							zrate.setWeekendworktime(pElement
									.attributeValue("a4"));
						}
						if (pElement.attributeValue("a5") != null) {
							zrate.setWeekendaftertime(pElement
									.attributeValue("a5"));
						}
						if (pElement.attributeValue("a6") != null) {
							zrate.setOnetofivetuipiaoworktime(pElement
									.attributeValue("a6"));
						}
						if (pElement.attributeValue("a7") != null) {
							zrate.setOnetofivetuipiaoaftertime(pElement
									.attributeValue("a7"));
						}
						if (pElement.attributeValue("a8") != null) {
							zrate.setWeekendtuipiaoworktime(pElement
									.attributeValue("a8"));
						}
						if (pElement.attributeValue("a9") != null) {
							zrate.setWeekendtuipiaoaftertime(pElement
									.attributeValue("a9"));
						}
						if (pElement.attributeValue("a10") != null) {
							zrate.setOnetofivefeipiaoworktime(pElement
									.attributeValue("a10"));
						}
						if (pElement.attributeValue("a11") != null) {
							zrate.setOnetofivetfeipiaoaftertime(pElement
									.attributeValue("a11"));
							zrate.setOnetofivewastetime(pElement
									.attributeValue("a11"));
						}
						if (pElement.attributeValue("a12") != null) {
							zrate.setWeekendfeipiaoworktime(pElement
									.attributeValue("a12"));
						}
						if (pElement.attributeValue("a13") != null) {
							zrate.setWeekendfeipiaoaftertime(pElement
									.attributeValue("a13"));
							zrate.setWeekendwastetime(pElement
									.attributeValue("a13"));
						}
						if (pElement.attributeValue("a14") != null) {
							String tickettype = pElement.attributeValue("a14");
							if ("0".equals(tickettype)) {
								zrate.setTickettype(2);
							} else {
								zrate.setTickettype(1);
							}
						}
						if (pElement.attributeValue("a15") != null) {
							zrate.setWhetherincoding(pElement
									.attributeValue("a15"));
						}
						if (pElement.attributeValue("a16") != null) {
							zrate.setZtype(pElement.attributeValue("a16"));
							zrate.setGeneral(Long.parseLong(pElement
									.attributeValue("a16")) + 1);
						}
						if (pElement.attributeValue("a17") != null) {
							zrate.setWhetherthehighreturn(pElement
									.attributeValue("a17"));
						}
						if (pElement.attributeValue("a18") != null) {
							zrate.setRemark(pElement.attributeValue("a18"));
						}
						if (pElement.attributeValue("a19") != null) {
							zrate.setOfficeno(pElement.attributeValue("a19"));
						}
						zrate.setAgentid(9L);
						WriteLog.write("根据航班获取政策（KKKK）", "根据航班获取政策从接口中接收的数据[政策id:"
								+ zrate.getOutid() + "---政策返点:"
								+ zrate.getRatevalue() + "---政策类型:"
								+ zrate.getGeneral() + "]");
						if (zrate.getRatevalue() > 0) {
							zrateList.add(zrate);
						}
					}
				} else {
					WriteLog.write("根据航班获取政策（KKKK）", "根据航班获取政策从接口中接收的错误信息[" + result
							+ "]");
					return zrateList;
				}
			} else {
				System.out.println("KKKK账号：被禁用");
				return zrateList;
			}
		} catch (DocumentException e) {
			e.printStackTrace();
			return zrateList;
		}
		return zrateList;
	}

	/**
	 * 通过pnr信息创建订单
	 * 
	 * @param order
	 * @param policyId
	 *            政策ID
	 * @param settleAmount
	 *            支付金额（无论小数是多少都要舍掉小数位向整数位进一）儿童传pat票面价
	 * @param passengerType
	 *            乘机人类型 0->成人;1->儿童;2->婴儿
	 * @param adultTicketNos
	 *            成人票号 儿童生成订单，多个采用“，”分割 如果不定儿童票，可以为空
	 * @return 如果未创建成功返回 -1 成功：返回 "S|" + orderNO + "|" + payurl + "|" + officeid +
	 *         "|"+ payfee;
	 */
	public String createOrderByPNRInfo(Orderinfo order, String policyId,
			String settleAmount, String passengerType, String adultTicketNos) {
		try {
			WriteLog.write("通过pnr信息创建订单（KKKK）", "通过pnr信息创建订单向接口中传入的数据[rt信息："
					+ order.getUserRtInfo() + "---pat信息:"
					+ order.getPnrpatinfo() + "---政策Id:" + policyId + "---联系人:"
					+ kkkkbook.getKkkkLinkName() + "---联系人电话:"
					+ kkkkbook.getKkkkLinkMobile() + "---支付金额:" + settleAmount
					+ "---pnr:" + order.getPnr() + "---乘机人类型:" + passengerType
					+ "---票号:" + adultTicketNos + "]");
			if (kkkkbook.getKkkkStaus().equals("1")) {
				PlatformInterface pfi = new PlatformInterface();
				String result = pfi.getPlatformInterfaceSoap()
						.createOrderByPNRInfo(kkkkbook.getKkkkUnitID(),
								order.getUserRtInfo(), order.getPnrpatinfo(),
								order.getPnr(), "C", passengerType, policyId,
								kkkkbook.getKkkkLinkName(),
								kkkkbook.getKkkkLinkMobile(), "", "0",
								kkkkbook.getKkkkSaftNo(), "2", settleAmount,
								adultTicketNos);
				// WriteLog.write("KKKK接口日志", "通过pnr信息创建订单从接口中返回的数据" + result +
				// "]");
				Document document = DocumentHelper.parseText(result);
				Element root = document.getRootElement();
				if (root.attributeValue("ecode") != null
						&& root.attributeValue("ecode").equals("0000")) {
					Element oElement = root.element("o");
					String orderNo = oElement.attributeValue("a0");// 订单号
					WriteLog.write("通过pnr信息创建订单（KKKK）", "通过pnr信息创建订单从接口中返回的订单号：" + orderNo);
					String officeid = oElement.attributeValue("a12");// officeid
					String payfee = oElement.attributeValue("a33");// 采购应付金额
					String payurl = getPayUrl(orderNo);
					String resultInfo = "-1";
					if (!payurl.equals("FAIL")) {
						resultInfo = "S|" + orderNo + "|" + payurl + "|"
								+ officeid + "|" + payfee;
						WriteLog.write("通过pnr信息创建订单（KKKK）", "通过pnr信息创建订单：" + resultInfo);
					}
					return resultInfo;
				} else {
					WriteLog.write("通过pnr信息创建订单（KKKK）", "通过pnr信息创建订单从接口中接收的错误信息："
							+ result);
					return "-1";
				}
			} else {
				System.out.println("KKKK账号：被禁用");
				return "-1";
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return "-1";
	}

	/**
	 * 获取支付链接
	 * 
	 * @param orderNo
	 *            外部订单号
	 * @return
	 */
	public String getPayUrl(String orderNo) {
		if (kkkkbook.getKkkkStaus().equals("1")) {
			String payUrl = "";
			if (kkkkbook.getKkkkPayUrl() != null) {
				payUrl = kkkkbook.getKkkkPayUrl() + "?user="
						+ kkkkbook.getKkkkUnitID() + "&resultFormat=0&sign="
						+ kkkkbook.getKkkkSaftNo() + "&version=2.0&orderId="
						+ orderNo;
				WriteLog.write("获取支付链接（KKKK）", "获取支付链接：" + payUrl);
				return payUrl;
			} else {
				WriteLog.write("获取支付链接（KKKK）", "获取支付链接：" + payUrl);
			}
		} else {
			System.out.println("KKKK账号：被禁用");
			return "FAIL";
		}
		return "FAIL";
	}

	/**
	 * 自动代扣
	 * 
	 * @param orderNo
	 *            外部订单号
	 * @param pnr
	 *            生成的pnr
	 * @param orderId
	 *            本地订单号
	 * @param payType
	 *            支付类型：1.支付宝；2财付通；3汇付
	 * @return
	 */
	public String autoPayOrder(String orderNo, String pnr, String orderId,
			String payType) {
		String resultStr = "-1";
		try {
			if (kkkkbook.getKkkkStaus().equals("1")) {
				PlatformInterface pfi = new PlatformInterface();
				WriteLog.write("自动代扣", "KKKK订单自动代扣向接口中传入的数据[外部订单号：" + orderNo
						+ "---pnr:" + pnr + "---本地订单号:" + orderId + "---支付类型:"
						+ payType + "]");
				String result = pfi.getPlatformInterfaceSoap().payOrder(
						kkkkbook.getKkkkUnitID(), orderNo, pnr, orderId,
						payType, "0", kkkkbook.getKkkkSaftNo(), "2" + "]");
				WriteLog.write("自动代扣", "订单自动代扣从接口中返回的数据：" + result);
				Document document = DocumentHelper.parseText(result);
				Element root = document.getRootElement();
				if (root.attributeValue("ecode") != null
						&& root.attributeValue("ecode").equals("0000")
						&& root.getTextTrim() != null
						&& root.getTextTrim().equals("支付提交成功")) {
					resultStr = "S";
					String sql = "UPDATE T_ORDERINFO SET "
							+ Orderinfo.COL_extorderstatus + "='2' WHERE ID="
							+ orderId;
					Server.getInstance().getSystemService().findMapResultBySql(
							sql, null);
					return "S";
				} else {
					return resultStr;
				}
			} else {
				System.out.println("KKKK账号：被禁用");
				return resultStr;
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return resultStr;
	}

	public static void main(String[] args) {
		KkkkMethod kkk = new KkkkMethod();
		kkk.autoPayOrder("", "", "", "");
	}
	
	public static KkkkBook getKkkkbook() {
		return kkkkbook;
	}
}