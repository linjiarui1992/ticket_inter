package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class CtripBook {
    // 以下是携程接口需要参数
    private String ctripuser = "";

    private String ctrippwd = "";

    private String ctripkey = "";

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    public CtripBook() {
        List<Eaccount> listEaccountctrip = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(14);
        if (e != null && e.getName() != null) {
            listEaccountctrip.add(e);
        }
        if (listEaccountctrip.size() > 0) {
            ctripuser = listEaccountctrip.get(0).getUsername();
            ctrippwd = listEaccountctrip.get(0).getPassword();
            ctripkey = listEaccountctrip.get(0).getKeystr();
            staus = listEaccountctrip.get(0).getState();
        }
        else {
            staus = "0";
            System.out.println("NO-ctrip");
        }
    }

    public String getCtripuser() {
        return ctripuser;
    }

    public void setCtripuser(String ctripuser) {
        this.ctripuser = ctripuser;
    }

    public String getCtrippwd() {
        return ctrippwd;
    }

    public void setCtrippwd(String ctrippwd) {
        this.ctrippwd = ctrippwd;
    }

    public String getCtripkey() {
        return ctripkey;
    }

    public void setCtripkey(String ctripkey) {
        this.ctripkey = ctripkey;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

}
