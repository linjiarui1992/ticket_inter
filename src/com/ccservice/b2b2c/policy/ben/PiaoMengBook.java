package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class PiaoMengBook {
    // 以下是票盟接口需要参数
    private String piaomenguser = "";

    private String piaomengpwd = "";

    private String piaomengkey = "";

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    public PiaoMengBook() {
        List<Eaccount> listEaccountpiaomeng = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(4);
        if (e != null && e.getName() != null) {
            listEaccountpiaomeng.add(e);
        }
        if (listEaccountpiaomeng.size() > 0) {
            piaomenguser = listEaccountpiaomeng.get(0).getUsername();
            piaomengpwd = listEaccountpiaomeng.get(0).getPassword();
            piaomengkey = listEaccountpiaomeng.get(0).getKeystr();
            staus = listEaccountpiaomeng.get(0).getState();
        }
        else {
            staus = "0";
            System.out.println("NO-piaomeng");
        }
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getPiaomenguser() {
        return piaomenguser;
    }

    public void setPiaomenguser(String piaomenguser) {
        this.piaomenguser = piaomenguser;
    }

    public String getPiaomengpwd() {
        return piaomengpwd;
    }

    public void setPiaomengpwd(String piaomengpwd) {
        this.piaomengpwd = piaomengpwd;
    }

    public String getPiaomengkey() {
        return piaomengkey;
    }

    public void setPiaomengkey(String piaomengkey) {
        this.piaomengkey = piaomengkey;
    }

}
