package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

/**
 * qunar优选
 * @author wzc
 *
 */
public class QunarBestBook {
    private String qunarurl = "";// qunar接口访问地址

    private String vendorID = "";// 客户端来源

    private String registname = "";

    private String signKey = "";

    private String linkmobile = "";

    private String status;

    public QunarBestBook() {
        List<Eaccount> listEaccount = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(17);
        if (e != null && e.getName() != null) {
            listEaccount.add(e);
        }
        if (listEaccount.size() > 0) {
            qunarurl = listEaccount.get(0).getUrl();
            registname = listEaccount.get(0).getUsername();
            signKey = listEaccount.get(0).getKeystr();
            vendorID = listEaccount.get(0).getXiausername();
            linkmobile = listEaccount.get(0).getEdesc();
            status = listEaccount.get(0).getState();
        }
        else {
            status = "0";
            System.out.println("NO-QunarBest");
        }
    }

    public String getQunarurl() {
        return qunarurl;
    }

    public void setQunarurl(String qunarurl) {
        this.qunarurl = qunarurl;
    }

    public String getVendorID() {
        return vendorID;
    }

    public void setVendorID(String vendorID) {
        this.vendorID = vendorID;
    }

    public String getRegistname() {
        return registname;
    }

    public void setRegistname(String registname) {
        this.registname = registname;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }

    public String getLinkmobile() {
        return linkmobile;
    }

    public void setLinkmobile(String linkmobile) {
        this.linkmobile = linkmobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
