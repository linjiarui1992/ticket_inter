package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class One2580Book {
    // 以下是12580接口需要参数
    private String one2580user = "";

    private String one2580pwd = "";

    private String one2580key = "";

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    public One2580Book() {
        List<Eaccount> listEaccountone2580 = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(13);
        if (e != null && e.getName() != null) {
            listEaccountone2580.add(e);
        }
        if (listEaccountone2580.size() > 0) {
            one2580user = listEaccountone2580.get(0).getUsername();
            one2580pwd = listEaccountone2580.get(0).getPassword();
            one2580key = listEaccountone2580.get(0).getKeystr();
            staus = listEaccountone2580.get(0).getState();
        }
        else {
            staus = "0";
            System.out.println("NO-one2580");
        }
    }

    public String getOne2580user() {
        return one2580user;
    }

    public void setOne2580user(String one2580user) {
        this.one2580user = one2580user;
    }

    public String getOne2580pwd() {
        return one2580pwd;
    }

    public void setOne2580pwd(String one2580pwd) {
        this.one2580pwd = one2580pwd;
    }

    public String getOne2580key() {
        return one2580key;
    }

    public void setOne2580key(String one2580key) {
        this.one2580key = one2580key;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

}
