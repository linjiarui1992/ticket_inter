package com.ccservice.b2b2c.policy.ben;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

/**
 * 本地政策或者远程政策获取的用户信息，是否开启
 * @author 陈栋
 *
 */
public class LocalBook {
    // 以下是本地接口需要参数
    private String agentcode;// 本地用户名

    private String safecode;// 本地安全码

    private String notifiedUrl;

    private String paymentReturnUrl;

    private String b2cCreatorCn;// webclient createuser;

    private String ispay = "0";// 代扣 1自动 0不自动

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    private String tuifeiurl;// 退费订单通知URL

    private String password;

    private String updatetime;

    private String lastid;

    private String zrateUrl;

    public LocalBook() {
        Eaccount e1 = Server.getInstance().getSystemService().findEaccount(16);
        if (e1 != null) {
            agentcode = e1.getUsername();
            safecode = e1.getPwd();
            notifiedUrl = e1.getNourl();
            paymentReturnUrl = e1.getPayurl();
            ispay = e1.getIspay();
            tuifeiurl = e1.getUrl();
            password = e1.getPassword();
            staus = e1.getState();
            zrateUrl = e1.getZrateUrl();
        }
        else {
            staus = "0";
            System.out.println("NO-本地");
        }
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getLastid() {
        return lastid;
    }

    public void setLastid(String lastid) {
        this.lastid = lastid;
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode;
    }

    public String getSafecode() {
        return safecode;
    }

    public void setSafecode(String safecode) {
        this.safecode = safecode;
    }

    public String getNotifiedUrl() {
        return notifiedUrl;
    }

    public void setNotifiedUrl(String notifiedUrl) {
        this.notifiedUrl = notifiedUrl;
    }

    public String getPaymentReturnUrl() {
        return paymentReturnUrl;
    }

    public void setPaymentReturnUrl(String paymentReturnUrl) {
        this.paymentReturnUrl = paymentReturnUrl;
    }

    public String getB2cCreatorCn() {
        return b2cCreatorCn;
    }

    public void setB2cCreatorCn(String creatorCn) {
        b2cCreatorCn = creatorCn;
    }

    public String getIspay() {
        return ispay;
    }

    public void setIspay(String ispay) {
        this.ispay = ispay;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getTuifeiurl() {
        return tuifeiurl;
    }

    public void setTuifeiurl(String tuifeiurl) {
        this.tuifeiurl = tuifeiurl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getZrateUrl() {
        return zrateUrl;
    }

    public void setZrateUrl(String zrateUrl) {
        this.zrateUrl = zrateUrl;
    }

}
