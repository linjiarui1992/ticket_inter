package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;

/**
 * 
 * @author 栋2013-9-12 10:29:14
 * 
 */
public class Yi9eZratebyBack extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public Yi9eZratebyBack() {
    }

    public Yi9eZratebyBack(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> yi9ezrates = new ArrayList<Zrate>();
        try {
            String url = getSysconfigString("19Ezrateurl");
            //			yi9ezrates = Yi9eMethod.getZrateByFlightNumberbyDB(scity,ecity, sdate, flightnumber, cabin,url);
            long l1 = System.currentTimeMillis();
            yi9ezrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
        }
        catch (Exception e) {
        }
        return yi9ezrates;
    }

}
