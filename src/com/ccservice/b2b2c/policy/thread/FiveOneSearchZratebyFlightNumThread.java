package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.FiveoneBookutil;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.inter.job.WriteLog;

/**
 * 
 * @author 陈栋
 * 
 */
public class FiveOneSearchZratebyFlightNumThread extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public FiveOneSearchZratebyFlightNumThread() {
    }

    public FiveOneSearchZratebyFlightNumThread(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> fiveOneV3Zrates = new ArrayList<Zrate>();
        String url = getSysconfigString("51bookzrateurl");
        Long t = System.currentTimeMillis();
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), t + "");
        try {
            //            fiveOneV3Zrates = FiveoneBookutil.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin, url);
            fiveOneV3Zrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        t = System.currentTimeMillis() - t;
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), t + "");
        url = null;
        t = null;
        return fiveOneV3Zrates;
    }
}
