package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Bartour.BartourMethod;

public class BaiTuoFlightNumThread implements Callable<List<Zrate>> {
    private Orderinfo order;

    private List<Passenger> listPassenger;

    private Segmentinfo sinfo;

    public BaiTuoFlightNumThread(Orderinfo order, List<Passenger> listPassenger, Segmentinfo sinfo) {
        this.order = order;
        this.listPassenger = listPassenger;
        this.sinfo = sinfo;
    }

    @Override
    public List<Zrate> call() throws Exception {
        String agentCode = "B2B_073104";
        String agentUserName = "hthy";
        String agentPwd = "hthy";
        BartourMethod bartourMethod = new BartourMethod(agentCode, agentUserName, agentPwd);
        List<Segmentinfo> listSinfo = new ArrayList<Segmentinfo>();
        listSinfo.add(sinfo);
        return bartourMethod.MatchCommonPolicy(listSinfo, listPassenger);
    }

}
