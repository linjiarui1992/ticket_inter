package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Airutil;

public class EightZrateThread implements Callable<List<Zrate>> {

    private Orderinfo order;

    private int special;

    public EightZrateThread() {
    }

    /**
     * 8000yi根据PNR获取政策
     * @param order
     * @param special
     */
    public EightZrateThread(Orderinfo order, int special) {
        this.order = order;
        this.special = special;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> eightZrates = new ArrayList<Zrate>();
        try {
            eightZrates = Airutil.getBestZratebyPnr_Note(order, special);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return eightZrates;
    }
}
