package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.inter.job.WriteLog;

/**
 * 
 * @author 栋2013-6-28 19:29:14
 *
 */
public class PiaomengZratebyBack extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public PiaomengZratebyBack() {
    }

    public PiaomengZratebyBack(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> piaomengzrates = new ArrayList<Zrate>();
        String url = getSysconfigString("piaomengzrateurl");
        Long t = System.currentTimeMillis();
        int tempRandom = new Random().nextInt(1000);
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), tempRandom + ":" + t + "");
        try {
            piaomengzrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
        }
        catch (Exception e) {
        }
        t = System.currentTimeMillis() - t;
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), tempRandom + ":" + t + ":"
                + url);
        url = null;
        t = null;
        return piaomengzrates;
    }

}
