package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.FiveoneBookutil;

public class FiveoneZrateThread implements Callable<List<Zrate>> {

    private Orderinfo order;

    public FiveoneZrateThread() {
    }

    /**
     * 51book根据PNR获取政策
     * @param order
     */
    public FiveoneZrateThread(Orderinfo order) {
        this.order = order;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> fivezrates = new ArrayList<Zrate>();
        try {
            // 51book(v3.0)根据pnr获取政策
            fivezrates = FiveoneBookutil.getPolicyByPNR(order);
        }
        catch (Exception e) {
        }
        try {
            // 51book(v2.0)根据pnr获取政策
            //fivezrates = FiveoneBookutil.GetPolicyDataByPNR(order);
        }
        catch (Exception e) {
        }
        return fivezrates;
    }
}
