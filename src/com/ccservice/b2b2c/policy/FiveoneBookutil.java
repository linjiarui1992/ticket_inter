package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.axis2.AxisFault;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import client.AVServiceImpl_1_0ServiceStub;
import client.AVServiceImpl_1_0ServiceStub.WsFlight;
import client.AVServiceImpl_1_0ServiceStub.WsFlightItem;
import client.AVServiceImpl_1_0ServiceStub.WsSeatItem;
import client.BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub;
import client.CreatePolicyOrderByPNRService_2_0Stub;
import client.CreatePolicyOrderByPNRTxtService_2_0Stub;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfPassengerInfo;
import client.CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfSegmentInfo;
import client.GDSBookingServiceImpl_1_0ServiceStub;
import client.GDSBookingServiceImpl_1_0ServiceStub.WsBookingPassenger;
import client.GDSBookingServiceImpl_1_0ServiceStub.WsBookingSegment;
import client.GetOrderByOrderNoServiceImpl_1_0ServiceStub;
import client.GetPolicyDataByFlightsService_2_0Stub;
import client.GetPolicyDataByPNRService_2_0Stub;
import client.GetPolicyDataByPNRTxtService_2_0Stub;
import client.GetPolicyOrderByOrderNoService_2_0Stub;
import client.GetRealtimeSpeProductServiceImpl_1_0ServiceStub;
import client.PayCenterOrderServiceImpl_1_0ServiceStub;
import client.PayPolicyOrderService_2_0Stub;
import client.SearchFlightService_2_0Stub;
import client.SearchFlightService_2_0Stub.Flight;
import client.SearchFlightService_2_0Stub.FlightSegment;
import client.SearchFlightService_2_0Stub.Seat;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.cityairport.Cityairport;
import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.FiveonBook;
import com.ccservice.b2b2c.policy.ben.FiveonBookVersion3;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;
import com.liantuo.webservice.client.CancelPolicyOrderService_2_0Stub;
import com.liantuo.webservice.client.GetPolicyOrderStatusByOrderNoService_2_0Stub;
import com.liantuo.webservice.version2_0.SecurityCredential;
import com.liantuo.webservice.version2_0.applypolicyorderrefund.ApplyPolicyOrderRefundReply;
import com.liantuo.webservice.version2_0.applypolicyorderrefund.ApplyPolicyOrderRefundRequest;
import com.liantuo.webservice.version2_0.applypolicyorderrefund.ApplyPolicyOrderRefundService20;
import com.liantuo.webservice.version2_0.applypolicyorderrefund.ApplyPolicyOrderRefundService20PortType;
import com.liantuo.webservice.version2_0.applypolicyorderrefund.ArrayOfString;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRReply;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRRequest;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRService20;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.CreatePolicyOrderByPNRService20PortType;
import com.liantuo.webservice.version2_0.createpolicyorderbypnr.WSPolicyOrder;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.CreatePolicyOrderByPNRB2CReply;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.CreatePolicyOrderByPNRB2CRequest;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.CreatePolicyOrderByPNRB2CService20;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.CreatePolicyOrderByPNRB2CService20PortType;
import com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.PaymentInfo;
import com.ltips.model.webservice.version1_0.service.b2b.getpolicyandfarebyflights.GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub;
import com.ltips.model.webservice.version1_0.service.b2b.getpolicybyid.GetPolicyByIdServiceImpl_1_0ServiceStub;
import com.tenpay.util.MD5Util;
import com.webservice.fiveonebookv3.CreateOrderByRtPatServiceImpl_1_0ServiceStub;
import com.webservice.fiveonebookv3.GetPaymentUrlServiceImpl_1_0ServiceStub;
import com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub;
import com.webservice.fiveonebookv3.GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.WsPolicyData;
import com.webservice.fiveonebookv3.PayServiceImpl_1_0ServiceStub;

/**
 * @author Administrator
 * 
 */
public class FiveoneBookutil extends SupplyMethod {
    public final static Logger logger = LogManager.getLogger(FiveoneBookutil.class.getSimpleName());

    static FiveonBook book = new FiveonBook();

    static FiveonBookVersion3 bookv3 = new FiveonBookVersion3();

    //    static FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();// 51book(v3.0)接口账号信息

    public String TuiFeiOrder(String TicketNo, String OrderNo, String whyid, String url) {
        WriteLog.write("51book", "申请退费票访问:TicketNo:" + TicketNo + ",OrderNo:" + OrderNo + ",whyid:" + whyid + ",url:"
                + url);
        if (OrderNo == null) {
            OrderNo = "11111";
        }
        if (TicketNo.indexOf("-") != -1) {
            TicketNo = TicketNo.replace("-", "");
        }
        try {
            ApplyPolicyOrderRefundService20 service20 = new ApplyPolicyOrderRefundService20();
            ApplyPolicyOrderRefundService20PortType service20PortType = service20
                    .getApplyPolicyOrderRefundService20HttpPort();
            ApplyPolicyOrderRefundRequest request = new ApplyPolicyOrderRefundRequest();
            String agentCode = book.getAgentcode().trim(); // 从51book申请的测试账号
            String securityCode = book.getSafecode().trim(); // 安全码（保密）
            String orderSequenceNo = OrderNo.trim();
            String ActionType = whyid.trim();
            String LogMassage = "test";
            String sign = HttpClient.MD5(agentCode + orderSequenceNo + ActionType + LogMassage + securityCode);
            SecurityCredential credential = new SecurityCredential();
            credential.setAgencyCode(agentCode);
            credential.setSign(sign); // 加密
            request.setCredential(credential);
            request.setActionType(ActionType);
            request.setOrderSequenceNo(orderSequenceNo);
            request.setOrderSequenceNoType("1");
            ArrayOfString arrayOfString = new ArrayOfString();
            ArrayList listtickno = new ArrayList();
            if (TicketNo.indexOf("|") != -1) {
                String ticks[] = TicketNo.split("\\|");
                if (ticks.length > 0) {
                    for (int a = 0; a < ticks.length; a++) {
                        if (ticks[a] != null && ticks[a].length() > 0 && !ticks[a].equals("") && !ticks[a].equals(" ")) {
                            listtickno.add(ticks[a]);
                        }
                    }
                }
            }
            else {
                listtickno.add(TicketNo.trim());
            }
            // WriteLog.write("51book申请退费票的票号",
            // "OrderNo:"+OrderNo+",票号:"+listtickno.toString());

            arrayOfString.setString(listtickno);
            request.setTicketNos(arrayOfString);
            request.setLogMassage(LogMassage);
            request.setRefundNotifiedUrl(url);

            ApplyPolicyOrderRefundReply reply = service20PortType.applyPolicyOrderRefund(request);

            System.out.println("-" + reply.getReturnCode().getValue() + "-");
            System.out.println("-" + reply.getReturnMessage().getValue() + "-");
            System.out.println("-" + reply.getRefundOrderNo() + "-");
            System.out.println("-" + reply.getStatus() + "-");
            WriteLog.write("51book", "申请退费票访问返回结果:OrderNo:" + OrderNo + ",TicketNo:" + TicketNo + ",ReturnCode:"
                    + reply.getReturnCode().getValue() + ",ReturnMessage:" + reply.getReturnMessage().getValue());
            String NewOrderNO = "-1";
            String stayus = "0";// 1：新申请 0：申请失败,请重试

            if (reply.getReturnCode().getValue() != null && reply.getReturnCode().getValue().equals("S")) {// 调用成功
                NewOrderNO = reply.getRefundOrderNo();
                stayus = reply.getStatus();
                if (stayus.equals("1")) {// 申请成功
                    WriteLog.write("51book", "申请退费票访问成功:TicketNo:" + TicketNo + ",NewOrderNO:" + NewOrderNO);
                    return NewOrderNO;
                }
                else {
                    WriteLog.write("51book", "申请退费票访问失败:TicketNo:" + TicketNo + ",NewOrderNO:" + NewOrderNO);
                    return "-1@申请失败,请稍后从试!或者联系客服!";
                }
            }
            else {// 失败
                return "-1@" + reply.getReturnMessage().getValue();
            }
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "-1@申请失败,请稍后从试!或者联系客服!";
    }

    public List SeachFlight(String scitycode, String ecitycode, String airlineCode, String depdate, String type) {
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        //
        // FiveonBook book=new FiveonBook();
        // book.setAgentcode("BJS_S111016");
        // book.setSafecode("n5Y)F6r^");
        //
        WriteLog.write("51bookSeachFlight_seach", "scitycode:" + scitycode + ",scitycode:" + scitycode
                + ",airlineCode:" + airlineCode + ",depdate:" + depdate + ",type:" + type);
        try {
            SearchFlightService_2_0Stub stub = new SearchFlightService_2_0Stub();
            SearchFlightService_2_0Stub.SearchFlightRequest request = new SearchFlightService_2_0Stub.SearchFlightRequest();
            SearchFlightService_2_0Stub.SecurityCredential credential = new SearchFlightService_2_0Stub.SecurityCredential();

            credential.setAgencyCode(book.getAgentcode());
            String sign = HttpClient.MD5(book.getAgentcode() + airlineCode + "OW" + book.getSafecode()).toLowerCase();
            credential.setSign(sign);
            request.setCredential(credential);
            request.setAirline(airlineCode);
            request.setRouteType("OW");
            // FlightCity flightCity = new FlightCity();
            // List<FlightCity> ListflightCity=new ArrayList<FlightCity>();
            SearchFlightService_2_0Stub.ArrayOfFlightCity arrayOfFlightCity = new SearchFlightService_2_0Stub.ArrayOfFlightCity();
            SearchFlightService_2_0Stub.FlightCity[] FlightCitys = new SearchFlightService_2_0Stub.FlightCity[1];// 数组
            SearchFlightService_2_0Stub.FlightCity flightCity2 = new SearchFlightService_2_0Stub.FlightCity();
            flightCity2.setFromCity(scitycode);
            flightCity2.setToCity(ecitycode);
            flightCity2.setFlightDate(depdate);
            FlightCitys[0] = flightCity2;
            arrayOfFlightCity.setFlightCity(FlightCitys);
            request.setCitys(arrayOfFlightCity);
            SearchFlightService_2_0Stub.SearchFlight searchFlight = new SearchFlightService_2_0Stub.SearchFlight();
            searchFlight.setIn0(request);
            SearchFlightService_2_0Stub.SearchFlightResponse response = stub.searchFlight(searchFlight);
            SearchFlightService_2_0Stub.SearchFlightReply reply = response.getOut();
            if (reply.getReturnCode().equals("F")) {

                System.out.println("51book查询航班失败,原因==" + reply.getReturnMessage());

                WriteLog.write("51bookSeachFlight_ERRER", reply.getReturnMessage());
                return listFlightInfoAll;
            }
            // List<FlightSegment> ListflightSegment= new
            // ArrayList<FlightSegment>();
            // ListflightSegment=(List<FlightSegment>)
            // reply.getFlightSegments();
            SearchFlightService_2_0Stub.ArrayOfFlightSegment ListflightSegment = reply.getFlightSegments();
            FlightSegment[] flightSegments = ListflightSegment.getFlightSegment();
            int len = flightSegments.length;
            if (len > 0) {
                for (int a = 0; a < len; a++) {
                    String flightDat = flightSegments[a].getFlightDate();// 格式“yyyy-MM-dd”
                    String fromCity = flightSegments[a].getFromCity();// 出发城市三字码
                    String toCity = flightSegments[a].getToCity();// 抵达城市三字码
                    List<Flight> ListFlight = new ArrayList<Flight>();
                    // ListFlight=(List<Flight>) flightSegments[a].getFlights();
                    SearchFlightService_2_0Stub.ArrayOfFlight arrayOfFlight = flightSegments[a].getFlights();
                    if (arrayOfFlight == null) {
                        return listFlightInfoAll;
                    }
                    Flight[] flights = arrayOfFlight.getFlight();
                    int flen = flights.length;
                    System.out.println("航班数==" + flen);
                    WriteLog.write("51bookSeachFlight_seach", "航班数:" + flen);
                    if (flen > 0) {
                        for (int f = 0; f < flen; f++) {
                            CarbinInfo lowCabinInfo = new CarbinInfo();// 第一个显示得特价得类
                            String flightNo = flights[f].getFlightNo();// 航班号
                            String fromPort = flights[f].getFromPort();// 出发机场
                            String toPort = flights[f].getToPort();// 抵达机场
                            String fromTime = flights[f].getFromTime();// 起飞时间
                            // 格式“HHmm”
                            String toTime = flights[f].getToTime();// 抵达时间
                            // 格式“HHmm”
                            // String
                            // codeShare=flights[f].getCodeShare();//是否代码共享航班
                            // True 或False
                            String carrier = flights[f].getCarrier();// 承运人
                            // 如果是代码共享航班，真正的承运航班
                            int distance = flights[f].getDistance();// 航程公里数 整数
                            // 单位：千米
                            String planeType = flights[f].getPlaneType();// 机型
                            // 比如：773
                            int stopnum = flights[f].getStopnum();// 经停 整数
                            // 经停点个数
                            String meal = flights[f].getMeal();// 餐食 是否有餐食 =C
                            // 是热便餐 =D是正餐 =L
                            // 是正餐 =B 是早餐 =S
                            // 是点心
                            int audletAirportTax = flights[f].getAudletAirportTax();// 成人机建
                            int audultFuelTax = flights[f].getAudultFuelTax();// 成人燃油税
                            int childAirportTax = flights[f].getChildAirportTax();// 儿童机建
                            int childFuelTax = flights[f].getChildFuelTax();// 儿童燃油税
                            int basePrice = flights[f].getBasePrice();// Y仓位价格
                            String leaveTerminal = flights[f].getParam1();
                            String arriveTermimal = flights[f].getParam2();
                            List<Seat> ListSeat = new ArrayList<Seat>();

                            SearchFlightService_2_0Stub.ArrayOfSeat arrayOfSeat = flights[f].getSeats();

                            Seat[] seats = arrayOfSeat.getSeat();
                            int slen = seats.length;
                            System.out.println("航班号:" + flights[f].getFlightNo() + ",的仓位数是=" + slen);

                            Float yprice = 0.0f;// Y仓位价格
                            // 定义仓位list
                            List<CarbinInfo> listCabinAll = new ArrayList<CarbinInfo>();

                            if (slen > 0) {
                                for (int s = 0; s < slen; s++) {
                                    String seatClassCod = seats[s].getSeatClassCode();// 舱位码
                                    Double discount = seats[s].getDiscount();// 折扣
                                    // 如
                                    // 0.3表示
                                    // 3折
                                    String seatNum = seats[s].getSeatNum();// 舱位状态
                                    // 0~9的数字表示剩余舱位个数，A表示大于9个以上座位
                                    int parPrice = seats[s].getParPrice();// 舱位票面价
                                    String commisionInf = seats[s].getCommisionInfo();// 如
                                    // “3%-3元”，表示返点为
                                    // 3% 减3元 返点信息
                                    String Ztype = seats[s].getType();// 政策类型
                                    // 是普通政策，还是特价政策
                                    // normal——普通政策
                                    // special——特殊政策，预留，当前不可用
                                    Double adultPrice = seats[s].getAdultPrice();// 成人票面结算价
                                    // 如：parPrice是1000，返3个点，
                                    // 成人票面结算价就是970元
                                    Double childPrice = seats[s].getChildPrice();// 儿童票面结算价
                                    String workingTime = seats[s].getWorkingTime();// 供应商工作时间
                                    // 格式“HH:mm-HH:mm”
                                    // 24小时制
                                    // 如：“8：00-24：00”
                                    Boolean onWorking = seats[s].getOnWorking();// True
                                    // 或False
                                    // True——在工作时间
                                    // False——不在工作时间

                                    String cabinType = seats[s].getType();

                                    // 赋值仓位list
                                    CarbinInfo cabin = new CarbinInfo();
                                    cabin.setCabin(seatClassCod);
                                    if (seatNum != null && seatNum.equals("A")) {

                                        cabin.setSeatNum("9");// 座位数
                                    }
                                    else {

                                        cabin.setSeatNum(seatNum);// 座位数
                                    }
                                    if (discount > 0) {

                                        cabin.setDiscount(Float.parseFloat(formatMoney(Float.parseFloat(discount * 100
                                                + ""))));
                                    }
                                    if (cabinType.equals("special")) {

                                        cabin.setDiscount(0.0f);
                                    }

                                    cabin.setPrice(Float.parseFloat(parPrice + ""));

                                    System.out.println("航班号:" + flights[f].getFlightNo() + ",的" + cabin.getCabin()
                                            + "仓位价格=" + cabin.getPrice() + ",折扣=" + discount);

                                    if (cabin.getCabin().equals("Y")) {
                                        yprice = cabin.getPrice();
                                        System.out.println("Y仓位的价格==" + yprice);
                                    }

                                    listCabinAll.add(cabin);
                                    // 赋值仓位结束
                                }

                            }

                            // 开始赋值
                            FlightInfo flighInfo = new FlightInfo();
                            if (stopnum > 0) {
                                flighInfo.setIsStopInfo(stopnum + "");
                            }
                            String com = flightNo.substring(0, 2);
                            flighInfo.setAirCompany(com);// 航空公司二字码
                            flighInfo.setAirCompanyName(com);// 航空公司名字
                            flighInfo.setAirline(flightNo);// 航班号
                            flighInfo.setAirplaneType(planeType);// 机型
                            flighInfo.setAirportFee(audletAirportTax);// 基建
                            flighInfo.setFuelFee(audultFuelTax);// 燃油费
                            flighInfo.setStartAirport(fromPort);// 起飞城市三字码
                            flighInfo.setEndAirport(toPort);// 到达城市三字码
                            flighInfo.setOffPointAT(leaveTerminal);
                            flighInfo.setBorderPointAT(arriveTermimal);
                            WriteLog.write("51bookSeachFlight_seach航站楼", "51book:出发航站楼:" + leaveTerminal + "到达航站楼:"
                                    + arriveTermimal);
                            // 出发时间
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HHmm");
                            Date startDate = dateFormat.parse(flightDat + " " + fromTime);
                            flighInfo.setDepartTime(new Timestamp(startDate.getTime()));// 起飞时间
                            // 到达时间
                            Date arriveDate = dateFormat.parse(flightDat + " " + toTime);
                            flighInfo.setArriveTime(new Timestamp(arriveDate.getTime()));// 降落时间
                            flighInfo.setYPrice(basePrice);// Y仓位价格

                            if (yprice == 0.0f) {

                                // yprice=getYPriceFormEterm(fromPort+toPort,
                                // com, "Y");
                                // flighInfo=
                            }

                            // 从新设置折扣开始

                            for (int i = 0; i < listCabinAll.size(); i++) {
                                if (listCabinAll.get(i).getPrice() != 0 && yprice > 0) {
                                    Float dis = listCabinAll.get(i).getPrice() / yprice;

                                    dis = Float.parseFloat(formatMoney(dis));
                                    // Float st=dis*100;

                                    BigDecimal big = new BigDecimal(dis);
                                    double f1 = big.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

                                    listCabinAll.get(i).setDiscount(Float.parseFloat(f1 * 100 + ""));
                                    System.out.println("新折扣===" + dis + ",折扣:" + listCabinAll.get(i).getDiscount());

                                    if (dis <= 0.4f) {// 特价

                                        listCabinAll.get(i).setSpecial(true);
                                        System.out.println("特价仓位==" + listCabinAll.get(i).getCabin() + ",价格=="
                                                + listCabinAll.get(i).getPrice() + ",折扣=" + dis);
                                    }

                                    // System.out.println("新折扣=="+formatMoney_short(dis.toString()));
                                }
                            }
                            // 设置折扣结束

                            // 处理最低价开始
                            // 循环list,排序最低
                            int cabinIndex = 0;
                            int newcabinindex = 0;
                            float fcabtemp = 0;
                            try {
                                fcabtemp = listCabinAll.get(0).getPrice();
                            }
                            catch (Exception ex) {
                                fcabtemp = 0;
                            }

                            for (int i = 1; i < listCabinAll.size(); i++) {

                                if (listCabinAll.get(i).getPrice() != 0) {
                                    /*
                                     * if(!listCabinAll.get(i).getCabin().equals(
                                     * "Y" )&&listCabinAll.get(i).getPrice()<yprice &&
                                     * (listCabinAll.get(i).getDiscount()<0.4*
                                     * 100 ||
                                     * listCabinAll.get(i).getDiscount()==
                                     * 1.0*100 )
                                     * ){//listCabinAll.get(i).getDiscount()<0.4 *
                                     * 100||listCabinAll.get(i).getDiscount()==
                                     * 1.0 *100
                                     * 
                                     * System.out.println(
                                     * "不是Y仓位,并且折扣小于0.4或者折扣等于1.0的位置=="+i);
                                     * System
                                     * .out.println("仓位是"+listCabinAll.get(
                                     * i).getCabin
                                     * ()+",价格=="+listCabinAll.get(i).
                                     * getPrice()); }
                                     */

                                    float Discount = 0.0f;
                                    if (yprice > 0) {
                                        Discount = yprice / listCabinAll.get(i).getPrice();
                                    }

                                    if (fcabtemp > listCabinAll.get(i).getPrice()) {
                                        fcabtemp = listCabinAll.get(i).getPrice();
                                        cabinIndex = i;

                                    }
                                }
                            }
                            System.out.println("本航班的最低价格index是：" + cabinIndex);

                            // 设置最低折扣
                            if (listCabinAll.size() > 0 && listCabinAll.size() >= cabinIndex) {
                                CarbinInfo tempCabinInfo = (CarbinInfo) listCabinAll.get(cabinIndex);
                                if (tempCabinInfo.getCabin() != null) {
                                    lowCabinInfo.setCabin(tempCabinInfo.getCabin());
                                }
                                if (tempCabinInfo.getRatevalue() != null) {
                                    lowCabinInfo.setRatevalue(tempCabinInfo.getRatevalue());
                                }
                                if (tempCabinInfo.getCabinRemark() != null) {
                                    lowCabinInfo.setCabinRemark(tempCabinInfo.getCabinRemark());
                                }
                                else {
                                    lowCabinInfo.setCabinRemark("暂无");
                                }
                                if (tempCabinInfo.getCabinRules() != null) {
                                    lowCabinInfo.setCabinRules(tempCabinInfo.getCabinRules());
                                }
                                else {
                                    lowCabinInfo.setCabinRules("暂无");
                                }
                                if (tempCabinInfo.getDiscount() != null) {
                                    lowCabinInfo.setDiscount(tempCabinInfo.getDiscount());
                                }
                                if (tempCabinInfo.getLevel() != null) {
                                    lowCabinInfo.setLevel(tempCabinInfo.getLevel());
                                }
                                else {
                                    lowCabinInfo.setLevel(1);
                                }
                                if (tempCabinInfo.getPrice() != null) {
                                    lowCabinInfo.setPrice(tempCabinInfo.getPrice());
                                }
                                else {
                                    lowCabinInfo.setPrice(0f);
                                }
                                if (tempCabinInfo.getSeatNum() != null) {
                                    lowCabinInfo.setSeatNum(tempCabinInfo.getSeatNum());
                                }
                                else {
                                    lowCabinInfo.setSeatNum("0");
                                }
                                if (tempCabinInfo.getCabintypename() != null) {
                                    lowCabinInfo.setCabintypename(tempCabinInfo.getCabintypename());
                                }
                                else {
                                    lowCabinInfo.setCabintypename("经济舱");
                                }
                                if (tempCabinInfo.isSpecial()) {
                                    lowCabinInfo.setSpecial(true);
                                }
                                else {
                                    lowCabinInfo.setSpecial(false);
                                }

                                flighInfo.setLowCarbin(lowCabinInfo);

                            }
                            else {
                                continue;
                            }
                            flighInfo.setLowCarbin(lowCabinInfo);// 特价第一个显示得

                            // 处理最低价结束
                            flighInfo.setCarbins(listCabinAll);
                            // System.out.println(flighInfo.getCarbins());
                            listFlightInfoAll.add(flighInfo);
                            // System.out.println("listFlightInfoAll=="+listFlightInfoAll.size());
                        }
                    }
                }
            }

        }
        catch (AxisFault e) {
            e.printStackTrace();
            WriteLog.write("51bookSeachFlight_seach_errer", e.toString());
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            WriteLog.write("51bookSeachFlight_seach_errer", e.toString());
        }
        catch (RemoteException e) {
            e.printStackTrace();
            WriteLog.write("51bookSeachFlight_seach_errer", e.toString());
        }
        catch (ParseException e) {
            e.printStackTrace();
            WriteLog.write("51bookSeachFlight_seach_errer", e.toString());
        }
        try {
            // 舱位价格由低到高排序
            for (FlightInfo flightInfo : listFlightInfoAll) {
                List<CarbinInfo> listcabin = flightInfo.getCarbins();
                Comparator comp = new CabinDiscountComparator();
                Collections.sort(listcabin, comp);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("航班舱位排序出现异常：" + ex.getMessage());
        }

        return listFlightInfoAll;

    }

    // CarbinInfo lowCabinInfo = new CarbinInfo();
    // List<CarbinInfo> listCabinAll = new ArrayList<CarbinInfo>();
    // flighInfo.setLowCarbin(lowCabinInfo);

    // flighInfo.setCarbins(listCabinAll);
    // listFlightInfoAll.add(flighInfo);
    // AVH 黑屏查询
    public List AVHFlight(String scitycode, String ecitycode, String airlineCode, String depdate, String depTime) {
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        try {
            AVServiceImpl_1_0ServiceStub stub = new AVServiceImpl_1_0ServiceStub();
            AVServiceImpl_1_0ServiceStub.AvailableFlightRequest request = new AVServiceImpl_1_0ServiceStub.AvailableFlightRequest();
            request.setAgencyCode(book.getAgentcode());// 编码
            request.setOrgAirportCode(scitycode);// 起飞机场
            request.setDstAirportCode(ecitycode);// 到达机场
            if (airlineCode != null && airlineCode.length() > 0) {
                request.setAirlineCode(airlineCode);// 航空公司
            }
            request.setDate(depdate);// 日期
            String sig = HttpClient.MD5(request.getAgencyCode() + request.getDstAirportCode()
                    + request.getOrgAirportCode() + book.getSafecode());
            request.setSign(sig);

            AVServiceImpl_1_0ServiceStub.GetAvailableFlightE getAvailableFlightE = new AVServiceImpl_1_0ServiceStub.GetAvailableFlightE();
            AVServiceImpl_1_0ServiceStub.GetAvailableFlight getAvailableFlight = new AVServiceImpl_1_0ServiceStub.GetAvailableFlight();

            getAvailableFlight.setRequest(request);

            getAvailableFlightE.setGetAvailableFlight(getAvailableFlight);

            AVServiceImpl_1_0ServiceStub.GetAvailableFlightResponseE responseE = stub
                    .getAvailableFlight(getAvailableFlightE);

            AVServiceImpl_1_0ServiceStub.GetAvailableFlightResponse response = responseE
                    .getGetAvailableFlightResponse();
            AVServiceImpl_1_0ServiceStub.AvailableFlightReply reply = response.get_return();

            if (reply.getReturnCode().equals("F")) {

                System.out.println("51book查询航班失败,原因==" + reply.getReturnMessage());

                WriteLog.write("51bookAVFLIGHT_ERRER", reply.getReturnMessage());
                return listFlightInfoAll;
            }

            WsFlightItem[] wsFlightItem = reply.getFlightItems();
            int len = wsFlightItem.length;
            if (len > 0) {

                for (int a = 0; a < len; a++) {

                    WsFlight[] wsFlight = wsFlightItem[a].getFlights();
                    int wsFlightLEN = wsFlight.length;
                    if (wsFlightLEN > 0) {
                        for (int f = 0; f < wsFlightLEN; f++) {
                            WsFlight wsFlight2 = wsFlight[f];
                            Boolean codeShare = wsFlight2.getCodeShare();// 是否共享航班
                            String shareNum = wsFlight2.getShareNum();// 共享航班号
                            String link = wsFlight2.getLink();// 联接协议级别
                            String orgCity = wsFlight2.getOrgCity();// 出发城市
                            String dstCity = wsFlight2.getDstCity();// 到达城市
                            String s_depTime = wsFlight2.getDepTime();// 起飞时间
                            String arriTime = wsFlight2.getArriTime();// 降落时间
                            String planeType = wsFlight2.getPlaneType();// 机型
                            int stopNum = wsFlight2.getStopnum();// 经停次数

                            // 下面是仓位
                            WsSeatItem[] wsSeatItems = wsFlight2.getSeatItems();
                            int cabinlen = wsSeatItems.length;
                            if (cabinlen > 0) {
                                for (int c = 0; c < cabinlen; c++) {
                                    String seatCode = wsSeatItems[c].getSeatCode();
                                    String seatStatus = wsSeatItems[c].getSeatStatus();

                                }
                            }

                        }

                    }

                }

            }

        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return listFlightInfoAll;
    }

    public String GDSBookingServicePNR(List<Segmentinfo> Lisysegmentinfo, List<Passenger> listpass, Orderinfo orderinfo) {

        try {
            book.setAgentcode("TJZS");
            book.setSafecode("rr1mWify");
            GDSBookingServiceImpl_1_0ServiceStub stub = new GDSBookingServiceImpl_1_0ServiceStub();
            GDSBookingServiceImpl_1_0ServiceStub.GdsBookingRequest request = new GDSBookingServiceImpl_1_0ServiceStub.GdsBookingRequest();
            request.setAgencyCode(book.getAgentcode());// 编码
            WsBookingSegment[] wsBookingSegments = new WsBookingSegment[Lisysegmentinfo.size()];// 航程数组
            for (int a = 0; a < Lisysegmentinfo.size(); a++) {
                WsBookingSegment wsBookingSegment = new WsBookingSegment();
                wsBookingSegment.setOrgCode(Lisysegmentinfo.get(a).getStartairport());
                wsBookingSegment.setDstCode(Lisysegmentinfo.get(a).getEndairport());
                wsBookingSegment.setFlightNo(Lisysegmentinfo.get(a).getFlightnumber());
                wsBookingSegment.setSeatClass(Lisysegmentinfo.get(a).getCabincode());
                wsBookingSegment.setDepDate(formatTimestampyyyyMMdd(Lisysegmentinfo.get(a).getDeparttime()));
                wsBookingSegment.setDepTime(formatTimestampHHmm(Lisysegmentinfo.get(a).getDeparttime()).replace(":",
                        "-"));
                wsBookingSegment.setRouteType("0");
                wsBookingSegments[a] = wsBookingSegment;
            }
            request.setSegments(wsBookingSegments);// 航程
            WsBookingPassenger[] wSBookingPassengers = new WsBookingPassenger[listpass.size()];// 乘机人数组
            for (int a = 0; a < listpass.size(); a++) {
                WsBookingPassenger wsBookingPassenger = new WsBookingPassenger();
                wsBookingPassenger.setName(listpass.get(a).getName());
                wsBookingPassenger.setPassengerType(GetPassType(listpass.get(a).getPtype() + ""));
                wsBookingPassenger.setIdentityType(GetNumberType(listpass.get(a).getIdtype() + ""));
                wsBookingPassenger.setIdentityNo(listpass.get(a).getIdnumber());
                wsBookingPassenger.setTelePhone("010-88888888");

                if (GetPassType(listpass.get(a).getPtype() + "").equals("INF")) {// 婴儿

                    // wsBookingPassenger.setBirth(param);//出生日期
                    // wsBookingPassenger.setHenchMan(param);//跟随的成人名字

                }

                wSBookingPassengers[a] = wsBookingPassenger;

            }
            request.setPassengers(wSBookingPassengers);
            String[] Contacts = { orderinfo.getContactmobile() }; // 联系信息
            // 不能有中文

            request.setContacts(Contacts);

            request.setLeaveDate(formatTimestampyyyyMMdd(Lisysegmentinfo.get(0).getDeparttime()));// 预留日期
            request.setLeaveTime(formatTimestampHHmm(Lisysegmentinfo.get(0).getDeparttime()).replace(":", ""));// 预留时间
            request.setDoPat("F");// 是否解析PAT T 创建PNR成功后做PAT,并解析PAT结果; F
            // 创建PNR成功后不做PAT
            request.setDoRT("F");// 是否解析PNR T 创建成功后解析PNR;F 创建成功后不解析PNR;

            String sig = HttpClient.MD5(
                    book.getAgentcode() + request.getDoPat() + request.getDoRT() + request.getLeaveDate()
                            + request.getLeaveTime() + book.getSafecode()).toLowerCase();
            request.setSign(sig);
            GDSBookingServiceImpl_1_0ServiceStub.GdsBooking gdsBooking = new GDSBookingServiceImpl_1_0ServiceStub.GdsBooking();
            gdsBooking.setRequest(request);

            GDSBookingServiceImpl_1_0ServiceStub.GdsBookingE gdsBookinge = new GDSBookingServiceImpl_1_0ServiceStub.GdsBookingE();

            gdsBookinge.setGdsBooking(gdsBooking);
            // GdsBooking booking = new GdsBooking();
            GDSBookingServiceImpl_1_0ServiceStub.GdsBookingResponseE response = stub.gdsBooking(gdsBookinge);

            GDSBookingServiceImpl_1_0ServiceStub.GdsBookingResponse response2 = response.getGdsBookingResponse();
            GDSBookingServiceImpl_1_0ServiceStub.GdsBookingReply reply = response2.get_return();
            if (reply.getReturnCode().equals("F")) {

                System.out.println("51book创建PNR失败==失败原因==" + reply.getReturnMessage());

                WriteLog.write("51bookCreatePNR_ERRER", reply.getReturnMessage());
                return "-1";
            }
            String pnr = reply.getPnrNo();
            WriteLog.write("51bookCreatePNR", pnr);
            return pnr;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "-1";
    }

    public String createOrderbyNET(int policyid, String PNR, String pnrtxt, String patatxt, String bigPnr) {
        String paramers;
        String url = null;
        String strcreateor = "";
        if (book.getB2cCreatorCn() == null) {
            strcreateor = "";
        }
        if (bigPnr == null) {
            bigPnr = "";
        }
        pnrtxt = pnrtxt.replace("\r\n", "\n");
        pnrtxt = pnrtxt.replace("\n", "\r\n");
        try {
            paramers = "?pnrNo="
                    + PNR.trim()
                    + "&policyId="
                    + policyid
                    + "&notifiedUrl="
                    + book.getNotifiedUrl()
                    + "&paymentReturnUrl="
                    + book.getPaymentReturnUrl()
                    + "&b2cCrateorCN="
                    + book.getB2cCreatorCn()
                    + "&agentCode="
                    + book.getAgentcode()
                    + "&safeCode="
                    + book.getSafecode()
                    + "&param1="
                    + pnrtxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "").replaceAll("\\s{1,}", "%20")
                    + "&param2=>PAT:A"
                    + patatxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "")
                            .replaceAll("\\s{1,}", "%20") + "&param3=" + bigPnr;
            // 自由飞翔
            // url = "http://122.0.66.227:8015/index.aspx" + paramers + "&m="
            // + Math.random();
            // 苍南
            if (book.getAgentcode().equals("WNZ_S12075")) {
                url = "http://210.83.81.120:8015/index.aspx" + paramers + "&m=" + Math.random();
            }
            // 易订行
            if (book.getAgentcode().equals("HTHUAYOU")) {
                url = "http://112.124.40.195:8015/index.aspx" + paramers + "&m=" + Math.random();
            }
            // 龙艺行
            if (book.getAgentcode().equals("szxlyx")) {
                url = "http://121.199.43.29:8015/index.aspx" + paramers + "&m=" + Math.random();
            }
            // System.out.println("************访问.Neturl"+url);
            WriteLog.write("CreateOrder51book", "根据PNR创建订单的URL:Neturl:" + url);
            byte[] bytes = HttpClient.httpget(url.trim());
            String str = new String(bytes);
            // System.out.println("51book下单返回信息==="+str);

            if (str.startsWith("S")) {
                WriteLog.write("CreateOrder51book", "根据PNR创建订单成功:PNR:" + PNR + ",外部政策ID:" + policyid + ",str:" + str);
                return str;
            }
            else {
                // System.out.println("51book调用net下单失败,错误信息=="+str);
                // WriteLog.write("51book_createOrderbyNETERRER",
                // "PNR:"+PNR+",policyid:"+policyid+"+str:" + str);
                WriteLog.write("CreateOrder51book", "根据PNR创建订单失败:PNR:" + PNR + ",外部政策ID:" + policyid + ",str:" + str);
                return "-1";
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            UtilMethod.writeEx(this.getClass().getSimpleName(), e);
            WriteLog.write("CreateOrder51book", "根据PNR创建订单异常" + e.toString());
            return "-1";
        }
    }

    /**
     * 根据PNR创建订单并获得支付链接
     * 
     * @param policyid
     * @param PNR
     * @param pnrtxt
     * @param patatxt
     * @param bigPnr
     * @return
     */
    public String createPolicyOrderByPNR(int policyid, String PNR, String pnrtxt, String patatxt, String bigPnr) {
        String fiveoutid = createOrderbyNET(policyid, PNR, pnrtxt, patatxt, bigPnr);
        if (true) {
            return fiveoutid;
        }
        try {
            CreatePolicyOrderByPNRService_2_0Stub stub = new CreatePolicyOrderByPNRService_2_0Stub();
            CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR creorderbypnr = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR();
            CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest request = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest();
            CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential credential = new CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential();
            String par1 = pnrtxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "").trim();
            String par2 = patatxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "").trim();

            String par3 = "";
            // FAILURE_CHECK_SIGN,MD5验证(agencyCode+pnr+policyId+notifiedUrl+paymentReturnUrl+param1+param2+安全码)
            String credentialString = book.getAgentcode() + PNR + policyid + book.getNotifiedUrl()
                    + book.getPaymentReturnUrl() + par1 + par2 + book.getSafecode();
            System.out.println(credentialString);
            String sign = MD5Encrypt.md5(credentialString.trim()).toLowerCase();
            WriteLog.write("51book下单的MD5", credentialString + "--------" + sign);
            System.out.println(sign);
            credential.setAgencyCode(book.getAgentcode());
            credential.setSign(sign.trim().toLowerCase());
            request.setCredential(credential);
            request.setPnrNo(PNR.trim());
            request.setPolicyId(policyid);
            request.setNotifiedUrl(book.getNotifiedUrl());
            request.setPaymentReturnUrl(book.getPaymentReturnUrl());
            request.setB2CCreatorCn(book.getB2cCreatorCn());
            request.setParam1(par1);
            request.setParam2(par2);

            creorderbypnr.setIn0(request);
            CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRResponse response = stub
                    .createPolicyOrderByPNR(creorderbypnr);

            CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRReply reply = response.getOut();

            System.out.println(reply.getReturnCode());
            System.out.println(reply.getReturnMessage());
            String result = "";
            reply.getOrder();
            if (reply.getReturnCode().equals("S")) {
                CreatePolicyOrderByPNRService_2_0Stub.WSPolicyOrder wsPolicyOrder = reply.getOrder();
                result += "S|" + wsPolicyOrder.getSequenceNo() + "|" + wsPolicyOrder.getPaymentInfo().getPaymentUrl()
                        + "|" + wsPolicyOrder.getParam1();
                return result;
            }
            else {
                WriteLog.write("51book_根据PNR创建订单失败", "PNR:" + PNR + ",ReturnMessage:" + reply.getReturnMessage());
                return "-1";
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return "";

    }

    /**
     * 3.0根据订单号，获取票信息
     * @param ordernum
     * @return
     */
    public String Get51bookOrderInfoByOrderNo(String ordernum) {
        String result = "-1";
        String ticktNO = "";
        String passname = "";
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        try {
            GetOrderByOrderNoServiceImpl_1_0ServiceStub stub = new GetOrderByOrderNoServiceImpl_1_0ServiceStub();
            GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoE getOrderByOrderNo = new GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoE();
            GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNo param = new GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNo();
            GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoRequest param1 = new GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoRequest();
            param1.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
            param1.setOrderNo(ordernum);
            String sign = HttpClient.MD5(fiveonbookversion3.getFiveoneUsernameV3() + ordernum
                    + fiveonbookversion3.getFiveonePasswordV3());
            param1.setSign(sign);
            param.setRequest(param1);
            getOrderByOrderNo.setGetOrderByOrderNo(param);
            WriteLog.write("51BOOK", "查询订单状态:0:" + ordernum);
            GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoResponseE res = stub
                    .getOrderByOrderNo(getOrderByOrderNo);
            GetOrderByOrderNoServiceImpl_1_0ServiceStub.GetOrderByOrderNoResponse response = res
                    .getGetOrderByOrderNoResponse();
            String returnCode = response.get_return().getReturnCode();
            String returnMessage = response.get_return().getReturnMessage();
            if ("S".equals(returnCode)) {
                GetOrderByOrderNoServiceImpl_1_0ServiceStub.WsPolicyOrder policyOrder = response.get_return()
                        .getPolicyOrder();
                String orderstatus = policyOrder.getStatus();
                if ("TICKET_SUCCESS".equals(orderstatus)) {
                    GetOrderByOrderNoServiceImpl_1_0ServiceStub.WsPassenger[] passList = policyOrder.getPassengerList();
                    for (GetOrderByOrderNoServiceImpl_1_0ServiceStub.WsPassenger wsPassenger : passList) {
                        ticktNO += wsPassenger.getTicketNo() + "|";
                        passname += wsPassenger.getName() + "|";
                    }
                    result = passname + "^" + ticktNO;
                }
                WriteLog.write("51BOOK", "查询订单状态:1:" + orderstatus + ":" + result);
            }
            else {
                WriteLog.write("51BOOK", "查询订单状态:1:" + returnMessage);
                System.out.println(returnMessage);
            }
        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public String Get51bookOrderInfoByOrderNo_2_0(String ordernum) {

        try {
            GetPolicyOrderByOrderNoService_2_0Stub stub = new GetPolicyOrderByOrderNoService_2_0Stub();
            GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNo data = new GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNo();
            GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNoRequest re = new GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNoRequest();
            GetPolicyOrderByOrderNoService_2_0Stub.SecurityCredential sec = new GetPolicyOrderByOrderNoService_2_0Stub.SecurityCredential();
            re.setSequenceNo(ordernum);// 订单编号
            re.setSequenceNoType("1");// 51book平台订单号：1 B2C平台订单号：2
            sec.setAgencyCode(book.getAgentcode());
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getSequenceNo() + re.getSequenceNoType()
                    + book.getSafecode());
            sec.setSign(sign);
            re.setCredential(sec);
            data.setIn0(re);

            GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNoResponse response = stub
                    .getPolicyOrderByOrderNo(data);
            GetPolicyOrderByOrderNoService_2_0Stub.GetPolicyOrderByOrderNoReply reply = response.getOut();
            System.out.println("==" + reply.getReturnCode() + "==" + reply.getReturnMessage());
            if (reply.getReturnCode().equals("F")) {

                System.out.println("51book根据订单号查询外部订单信息失败==失败原因==" + reply.getReturnMessage());
                return "-1";
            }

            String orderstaus = reply.getOrderStatus();// 状态
            String ticktPrice = reply.getPreBuyerCharge();// 票价
            String ticktNo = "";// 票号
            String passname = "";// 乘机人

            String orderno = reply.getOrderNo();
            System.out.println("--" + orderstaus + "---" + orderno);
            String aa = response.getOut().getOrderStatus();
            System.out.println(aa);
            Orderinfo orderinfo = new Orderinfo();
            Passenger passenger = new Passenger();
            if (reply.getWsPassengerList() != null) {
                GetPolicyOrderByOrderNoService_2_0Stub.ArrayOfWSPassenger listpassenger2 = reply.getWsPassengerList();
                if (listpassenger2 != null && listpassenger2.getWSPassenger() != null
                        && listpassenger2.getWSPassenger().length > 0) {
                    int len = listpassenger2.getWSPassenger().length;

                    for (int a = 0; a < len; a++) {
                        if (listpassenger2.getWSPassenger()[a].getTicketNo() != null) {
                            ticktNo += listpassenger2.getWSPassenger()[a].getTicketNo() + "|";
                            passname += listpassenger2.getWSPassenger()[a].getName() + "|";
                        }
                    }
                    System.out.println("ticktNo=" + ticktNo);
                }
                System.out.println("orderstaus=" + Get51bookOrderStaus(orderstaus));
                System.out.println(passname + "@" + ticktNo + "@" + ticktPrice + "@" + Get51bookOrderStaus(orderstaus));
                return passname + "@" + ticktNo + "@" + ticktPrice + "@" + Get51bookOrderStaus(orderstaus);

            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
            return "-1";
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "-1";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return "-1";
        }
        return "-1";
    }

    public static List<Zrate> GetPolicyDataByPNR(Orderinfo order) {
        List<Zrate> listRate = new ArrayList<Zrate>();
        try {
            GetPolicyDataByPNRService_2_0Stub stub = new GetPolicyDataByPNRService_2_0Stub();
            GetPolicyDataByPNRService_2_0Stub.GetPolicyDataByPNR data = new GetPolicyDataByPNRService_2_0Stub.GetPolicyDataByPNR();
            GetPolicyDataByPNRService_2_0Stub.GetPolicyDataByPNRRequest req = new GetPolicyDataByPNRService_2_0Stub.GetPolicyDataByPNRRequest();
            GetPolicyDataByPNRService_2_0Stub.SecurityCredential sec = new GetPolicyDataByPNRService_2_0Stub.SecurityCredential();
            req.setPnrNo(order.getPnr().trim());
            req.setParam1(order.getUserRtInfo().replaceAll(">", "").replaceAll("<", "").replaceAll(">", "")
                    .replaceAll("\n", "").replaceAll("\r", "").trim());
            req.setParam2(order.getPnrpatinfo().replaceAll(">", "").replaceAll("<", "").replaceAll(">", "")
                    .replaceAll("\n", "").replaceAll("\r", "").trim());

            sec.setAgencyCode(book.getAgentcode());
            String temp = sec.getAgencyCode() + req.getPnrNo() + req.getParam1() + req.getParam2() + book.getSafecode();
            String sign = MD5Encrypt.md5(temp);
            WriteLog.write("51book",
                    "根据PNR匹配最优政策:加密前和加密后" + temp + "----" + sign.toLowerCase() + "----pnr:" + order.getPnr());
            sec.setSign(sign.toLowerCase());
            req.setCredential(sec);
            data.setIn0(req);
            GetPolicyDataByPNRService_2_0Stub.GetPolicyDataByPNRResponse res = stub.getPolicyDataByPNR(data);
            if (res.getOut().getReturnCode().equals("F")) {// 匹配最优政策失败
                WriteLog.write("51book", "根据PNR匹配最优政策失败:PNR:" + order.getPnr() + ",ReturnCode:"
                        + res.getOut().getReturnCode() + ",ReturnMessage:" + res.getOut().getReturnMessage());

            }
            else {
                WriteLog.write("51book", "根据PNR匹配最优政策成功:PNR:" + order.getPnr() + ",ReturnCode:"
                        + res.getOut().getReturnCode() + ",ReturnMessage:" + res.getOut().getReturnMessage());
                // WriteLog.write("51book_根据PNR匹配最优政策成功", "ReturnCode:" +
                // res.getOut().getReturnCode());
                // WriteLog.write("51book_根据PNR匹配最优政策成功", "ReturnMessage:" +
                // res.getOut().getReturnMessage());
            }

            // 开始解析政策
            if (res.getOut().getPolicyList() != null && res.getOut().getPolicyList().getPolicyData() != null) {

                int len = res.getOut().getPolicyList().getPolicyData().length;

                for (int a = 0; a < len; a++) {

                    // client.GetPolicyDataByPNRTxtService_2_0Stub.PolicyData aa
                    // =res.getOut().getPolicyList().getPolicyData()[a];

                    GetPolicyDataByPNRService_2_0Stub.PolicyData aa = res.getOut().getPolicyList().getPolicyData()[a];

                    if (aa != null) {
                        Zrate zrate = new Zrate();
                        zrate.setAgentid(5L);
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setCreateuser("job");
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifyuser("job");
                        zrate.setAgentid(5l);
                        zrate.setIsenable(1);
                        // zrate.setRemark(remark)
                        zrate.setOutid(aa.getId() + "");
                        zrate.setAircompanycode(aa.getAirlineCode());
                        if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                                && aa.getFlightCourse().indexOf("-") != -1) {
                            zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场

                            if (zrate.getDepartureport() != null && zrate.getDepartureport().equals("999")) {
                                if (aa.getDepartureExclude() != null) {
                                    zrate.setDepartureexclude(aa.getDepartureExclude());// 出发不适用
                                }
                            }

                            zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达

                            if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                                if (aa.getArrivalExclude() != null) {
                                    zrate.setArrivalexclude(aa.getArrivalExclude());// 到达不适用
                                }
                            }

                        }
                        if (aa.getCommision() > 0) {
                            zrate.setRatevalue(aa.getCommision());
                        }
                        zrate.setCabincode(aa.getSeatClass());
                        zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                        zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                        zrate.setSchedule(aa.getFlightCycle());// 航班周期 1234567

                        // aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票
                        // False——不许更换

                        // System.out.println("shijian=="+new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        // zrate.setBegindate(new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                            zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                        }
                        if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                            zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
                        }
                        if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                            zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                        }
                        if (aa.getPrintTicketExpiredDate() != null && aa.getPrintTicketExpiredDate().getTime() != null) {
                            zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间

                        }
                        // new Timestamp( new SimpleDateFormat("yyyy-MM-dd").p);

                        // zrate.setIssuedendate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getExpiredDate()+"").getTime()));
                        // //出票结束时间

                        // zrate.setEnddate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getPrintTicketExpiredDate()+"").getTime()));
                        // //政策有效期结束时间

                        if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                            zrate.setTickettype(2);

                        }
                        else {

                            zrate.setTickettype(1);
                        }

                        if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程

                            zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返

                        }
                        else {
                            zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返

                        }

                        if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                && aa.getWorkTime().indexOf("-") != -1) {
                            String worktime = aa.getWorkTime().split("-")[0];
                            zrate.setWorktime(worktime);
                            String afterworktime = aa.getWorkTime().split("-")[1];
                            zrate.setAfterworktime(afterworktime);

                        }
                        if (aa.getChooseOutWorkTime() != null) {
                            zrate.setOnetofivewastetime(aa.getChooseOutWorkTime());
                            zrate.setWeekendwastetime(aa.getChooseOutWorkTime());
                        }
                        if (aa.getBusinessUnitType() != null) {

                            if (aa.getBusinessUnitType().equals("0")) {// =0
                                // 普通政策
                                // =1
                                // 特殊政策

                                zrate.setGeneral(1l);
                                zrate.setZtype("1");
                            }
                            else {

                                zrate.setGeneral(2l);// 1,普通 2高反
                                zrate.setZtype("2");
                            }

                        }
                        else {

                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }

                        zrate.setUsertype("1");
                        if (aa.getAgencyEfficiency() != null) {
                            zrate.setSpeed(aa.getAgencyEfficiency());
                        }
                        if (aa.getComment() != null) {
                            zrate.setRemark(aa.getComment());
                        }
                        if (zrate.getOutid() != null && zrate.getRatevalue() != null && zrate.getRatevalue() > 0
                                && zrate.getIsenable() == 1 && zrate.getAgentid() != null) {
                            if (zrate.getRatevalue() > 0) {
                                listRate.add(zrate);
                            }
                        }
                        else {
                            continue;
                        }
                        try {
                            WriteLog.write("51book", "根据PNR匹配最优政策成功" + zrate.getOutid() + ":" + zrate.getRemark() + ":"
                                    + zrate.getRatevalue() + ":" + zrate.getGeneral());
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return listRate;
    }

    /**
     * 根据PNR中航段信息，乘客，票价创建订单并获得支付链接
     * 
     * @param orderinfo
     * @param segment
     * @param listPassenger
     * @return
     */
    public String Create51BookOrder(Orderinfo orderinfo, Segmentinfo segment, List<Passenger> listPassenger) {

        try {
            CreatePolicyOrderBySpecialPnrService_2_0Stub stub = new CreatePolicyOrderBySpecialPnrService_2_0Stub();

            CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnr data = new CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnr();
            CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrRequest re = new CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrRequest();
            CreatePolicyOrderBySpecialPnrService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderBySpecialPnrService_2_0Stub.SecurityCredential();

            re.setPnrNo(orderinfo.getPnr());// 小PNR
            re.setBPnrNo(orderinfo.getBigpnr());// 大PNR
            re.setTicketPrice(listPassenger.get(0).getPrice().toString());// 单人票面价
            re.setOilPrice(listPassenger.get(0).getFuelprice().toString());// 单人燃油价
            re.setBuildPrice(listPassenger.get(0).getAirportfee().toString());// 单人机场建设费
            re.setPaymentReturnUrl("http://www.alhk999.com");// 支付完成后返回的url
            re.setNotifiedUrl("http://www.alhk999.com");// 出票通知地址..
            re.setB2CCreatorCn("cangnan");// 订单创建人(客户端)
            re.setRouteType("ONEWAY");// 星程类型 单程：ONEWAY 往返：ROUND 联程：JOIN
            // 往返联程：JOIN_AND_ROUND 缺口程：ARNK
            re.setIsGroup(false);// 是否是团队 True false
            // re.setParam1("");
            // re.setParam2("");
            // re.setParam3("");
            CreatePolicyOrderBySpecialPnrService_2_0Stub.PassengerInfo passengerInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.PassengerInfo();
            ArrayOfPassengerInfo arrayOfPassengerInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfPassengerInfo();

            for (int a = 0; a < listPassenger.size(); a++) {
                passengerInfo.setName(listPassenger.get(a).getName());// 姓名
                if (listPassenger.get(a).getPtype() == 1) {// 1,成人 2 儿童 3婴儿
                    passengerInfo.setType(0);// 乘客类型 =0成人;=1儿童；=2无人陪伴儿童

                }
                else if (listPassenger.get(a).getPtype() == 2) {// 1,成人 2 儿童
                    // 3婴儿
                    passengerInfo.setType(1);// 乘客类型 =0成人;=1儿童；=2无人陪伴儿童

                }
                else {

                    passengerInfo.setType(1);// 乘客类型 =0成人;=1儿童；=2无人陪伴儿童

                }
                if (listPassenger.get(a).getIdtype() == 1) {// 1,身份证 3,护照 4港澳通行证
                    // 5台湾通行证 6台胞证 7回乡证
                    passengerInfo.setIdentityType("1");// 证件类型
                    // =1身份证；=2护照；=3军官证；=4士兵证；
                    // =5 台胞证；=6 其他.
                }
                if (listPassenger.get(a).getIdtype() == 3) {// 1,身份证 3,护照 4港澳通行证
                    // 5台湾通行证 6台胞证 7回乡证
                    passengerInfo.setIdentityType("2");// 证件类型
                    // =1身份证；=2护照；=3军官证；=4士兵证；
                    // =5 台胞证；=6 其他.
                }
                if (listPassenger.get(a).getIdtype() == 5) {// 1,身份证 3,护照 4港澳通行证
                    // 5台湾通行证 6台胞证 7回乡证
                    passengerInfo.setIdentityType("5");// 证件类型
                    // =1身份证；=2护照；=3军官证；=4士兵证；
                    // =5 台胞证；=6 其他.
                }
                else {
                    passengerInfo.setIdentityType("6");// 证件类型
                    // =1身份证；=2护照；=3军官证；=4士兵证；
                    // =5 台胞证；=6 其他.
                }
                // passengerInfo.setIdentityType("1");//证件类型
                // =1身份证；=2护照；=3军官证；=4士兵证； =5 台胞证；=6 其他.
                passengerInfo.setIdentityNo(listPassenger.get(a).getIdnumber());// 证件号码
                passengerInfo.setParam1("");
                arrayOfPassengerInfo.addPassengerInfo(passengerInfo);
            }
            re.setPassengers(arrayOfPassengerInfo);// 乘机人

            CreatePolicyOrderBySpecialPnrService_2_0Stub.SegmentInfo segmentInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.SegmentInfo();

            ArrayOfSegmentInfo arrayOfSegmentInfo = new CreatePolicyOrderBySpecialPnrService_2_0Stub.ArrayOfSegmentInfo();

            segmentInfo.setFlightNo(segment.getFlightnumber());// 航班号
            segmentInfo.setDepartureAirport(segment.getStartairport());// 出发地三字码
            segmentInfo.setArrivalAirport(segment.getEndairport());// 抵达地三字码
            segmentInfo.setDepartureDate(formatTimestampYYYYMMDD(segment.getDeparttime()));// 出发日期 格式：2009-10-10
            segmentInfo.setDepartureTime(formatTimestampToHm(segment.getDeparttime()));// 出发时间 格式：0730 代表7点30
            segmentInfo.setArrivalDate(formatTimestampYYYYMMDD(segment.getArrivaltime()));// 抵达日期 格式：2009-10-10
            segmentInfo.setArrivalTime(formatTimestampToHm(segment.getArrivaltime()));// 抵达时间 格式：0730 代表7点30
            segmentInfo.setPlaneModle(segment.getFlightmodel());// 机型
            segmentInfo.setSeatClass(segment.getCabincode());// 仓位
            segmentInfo.setParam1("");
            arrayOfSegmentInfo.addSegmentInfo(segmentInfo);
            re.setSegments(arrayOfSegmentInfo);// 行程
            if (orderinfo.getPolicyid() != null) {
                Zrate zrate = Server.getInstance().getAirService().findZrate(orderinfo.getPolicyid());
                if (zrate.getOutid() != null) {
                    re.setPolicyId(Integer.parseInt(zrate.getOutid()));
                }
            }
            sec.setAgencyCode("BJS_S111016");
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getPnrNo() + re.getBPnrNo() + re.getPolicyId()
                    + "n5Y)F6r^");
            sec.setSign(sign);
            re.setCredential(sec);
            data.setCreatePolicyOrderBySpecialPnrRequest(re);

            CreatePolicyOrderBySpecialPnrService_2_0Stub.CreatePolicyOrderBySpecialPnrResponse res = stub
                    .createPolicyOrderBySpecialPnr(data);
            System.out.println("----" + res.getCreatePolicyOrderBySpecialPnrReply().getReturnMessage());
            if (res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo() != null) {// 成功

                System.out.println("外部订单号:" + res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo());

                return res.getCreatePolicyOrderBySpecialPnrReply().getOrder().getSequenceNo();

            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
            return "-1";
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "-1";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return "-1";
        }

        return "-1";
    }

    /**
     * 根据政策ID返回政策数据
     * 
     * @param zid
     * @return
     */
    public static Zrate FindOneZrateByIdTo51Book(String zid) {
        Zrate zrate = new Zrate();
        try {
            //            GetPolicyDataByIdService_2_0Stub stub = new GetPolicyDataByIdService_2_0Stub();
            GetPolicyByIdServiceImpl_1_0ServiceStub stub = new GetPolicyByIdServiceImpl_1_0ServiceStub();
            //            GetPolicyDataByIdService_2_0Stub.GetPolicyById data = new GetPolicyDataByIdService_2_0Stub.GetPolicyById();
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdE data = new GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdE();
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyById param = new GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyById();
            //            GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest re = new GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest();
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdRequest paramRequest = new GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdRequest();
            //            GetPolicyDataByIdService_2_0Stub.SecurityCredential sec = new GetPolicyDataByIdService_2_0Stub.SecurityCredential();
            //            re.setPolicyId(zid);// 政策ID,外部id
            paramRequest.setPolicyId(zid);
            //            sec.setAgencyCode(bookv3.getFiveoneUsernameV3());
            paramRequest.setAgencyCode(bookv3.getFiveoneUsernameV3());
            String sign = HttpClient.MD5(paramRequest.getAgencyCode() + paramRequest.getPolicyId()
                    + bookv3.getFiveonePasswordV3());
            paramRequest.setSign(sign);
            //            sec.setSign(sign);
            //            re.setCredential(sec);
            param.setRequest(paramRequest);
            //            data.setIn0(re);
            data.setGetPolicyById(param);
            //            GetPolicyDataByIdService_2_0Stub.GetPolicyByIdResponse res = stub.getPolicyById(data);
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdResponseE res = stub.getPolicyById(data);
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdResponse getPolicyByIdResponse = res
                    .getGetPolicyByIdResponse();
            GetPolicyByIdServiceImpl_1_0ServiceStub.GetPolicyByIdReply getPolicyByIdReply = getPolicyByIdResponse
                    .get_return();
            System.out.println("----" + getPolicyByIdReply.getReturnCode() + ":"
                    + getPolicyByIdReply.getReturnMessage());
            if (getPolicyByIdReply.getReturnCode() != null && "S".equals(getPolicyByIdReply.getReturnCode())) {// 成功 S 失败 F
                GetPolicyByIdServiceImpl_1_0ServiceStub.WsPolicyData aa = getPolicyByIdReply.getPolicyData();

                if (aa != null) {
                    System.out.println(JSONObject.toJSONString(aa));
                    //                    String where = " where 1=1 and " + Zrate.COL_outid + " ='" + aa.getPolicyId() + "' and "
                    //                            + Zrate.COL_agentid + " =5";
                    //                    List<Zrate> listz = Server.getInstance().getAirService()
                    //                            .findAllZrate(where, " ORDER BY ID DESC", -1, 0);
                    //                    if (listz.size() > 0) {
                    //                        zrate = listz.get(0);
                    //                    }
                    //                    zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    //                    zrate.setCreateuser("job");
                    //                    zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    //                    zrate.setModifyuser("job");
                    //                    zrate.setAgentid(5l);
                    //                    zrate.setTickettype(1);
                    //                    zrate.setIsenable(1);
                    //
                    //                    zrate.setOutid(aa.getId() + "");
                    //
                    //                    zrate.setAircompanycode(aa.getAirlineCode());
                    //                    if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0) {
                    //                        zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
                    //                        zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达
                    //                    }
                    //                    if (aa.getCommision() > 0) {
                    //                        zrate.setRatevalue(aa.getCommision());
                    //                    }
                    //                    zrate.setCabincode(aa.getSeatClass());
                    //                    zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                    //                    zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                    //                    zrate.setSchedule(aa.getFlightCycle());// 航班周期 1234567

                    // aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票 False——不许更换

                    // System.out.println("shijian=="+new
                    // Timestamp(aa.getStartDate().getTime().getTime()));
                    // zrate.setBegindate(new
                    // Timestamp(aa.getStartDate().getTime().getTime()));
                    //                    if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                    //                        zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                    //                    }
                    //                    if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                    //                        zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
                    //                    }
                    //                    if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                    //                        zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                    //                    }
                    //                    if (aa.getPrintTicketExpiredDate() != null && aa.getPrintTicketExpiredDate().getTime() != null) {
                    //                        zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间
                    //                    }
                    //                    System.out.println("修改时间：" + new Timestamp(aa.getGmtModified().getTime().getTime()));
                    //                    if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                    //                        zrate.setTickettype(2);
                    //                    }
                    //                    else {
                    //                        zrate.setTickettype(1);
                    //                    }
                    //                    if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程
                    //                        zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返
                    //                    }
                    //                    else {
                    //                        zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返
                    //                    }
                    //                    if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                    //                            && aa.getWorkTime().indexOf("-") != -1) {
                    //                        String worktime = aa.getWorkTime().split("-")[0];
                    //                        zrate.setWorktime(worktime);
                    //                        zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);
                    //                    }
                    //                    if (aa.getBusinessUnitType() != null) {
                    //                        if (aa.getBusinessUnitType().equals("0")) {// =0 普通政策
                    //                            // =1 特殊政策
                    //                            zrate.setGeneral(1l);
                    //                            zrate.setZtype("1");
                    //                        }
                    //                        else {
                    //                            zrate.setGeneral(2l);// 1,普通 2高反
                    //                            zrate.setZtype("2");
                    //                        }
                    //                    }
                    //                    else {
                    //                        zrate.setGeneral(1l);
                    //                        zrate.setZtype("1");
                    //                    }
                    //                    if (aa.getModifyFlag() > 0) {
                    //                        if (aa.getModifyFlag() == 1 || aa.getModifyFlag() == 2) {
                    //                            zrate.setIsenable(1);
                    //                        }
                    //                        else {
                    //                            zrate.setIsenable(0);
                    //                        }
                    //                    }
                    //                    zrate.setUsertype("1");
                    //                    zrate.setSpeed(aa.getAgencyEfficiency());
                    //                    zrate.setRemark(aa.getComment());
                    //
                    //                    if (listz.size() > 0) {
                    //                        Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
                    //                        System.out.println("update==" + zrate.getRatevalue());
                    //                    }
                    //                    else {
                    //                        try {
                    //                            Server.getInstance().getAirService().createZrate(zrate);
                    //                            System.out.println("add==" + zrate.getRatevalue());
                    //                        }
                    //                        catch (SQLException e) {
                    //                            e.printStackTrace();
                    //                            System.out.println("add出错");
                    //                        }
                    //                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return zrate;
    }

    /**
     * 根据PNR信息与PATA信息匹配政策
     * 
     * @param strpnr
     * @param PataTxt
     * @return
     */
    public Zrate SerachZrateByPNR(String strpnr, String PataTxt) {
        // System.out.println("pnr=="+strpnr);
        List<Zrate> listRate = new ArrayList<Zrate>();
        Zrate zrate = new Zrate();
        try {
            GetPolicyDataByPNRTxtService_2_0Stub stub = new GetPolicyDataByPNRTxtService_2_0Stub();
            GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxt data = new GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxt();
            GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtRequest re = new GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtRequest();
            GetPolicyDataByPNRTxtService_2_0Stub.SecurityCredential sec = new GetPolicyDataByPNRTxtService_2_0Stub.SecurityCredential();

            re.setPnrTxt(strpnr);// pnr
            re.setPataTxt(PataTxt);// pate
            re.setParam1("");
            re.setParam2("");
            sec.setAgencyCode(book.getAgentcode());
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getParam1() + re.getParam2() + book.getSafecode());

            sec.setSign(sign);

            re.setCredential(sec);
            data.setIn0(re);
            GetPolicyDataByPNRTxtService_2_0Stub.GetPolicyDataByPNRTxtResponse res = stub.getPolicyDataByPNRTxt(data);

            System.out.println("--" + res.getOut().getReturnCode() + "--");
            System.out.println("--" + res.getOut().getReturnMessage() + "--");
            if (res.getOut().getPolicyList() != null && res.getOut().getPolicyList().getPolicyData() != null) {

                int len = res.getOut().getPolicyList().getPolicyData().length;

                for (int a = 0; a < len; a++) {

                    client.GetPolicyDataByPNRTxtService_2_0Stub.PolicyData aa = res.getOut().getPolicyList()
                            .getPolicyData()[a];

                    if (aa != null) {
                        // System.out.println("返点="+aa.getCommision()+",航空公司代码="+aa.getAirlineCode()+",适合航班="+aa.getFlightNoIncluding()+",不适合航班="+aa.getFlightNoExclude()+",航线="+aa.getFlightCourse());

                        String where = " where 1=1 and " + Zrate.COL_outid + " ='" + aa.getId() + "' and "
                                + Zrate.COL_agentid + " =5";
                        List<Zrate> listz = Server.getInstance().getAirService()
                                .findAllZrate(where, " ORDER BY ID DESC", -1, 0);
                        if (listz.size() > 0) {
                            zrate = listz.get(0);

                        }

                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setCreateuser("job");
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifyuser("job");
                        zrate.setAgentid(5l);
                        zrate.setTickettype(1);
                        zrate.setIsenable(1);

                        zrate.setOutid(aa.getId() + "");

                        zrate.setAircompanycode(aa.getAirlineCode());
                        if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0) {
                            zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场
                            zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达
                        }
                        if (aa.getCommision() > 0) {
                            zrate.setRatevalue(aa.getCommision());
                        }
                        zrate.setCabincode(aa.getSeatClass());
                        zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                        zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                        zrate.setSchedule(aa.getFlightCycle());// 航班周期 1234567

                        // aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票
                        // False——不许更换

                        // System.out.println("shijian=="+new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        // zrate.setBegindate(new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                            zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                        }
                        if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                            zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
                        }
                        if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                            zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                        }
                        if (aa.getPrintTicketExpiredDate() != null && aa.getPrintTicketExpiredDate().getTime() != null) {
                            zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间

                        }
                        // new Timestamp( new SimpleDateFormat("yyyy-MM-dd").p);

                        // zrate.setIssuedendate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getExpiredDate()+"").getTime()));
                        // //出票结束时间

                        // zrate.setEnddate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getPrintTicketExpiredDate()+"").getTime()));
                        // //政策有效期结束时间

                        if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                            zrate.setTickettype(2);

                        }
                        else {

                            zrate.setTickettype(1);
                        }

                        if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程

                            zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返

                        }
                        else {
                            zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返

                        }

                        if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                && aa.getWorkTime().indexOf("-") != -1) {
                            String worktime = aa.getWorkTime().split("-")[0];
                            zrate.setWorktime(worktime);
                            zrate.setAfterworktime(aa.getWorkTime().split("-")[1]);

                        }
                        if (aa.getBusinessUnitType() != null) {

                            if (aa.getBusinessUnitType().equals("0")) {// =0
                                // 普通政策
                                // =1
                                // 特殊政策

                                zrate.setGeneral(1l);
                                zrate.setZtype("1");
                            }
                            else {

                                zrate.setGeneral(2l);// 1,普通 2高反
                                zrate.setZtype("2");
                            }

                        }
                        else {

                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }

                        zrate.setUsertype("1");
                        zrate.setSpeed(aa.getAgencyEfficiency());
                        zrate.setRemark(aa.getComment());

                        if (listz.size() > 0) {
                            Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
                            System.out.println("update==" + zrate.getRatevalue());
                        }
                        else {

                            Server.getInstance().getAirService().createZrate(zrate);
                            System.out.println("add==" + zrate.getRatevalue());
                        }
                        System.out.println("listzrate==" + zrate.getRatevalue());
                        listRate.add(zrate);
                        break;
                    }

                    /*
                     * if(listRate.size()>0){
                     * 
                     * try { float fzratetemp=0f; //临时政策值 int intindex=0;
                     * //最高政策序号 for(int z=0;z<listRate.size();z++){
                     * if(listRate.get(z).getRatevalue()>fzratetemp) {
                     * fzratetemp=listRate.get(z).getRatevalue(); intindex=z; } }
                     * zrate=listRate.get(intindex); } catch(Exception ex) {
                     * zrate=listRate.get(0); } }
                     */
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("zrate==" + zrate.getRatevalue());
        return zrate;
    }

    /**
     * 根据PNR RT信息与PATA信息创建订单并获得支付链接
     * 
     * @param pnr
     * @param pnrTxt
     * @param pataTxt
     * @param policyId
     * @param bigPnr
     * @return
     */
    public String CreateOrderByPNRTxtReply(String pnr, String pnrTxt, String pataTxt, int policyId, String bigPnr) {
        try {
            CreatePolicyOrderByPNRTxtService_2_0Stub stub = new CreatePolicyOrderByPNRTxtService_2_0Stub();
            CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxt data = new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxt();
            CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtRequest re = new CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtRequest();
            CreatePolicyOrderByPNRTxtService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderByPNRTxtService_2_0Stub.SecurityCredential();
            re.setPnrTxt(pnrTxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "").trim());
            re.setPataTxt(pataTxt.replaceAll(">", "").replaceAll("\n", "").replaceAll("\r", "").trim());
            re.setPolicyId(policyId);
            re.setB2CCreatorCn(book.getB2cCreatorCn());
            re.setNotifiedUrl(new String(book.getNotifiedUrl().getBytes("GBK"), "UTF-8"));
            re.setPaymentReturnUrl(new String(book.getPaymentReturnUrl().getBytes("GBK"), "UTF-8"));
            re.setParam1(bigPnr);
            sec.setAgencyCode(book.getAgentcode());
            // FAILURE_CHECK_SIGN,MD5验证(agencyCode+pnr+policyId+notifiedUrl+paymentReturnUrl+
            // param1+param2+param3+安全码)
            String sign = MD5Encrypt.md5(book.getAgentcode() + pnrTxt + policyId + book.getNotifiedUrl()
                    + book.getNotifiedUrl() + book.getSafecode());
            sec.setSign(sign);
            re.setCredential(sec);
            data.setIn0(re);
            CreatePolicyOrderByPNRTxtService_2_0Stub.CreatePolicyOrderByPNRTxtResponse res = stub
                    .createPolicyOrderByPNRTxt(data);
            System.out.println("--" + res.getOut().getReturnCode() + "--" + res.getOut().getReturnMessage() + "--");
            if (res.getOut().getOrder() != null) {
                if (res.getOut().getOrder().getSequenceNo() != null) {
                    String ordernumandPNR = "S|" + res.getOut().getOrder().getSequenceNo() + "|"
                            + res.getOut().getOrder().getPaymentInfo().getPaymentUrl() + "|" + res.getOut().getParam1();
                    // S|订单号|支付url|Office号
                    System.out.println("外部订单号_PNR(S|订单号|支付url|Office号)=" + ordernumandPNR);
                    return ordernumandPNR;
                }
                else {
                    return "-1";
                }
            }
            else {
                return "-1";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
            return "-1";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return "-1";
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "-1";
        }
    }

    public String CreateOrderByPNR(String pnr, String pnrTxt, String pataTxt, String policyId, String notifiedUrl,
            String paymentReturnUrl, String b2cCreatorCn) throws RemoteException, NoSuchAlgorithmException {
        System.out.println("政策id==" + policyId + ",pnr==" + pnr);
        CreatePolicyOrderByPNRService_2_0Stub stub = new CreatePolicyOrderByPNRService_2_0Stub();
        CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR data = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNR();
        CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest req = new CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRRequest();
        CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential sec = new CreatePolicyOrderByPNRService_2_0Stub.SecurityCredential();
        String agencycode = book.getAgentcode();
        String securitycode = book.getSafecode();
        req.setB2CCreatorCn("b2b");
        req.setIsPay("0");
        req.setNotifiedUrl("http://www.sina.com.cn");
        req.setPaymentReturnUrl("http://www.sina.com.cn");
        req.setPnrNo(pnr);
        req.setPolicyId(Integer.parseInt(policyId));
        // req.setParam1("");
        // req.setParam2("");
        // req.setParam3("");
        sec.setAgencyCode(agencycode);
        sec.setSign(HttpClient.MD5(agencycode + req.getPnrNo() + req.getPolicyId() + req.getNotifiedUrl()
                + req.getPaymentReturnUrl() + securitycode));
        req.setCredential(sec);
        data.setIn0(req);
        CreatePolicyOrderByPNRService_2_0Stub.CreatePolicyOrderByPNRResponse res = stub.createPolicyOrderByPNR(data);
        System.out.println(res.getOut().getReturnMessage());
        System.out.println(res.getOut().getOrder().getSequenceNo());
        /*
         * CreatePolicyOrderByPNRTxtService20 service20 = new
         * CreatePolicyOrderByPNRTxtService20();
         * com.liantuo.webservice.version2_0
         * .createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxt data = new
         * CreatePolicyOrderByPNRTxt();
         * com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt
         * .CreatePolicyOrderByPNRTxtRequest re = new
         * CreatePolicyOrderByPNRTxtRequest();
         * com.liantuo.webservice.version2_0.SecurityCredential sec = new
         * SecurityCredential();
         * 
         * re.setPnrTxt(pnrTxt); re.setPataTxt(pataTxt);
         * re.setPolicyId(Integer.parseInt(policyId));
         * re.setNotifiedUrl(notifiedUrl);
         * re.setPaymentReturnUrl(paymentReturnUrl); //re.setB2CCreatorCn();
         * 
         * sec.setAgencyCode("BJS_S111016"); String
         * sign=HttpClient.MD5(sec.getAgencyCode
         * ()+re.getPnrTxt()+re.getPolicyId(
         * )+re.getNotifiedUrl()+re.getPaymentReturnUrl()+"n5Y)F6r^");
         * sec.setSign(sign);
         * 
         * 
         * re.setCredential(sec); data.setIn0(re);
         * //com.liantuo.webservice.version2_0
         * .createpolicyorderbypnrtxt.CreatePolicyOrderByPNRTxtResponse res
         * =service20.
         */

        return "-1";
    }

    public String Seach51BookOrderStaus(String ordernum, String stype) {
        try {
            GetPolicyOrderStatusByOrderNoService_2_0Stub stub = new GetPolicyOrderStatusByOrderNoService_2_0Stub();

            GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNo data = new GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNo();
            GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoRequest re = new GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoRequest();

            GetPolicyOrderStatusByOrderNoService_2_0Stub.SecurityCredential sec = new GetPolicyOrderStatusByOrderNoService_2_0Stub.SecurityCredential();
            sec.setAgencyCode(book.getAgentcode());
            re.setOrderNo(ordernum);// 订单号,51book返回的订单号
            re.setStatusType(Integer.parseInt(stype));// =1订单采购状态；=2 订单供应状态
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getOrderNo() + re.getStatusType()
                    + book.getSafecode());
            sec.setSign(sign);

            re.setCredential(sec);
            data.setIn0(re);

            GetPolicyOrderStatusByOrderNoService_2_0Stub.GetPolicyOrderStatusByOrderNoResponse res = stub
                    .getPolicyOrderStatusByOrderNo(data);

            System.out.println("订单状态编号为==" + res.getOut().getStatusCode());
            System.out.println("订单状态描述==" + res.getOut().getStatusDesc());

            return res.getOut().getStatusDesc();
        }
        catch (AxisFault e) {
            e.printStackTrace();
            return "未知";

        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "未知";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return "未知";
        }

    }

    public String Cancel51BookOrderByID(String orderid) {

        try {
            CancelPolicyOrderService_2_0Stub stub = new CancelPolicyOrderService_2_0Stub();

            CancelPolicyOrderService_2_0Stub.CancelPolicyOrder data = new CancelPolicyOrderService_2_0Stub.CancelPolicyOrder();

            CancelPolicyOrderService_2_0Stub.CancelPolicyOrderRequest re = new CancelPolicyOrderService_2_0Stub.CancelPolicyOrderRequest();

            CancelPolicyOrderService_2_0Stub.SecurityCredential sec = new CancelPolicyOrderService_2_0Stub.SecurityCredential();

            re.setSequenceNo(orderid);// 51book订单号

            sec.setAgencyCode(book.getAgentcode());
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getSequenceNo() + book.getSafecode());
            sec.setSign(sign);

            re.setCredential(sec);
            data.setIn0(re);

            CancelPolicyOrderService_2_0Stub.CancelPolicyOrderResponse res = stub.cancelPolicyOrder(data);
            if (res.getOut().getReturnCode().equals("S")) {
                System.out.println("取消订单状态==" + res.getOut().getStatus());
                return res.getOut().getStatus();
            }
            else {

                System.out.println("取消失败");
                return "-1";
            }

        }
        catch (AxisFault e) {
            e.printStackTrace();
            System.out.println("取消出现异常");
            return "-1";
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.out.println("取消出现异常");
            return "-1";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            System.out.println("取消出现异常");
            return "-1";
        }

    }

    public String Get51BookPayUrlByOrderID(String orderid) {

        try {
            PayPolicyOrderService_2_0Stub stub = new PayPolicyOrderService_2_0Stub();

            PayPolicyOrderService_2_0Stub.PayPolicyOrder data = new PayPolicyOrderService_2_0Stub.PayPolicyOrder();
            // GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest re = new
            // GetPolicyDataByIdService_2_0Stub.GetPolicyByIdRequest();
            PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest re = new PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest();

            PayPolicyOrderService_2_0Stub.SecurityCredential sec = new PayPolicyOrderService_2_0Stub.SecurityCredential();

            re.setOrderNo(orderid);// 订单号,51book返回的订单号
            re.setUserName(book.getAgentcode());// 用户名
            re.setUserPassword("1111");// 密码
            sec.setAgencyCode(book.getAgentcode());
            String sign = HttpClient.MD5(sec.getAgencyCode() + re.getOrderNo() + re.getUserName()
                    + re.getUserPassword() + book.getSafecode());
            sec.setSign(sign);
            re.setCredential(sec);
            data.setIn0(re);

            PayPolicyOrderService_2_0Stub.PayPolicyOrderResponse res = stub.payPolicyOrder(data);
            PayPolicyOrderService_2_0Stub.PayPolicyOrderReply reply = res.getOut();

            System.out.println("url==" + res.getOut().getOrderNo());

            if (res.getOut().getPaymentInfo() != null) {
                if (res.getOut().getReturnCode().equals("S")) {

                    System.out.println("订单号==" + res.getOut().getOrderNo());
                    System.out.println("订单状态==" + res.getOut().getOrderStatus());
                    System.out.println("支付URL==" + res.getOut().getPaymentInfo().getPaymentUrl());
                    System.out.println("支付价格==" + res.getOut().getPaymentInfo().getSettlePrice());
                    System.out.println("支付宝交易号==" + res.getOut().getPaymentInfo().getTradeNo());
                    System.out.println("支付宝张号==" + res.getOut().getPaymentInfo().getPayerAccount());

                    return res.getOut().getPaymentInfo().getPaymentUrl();
                }
                else {

                    System.out.println("错误信息==" + res.getOut().getReturnMessage());
                    return "-1";
                }

            }
            else {
                System.out.println("51book支付URL出问题了");
                return "-1";
            }

        }
        catch (Exception e) {
            System.out.println("51book支付URL出异常了");
            e.printStackTrace();
        }

        return "";

    }

    /**
     * 51book自动支付
     * 
     * @param order
     * @return
     */
    public String payOut(Orderinfo order) {
        String result = "-1";
        try {
            PayPolicyOrderService_2_0Stub stub = new PayPolicyOrderService_2_0Stub();
            PayPolicyOrderService_2_0Stub.PayPolicyOrder payPolicyOrder = new PayPolicyOrderService_2_0Stub.PayPolicyOrder();
            PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest request = new PayPolicyOrderService_2_0Stub.PayPolicyOrderRequest();
            PayPolicyOrderService_2_0Stub.SecurityCredential sec = new PayPolicyOrderService_2_0Stub.SecurityCredential();

            // agencyCode+ OrderNo +userName + userPassword+安全码
            String signTemp = book.getAgentcode() + order.getExtorderid() + book.getAgentcode() + book.getPassword()
                    + book.getSafecode();
            String sign = MD5Encrypt.md5(signTemp).toLowerCase();
            sec.setSign(sign);
            sec.setAgencyCode(book.getAgentcode().trim());
            request.setCredential(sec);
            request.setOrderNo(order.getExtorderid());
            request.setUserName(book.getAgentcode().trim());
            request.setUserPassword(book.getPassword().trim());

            payPolicyOrder.setIn0(request);
            WriteLog.write("自动代扣",
                    "51book:0:signTemp:" + signTemp + ",sign:" + sign + ",Safecode:" + book.getSafecode()
                            + ",Extorderid:" + order.getExtorderid() + ",Agentcode:" + book.getAgentcode().trim()
                            + ",Password:" + book.getPassword().trim());
            PayPolicyOrderService_2_0Stub.PayPolicyOrderResponse response = stub.payPolicyOrder(payPolicyOrder);

            String returnCode = response.getOut().getReturnCode();
            String retrunMessage = response.getOut().getReturnMessage();
            String tradeNo = "";// 支付宝交易号
            String payerAccount = "";// 支付宝账号
            if (response.getOut().getPaymentInfo() != null && response.getOut().getPaymentInfo().getTradeNo() != null) {
                tradeNo = response.getOut().getPaymentInfo().getTradeNo();
                double settlePrice = response.getOut().getPaymentInfo().getSettlePrice();
                payerAccount = response.getOut().getPaymentInfo().getPayerAccount();
                result = "S|" + tradeNo;
                WriteLog.write("自动代扣", "51book:1:returnCode:" + returnCode + ",retrunMessage:" + retrunMessage
                        + ",tradeNo:" + tradeNo + ",payerAccount:" + payerAccount + ",金额:" + settlePrice);
                try {
                    String where = "UPDATE T_ORDERINFO SET " + Orderinfo.COL_extorderprice + "=" + settlePrice
                            + " WHERE " + Orderinfo.COL_extorderid + "='" + tradeNo + "'";
                    WriteLog.write("z支付供应", "51AutoPay:" + where);
                    Server.getInstance().getSystemService().findMapResultBySql(where, null);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                WriteLog.write("自动代扣", "51book:2:returnCode" + returnCode + ",retrunMessage:" + retrunMessage
                        + ",tradeNo:" + tradeNo + ",payerAccount:" + payerAccount);
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        WriteLog.write("自动代扣", "51book_autopay_over");
        return result;
    }

    public String formatTimestampYYYYMMDD(Timestamp date) {
        try {
            return (new SimpleDateFormat("yyyy-MM-dd").format(date));

        }
        catch (Exception e) {
            return "";
        }

    }

    public String formatTimestampToHm(Timestamp date) {
        try {

            return (new SimpleDateFormat("HH:mm").format(date)).replace(":", "");

        }
        catch (Exception e) {
            return "";
        }

    }

    public void insertcityairport() {
        String temp = "AKU,阿克苏,Akesu,AKS|AAT,阿勒泰,Aletai,ALT|ALI,阿里,Ali,AL|ANJ,安吉,Anji,AJ|AKA,安康,Ankang,AK|AQG,安庆,Anqing,AQ|AOG,鞍山,Anshan,AS|AVA,安顺,Anshun,AS|ANT,安图,Antu,AT|AXI,安溪,Anxi,AX|ANY,安阳,Anyang,AY|MAC,澳门,Aomen,AM|AEB,百色,Baise,BS|BSD,保山,Baoshan,BS|BDG,保定,Baoding,BD|WAN,保亭,Baoting,BT|BAV,包头,Baotou,BT|BJI,宝鸡,Baoji,BJ|BDH,北戴河,Beidaihe,BDH|BHY,北海,Beihai,BH|PEK,北京首都,Beijingshoudu,BJSD|BEX,本溪,Benxi,BX|BBU,蚌埠,Bengbu,BB|BJE,毕节,Bijie,BJ|BIZ,滨州,Binzhou,BZ|BAO,博鳌,Boao,BA|BEJ,布尔津,Buerjin,BEJ|CHS,常熟,Changshu,CS|CIH,长治,Changzhi,CZ|CGQ,长春,Changchun,CC|CLE,长乐,Changle,CL|CHE,潮州,Chaozhou,CZ|CHY,潮阳,Chaoyang,CY|COH,巢湖,Chaohu,CH|CZX,常州,Changzhou,CZ|CHL,昌黎,Changli,CL|CGD,常德,Changde,CD|CSX,长沙,Changsha,CS|CMA,澄迈县,Chengmaixian,CMX|CDE,承德,Chengde,CD|CHZ,郴州,Chenzhou,CZ|CTU,成都,Chengdu,CD|SWB,澄海,Chenghai,CH|CIF,赤峰,Chifeng,CF|CKG,重庆,Chongqing,ZQ|CZH,滁州,Chuzhou,CZ|CUX,楚雄,Chuxiong,CX|CIX,慈溪,Cixi,CX|DLC,大连,Dalian,DL|DLU,大理,Dali,DDL|DDG,丹东,Dandong,DD|DAZ,儋州,Danzhou,DZ|DAY,丹阳,Danyang,DY|DQA,大庆,Daqing,DQ|DAT,大同,Datong,DT|DAX,达县,Daxian,DX|DHU,德化,Dehua,DH|DFE,登封,Dengfen,DF|DEQ,德清,Deqing,DQ|DEY,德阳,Deyang,DY|DIA,定安县,Dinganxian,DAX|DIG,迪庆,Diqing,DQ|DNG,东莞,Dongguan,DD|DOA,东方,Dongfang,DF|DSH,东山,Dongshan,DS|DOY,东营,Dongyin,DY|DJY,都江堰,Dujiangyan,DJY|DNH,敦煌,Dunhuang,DH|DYU,都匀,Duyun,DY|DSN,鄂尔多斯,Eerduosi,EEDS|EMS,峨眉山,Emeishan,EMS|EPG,恩平,Enping,EP|ENH,恩施,Enshi,ES|FGC,防城港,Fangchenggang,FCG|FEH,奉化,Fenghua,FH|FHX,凤凰县,Fenghuangxian,FHX|FUD,福鼎,Fuding,FD|FUQ,福清,Fuqing,FQ|FOH,佛山,Fushan,FS|BXN,阜新,Fuxin,FX|FYN,富蕴,Fuyun,FW|FUY,富阳,Fuyang,FY|FUG,阜阳,Fuyang,FY|FOC,福州,Fuzhou,FZ|KOW,赣州,Ganzhou,GZ|GOQ,格尔木,Geermu,GEM|CAN,广州,Guangzhou,GZ|GUA,广安,Guangan,GA|GHN,广汉,Guanghan,GH|GYS,广元,Guangyuan,GY|KWL,桂林,Guilin,GL|KWE,贵阳,Guiyang,GY|GUG,贵港,Guigang,GG|HRB,哈尔滨,Haerbin,HEB|HIY,海盐,Haiyan,HY|HLD,海拉尔,Hailaer,HLE|HCG,海城,Haicheng,HC|HHH,海安,Haian,HA|HIN,海宁,Haining,HN|HLG,海螺沟,Hailuogou,HLG|HAK,海口,Haikou,HK|HMI,哈密,Hami,HM|HCN,韩城,Hancheng,HC|HDG,邯郸,Handan,HD|HGH,杭州,Hangzhou,HZ|HZG,汉中,Hanzhong,HZ|HEB,鹤壁,Hebi,HB|HCH,河池,Hechi,HC|HFE,合肥,Hefei,HF|HEG,鹤岗,Hegang,HG|HEK,黑河,Heihe,HH|HED,横店,Hengdian,HD|HES,衡水,Hengshui,HS|HGY,衡阳,Hengyang,HY|HSO,鹤山,Heshan,HS|HTN,和田,Hetian,HT|HEY,河源,Heyuan,HY|HEZ,贺州,Hezhou,HZ|HAB,淮北,Huaibei,HB|HNA,淮南,Huainan,HN|TXN,黄山,Huangshan,HS|HYN,黄岩,Huangyan,HY|HHU,怀化,Huaihua,HH|HIA,淮安,Huaian,HA|HET,呼和浩特,Huhehaote,HHHT|HAN,惠安,Huian,HA|HUI,惠州,Huizhou,HZ|HLD,葫芦岛,Huludao,HLD|HLB,呼伦贝尔,Hulunbeier,HLBE|HUZ,湖州,Huzhou,HZ|JMO,江门,Jiangmen,JM|JYN,江阴,Jiangyin,JY|JID,江都,Jiangdu,JD|JIA,吉安,Jian,JA|JIS,嘉善,Jiashan,JS|JMU,佳木斯,Jiamusi,JMS|JZO,胶州,Jiaozhou,JZ|JIX,嘉兴,Jiaxing,JX|JDE,建德,Jiande,JD|JIQ,江油,Jiangyou,JY|DEZ,德州,Jiaozhou,DZ|JGN,嘉峪关,Jiayuguan,JYG|JIZ,焦作,Jiaozuo,JZ|JIY,揭阳,Jieyang,JY|JIL,吉林,Jilin,JL|JIO,即墨,Jimo,JM|JOZ,晋中,Jinzhong,JZ|JNZ,锦州,Jinzhou,JZ|JTA,金坛,Jintan,JT|JDZ,景德镇,Jindezhen,JDZ|JZH,荆州,Jinzhou,JZ|JUY,缙云,Jinyun,JY|JIM,荆门,Jingmen,JM|JIH,金华,Jinhua,JH|JGS,井冈山,Jingangshan,JGS|JJN,晋江,Jinjiang,JJ|TNA,济南,Jinan,JN|JNG,济宁,Jining,JN|JIC,晋城,Jincheng,JC|JSH,吉首,Jishou,JS|JHS,九华山,Jiuhuashan,JHS|JIU,九江,Jiujiang,JJ|JZH,九寨沟,Jiuzhaigou,JZG|CHW,酒泉,Jiuquan,JQ|JUR,句容,Jurong,JR|KYN,开元,Kaiyuan,KY|KPG,开平,Kaiping,KP|KAL,凯里,Kaili,KL|KFG,开封,Kaifen,KF|KJI,喀纳斯,Kanasi,KNS|KHG,喀什,Kashi,KS|KRY,克拉玛依,Kelamayi,KLMY|KCA,库车,Kuche,KC|KRL,库尔勒,Kuerle,KEL|KUS,昆山,Kunshan,KS|KMG,昆明,Kunming,KM|LAF,廊坊,Langfang,LF|LHW,兰州,Lanzhou,LZ|LAX,兰溪,Lanxi,LX|LXA,拉萨,Lasha,LS|LEQ,乐清,Leqing,LQ|LSN,乐山,Leshan,LS|LIZ,辽阳,Liaoyang,LY|LIY,辽源,Liaoyuan,LY|LYG,连云港,Lianyungang,LYG|LCX,连城,Liancheng,LC|LJG,丽江,Lijiang,LJ|LIF,临汾,Linfen,LF|LNS,陵水,Lingshui,LS|LZY,林芝,Linzhi,LZ|LNJ,临沧,Lincang,LC|LIH,临海,Linhai,LH|LIA,临安,Linan,LA|LYI,临沂,Linyi,LY|LIS,丽水,Lishui,LS|LPS,六盘水,Liupanshui,LPS|LZH,柳州,Liuzhou,LZ|LYO,溧阳,Liyang,LY|LNY,龙岩,Longyan,LY|LOY,龙游,Longyou,LY|LHA,龙海,Longhai,LH|LOD,娄底,Loudi,LD|LUH,漯河,Luohe,LH|LYA,洛阳,Luoyang,LY|LUS,庐山,Lushan,LS|LZO,泸州,Luzhou,LZ|LUM,芒市,Mangshi,MS|NZH,满洲里,Manzhouli,MZL|MMI,茂名,Maoming,MM|MEZ,梅州,Meizhou,MZ|MIG,绵阳,Mianyang,MY|MDG,牡丹江,Mudanjiang,MDJ|NNY,南阳,Nanyang,NY|NDH,南戴河,Nandaihe,NDH|NKG,南京,Nanjin,NJ|NAO,南充,Nanchong,NC|NTG,南通,Nantong,NT|NAP,南平,Nanping,NP|NNG,南宁,Nanning,NN|NAN,南安,Nanan,NA|KHN,南昌,Nanchang,NC|NEJ,内江,Neijiang,NJ|NGB,宁波,Ningbo,NB|NID,宁德,Ningde,ND|NHI,宁海,Ninghai,NH|PJN,盘锦,Panjin,PJ|PZI,攀枝花,Panzhihua,PZH|PLI,蓬莱,Penglai,PL|PSX,平山县,Pingshanxian,PSX|PIX,萍乡,Pingxiang,PX|PYO,平遥,Pingyao,PY|PIH,平湖,Pinghu,PH|PLN,普宁,Puning,PN|PUT,莆田,Putian,PT|PUY,濮阳,Puyang,PY|QDH,千岛湖,Qiandaohu,QDH|QID,启东,Qidong,QD|IQM,且末,Qiemo,QM|IQN,庆阳,Qingyang,QY|QZH,钦州,Qinzhou,QZ|SHP,秦皇岛,Qinghuangdao,QHD|TAO,青岛,Qingdao,QD|QIN,清远,Qingyuan,QY|QIH,琼海,Qionghai,QH|NDG,齐齐哈尔,Qiqihaer,QQHE|QGA,泉港,Quangang,QG|QHU,泉州,Quanzhou,QZ|QF,曲阜,Qufu,QF|QUJ,曲靖,Qujing,QJ|JUZ,衢州,Quzhou,QZ|REQ,任丘,Renqiu,RQ|RKZ,日喀则,Rikaze,RKZ|RZO,日照,Rizhao,RZ|RUA,瑞安,Ruian,RA|SMX,三门峡,Sanmenxia,SMX|SAH,三河,Sanhe,SH|SYX,三亚,Sanya,SY|SMI,三明,Sanming,SM|SYA,邵阳,Shaoyang,SY|SHS,韶山,Shaoshan,SS|SHN,上饶,Shangrao,SR|SWA,汕头,Shantou,ST|ZAT,昭通,Shaotong,ZT|SHS,沙市,Shashi,SS|SHA,上海虹桥,Shanghaihongqiao,SHHQ|PVG,上海浦东,Shanghaipudong,SHPD|SHY,上虞,Shangyu,SY|SHG,韶关,Shaoguan,SG|SHW,汕尾,Shanwei,SW|SHZ,嵊州,Shengzhou,SZ|SMU,神木,Shenmu,SM|SHE,沈阳,Shenyang,SY|SZX,深圳,Shenzhen,SZ|SYN,十堰,Shiyan,SY|SJW,石家庄,Shijiazhuang,SJZ|SIS,石狮,Shishi,SS|SNI,寿宁,Shouning,SN|SOG,寿光,Shouguang,SG|SHD,顺德,Shunde,SD|SYM,思茅,Simao,SM|SIP,四平,Siping,SP|SIY,泗阳,Siyang,SY|SOP,松潘,Songpan,SP|SUH,绥化,Suihua,SH|SFH,绥芬河,Suifenhe,SFH|SUN,遂宁,Suining,SN|SUQ,宿迁,Suqian,SQ|SOZ,苏州,Suzhou,SZ|TCG,塔城,Tacheng,TC|THQ,天水,TianShui,TS|TIX,泰兴,Taixing,TX|TYN,太原,Taiyuan,TY|TAN,泰安,Taian,TA|TCG,太仓,Taicang,TC|TIS,泰顺,Taishun,TS|HYN,台州,Taizhou,TZ|TSH,台山,Taishan,TS|TVS,唐山,Tangshan,TS|TCZ,腾冲,Tengchong,TC|TZS,天柱山,Tianzhushan,TZS|TIT,天台,Tiantai,TT|TSN,天津,Tianjin,TJ|TLG,铁岭,Tieling,TL|TEN,铜仁,Tongren,TR|TGO,通辽,Tongliao,TL|TOX,桐乡,Tongxiang,TX|TOZ,通州,Tongzhou,TZ|TOL,同里,Tongli,TL|TOS,通什,Tongshi,TS|TOU,桐庐,Tonglu,TL|TNH,通化,Tonghua,TH|TLF,吐鲁番,Tunufan,TTLF|WQX,汪清县,Wangqingxian,WQX|WXN,万州,Wanzhou,WZ|XGL,万宁,Wanning,WN|WEF,潍坊,Weifang,WF|WEH,威海,Weihai,WH|WNZ,温州,Wenzhou,WZ|WEC,文昌,Wenchang,WC|WEL,温岭,Wenling,WL|WHU,芜湖,Wuhu,WH|WUH,武汉,Wuhan,WH|WUA,乌海,Wuhai,WH|WUJ,吴江,Wujiang,WJ|HLH,乌兰浩特,Wulanhaote,WLHT|URC,乌鲁木齐,Wulumuqi,WLMQ|FSN,抚顺,Wushun,FS|WU,五台山,Wutaishan,WTS|WUX,无锡,Wuxi,WX|WUS,武夷山,Wuyishan,WYS|WUY,武义,Wuyi,WY|WUZ,梧州,Wuzhou,WZ|FUZ,抚州,Wuzhou,FZ|XXI,湘西,Xiangxi,XX|XIT,湘潭,Xiangtan,XT|XIY,西安,Xian,XA|XYG,咸阳,Xianyang,XY|XGLL,香格里拉,Xianggelila,XGLL|XMN,厦门,Xiamen,XM|XIS,象山,Xiangshan,XS|HKG,香港,Xianggang,XG|XIJ,仙居,Xianju,XJ|XFN,襄阳,Xiangfan,XF|XIC,西昌,Xichang,XC|XIL,锡林浩特,Xilinhaote,XLHT|ACX,兴义,Xingyi,XY|XIQ,新沂,Xinyi,XY|XCH,新昌,Xinchang,XC|XIY,新余,Xinyu,XY|XNN,西宁,Xining,XN|XIZ,忻州,Xinzhou,XZ|XIX,新乡,Xinxiang,XX|XTA,邢台,Xingtai,XT|JHG,西双版纳,Xishuangbanna,XSBN|XUC,许昌,Xuchang,XC|XUZ,徐州,Xuzhou,XZ|YAA,雅安,Yaan,YA|YBL,亚布力,Yabuli,YBL|YJI,阳江,Yangjiang,YJ|YNJ,延吉,Yanji,YJ|YAS,阳朔,Yangshuo,YS|YDS,雁荡山,Yandangshan,YDS|YQU,阳泉,Yangquan,YQ|YNT,烟台,Yantai,YT|YAZ,扬中,Yangzhong,YZ|ENY,延安,Yanan,YA|YNZ,盐城,Yancheng,YC|YPI,延平,Yanping,YP|YTY,扬州泰州,YangZhouTaiZhou,YZTZ|YBP,宜宾,Yibin,YB|YIH,宜昌,Yichang,YC|YIC,宜春,Yichun,YC|YIN,伊宁,Yining,YN|YID,英德,Yingde,YD|YKU,营口,Yinhou,YK|INC,银川,Yinchuan,YC|YIT,鹰潭,Yingtan,YT|YIW,义乌,Yiwu,YW|YIX,宜兴,Yixing,YX|YIY,益阳,Yiyang,YY|YIZ,仪征,Yizheng,YZ|YOJ,永嘉,Yongjia,YJ|YKN,永康,Yongkang,YK|LLF,永州,Yongzhou,YZ|YOA,永安,Yongan,YA|YYA,岳阳,Yueyang,YY|UYN,榆林,Yulin,YL|YUL,玉林,Yulin,YL|YCU,运城,Yuncheng,YC|YFU,云浮,Yunfu,YF|YUX,玉溪,Yuxi,YX|YUY,余姚,Yuyao,YY|ZHA,湛江,Zhanjiang,ZJ|CHG,朝阳,Zhaoyang,ZY|YZY,张掖,Zhangye,ZY|ZHQ,肇庆,Zhaoqing,ZQ|DYG,张家界,Zhangjiajie,ZJJ|ZJG,张家港,Zhangjiagang,ZJG|ZJK,张家口,Zhangjiakou,ZZJK|ZHA,漳州,Zhangzhou,ZZ|ZJI,镇江,Zhenjiang,ZJ|CGO,郑州,Zhengzhou,ZZ|HJJ,芷江,Zhijiang,ZJ|ZHZ,周庄,Zhouzhuang,ZZ|ZHD,中甸,Zhongdian,ZD|ZSO,中山,Zhongshan,ZS|ZZU,株洲,Zhuzhou,ZZ|ZUH,珠海,Zhuhai,ZH|ZUJ,诸暨,Zhuji,ZJ|ZBO,淄博,Zibo,ZB|ZGG,自贡,Zigong,ZG|ZYI,遵义,Zunyi,ZY|TOY,富山,Fushan,FS|NAY,北京南苑,Beijingnanyuan,BJNY|SXG,绍兴,Shaoxing,SX|HSN,舟山普陀山,Zhoushanputuoshan,ZSPTS|KIJ,新泻,Xinxie,XX|WNH,文山,Wenshan,WS|KTM,加德满都,Jiademandu,JDMD|BPX,昌都,Changdu,CD|MXZ,梅县,Meixian,MX|JHG,景洪,Jinghong,JH|OHE,漠河,Mohe,MH|HZH,黎平,Liping,LP|NBS,长白山,Changbaishan,CBS|NLT,那拉提,Nalati,NLT|LDS,伊春,Yichun,YC|FUO,佛山,Foshan,FS|JXA,鸡西,Jixi,JX|KGT,康定,Kangding,KD|YUS,玉树,Yushu,YS|ERL,二连浩特,Erlianhaote,ELRT|ZHY,中卫,Zhongwei,ZW|JIQ,黔江,Qianjiang,QJ|GYU,固原,Guyuan,GY|RLK,临河,Linhe,LH";
        String[] citys = temp.split("[|]");
        for (int i = 0; i < citys.length; i++) {
            String t = citys[i];
            String[] city = t.split(",");
            Cityairport port = new Cityairport();
            port.setCityname(city[1]);
            port.setPinyin(city[2].toUpperCase());
            port.setShortpinyin(city[3]);
            port.setAirportcode(city[0]);
            port.setAirportname(city[1] + "机场");
            port.setCityindex(100);
            port.setCreateuser("admin");
            port.setCreatetime(new Timestamp(new Date().getTime()));
            port.setIsenable(1);
            port.setLanguage(0);
            port.setCountrycode("CN");
            try {
                port = Server.getInstance().getAirService().createCityairport(port);
                port.setUcode(port.getId());
                Server.getInstance().getAirService().updateCityairport(port);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String Create51OrderByPnrToJdkWebService(String pnr, String zrateid) {

        try {

            System.out.println("pnr==" + pnr + ",zrateid==" + zrateid);
            CreatePolicyOrderByPNRService20 service = new CreatePolicyOrderByPNRService20();
            CreatePolicyOrderByPNRService20PortType port = service.getCreatePolicyOrderByPNRService20HttpPort();
            // CreatePolicyOrderByPNR data = new CreatePolicyOrderByPNR();
            CreatePolicyOrderByPNRRequest req = new CreatePolicyOrderByPNRRequest();
            // CreatePolicyOrderByPNRResponse res = new
            // CreatePolicyOrderByPNRResponse();
            SecurityCredential sec = new SecurityCredential();
            req.setPnrNo(pnr);
            req.setPolicyId(Integer.parseInt(zrateid));
            req.setNotifiedUrl("http://www.sina.com.cn");
            req.setPaymentReturnUrl("http://www.sina.com.cn");
            JAXBElement<String> username = new JAXBElement<String>(new QName(
                    "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "b2creatorcn"), String.class,
                    "Tom");
            req.setB2CCreatorCn(username);
            JAXBElement<String> ispay = new JAXBElement<String>(new QName(
                    "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "ispay"), String.class, "0");
            req.setIsPay(ispay);
            sec.setAgencyCode(book.getAgentcode());
            sec.setSign(HttpClient.MD5(sec.getAgencyCode() + req.getPnrNo() + req.getPolicyId() + req.getNotifiedUrl()
                    + req.getPaymentReturnUrl() + book.getSafecode()));
            req.setCredential(sec);
            CreatePolicyOrderByPNRReply reply = port.createPolicyOrderByPNR(req);
            JAXBElement<WSPolicyOrder> order = reply.getOrder();
            System.out.println("aaaa==" + order.getValue().getSequenceNo());

        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }

    // B2cWebPrice B2C网站利润
    // WebPayPrice C客户端支付价格
    public String CreatePolicyOrderByPNRB2C(int PolicyID, String Pnr, String B2cWebPrice, String WebPayPrice) {
        WriteLog.write("51book创建B2C订单访问", "Pnr:" + Pnr + ",PolicyID:" + PolicyID + ",B2cWebPrice:" + B2cWebPrice
                + ",WebPayPrice:" + WebPayPrice);

        CreatePolicyOrderByPNRB2CService20 service20 = new CreatePolicyOrderByPNRB2CService20();
        CreatePolicyOrderByPNRB2CService20PortType service20PortType = service20
                .getCreatePolicyOrderByPNRB2CService20HttpPort();
        // CreatePolicyOrderByPNRB2CRequest request=buildRequestB2C();
        CreatePolicyOrderByPNRB2CRequest request = new CreatePolicyOrderByPNRB2CRequest();
        String agentCode = book.getAgentcode().trim(); // 从51book申请的账号
        String securityCode = book.getSafecode().trim(); // 安全码（保密）
        String NotifiedUrl = book.getNotifiedUrl();
        String PaymentReturnUrl = book.getPaymentReturnUrl();

        SecurityCredential credential = new SecurityCredential();
        credential.setAgencyCode(agentCode);
        request.setPnrNo(Pnr.trim());
        request.setPolicyId(PolicyID);
        request.setNotifiedUrl(NotifiedUrl);
        request.setPaymentReturnUrl(PaymentReturnUrl);
        String sign = "";
        try {
            sign = HttpClient.MD5(credential.getAgencyCode() + request.getPnrNo() + request.getPolicyId()
                    + request.getNotifiedUrl() + request.getPaymentReturnUrl() + securityCode);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        System.out.println("sign==" + sign);
        credential.setSign(sign); // 加密
        request.setCredential(credential);
        JAXBElement<String> username = new JAXBElement<String>(new QName(
                "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "username"), String.class,
                "chenx");
        request.setB2CCreatorCn(username);// B2C订单创建人
        request.setPayType(new JAXBElement<String>(new QName(
                "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "payType"), String.class, "1"));// 支付类型 1—支付宝支付
        // 2—网银支付
        // 3—信用卡支付
        request.setWebCharge(B2cWebPrice);// B2C网站利润
        request.setWebPayPrice(WebPayPrice);// C端支付价
        CreatePolicyOrderByPNRB2CReply reply = service20PortType.createPolicyOrderByPNRB2C(request);
        if (reply != null && reply.getReturnCode().getValue().equals("S")) {// 成功

            // System.out.println("-"+reply.getReturnCode().getValue()+"-");
            // System.out.println("-"+reply.getReturnMessage().getValue()+"-");
            // System.out.println("-"+reply.getOrder().getDeclaredType()+"-");
            // reply.getOrder().getValue();

            com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.WSPolicyOrder policyOrder = new com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c.WSPolicyOrder();
            policyOrder = reply.getOrder().getValue();
            JAXBElement<PaymentInfo> PayInfo = policyOrder.getPaymentInfo();
            String PayUrl = PayInfo.getValue().getPaymentUrl().getValue();

            JAXBElement<String> OrderNo = policyOrder.getSequenceNo();
            System.out.println("orderNO=" + OrderNo.getValue());
            WriteLog.write("51book创建B2C订单成功", "Pnr:" + Pnr + ",PolicyID:" + PolicyID + ",B2cWebPrice:" + B2cWebPrice
                    + ",WebPayPrice:" + WebPayPrice + ",OrderNo:" + OrderNo.getValue());
            return "S|" + OrderNo.getValue() + "|" + PayUrl;
        }
        else {// 失败
            WriteLog.write("51book创建B2C订单失败", "Pnr:" + Pnr + ",PolicyID:" + PolicyID + ",B2cWebPrice:" + B2cWebPrice
                    + ",WebPayPrice:" + WebPayPrice + ",TEXT:" + reply.getReturnMessage().getValue());
            return "-1";
        }

    }

    public static CreatePolicyOrderByPNRB2CRequest buildRequestB2C() {
        CreatePolicyOrderByPNRB2CRequest request = new CreatePolicyOrderByPNRB2CRequest();

        String agentCode = "SDFH"; // 从51book申请的测试账号
        String securityCode = "~!!aLYE1"; // 安全码（保密）

        SecurityCredential credential = new SecurityCredential();
        credential.setAgencyCode(agentCode);

        request.setPnrNo("JPYXYV");
        request.setPolicyId(13936671);
        request.setNotifiedUrl("http://");
        request.setPaymentReturnUrl("http://");
        String sign = "";
        try {
            sign = HttpClient.MD5(credential.getAgencyCode() + request.getPnrNo() + request.getPolicyId()
                    + request.getNotifiedUrl() + request.getPaymentReturnUrl() + "~!!aLYE1");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        System.out.println("sign==" + sign);

        credential.setSign(sign); // 加密
        request.setCredential(credential);
        JAXBElement<String> username = new JAXBElement<String>(new QName(
                "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "username"), String.class,
                "chenx");
        // JAXBElement<String> paytype = new JAXBElement<String>(new QName("
        // http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com",
        // "payType"), String.class, "1");

        request.setB2CCreatorCn(username);// B2C订单创建人
        request.setPayType(new JAXBElement<String>(new QName(
                "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "payType"), String.class, "1"));// 支付类型 1—支付宝支付
        // 2—网银支付
        // 3—信用卡支付
        request.setWebCharge("10");// B2C网站利润
        request.setWebPayPrice("710");// C端支付价

        System.out.println(request.getPayType());

        return request;

    }

    // ======================51book特惠产品开始

    /**
     * 特惠政策实时获取接口
     * 
     * @param segmentinfo
     */
    public static void GetRealtimeSpeProduct(Segmentinfo segmentinfo) {
        FiveonBook book = new FiveonBook();
        try {
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub stub = new GetRealtimeSpeProductServiceImpl_1_0ServiceStub();
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductE getRealtimeSpeProduct = new GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductE();
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProduct param = new GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProduct();
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductRequest request = new GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductRequest();
            // AgencyCode:51book给出
            request.setAgencyCode(book.getAgentcode());
            String arrAirportCode = segmentinfo.getAircomapnycode();
            String page = "1";
            String rowsPerPage = "100";
            int seatClassOpenStatus = 0;
            int ticketProbability = 0;
            int ticketTimeType = 0;
            // Sign:MD5
            // (agencyCode+airline+departure+arrival+page+rowPerPage+安全码)
            // agencyCode+airline+departure+arrival+page+rowPerPage+安全码
            String depDate = segmentinfo.getDeparttime().toString().substring(0, 10);
            // 注：按括号内的格式构造MD5码
            // agencyCode+airlineCode+arrivalCode+dataRows+depDate+departureCode+flightNo+seatClassOpenStatus+ticketProbability+ticketTimeType+安全码
            // HTHUAYOUSHA1002013-03-18PEK000$EZ7)cTe
            String signTemp = book.getAgentcode() + arrAirportCode + segmentinfo.getEndairport() + rowsPerPage
                    + depDate + segmentinfo.getStartairport() + seatClassOpenStatus + ticketProbability
                    + ticketTimeType + book.getSafecode();
            System.out.println("加密前的sign=" + signTemp);
            String sign = MD5Encrypt.md5(signTemp).toLowerCase();
            System.out.println("加密后的sign=" + sign);
            request.setSign(sign);
            // request.setSign("55830d8ae184f4598a750ff2db07f408");

            request.setAirlineCode(arrAirportCode);// airlineCode 航空公司二字码 否
            request.setDepartureCode(segmentinfo.getStartairport());// departureCode
            // 出发地三字码
            // String 是
            request.setArrivalCode(segmentinfo.getEndairport());// arrivalCode
            // 抵达地三字码 是
            request.setDepDate(depDate);// depDate 起飞日期 String 是
            request.setFlightNo("");// flightNo 航班号 String 否
            request.setTicketProbability(ticketProbability);// ticketProbability
            // 出票机率 int 是
            // 0所有默认为0 1 保证出票
            request.setSeatClassOpenStatus(seatClassOpenStatus);// seatClassOpenStatus
            // 舱位开放状态 int 是
            // 0-所有1-见舱
            // 2-Q状态 3-无舱
            request.setTicketTimeType(ticketTimeType);// ticketTimeType 出票时间类型
            // Int 是 0不限制 1
            // 只需要能立即出票的政策
            request.setDataRows(100);// dataRows 返回多少条数据 int 是 每页返回数据条数，默认20
            // 最多100条
            param.setRequest(request);
            getRealtimeSpeProduct.setGetRealtimeSpeProduct(param);
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductResponseE responseE = stub
                    .getRealtimeSpeProduct(getRealtimeSpeProduct);
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.GetRealtimeSpeProductResponse response = responseE
                    .getGetRealtimeSpeProductResponse();
            String returnCode = response.get_return().getReturnCode();
            System.out.println("ReturnMessage=" + response.get_return().getReturnMessage());
            System.out.println(returnCode);
            WriteLog.write("51book特惠政策实时", returnCode + "ReturnMessage:" + response.get_return().getReturnMessage());
            GetRealtimeSpeProductServiceImpl_1_0ServiceStub.SpePolicy[] spePolicies = response.get_return()
                    .getSpecialPolicyDataList();
            if (spePolicies != null)
                for (int i = 0; i < spePolicies.length; i++) {
                    GetRealtimeSpeProductServiceImpl_1_0ServiceStub.SpePolicy spePolicy = spePolicies[i];
                    GetRealtimeSpeProductServiceImpl_1_0ServiceStub.SpePolicy sp = spePolicies[i];
                    System.out.println(spePolicy.toString());
                    // 把所有数据输出到日志
                    // WriteLog.write
                    System.out.println("51book第" + i + "条：Id：" + sp.getId() + ",airlineCode:" + sp.getAirlineCode()
                            + ",arrivalAirport:" + sp.getArrivalAirport() + ",backTimeLimit:" + sp.getBackTimeLimit()
                            + ",departureAirport:" + sp.getDepartureAirport() + ",modifiedAt:" + sp.getModifiedAt()
                            + ",needCreatePnr" + sp.getNeedCreatePnr() + ",validSeatClass:" + sp.getValidSeatClass()
                            + ",flightCourse" + sp.getFlightCourse() + ",routeType:" + sp.getRouteType()
                            + ",commision:" + sp.getCommision() + ",supportShareFlight:" + sp.getSupportShareFlight()
                            + ",seatClassOpenStatus:" + sp.getSeatClassOpenStatus() + ",seatClass:" + sp.getSeatClass()
                            + ",preferentialType:" + sp.getPreferentialType() + ",preferential:" + sp.getPreferential()
                            + ",inventoryType:" + sp.getInventoryType() + ",inventoryList:" + sp.getInventoryList()
                            + ",flightNoIncluding:" + sp.getFlightNoIncluding() + ",flightNoExclude:"
                            + sp.getFlightNoExclude() + ",flightCycle:" + sp.getFlightCycle() + ",needSwitchPNR:"
                            + sp.getNeedSwitchPNR() + ",needSwitchSeatClass:" + sp.getNeedSwitchSeatClass()
                            + ",validSeatClassOpen:" + sp.getValidSeatClassOpen() + ",startDate:" + sp.getStartDate()
                            + ",expiredDate:" + sp.getExpiredDate() + ",printTicketStartDate:"
                            + sp.getPrintTicketStartDate() + ",printTicketExpiredDate:"
                            + sp.getPrintTicketExpiredDate() + ",policyType:" + sp.getPolicyType() + ",workTime:"
                            + sp.getWorkTime() + ",vtWorkTime:" + sp.getVtWorkTime() + ",ticketProbability:"
                            + sp.getTicketProbability() + ",invoiceEnable:" + sp.getInvoiceEnable()
                            + ",sellingPriceEqual:" + sp.getSellingPriceEqual() + ",ticketTime:" + sp.getTicketTime()
                            + ",refundExplain:" + sp.getRefundExplain() + ",changeExplain:" + sp.getChangeExplain()
                            + ",allowEndorse：" + sp.getAllowEndorse() + ",vtExplain:" + sp.getVtExplain()
                            + ",departureExclude" + sp.getDepartureExclude() + ",arrivalExclude:"
                            + sp.getArrivalExclude() + ",passengerType:" + sp.getPassengerType()
                            + ",advanceTicketDays:" + sp.getAdvanceTicketDays() + ",param1:" + sp.getParam1()
                            + ",param2:" + sp.getParam2() + ",param3:" + sp.getParam3() + ",param4:" + sp.getParam4()
                            + ",param5:" + sp.getParam5() + "----");
                }

        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 特惠支付接口
     * 
     * @param segmentinfo
     */
    public static void SpecialPayment(Orderinfo order) {
        FiveonBook book = new FiveonBook();
        try {
            PayCenterOrderServiceImpl_1_0ServiceStub stub = new PayCenterOrderServiceImpl_1_0ServiceStub();
            PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderE payCenterOrderE = new PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderE();
            PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrder payCenterOrder = new PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrder();
            PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderRequest request = new PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderRequest();
            // AgencyCode:51book给出
            request.setAgencyCode(book.getAgentcode());
            // agencyCode+outerOrderNo+安全码
            String signTemp = book.getAgentcode() + order.getExtorderid() + book.getSafecode();
            System.out.println("加密前的sign=" + signTemp);
            String sign = MD5Encrypt.md5(signTemp).toLowerCase();
            System.out.println("加密后的sign=" + signTemp);
            request.setSign(sign);
            request.setOuterOrderNo(order.getExtorderid());
            payCenterOrder.setRequest(request);
            payCenterOrderE.setPayCenterOrder(payCenterOrder);
            PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderResponseE responseE = stub
                    .payCenterOrder(payCenterOrderE);
            PayCenterOrderServiceImpl_1_0ServiceStub.PayCenterOrderResponse response = responseE
                    .getPayCenterOrderResponse();
            String returnCode = response.get_return().getReturnCode();
            System.out.println("returnCode=" + returnCode);
            System.out.println("ReturnMessage=" + response.get_return().getReturnMessage());
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 特惠创建订单
     * 
     * @param segmentinfo
     */
    public static void CreatSpecialOrder(Orderinfo order, Segmentinfo segment, List<Passenger> listPassenger) {
        FiveonBook book = new FiveonBook();
        try {
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub stub = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub();
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderE ordee = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderE();
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrder orde = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrder();
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderRequest request = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderRequest();
            // 订座所需信息
            List<BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsTicketCenterPnrWithSpecialPolicyInfo> list = new ArrayList();
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsTicketCenterPnrWithSpecialPolicyInfo winfo = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsTicketCenterPnrWithSpecialPolicyInfo();
            // 乘客信息
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookPassenger wpassenger = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookPassenger();
            // 航段信息
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookSegment wsegment = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookSegment();
            request.setAgencyCode(book.getAgentcode());
            String signTemp = book.getAgentcode() + "李小龙" + "597549636@qq.com" + order.getExtorderid()
                    + book.getSafecode();
            System.out.println("加密前的sign=" + signTemp);
            String sign = MD5Encrypt.md5(signTemp).toLowerCase();
            System.out.println("加密后的sign=" + sign);
            request.setSign(sign);
            // 设置乘客信息
            Passenger p = null;
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookPassenger wp[] = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookPassenger[5];
            if (listPassenger != null)
                for (int i = 0; i < listPassenger.size(); i++) {
                    p = listPassenger.get(i);
                    if (p != null) {
                        wpassenger.setName(p.getName());
                        wpassenger.setPassengerType(p.getPtype() + "");
                        wpassenger.setIdentityType(p.getIdtype().toString());
                        wpassenger.setIdentityNo(p.getIdnumber());
                    }
                    wp[0] = wpassenger;
                }
            // 航段信息
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookSegment[] ws = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsBookSegment[5];
            wsegment.setOrgCode(segment.getStartairport());
            wsegment.setDstCode(segment.getEndairport());
            wsegment.setFlightNo(segment.getFlightnumber());
            wsegment.setSeatClass(segment.getCabincode());
            wsegment.setDepDate((segment.getDeparttime() + "").substring(0, 10));
            System.out.println((segment.getDeparttime() + "").substring(0, 10));
            System.out.println((segment.getDeparttime() + "").substring(11, 16));
            // wsegment.setDstCode(segment.getTraveltype()+"");
            wsegment.setRouteType(segment.getTraveltype() + "");
            // 格式：hhmm
            wsegment.setArrTime((segment.getArrivaltime() + "").substring(11, 16).replace(":", ""));
            System.out.println((segment.getArrivaltime() + "").substring(11, 16).replace(":", ""));
            wsegment.setDepTime((segment.getDeparttime() + "").substring(11, 16).replace(":", ""));
            ws[0] = wsegment;
            // 订座所需信息
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsTicketCenterPnrWithSpecialPolicyInfo[] wi = new BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.WsTicketCenterPnrWithSpecialPolicyInfo[5];
            winfo.setSegments(ws);
            winfo.setPassengers(wp);
            // ????int类型的价格...
            winfo.setParPrice(segment.getPrice().intValue());
            winfo.setFuelTax(segment.getFuelfee().intValue());
            winfo.setAirportTax(segment.getAirportfee().intValue());
            winfo.setPolicyId(segment.getZrateid().intValue());
            winfo.setCommisionPoint(segment.getRatevalue() + "");
            wi[0] = winfo;
            request.setPnrInfos(wi);
            // WSTicketCenterPnrWithSpecialPolicyInfo i = null;
            // request.setPnrInfos(param)
            request.setOuterOrderNo(order.getOrdernumber());
            request.setNotifiedUrl(book.getNotifiedUrl());
            request.setCreatorCn("hthy");
            orde.setRequest(request);
            ordee.setBookingAndCreateTicketSpecialCenterOrder(orde);
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderResponseE responseE = stub
                    .bookingAndCreateTicketSpecialCenterOrder(ordee);
            BookingAndCreateTicketSpecialCenterOrderServiceImpl_1_0ServiceStub.BookingAndCreateTicketSpecialCenterOrderResponse response = responseE
                    .getBookingAndCreateTicketSpecialCenterOrderResponse();
            // String orderNo = response.get_return().getTicketCenterOrderNo();
            String orderNo = response.get_return().getReturnMessage();
            System.out.println("出票中心订单号=" + orderNo);
            System.out.println("订单状态=" + response.get_return().getOrderStatus());
            System.out.println("订单创建时间=" + response.get_return().getCreateTime());
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    // ======================51book特惠产品结束

    public static String Get51bookOrderStaus(String staus) {
        if (staus.equals("30")) {
            return "等待申请位置";
        }
        if (staus.equals("31")) {
            return "申请处理中";
        }
        if (staus.equals("34")) {

            return "等待审核票价";
        }
        if (staus.equals("27")) {

            return "审核处理中(确认票价)";
        }
        if (staus.equals("32")) {

            return "无法出票";
        }
        if (staus.equals("28")) {

            return "票价已确认";
        }
        if (staus.equals("15")) {

            return "已支付，等待出票";
        }
        if (staus.equals("5")) {

            return "出票方正在处理";
        }
        if (staus.equals("6")) {

            return "已出票";
        }
        if (staus.equals("12")) {

            return "处理结束的订单";
        }
        if (staus.equals("35")) {

            return "等待客服处理";
        }
        if (staus.equals("36")) {

            return "无法更新为已出票，等待客服处理";
        }
        if (staus.equals("26")) {

            return "作废的订单";
        }
        if (staus.equals("13")) {

            return "采购方确认作废";
        }
        if (staus.equals("19")) {

            return "申请客票变更";
        }
        if (staus.equals("20")) {

            return "已变更,待确认";
        }
        if (staus.equals("21")) {

            return "无法变更,拒绝";
        }
        if (staus.equals("22")) {

            return "采购方确认变更结束";
        }
        if (staus.equals("1")) {

            return "新建订单,未订座";
        }
        if (staus.equals("2")) {

            return "新建订单,已订座";
        }

        if (staus.equals("9")) {

            return "订单取消";
        }
        if (staus.equals("3")) {

            return "已订PNR但订座情况不明";
        }
        if (staus.equals("4")) {

            return "新订单订座失败";
        }
        if (staus.equals("16")) {

            return "订座员确认订单生效,等待支付等待对方出票";
        }
        if (staus.equals("23")) {

            return "等待代支付";
        }
        if (staus.equals("24")) {

            return "代支付被拒绝";
        }
        if (staus.equals("25")) {

            return "被买家确认";
        }
        if (staus.equals("29")) {

            return "等待支付特价手续费";
        }
        if (staus.equals("33")) {

            return "等待财务支付特价手续费";
        }
        if (staus.equals("38")) {

            return "采购取消并退款";
        }
        if (staus.equals("39")) {

            return "采购取消并退款,确认";
        }
        return "";
    }

    public String formatTimestampHHmm(Timestamp date) {
        try {
            return (new SimpleDateFormat("HH:mm").format(date));

        }
        catch (Exception e) {
            return "";
        }

    }

    public String formatTimestampyyyyMMdd(Timestamp date) {
        try {
            return (new SimpleDateFormat("yyyy-MM-dd").format(date));

        }
        catch (Exception e) {
            return "";
        }

    }

    /**
     * 将money格式化为类似于2,243,234.00的格式
     * 
     * @param money
     * @return
     */
    DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();

    public String formatMoney(Float money) {
        format.applyPattern("#,##0.00");
        try {
            String result = format.format(money);
            return result;
        }
        catch (Exception e) {
            if (money != null) {
                return Float.toString(money);
            }
            else {
                return "0";
            }
        }
    }

    public String formatMoney_short(String s) {

        return "";
        // String s = "123.456 ";
        /*
         * float money = Float.valueOf(s).floatValue();
         * 
         * DecimalFormat format = null; format = (DecimalFormat)
         * NumberFormat.getInstance(); format.applyPattern("###0.00"); try {
         * String result = format.format(money); return result; } catch
         * (Exception e) { return Float.toString(money); }
         */
    }

    // 根据乘客类型得到相应编码
    public String GetPassType(String ty) {
        if (ty.equals("1")) {

            return "ADULT";// 成人
        }
        if (ty.equals("2")) {

            return "CHILD";// 儿童
        }
        if (ty.equals("3")) {

            return "INF";// 婴儿
        }
        return "ADULT";
    }

    public String GetNumberType(String ty) {
        if (ty.equals("1")) {

            return "1";// 身份证
        }
        if (ty.equals("2")) {

            return "2";// 护照
        }
        /*
         * if(ty.equals("3")){
         * 
         * return "3";//军官证 } if(ty.equals("1")){
         * 
         * return "4";//士兵证 }
         */
        if (ty.equals("5")) {

            return "5";// 台胞证

        }
        /*
         * if(ty.equals("3")){
         * 
         * return "6";//其它 }
         */
        return "6";
    }

    public static String fun(String str) {
        String temp = "";
        for (int i = 0; i < str.length() - 1; i++) {
            String ch = str.substring(i, i + 1);
            if (!ch.equals(" "))
                temp += ch;
        }
        return temp;
    }

    public FiveonBook getBook() {
        return book;
    }

    public void setBook(FiveonBook book) {
        this.book = book;
    }

    /**
     * 根据航班获取政策信息
     * 
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            GetPolicyDataByFlightsService_2_0Stub stub = new GetPolicyDataByFlightsService_2_0Stub();
            GetPolicyDataByFlightsService_2_0Stub.GetPolicyDataByFlights getPolicyDataByFlights = new GetPolicyDataByFlightsService_2_0Stub.GetPolicyDataByFlights();
            GetPolicyDataByFlightsService_2_0Stub.GetPolicyDataByFlightsRequest request = new GetPolicyDataByFlightsService_2_0Stub.GetPolicyDataByFlightsRequest();

            GetPolicyDataByFlightsService_2_0Stub.ArrayOfFlightNoInfo flightNoList = new GetPolicyDataByFlightsService_2_0Stub.ArrayOfFlightNoInfo();
            GetPolicyDataByFlightsService_2_0Stub.FlightNoInfo flightNoInfo1 = new GetPolicyDataByFlightsService_2_0Stub.FlightNoInfo();
            flightNoInfo1.setFlightNo(flightnumber);
            flightNoInfo1.setSeatClass(cabin);
            GetPolicyDataByFlightsService_2_0Stub.FlightNoInfo[] flightNoInfo = new GetPolicyDataByFlightsService_2_0Stub.FlightNoInfo[] { flightNoInfo1 };
            flightNoList.setFlightNoInfo(flightNoInfo);

            request.setFlightNoList(flightNoList);
            request.setDeparture(scity);
            request.setArrival(ecity);
            request.setDepartureDate(sdate);
            request.setIncludeLocalPolicy("1");
            request.setBusinessUnit("1");
            request.setParam1("");
            GetPolicyDataByFlightsService_2_0Stub.SecurityCredential sec = new GetPolicyDataByFlightsService_2_0Stub.SecurityCredential();
            sec.setAgencyCode(book.getAgentcode());
            String temp = book.getAgentcode() + request.getDeparture() + request.getArrival()
                    + request.getDepartureDate() + request.getIncludeLocalPolicy() + book.getSafecode();
            String sign = MD5Encrypt.md5(temp).toLowerCase();
            sec.setSign(sign);
            request.setCredential(sec);

            getPolicyDataByFlights.setIn0(request);
            int tempint = getRandomInt();
            GetPolicyDataByFlightsService_2_0Stub.GetPolicyDataByFlightsResponse response = stub
                    .getPolicyDataByFlights(getPolicyDataByFlights);
            if (response.getOut().getReturnCode().equals("F")) {// 匹配最优政策失败
                WriteLog.write("51book", "getZrateByFlightNumber:" + tempint + ":0:" + scity + ecity + sdate
                        + flightnumber + ":" + cabin + ":" + book.getAgentcode() + ":" + temp + ":" + sign);
                WriteLog.write("51book", "getZrateByFlightNumber:" + tempint + ":1:"
                        + response.getOut().getReturnCode() + ",ReturnMessage:" + response.getOut().getReturnMessage());
            }
            else {
            }
            // 开始解析政策
            if (response.getOut().getPolicyDatasList() != null
                    && response.getOut().getPolicyDatasList().getPolicyData() != null) {

                int len = response.getOut().getPolicyDatasList().getPolicyData().length;

                for (int a = 0; a < len; a++) {
                    GetPolicyDataByFlightsService_2_0Stub.PolicyData aa = response.getOut().getPolicyDatasList()
                            .getPolicyData()[a];
                    if (aa != null) {
                        WriteLog.write("51book",
                                "getZrateByFlightNumber:" + tempint + ":1:" + aa.getId() + ":" + aa.getRouteType()
                                        + ":" + aa.getCommision() + ":" + aa.getStartDate());
                        Zrate zrate = new Zrate();
                        zrate.setAgentid(5L);
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setCreateuser("job");
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifyuser("job");
                        zrate.setIsenable(1);
                        // zrate.setRemark(remark)
                        zrate.setOutid(aa.getId() + "");
                        zrate.setAircompanycode(aa.getAirlineCode());
                        if (aa.getFlightCourse() != null && aa.getFlightCourse().length() > 0
                                && aa.getFlightCourse().indexOf("-") != -1) {
                            zrate.setDepartureport(aa.getFlightCourse().split("-")[0]);// 出发机场

                            if (zrate.getDepartureport() != null && zrate.getDepartureport().equals("999")) {
                                if (aa.getDepartureExclude() != null) {
                                    zrate.setDepartureexclude(aa.getDepartureExclude());// 出发不适用
                                }
                            }

                            zrate.setArrivalport(aa.getFlightCourse().split("-")[1]);// 到达

                            if (zrate.getArrivalport() != null && zrate.getArrivalport().equals("999")) {
                                if (aa.getArrivalExclude() != null) {
                                    zrate.setArrivalexclude(aa.getArrivalExclude());// 到达不适用
                                }
                            }

                        }
                        if (aa.getCommision() > 0) {
                            zrate.setRatevalue(aa.getCommision());
                        }
                        zrate.setCabincode(aa.getSeatClass());
                        zrate.setFlightnumber(aa.getFlightNoIncluding());// 适用的航班
                        zrate.setWeeknum(aa.getFlightNoExclude());// 不适用的航班
                        zrate.setSchedule(aa.getFlightCycle());// 航班周期 1234567

                        // aa.getNeedSwitchPNR(); True——供应商需要更换成自己的PNR出票
                        // False——不许更换

                        // System.out.println("shijian=="+new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        // zrate.setBegindate(new
                        // Timestamp(aa.getStartDate().getTime().getTime()));
                        if (aa.getStartDate() != null && aa.getStartDate().getTime() != null) {
                            zrate.setBegindate(new Timestamp(aa.getStartDate().getTime().getTime()));
                        }
                        if (aa.getPrintTicketStartDate() != null && aa.getPrintTicketStartDate().getTime() != null) {
                            zrate.setIssuedstartdate(new Timestamp(aa.getPrintTicketStartDate().getTime().getTime()));
                        }
                        if (aa.getExpiredDate() != null && aa.getExpiredDate().getTime() != null) {
                            zrate.setIssuedendate(new Timestamp(aa.getExpiredDate().getTime().getTime())); // 出票结束时间
                        }
                        if (aa.getPrintTicketExpiredDate() != null && aa.getPrintTicketExpiredDate().getTime() != null) {
                            zrate.setEnddate(new Timestamp(aa.getPrintTicketExpiredDate().getTime().getTime())); // 政策有效期结束时间

                        }
                        // new Timestamp( new SimpleDateFormat("yyyy-MM-dd").p);

                        // zrate.setIssuedendate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getExpiredDate()+"").getTime()));
                        // //出票结束时间

                        // zrate.setEnddate(new Timestamp( new
                        // SimpleDateFormat("yyyy-MM-dd").parse(aa.getPrintTicketExpiredDate()+"").getTime()));
                        // //政策有效期结束时间

                        if (aa.getPolicyType() != null && aa.getPolicyType().equals("B2B")) {
                            zrate.setTickettype(2);

                        }
                        else {

                            zrate.setTickettype(1);
                        }

                        if (aa.getRouteType() != null && aa.getRouteType().equals("OW")) {// 单程

                            zrate.setVoyagetype("1");// 1单程,2:往返，3:单程或往返

                        }
                        else {
                            zrate.setVoyagetype("2");// 1单程,2:往返，3:单程或往返

                        }

                        if (aa.getWorkTime() != null && aa.getWorkTime().length() > 0
                                && aa.getWorkTime().indexOf("-") != -1) {
                            String worktime = aa.getWorkTime().split("-")[0];
                            zrate.setWorktime(worktime);
                            String afterworktime = aa.getWorkTime().split("-")[1];
                            zrate.setAfterworktime(afterworktime);

                        }
                        if (aa.getChooseOutWorkTime() != null) {
                            zrate.setOnetofivewastetime(aa.getChooseOutWorkTime());
                            zrate.setWeekendwastetime(aa.getChooseOutWorkTime());
                        }
                        if (aa.getBusinessUnitType() != null) {

                            if (aa.getBusinessUnitType().equals("0")) {// =0
                                // 普通政策
                                // =1
                                // 特殊政策

                                zrate.setGeneral(1l);
                                zrate.setZtype("1");
                            }
                            else {

                                zrate.setGeneral(2l);// 1,普通 2高反
                                zrate.setZtype("2");
                            }

                        }
                        else {

                            zrate.setGeneral(1l);
                            zrate.setZtype("1");
                        }
                        zrate.setUsertype("1");
                        if (aa.getAgencyEfficiency() != null) {
                            String speed = aa.getAgencyEfficiency();
                            if (speed.indexOf("分钟") >= 0) {
                                speed = speed.split("分钟")[0];
                            }
                            zrate.setSpeed(speed);
                        }
                        if (aa.getComment() != null) {
                            zrate.setRemark(aa.getComment());
                        }
                        if (zrate.getOutid() != null && zrate.getRatevalue() != null && zrate.getRatevalue() > 0
                                && zrate.getIsenable() == 1 && zrate.getAgentid() != null) {
                            if (zrate.getRatevalue() > 0) {
                                zrates.add(zrate);
                            }
                        }
                        else {
                            continue;
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return zrates;
    }

    //    public static FiveonBookVersion3 getFiveonbookversion3() {
    //        return fiveonbookversion3;
    //    }

    /**
     * 根据航班查询政策(51book v3.0)
     * 
     * @param segmentinfo
     *            行程
     * @param routeType
     *            航程类型：1 单程 2 往返 3联程 4 缺口程
     * @param backDate
     *            返程日期，当routeType不为1时，backDate必填
     * @return
     */
    public static List<Zrate> getPolicyAndFareInfoByFlights(Segmentinfo segmentinfo, String routeType, String backDate) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        try {
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsRequest getpolicyandfarebyflightsrequest = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsRequest();
            getpolicyandfarebyflightsrequest.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
            String depCode = segmentinfo.getStartairport();
            String arrCode = segmentinfo.getEndairport();

            String signStr = fiveonbookversion3.getFiveoneUsernameV3() + arrCode + depCode
                    + fiveonbookversion3.getFiveonePasswordV3();
            signStr = MD5Util.MD5Encode(signStr, "UTF-8");
            getpolicyandfarebyflightsrequest.setSign(signStr);
            getpolicyandfarebyflightsrequest.setDepCode(depCode);
            getpolicyandfarebyflightsrequest.setArrCode(arrCode);
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.FlightNoInfo flightnoinfo = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.FlightNoInfo();
            flightnoinfo.setFlightNo(segmentinfo.getFlightnumber());
            flightnoinfo.setSeatClass(segmentinfo.getCabincode());
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.FlightNoInfo[] flightnoinfolist = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.FlightNoInfo[1];
            flightnoinfolist[0] = flightnoinfo;
            getpolicyandfarebyflightsrequest.setFlightList(flightnoinfolist);
            String depDate = segmentinfo.getDeparttime().toString();
            depDate = depDate.substring(0, 10);
            getpolicyandfarebyflightsrequest.setDepDate(depDate);
            getpolicyandfarebyflightsrequest.setNeedLocalPolicy("1");
            getpolicyandfarebyflightsrequest.setNeedSpeRulePolicy(1);
            getpolicyandfarebyflightsrequest.setNeedSpePricePolicy(1);
            getpolicyandfarebyflightsrequest.setOnlyOnWorking(1);
            getpolicyandfarebyflightsrequest.setAllowSwitchPnr(1);
            getpolicyandfarebyflightsrequest.setRouteType(routeType);
            getpolicyandfarebyflightsrequest.setBackDate(backDate);
            getpolicyandfarebyflightsrequest.setParam1("15");
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlights getpolicyandfarebyflights = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlights();
            getpolicyandfarebyflights.setRequest(getpolicyandfarebyflightsrequest);
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsE getpolicyandfarebyflightse = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsE();
            getpolicyandfarebyflightse.setGetPolicyAndFareByFlights(getpolicyandfarebyflights);

            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub stub = new GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub();
            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsResponseE responseE = stub
                    .getPolicyAndFareByFlights(getpolicyandfarebyflightse);

            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsResponse response = responseE
                    .getGetPolicyAndFareByFlightsResponse();

            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.GetPolicyAndFareByFlightsReply reply = response
                    .get_return();
            if (reply.getReturnCode() != null && reply.getReturnCode().equals("S")) {
                if (reply.getFlightDataList() != null && reply.getFlightDataList().length > 0) {
                    GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsFlightData[] arrWsFlightData = reply
                            .getFlightDataList();
                    if (arrWsFlightData != null && arrWsFlightData.length > 0) {
                        for (int i = 0; i < arrWsFlightData.length; i++) {
                            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsFlightData wsflightdata = arrWsFlightData[i];
                            if (wsflightdata != null) {
                                GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsSeatAndPolicy[] arrWsSeatAndPolicy = wsflightdata
                                        .getSeatAndPolicyList();
                                if (arrWsSeatAndPolicy != null && arrWsSeatAndPolicy.length > 0) {
                                    for (int j = 0; j < arrWsSeatAndPolicy.length; j++) {
                                        GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsSeatAndPolicy wsseatandpolicy = arrWsSeatAndPolicy[j];
                                        if (wsseatandpolicy != null) {
                                            GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsPolicyData[] arrWsPolicyData = wsseatandpolicy
                                                    .getPolicyList();
                                            if (arrWsPolicyData != null && arrWsPolicyData.length > 0) {
                                                for (int k = 0; k < arrWsPolicyData.length; k++) {
                                                    GetPolicyAndFareByFlightsServiceImpl_1_0ServiceStub.WsPolicyData wspolicydata = arrWsPolicyData[k];
                                                    try {
                                                        Zrate zrate = new Zrate();
                                                        zrate.setAgentid(5L);
                                                        zrate.setCreateuser("51book(v3.0)");
                                                        zrate.setModifyuser("51book(v3.0)");
                                                        zrate.setIsenable(1);
                                                        zrate.setOutid(String.valueOf(wspolicydata.getPolicyId()));
                                                        zrate.setAircompanycode(wspolicydata.getAirlineCode());

                                                        Float ratevalue = (float) wspolicydata.getCommisionPoint();
                                                        ratevalue = getNewRateValue(ratevalue, 0D, segmentinfo);
                                                        zrate.setRatevalue(ratevalue.floatValue());

                                                        if (wspolicydata.getPolicyType() != null) {
                                                            if (wspolicydata.getPolicyType().equals("1")) {
                                                                zrate.setTickettype(1);
                                                            }
                                                            else if (wspolicydata.getPolicyType().equals("2")) {
                                                                zrate.setTickettype(2);
                                                            }
                                                        }
                                                        if (wspolicydata.getRouteType() != null) {
                                                            if (wspolicydata.getRouteType().equals("OW")) {
                                                                zrate.setVoyagetype("1");
                                                            }
                                                            else if (wspolicydata.getRouteType().equals("RT")) {
                                                                zrate.setVoyagetype("2");
                                                            }
                                                        }
                                                        if (wspolicydata.getWorkTime() != null
                                                                && !wspolicydata.getWorkTime().equals("")) {
                                                            if (wspolicydata.getWorkTime().contains("小时")) {
                                                                zrate.setWorktime("08:00");
                                                                zrate.setAfterworktime("19:00");
                                                            }
                                                            else {
                                                                String[] str = wspolicydata.getWorkTime().split("-");
                                                                zrate.setWorktime(str[0].trim());
                                                                zrate.setAfterworktime(str[1].trim());
                                                            }
                                                        }
                                                        if (wspolicydata.getVtWorkTime() != null
                                                                && !wspolicydata.getVtWorkTime().equals("")) {
                                                            if (wspolicydata.getVtWorkTime().contains("小时")) {
                                                                zrate.setOnetofivewastetime("19:00");
                                                                zrate.setWeekendwastetime("19:00");
                                                            }
                                                            else {
                                                                String[] str = wspolicydata.getVtWorkTime().split("-");
                                                                zrate.setOnetofivewastetime(str[1].trim());
                                                                zrate.setWeekendwastetime(str[1].trim());
                                                            }
                                                        }
                                                        if (wspolicydata.getProductType() == 0) {
                                                            zrate.setGeneral(1L);
                                                        }
                                                        else if (wspolicydata.getProductType() == 1) {
                                                            zrate.setGeneral(2L);
                                                        }
                                                        String ticketSpeed = wspolicydata.getTicketSpeed();
                                                        zrate.setSpeed(ticketSpeed);
                                                        zrate.setRemark(wspolicydata.getComment());
                                                        if (zrate.getRatevalue() > 0) {
                                                            zrateList.add(zrate);
                                                        }
                                                    }
                                                    catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                System.out.println(reply.getReturnMessage());
                WriteLog.write("根据航班查询政策(51book(v3.0))", "根据航班查询政策从接口中返回的错误信息:" + reply.getReturnMessage());
                return zrateList;
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return zrateList;
    }

    /**
     * 根据RT、PAT创建订单(51book v3.0)
     * 
     * @param rttxt
     * @param pattxt
     * @param policyId
     *            政策id
     * @return 如果未创建成功返回 -1 成功：返回 "S|" + orderNO + "|" + payurl + "|" + payfee;
     */
    public String createOrderByRtPat(String rttxt, String pattxt, String policyId) {
        long l1 = System.currentTimeMillis();
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        String str = "-1";
        try {
            WriteLog.write("CreateOrder51book(v3.0)", l1 + ":根据RT、PAT创建订单向接口中传入的数据[RT:" + rttxt + "---PAT:" + pattxt
                    + "---政策id:" + policyId + "]");
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatRequest createorderbyrtpatrequest = new CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatRequest();
            createorderbyrtpatrequest.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
            String signStr = fiveonbookversion3.getFiveoneUsernameV3() + fiveonbookversion3.getFiveoneLinknameV3()
                    + fiveonbookversion3.getFiveoneLinknameV3() + fiveonbookversion3.getFiveoneLinkphoneV3() + policyId
                    + fiveonbookversion3.getFiveonePasswordV3();
            signStr = MD5Util.MD5Encode(signStr, "UTF-8");
            WriteLog.write("CreateOrder51book(v3.0)", l1 + ":policyId:" + policyId);
            WriteLog.write("CreateOrder51book(v3.0)",
                    l1 + ":fiveonbookversion3:" + JSONObject.toJSONString(fiveonbookversion3));
            createorderbyrtpatrequest.setSign(signStr);
            createorderbyrtpatrequest.setPnrTxt(rttxt);
            createorderbyrtpatrequest.setPataTxt(pattxt);
            createorderbyrtpatrequest.setPolicyId(Integer.valueOf(policyId));
            createorderbyrtpatrequest.setAllowSwitchPolicy(0);
            createorderbyrtpatrequest.setNeedProductType(3);
            createorderbyrtpatrequest.setAllowSwitchPnr(1);
            createorderbyrtpatrequest.setLinkMan(fiveonbookversion3.getFiveoneLinknameV3());
            createorderbyrtpatrequest.setLinkPhone(fiveonbookversion3.getFiveoneLinkphoneV3());
            createorderbyrtpatrequest.setCreatedBy(fiveonbookversion3.getFiveoneLinknameV3());
            createorderbyrtpatrequest.setTicketNotifiedUrl(fiveonbookversion3.getFiveoneTicketNotifiedUrlV3());
            createorderbyrtpatrequest.setPaymentReturnUrl(fiveonbookversion3.getFiveonePaymentReturnUrlV3());
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPat createorderbyrtpat = new CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPat();
            createorderbyrtpat.setRequest(createorderbyrtpatrequest);
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatE createorderbyrtpate = new CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatE();
            createorderbyrtpate.setCreateOrderByRtPat(createorderbyrtpat);
            CreateOrderByRtPatServiceImpl_1_0ServiceStub stub = new CreateOrderByRtPatServiceImpl_1_0ServiceStub();
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatResponseE responseE = stub
                    .createOrderByRtPat(createorderbyrtpate);
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatResponse response = responseE
                    .getCreateOrderByRtPatResponse();
            CreateOrderByRtPatServiceImpl_1_0ServiceStub.CreateOrderByRtPatReply reply = response.get_return();
            WriteLog.write("CreateOrder51book(v3.0)", l1 + ":" + reply.getReturnMessage());
            WriteLog.write("CreateOrder51book(v3.0)", l1 + ":" + reply.getReturnCode());
            if ("S".equals(reply.getReturnCode())) {
                CreateOrderByRtPatServiceImpl_1_0ServiceStub.WsOrder wsorder = reply.getPolicyOrder();
                String orderNO = wsorder.getLiantuoOrderNo();
                CreateOrderByRtPatServiceImpl_1_0ServiceStub.WsPaymentInfo wspaymentinfo = wsorder.getPaymentInfo();
                String payfee = String.valueOf(wspaymentinfo.getTotalPay());
                String payurl = wspaymentinfo.getPaymentUrl();
                String supplyOfficeNo = "";
                try {
                    supplyOfficeNo = wsorder.getPolicyData().getSupplyOfficeNo();
                }
                catch (Exception e) {
                }
                str = "S|" + orderNO + "|" + payurl + "|" + supplyOfficeNo + "|" + payfee;
            }
            else {
                str = "F|" + reply.getReturnMessage();
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        WriteLog.write("CreateOrder51book(v3.0)", l1 + ":" + str);
        return str;
    }

    /**
     * 根据订单号获取支付链接(51book v3.0)
     * 
     * @param orderid
     *            订单号
     * @return
     */
    public String getPayUrl123(String orderid) {
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        try {
            if (fiveonbookversion3.getFiveoneStatusV3().equals("1")) {
                WriteLog.write("获取支付链接（51book(v3.0))", "根据订单号获取支付链接向接口中传入的数据[订单号:" + orderid + "]");
                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlRequest getpaymenturlrequest = new GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlRequest();
                getpaymenturlrequest.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
                String signStr = fiveonbookversion3.getFiveoneUsernameV3() + orderid
                        + fiveonbookversion3.getFiveonePasswordV3();
                signStr = MD5Util.MD5Encode(signStr, "UTF-8");
                getpaymenturlrequest.setSign(signStr);
                getpaymenturlrequest.setOrderNo(orderid);

                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrl getpaymenturl = new GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrl();
                getpaymenturl.setRequest(getpaymenturlrequest);

                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlE getpaymenturle = new GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlE();
                getpaymenturle.setGetPaymentUrl(getpaymenturl);

                GetPaymentUrlServiceImpl_1_0ServiceStub stub = new GetPaymentUrlServiceImpl_1_0ServiceStub();
                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlResponseE responseE = stub
                        .getPaymentUrl(getpaymenturle);

                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlResponse response = responseE
                        .getGetPaymentUrlResponse();

                GetPaymentUrlServiceImpl_1_0ServiceStub.GetPaymentUrlReply reply = response.get_return();
                if (reply.getReturnCode() != null && reply.getReturnCode().equals("S")) {
                    if (reply.getPaymentUrl() != null && !reply.getPaymentUrl().equals("")) {
                        String payUrl = reply.getPaymentUrl();
                        WriteLog.write("获取支付链接（51book(v3.0))", "根据订单号获取支付链接从接口中返回的URL[" + payUrl + "]");
                        return payUrl;
                    }
                }
                else {
                    WriteLog.write("获取支付链接（51book(v3.0))", "根据订单号获取支付链接从接口中返回的错误信息[" + reply.getReturnMessage() + "]");
                }
            }
            else {
                System.out.println("51book version 3.0 接口被禁用");
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return "FAIL";
    }

    /**
     * 订单自动支付(51book v3.0)
     * 
     * @param orderNO
     *            外部订单号
     * @param orderId
     *            本地订单号
     * @param payType
     *            支付类型：0支付宝 1 财付通 2 汇付
     * @param payMethod
     *            支付方式：1-自动支付 2-虚拟支付
     * 
     * @return
     */
    public String autoPayOrder(String orderNO, String orderId, String payType, String payMethod) {
        String resultStr = "-1";
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        try {
            WriteLog.write("自动代扣", orderId + ":订单自动支付向接口中传入的数据[外部订单号:" + orderNO + "---支付类型" + payType + "---支付方式"
                    + payMethod + "]");
            PayServiceImpl_1_0ServiceStub.PayRequest payrequest = new PayServiceImpl_1_0ServiceStub.PayRequest();
            payrequest.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
            String signStr = fiveonbookversion3.getFiveoneUsernameV3() + orderNO + payMethod
                    + fiveonbookversion3.getFiveoneLinknameV3() + fiveonbookversion3.getFiveonePasswordV3();
            WriteLog.write("自动代扣", orderId + ":" + signStr);
            signStr = MD5Util.MD5Encode(signStr, "UTF-8");
            WriteLog.write("自动代扣", orderId + ":" + signStr);
            payrequest.setSign(signStr);
            payrequest.setOrderNo(orderNO);
            payrequest.setPayType(payMethod);
            payrequest.setPayerLoginName(fiveonbookversion3.getFiveoneLinknameV3());
            payrequest.setParam1(payType);

            PayServiceImpl_1_0ServiceStub.Pay pay = new PayServiceImpl_1_0ServiceStub.Pay();
            pay.setRequest(payrequest);

            PayServiceImpl_1_0ServiceStub.PayE paye = new PayServiceImpl_1_0ServiceStub.PayE();
            paye.setPay(pay);

            PayServiceImpl_1_0ServiceStub stub = new PayServiceImpl_1_0ServiceStub();
            PayServiceImpl_1_0ServiceStub.PayResponseE responseE = stub.pay(paye);

            PayServiceImpl_1_0ServiceStub.PayResponse response = responseE.getPayResponse();

            PayServiceImpl_1_0ServiceStub.PayReply reply = response.get_return();
            WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0):" + reply.getOrderNo());
            WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0):" + reply.getReturnMessage());
            WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0):" + reply.getReturnCode());
            WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0):" + reply.getOrderStatus());

            if ("S".equals(reply.getReturnCode())) {
                resultStr = "S";
            }
            else {
                resultStr = "F";
                WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0)接口日志订单自动支付从接口中返回的错误信息[" + reply.getReturnMessage()
                        + "]");
            }
            if (reply.getPaymentInfo() != null) {
                WriteLog.write("自动代扣", orderId + ":" + "51book(v3.0):" + reply.getPaymentInfo().getPayerAccount() + "-"
                        + reply.getPaymentInfo().getTotalPay() + "-" + reply.getPaymentInfo().getPayTradeNo());
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        WriteLog.write("自动代扣", orderId + ":" + "return:" + resultStr);
        return resultStr;
    }

    /**
     * 根据pnr获取政策(51book v3.0)
     * 
     * @param order
     * @return
     */
    public static List<Zrate> getPolicyByPNR(Orderinfo order) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        zrateList = getZrateByRtAndPatFromFiveOneBook(order);
        if (zrateList.size() == 0) {
            zrateList = getZrateByFlightNoFromFiveOneBook(order);
        }
        return zrateList;
    }

    /**
     * 根据航班号获取政策
     * @param order
     * @return
     * @time 2016年6月20日 下午7:56:30
     * @author chendong
     */
    private static List<Zrate> getZrateByFlightNoFromFiveOneBook(Orderinfo order) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        String sql = "select * from T_SEGMENTINFO where c_orderid=" + order.getId();
        List<Segmentinfo> list = Server.getInstance().getAirService().findAllSegmentinfoBySql(sql, -1, 0);
        if (list.size() > 0) {
            Segmentinfo segmentinfo = list.get(0);
            zrateList = getPolicyAndFareInfoByFlights(segmentinfo, "1", "");
        }
        return zrateList;
    }

    /**
     * 
     * @param order
     * @return
     * @time 2016年6月20日 下午7:55:46
     * @author chendong
     */
    private static List<Zrate> getZrateByRtAndPatFromFiveOneBook(Orderinfo order) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        FiveonBookVersion3 fiveonbookversion3 = new FiveonBookVersion3();
        try {
            String rt = order.getUserRtInfo();
            String pat = order.getPnrpatinfo();
            rt = rt.replaceAll("", "").trim();
            pat = pat.replaceAll("", "").trim();
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtRequest getpolicybypnrtxtrequest = new GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtRequest();
            getpolicybypnrtxtrequest.setAgencyCode(fiveonbookversion3.getFiveoneUsernameV3());
            //agencyCode+allowSwitchPnr+needSpePricePolicy +needSpeRulePolicy+onlyOnWorking+安全码
            int allowSwitchPnr = 1;//是否可更换PNR出票 Integer 是   0 只用自己的PNR出票            1可以更换PNR后出票
            int needSpePricePolicy = 1;//是否包括特价政策 Integer 是   0 不包括            1 包括
            int needSpeRulePolicy = 1;//是否包括特殊政策    Integer 是   0 不包括             1 包括
            int onlyOnWorking = 1;//是否只返回在工作时间内政策   Integer  是   0仅返回当前仍在工作时间的政策;            1不做限制
            String signStr = fiveonbookversion3.getFiveoneUsernameV3() + allowSwitchPnr + needSpePricePolicy
                    + needSpeRulePolicy + onlyOnWorking + fiveonbookversion3.getFiveonePasswordV3();
            WriteLog.write("51pnr取政策v3.0_other", order.getId() + ":" + signStr);
            signStr = MD5Util.MD5Encode(signStr, "UTF-8");
            WriteLog.write("51pnr取政策v3.0_other", order.getId() + ":" + signStr);
            WriteLog.write("51pnr取政策v3.0_other", order.getId() + ":" + rt);
            rt = rt.replace("\n", " ");
            WriteLog.write("51pnr取政策v3.0", order.getId() + ":" + rt);
            WriteLog.write("51pnr取政策v3.0_other", order.getId() + ":" + pat);
            pat = pat.replace("\n", " ");
            WriteLog.write("51pnr取政策v3.0", order.getId() + ":" + pat);
            getpolicybypnrtxtrequest.setSign(signStr);
            getpolicybypnrtxtrequest.setPnrTxt(rt);
            getpolicybypnrtxtrequest.setPataTxt(pat);
            getpolicybypnrtxtrequest.setAllowSwitchPnr(allowSwitchPnr);
            getpolicybypnrtxtrequest.setNeedSpePricePolicy(needSpePricePolicy);
            getpolicybypnrtxtrequest.setNeedSpeRulePolicy(needSpeRulePolicy);
            getpolicybypnrtxtrequest.setOnlyOnWorking(onlyOnWorking);
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxt getpolicybypnrtxt = new GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxt();
            getpolicybypnrtxt.setRequest(getpolicybypnrtxtrequest);
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE getpolicybypnrtxte = new GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtE();
            getpolicybypnrtxte.setGetPolicyByPnrTxt(getpolicybypnrtxt);
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub stub = new GetPolicyByPnrTxtServiceImpl_1_0ServiceStub();
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponseE responseE = stub
                    .getPolicyByPnrTxt(getpolicybypnrtxte);
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtResponse response = responseE
                    .getGetPolicyByPnrTxtResponse();
            GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.GetPolicyByPnrTxtReply reply = response.get_return();
            if (reply.getReturnCode() != null && reply.getReturnCode().equals("S")) {

                WriteLog.write("51pnr取政策v3.0", order.getId() + ":获取成功:" + reply.getPolicyList().length);
                String s_param1 = reply.getParam1();
                WriteLog.write("51pnr取政策v3.0", order.getId() + ":Param1:" + s_param1);
                float f_param1 = 0;
                try {
                    f_param1 = Float.parseFloat(s_param1);
                }
                catch (Exception e) {
                }
                GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.WsPolicyData[] arrayWsPolicyData = reply.getPolicyList();
                if (arrayWsPolicyData != null && arrayWsPolicyData.length > 0) {
                    for (int i = 0; i < arrayWsPolicyData.length; i++) {
                        GetPolicyByPnrTxtServiceImpl_1_0ServiceStub.WsPolicyData wspolicydata = arrayWsPolicyData[i];
                        if (wspolicydata != null) {
                            try {
                                WriteLog.write("51pnr取政策v3.0",
                                        order.getId() + ":" + JSONObject.toJSONString(wspolicydata));
                            }
                            catch (Exception e) {
                            }
                            Zrate zrate = new Zrate();
                            zrate.setTotalfaceprice(f_param1);//获取到的票面价
                            zrate.setAgentid(5L);
                            zrate.setCreateuser("51book(v3.0)");
                            zrate.setModifyuser("51book(v3.0)");
                            zrate.setIsenable(1);
                            zrate.setOutid(String.valueOf(wspolicydata.getPolicyId()));
                            zrate.setAircompanycode(wspolicydata.getAirlineCode());
                            zrate.setCabincode(wspolicydata.getSeatClass());
                            Float ratevalue = wspolicydata.getCommisionPoint();
                            if (order.getSegmentlist() != null) {
                                ratevalue = getNewRateValue(ratevalue, wspolicydata.getCommisionMoney(), order
                                        .getSegmentlist().get(0));
                            }
                            zrate.setRatevalue(ratevalue);
                            //                            zrate.setRatevalueMoney(wspolicydata.getCommisionMoney());//返钱的金额

                            Long ischange = 0L;// 是否需转换记录 是否换编码出票（0：不是换编码出票； 1：是换编码出票；）
                            ischange = (long) wspolicydata.getNeedSwitchPNR();//1需要更换PNR出票                            0不更换PNR
                            zrate.setIschange(ischange);
                            if (wspolicydata.getPolicyType() != null) {
                                if (wspolicydata.getPolicyType().equals("B2B")) {
                                    zrate.setTickettype(2);
                                }
                                else {
                                    zrate.setTickettype(1);
                                }
                            }
                            if (wspolicydata.getWorkTime() != null && !wspolicydata.getWorkTime().equals("")) {
                                if (wspolicydata.getWorkTime().contains("小时")) {
                                    zrate.setWorktime("08:00");
                                    zrate.setAfterworktime("19:00");
                                }
                                else {
                                    String[] str = wspolicydata.getWorkTime().split("-");
                                    if (str != null && str.length > 0) {
                                        zrate.setWorktime(str[0].trim().replaceAll(":", ""));
                                        zrate.setAfterworktime(str[1].trim().replaceAll(":", ""));
                                    }
                                }
                            }
                            if (wspolicydata.getVtWorkTime() != null && !wspolicydata.getVtWorkTime().equals("")) {
                                if (wspolicydata.getVtWorkTime().contains("小时")) {
                                    zrate.setOnetofivewastetime("19:00");
                                    zrate.setWeekendwastetime("19:00");
                                }
                                else {
                                    String[] str = wspolicydata.getVtWorkTime().split("-");
                                    if (str != null && str.length > 0) {
                                        zrate.setOnetofivewastetime(str[1].trim().replaceAll(":", ""));
                                        zrate.setWeekendwastetime(str[1].trim().replaceAll(":", ""));
                                    }
                                }
                            }
                            if (wspolicydata.getTicketSpeed() != null && !wspolicydata.getTicketSpeed().equals("")) {
                                String[] str = wspolicydata.getTicketSpeed().split("分钟");
                                if (str != null && str.length > 0) {
                                    if (str[0].indexOf("秒钟") >= 0) {
                                        zrate.setSpeed("1分钟");
                                    }
                                    else {
                                        zrate.setSpeed(str[0].trim() + "分钟");
                                    }
                                }
                            }
                            Long general = getGeneral(wspolicydata);//获取政策类型
                            zrate.setGeneral(general);
                            String voyagetype = getVoyagetype(wspolicydata);// 航程类型 返回1表示单程，返回2表示往返，返回3表示单程往返
                            zrate.setVoyagetype(voyagetype);
                            zrate.setRemark(wspolicydata.getComment());
                            zrate.setOfficeno(wspolicydata.getSupplyOfficeNo());
                            WriteLog.write("51pnr取政策v3.0", order.getId() + ":Outid:" + zrate.getOutid() + ":Ratevalue:"
                                    + zrate.getRatevalue() + ":General:" + zrate.getGeneral());
                            if (zrate.getRatevalue() > 0) {
                                zrateList.add(zrate);
                            }
                        }
                    }
                }
            }
            else {
                WriteLog.write("51pnr取政策v3.0", order.getId() + ":" + "根据pnr获取政策从接口中返回的错误信息[" + reply.getReturnMessage()
                        + "]");
                WriteLog.write("51pnr取政策v3.0", order.getId() + ":" + "根据pnr获取政策从接口中返回的错误信息[" + reply.getReturnCode()
                        + "]");
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
            logger.error(JSONObject.toJSONString(order), e);
        }
        catch (RemoteException e) {
            //            e.printStackTrace();
            logger.error(JSONObject.toJSONString(order) + ":" + e.fillInStackTrace());
        }
        return zrateList;
    }

    private static String getVoyagetype(WsPolicyData wspolicydata) {
        String voyagetype = "1";
        if (wspolicydata.getRouteType() != null) {
            if (wspolicydata.getRouteType().equals("OW")) {
                voyagetype = "1";
            }
            else if (wspolicydata.getRouteType().equals("RT")) {
                voyagetype = "2";
            }
        }
        return voyagetype;
    }

    /**
     * 获取政策类型
     * 
     * @param wspolicydata
     * @return
     * @time 2016年10月13日 下午4:36:50
     * @author chendong
     */
    private static Long getGeneral(WsPolicyData wspolicydata) {
        Long general = 1L;
        if (wspolicydata.getProductType() == 0) {
            general = 1L;
        }
        else if (wspolicydata.getProductType() == 1) {
            general = 1L;
        }
        else if (wspolicydata.getProductType() == 2) {
            general = 2L;
        }
        else if (wspolicydata.getProductType() == 3) {
            general = 2L;
        }
        return general;
    }

    /**
     * 
     * @return
     * @time 2016年7月1日 上午11:18:38
     * @author chendong
     * @param wspolicydata 
     * @param ratevalue 
     * @param order 
     */
    private static Float getNewRateValue(Float ratevalue, Double commisionMoney, Segmentinfo segmentInfo) {

        try {
            if (commisionMoney > 0) {
                if (segmentInfo != null) {
                    Float price = segmentInfo.getParvalue();
                    Float moneyPoint = (float) (commisionMoney / price * 100);
                    ratevalue += moneyPoint;
                    DecimalFormat decimalFormat = new DecimalFormat("0.0");//构造方法的字符格式这里如果小数不足1位,会以0补足.
                    String p = decimalFormat.format(ratevalue);//format 返回的是字符串
                    ratevalue = Float.parseFloat(p);
                }
            }
        }
        catch (Exception e) {
        }
        return ratevalue;
    }

    /**
     * 根据条件从51book的政策数据库里获取政策
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @return
     */
    public List<Zrate> getZrateByFlightNumberbyDBusesp(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        long l2 = System.currentTimeMillis();
        List<Zrate> zrates = new ArrayList<Zrate>();
        List list = new ArrayList();
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String strSP = "[dbo].[sp_GetZrateByFlight] " + "@chufajichang = N'" + scity + "'," + "@daodajichang = N'"
                + ecity + "'," + "@chufariqi = N'" + sdate + "'," + "@dangqianshijian= N'" + mDateTime + "',"
                + "@hangkonggongsi= N'" + flightnumber.substring(0, 2) + "'," + "@cangwei= N'" + cabin + "',"
                + "@hangbanhao= N'" + flightnumber + "'," + "@ismulity=1,@isgaofan=1";
        try {
            String url = getSysconfigString("51bookzrateurl") + "/cn_service/service/";
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService servier = (ISystemService) factory.create(ISystemService.class,
                    url + ISystemService.class.getSimpleName());
            list = servier.findMapResultByProcedure(strSP);
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Zrate zrate = new Zrate();
                    Map zrateMap = (Map) list.get(i);
                    if (zrateMap.get("id") != null) {
                        zrate.setId(Integer.valueOf(zrateMap.get("id").toString()));
                    }
                    if (zrateMap.get("afterworktime") != null) {
                        zrate.setAfterworktime(zrateMap.get("afterworktime").toString());
                    }
                    if (zrateMap.get("ratevalue") != null) {
                        zrate.setRatevalue(Float.valueOf(zrateMap.get("ratevalue").toString()));
                    }
                    if (zrateMap.get("remark") != null) {
                        zrate.setRemark(zrateMap.get("remark").toString());
                    }
                    if (zrateMap.get("speed") != null) {
                        zrate.setSpeed(zrateMap.get("speed").toString());
                    }
                    if (zrateMap.get("onetofivewastetime") != null) {
                        zrate.setOnetofivewastetime(zrateMap.get("onetofivewastetime").toString());
                    }
                    if (zrateMap.get("worktime") != null) {
                        zrate.setWorktime(zrateMap.get("worktime").toString());
                    }
                    if (zrateMap.get("outid") != null) {
                        zrate.setOutid(zrateMap.get("outid").toString());
                    }
                    if (zrateMap.get("general") != null) {
                        zrate.setGeneral(Long.valueOf(zrateMap.get("general").toString()));
                    }
                    if (zrateMap.get("agentid") != null) {
                        zrate.setAgentid(Long.valueOf(zrateMap.get("agentid").toString()));
                    }
                    zrates.add(zrate);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("t5");
        System.out.println(System.currentTimeMillis() - l2);
        return zrates;
    }

    /**
     * 51book从航班查询时保存的政策里面查询政策信息
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @param url2
     * @return
     */
    public List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String url2) {
        // TODO Auto-generated method stub
        String where = "where C_AGENTID=5 and C_BEGINDATE = '" + sdate + "' and C_DEPARTUREPORT='" + scity
                + "' and C_ARRIVALPORT='" + ecity + "' and C_AIRCOMPANYCODE='" + flightnumber.substring(0, 2)
                + "' and C_FLIGHTNUMBER='" + flightnumber + "' and C_CABINCODE='" + cabin + "'";
        url2 = url2 + "/cn_service/service/";
        HessianProxyFactory factory = new HessianProxyFactory();
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            zrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url2);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            WriteLog.write("getzrateurlerr", "51_getZrateByFlightNumber:" + url2);
        }

        if (zrates.size() == 0) {
            zrates = new ArrayList<Zrate>();
        }
        return zrates;
    }

    public static void main(String[] args) {
        testGetZrateByPnr();
    }

    /**
     * 测试根据pnr获取政策
     * 
     * @time 2016年10月13日 下午12:10:50
     * @author chendong
     */
    private static void testGetZrateByPnr() {

        Orderinfo order = new Orderinfo();
        order.setId(System.currentTimeMillis());
        String userRtInfo = "rtKNCT9M                                                                       "
                + " 1.陈栋 KNCT9M                                                                  "
                + " 2.  CA1501 A   SU30OCT  PEKSHA HK1   0830 1040          E T3T2                 "
                + " 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG   "
                + " 4.REMARK 1013 1057 GUEST                                                       "
                + " 5.TL/0630/30OCT/PEK242                                                         "
                + " 6.SSR FOID CA HK1 NI410883197601013011/P1                                      "
                + " 7.SSR ADTK 1E BY PEK23OCT16/0830 OR CXL CA ALL SEGS                            "
                + " 8.OSI CA CTCT15538956152                                                       "
                + " 9.OSI CA CTCT13439311111                                                       "
                + "10.RMK CA/MVN98L                                                                " + "11.PEK242";
        order.setUserRtInfo(userRtInfo);
        System.out.println(userRtInfo);
        String pnrpatinfo = ">PAT:A                                                                          "
                + "01 A FARE:CNY3780.00 TAX:CNY50.00 YQ:TEXEMPTYQ  TOTAL:3830.00                   "
                + "SFC:01   SFN:01                                                               "
                + "01 A/CA1T162469 FARE:CNY2110.00 TAX:CNY50.00 YQ:TEXEMPTYQ  TOTAL:2160.00        "
                + "SFC:01   SFN:01                                                               "
                + "01 A/CA1T161987 FARE:CNY3100.00 TAX:CNY50.00 YQ:TEXEMPTYQ  TOTAL:3150.00        "
                + "SFC:01   SFN:01  " + "";
        System.out.println(pnrpatinfo);
        order.setPnrpatinfo(pnrpatinfo);
        List<Zrate> fivezrates = FiveoneBookutil.getPolicyByPNR(order);
        for (int i = 0; i < fivezrates.size(); i++) {
            System.out.println(fivezrates.get(i));
        }

    }

}
