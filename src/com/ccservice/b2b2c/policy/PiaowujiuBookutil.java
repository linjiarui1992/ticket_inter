package com.ccservice.b2b2c.policy;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.ccservice.b2b2c.base.fguest.Fguest;
import com.ccservice.b2b2c.base.forderinfo.Forderinfo;
import com.ccservice.b2b2c.base.interflight.SearchFligehtBean;
import com.ccservice.b2b2c.base.interflight.SearchPointBean;
import com.ccservice.b2b2c.base.interticket.AirSegementBean;
import com.ccservice.b2b2c.base.interticket.AllRouteBean;
import com.ccservice.b2b2c.base.interticket.FlightinfoBean;
import com.ccservice.b2b2c.base.interticket.RouteBean;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.util.AirUtil;
import com.ccservice.inter.job.WriteLog;

/**
 * 查询国际航班-piao59 作者：邹远超 日期：2014-4-11
 */
public class PiaowujiuBookutil extends SupplyMethod {
    public final static Logger logger = Logger.getLogger(PiaowujiuBookutil.class.getSimpleName());

    /**
     * 作者：邹远超 日期：2014-4-10 说明：查询国际航班-piao59
     * 
     * @param fromCtiy
     * @param toCity
     * @param fromDate
     * @param returnDate
     * @param airCo
     * @param seatType
     * @return 01
     */
    public AllRouteBean SeachInterFlight(SearchFligehtBean searchFligehtBean) {
        List<SearchPointBean> searchPointBeans = searchFligehtBean.getPoints();
        /*String urlstr = "http://ws.hangtian123.com/htslinterpiao?";
        if (null != searchPointBeans.get(0).getReturnTime()) {
            urlstr += "type=1&paramContent=fromCity=" + searchPointBeans.get(0).getStartCity() + "~toCity="
                    + searchPointBeans.get(0).getEndCity() + "~fromDate=" + searchPointBeans.get(0).getStartTime()
                    + "~returnDate=" + searchPointBeans.get(0).getReturnTime() + "~airCo="
                    + searchPointBeans.get(0).getAirCompany() + "~seatType=" + searchPointBeans.get(0).getSeatType();
        }
        else {
            urlstr += "type=1&paramContent=fromCity=" + searchPointBeans.get(0).getStartCity() + "~toCity="
                    + searchPointBeans.get(0).getEndCity() + "~fromDate=" + searchPointBeans.get(0).getStartTime()
                    + "~returnDate=~airCo=~seatType=" + searchPointBeans.get(0).getSeatType();
        }*/

        String urlstr = getUrlbytype("1") + "?";
        if (null != searchPointBeans.get(0).getReturnTime()) {
            urlstr += "type=1&fromCity=" + searchPointBeans.get(0).getStartCity() + "&toCity="
                    + searchPointBeans.get(0).getEndCity() + "&fromDate=" + searchPointBeans.get(0).getStartTime()
                    + "&returnDate=" + searchPointBeans.get(0).getReturnTime() + "&airCo="
                    + searchPointBeans.get(0).getAirCompany() + "&seatType=" + searchPointBeans.get(0).getSeatType();
        }
        else {
            urlstr += "type=1&fromCity=" + searchPointBeans.get(0).getStartCity() + "&toCity="
                    + searchPointBeans.get(0).getEndCity() + "&fromDate=" + searchPointBeans.get(0).getStartTime()
                    + "&returnDate=&airCo=&seatType=" + searchPointBeans.get(0).getSeatType();
        }

        // System.out.println("查询国际航班地址：" + urlstr);
        AllRouteBean allRoute = getInterFlight(urlstr);
        return allRoute;
    }

    private String getUrlbytype(String type) {
        String url = "-1";
        if ("1".equals(type)) {
            url = "http://www.piao59.com/api/formal/tdts.php";
        }
        if ("2".equals(type)) {
            url = "http://www.piao59.com/api/formal/gj_policy.php";
        }
        if ("3".equals(type)) {
            url = "http://www.piao59.com/api/formal/p_limit.php";
        }
        return url;
    }

    /**
     * 作者：邹远超 日期：2014-4-15 说明：查询政策
     * 
     * @param searchFligehtBean
     * @return 02
     */
    public List<Zrate> SeachZrate() {
        List<Zrate> zrates = new ArrayList<Zrate>();
        String urlstr = "http://ws.hangtian123.com/htslinterpiao?type=2";
        try {
            // System.out.println(urlstr);
            zrates = getZrate(urlstr);
        }
        catch (Exception e) {
            logger.error(e);
        }
        if (zrates == null || zrates.size() == 0) {
            zrates.add(AirUtil.getbaseZrate());
        }
        return zrates;
    }

    /**
     * 作者：邹远超 日期：2014-4-17 说明：退改签规定接口
     * 
     * @param str
     * @return 03
     */
    public List<Map<String, String>> getrefundAndapplychange(String str) {
        String urlstr = "http://ws.hangtian123.com/htslinterpiao?";
        try {
            urlstr += "type=3&paramContent=p=" + str;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return refundAndapplychange(urlstr);
    }

    /**
     * 作者：邹远超 日期：2014-4-16 说明：验证舱位接口 04
     * 
     * @return
     */
    public List<AirSegementBean> getcheckspace(List<AirSegementBean> airSegementBeans, RouteBean route) {
        String urlstr = "http://www.piao59.com/api/test/yn_seat.php?";
        for (AirSegementBean sg : airSegementBeans) {
            urlstr += sg.getAfromCity() + sg.getAtoCity() + sg.getAfromDate() + "," + sg.getAseatType() + ","
                    + sg.getAflightNumber() + "/" + sg.getAairCo();
            if (airSegementBeans.size() > 1) {
                urlstr += ";";
            }
        }
        if (airSegementBeans.size() > 1) {
            urlstr = urlstr.substring(0, urlstr.length() - 1);
        }
        urlstr += ":" + route.getRlimitterm();
        return checkspace(urlstr);
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：订单状态查询
     * 
     * @param forderinfo
     * @return 08
     */
    public Forderinfo getOrderDetail(Forderinfo forderinfo) {
        String url = "http://www.piao59.com/api/test/tdts_detail_order.php?detail=J20131104100039"
                + forderinfo.getContactmark();
        return selectOrderDetail(url);
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：订单状态查询
     * 
     * @param forderinfo
     * @return 09
     */
    public String getforderinfostate(Forderinfo forderinfo) {
        String url = "http://www.piao59.com/api/test/tdts_order_select.php?select=" + forderinfo.getContactmark();
        return selectOrderStatus(url);
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：订单状态查询
     * 
     * @param forderinfo
     * @return 10
     */
    public String cancelOrderbyno(Forderinfo forderinfo) {
        String url = "http://www.piao59.com/api/test/tdts_cancel_order.php?cancel=" + forderinfo.getContactmark();
        return cancelOrder(url);
    }

    /**
     * 通过url查询国际航班01
     * url返回的是json数据
     * @param urlstr
     * @return
     * @time 2014-4-10
     * @author 邹远超
     */
    public static AllRouteBean getInterFlight(String urlstr) {
        AllRouteBean allRoute = new AllRouteBean();
        List<RouteBean> routes = new ArrayList<RouteBean>();
        try {
            WriteLog.write("piao59_INTER", urlstr);
            String resultStr = SendPostandGet.submitPost(urlstr, "").toString();
            //            String resultStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><RS><R><F>BJS</F><T>PAR</T><A>VN</A><M>2470</M><PM>2600</PM><X>1477</X><XNQ>1477</XNQ><Q>0</Q><CM>1860</CM><CPM>1950</CPM><C1>5</C1><C2>5</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201402181558251874_23973683_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>1</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>HAN</T><A>VN</A><No>VN513</No><FA>PEK</FA><TA>HAN</TA><FD>2014-04-15</FD><FT>1545</FT><TD>2014-04-15</TD><TT>1820</TT><ST>L</ST><M>2600</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>VN513</No><FA>PEK</FA><TA>HAN</TA><A>VN</A><ET>321</ET><FD>2014-04-15</FD><FT>1545</FT><TD>2014-04-15</TD><TT>1820</TT><D>3:35</D><ST>1</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>02:05</STIME><OF></OF></F></FS></S><S><F>HAN</F><T>PAR</T><A>VN</A><No>VN017</No><FA>HAN</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2320</FT><TD>2014-04-16</TD><TT>0700</TT><ST>L</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>VN017</No><FA>HAN</FA><TA>CDG</TA><A>VN</A><ET>777</ET><FD>2014-04-15</FD><FT>2320</FT><TD>2014-04-16</TD><TT>0700</TT><D>13:40</D><ST>4</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>SU</A><M>2527</M><PM>2660</PM><X>1315</X><XNQ>1315</XNQ><Q>0</Q><CM>1900</CM><CPM>2000</CPM><C1>5</C1><C2>5</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106071552355467_23833396_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>SU201,SU2452;SU201,SU3000;SU201,SU2454;SU201,SU2462;SU201,SU2458;SU205,SU261</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>MOW</T><A>SU</A><No>SU201</No><FA>PEK</FA><TA>SVO</TA><FD>2014-04-15</FD><FT>0230</FT><TD>2014-04-15</TD><TT>0640</TT><ST>T</ST><M>2660</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>SU201</No><FA>PEK</FA><TA>SVO</TA><A>SU</A><ET>333</ET><FD>2014-04-15</FD><FT>0230</FT><TD>2014-04-15</TD><TT>0640</TT><D>9:10</D><ST>7</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU205</No><FA>PEK</FA><TA>SVO</TA><A>SU</A><ET>77W</ET><FD>2014-04-15</FD><FT>1140</FT><TD>2014-04-15</TD><TT>1530</TT><D>8:50</D><ST>7</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>F</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>MOW</F><T>PAR</T><A>SU</A><No>SU2452</No><FA>SVO</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0850</FT><TD>2014-04-15</TD><TT>1045</TT><ST>T</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>SU2452</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-15</FD><FT>0850</FT><TD>2014-04-15</TD><TT>1045</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU3000</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>321</ET><FD>2014-04-15</FD><FT>0945</FT><TD>2014-04-15</TD><TT>1140</TT><D>3:55</D><ST>7</ST><V>1</V><T>0</T><IFT>E</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF>AF1145</OF></F><F><No>SU2454</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-15</FD><FT>1110</FT><TD>2014-04-15</TD><TT>1310</TT><D>4:0</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU2462</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>321</ET><FD>2014-04-15</FD><FT>1240</FT><TD>2014-04-15</TD><TT>1435</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU2458</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-15</FD><FT>1505</FT><TD>2014-04-15</TD><TT>1700</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU261</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-15</FD><FT>1855</FT><TD>2014-04-15</TD><TT>2055</TT><D>4:0</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>01:30</STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>EY</A><M>3328</M><PM>3430</PM><X>1093</X><XNQ>1093</XNQ><Q>0</Q><CM>2500</CM><CPM>2580</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106081319013129_23573578_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>EY889,EY037</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>AUH</T><A>EY</A><No>EY889</No><FA>PEK</FA><TA>AUH</TA><FD>2014-04-15</FD><FT>0130</FT><TD>2014-04-15</TD><TT>0620</TT><ST>V</ST><M>3430</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>EY889</No><FA>PEK</FA><TA>AUH</TA><A>EY</A><ET>332</ET><FD>2014-04-15</FD><FT>0130</FT><TD>2014-04-15</TD><TT>0620</TT><D>8:50</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>03:40</STIME><OF></OF></F></FS></S><S><F>AUH</F><T>PAR</T><A>EY</A><No>EY037</No><FA>AUH</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0900</FT><TD>2014-04-15</TD><TT>1425</TT><ST>V</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>EY037</No><FA>AUH</FA><TA>CDG</TA><A>EY</A><ET>346</ET><FD>2014-04-15</FD><FT>0900</FT><TD>2014-04-15</TD><TT>1425</TT><D>8:25</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KA</A><M>3450</M><PM>3450</PM><X>984</X><XNQ>954</XNQ><Q>30</Q><CM>2590</CM><CPM>2590</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171123302067_18671925_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>KA997,CX261;KA937,CX261;KA909,CX261;KA993,CX261</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>KA</A><No>KA997</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><ST>V</ST><M>3450</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KA997</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>320</ET><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><D>3:35</D><ST>6</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA937</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0740</FT><TD>2014-04-15</TD><TT>1115</TT><D>3:35</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA909</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>2010</TT><D>3:40</D><ST>3</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA993</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1830</FT><TD>2014-04-15</TD><TT>2205</TT><D>3:35</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>V</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>6</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>QR</A><M>3473</M><PM>3580</PM><X>1368</X><XNQ>1058</XNQ><Q>310</Q><CM>2610</CM><CPM>2690</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201105250940342187_23562735_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>QR895,QR037;QR895,QR041</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>DOH</T><A>QR</A><No>QR895</No><FA>PEK</FA><TA>DOH</TA><FD>2014-04-15</FD><FT>0155</FT><TD>2014-04-15</TD><TT>0550</TT><ST>N</ST><M>3580</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>QR895</No><FA>PEK</FA><TA>DOH</TA><A>QR</A><ET>77W</ET><FD>2014-04-15</FD><FT>0155</FT><TD>2014-04-15</TD><TT>0550</TT><D>8:55</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>DOH</F><T>PAR</T><A>QR</A><No>QR037</No><FA>DOH</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1415</FT><TD>2014-04-15</TD><TT>2015</TT><ST>N</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>QR037</No><FA>DOH</FA><TA>CDG</TA><A>QR</A><ET>346</ET><FD>2014-04-15</FD><FT>1415</FT><TD>2014-04-15</TD><TT>2015</TT><D>8:0</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>QR041</No><FA>DOH</FA><TA>CDG</TA><A>QR</A><ET>346</ET><FD>2014-04-16</FD><FT>0125</FT><TD>2014-04-16</TD><TT>0725</TT><D>8:0</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>MS</A><M>3563</M><PM>3750</PM><X>1093</X><XNQ></XNQ><Q></Q><CM>2680</CM><CPM>2820</CPM><C1>5</C1><C2>5</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201107071400187964_23594955_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>MS956,MS799</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>CAI</T><A>MS</A><No>MS956</No><FA>PEK</FA><TA>CAI</TA><FD>2014-04-15</FD><FT>0030</FT><TD>2014-04-15</TD><TT>0520</TT><ST>K</ST><M>3750</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>MS956</No><FA>PEK</FA><TA>CAI</TA><A>MS</A><ET>330</ET><FD>2014-04-15</FD><FT>0030</FT><TD>2014-04-15</TD><TT>0520</TT><D>10:50</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>3</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CAI</F><T>PAR</T><A>MS</A><No>MS799</No><FA>CAI</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0935</FT><TD>2014-04-15</TD><TT>1425</TT><ST>K</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>MS799</No><FA>CAI</FA><TA>CDG</TA><A>MS</A><ET>333</ET><FD>2014-04-15</FD><FT>0935</FT><TD>2014-04-15</TD><TT>1425</TT><D>5:50</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>TK</A><M>3594</M><PM>3630</PM><X>1596</X><XNQ>1596</XNQ><Q>0</Q><CM>2700</CM><CPM>2730</CPM><C1>1</C1><C2>1</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106080952194848_23645821_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>TK021,TK1821</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>IST</T><A>TK</A><No>TK021</No><FA>PEK</FA><TA>IST</TA><FD>2014-04-15</FD><FT>0010</FT><TD>2014-04-15</TD><TT>0525</TT><ST>S</ST><M>3630</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>TK021</No><FA>PEK</FA><TA>IST</TA><A>TK</A><ET>77C</ET><FD>2014-04-15</FD><FT>0010</FT><TD>2014-04-15</TD><TT>0525</TT><D>11:15</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>I</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>IST</F><T>PAR</T><A>TK</A><No>TK1821</No><FA>IST</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0735</FT><TD>2014-04-15</TD><TT>1015</TT><ST>S</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>TK1821</No><FA>IST</FA><TA>CDG</TA><A>TK</A><ET>333</ET><FD>2014-04-15</FD><FT>0735</FT><TD>2014-04-15</TD><TT>1015</TT><D>3:40</D><ST>9</ST><V>0</V><T>0</T><IFT>I</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>MH</A><M>3677</M><PM>3830</PM><X>209</X><XNQ>209</XNQ><Q>0</Q><CM>3677</CM><CPM>3830</CPM><C1>4</C1><C2>4</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201311071406454721_23927752_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>KUL</T><A>MH</A><No>MH361</No><FA>PEK</FA><TA>KUL</TA><FD>2014-04-15</FD><FT>0130</FT><TD>2014-04-15</TD><TT>0740</TT><ST>O</ST><M>3830</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>MH361</No><FA>PEK</FA><TA>KUL</TA><A>MH</A><ET>333</ET><FD>2014-04-15</FD><FT>0130</FT><TD>2014-04-15</TD><TT>0740</TT><D>6:10</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>M</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MH319</No><FA>PEK</FA><TA>KUL</TA><A>MH</A><ET>772</ET><FD>2014-04-15</FD><FT>0900</FT><TD>2014-04-15</TD><TT>1520</TT><D>6:20</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>KUL</F><T>PAR</T><A>MH</A><No>MH020</No><FA>KUL</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2340</FT><TD>2014-04-16</TD><TT>0640</TT><ST>O</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>MH020</No><FA>KUL</FA><TA>CDG</TA><A>MH</A><ET>380</ET><FD>2014-04-15</FD><FT>2340</FT><TD>2014-04-16</TD><TT>0640</TT><D>14:0</D><ST>9</ST><V>0</V><T>0</T><IFT>M</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>QR</A><M>3783</M><PM>3900</PM><X>1368</X><XNQ>1058</XNQ><Q>310</Q><CM>2850</CM><CPM>2930</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201105250940342187_23562728_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>QR895,QR039;QR895,QR037;QR895,QR041</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>DOH</T><A>QR</A><No>QR895</No><FA>PEK</FA><TA>DOH</TA><FD>2014-04-15</FD><FT>0155</FT><TD>2014-04-15</TD><TT>0550</TT><ST>S</ST><M>3900</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>QR895</No><FA>PEK</FA><TA>DOH</TA><A>QR</A><ET>77W</ET><FD>2014-04-15</FD><FT>0155</FT><TD>2014-04-15</TD><TT>0550</TT><D>8:55</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>DOH</F><T>PAR</T><A>QR</A><No>QR039</No><FA>DOH</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0810</FT><TD>2014-04-15</TD><TT>1410</TT><ST>S</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>QR039</No><FA>DOH</FA><TA>CDG</TA><A>QR</A><ET>346</ET><FD>2014-04-15</FD><FT>0810</FT><TD>2014-04-15</TD><TT>1410</TT><D>8:0</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>QR037</No><FA>DOH</FA><TA>CDG</TA><A>QR</A><ET>346</ET><FD>2014-04-15</FD><FT>1415</FT><TD>2014-04-15</TD><TT>2015</TT><D>8:0</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>QR041</No><FA>DOH</FA><TA>CDG</TA><A>QR</A><ET>346</ET><FD>2014-04-16</FD><FT>0125</FT><TD>2014-04-16</TD><TT>0725</TT><D>8:0</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>UL</A><M>3804</M><PM>4090</PM><X>712</X><XNQ></XNQ><Q></Q><CM>2860</CM><CPM>3070</CPM><C1>7</C1><C2>1</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201304171038091422_23613823_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>UL869,UL563</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>CMB</T><A>UL</A><No>UL869</No><FA>PEK</FA><TA>CMB</TA><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0550</TT><ST>K</ST><M>4090</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>UL869</No><FA>PEK</FA><TA>CMB</TA><A>UL</A><ET>332</ET><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0550</TT><D>7:30</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CMB</F><T>PAR</T><A>UL</A><No>UL563</No><FA>CMB</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0130</FT><TD>2014-04-16</TD><TT>0900</TT><ST>K</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>UL563</No><FA>CMB</FA><TA>CDG</TA><A>UL</A><ET>332</ET><FD>2014-04-16</FD><FT>0130</FT><TD>2014-04-16</TD><TT>0900</TT><D>12:0</D><ST>4</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>MU</A><M>3977</M><PM>4100</PM><X>1435</X><XNQ>1435</XNQ><Q>0</Q><CM>2990</CM><CPM>3080</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201204281653245622_23405026_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SHA</T><A>MU</A><No>MU5138</No><FA>PEK</FA><TA>SHA</TA><FD>2014-04-15</FD><FT>0700</FT><TD>2014-04-15</TD><TT>0910</TT><ST>Y</ST><M>4100</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>MU5138</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>0700</FT><TD>2014-04-15</TD><TT>0910</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU9108</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>738</ET><FD>2014-04-15</FD><FT>0730</FT><TD>2014-04-15</TD><TT>0940</TT><D>2:10</D><ST>9</ST><V>1</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF>FM9108</OF></F><F><No>MU5183</No><FA>PEK</FA><TA>PVG</TA><A>MU</A><ET>321</ET><FD>2014-04-15</FD><FT>0735</FT><TD>2014-04-15</TD><TT>0950</TT><D>2:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5102</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>1010</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5104</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>0900</FT><TD>2014-04-15</TD><TT>1110</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5106</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1000</FT><TD>2014-04-15</TD><TT>1210</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5108</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1100</FT><TD>2014-04-15</TD><TT>1310</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5146</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>76A</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1340</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5110</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1200</FT><TD>2014-04-15</TD><TT>1410</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5112</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1300</FT><TD>2014-04-15</TD><TT>1510</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5140</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>76A</ET><FD>2014-04-15</FD><FT>1335</FT><TD>2014-04-15</TD><TT>1535</TT><D>2:0</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU271</No><FA>PEK</FA><TA>PVG</TA><A>MU</A><ET>321</ET><FD>2014-04-15</FD><FT>1400</FT><TD>2014-04-15</TD><TT>1610</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>00:55</STIME><OF></OF></F><F><No>MU5114</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1400</FT><TD>2014-04-15</TD><TT>1610</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5116</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>1710</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SHA</F><T>PAR</T><A>MU</A><No>MU553</No><FA>PVG</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2355</FT><TD>2014-04-16</TD><TT>0630</TT><ST>R</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>MU553</No><FA>PVG</FA><TA>CDG</TA><A>MU</A><ET>33E</ET><FD>2014-04-15</FD><FT>2355</FT><TD>2014-04-16</TD><TT>0630</TT><D>13:35</D><ST>2</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>OZ</A><M>4010</M><PM>4010</PM><X>1162</X><XNQ>1162</XNQ><Q>0</Q><CM>3010</CM><CPM>3010</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201201041641176405_638663_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SEL</T><A>OZ</A><No>OZ3325</No><FA>PEK</FA><TA>GMP</TA><FD>2014-04-15</FD><FT>1110</FT><TD>2014-04-15</TD><TT>1410</TT><ST>E</ST><M>4010</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>OZ3325</No><FA>PEK</FA><TA>GMP</TA><A>OZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1110</FT><TD>2014-04-15</TD><TT>1410</TT><D>2:0</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>I</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>OZ334</No><FA>PEK</FA><TA>ICN</TA><A>OZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1520</FT><TD>2014-04-15</TD><TT>1820</TT><D>2:0</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>OZ336</No><FA>PEK</FA><TA>ICN</TA><A>OZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1710</FT><TD>2014-04-15</TD><TT>2010</TT><D>2:0</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SEL</F><T>PAR</T><A>OZ</A><No>OZ501</No><FA>ICN</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>1230</FT><TD>2014-04-16</TD><TT>1750</TT><ST>E</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>OZ501</No><FA>ICN</FA><TA>CDG</TA><A>OZ</A><ET>772</ET><FD>2014-04-16</FD><FT>1230</FT><TD>2014-04-16</TD><TT>1750</TT><D>13:20</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KA</A><M>4050</M><PM>4050</PM><X>984</X><XNQ>954</XNQ><Q>30</Q><CM>3040</CM><CPM>3040</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171123302067_18671926_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>KA997,CX261;KA937,CX261;KA937,CX279;KA905,CX261;KA909,CX261;KA909,CX279;KA993,CX261;KA993,CX279</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>KA</A><No>KA997</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><ST>L</ST><M>4050</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KA997</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>320</ET><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA937</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0740</FT><TD>2014-04-15</TD><TT>1115</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA905</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>1140</TT><D>3:40</D><ST>2</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA909</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>2010</TT><D>3:40</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA993</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1830</FT><TD>2014-04-15</TD><TT>2205</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>L</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CX279</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>343</ET><FD>2014-04-16</FD><FT>0040</FT><TD>2014-04-16</TD><TT>0755</TT><D>14:15</D><ST>2</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>BA</A><M>4130</M><PM>4130</PM><X>1803</X><XNQ>1803</XNQ><Q>0</Q><CM>3100</CM><CPM>3100</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201207041520374697_23932010_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>BA038,BA336;BA038,BA322;BA038,BA338;BA038,BA326;BA038,BA332</MS><G>0</G><PT>ADT</PT><V>1</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>LON</T><A>BA</A><No>BA038</No><FA>PEK</FA><TA>LHR</TA><FD>2014-04-15</FD><FT>1115</FT><TD>2014-04-15</TD><TT>1510</TT><ST>V</ST><M>4130</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>BA038</No><FA>PEK</FA><TA>LHR</TA><A>BA</A><ET>744</ET><FD>2014-04-15</FD><FT>1115</FT><TD>2014-04-15</TD><TT>1510</TT><D>11:55</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>5</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>LON</F><T>PAR</T><A>BA</A><No>BA336</No><FA>LHR</FA><TA>ORY</TA><FD>2014-04-15</FD><FT>1640</FT><TD>2014-04-15</TD><TT>1855</TT><ST>V</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>BA336</No><FA>LHR</FA><TA>ORY</TA><A>BA</A><ET>319</ET><FD>2014-04-15</FD><FT>1640</FT><TD>2014-04-15</TD><TT>1855</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>5</IFT><IDT>W</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>BA322</No><FA>LHR</FA><TA>CDG</TA><A>BA</A><ET>320</ET><FD>2014-04-15</FD><FT>1745</FT><TD>2014-04-15</TD><TT>2000</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>5</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>BA338</No><FA>LHR</FA><TA>ORY</TA><A>BA</A><ET>319</ET><FD>2014-04-15</FD><FT>1925</FT><TD>2014-04-15</TD><TT>2135</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>5</IFT><IDT>W</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>BA326</No><FA>LHR</FA><TA>CDG</TA><A>BA</A><ET>319</ET><FD>2014-04-15</FD><FT>2040</FT><TD>2014-04-15</TD><TT>2250</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>5</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>BA332</No><FA>LHR</FA><TA>ORY</TA><A>BA</A><ET>319</ET><FD>2014-04-16</FD><FT>0650</FT><TD>2014-04-16</TD><TT>0905</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>5</IFT><IDT>W</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KE</A><M>4249</M><PM>4380</PM><X>1269</X><XNQ></XNQ><Q></Q><CM>3190</CM><CPM>3290</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201201161015536561_23910199_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SEL</T><A>KE</A><No>KE5804</No><FA>PEK</FA><TA>ICN</TA><FD>2014-04-15</FD><FT>1910</FT><TD>2014-04-15</TD><TT>2210</TT><ST>E</ST><M>4380</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KE5804</No><FA>PEK</FA><TA>ICN</TA><A>KE</A><ET>321</ET><FD>2014-04-15</FD><FT>1910</FT><TD>2014-04-15</TD><TT>2210</TT><D>2:0</D><ST>9</ST><V>1</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF>CZ0315</OF></F><F><No>KE854</No><FA>PEK</FA><TA>ICN</TA><A>KE</A><ET>772</ET><FD>2014-04-15</FD><FT>2115</FT><TD>2014-04-16</TD><TT>0020</TT><D>2:5</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SEL</F><T>PAR</T><A>KE</A><No>KE5901</No><FA>ICN</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0930</FT><TD>2014-04-16</TD><TT>1425</TT><ST>E</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>KE5901</No><FA>ICN</FA><TA>CDG</TA><A>KE</A><ET>772</ET><FD>2014-04-16</FD><FT>0930</FT><TD>2014-04-16</TD><TT>1425</TT><D>12:55</D><ST>9</ST><V>1</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF>AF0267</OF></F><F><No>KE901</No><FA>ICN</FA><TA>CDG</TA><A>KE</A><ET>388</ET><FD>2014-04-16</FD><FT>1305</FT><TD>2014-04-16</TD><TT>1820</TT><D>13:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KA</A><M>4750</M><PM>4750</PM><X>984</X><XNQ>954</XNQ><Q>30</Q><CM>3570</CM><CPM>3570</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171123302067_18671927_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>KA997,CX261;KA937,CX261;KA937,CX279;KA905,CX261;KA905,CX279;KA901,CX261;KA901,CX279;KA909,CX261KA909,CX279;KA993,CX261;KA993,CX279</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>KA</A><No>KA997</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><ST>M</ST><M>4750</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KA997</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>320</ET><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA937</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0740</FT><TD>2014-04-15</TD><TT>1115</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA905</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>1140</TT><D>3:40</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA901</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1610</TT><D>3:40</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA909</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>2010</TT><D>3:40</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA993</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1830</FT><TD>2014-04-15</TD><TT>2205</TT><D>3:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>M</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CX279</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>343</ET><FD>2014-04-16</FD><FT>0040</FT><TD>2014-04-16</TD><TT>0755</TT><D>14:15</D><ST>6</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CX</A><M>4750</M><PM>4750</PM><X>1000</X><XNQ></XNQ><Q></Q><CM>3570</CM><CPM>3570</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171353093473_18672135_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>CX391,CX261</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>CX</A><No>CX391</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1715</TT><ST>M</ST><M>4750</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CX391</No><FA>PEK</FA><TA>HKG</TA><A>CX</A><ET>333</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1715</TT><D>3:45</D><ST>8</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>M</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>8</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>LX</A><M>4810</M><PM>4810</PM><X>1416</X><XNQ></XNQ><Q></Q><CM>3610</CM><CPM>3610</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201108011003100460_23606414_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>LX197,LX638;LX197,LX656</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>ZRH</T><A>LX</A><No>LX197</No><FA>PEK</FA><TA>ZRH</TA><FD>2014-04-15</FD><FT>0645</FT><TD>2014-04-15</TD><TT>1120</TT><ST>W</ST><M>4810</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>LX197</No><FA>PEK</FA><TA>ZRH</TA><A>LX</A><ET>333</ET><FD>2014-04-15</FD><FT>0645</FT><TD>2014-04-15</TD><TT>1120</TT><D>11:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>ZRH</F><T>PAR</T><A>LX</A><No>LX638</No><FA>ZRH</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1350</TT><ST>W</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>LX638</No><FA>ZRH</FA><TA>CDG</TA><A>LX</A><ET>AR1</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1350</TT><D>1:20</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LX656</No><FA>ZRH</FA><TA>CDG</TA><A>LX</A><ET>320</ET><FD>2014-04-15</FD><FT>1650</FT><TD>2014-04-15</TD><TT>1805</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>AY</A><M>5079</M><PM>5130</PM><X>1675</X><XNQ></XNQ><Q></Q><CM>3810</CM><CPM>3850</CPM><C1>1</C1><C2>1</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106141420110787_23915067_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>AY052,AY873;AY052,AY2877;AY052,AY871</MS><G>0</G><PT>ADT</PT><V>0</V><CL>3</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>HEL</T><A>AY</A><No>AY052</No><FA>PEK</FA><TA>HEL</TA><FD>2014-04-15</FD><FT>1055</FT><TD>2014-04-15</TD><TT>1425</TT><ST>T</ST><M>5130</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>AY052</No><FA>PEK</FA><TA>HEL</TA><A>AY</A><ET>333</ET><FD>2014-04-15</FD><FT>1055</FT><TD>2014-04-15</TD><TT>1425</TT><D>9:30</D><ST>7</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HEL</F><T>PAR</T><A>AY</A><No>AY873</No><FA>HEL</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1605</FT><TD>2014-04-15</TD><TT>1810</TT><ST>T</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>AY873</No><FA>HEL</FA><TA>CDG</TA><A>AY</A><ET>32B</ET><FD>2014-04-15</FD><FT>1605</FT><TD>2014-04-15</TD><TT>1810</TT><D>3:5</D><ST>7</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2D</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AY2877</No><FA>HEL</FA><TA>CDG</TA><A>AY</A><ET>E90</ET><FD>2014-04-15</FD><FT>1930</FT><TD>2014-04-15</TD><TT>2135</TT><D>3:5</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AY871</No><FA>HEL</FA><TA>CDG</TA><A>AY</A><ET>321</ET><FD>2014-04-16</FD><FT>0735</FT><TD>2014-04-16</TD><TT>0940</TT><D>3:5</D><ST>7</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2D</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>MU</A><M>5171</M><PM>5330</PM><X>0</X><XNQ></XNQ><Q></Q><CM>3890</CM><CPM>4000</CPM><C1>3</C1><C2>3</C2><Z>2</Z><ZZ>2</ZZ><N>1</N><L>3_I201306131455259407_20549178_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SHA</T><A>MU</A><No>MU5183</No><FA>PEK</FA><TA>PVG</TA><FD>2014-04-15</FD><FT>0735</FT><TD>2014-04-15</TD><TT>0950</TT><ST>Y</ST><M>0</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>MU5183</No><FA>PEK</FA><TA>PVG</TA><A>MU</A><ET>321</ET><FD>2014-04-15</FD><FT>0735</FT><TD>2014-04-15</TD><TT>0950</TT><D>2:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SHA</F><T>ROM</T><A>MU</A><No>MU787</No><FA>PVG</FA><TA>FCO</TA><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1910</TT><ST>L</ST><M>5330</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>MU787</No><FA>PVG</FA><TA>FCO</TA><A>MU</A><ET>33E</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1910</TT><D>13:40</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>3</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>ROM</F><T>PAR</T><A>AZ</A><No>AZ316</No><FA>FCO</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0755</FT><TD>2014-04-16</TD><TT>1005</TT><ST>Q</ST><M>0</M><FP>3</FP><DP>1</DP><K>0</K><FS><F><No>AZ316</No><FA>FCO</FA><TA>CDG</TA><A>AZ</A><ET>321</ET><FD>2014-04-16</FD><FT>0755</FT><TD>2014-04-16</TD><TT>1005</TT><D>2:10</D><ST>7</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AZ318</No><FA>FCO</FA><TA>CDG</TA><A>AZ</A><ET>321</ET><FD>2014-04-16</FD><FT>0915</FT><TD>2014-04-16</TD><TT>1125</TT><D>2:10</D><ST>7</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AZ324</No><FA>FCO</FA><TA>CDG</TA><A>AZ</A><ET>321</ET><FD>2014-04-16</FD><FT>1440</FT><TD>2014-04-16</TD><TT>1650</TT><D>2:10</D><ST>7</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AZ330</No><FA>FCO</FA><TA>CDG</TA><A>AZ</A><ET>32S</ET><FD>2014-04-16</FD><FT>1525</FT><TD>2014-04-16</TD><TT>1735</TT><D>2:10</D><ST>7</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AZ326</No><FA>FCO</FA><TA>CDG</TA><A>AZ</A><ET>32S</ET><FD>2014-04-16</FD><FT>1705</FT><TD>2014-04-16</TD><TT>1915</TT><D>2:10</D><ST>7</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>MU</A><M>5171</M><PM>5330</PM><X>0</X><XNQ></XNQ><Q></Q><CM>3890</CM><CPM>4000</CPM><C1>3</C1><C2>3</C2><Z>2</Z><ZZ>2</ZZ><N>1</N><L>3_I201306131455259407_20548168_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SHA</T><A>MU</A><No>MU5106</No><FA>PEK</FA><TA>SHA</TA><FD>2014-04-15</FD><FT>1000</FT><TD>2014-04-15</TD><TT>1210</TT><ST>Y</ST><M>0</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>MU5106</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1000</FT><TD>2014-04-15</TD><TT>1210</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5108</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1100</FT><TD>2014-04-15</TD><TT>1310</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5146</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>76A</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1340</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5110</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1200</FT><TD>2014-04-15</TD><TT>1410</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5112</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1300</FT><TD>2014-04-15</TD><TT>1510</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5140</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>76A</ET><FD>2014-04-15</FD><FT>1335</FT><TD>2014-04-15</TD><TT>1535</TT><D>2:0</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU271</No><FA>PEK</FA><TA>PVG</TA><A>MU</A><ET>321</ET><FD>2014-04-15</FD><FT>1400</FT><TD>2014-04-15</TD><TT>1610</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>00:55</STIME><OF></OF></F><F><No>MU5114</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1400</FT><TD>2014-04-15</TD><TT>1610</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>MU5116</No><FA>PEK</FA><TA>SHA</TA><A>MU</A><ET>333</ET><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>1710</TT><D>2:10</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SHA</F><T>MOW</T><A>MU</A><No>MU591</No><FA>PVG</FA><TA>SVO</TA><FD>2014-04-16</FD><FT>1155</FT><TD>2014-04-16</TD><TT>1800</TT><ST>L</ST><M>5330</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>MU591</No><FA>PVG</FA><TA>SVO</TA><A>MU</A><ET>76E</ET><FD>2014-04-16</FD><FT>1155</FT><TD>2014-04-16</TD><TT>1800</TT><D>11:5</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>F</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>MOW</F><T>PAR</T><A>SU</A><No>SU2452</No><FA>SVO</FA><TA>CDG</TA><FD>2014-04-17</FD><FT>0850</FT><TD>2014-04-17</TD><TT>1045</TT><ST>T</ST><M>0</M><FP>3</FP><DP>1</DP><K>0</K><FS><F><No>SU2452</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-17</FD><FT>0850</FT><TD>2014-04-17</TD><TT>1045</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU2454</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-17</FD><FT>1110</FT><TD>2014-04-17</TD><TT>1310</TT><D>4:0</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU2462</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>321</ET><FD>2014-04-17</FD><FT>1240</FT><TD>2014-04-17</TD><TT>1435</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SU2458</No><FA>SVO</FA><TA>CDG</TA><A>SU</A><ET>320</ET><FD>2014-04-17</FD><FT>1505</FT><TD>2014-04-17</TD><TT>1700</TT><D>3:55</D><ST>7</ST><V>0</V><T>0</T><IFT>D</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CA</A><M>5297</M><PM>5350</PM><X>1315</X><XNQ>1315</XNQ><Q>0</Q><CM>3980</CM><CPM>4020</CPM><C1>1</C1><C2>1</C2><Z>0</Z><ZZ>0</ZZ><N>0</N><L>3_I201104261127068755_23653693</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>PAR</T><A>CA</A><No>CA933</No><FA>PEK</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1335</FT><TD>2014-04-15</TD><TT>1840</TT><ST>Q</ST><M>5350</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CA933</No><FA>PEK</FA><TA>CDG</TA><A>CA</A><ET>773</ET><FD>2014-04-15</FD><FT>1335</FT><TD>2014-04-15</TD><TT>1840</TT><D>12:5</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CA</A><M>5297</M><PM>5350</PM><X>1842</X><XNQ></XNQ><Q></Q><CM>3980</CM><CPM>4020</CPM><C1>1</C1><C2>1</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201302220957233975_23866064_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>MUC</T><A>CA</A><No>CA961</No><FA>PEK</FA><TA>MUC</TA><FD>2014-04-15</FD><FT>0110</FT><TD>2014-04-15</TD><TT>0530</TT><ST>Q</ST><M>5350</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CA961</No><FA>PEK</FA><TA>MUC</TA><A>CA</A><ET>330</ET><FD>2014-04-15</FD><FT>0110</FT><TD>2014-04-15</TD><TT>0530</TT><D>11:20</D><ST>3</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2</IDT><IST></IST><STIME>01:50</STIME><OF></OF></F></FS></S><S><F>MUC</F><T>PAR</T><A>LH</A><No>LH2230</No><FA>MUC</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1225</FT><TD>2014-04-15</TD><TT>1400</TT><ST>W</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>LH2230</No><FA>MUC</FA><TA>CDG</TA><A>LH</A><ET>320</ET><FD>2014-04-15</FD><FT>1225</FT><TD>2014-04-15</TD><TT>1400</TT><D>1:35</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CA</A><M>5297</M><PM>5350</PM><X>1917</X><XNQ>1917</XNQ><Q>0</Q><CM>3980</CM><CPM>4020</CPM><C1>1</C1><C2>1</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201302220957233975_23866067_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>FRA</T><A>CA</A><No>CA965</No><FA>PEK</FA><TA>FRA</TA><FD>2014-04-15</FD><FT>0205</FT><TD>2014-04-15</TD><TT>0625</TT><ST>Q</ST><M>5350</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CA965</No><FA>PEK</FA><TA>FRA</TA><A>CA</A><ET>773</ET><FD>2014-04-15</FD><FT>0205</FT><TD>2014-04-15</TD><TT>0625</TT><D>11:20</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>04:00</STIME><OF></OF></F><F><No>CA931</No><FA>PEK</FA><TA>FRA</TA><A>CA</A><ET>773</ET><FD>2014-04-15</FD><FT>1400</FT><TD>2014-04-15</TD><TT>1815</TT><D>11:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>03:05</STIME><OF></OF></F></FS></S><S><F>FRA</F><T>PAR</T><A>LH</A><No>LH1028</No><FA>FRA</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0845</FT><TD>2014-04-15</TD><TT>0955</TT><ST>W</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>LH1028</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>321</ET><FD>2014-04-15</FD><FT>0845</FT><TD>2014-04-15</TD><TT>0955</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1030</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>320</ET><FD>2014-04-15</FD><FT>0930</FT><TD>2014-04-15</TD><TT>1040</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1034</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>321</ET><FD>2014-04-15</FD><FT>1205</FT><TD>2014-04-15</TD><TT>1315</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1040</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>32A</ET><FD>2014-04-15</FD><FT>1600</FT><TD>2014-04-15</TD><TT>1710</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1042</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>321</ET><FD>2014-04-15</FD><FT>1650</FT><TD>2014-04-15</TD><TT>1800</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1046</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>733</ET><FD>2014-04-15</FD><FT>1740</FT><TD>2014-04-15</TD><TT>1850</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1050</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>733</ET><FD>2014-04-15</FD><FT>2045</FT><TD>2014-04-15</TD><TT>2155</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1052</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>320</ET><FD>2014-04-15</FD><FT>2135</FT><TD>2014-04-15</TD><TT>2245</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>SQ</A><M>5700</M><PM>5700</PM><X>1810</X><XNQ></XNQ><Q></Q><CM>4280</CM><CPM>4280</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201304271130231719_10212209_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>SQ805,SQ334;SQ807,SQ334</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>SIN</T><A>SQ</A><No>SQ805</No><FA>PEK</FA><TA>SIN</TA><FD>2014-04-15</FD><FT>0855</FT><TD>2014-04-15</TD><TT>1515</TT><ST>E</ST><M>5700</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>SQ805</No><FA>PEK</FA><TA>SIN</TA><A>SQ</A><ET>772</ET><FD>2014-04-15</FD><FT>0855</FT><TD>2014-04-15</TD><TT>1515</TT><D>6:20</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>SQ807</No><FA>PEK</FA><TA>SIN</TA><A>SQ</A><ET>77W</ET><FD>2014-04-15</FD><FT>1635</FT><TD>2014-04-15</TD><TT>2255</TT><D>6:20</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>SIN</F><T>PAR</T><A>SQ</A><No>SQ334</No><FA>SIN</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2355</FT><TD>2014-04-16</TD><TT>0720</TT><ST>E</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>SQ334</No><FA>SIN</FA><TA>CDG</TA><A>SQ</A><ET>388</ET><FD>2014-04-15</FD><FT>2355</FT><TD>2014-04-16</TD><TT>0720</TT><D>14:25</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CX</A><M>5750</M><PM>5750</PM><X>1000</X><XNQ></XNQ><Q></Q><CM>4320</CM><CPM>4320</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171353093473_18672136_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>CX391,CX261;CX391,CX279</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>CX</A><No>CX391</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1715</TT><ST>K</ST><M>5750</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CX391</No><FA>PEK</FA><TA>HKG</TA><A>CX</A><ET>333</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1715</TT><D>3:45</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>K</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CX279</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>343</ET><FD>2014-04-16</FD><FT>0040</FT><TD>2014-04-16</TD><TT>0755</TT><D>14:15</D><ST>5</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>EK</A><M>6131</M><PM>6320</PM><X>680</X><XNQ>90</XNQ><Q>590</Q><CM>4610</CM><CPM>4740</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106071500155626_23917805_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>EK309,EK075;EK309,EK071</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>DXB</T><A>EK</A><No>EK309</No><FA>PEK</FA><TA>DXB</TA><FD>2014-04-15</FD><FT>0725</FT><TD>2014-04-15</TD><TT>1200</TT><ST>B</ST><M>6320</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>EK309</No><FA>PEK</FA><TA>DXB</TA><A>EK</A><ET>77W</ET><FD>2014-04-15</FD><FT>0725</FT><TD>2014-04-15</TD><TT>1200</TT><D>8:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>3</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>DXB</F><T>PAR</T><A>EK</A><No>EK075</No><FA>DXB</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>2005</TT><ST>B</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>EK075</No><FA>DXB</FA><TA>CDG</TA><A>EK</A><ET>388</ET><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>2005</TT><D>8:5</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2C</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>EK071</No><FA>DXB</FA><TA>CDG</TA><A>EK</A><ET>77W</ET><FD>2014-04-16</FD><FT>0410</FT><TD>2014-04-16</TD><TT>0930</TT><D>8:20</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2C</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>OS</A><M>6260</M><PM>6260</PM><X>1861</X><XNQ></XNQ><Q></Q><CM>4700</CM><CPM>4700</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201105111453155935_23944164_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>OS064,OS417;OS064,OS7117;OS064,AF1757</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>VIE</T><A>OS</A><No>OS064</No><FA>PEK</FA><TA>VIE</TA><FD>2014-04-15</FD><FT>1125</FT><TD>2014-04-15</TD><TT>1550</TT><ST>Q</ST><M>6260</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>OS064</No><FA>PEK</FA><TA>VIE</TA><A>OS</A><ET>763</ET><FD>2014-04-15</FD><FT>1125</FT><TD>2014-04-15</TD><TT>1550</TT><D>11:25</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>VIE</F><T>PAR</T><A>OS</A><No>OS417</No><FA>VIE</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1720</FT><TD>2014-04-15</TD><TT>1925</TT><ST>Q</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>OS417</No><FA>VIE</FA><TA>CDG</TA><A>OS</A><ET>321</ET><FD>2014-04-15</FD><FT>1720</FT><TD>2014-04-15</TD><TT>1925</TT><D>2:5</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2D</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>OS7117</No><FA>VIE</FA><TA>CDG</TA><A>OS</A><ET>318</ET><FD>2014-04-15</FD><FT>2010</FT><TD>2014-04-15</TD><TT>2215</TT><D>2:5</D><ST>9</ST><V>1</V><T>0</T><IFT>*</IFT><IDT>2D</IDT><IST></IST><STIME></STIME><OF>AF1239</OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>EK</A><M>6693</M><PM>6900</PM><X>680</X><XNQ>90</XNQ><Q>590</Q><CM>5030</CM><CPM>5180</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201106071500155626_23917808_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>EK309,EK075;EK309,EK071;EK307,EK073;EK307,EK075</MS><G>0</G><PT>ADT</PT><V>0</V><CL>2</CL><RL>3</RL><LD>0</LD><SS><S><F>BJS</F><T>DXB</T><A>EK</A><No>EK309</No><FA>PEK</FA><TA>DXB</TA><FD>2014-04-15</FD><FT>0725</FT><TD>2014-04-15</TD><TT>1200</TT><ST>M</ST><M>6900</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>EK309</No><FA>PEK</FA><TA>DXB</TA><A>EK</A><ET>77W</ET><FD>2014-04-15</FD><FT>0725</FT><TD>2014-04-15</TD><TT>1200</TT><D>8:35</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>3</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>EK307</No><FA>PEK</FA><TA>DXB</TA><A>EK</A><ET>388</ET><FD>2014-04-15</FD><FT>2355</FT><TD>2014-04-16</TD><TT>0420</TT><D>8:25</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>3</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>DXB</F><T>PAR</T><A>EK</A><No>EK075</No><FA>DXB</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>2005</TT><ST>M</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>EK075</No><FA>DXB</FA><TA>CDG</TA><A>EK</A><ET>388</ET><FD>2014-04-15</FD><FT>1500</FT><TD>2014-04-15</TD><TT>2005</TT><D>8:5</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2C</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>EK071</No><FA>DXB</FA><TA>CDG</TA><A>EK</A><ET>77W</ET><FD>2014-04-16</FD><FT>0410</FT><TD>2014-04-16</TD><TT>0930</TT><D>8:20</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2C</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>EK073</No><FA>DXB</FA><TA>CDG</TA><A>EK</A><ET>388</ET><FD>2014-04-16</FD><FT>0820</FT><TD>2014-04-16</TD><TT>1330</TT><D>8:10</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2C</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KA</A><M>7900</M><PM>7900</PM><X>984</X><XNQ>954</XNQ><Q>30</Q><CM>5930</CM><CPM>5930</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201402241153598882_22000864_0</L><SP>0</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>KA997,CX261;KA937,CX261;KA905,CX261;KA901,CX261</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>KA</A><No>KA997</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><ST>I</ST><M>7900</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KA997</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>320</ET><FD>2014-04-15</FD><FT>0250</FT><TD>2014-04-15</TD><TT>0625</TT><D>3:35</D><ST>1</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA937</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0740</FT><TD>2014-04-15</TD><TT>1115</TT><D>3:35</D><ST>2</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA905</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>1140</TT><D>3:40</D><ST>2</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KA901</No><FA>PEK</FA><TA>HKG</TA><A>KA</A><ET>333</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1610</TT><D>3:40</D><ST>2</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>R</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>1</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>8381</M><PM>8640</PM><X>1786</X><XNQ></XNQ><Q></Q><CM>6300</CM><CPM>6480</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201307301427230309_23747237_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>AMS</T><A>CZ</A><No>CZ345</No><FA>PEK</FA><TA>AMS</TA><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><ST>H</ST><M>8640</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ345</No><FA>PEK</FA><TA>AMS</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><D>11:50</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>0</IDT><IST></IST><STIME>01:10</STIME><OF></OF></F></FS></S><S><F>AMS</F><T>PAR</T><A>KL</A><No>KL1227</No><FA>AMS</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0715</FT><TD>2014-04-15</TD><TT>0840</TT><ST>K</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>KL1227</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73J</ET><FD>2014-04-15</FD><FT>0715</FT><TD>2014-04-15</TD><TT>0840</TT><D>1:25</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1229</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73J</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>0925</TT><D>1:25</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1233</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73H</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1345</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1243</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73J</ET><FD>2014-04-15</FD><FT>1625</FT><TD>2014-04-15</TD><TT>1740</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1245</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73W</ET><FD>2014-04-15</FD><FT>1745</FT><TD>2014-04-15</TD><TT>1900</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>8381</M><PM>8640</PM><X>1485</X><XNQ>1485</XNQ><Q>0</Q><CM>6300</CM><CPM>6480</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201210261735158010_16865262_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>CAN</T><A>CZ</A><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><ST>Y</ST><M>8640</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3000</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>380</ET><FD>2014-04-15</FD><FT>0930</FT><TD>2014-04-15</TD><TT>1245</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3116</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1345</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3112</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1445</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3102</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1545</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3106</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1645</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3162</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1430</FT><TD>2014-04-15</TD><TT>1745</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3104</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>380</ET><FD>2014-04-15</FD><FT>1530</FT><TD>2014-04-15</TD><TT>1845</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ323</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>1945</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>01:40</STIME><OF></OF></F><F><No>CZ3122</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>33A</ET><FD>2014-04-15</FD><FT>1730</FT><TD>2014-04-15</TD><TT>2045</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3100</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>787</ET><FD>2014-04-15</FD><FT>1830</FT><TD>2014-04-15</TD><TT>2145</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3110</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1930</FT><TD>2014-04-15</TD><TT>2245</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CAN</F><T>PAR</T><A>CZ</A><No>CZ347</No><FA>CAN</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0020</FT><TD>2014-04-16</TD><TT>0720</TT><ST>H</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CZ347</No><FA>CAN</FA><TA>CDG</TA><A>CZ</A><ET>332</ET><FD>2014-04-16</FD><FT>0020</FT><TD>2014-04-16</TD><TT>0720</TT><D>14:0</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>8381</M><PM>8640</PM><X>1485</X><XNQ>1485</XNQ><Q>0</Q><CM>6300</CM><CPM>6480</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201204131749135939_23764496_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>CAN</T><A>CZ</A><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><ST>Y</ST><M>8640</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3000</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>380</ET><FD>2014-04-15</FD><FT>0930</FT><TD>2014-04-15</TD><TT>1245</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3116</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1345</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3112</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1445</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3102</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1545</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3106</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1645</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3162</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1430</FT><TD>2014-04-15</TD><TT>1745</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3104</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>380</ET><FD>2014-04-15</FD><FT>1530</FT><TD>2014-04-15</TD><TT>1845</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ323</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>1945</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>01:40</STIME><OF></OF></F><F><No>CZ3122</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>33A</ET><FD>2014-04-15</FD><FT>1730</FT><TD>2014-04-15</TD><TT>2045</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CAN</F><T>PAR</T><A>CZ</A><No>CZ783</No><FA>CAN</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2300</FT><TD>2014-04-16</TD><TT>0550</TT><ST>H</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CZ783</No><FA>CAN</FA><TA>CDG</TA><A>CZ</A><ET>772</ET><FD>2014-04-15</FD><FT>2300</FT><TD>2014-04-16</TD><TT>0550</TT><D>13:50</D><ST>9</ST><V>1</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF>AF0107</OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>8381</M><PM>8640</PM><X>1473</X><XNQ></XNQ><Q></Q><CM>6300</CM><CPM>6480</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201210261735158010_16865027_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>AMS</T><A>CZ</A><No>CZ345</No><FA>PEK</FA><TA>AMS</TA><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><ST>H</ST><M>8640</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ345</No><FA>PEK</FA><TA>AMS</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><D>11:50</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>0</IDT><IST></IST><STIME>01:10</STIME><OF></OF></F></FS></S><S><F>AMS</F><T>PAR</T><A>CZ</A><No>CZ7947</No><FA>AMS</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>0925</TT><ST>H</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CZ7947</No><FA>AMS</FA><TA>CDG</TA><A>CZ</A><ET>737</ET><FD>2014-04-15</FD><FT>0800</FT><TD>2014-04-15</TD><TT>0925</TT><D>1:25</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CX</A><M>8550</M><PM>8550</PM><X>1000</X><XNQ></XNQ><Q></Q><CM>6420</CM><CPM>6420</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201312171353093473_18672138_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>CX347,CX261;CX347,CX279;CX391,CX261;CX391,CX279</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>HKG</T><A>CX</A><No>CX347</No><FA>PEK</FA><TA>HKG</TA><FD>2014-04-15</FD><FT>1000</FT><TD>2014-04-15</TD><TT>1350</TT><ST>B</ST><M>8550</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CX347</No><FA>PEK</FA><TA>HKG</TA><A>CX</A><ET>333</ET><FD>2014-04-15</FD><FT>1000</FT><TD>2014-04-15</TD><TT>1350</TT><D>3:50</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CX391</No><FA>PEK</FA><TA>HKG</TA><A>CX</A><ET>333</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1715</TT><D>3:45</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>HKG</F><T>PAR</T><A>CX</A><No>CX261</No><FA>HKG</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><ST>B</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CX261</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>77W</ET><FD>2014-04-16</FD><FT>0005</FT><TD>2014-04-16</TD><TT>0655</TT><D>13:50</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CX279</No><FA>HKG</FA><TA>CDG</TA><A>CX</A><ET>343</ET><FD>2014-04-16</FD><FT>0040</FT><TD>2014-04-16</TD><TT>0755</TT><D>14:15</D><ST>6</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>2A</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>LH</A><M>9670</M><PM>9670</PM><X>1919</X><XNQ>1919</XNQ><Q>0</Q><CM>7260</CM><CPM>7260</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201104251105588289_23940104_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>LH721,LH1040;LH721,LH1042;LH721,LH1046;LH721,LH1050;LH721,LH1052</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>FRA</T><A>LH</A><No>LH721</No><FA>PEK</FA><TA>FRA</TA><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1445</TT><ST>M</ST><M>9670</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>LH721</No><FA>PEK</FA><TA>FRA</TA><A>LH</A><ET>388</ET><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1445</TT><D>11:15</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>FRA</F><T>PAR</T><A>LH</A><No>LH1040</No><FA>FRA</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1600</FT><TD>2014-04-15</TD><TT>1710</TT><ST>M</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>LH1040</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>32A</ET><FD>2014-04-15</FD><FT>1600</FT><TD>2014-04-15</TD><TT>1710</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1042</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>321</ET><FD>2014-04-15</FD><FT>1650</FT><TD>2014-04-15</TD><TT>1800</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1046</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>733</ET><FD>2014-04-15</FD><FT>1740</FT><TD>2014-04-15</TD><TT>1850</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1050</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>733</ET><FD>2014-04-15</FD><FT>2045</FT><TD>2014-04-15</TD><TT>2155</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH1052</No><FA>FRA</FA><TA>CDG</TA><A>LH</A><ET>320</ET><FD>2014-04-15</FD><FT>2135</FT><TD>2014-04-15</TD><TT>2245</TT><D>1:10</D><ST>9</ST><V>0</V><T>0</T><IFT>1</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>LH</A><M>9670</M><PM>9670</PM><X>1836</X><XNQ>1836</XNQ><Q>0</Q><CM>7260</CM><CPM>7260</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201104251105588289_23940103_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>LH723,LH2236;LH723,LH2238</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>2</RL><LD>0</LD><SS><S><F>BJS</F><T>MUC</T><A>LH</A><No>LH723</No><FA>PEK</FA><TA>MUC</TA><FD>2014-04-15</FD><FT>1235</FT><TD>2014-04-15</TD><TT>1700</TT><ST>M</ST><M>9670</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>LH723</No><FA>PEK</FA><TA>MUC</TA><A>LH</A><ET>346</ET><FD>2014-04-15</FD><FT>1235</FT><TD>2014-04-15</TD><TT>1700</TT><D>11:25</D><ST>9</ST><V>0</V><T>0</T><IFT>3</IFT><IDT>2</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>MUC</F><T>PAR</T><A>LH</A><No>LH2236</No><FA>MUC</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>1845</FT><TD>2014-04-15</TD><TT>2020</TT><ST>M</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>LH2236</No><FA>MUC</FA><TA>CDG</TA><A>LH</A><ET>E95</ET><FD>2014-04-15</FD><FT>1845</FT><TD>2014-04-15</TD><TT>2020</TT><D>1:35</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>LH2238</No><FA>MUC</FA><TA>CDG</TA><A>LH</A><ET>319</ET><FD>2014-04-15</FD><FT>2125</FT><TD>2014-04-15</TD><TT>2300</TT><D>1:35</D><ST>9</ST><V>1</V><T>0</T><IFT>2</IFT><IDT>1</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>KL</A><M>11530</M><PM>11530</PM><X>1843</X><XNQ></XNQ><Q></Q><CM>8650</CM><CPM>8650</CPM><C1>0</C1><C2>0</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201107071354080787_23934089_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS>KL4302,KL1223;KL4302,KL1227;KL898,KL1243;KL898,KL1245</MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>1</RL><LD>0</LD><SS><S><F>BJS</F><T>AMS</T><A>KL</A><No>KL4302</No><FA>PEK</FA><TA>AMS</TA><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><ST>M</ST><M>11530</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>KL4302</No><FA>PEK</FA><TA>AMS</TA><A>KL</A><ET>332</ET><FD>2014-04-15</FD><FT>0050</FT><TD>2014-04-15</TD><TT>0540</TT><D>11:50</D><ST>9</ST><V>1</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME>01:10</STIME><OF>CZ0345</OF></F><F><No>KL898</No><FA>PEK</FA><TA>AMS</TA><A>KL</A><ET>74E</ET><FD>2014-04-15</FD><FT>1055</FT><TD>2014-04-15</TD><TT>1515</TT><D>11:20</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>AMS</F><T>PAR</T><A>KL</A><No>KL1223</No><FA>AMS</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0650</FT><TD>2014-04-15</TD><TT>0805</TT><ST>Y</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>KL1223</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73W</ET><FD>2014-04-15</FD><FT>0650</FT><TD>2014-04-15</TD><TT>0805</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1227</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73J</ET><FD>2014-04-15</FD><FT>0715</FT><TD>2014-04-15</TD><TT>0840</TT><D>1:25</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1243</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73J</ET><FD>2014-04-15</FD><FT>1625</FT><TD>2014-04-15</TD><TT>1740</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>KL1245</No><FA>AMS</FA><TA>CDG</TA><A>KL</A><ET>73W</ET><FD>2014-04-15</FD><FT>1745</FT><TD>2014-04-15</TD><TT>1900</TT><D>1:15</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2F</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>AF</A><M>12090</M><PM>12090</PM><X>1440</X><XNQ>1440</XNQ><Q>0</Q><CM>9070</CM><CPM>9070</CPM><C1>0</C1><C2>0</C2><Z>0</Z><ZZ>0</ZZ><N>0</N><L>3_I201106231607053750_23892734</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>1</RL><LD>0</LD><SS><S><F>BJS</F><T>PAR</T><A>AF</A><No>AF381</No><FA>PEK</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>0100</FT><TD>2014-04-15</TD><TT>0550</TT><ST>M</ST><M>12090</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>AF381</No><FA>PEK</FA><TA>CDG</TA><A>AF</A><ET>77W</ET><FD>2014-04-15</FD><FT>0100</FT><TD>2014-04-15</TD><TT>0550</TT><D>11:50</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>AF129</No><FA>PEK</FA><TA>CDG</TA><A>AF</A><ET>77W</ET><FD>2014-04-15</FD><FT>0905</FT><TD>2014-04-15</TD><TT>1400</TT><D>11:55</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>16005</M><PM>16500</PM><X>1485</X><XNQ>1485</XNQ><Q>0</Q><CM>12010</CM><CPM>12380</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>1</N><L>3_I201210261735158010_16865265_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>1</RL><LD>0</LD><SS><S><F>BJS</F><T>CAN</T><A>CZ</A><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><ST>W</ST><M>16500</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3116</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1345</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3112</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1445</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3102</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1545</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3106</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1645</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3162</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1430</FT><TD>2014-04-15</TD><TT>1745</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ323</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>1945</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>01:40</STIME><OF></OF></F><F><No>CZ3122</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>33A</ET><FD>2014-04-15</FD><FT>1730</FT><TD>2014-04-15</TD><TT>2045</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3110</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1930</FT><TD>2014-04-15</TD><TT>2245</TT><D>3:15</D><ST>8</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CAN</F><T>PAR</T><A>CZ</A><No>CZ347</No><FA>CAN</FA><TA>CDG</TA><FD>2014-04-16</FD><FT>0020</FT><TD>2014-04-16</TD><TT>0720</TT><ST>W</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CZ347</No><FA>CAN</FA><TA>CDG</TA><A>CZ</A><ET>332</ET><FD>2014-04-16</FD><FT>0020</FT><TD>2014-04-16</TD><TT>0720</TT><D>14:0</D><ST>9</ST><V>0</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S></SS></R><R><F>BJS</F><T>PAR</T><A>CZ</A><M>16005</M><PM>16500</PM><X>1485</X><XNQ>1485</XNQ><Q>0</Q><CM>12010</CM><CPM>12380</CPM><C1>3</C1><C2>3</C2><Z>1</Z><ZZ>1</ZZ><N>0</N><L>3_I201204131749135939_23764487_0</L><SP>1</SP><TP>0</TP><TF></TF><LCC>0</LCC><MS></MS><G>0</G><PT>ADT</PT><V>0</V><CL>1</CL><RL>1</RL><LD>0</LD><SS><S><F>BJS</F><T>CAN</T><A>CZ</A><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><ST>W</ST><M>16500</M><FP>1</FP><DP>1</DP><K>0</K><FS><F><No>CZ3108</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>0830</FT><TD>2014-04-15</TD><TT>1145</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3116</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1030</FT><TD>2014-04-15</TD><TT>1345</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3112</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1130</FT><TD>2014-04-15</TD><TT>1445</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3102</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>332</ET><FD>2014-04-15</FD><FT>1230</FT><TD>2014-04-15</TD><TT>1545</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3106</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>330</ET><FD>2014-04-15</FD><FT>1330</FT><TD>2014-04-15</TD><TT>1645</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ3162</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>333</ET><FD>2014-04-15</FD><FT>1430</FT><TD>2014-04-15</TD><TT>1745</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT>2</IFT><IDT>*</IDT><IST></IST><STIME></STIME><OF></OF></F><F><No>CZ323</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>321</ET><FD>2014-04-15</FD><FT>1630</FT><TD>2014-04-15</TD><TT>1945</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME>01:40</STIME><OF></OF></F><F><No>CZ3122</No><FA>PEK</FA><TA>CAN</TA><A>CZ</A><ET>33A</ET><FD>2014-04-15</FD><FT>1730</FT><TD>2014-04-15</TD><TT>2045</TT><D>3:15</D><ST>9</ST><V>0</V><T>0</T><IFT></IFT><IDT></IDT><IST></IST><STIME></STIME><OF></OF></F></FS></S><S><F>CAN</F><T>PAR</T><A>CZ</A><No>CZ783</No><FA>CAN</FA><TA>CDG</TA><FD>2014-04-15</FD><FT>2300</FT><TD>2014-04-16</TD><TT>0550</TT><ST>W</ST><M>0</M><FP>2</FP><DP>1</DP><K>0</K><FS><F><No>CZ783</No><FA>CAN</FA><TA>CDG</TA><A>CZ</A><ET>772</ET><FD>2014-04-15</FD><FT>2300</FT><TD>2014-04-16</TD><TT>0550</TT><D>13:50</D><ST>6</ST><V>1</V><T>0</T><IFT>*</IFT><IDT>2E</IDT><IST></IST><STIME></STIME><OF>AF0107</OF></F></FS></S></SS></R></RS>";
            WriteLog.write("piao59_INTER", resultStr);
            SAXReader reader = new SAXReader();
            // System.out.println(resultStr);
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            Iterator<Element> elementlist = root.elementIterator("R");
            while (elementlist.hasNext()) {
                List<AirSegementBean> airSegements = new ArrayList<AirSegementBean>();
                RouteBean route = new RouteBean();
                Element element = elementlist.next();
                String fromCity = element.elementTextTrim("F");// 出发城市三字码
                route.setRfromCity(fromCity);
                String toCity = element.elementTextTrim("T");// 到达城市三字码
                route.setRtoCity(toCity);
                String airCo = element.elementTextTrim("A");// 航空公司代码
                route.setRairCo(airCo);
                String totalFare = element.elementTextTrim("M");// 金额(不含税、不含基本代理费)
                route.setRtotalFare(Float.valueOf(totalFare));
                String newPrice = element.elementTextTrim("PM");// 金额(不含税、含基本代理费)
                route.setRnewPrice(Float.valueOf(newPrice));
                String totalTax = element.elementTextTrim("X");// 参考税(totalTax)
                route.setRtotalTax(Float.valueOf(totalTax));
                String XNQ = element.elementTextTrim("XNQ");
                String Q = element.elementTextTrim("Q");
                String childmoney = element.elementTextTrim("CM");// 儿童价
                route.setRchildMoney(Float.valueOf(childmoney));
                String cpmoney = element.elementTextTrim("CPM");// 儿童底价
                route.setRcpmoney(Float.valueOf(cpmoney));
                if (null != element.elementTextTrim("C1") && !"".equals(element.elementTextTrim("C1"))) {// 本航空公司代理费
                    route.setRcompanyagprice(Float.valueOf(element.elementTextTrim("C1")));
                }
                if (null != element.elementTextTrim("C2") && !"".equals(element.elementTextTrim("C2"))) {// 联运航空公司代理费
                    route.setRcompanyagprice2(Float.valueOf(element.elementTextTrim("C2")));
                }
                String truencount = element.elementTextTrim("Z");// 转机次数(往返程为中转次数之和)
                route.setRtruencount(Integer.valueOf(truencount));
                String truencountback = element.elementTextTrim("ZZ");// 转机次数(单程只有一个值，往返有两个值，以逗号分开，如：1,1)
                route.setRtruencountback(truencountback);
                String isnight = element.elementTextTrim("N");// 是否过夜(0否 1是)
                // (单程只有一个值，如：0；往返有两个值，以逗号分开，分别代表去程和回程，如：0,1)]
                route.setRisnight(isnight);
                String limitterm = element.elementTextTrim("L");// 限制条件查询参数，通过此参数，可以获取限制条件
                route.setRlimitterm(limitterm);
                String spec = element.elementTextTrim("SP");// 为1表示该价格以票面为准
                route.setRspec(Integer.valueOf(spec));
                String teshuprice = element.elementTextTrim("TP");// 1代表特殊价格
                // 0非特殊价格
                route.setRteshuprice(Integer.valueOf(teshuprice));
                String TF = element.elementTextTrim("TF");
                String iscompanyprice = element.elementTextTrim("LCC");// 是否廉价航空公司价格
                // 0 不是
                // 1 是
                route.setRiscompanyprice(Integer.valueOf(iscompanyprice));
                if (null != element.elementTextTrim("FC")) {// 去程航段数（仅往返出现）
                    route.setRflightcount(Integer.valueOf(element.elementTextTrim("FC")));
                }
                String flightmessage = element.elementTextTrim("MS");// 航信/GDS返回的固定航班对，必须要按这样的航班组合
                route.setRflightmessage(flightmessage);
                // <MS>SU201,SU3000;SU201,SU2462;SU201,SU3008;SU201,SU2458;SU3009,SU204;SU3005,SU204;SU260,SU204;SU3007,SU204;SU2455,SU204;SU2461,SU200</MS>
                String gds = element.elementTextTrim("G");// GDS数据为1，否则为0
                route.setRgds(Integer.valueOf(gds));
                String passengertype = element.elementTextTrim("PT");// 乘客类型
                route.setRpassengertype(passengertype);
                String istransitapply = element.elementTextTrim("V");// 是否需要过境签
                route.setRistransitapply(Integer.valueOf(istransitapply));
                String applylevel = element.elementTextTrim("CL");// 改期级别 0
                // 无法识别 1
                // 改期宽松 2
                // 改期有损失 3
                // 改期严格
                if (null != applylevel && !"".equals(applylevel)) {
                    route.setRapplylevel(Integer.valueOf(applylevel));
                }
                String turnticketlevel = element.elementTextTrim("RL");// 退票级别
                // 0
                // 无法识别
                // 1
                // 退票宽松
                // 2
                // 退票有损失
                // 3
                // 退票严格
                if (null != turnticketlevel && !"".equals(turnticketlevel)) {
                    route.setRturnticketlevel(Integer.valueOf(turnticketlevel));
                }
                String lastday = element.elementTextTrim("LD");// 代表本价格是否是最后一天
                if (null != lastday && !"".equals(lastday)) {
                    route.setRlastday(Integer.valueOf(lastday));
                }
                Element SS = element.element("SS");
                List<Element> segments = SS.elements("S");// 航段（多条）(segment)
                Iterator it = segments.iterator();
                // Map<Integer, List<AirSegementBean>> tempMap = new
                // HashMap<Integer, List<AirSegementBean>>();
                // int temp_ = 0;

                while (it.hasNext()) {
                    AirSegementBean airSegement = new AirSegementBean();
                    Element segments_info = (Element) it.next();
                    String s_fromCity = segments_info.elementTextTrim("F");// 多条-出发城市三字码
                    airSegement.setAfromCity(s_fromCity);
                    String s_toCity = segments_info.elementTextTrim("T");// 到达城市三字码
                    airSegement.setAtoCity(s_toCity);
                    String s_airCo = segments_info.elementTextTrim("A");// 航空公司代码
                    airSegement.setAairCo(s_airCo);
                    String s_flightNumber = segments_info.elementTextTrim("NO");// 航班号
                    airSegement.setAflightNumber(s_flightNumber);
                    String s_fromAirport = segments_info.elementTextTrim("FA");// 起飞机场代码
                    airSegement.setAfromAirport(s_fromAirport);
                    String s_toAirport = segments_info.elementTextTrim("TA");// 到达机场代码
                    airSegement.setAtoAirport(s_toAirport);
                    String s_fromDate = segments_info.elementTextTrim("FD");// 起飞日期
                    airSegement.setAfromDate(s_fromDate);
                    String s_fromTime = segments_info.elementTextTrim("FT");// 起飞时间
                    s_fromTime = s_fromTime.substring(0, 2) + ":" + s_fromTime.substring(2);
                    airSegement.setAfromTime(s_fromTime);
                    String s_toDate = segments_info.elementTextTrim("TD");// 到达日期
                    airSegement.setAtoDate(s_toDate);
                    String s_toTime = segments_info.elementTextTrim("TT");// 到达时间
                    s_toTime = s_toTime.substring(0, 2) + ":" + s_toTime.substring(2);
                    airSegement.setAtoTime(s_toTime);
                    String s_seatType = segments_info.elementTextTrim("ST");// 舱位类型编码
                    airSegement.setAseatType(s_seatType);
                    String s_stagemoney = segments_info.elementTextTrim("M");// 分段价格
                    airSegement.setAstagemoney(s_stagemoney);
                    String s_departtype = segments_info.elementTextTrim("DP");// 出发为1（返回为0）单程始终为1
                    airSegement.setAdeparttype(Integer.valueOf(s_departtype));
                    String s_segmentOrder = segments_info.elementTextTrim("FP");// 航段序号（去程和回程都从1开始，可以根据这个计算转机次数）
                    airSegement.setGroup(Integer.valueOf(s_segmentOrder));
                    String s_k = segments_info.elementTextTrim("K");

                    Element FS = segments_info.element("FS");
                    List<Element> flights = FS.elements("F");
                    Iterator itr = flights.iterator();
                    List<FlightinfoBean> flightinfos = new ArrayList<FlightinfoBean>();
                    while (itr.hasNext()) {
                        FlightinfoBean flightinfo = new FlightinfoBean();
                        Element flights_info = (Element) itr.next();
                        String f_flightNumber = flights_info.elementTextTrim("No");// 航班号
                        flightinfo.setFlightNumber(f_flightNumber);
                        String f_fromAirport = flights_info.elementTextTrim("FA");// 起飞机场代码
                        flightinfo.setOrigin(f_fromAirport);
                        String f_toAirport = flights_info.elementTextTrim("TA");// 到达机场代码
                        flightinfo.setDestination(f_toAirport);
                        String f_airCo = flights_info.elementTextTrim("A");// 航空公司代码
                        flightinfo.setCarrier(f_airCo);
                        String f_equipType = flights_info.elementTextTrim("ET");// 机型
                        flightinfo.setFequipType(f_equipType);
                        String f_fromDate = flights_info.elementTextTrim("FD");// 起飞日期
                        flightinfo.setFdepartureDate(f_fromDate);
                        String f_fromTime = flights_info.elementTextTrim("FT");// 起飞时间
                        flightinfo.setDepartureTime(f_fromTime);
                        String f_toDate = flights_info.elementTextTrim("TD");// 到达日期
                        flightinfo.setFarrivalDate(f_toDate);
                        String f_toTime = flights_info.elementTextTrim("TT");// 到达时间
                        flightinfo.setArrivalTime(f_toTime);
                        String f_duration = flights_info.elementTextTrim("D");// 飞行时长
                        flightinfo.setFduration(f_duration);
                        String f_spaceremainamount = flights_info.elementTextTrim("ST");// 舱位剩余数量
                        flightinfo.setFspaceremainamount(f_spaceremainamount);
                        String f_isenjoyflight = flights_info.elementTextTrim("V");// 是否代码共享航班(0-非共享航班
                        // 1-共享航班)
                        flightinfo.setFisenjoyflight(Integer.valueOf(f_isenjoyflight));
                        String f_isstopflight = flights_info.elementTextTrim("T");// 是否经停航班(0-否 1-是)
                        flightinfo.setFisstopflight(Integer.valueOf(f_isstopflight));
                        String f_starterminal = flights_info.elementTextTrim("IFT");// 起飞航站楼
                        flightinfo.setFstarterminal(f_starterminal);
                        String f_endterminal = flights_info.elementTextTrim("IDT");// 到达航站楼
                        flightinfo.setFendterminal(f_endterminal);
                        String f_stopairport = flights_info.elementTextTrim("IST");// 经停机场
                        flightinfo.setFstopairport(f_stopairport);
                        String stoptime = flights_info.elementTextTrim("STIME");// 停留时间
                        flightinfo.setFstoptime(stoptime);
                        String realflight = flights_info.elementTextTrim("OF");// 实际承运航班
                        flightinfo.setFrealflight(realflight);
                        flightinfos.add(flightinfo);
                    }
                    airSegement.setFlightinfos(flightinfos);
                    airSegements.add(airSegement);
                    // tempMap.put(temp_, airSegements);
                    // temp_++;
                }
                route.setAirSegements(airSegements);
                routes.add(route);
                /*
                 * if (segments.size() == 1) {
                 * route.setAirSegements(tempMap.get(0)); routes.add(route); }
                 * else if (segments.size() >= 2) { //List<FlightinfoBean>
                 * FlightinfoBean0 = tempMap.get(0).get(0).getFlightinfos();
                 * //List<FlightinfoBean> FlightinfoBean1 =
                 * tempMap.get(1).get(0).getFlightinfos(); RouteBean route1 =
                 * route;
                 * 
                 * for (int i = 0; i < segments.size(); i++) { if
                 * (tempMap.get(i).get(0).getAdeparttype() == 0) { List<AirSegementBean>
                 * airSegements_temps = new ArrayList<AirSegementBean>();
                 * //List<FlightinfoBean> FlightinfoBean_temps = new ArrayList<FlightinfoBean>();
                 * AirSegementBean airSegement_temp = new AirSegementBean();
                 * //FlightinfoBean_temps =
                 * tempMap.get(i).get(0).getFlightinfos(); airSegement_temp =
                 * tempMap.get(i).get(i);
                 * //airSegement_temp.setFlightinfos(FlightinfoBean_temps);
                 * airSegements_temps.add(airSegement_temp);
                 * route1.setAirSegements(airSegements_temps); } else { List<AirSegementBean>
                 * airSegements_temps = new ArrayList<AirSegementBean>();
                 * //List<FlightinfoBean> FlightinfoBean_temps = new ArrayList<FlightinfoBean>();
                 * AirSegementBean airSegement_temp = new AirSegementBean(); //
                 * FlightinfoBean_temps =
                 * tempMap.get(i).get(0).getFlightinfos(); airSegement_temp =
                 * tempMap.get(i).get(i);
                 * //airSegement_temp.setFlightinfos(FlightinfoBean_temps);
                 * airSegements_temps.add(airSegement_temp);
                 * route1.setAirSegements(airSegements_temps); } }
                 * routes.add(route1); /* for (int i = 0, size =
                 * FlightinfoBean0.size(); i < size; i++) { RouteBean route1 =
                 * route;
                 * 
                 * FlightinfoBean FlightinfoBean_temp1 = FlightinfoBean0.get(i);
                 * List<AirSegementBean> airSegements_temp = new ArrayList<AirSegementBean>();
                 * 
                 * List<FlightinfoBean> flightinfos_temp = new ArrayList<FlightinfoBean>();
                 * flightinfos_temp.add(FlightinfoBean_temp1);
                 * 
                 * AirSegementBean airSegement_temp = tempMap.get(0).get(0);
                 * airSegement_temp.setFlightinfos(flightinfos_temp);
                 * airSegements_temp.add(airSegement_temp);
                 * 
                 * for (int j = 0, sizej = FlightinfoBean1.size(); j < sizej;
                 * j++) { FlightinfoBean_temp1 = FlightinfoBean1.get(j);
                 * airSegements_temp = new ArrayList<AirSegementBean>();
                 * 
                 * flightinfos_temp = new ArrayList<FlightinfoBean>();
                 * flightinfos_temp.add(FlightinfoBean_temp1);
                 * 
                 * airSegement_temp = tempMap.get(1).get(0);
                 * airSegement_temp.setFlightinfos(flightinfos_temp);
                 * airSegements_temp.add(airSegement_temp);
                 * 
                 * route1.setAirSegements(airSegements_temp);
                 * routes.add(route1); } }
                 */
                // }
            }
            allRoute.setRoutes(routes);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allRoute;
    }

    public static void main(String[] args) {
        // getInterFlight("");
        /*
         * SearchFligehtBean searchFligehtBean = new SearchFligehtBean(); List<SearchPointBean>
         * searchPointBeans = new ArrayList<SearchPointBean>(); SearchPointBean
         * fromsearchPointBean = new SearchPointBean();
         * fromsearchPointBean.setStartCity("PEK");
         * fromsearchPointBean.setEndCity("GDE");
         * fromsearchPointBean.setStartTime("2014-05-29");
         * fromsearchPointBean.setSeatType("Y");
         * //fromsearchPointBean.setReturnTime("2014-06-05");
         * fromsearchPointBean.setAirCompany("");
         * searchPointBeans.add(fromsearchPointBean);
         * searchFligehtBean.setPoints(searchPointBeans);
         * searchPointBeans.add(fromsearchPointBean);
         * searchFligehtBean.setPoints(searchPointBeans); new
         * PiaowujiuBookutil().SeachInterFlight(searchFligehtBean);//1.航班
         *//*
           		 * List<Map<String, String>> a = new PiaowujiuBookutil()
           		 * .getrefundAndapplychange("15_1G1152077865989490001-1G1152077865989490002_0");//2.退改签
           		 * for (Map<String, String> map : a) { System.out.println("类型：" +
           		 * map.get("type")); System.out.println("内容：" + map.get("content")); }
           		 */
        /*
         * String urlstr =
         * "http://www.piao59.com/api/test/yn_seat.php?XMLStr=PEKHKG2014-04-18,Q,CA101/CA;HKGPEK2014-04-19,Q,CA116/CA:3";
         * new PiaowujiuBookutil().checkspace(urlstr);//3.舱位验证
         */// /String url =
           // "http://ws.hangtian123.com/htslinterpiao?type=1&paramContent=fromCity=PEK~toCity=GDE~fromDate=2014-04-18~returnDate=~airCo=~seatType=Y";
           // url =
           // "http://ws.hangtian123.com/htslinterpiao?type=1&paramContent=fromCity=PEK~toCity=CDG~fromDate=2014-04-15~returnDate=~airCo=~seatType=Y";
        /*
         * Forderinfo forderinfo = new Forderinfo();
         * forderinfo.setContactmark("J20131104100039"); new
         * PiaowujiuBookutil().getforderinfostate(forderinfo);
         */// 9、订单状态查询
        /*
         * Forderinfo forderinfo = new Forderinfo();
         * forderinfo.setContactmark("J20131104100039"); new
         * PiaowujiuBookutil().getOrderDetail(forderinfo);
         */

    }

    /**
     * 作者：邹远超 日期：2014-4-15 说明：获取政策02
     * 
     * @param url
     * @return
     */
    public List<Zrate> getZrate(String url) {

        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            String resultStr = SendPostandGet.submitPost(url, "").toString();// (url);
            // WriteLog.write("piao59Zrate", resultStr);
            // System.out.println(resultStr);
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            Iterator<Element> elementlist = root.elementIterator("FS");
            while (elementlist.hasNext()) {
                Zrate zrate = new Zrate();
                Element element = elementlist.next();
                String fromcity = element.elementTextTrim("FROMCITY");// 出发城市
                zrate.setDepartureport(fromcity);
                String tocity = element.elementTextTrim("TOCITY");// 到达城市
                zrate.setArrivalport(tocity);
                String policyid = element.elementTextTrim("POLICYID");// 政策ID
                zrate.setRelationzrateid(Long.valueOf(policyid));
                String company = element.elementTextTrim("COMPANY");// 航空公司二字码
                zrate.setAircompanycode(company);
                String banckpoint = element.elementTextTrim("BACKPOINT");// 后返点
                // ZrateBean
                // 中banckpoint国际机票用这个字段
                zrate.setRatevalue(Float.valueOf(banckpoint));
                String outticketmoney = element.elementTextTrim("ADDMONEY");// 出票费
                // ZrateBean
                // 中outticketmoney国际机票用这个字段
                zrate.setOutticketmoney(Float.valueOf(outticketmoney));
                String limitspace = element.elementTextTrim("CW");// 舱位限制
                // 多个以“/”分割。ZrateBean
                // 中limitspace国际机票用这个字段
                zrate.setLimitspace(limitspace);
                String isChangeSupply = element.elementTextTrim("ISHBM");// 是否换编码
                // 0代表不换，1代表换
                zrate.setWhetherincoding(isChangeSupply);
                String isSupplySingle = element.elementTextTrim("ISXCD");// 是否提供行程单
                // 0代表不提供，1代表提供。ZrateBean
                // 中isSupplySingle国际机票用这个字段
                zrate.setIsSupplySingle(Integer.valueOf(isSupplySingle));
                try {
                    String issuedstartdate = element.elementTextTrim("CPDATE1");// 政策开始时间
                    String issuedenddate = element.elementTextTrim("CPDATE2");// 政策结束时间
                    if (issuedstartdate != null && issuedenddate != null) {
                        zrate.setIssuedstartdate(formattime(issuedstartdate));
                        zrate.setEnddate(formattime(issuedenddate));
                    }
                }
                catch (Exception e) {
                }
                String remark = element.elementTextTrim("INFOS");// 政策备注
                zrate.setRemark(remark);
                zrates.add(zrate);
            }
        }
        catch (Exception e) {
            logger.error(e);
        }

        return zrates;
    }

    /**
     * 作者：邹远超 日期：2014年5月20日 说明：格式化时间
     * 
     * @param time
     * @return
     */
    public static Timestamp formattime(String time) {
        try {
            Format f = new SimpleDateFormat("yyyy-MM-dd");
            Date d = (Date) f.parseObject(time);
            Timestamp ts = new Timestamp(d.getTime());
            return ts;
        }
        catch (Exception e) {
        }
        return null;
    }

    /**
     * 作者：邹远超 日期：2014-4-17 说明：退改签规定接口03
     */
    public List<Map<String, String>> refundAndapplychange(String url) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        String resultStr = SendPostandGet.submitPost(url, "").toString();// (url);
        try {
            Document document = DocumentHelper.parseText(resultStr);
            // System.out.println(resultStr);
            Element root = document.getRootElement();
            Iterator<Element> elementlist = root.elementIterator("limit");
            while (elementlist.hasNext()) {
                Map<String, String> map = new HashMap<String, String>();
                Element element = elementlist.next();
                String type = element.elementTextTrim("type");// 限制类型
                String content = element.elementTextTrim("content");// 内容
                map.put("type", type);
                map.put("content", content);
                list.add(map);
            }
        }
        catch (Exception e) {
            logger.error(e);
        }
        return list;
    }

    /**
     * 作者：邹远超 日期：2014-4-16 说明：验证舱位接口 04 出发城市+到达城市+日期+“,”+舱位+“,”+ 航班号+
     * “/”+航空公司，如有多个同时验证，中间用“;”分割，最后为冒号“：”分割
     * 末尾取查询结果中限制条件首位，举例：3_CXPEKI00085_423567_0
     * 
     * @return
     */
    public List<AirSegementBean> checkspace(String urlstr) {
        AirSegementBean airsegement = new AirSegementBean();
        List<AirSegementBean> AirSegementBeans = new ArrayList<AirSegementBean>();
        try {
            String resultStr = SendPostandGet.submitPost(urlstr, "").toString();
            // System.out.println(resultStr);
            WriteLog.write("checkspace", resultStr);
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            String Status = root.elementTextTrim("Status");// 状态 0 代表验证成功
            if (Status.trim().equals("0")) {
                Iterator<Element> FlightArr = root.elementIterator("FlightArr");
                while (FlightArr.hasNext()) {
                    AirSegementBean airsegementbean = new AirSegementBean();
                    Element element = FlightArr.next();
                    String NO = element.attributeValue("NO");

                    String FlightNumber = element.elementTextTrim("FlightNumber");// 航班号
                    airsegementbean.setAflightNumber(FlightNumber);
                    String CW = element.elementTextTrim("CW");// 舱位
                    airsegementbean.setAseatType(CW);
                    String UpdateTime = element.elementTextTrim("UpdateTime");// 更新时间
                    airsegementbean.setUpdatetime(UpdateTime);
                    AirSegementBeans.add(airsegementbean);
                }
                return AirSegementBeans;
            }
            else if (Status.trim().equals("1")) {
                airsegement.setRemark("验证超时");
            }
            else if (Status.trim().equals("2")) {
                airsegement.setRemark("有无效航班号");
            }
            else if (Status.trim().equals("3")) {
                airsegement.setRemark("有无位");
            }
            else if (Status.trim().equals("4")) {
                airsegement.setRemark("没有这样的价格");
            }
            AirSegementBeans.add(airsegement);
            return AirSegementBeans;
        }
        catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * 作者：邹远超 日期：2014-4-15 说明：查询票面价接口05
     * 
     * @param airSegements
     * @param route
     * @param url
     * @return
     */
    public AllRouteBean searchInterTicketPrice(List<AirSegementBean> airSegements, RouteBean route, String url) {
        AllRouteBean allRouteBean = new AllRouteBean();
        try {
            String stb = url;
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<OTA_PNRRequest>");
            str.append("<FlightInfo>");
            for (AirSegementBean segment : airSegements) {
                str.append("<FlightSegment>");
                str.append("<FromCity>" + segment.getAfromCity() + "</FromCity>");
                str.append("<DestCity>" + segment.getAtoCity() + "</DestCity>");
                str.append("<AirCo>" + segment.getAairCo() + "</AirCo>");
                str.append("<FlightNumer>" + segment.getAflightNumber() + "</FlightNumer>");
                str.append("<Class>" + segment.getAflightNumber() + "</Class>");
                str.append("<QueryDate>" + segment.getAflightNumber() + "</QueryDate>");
                str.append("<FromDate>" + segment.getAfromDate() + "</FromDate>");
                str.append("<DestDate>" + segment.getAtoDate() + "</DestDate>");
                str.append("<FromTime>" + segment.getAfromTime() + "</FromTime>");
                str.append("<DestTime>" + segment.getAtoTime() + "</DestTime>");
                str.append("</FlightSegment>");
            }
            str.append("</FlightInfo>");
            str.append("<Fare>");
            str.append("<IsOnLine>" + 1 + "</IsOnLine>");// 是否线上，线上需要用1
            String limit = route.getRlimitterm();
            String FareNo = "";
            String priceNo = "";
            limit = limit.substring(limit.indexOf("_") + 1, limit.lastIndexOf("_"));
            FareNo = limit.substring(0, limit.lastIndexOf("_"));
            priceNo = limit.substring(limit.indexOf("_") + 1, limit.length());
            str.append("<FareNo>" + FareNo + "</FareNo>");// /文件编号
            str.append("<PriceNo>" + priceNo + "</PriceNo>");// 价格编号
            // --文件编号，价格编号取自查询结果中限制条件值3_CXPEKI00085_423567_0
            str.append("<TaxStr>" + 0 + "</TaxStr>");// PEKFRA2009-08-20,M,LH721/LH;FRACDG2009-08-20,M,LH4224/LH
            str.append("<IsRT>" + 0 + "</IsRT>");// 是否是RT，查询价格信息中有返回
            str.append("<IsQModel>" + route.getRqvalue() + "</IsQModel>");// 表示单独显示税
            // Q值
            str.append("<GDSUser>abc;1234</GDSUser>");// Q税使用的用户名和密码
            str.append("<IsChildQTax>" + 0 + "</IsChildQTax>");
            str.append("<IsSTUTax>" + 0 + "</IsSTUTax>");
            str.append("<IsDebugTax>" + 0 + "</IsDebugTax>");
            str.append("<QTEAirCo>" + route.getRairCo() + "</QTEAirCo>");// 该价格的主要航空公司
            str.append("</Fare>");
            str.append("</OTA_PNRRequest>");
            stb += str.toString();
            String resultStr = SendPostandGet.submitPost(stb, "").toString();
            WriteLog.write("TicketPrice", resultStr);
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            RouteBean routebean = new RouteBean();
            String SalePrice = root.elementTextTrim("SalePrice");// 票面销售价
            routebean.setSalePrice(Float.valueOf(SalePrice));
            String BasePrice = root.elementTextTrim("BasePrice");// 票面销售底价
            routebean.setBasePrice(Float.valueOf(BasePrice));
            String rnewPrice = root.elementTextTrim("PriceIncludeCommision");// 含代理费票面价格
            routebean.setRnewPrice(Float.valueOf(rnewPrice));
            String Tax = root.elementTextTrim("Tax");// 成人税金
            routebean.setRtotalTax(Float.valueOf(Tax));
            String Qvalue = root.elementTextTrim("Qvalue");// Qvalue Q值
            // 传入<IsQModel>1</IsQModel>=1时Q值分开显示
            routebean.setRqvalue(Integer.valueOf(Qvalue));
            String IsQModel = root.elementTextTrim("IsQModel");
            routebean.setIsQvalue(Integer.valueOf(IsQModel));
        }
        catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：PNR及订单生成接口06
     * 
     * @param airSegements
     * @param fguest
     * @param routebean
     * @param url
     * @return
     */
    public Forderinfo createOrderOrbyPNR(List<AirSegementBean> airSegements, Fguest fguest, RouteBean routebean,
            String url) {
        Forderinfo forderinfo = new Forderinfo();
        try {
            String substr = url;
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<OTA_PNRRequest>");
            str.append("<FlightInfo>");
            for (AirSegementBean segment : airSegements) {
                str.append("<FlightSegment>");
                str.append("<cwtype>" + segment.getAseatType() + "</cwtype>");
                str.append("<FromCity>" + segment.getAfromCity() + "</FromCity>");
                str.append("<DestCity>" + segment.getAtoCity() + "</DestCity>");
                str.append("<AirCo>" + segment.getAairCo() + "</AirCo>");
                str.append("<FlightNumer>" + segment.getAflightNumber() + "</FlightNumer>");
                str.append("<Class>" + segment.getAflightNumber() + "</Class>");
                str.append("<QueryDate>" + segment.getAflightNumber() + "</QueryDate>");
                str.append("<FromDate>" + segment.getAfromDate() + "</FromDate>");
                str.append("<DestDate>" + segment.getAtoDate() + "</DestDate>");
                str.append("<FromTime>" + segment.getAfromTime() + "</FromTime>");
                str.append("<DestTime>" + segment.getAtoTime() + "</DestTime>");
                str.append("</FlightSegment>");
            }
            str.append("</FlightInfo>");
            str.append("<Passenger>");
            str.append("<PassengerDetail>");
            str.append("<SurName>" + fguest.getGuestname() + "</SurName>");// 旅客姓名
            str.append("<CerType>" + 1 + "</CerType>");// 证件类型
            str.append("<CerCountry>" + 1 + "</CerCountry>");// 证件国家
            str.append("<CerNo>" + fguest.getGuestidno() + "</CerNo>");// 证件号码
            str.append("<Nationality>" + fguest.getCountrycode() + "</Nationality>");// 国籍
            str.append("<BirthDay>" + fguest.getBirthday() + "</BirthDay>");// 生日
            str.append("<Sex>" + fguest.getGender() + "</Sex>");// 性别
            str.append("<CerValidity>" + 1 + "</SexCerValidity>");// 证件有效期
            str.append("<PassengerType>" + fguest.getGuesttype() + "</PassengerType>");// 旅客类型
            str.append("</PassengerDetail>");
            str.append("</PassengerDetail>");
            str.append("</Passenger>");
            str.append("<Fare>");
            str.append("<TotalPrice>" + routebean.getRtotalFare() + "</TotalPrice>");// 总价
            str.append("<NetFare>" + routebean.getTotalPriceUnit() + "</NetFare>");// 净价
            str.append("<AdtPrice>" + routebean.getRnewPrice() + "</AdtPrice>");// 成人价
            str.append("<ChdPrice>" + routebean.getRcpmoney() + "</ChdPrice>");// 儿童价
            str.append("<FareNo>" + routebean.getRfareNo() + "</FareNo>");// 文件编号
            str.append("<Tax>" + routebean.getRtotalTax() + "</Tax>");// 税
            str.append("<TaxStr>" + 0 + "</TaxStr>");// 税字符串
            str.append("</Fare>");
            str.append("<OP>");
            str.append("<policyID>" + 0 + "</policyID>");// 政策ID
            str.append("<policyID>" + 0 + "</policyID>");// 航程简介
            str.append("<limit>" + routebean.getRlimitterm() + "</limit>");// 限制条件
            if (routebean.getRflightcount() > 0) {
                str.append("<returnTicket>" + 1 + "</returnTicket>");// 是否往返
            }
            else {
                str.append("<returnTicket>" + 0 + "</returnTicket>");// 是否往返
            }
            str.append("<allp>" + 0 + "</allp>");// 公布运价
            str.append("<dlper>" + routebean.getRcompanyagprice() + "</dlper>");// 代理费
            str.append("</OP>");
            str.append("</OTA_PNRRequest>");
            substr += str.toString();
            String resultStr = SendPostandGet.submitPost(substr, "").toString();
            WriteLog.write("creatorderpiao59", resultStr);
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            String pnr = root.elementTextTrim("pnr");
            String orderForm = root.elementTextTrim("orderForm");
            forderinfo.setPrncode(pnr);
            forderinfo.setOrdernumber(orderForm);
        }
        catch (Exception e) {
            logger.error(e);
        }
        return forderinfo;
    }

    /**
     * 作者：邹远超 日期：2014-4-17 说明：根据PNR生成价格07
     * 
     * @param url
     * @return
     */
    public AllRouteBean LicreatePricebyPNR(String url) {

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        String resultStr = SendPostandGet.submitPost(url, "").toString();
        WriteLog.write("refundAndapplychange", resultStr);
        try {
            Document document = DocumentHelper.parseText(resultStr);
            // System.out.println(resultStr);
            Element root = document.getRootElement();
            Element element = root.element("Fare_Tax");
            RouteBean route = new RouteBean();
            Map<String, String> map = new HashMap<String, String>();
            String FareNo = element.elementTextTrim("FareNo");// 文件编号
            route.setRfareNo(FareNo);
            String PriceNo = element.elementTextTrim("PriceNo");// 价格编号
            route.setRpriceNo(PriceNo);
            String StandPrice = element.elementTextTrim("StandPrice");// 是否成功
            route.setRstandPrice(Integer.valueOf(StandPrice));
            String Fare = element.elementTextTrim("Fare");// 费用
            route.setRnewPrice(Float.valueOf(Fare));
            String Tax = element.elementTextTrim("Tax");// 税
            route.setTaxes(Integer.valueOf(Tax));
            String QValue = element.elementTextTrim("QValue");// Q值
            route.setRqvalue(Integer.valueOf(QValue));
            String QCHDValue = element.elementTextTrim("QCHDValue");// 儿童Q值
            route.setRchildqvalue(Integer.valueOf(QCHDValue));
            String CHDFare = element.elementTextTrim("CHDFare");// 儿童票价
            route.setRchildMoney(Float.valueOf(CHDFare));
            String CHDFareIncludeCommision = element.elementTextTrim("CHDFareIncludeCommision");// 含代理费的儿童价格
            route.setRcpmoney(Float.valueOf(CHDFareIncludeCommision));
            String CHDTax = element.elementTextTrim("CHDTax");// 儿童税金
            route.setTaxes(Integer.valueOf(CHDTax));
            String PriceIncludeCommision = element.elementTextTrim("PriceIncludeCommision");// 含代理费票面价格
            route.setRnewPrice(Float.valueOf(PriceIncludeCommision));
            String ticket = element.elementTextTrim("Ticket");// 客规
            route.setTicket(ticket);
            String passenger = element.elementTextTrim("Passenger");// 乘客姓名
            route.setPassenger(passenger);
            String AirCo = element.elementTextTrim("AirCo");// 主航空公司
            route.setCarrier(AirCo);
            String IsSp = element.elementTextTrim("IsSp");// 是否特价
            route.setCarrier(IsSp);
            String spNo = element.elementTextTrim("SpNo");// 特价编号
            route.setSpNo(spNo);

            Iterator<Element> elementlist = root.elementIterator("FlightInfo");
            while (elementlist.hasNext()) {
                FlightinfoBean flightinfobean = new FlightinfoBean();
                Element felement = elementlist.next();
                String FlightSegment = element.elementTextTrim("FlightSegment");// 第一段航班信息
                String number = element.attributeValue("number");
                String FromCity = element.elementTextTrim("FromCity");// 出发城市
                flightinfobean.setFfromctiy(FromCity);
                String DestCity = element.elementTextTrim("DestCity");// 到达城市
                flightinfobean.setFtocity(DestCity);
                String FromDate = element.elementTextTrim("FromDate");// 出发日期
                flightinfobean.setFdepartureDate(FromDate);
                String FromTime = element.elementTextTrim("FromTime");// 出发时间
                flightinfobean.setDepartureTime(FromTime);
                String DestTime = element.elementTextTrim("DestTime");// 到达时间
                flightinfobean.setDepartureTime(DestTime);
                String FlightNumber = element.elementTextTrim("FlightNumber");// 航班号
                flightinfobean.setFlightNumber(FlightNumber);
                String cw = element.elementTextTrim("CW");// 舱位
                flightinfobean.setFspacere(cw);
                String IsCodeShare = element.elementTextTrim("IsCodeShare");// 是否代码共享航班
                flightinfobean.setFisenjoyflight(Integer.valueOf(IsCodeShare));
            }

            list.add(map);
        }
        catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：订单详情查询08
     * 
     * @param url
     * @return
     */
    public Forderinfo selectOrderDetail(String url) {
        Forderinfo forderinfo = new Forderinfo();
        String resultStr = SendPostandGet.submitPost(url, "").toString();
        // System.out.println(resultStr);
        // WriteLog.write("orderdetail", resultStr);
        try {
            Document document = DocumentHelper.parseText(resultStr);
            Element root = document.getRootElement();
            Element element = root.element("OP");
            String orderForm = element.elementTextTrim("ordersList");// 订单号
            forderinfo.setOrdernumber(orderForm);
            String pnrcode = element.elementTextTrim("pnr");// PNR
            forderinfo.setPrncode(pnrcode);
            String allp = element.elementTextTrim("allp");// 公布运价
            forderinfo.setAllp(Float.valueOf(allp));
            String dlper = element.elementTextTrim("dlper");// 代理费
            forderinfo.setDlper(Float.valueOf(dlper));
            String addm = element.elementTextTrim("addm");// 出票费
            forderinfo.setAddm(Float.valueOf(addm));
            String price = element.elementTextTrim("price");// 票价成人价
            forderinfo.setPrice(Float.valueOf(price));
            String per = element.elementTextTrim("per");// 返点
            forderinfo.setPer(per);
            String buy_num = element.elementTextTrim("buy_num");// 出票张数
            forderinfo.setBuynum(Integer.valueOf((buy_num)));
            String ptotal = element.elementTextTrim("ptotal");// 总价格
            forderinfo.setTotalticketfare(Double.valueOf(ptotal));
            String limit = element.elementTextTrim("limit");// 限制条件
            forderinfo.setLimit(limit);
            String status = element.elementTextTrim("status");// 订单状态
            forderinfo.setOrderstatus(Integer.valueOf(status));

            Iterator<Element> elementlist = root.elementIterator("FlightInfo");// 航段信息
            while (elementlist.hasNext()) {
                FlightinfoBean flightinfo = new FlightinfoBean();
                Element felement = elementlist.next();
                Element FlightSegment = felement.element("FlightSegment");// 第一段航班信息
                String fromcity = FlightSegment.elementTextTrim("fromcity");// 出发城市
                flightinfo.setFfromctiy(fromcity);
                String destcity = FlightSegment.elementTextTrim("destcity");// 返回城市
                flightinfo.setFtocity(destcity);
                String startdate = FlightSegment.elementTextTrim("startdate");// 出发日期
                flightinfo.setFdepartureDate(startdate);
                String enddate = FlightSegment.elementTextTrim("enddate");// 返回日期
                flightinfo.setFarrivalDate(enddate);
                String company = FlightSegment.elementTextTrim("company");// 航空公司
                flightinfo.setCarrier(company);
                String cw_type = FlightSegment.elementTextTrim("cw_type");// 舱位
                flightinfo.setFspacere(cw_type);
                String hb_num = FlightSegment.elementTextTrim("hb_num");// 航班号
                flightinfo.setFlightNumber(hb_num);
            }
            Iterator<Element> plist = root.elementIterator("Passengerdetail");// 乘客信息
            while (plist.hasNext()) {
                Fguest fguest = new Fguest();
                Element pelement = elementlist.next();
                Element passengerment = pelement.element("passenger");// 第一个乘客人信息
                String nationality = passengerment.elementTextTrim("nationality");// 国籍
                fguest.setNationality(nationality);
                String credtype = passengerment.elementTextTrim("credtype");// 证件类型
                fguest.setCredtype(credtype);
                String crednum = passengerment.elementTextTrim("crednum");// 证件号码
                fguest.setGuestidno(crednum);
                String credissue = passengerment.elementTextTrim("credissue");// 证件发布国家
                fguest.setCountrycode(credissue);
                String birthday = passengerment.elementTextTrim("birthday");// 生日
                fguest.setBirthday(Timestamp.valueOf(birthday));
                String credpriod = passengerment.elementTextTrim("credpriod");// 证件有效期至
                fguest.setCredpriod(credpriod);
                String surname = passengerment.elementTextTrim("surname");// 乘客人姓名
                fguest.setGuestname(surname);
                String passengerType = passengerment.elementTextTrim("passengerType");// 乘客类型
                fguest.setGuesttype(Integer.valueOf(passengerType));
                String sex = passengerment.elementTextTrim("sex");// 乘客性别
                if (sex.equals("M")) {
                    fguest.setGender(0);
                }
                else {
                    fguest.setGender(1);
                }
            }
        }
        catch (Exception e) {
            logger.equals(e);
        }
        return forderinfo;
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：订单状态查询09
     * 
     * @param url
     * @return
     */
    public String selectOrderStatus(String url) {
        String resultStr = SendPostandGet.submitPost(url, "").toString();
        WriteLog.write("selectOrderStatus", resultStr);
        try {
            Document document = DocumentHelper.parseText(resultStr);
            String orderForm = document.getStringValue();
            // System.out.println(orderForm);
        }
        catch (Exception e) {
            logger.equals(e);
        }
        return "";
    }

    /**
     * 作者：邹远超 日期：2014-4-21 说明：取消订单10
     * 
     * @param url
     * @return
     */
    public String cancelOrder(String url) {
        String resultStr = SendPostandGet.submitPost(url, "").toString();
        // System.out.println(resultStr);
        WriteLog.write("cancelOrder", resultStr);
        return "resultStr";
    }
}
