package com.ccservice.b2b2c.policy;

import java.io.StringReader;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.tree.DefaultText;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import client.JinRiAutoPayServerStub;
import client.JinRiFlightServerStub;
//import client.JinRiOrderServerStub;
//import client.JinRiOrderServerStub.CancelOrder;
//import client.JinRiOrderServerStub.CancelOrderResponse;
//import client.JinRiOrderServerStub.GaiqianOrder;
//import client.JinRiOrderServerStub.GaiqianOrderResponse;
//import client.JinRiOrderServerStub.GetGaiqianOrder;
//import client.JinRiOrderServerStub.GetGaiqianOrderResponse;
//import client.JinRiOrderServerStub.GetRaiseOrder;
//import client.JinRiOrderServerStub.GetRaiseOrderResponse;
//import client.JinRiOrderServerStub.RiseCabin;
//import client.JinRiOrderServerStub.RiseCabinResponse;
//import client.JinRiOrderServerStub.TuiFeiOrder;
//import client.JinRiOrderServerStub.TuiFeiOrderResponse;
import client.JinRiPayServerStub;
import client.JinRiRateServerStub;
import cn.jinri.policy.JinRiOrderServerStub;
import cn.jinri.policy.JinRiOrderServerStub.CancelOrderResponse;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.cityairport.Cityairport;
import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.b2b2c.policy.util.AirUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.test.HttpClient;

public class JinriMethod extends SupplyMethod {
    Log logger = LogFactory.getLog(this.getClass());

    final private static JinRiBook jinRiBook = new JinRiBook();

    final private static String JRURL = jinRiBook.getJRURL();

    final private static String JRUSER = jinRiBook.getJRUSER();

    final private static String JRUSER_xiaji = jinRiBook.getJRUSER_xiaji();

    final private static String JRKey = jinRiBook.getJRKey();

    /**
     * 
     * 查找今日订单详情
     * @param orderno
     * @return
     * @time 2014年8月30日 上午10:51:08
     * @author chendong
     */
    public String GetJinRiOrderTickteNoByOrderNo(String orderno) {
        String result = "";
        String ticktNO = "";
        String passname = "";
        if (!jinRiBook.getStaus().equals("0")) {
            try {
                JinRiOrderServerStub stub = new JinRiOrderServerStub();
                JinRiOrderServerStub.GetOrderInfo getOrderInfo = new JinRiOrderServerStub.GetOrderInfo();
                StringBuffer str = new StringBuffer();
                str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
                str.append("<JIT-Order-Request>");
                str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" orderno=\"" + orderno + "\" />");
                str.append("</JIT-Order-Request>");
                WriteLog.write("今日", "查找详细订单信息:0:" + str.toString());
                //                getOrderInfo.setData(str.toString());
                getOrderInfo.setXml(str.toString());

                JinRiOrderServerStub.GetOrderInfoResponse response = stub.getOrderInfo(getOrderInfo);
                String resultXML = response.getGetOrderInfoResult();
                WriteLog.write("今日", "查找详细订单信息:1:" + resultXML);
                org.dom4j.Document document = DocumentHelper.parseText(resultXML);
                org.dom4j.Element root = document.getRootElement();
                List list = root.elements("Response");
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                    if (!elmt.elementText("TicketNo").isEmpty()) {
                        ticktNO = elmt.elementText("TicketNo");
                    }
                    if (!elmt.elementText("PName").isEmpty()) {
                        passname = elmt.elementText("PName");
                    }
                }
                result = passname + "^" + ticktNO;
            }
            catch (AxisFault e1) {
                e1.printStackTrace();
                return "-1";
            }
            catch (RemoteException e) {
                e.printStackTrace();
                return "-1";
            }
            catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        else {
            return "-1";
        }
        return result;
    }

    /**
     * 根据订单号获得要支付的价格
     * 
     * @param orderno
     * @return
     */
    public float GetJinRiOrderPriceByOrderNo(String orderno) {
        float result = 0f;
        if (!jinRiBook.getStaus().equals("0")) {
            try {
                JinRiOrderServerStub stub = new JinRiOrderServerStub();
                JinRiOrderServerStub.GetOrderInfo getOrderInfo = new JinRiOrderServerStub.GetOrderInfo();
                StringBuffer str = new StringBuffer();
                str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
                str.append("<JIT-Order-Request>");
                str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" orderno=\"" + orderno + "\" />");
                str.append("</JIT-Order-Request>");
                WriteLog.write("今日", "查找价格0:" + str.toString());
                //                getOrderInfo.setData(str.toString());
                getOrderInfo.setXml(str.toString());
                JinRiOrderServerStub.GetOrderInfoResponse response = stub.getOrderInfo(getOrderInfo);
                String resultXML = response.getGetOrderInfoResult();
                System.out.println(resultXML);
                WriteLog.write("今日", "查找价格1:" + resultXML);
                org.dom4j.Document document = DocumentHelper.parseText(resultXML);
                org.dom4j.Element root = document.getRootElement();
                List list = root.elements("Response");
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                    if (!elmt.elementText("PayMoeny").isEmpty()) {
                        String PayMoeny = elmt.elementText("PayMoeny");
                        result = Float.parseFloat(PayMoeny);
                    }
                }
            }
            catch (AxisFault e1) {
                e1.printStackTrace();
            }
            catch (RemoteException e) {
                e.printStackTrace();
            }
            catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        else {
            return result;
        }
        return result;
    }

    public String GetJinRiOrderInfoByOrderNo(String orderno) {

        if (!jinRiBook.getStaus().equals("0")) {
            String orderstaus = "";// 订单状态
            String orderprice = "";// 订单价格
            String ticktno = "";// 票号
            String passname = "";// 乘机人
            try {
                String url = JRURL + "service=Order_QueryOrderInfo&partner=" + JRUSER + "&orderNo=" + orderno
                        + "&sign=";

                String[] Sortedstr = new String[] { "service=Order_QueryOrderInfo", "partner=" + JRUSER,
                        "orderNo=" + orderno };
                url += HttpClient.GetSign(Sortedstr);

                System.out.println("url==" + url);
                String re = HttpClient.httpget(url, "GB2312");

                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(new StringReader(re.trim()));
                Element root = doc.getRootElement();
                if (root.getChild("is_success").getTextTrim().equals("T")) {

                    Element eresponse = root.getChild("response");
                    if (eresponse != null) {

                        Element items = eresponse.getChild("OrderInformation");
                        Element itemsOrderNo = items.getChild("OrderItem");
                        orderstaus = itemsOrderNo.getChildTextTrim("OrderStatus");
                        System.out.println("订单状态==" + orderstaus);
                        System.out.println("ParValue==" + itemsOrderNo.getChildTextTrim("ParValue"));
                        orderprice = itemsOrderNo.getChildTextTrim("ParValue");
                        // 乘机人解析

                        Element itemspass = items.getChild("PassengerItems");
                        List<Element> listpass = itemspass.getChildren("PassengerItem");
                        System.out.println(listpass.size());
                        if (listpass.size() > 0) {
                            for (int p = 0; p < listpass.size(); p++) {
                                Element itemspas = listpass.get(p);
                                String ticktNo = itemspas.getChildTextTrim("TicketNo");
                                String PassName = itemspas.getChildTextTrim("TicketNo");
                                System.out.println("ticktNo==" + ticktNo);
                                ticktno += ticktNo + "|";
                                passname += PassName + "|";
                            }

                        }

                    }
                    else {

                        System.out.println("今日查询外部订单信息接口出错,eresponse==null");
                        return "-1";
                    }

                }
                else {

                    System.out.println("今日查询外部订单信息接口出错");
                    return "-1";
                }
                System.out.println("ticktno==" + ticktno);
                System.out.println("passname==" + passname);

                System.out.println(passname + "@" + ticktno + "@" + orderprice + "@" + GetJinRiOrderStaus(orderstaus));
                return passname + "@" + ticktno + "@" + orderprice + "@" + GetJinRiOrderStaus(orderstaus);
            }
            catch (Exception e) {
                e.printStackTrace();
                return "-1";
            }

        }
        else {

            System.out.println("今日接口被禁用,请联系管理员");
            return "-1";
        }

    }

    public static String GetJinRiOrderStaus(String staus) {
        if (staus.equals("-6")) {

            return "审核失败";
        }
        if (staus.equals("-5")) {

            return "等待审核";
        }
        if (staus.equals("-4")) {

            return "订位失败";
        }
        if (staus.equals("-3")) {

            return "等待订位";
        }

        if (staus.equals("-2")) {

            return "特价审核";
        }
        if (staus.equals("-1")) {

            return "关闭订单";
        }
        if (staus.equals("0")) {

            return "等待支付";
        }
        if (staus.equals("1")) {

            return "支付完成";
        }

        if (staus.equals("2")) {

            return "出票完成";
        }
        if (staus.equals("3")) {

            return "申请退款";
        }
        if (staus.equals("4")) {

            return "退款成功";
        }

        if (staus.equals("5")) {

            return "暂不能出票";
        }

        if (staus.equals("6")) {

            return "暂不能退废";
        }
        if (staus.equals("7")) {

            return "航班延误";
        }
        if (staus.equals("8")) {

            return "航班取消";
        }
        return "";
    }

    /**
     * 根据航线匹配最优政策
     * 
     * @author 陈栋 2012-5-3 15:02:59
     * @param sinfo
     *            行程表
     * @return bestZrate
     */
    public List<Zrate> getBestZrate(Segmentinfo sinfo) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        if (jinRiBook.getStaus().equals("1")) {
            try {
                JinRiRateServerStub stub = new JinRiRateServerStub();
                JinRiRateServerStub.GetRateList rateList = new JinRiRateServerStub.GetRateList();
                StringBuffer str = new StringBuffer();
                str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
                str.append("<JIT-Policy-Request>");
                str.append("<Request username=\"" + jinRiBook.getJRUSER() + "\" scity=\"" + sinfo.getStartairport()
                        + "\" ecity=\"" + sinfo.getEndairport() + "\" date=\""
                        + sinfo.getDeparttime().toString().substring(0, 10) + "\" aircome=\""
                        + sinfo.getAircomapnycode() + "\" vayagetype=\"0\" amount=\"10\" cabin=\""
                        + sinfo.getCabincode() + "\" ");
                if (sinfo.getCabincode().equals("X")) {
                    str.append("rateway='1' ");
                }
                str.append("/>");
                str.append("</JIT-Policy-Request>");
                rateList.setData(str.toString());
                WriteLog.write("getzratebypnr_今日", "根据航线获取最优政策返回的信息:" + str.toString());
                JinRiRateServerStub.GetRateListResponse response = stub.getRateList(rateList);
                String result = response.getGetRateListResult();
                System.out.println(result);
                WriteLog.write("getzratebypnr_今日", "根据航线获取最优政策返回的信息:" + result);
                if (result.length() < 10) {
                    return zrates;
                }
                org.dom4j.Document document = DocumentHelper.parseText(result);
                org.dom4j.Element root = document.getRootElement();
                if (root.elements("Response").size() > 0) {
                    List<org.dom4j.Element> list = root.elements("Response");
                    // if (list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {
                        Zrate zrate = new Zrate();
                        org.dom4j.Element e = list.get(a);
                        zrate.setAgentid(6L);
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setCreateuser("job");
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifyuser("job");
                        zrate.setTickettype(1);
                        zrate.setIsenable(1);
                        if (e.attributeValue("PolicyId") != null) {
                            zrate.setOutid(e.attributeValue("PolicyId"));
                        }
                        if (e.attributeValue("RateId") != null) {
                            zrate.setAgentcode(e.attributeValue("RateId"));
                        }
                        if (e.attributeValue("ScityE") != null) {
                            zrate.setDepartureport(e.attributeValue("ScityE"));
                        }
                        if (e.attributeValue("EcityE") != null) {
                            zrate.setArrivalport(e.attributeValue("EcityE"));
                        }
                        if (e.attributeValue("AirComE") != null) {
                            if (e.attributeValue("AirComE").length() > 2) {
                                zrate.setAircompanycode(e.attributeValue("AirComE").substring(0, 2));
                                zrate.setFlightnumber(e.attributeValue("AirComE"));
                            }
                            else {
                                zrate.setAircompanycode(e.attributeValue("AirComE"));
                            }
                        }
                        if (e.attributeValue("NoAirComE") != null) {
                            zrate.setWeeknum(e.attributeValue("NoAirComE"));
                        }
                        if (e.attributeValue("WorkTimeBegin") != null) {
                            zrate.setWorktime(e.attributeValue("WorkTimeBegin"));
                        }
                        if (e.attributeValue("WorkTimeEnd") != null) {
                            zrate.setAfterworktime(e.attributeValue("WorkTimeEnd"));
                        }
                        if (e.attributeValue("SP") != null) {
                            zrate.setSpeed(e.attributeValue("SP"));
                        }

                        if (e.attributeValue("Cabin") != null) {
                            zrate.setCabincode(e.attributeValue("Cabin"));
                        }
                        if (e.attributeValue("VoyageType") != null) {
                            if (e.attributeValue("VoyageType").equals("0")) {
                                zrate.setVoyagetype("1");
                            }
                            else if (e.attributeValue("VoyageType").equals("1")) {
                                zrate.setVoyagetype("2");
                            }
                            else {
                                zrate.setVoyagetype("3");
                            }
                        }
                        String UserType = e.attributeValue("UserType");
                        if (e.attributeValue("RateType") != null) {
                            String rateType = e.attributeValue("RateType");
                            if (rateType.equals("0")) {
                                zrate.setGeneral(1L);
                            }
                            else {
                                zrate.setGeneral(2L);
                            }
                        }
                        if (e.attributeValue("Discounts") != null) {
                            zrate.setRatevalue(Float.parseFloat(e.attributeValue("Discounts")));
                        }
                        if (e.attributeValue("Sdate") != null) {
                            zrate.setBegindate(dateToTimestamp(e.attributeValue("Sdate")));
                        }
                        if (e.attributeValue("Edate") != null) {
                            zrate.setEnddate(dateToTimestamp(e.attributeValue("Edate")));
                        }
                        if (e.attributeValue("WorkTimBegin") != null) {
                            zrate.setWorktime(e.attributeValue("WorkTimBegin"));
                        }
                        if (e.attributeValue("WorkTImeEnd") != null) {
                            zrate.setAfterworktime(e.attributeValue("WorkTImeEnd"));
                        }
                        String Rewards = e.attributeValue("Rewards");
                        if (e.attributeValue("Remark") != null) {
                            zrate.setRemark(e.attributeValue("Remark"));
                        }
                        if (zrate.getAfterworktime() != null && !zrate.getAfterworktime().equals("")) {
                            if (zrate.getFlightnumber() != null) {
                                if (zrate.getFlightnumber().indexOf(sinfo.getFlightnumber()) > 0) {
                                    zrates.add(zrate);
                                }
                            }
                            else if (zrate.getWeeknum() != null) {
                                if (zrate.getWeeknum().indexOf(sinfo.getFlightnumber()) < 0) {
                                    zrates.add(zrate);
                                }
                            }
                            else {
                                zrates.add(zrate);
                            }
                        }
                    }
                }
            }
            catch (AxisFault e) {
                e.printStackTrace();
            }
            catch (RemoteException e) {
                e.printStackTrace();
            }
            catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("--------今日被禁用-----------------");
        }
        return zrates;
    }

    /**
     * 根据PNR获取最优政策
     * 
     * @author 陈栋 2012-5-3 21:14:06
     * @param pnr
     * @return bestZrate
     */
    public List<Zrate> getBestZrateByPnr(Orderinfo order, List<Passenger> listPassenger, Segmentinfo sinfo) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        //因为今日<Error ErrorCode="1545" ErrorType="0" ErrorInfo="不支持预订南航Z舱"/>
        //所以就不取这样的今日的政策了
        if (sinfo.getAircomapnycode().toUpperCase().indexOf("CZ") >= 0
                && "Z".equals(sinfo.getCabincode().toUpperCase())) {
            return zrates;
        }
        String names = "";
        try {
            for (int i = 0; i < listPassenger.size(); i++) {
                names += listPassenger.get(i).getName() + "@";
            }
            // O|P|HDY4E6^F^MT0PX8|2010-12-07|HAK|海口|CKG|重庆|PN6208^N||17:20|19:20|U|150||540.00|120.00|1|蒋海@
            // 固定值|固定值|PNR^固定值^大编|出发日期（不带时间）|出发城市三字码|
            // 出发城市|到达城市三字码|到达城市|航班号^固定值||起飞时间|到达时间|
            // 舱位|折扣||票面价|机建燃油|乘客人数|乘客名字@乘客名字@
            String PnrInfo = "O|P|" + order.getPnr() + "^F^" + order.getBigpnr() + "|"
                    + sinfo.getDeparttime().toString().substring(0, 10) + "|" + sinfo.getStartairport() + "|"
                    + getCityNamebyCode(sinfo.getStartairport()) + "|" + sinfo.getEndairport() + "|"
                    + getCityNamebyCode(sinfo.getEndairport()) + "|" + sinfo.getFlightnumber() + "^N||"
                    + sinfo.getDeparttime().toString().substring(11, 16) + "|"
                    + sinfo.getArrivaltime().toString().substring(11, 16) + "|" + sinfo.getCabincode() + "|"
                    + sinfo.getDiscount() + "||" + sinfo.getParvalue() + "|"
                    + (sinfo.getAirportfee() + sinfo.getFuelfee()) + "|" + listPassenger.size() + "|" + names;
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Policy-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRUSER() + "\" pnr=\"" + order.getPnr() + "\" PnrInfo=\""
                    + PnrInfo + "\"/>");
            str.append("</JIT-Policy-Request>");
            WriteLog.write("今日", "PNR最优政策0:" + str.toString());

            JinRiRateServerStub stub = new JinRiRateServerStub();
            JinRiRateServerStub.GetRateListByPNR getRateListByPNR = new JinRiRateServerStub.GetRateListByPNR();
            getRateListByPNR.setData(str.toString());
            JinRiRateServerStub.GetRateListByPNRResponse response = stub.getRateListByPNR(getRateListByPNR);
            String result = response.getGetRateListByPNRResult();
            WriteLog.write("今日", "PNR最优政策1:" + result);
            if (result.length() == 4) {
                return zrates;
            }
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                if (list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {
                        Zrate zrate = new Zrate();
                        org.dom4j.Element e = list.get(a);
                        zrate.setAgentid(6L);
                        zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                        zrate.setCreateuser("job");
                        zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                        zrate.setModifyuser("job");
                        zrate.setIsenable(1);
                        if (e.attributeValue("PolicyId") != null) {
                            zrate.setOutid(e.attributeValue("PolicyId"));
                        }
                        if (e.attributeValue("RateId") != null) {
                            zrate.setAgentcode(e.attributeValue("RateId"));
                        }
                        if (e.attributeValue("ScityE") != null) {
                            zrate.setDepartureport(e.attributeValue("ScityE"));
                        }
                        if (e.attributeValue("EcityE") != null) {
                            zrate.setArrivalport(e.attributeValue("EcityE"));
                        }
                        if (e.attributeValue("AirComE") != null) {
                            if (e.attributeValue("AirComE").length() > 2) {
                                zrate.setAircompanycode(e.attributeValue("AirComE").substring(0, 2));
                                zrate.setFlightnumber(e.attributeValue("AirComE"));
                            }
                            else {
                                zrate.setAircompanycode(e.attributeValue("AirComE"));
                            }
                        }
                        if (e.attributeValue("WorkTimeBegin") != null) {
                            zrate.setWorktime(e.attributeValue("WorkTimeBegin"));
                        }
                        if (e.attributeValue("WorkTimeEnd") != null) {
                            zrate.setAfterworktime(e.attributeValue("WorkTimeEnd"));
                        }
                        if (e.attributeValue("SP") != null) {
                            zrate.setSpeed(e.attributeValue("SP") + "分种");
                        }
                        String NoAirComE = e.attributeValue("NoAirComE");
                        if (e.attributeValue("Cabin") != null) {
                            zrate.setCabincode(e.attributeValue("Cabin"));
                        }
                        if (e.attributeValue("VoyageType") != null) {
                            if (e.attributeValue("VoyageType").equals("0")) {
                                zrate.setVoyagetype("1");
                            }
                            else if (e.attributeValue("VoyageType").equals("1")) {
                                zrate.setVoyagetype("2");
                            }
                            else {
                                zrate.setVoyagetype("3");
                            }
                        }
                        String UserType = e.attributeValue("UserType");
                        if (e.attributeValue("RateType") != null) {
                            String rateType = e.attributeValue("RateType");
                            if (rateType.equals("0")) {
                                zrate.setGeneral(1L);
                            }
                            else {
                                zrate.setGeneral(2L);
                            }
                        }
                        if (e.attributeValue("Discounts") != null) {
                            zrate.setRatevalue(Float.parseFloat(e.attributeValue("Discounts")));
                        }
                        if (e.attributeValue("Sdate") != null) {
                            zrate.setBegindate(dateToTimestamp(e.attributeValue("Sdate")));
                        }
                        if (e.attributeValue("Edate") != null) {
                            zrate.setEnddate(dateToTimestamp(e.attributeValue("Edate")));
                        }
                        if (e.attributeValue("WorkTimBegin") != null) {
                            zrate.setWorktime(e.attributeValue("WorkTimBegin").replaceAll(":", ""));
                        }
                        if (e.attributeValue("WorkTImeEnd") != null) {
                            zrate.setAfterworktime(e.attributeValue("WorkTImeEnd").replaceAll(":", ""));
                        }
                        if (e.attributeValue("Remark") != null) {
                            zrate.setRemark(e.attributeValue("Remark"));
                        }
                        //RefundTimeEnd
                        if (e.attributeValue("RefundTimeEnd") != null) {
                            zrate.setOnetofivewastetime(e.attributeValue("RefundTimeEnd").replaceAll(":", ""));
                            zrate.setWeekendwastetime(e.attributeValue("RefundTimeEnd").replaceAll(":", ""));
                        }
                        if (e.attributeValue("ET") != null) {
                            if ("0".equals(e.attributeValue("ET"))) {
                                zrate.setTickettype(1);
                            }
                            else {
                                zrate.setTickettype(2);
                            }
                        }
                        zrates.add(zrate);
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //        if (zrates.size() > 5) {
        //            zrates = zrates.subList(0, 5);
        //        }
        return zrates;
    }

    /**
     * 今日根据PNR生成订单
     * 
     * @author 陈栋 2012-5-5 16:59:06
     * @param order
     * @param listPassenger
     * @param sinfo
     * @return
     * @throws Exception
     */
    public String createOrderbyPNR(Orderinfo order, List<Passenger> listPassenger, Segmentinfo sinfo) {
        String resultstr = "";
        String OrderNo = "";
        String payurl = "";
        String officeId = "";
        String PayMoney = "";
        try {
            String names = "";
            String IDS = "";
            for (int i = 0; i < listPassenger.size(); i++) {
                Passenger tPassenger = listPassenger.get(i);
                names += tPassenger.getName() + "@";
                IDS += "NI" + tPassenger.getIdnumber() + "@";

            }
            // O|P|HDY4E6^F^MT0PX8|2010-12-07|HAK|海口|CKG|重庆|PN6208^N||17:20|19:20|U|150||540.00|120.00|1|蒋海@
            // 固定值|固定值|PNR^固定值^大编|出发日期（不带时间）|出发城市三字码|
            // 出发城市|到达城市三字码|到达城市|航班号^固定值||起飞时间|到达时间|
            // 舱位|折扣||票面价|机建燃油|乘客人数|乘客名字@乘客名字@
            String PnrInfo = "O|P|" + order.getPnr() + "^F^";
            PnrInfo += getBigPnr(order.getUserRtInfo(), order.getPnr());
            PnrInfo += "|" + sinfo.getDeparttime().toString().substring(0, 10) + "|" + sinfo.getStartairport() + "|"
                    + getCityNamebyCode(sinfo.getStartairport()) + "|" + sinfo.getEndairport() + "|"
                    + getCityNamebyCode(sinfo.getEndairport()) + "|" + sinfo.getFlightnumber() + "^N||"
                    + sinfo.getDeparttime().toString().substring(11, 16) + "|"
                    + sinfo.getArrivaltime().toString().substring(11, 16) + "|" + sinfo.getCabincode() + "|"
                    + sinfo.getDiscount() + "||" + sinfo.getParvalue() + "|"
                    + (sinfo.getAirportfee() + sinfo.getFuelfee()) + "|" + listPassenger.size() + "|" + names + "|"
                    + IDS;

            JinRiOrderServerStub stub = new JinRiOrderServerStub();
            JinRiOrderServerStub.CreateOrderByPNR createOrderByPNR = new JinRiOrderServerStub.CreateOrderByPNR();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-OrderByPNR-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" pnr=\"" + order.getPnr() + "\""
                    + " rateid=\"" + sinfo.getZrate().getAgentcode() + "\" dotnum=\"" + sinfo.getZrate().getRatevalue()
                    + "\" newRateId=\""
                    //					+ sinfo.getZrate().getRatevalue() + "\" PolicyId=\""
                    + sinfo.getZrate().getOutid() + "\" PnrInfo=\"" + PnrInfo + "\"" + "/>");
            str.append("</JIT-OrderByPNR-Request>");
            //            createOrderByPNR.setData(str.toString());
            createOrderByPNR.setXml(str.toString());
            WriteLog.write("CreateOrder今日", "今日创建订单的xml:" + str.toString());
            JinRiOrderServerStub.CreateOrderByPNRResponse response = stub.createOrderByPNR(createOrderByPNR);
            String result = response.getCreateOrderByPNRResult();
            WriteLog.write("CreateOrder今日", "今日创建订单返回的xml:" + result);
            if (result.length() < 10) {
                deleteZratebyoutid(sinfo.getZrate().getOutid());
                return "-1";
            }
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            List list = root.elements("Response");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                OrderNo = elmt.elementText("OrderNo");// 订单号
                // System.out.println(elmt.elementText("PNR"));//
                // System.out.println(elmt.elementText("GrowDiscount"));
                // System.out.println(elmt.elementText("StayDiscount"));
                PayMoney = elmt.elementText("PayMoney");// 应付金额
                // System.out.println(elmt.elementText("Profit"));// 利润
                // System.out.println(elmt.elementText("Status"));// 状态
                // System.out.println(elmt.elementText("Date"));// 航班日期
                // System.out.println(elmt.elementText("Scity"));
                // System.out.println(elmt.elementText("Ecity"));
                // System.out.println(elmt.elementText("Flight"));
                // System.out.println(elmt.elementText("Stime"));
                // System.out.println(elmt.elementText("Etime"));
                // System.out.println(elmt.elementText("Cabin"));
                // System.out.println(elmt.elementText("Price"));
                // System.out.println(elmt.elementText("Tax"));
                // System.out.println(elmt.elementText("PCount"));
                // System.out.println(elmt.elementText("Stime"));
                // System.out.println(elmt.elementText("PName"));
                // System.out.println(elmt.elementText("ZFBPayURL"));
                // System.out.println(elmt.elementText("KOPayURL"));
                // System.out.println(elmt.elementText("CFTPayURL"));
                // PNR:HXF890,外部政策ID:17784275,str:S|112012060717240255|https://www.alipay.com/cooperate/gateway.do?body=%E6%9C%BA%E7%A5%A8%E9%87%87%E8%B4%AD+++%E8%88%AA%E7%8F%AD%3A+GS7565++%E8%88%AA%E7%A8%8B%3A+LHW-JIC+%E4%B9%98%E6%9C%BA%E4%BA%BA%3A%E5%BC%A0%E6%9C%89%E8%BE%BE%28%E8%AE%A2%E5%8D%95%E5%8F%B7%3A112012060717240255%29%E6%93%8D%E4%BD%9C%E5%91%98%3Aluoruncai&subject=%E6%9C%BA%E7%A5%A8&notify_url=http%3A%2F%2Falipay.51book.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderPaymentNotify.in&out_trade_no=112012060717240255&return_url=http%3A%2F%2Fhttp%3A%2F%2Falipay.51book.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderPaidReturn.in&credit_card_pay=Y&_input_charset=utf-8&total_fee=529.0&credit_card_default_display=Y&service=create_direct_pay_by_user&paymethod=directPay&partner=2088001425892773&seller_email=zfblt%40126.com&payment_type=1&show_url=http%3A%2F%2Falipay.51book.com%3A8000%2Fliantuo%2Fmanage%2FpolicyOrderDetail.in%3ForderNo%3D112012060717240255&sign=9b8edd93563a7a5499e1b4544876e614&sign_type=MD5|XMN148

            }
            // if (!PayMoney.isEmpty()) {
            // try {
            // String where = "UPDATE T_ORDERINFO SET "
            // + Orderinfo.COL_extorderprice + "=" + PayMoney
            // + " WHERE " + Orderinfo.COL_pnr + "='"
            // + order.getPnr() + "'";
            // WriteLog.write("z支付供应", "jinriCreateOrder:" + where);
            // Server.getInstance().getSystemService().findMapResultBySql(
            // where, null);
            // } catch (Exception e) {
            // e.printStackTrace();
            // }
            // }
            // 返回的格式S|订单号|支付链接|office号
            payurl = getPayUrl(OrderNo, order.getPnr());
            officeId = getOffice(OrderNo);
            if (!PayMoney.isEmpty()) {
                resultstr = "S~" + OrderNo + "~" + payurl + "~" + officeId + "~" + PayMoney;
            }
            else {
                resultstr = "S~" + OrderNo + "~" + payurl + "~" + officeId;
            }
        }
        catch (Exception e) {
            logger.error(e);
            WriteLog.write("今日", "下单返回的字符串:" + e.getMessage());
        }
        WriteLog.write("今日", "下单返回的字符串:" + resultstr);
        return resultstr;
    }

    /**
     * 从指定的数据库里删除指定政策
     * @param outid 政策的outid
     */
    public void deleteZratebyoutid(String outid) {
        try {
            String url = getSysconfigString("jinrizrateurl");
            HessianProxyFactory factory = new HessianProxyFactory();
            url = url + "/cn_service/service/";
            IAirService servier = (IAirService) factory.create(IAirService.class,
                    url + IAirService.class.getSimpleName());
            String sql = "UPDATE T_ZRATE SET C_ISENABLE=0  WHERE C_OUTID='" + outid + "'";
            int count = servier.excuteZrateBySql(sql);
            WriteLog.write("CreateOrder今日_del", count + ":" + sql);
        }
        catch (Exception e) {
            WriteLog.write("CreateOrder今日_del", e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 获得块钱支付链接
     * 
     * @param orderNO
     * @param pnr
     * @return
     */
    public String getKQPayUrl(String orderNO, String pnr) {
        String resultstr = "";
        try {
            JinRiPayServerStub stub = new JinRiPayServerStub();
            JinRiPayServerStub.GetKqURLResponse response = new JinRiPayServerStub.GetKqURLResponse();
            JinRiPayServerStub.GetKqURL getKqURL = new JinRiPayServerStub.GetKqURL();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Order-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" orderno=\"" + orderNO + "\""
                    + " pnr=\"" + pnr + "\" />");
            str.append("</JIT-Order-Request>");
            getKqURL.setData(str.toString());
            response = stub.getKqURL(getKqURL);

            String result = response.getGetKqURLResult();
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            List list = root.elements("Response");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                resultstr = elmt.elementText("URL");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resultstr;
    }

    /**
     * 获得支付宝支付链接
     * 
     * @param orderNO
     * @param pnr
     * @return
     */
    public String getPayUrl(String orderNO, String pnr) {
        String resultstr = "";
        try {
            JinRiPayServerStub stub = new JinRiPayServerStub();
            JinRiPayServerStub.GetAlipayURLResponse response = new JinRiPayServerStub.GetAlipayURLResponse();
            JinRiPayServerStub.GetAlipayURL getAlipayURL = new JinRiPayServerStub.GetAlipayURL();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Order-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" orderno=\"" + orderNO + "\""
                    + " pnr=\"" + pnr + "\" />");
            str.append("</JIT-Order-Request>");
            getAlipayURL.setData(str.toString());
            response = stub.getAlipayURL(getAlipayURL);
            String result = response.getGetAlipayURLResult();
            WriteLog.write("CreateOrder今日", "getPayUrl:" + result);
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            List list = root.elements("Response");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                resultstr = elmt.elementText("URL");
            }
        }
        catch (Exception e) {
            logger.error(e);
        }
        return resultstr;
    }

    /**
     * 根据订单号获取今日的officeid
     * 
     * @author 陈栋
     * @param orderNO
     * @return
     */
    public String getOffice(String orderNO) {
        String result = "";
        JinRiOrderServerStub stub;
        try {
            stub = new JinRiOrderServerStub();
            JinRiOrderServerStub.GetOfficeId getOfficeId = new JinRiOrderServerStub.GetOfficeId();
            JinRiOrderServerStub.GetOfficeIdResponse response = new JinRiOrderServerStub.GetOfficeIdResponse();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Provider-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRXiausername() + "\" code=\"" + orderNO + "\""
                    + " type=\"4\" />");
            str.append("</JIT-Provider-Request>");
            getOfficeId.setXml(str.toString());
            //            getOfficeId.setData(str.toString());
            response = stub.getOfficeId(getOfficeId);
            String resultStr = response.getGetOfficeIdResult();
            WriteLog.write("CreateOrder今日", "今日获得授权officeid:" + resultStr);
            org.dom4j.Document document = DocumentHelper.parseText(resultStr);
            org.dom4j.Element root = document.getRootElement();
            DefaultText text = (DefaultText) root.content().get(0);
            String officeIdStr = text.getText();
            String[] ids = officeIdStr.split("/");
            if (ids.length > 1) {
                result = ids[1];
            }
            else {
                result = ids[0];
            }
        }
        catch (Exception e) {
            logger.error(e);
        }
        return result;
    }

    /**
     * 今日创建订单(此方法在今日天下通生成PNR)
     * 
     * @author 陈栋 2012-5-3 15:03:15
     * @param order
     * @param listPassenger
     * @param sinfo
     * @return
     */
    public String createOrder(Orderinfo order, List<Passenger> listPassenger, Segmentinfo sinfo) {
        String resultString = "-1";
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();
            JinRiOrderServerStub.CreateOrder createOrder = new JinRiOrderServerStub.CreateOrder();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-CreateOrder>");
            str.append("<Request rateid=\"" + sinfo.getZrate().getAgentcode() + "\" ");
            str.append("name=\"");
            for (int i = 0; i < listPassenger.size(); i++) {
                str.append(listPassenger.get(i).getName());
                if (i != listPassenger.size() - 1) {
                    str.append("|");
                }
            }
            str.append("\" ");
            str.append("idcard=\"");
            for (int i = 0; i < listPassenger.size(); i++) {
                if (listPassenger.get(i).getIdtype() == 1) {
                    str.append("NI" + listPassenger.get(i).getIdnumber());
                }
                else if (listPassenger.get(i).getIdtype() == 3) {
                    str.append("PP" + listPassenger.get(i).getIdnumber());
                }
                if (i != listPassenger.size() - 1) {
                    str.append("|");
                }
            }
            str.append("\"");

            str.append(" cabins=\"" + sinfo.getCabincode() + "\" airline=\"" + sinfo.getFlightnumber() + "\" sdate=\""
                    + sinfo.getDeparttime().toString().substring(0, 10) + "\" username=\""
                    + jinRiBook.getJRXiausername() + "\" scity=\"" + sinfo.getStartairport() + "\" ecity=\""
                    + sinfo.getEndairport() + "\" dotnum=\"" + sinfo.getZrate().getRatevalue() + "\" isallowPnr=\"1\"");
            str.append(" jounery=\"");
            for (int i = 0; i < listPassenger.size(); i++) {
                str.append("1");
                if (i != listPassenger.size() - 1) {
                    str.append("|");
                }
            }
            str.append("\"");
            str.append(" IsFront=\"1\">");
            str.append("</Request>");
            str.append("</JIT-CreateOrder>");
            //            createOrder.setData(str.toString());
            createOrder.setXml(str.toString());
            WriteLog.write("今日", "在今日创建PNR后创建的xml:" + str.toString());
            JinRiOrderServerStub.CreateOrderResponse response = stub.createOrder(createOrder);
            String result = response.getCreateOrderResult();
            WriteLog.write("今日", "在今日创建PNR后创建订单返回的xml:" + result);
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            List list = root.elements("Response");
            Iterator it = list.iterator();

            while (it.hasNext()) {
                org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                String OrderNo = elmt.elementText("OrderNo");
                // S$PNR$ORderNo$返点
                String payUrl = getPayUrl(OrderNo, order.getPnr());
                resultString = "S$" + elmt.elementText("PNR") + "$" + OrderNo + "$" + payUrl + "$"
                        + elmt.elementText("PayMoeny");
                String temp = elmt.elementText("PNR") + ":" + elmt.elementText("OrderNo") + ":"
                        + elmt.elementText("Discount") + ":" + elmt.elementText("PayMoeny") + ":"
                        + elmt.elementText("Profit") + ":" + elmt.elementText("Status") + ":"
                        + elmt.elementText("Date") + ":" + elmt.elementText("Scity") + ":" + elmt.elementText("Ecity")
                        + ":" + elmt.elementText("Flight") + ":" + elmt.elementText("Stime") + ":"
                        + elmt.elementText("Etime") + ":" + elmt.elementText("Cabin") + ":" + elmt.elementText("Price")
                        + ":" + elmt.elementText("Tax") + ":" + elmt.elementText("PCount");
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
            return "-1";
        }
        catch (RemoteException e) {
            e.printStackTrace();
            return "-1";
        }
        catch (DocumentException e) {
            e.printStackTrace();
            return "-1";
        }
        return resultString;
    }

    public String AutoPayOrder(Orderinfo order) {
        String result = "-1";
        try {
            JinRiAutoPayServerStub stub = new JinRiAutoPayServerStub();
            JinRiAutoPayServerStub.AutoPayOrder autoPayOrder = new JinRiAutoPayServerStub.AutoPayOrder();
            String XML = "<?xml version=\"1.0\" encoding=\"gb2312\"?><JIT-AutoPay-Order><Request username=\""
                    + jinRiBook.getJRXiausername() + "\" orderno=\"" + order.getExtorderid()
                    + "\"/></JIT-AutoPay-Order>";
            WriteLog.write("自动代扣", "今日:0:" + XML);
            //            autoPayOrder.setData(XML);
            autoPayOrder.setXml(XML);
            JinRiAutoPayServerStub.AutoPayOrderResponse res = stub.autoPayOrder(autoPayOrder);
            String resultXML = res.getAutoPayOrderResult();
            WriteLog.write("自动代扣", "今日:1:" + resultXML);
            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            org.dom4j.Element root = document.getRootElement();
            String text = root.elementText("Result");
            if (text.equals("T")) {
                result = "S";
                String sql = "UPDATE T_ORDERINFO SET " + Orderinfo.COL_extorderstatus + "='2' WHERE ID="
                        + order.getId();
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    // --------------------------------------------------------------------今日退费取消订单
    // start--------------------------------------------------------------

    /**
     * 取消订单
     * 
     * @param order
     */
    public String CancelOrder(Orderinfo order) {
        String result = "F";
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.CancelOrder cancelOrder = new JinRiOrderServerStub.CancelOrder();

            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-CancelOrder><Request username="dai001"
            // orderno="B0011412005365908855" pnr="QB0GQ"
            // PayType="ZFB" CanBeiZhu="test" />
            // </JIT-CancelOrder>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-CancelOrder>");
            sb.append("<Request");
            sb.append(" username=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" orderno=\"" + order.getExtorderid() + "\"");// 订单号
            sb.append(" pnr=\"" + order.getPnr() + "\"");// Pnr编码
            sb.append(" PayType=\"ZFB\"");// 支付类型（ZFB、KQ、 CFT） 支付方式
            sb.append("CanBeiZhu=\"\"");// 取消说明 备注信息
            sb.append("/></JIT-CancelOrder>");

            String XML = sb.toString();
            WriteLog.write("今日", "取消订单请求:" + XML);
            cancelOrder.setXml(XML);
            //            cancelOrder.setData(XML);
            CancelOrderResponse res = stub.cancelOrder(cancelOrder);
            String resultXML = res.getCancelOrderResult();
            WriteLog.write("今日", "取消订单返回:" + resultXML);
            // <?xml version='1.0' encoding='gb2312'?><Result>T</Result>
            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            // org.dom4j.Element root = document.getRootElement();
            // String text = root.elementText("Result");
            String text = ((Element) document.selectNodes("/Result").get(0)).getText();
            if ("T".equals(text)) {// T 成功 F 失败 其他错误代号
                System.out.println(order.getId() + "取消订单 成功!!!!!!!");
                result = "S";
            }
            else {
                System.out.println(order.getId() + "取消订单 失败!!!!!!!!");
                result = "F";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 退费订单
     * 
     * @param order
     * @param listPassenger
     *            退费人 列表
     * @param type //
     *            废票(A) 退票(B)
     */
    public String TuiFeiOrder(Orderinfo order, List<Passenger> listPassenger, String type) {
        String result = "F";
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.TuiFeiOrder tuiFeiOrder = new JinRiOrderServerStub.TuiFeiOrder();
            // <?xml version="1.0" encoding="gb2312"
            // ?><JIT-TuiFeiOrder-Request><Request name="john168" type="B "
            // OrderNo="W1112051534316594004" Repeal="2|2|0|"
            // personName="郭维敏|彭玉蓉|杨璐|" isCancelSeat="是" Cause="航班延误申请全退"
            // Remarks="zzf" Rnum="2"
            // TicketNo="333333333333333|333333333333334|333333333333335"
            // Ramount="1" /></JIT-TuiFeiOrder-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-TuiFeiOrder-Request>");
            sb.append("<Request");
            sb.append(" name=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" type=\"" + type + " \"");// 废票(A) 退票(B)
            sb.append(" OrderNo=\"" + order.getExtorderid() + "\"");// 订单号
            sb.append("Repeal=\"");// // 退费标识 废票(1) 退票(2) 不变(0) 按乘客姓名顺序多个用| 分隔
            for (int i = 0; i < listPassenger.size(); i++) {
                if ("A".equals(type)) {
                    sb.append("1");
                }
                else {
                    sb.append("2");
                }
                if (i != listPassenger.size() - 1) {
                    sb.append("|");
                }
            }
            sb.append("\"");
            sb.append("personName=\"");// 退费票人姓名
            for (int i = 0; i < listPassenger.size(); i++) {
                sb.append(listPassenger.get(i).getName());
                if (i != listPassenger.size() - 1) {
                    sb.append("|");
                }
            }
            sb.append("\"");

            sb.append(" isCancelSeat=\"否\"");// 是否取消今日位置
            sb.append(" Cause=\"" + listPassenger.get(0).getTuifeidesc() + "\"");// 退费原因
            sb.append(" Remarks=\"" + order.getMemo() + "\"");// 备注
            sb.append(" Rnum=\"" + listPassenger.size() + "\"");// 退费人数
            sb.append(" TicketNo=\"");// 退费订单的票号
            for (int i = 0; i < listPassenger.size(); i++) {
                sb.append(listPassenger.get(i).getTicketnum());
                if (i != listPassenger.size() - 1) {
                    sb.append("|");
                }
            }
            sb.append("\"");

            sb.append(" Ramount=\"\"");// 退费总金额（decimal 类型 ）
            sb.append("/></JIT-TuiFeiOrder-Request>");

            String XML = sb.toString();
            WriteLog.write("今日", "退费订单请求:" + XML);
            tuiFeiOrder.setXml(XML);
            //            tuiFeiOrder.setData(XML);
            JinRiOrderServerStub.TuiFeiOrderResponse res = stub.tuiFeiOrder(tuiFeiOrder);
            String resultXML = res.getTuiFeiOrderResult();
            WriteLog.write("今日", "退费订单返回:" + resultXML);
            // <?xml version='1.0' encoding='gb2312'?><Result>T</Result>
            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            // org.dom4j.Element root = document.getRootElement();
            // String text = root.elementText("Result");
            String text = ((Element) document.selectNodes("/Result").get(0)).getText();
            if ("T".equals(text)) {// T 成功 F 失败 其他错误代号
                System.out.println(order.getId() + "退费订单 成功!!!!!!!");
                result = "S";
            }
            else {
                System.out.println(order.getId() + "退费订单 失败!!!!!!!!");
                result = "F";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 申请改签
     * 
     * @param order
     */
    public String GaiqianOrder(Orderinfo order) {
        String result = "F";
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.GaiqianOrder gaiqianOrder = new JinRiOrderServerStub.GaiqianOrder();

            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-GaiQianOrder-Request><Request username="dai001"
            // orderno="W1001211117381581844" Content="接口测试改签" />
            // </JIT-GaiQianOrder-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-GaiQianOrder-Request>");
            sb.append("<Request");
            sb.append(" username=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" orderno=\"" + order.getExtorderid() + "\"");// 要改签的订单号
            sb.append(" Content=\"" + order.getMemo() + "\"");// 改签说明 备注
            sb.append("/></JIT-GaiQianOrder-Request>");
            String XML = sb.toString();
            WriteLog.write("今日", "申请改签请求:" + XML);
            gaiqianOrder.setXml(XML);
            JinRiOrderServerStub.GaiqianOrderResponse res = stub.gaiqianOrder(gaiqianOrder);
            //            JinRiOrderServerStub.GaiqianOrderResponse
            String resultXML = res.getGaiqianOrderResult();
            WriteLog.write("今日", "申请改签返回:" + resultXML);
            // <?xml version='1.0' encoding='gb2312'?><Result>T</Result>
            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            // org.dom4j.Element root = document.getRootElement();
            // String text = root.elementText("Result");
            String text = ((Element) document.selectNodes("/Result").get(0)).getText();
            if ("T".equals(text)) {// T 成功 F 失败 其他错误代号
                System.out.println(order.getId() + "申请改签 成功!!!!!!!");
                result = "S";
            }
            else {
                System.out.println(order.getId() + "申请改签 失败!!!!!!!!");
                result = "F";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 申请升舱 未完成
     * 
     * @param order
     */
    public String RiseCabin(Orderinfo order) {
        String result = "F";
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.RiseCabin riseCabin = new JinRiOrderServerStub.RiseCabin();
            List<Passenger> listPassenger = order.getPassengerlist();

            // <?xml version="1.0" encoding="gb2312"
            // ?><JIT-RiseCabinOrder-Request>
            // <Request UserName="john168" OrderNo="W1111211755356594003"
            // psgName="郭维敏|" PNR="HXLLQF" DepartDate="2011-12-25"
            // DepartTime="17:50" FlightNo="CA1367" Cabin="Y"
            // Remark="SS555" ProviderID="FbBs3Vo0yyU="
            // BuyId="659468" IsFront="0" />
            // </JIT-RiseCabinOrder-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-RiseCabinOrder-Request>");
            sb.append("<Request");
            sb.append(" username=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" orderno=\"" + order.getExtorderid() + "\"");// 订单号
            sb.append("personName=\"");// 改签乘客姓名 多个已|隔开 已|结尾
            for (int i = 0; i < listPassenger.size(); i++) {
                sb.append(listPassenger.get(i).getName()).append("|");
            }
            sb.append("\"");

            sb.append(" pnr=\"" + order.getPnr() + "\"");// 需要改签的Pnr TODO
            sb.append(" DepartDate=\"" + order.getPnr() + "\"");// 申请改签飞机的起飞日期
            // TODO
            sb.append(" DepartTime=\"" + order.getPnr() + "\"");// 申请改签飞机的起飞时间
            // TODO
            sb.append(" FlightNo=\"" + order.getPnr() + "\"");// 改签到的航班 TODO
            sb.append(" Cabin=\"" + order.getPnr() + "\"");// 改签到的仓位 TODO
            sb.append(" Remark=\"" + order.getMemo() + "\"");// TODO 备注
            sb.append(" ProviderID=\"" + order.getMemo() + "\"");// 供票商id（加密）
            // TODO
            sb.append(" BuyId=\"" + order.getAgent().getId() + "\"");// 采购商id
            sb.append(" IsFront=\"" + order.getMemo() + "\"");// 前台1/接口0 TODO

            sb.append("/></JIT-RiseCabinOrder-Request>");
            String XML = sb.toString();
            WriteLog.write("今日", "申请升舱请求:" + XML);
            riseCabin.setXml(XML);
            JinRiOrderServerStub.RiseCabinResponse res = stub.riseCabin(riseCabin);
            String resultXML = res.getRiseCabinResult();
            WriteLog.write("今日", "申请升舱返回:" + resultXML);
            // <?xml version='1.0' encoding='gb2312'?><Result>T</Result>
            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            // org.dom4j.Element root = document.getRootElement();
            // String text = root.elementText("Result");
            String text = ((Element) document.selectNodes("/Result").get(0)).getText();
            if ("T".equals(text)) {// T 成功 F 失败 其他错误代号
                System.out.println(order.getId() + "申请升舱 成功!!!!!!!");
                result = "S";
            }
            else {
                System.out.println(order.getId() + "申请升舱 失败!!!!!!!!");
                result = "F";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 根据订单号获取改签信息 未完成
     * 
     * @param order
     */
    public void GetGaiqianOrder(Orderinfo order) {
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.GetGaiqianOrder getGaiqianOrder = new JinRiOrderServerStub.GetGaiqianOrder();
            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-GetGaiQianOrder-Request>
            // <Request UserName="dai001" OrderNo="W1001211117381581844" />
            // </JIT-GetGaiQianOrder-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-GetGaiQianOrder-Request>");
            sb.append("<Request");
            sb.append(" UserName=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" OrderNo=\"" + order.getExtorderid() + "\"");// 订单号

            sb.append("/></JIT-GetGaiQianOrder-Request>");
            String XML = sb.toString();
            WriteLog.write("今日", "根据订单号获取改签信息请求:" + XML);
            getGaiqianOrder.setXml(XML);
            JinRiOrderServerStub.GetGaiqianOrderResponse res = stub.getGaiqianOrder(getGaiqianOrder);
            String resultXML = res.getGetGaiqianOrderResult();
            WriteLog.write("今日", "根据订单号获取改签信息返回:" + resultXML);
            // <?xml version='1.0' encoding='gb2312'?><Result>T</Result>

            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-GetGaiQianOrder-Response>
            // <Response OrderNo="W1002241445381581789" UserName="dai001"
            // AddDate="2010-2-24 15:14:53" IP="192.168.1.4" State="申请改签"
            // ReplyContent="fgfdg" ISProxyerAdd="1" />
            // </JIT-GetGaiQianOrder-Response>

            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                if (list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {
                        org.dom4j.Element e = list.get(a);
                        if (e.attributeValue("OrderNo") != null) {
                            System.out.println("OrderNo:" + e.attributeValue("OrderNo"));
                        }
                        if (e.attributeValue("UserName") != null) {
                            System.out.println("UserName:" + e.attributeValue("UserName"));
                        }
                        if (e.attributeValue("AddDate") != null) {
                            System.out.println("AddDate:" + e.attributeValue("AddDate"));
                        }
                        if (e.attributeValue("IP") != null) {
                            System.out.println("IP:" + e.attributeValue("IP"));
                        }
                        if (e.attributeValue("State") != null) {
                            System.out.println("State:" + e.attributeValue("State"));
                        }
                        if (e.attributeValue("ReplyContent") != null) {
                            System.out.println("ReplyContent:" + e.attributeValue("ReplyContent"));
                        }
                        if (e.attributeValue("ISProxyerAdd") != null) {
                            System.out.println("ISProxyerAdd:" + e.attributeValue("ISProxyerAdd"));
                        }

                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取升舱信息// 未完成
     * 
     * @param order
     */
    public void GetRiseOrder(Orderinfo order) {
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();

            JinRiOrderServerStub.GetRaiseOrder getRaiseOrder = new JinRiOrderServerStub.GetRaiseOrder();
            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-GetRiseOrder-Request>
            // <Request UserName="dai001" OrderNo="W1001200949571581519" />
            // </JIT-GetRiseOrder-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JIT-GetRiseOrder-Request>");
            sb.append("<Request");
            sb.append(" UserName=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" OrderNo=\"" + order.getExtorderid() + "\"");// 订单号

            sb.append("/></JIT-GetRiseOrder-Request>");
            String XML = sb.toString();
            WriteLog.write("今日", "获取升舱信息请求:" + XML);
            getRaiseOrder.setXml(XML);
            JinRiOrderServerStub.GetRaiseOrderResponse res = stub.getRaiseOrder(getRaiseOrder);
            String resultXML = res.getGetRaiseOrderResult();
            WriteLog.write("今日", "获取升舱信息返回:" + resultXML);
            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-RiseOrder-Response>
            // <Response mid="4647" CabinNo="10020112592201234269"
            // OrderStatus="0" PNR="SLD1G" DepartDate="2010-2-1"
            // DepartTime="11:44" FlightNo="F3234" Cabin="G" CreatTime="2010-2-1
            // 13:25:59"
            // Remark="SS" AgreeTime="" PayTime="" SuccessTime="" FailTime=""
            // PayAmount=""
            // DiffAmount="" AlipayTradeNo="" BuyerAccount="" BuyerID="659088"
            // ProviderID="324324"
            // OrderInfoRemark="" person="周晶晶|" />
            // </JIT-RiseOrder-Response>

            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                if (list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {
                        org.dom4j.Element e = list.get(a);
                        if (e.attributeValue("mid") != null) {
                            System.out.println("mid:" + e.attributeValue("mid"));
                        }
                        if (e.attributeValue("CabinNo") != null) {
                            System.out.println("CabinNo:" + e.attributeValue("CabinNo"));
                        }
                        if (e.attributeValue("OrderStatus") != null) {
                            System.out.println("OrderStatus:" + e.attributeValue("OrderStatus"));
                        }
                        if (e.attributeValue("PNR") != null) {
                            System.out.println("PNR:" + e.attributeValue("PNR"));
                        }
                        if (e.attributeValue("DepartDate") != null) {
                            System.out.println("DepartDate:" + e.attributeValue("DepartDate"));
                        }
                        if (e.attributeValue("DepartTime") != null) {
                            System.out.println("DepartTime:" + e.attributeValue("DepartTime"));
                        }
                        if (e.attributeValue("FlightNo") != null) {
                            System.out.println("FlightNo:" + e.attributeValue("FlightNo"));
                        }
                        if (e.attributeValue("Cabin") != null) {
                            System.out.println("Cabin:" + e.attributeValue("Cabin"));
                        }
                        if (e.attributeValue("CreatTime") != null) {
                            System.out.println("CreatTime:" + e.attributeValue("CreatTime"));
                        }
                        if (e.attributeValue("Remark") != null) {
                            System.out.println("Remark:" + e.attributeValue("Remark"));
                        }
                        if (e.attributeValue("AgreeTime") != null) {
                            System.out.println("AgreeTime:" + e.attributeValue("AgreeTime"));
                        }
                        if (e.attributeValue("PayTime") != null) {
                            System.out.println("PayTime:" + e.attributeValue("PayTime"));
                        }
                        if (e.attributeValue("SuccessTime") != null) {
                            System.out.println("SuccessTime:" + e.attributeValue("SuccessTime"));
                        }
                        if (e.attributeValue("FailTime") != null) {
                            System.out.println("FailTime:" + e.attributeValue("FailTime"));
                        }
                        if (e.attributeValue("PayAmount") != null) {
                            System.out.println("PayAmount:" + e.attributeValue("PayAmount"));
                        }
                        if (e.attributeValue("DiffAmount") != null) {
                            System.out.println("DiffAmount:" + e.attributeValue("DiffAmount"));
                        }
                        if (e.attributeValue("AlipayTradeNo") != null) {
                            System.out.println("AlipayTradeNo:" + e.attributeValue("AlipayTradeNo"));
                        }
                        if (e.attributeValue("BuyerAccount") != null) {
                            System.out.println("BuyerAccount:" + e.attributeValue("BuyerAccount"));
                        }
                        if (e.attributeValue("BuyerID") != null) {
                            System.out.println("BuyerID:" + e.attributeValue("BuyerID"));
                        }
                        if (e.attributeValue("ProviderID") != null) {
                            System.out.println("ProviderID:" + e.attributeValue("ProviderID"));
                        }
                        if (e.attributeValue("OrderInfoRemark") != null) {
                            System.out.println("OrderInfoRemark:" + e.attributeValue("OrderInfoRemark"));
                        }
                        if (e.attributeValue("person") != null) {
                            System.out.println("person:" + e.attributeValue("person"));
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取退费票详情 未完成
     * 
     * @param order
     */
    public void GetOrderInfo(Orderinfo order) {
        try {
            JinRiOrderServerStub stub = new JinRiOrderServerStub();
            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-Order-Request>
            // <Request username="john168" orderno="
            // W1204240944486594002"></Request>
            // </JIT-Order-Request>

            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            sb.append("<JJIT-Order-Request>");
            sb.append("<Request");
            sb.append(" username=\"" + jinRiBook.getJRUSER() + "\"");// 子账户（用户名）
            sb.append(" orderno=\"" + order.getExtorderid() + "\">");// 订单号

            sb.append("</Request></JIT-GetRiseOrder-Request>");
            String XML = sb.toString();
            WriteLog.write("今日", "获取退费票详情请求:" + XML);
            // getRaiseOrder.setData(XML);
            // GetRaiseOrderResponse res = stub.getRaiseOrder(getRaiseOrder);
            // String resultXML = res.getGetRaiseOrderResult();
            String resultXML = "";
            WriteLog.write("今日", "获取退费票详情返回:" + resultXML);
            // <?xml version="1.0" encoding="gb2312"?>
            // <JIT-ReturnOrder-Response>
            // <Response Status="2" PassengerName="李清" TicketNo=" - "
            // Cause="- 请选择原因 -" Remark="kk" RequestDate="2010-02-26 18:05:00"
            // ReturnDate="处理中" ReturnMoney="处理中" LastStatus="0"
            // ReturnPrice="0" BatchNoTOptType="0" RefundPrice="" />
            // </JIT-ReturnOrder-Response>

            org.dom4j.Document document = DocumentHelper.parseText(resultXML);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                if (list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {
                        org.dom4j.Element e = list.get(a);
                        if (e.attributeValue("Status") != null) {
                            System.out.println("Status:" + e.attributeValue("Status"));
                        }
                        if (e.attributeValue("PassengerName") != null) {
                            System.out.println("PassengerName:" + e.attributeValue("PassengerName"));
                        }
                        if (e.attributeValue("TicketNo") != null) {
                            System.out.println("TicketNo:" + e.attributeValue("TicketNo"));
                        }
                        if (e.attributeValue("Cause") != null) {
                            System.out.println("Cause:" + e.attributeValue("Cause"));
                        }
                        if (e.attributeValue("Remark") != null) {
                            System.out.println("Remark:" + e.attributeValue("Remark"));
                        }
                        if (e.attributeValue("RequestDate") != null) {
                            System.out.println("RequestDate:" + e.attributeValue("RequestDate"));
                        }
                        if (e.attributeValue("ReturnDate") != null) {
                            System.out.println("ReturnDate:" + e.attributeValue("ReturnDate"));
                        }
                        if (e.attributeValue("ReturnMoney") != null) {
                            System.out.println("ReturnMoney:" + e.attributeValue("ReturnMoney"));
                        }
                        if (e.attributeValue("LastStatus") != null) {
                            System.out.println("LastStatus:" + e.attributeValue("LastStatus"));
                        }
                        if (e.attributeValue("ReturnPrice") != null) {
                            System.out.println("ReturnPrice:" + e.attributeValue("ReturnPrice"));
                        }
                        if (e.attributeValue("BatchNoTOptType") != null) {
                            System.out.println("BatchNoTOptType:" + e.attributeValue("BatchNoTOptType"));
                        }
                        if (e.attributeValue("RefundPrice") != null) {
                            System.out.println("RefundPrice:" + e.attributeValue("RefundPrice"));
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    // --------------------------------------------------------------------今日退费取消订单
    // end--------------------------------------------------------------

    private static SimpleDateFormat dateFormat = new SimpleDateFormat();

    public static Timestamp dateToTimestamp(String date) {
        try {

            if (date.length() == 10) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            }
            else {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }
            return (new Timestamp(dateFormat.parse(date).getTime()));
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据机场三字码获取城市名称
     * 
     * @param aircompany
     * @return
     */
    public String getCityNamebyCode(String code) {
        String where = "where " + Cityairport.COL_airportcode + "='" + code + "'";
        List<Cityairport> list = Server.getInstance().getAirService().findAllCityairport(where, "", -1, 0);
        return list.get(0).getCityname();
    }

    /**
     * 根据航班号查询政策
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @author chendong 2013年6月20日11:32:19
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        //因为今日<Error ErrorCode="1545" ErrorType="0" ErrorInfo="不支持预订南航Z舱"/>
        //所以就不取这样的今日的政策了
        if (flightnumber.toUpperCase().indexOf("CZ") >= 0 && "Z".equals(cabin.toUpperCase())) {
            return zrates;
        }
        try {
            JinRiRateServerStub stub = new JinRiRateServerStub();
            JinRiRateServerStub.GetRateList rateList = new JinRiRateServerStub.GetRateList();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Policy-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRUSER() + "\" scity=\"" + scity + "\" ecity=\"" + ecity
                    + "\" date=\"" + sdate + "\" aircome=\"" + flightnumber.substring(0, 2)
                    + "\" vayagetype=\"0\" amount=\"10\" FlightNo=\"" + flightnumber + "\" cabin=\"" + cabin + "\" ");
            str.append(" />");
            str.append("</JIT-Policy-Request>");
            rateList.setData(str.toString());
            JinRiRateServerStub.GetRateListResponse response = stub.getRateList(rateList);
            String result = response.getGetRateListResult();
            if (result.length() < 10) {
                WriteLog.write("今日ERR", "getZrateByFlightNumber:" + result);
                return zrates;
            }
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                // if (list.size() > 0) {
                for (int a = 0; a < list.size(); a++) {
                    Zrate zrate = new Zrate();
                    org.dom4j.Element e = list.get(a);
                    zrate.setAgentid(6L);
                    zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    zrate.setCreateuser("job");
                    zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    zrate.setModifyuser("job");

                    zrate.setIsenable(1);
                    if (e.attributeValue("PolicyId") != null) {
                        zrate.setOutid(e.attributeValue("PolicyId"));
                    }
                    if (e.attributeValue("RateId") != null) {
                        zrate.setAgentcode(e.attributeValue("RateId"));
                    }
                    if (e.attributeValue("ScityE") != null) {
                        zrate.setDepartureport(e.attributeValue("ScityE"));
                    }
                    if (e.attributeValue("EcityE") != null) {
                        zrate.setArrivalport(e.attributeValue("EcityE"));
                    }
                    if (e.attributeValue("AirComE") != null) {
                        if (e.attributeValue("AirComE").length() > 2) {
                            zrate.setAircompanycode(e.attributeValue("AirComE").substring(0, 2));
                            zrate.setFlightnumber(e.attributeValue("AirComE"));
                        }
                        else {
                            zrate.setAircompanycode(e.attributeValue("AirComE"));
                        }
                    }
                    if (e.attributeValue("NoAirComE") != null) {
                        zrate.setWeeknum(e.attributeValue("NoAirComE"));
                    }
                    if (e.attributeValue("SP") != null) {
                        zrate.setSpeed(e.attributeValue("SP"));
                    }

                    if (e.attributeValue("Cabin") != null) {
                        zrate.setCabincode(e.attributeValue("Cabin"));
                    }
                    if (e.attributeValue("VoyageType") != null) {
                        if (e.attributeValue("VoyageType").equals("0")) {
                            zrate.setVoyagetype("1");
                        }
                        else if (e.attributeValue("VoyageType").equals("1")) {
                            zrate.setVoyagetype("2");
                        }
                        else {
                            zrate.setVoyagetype("3");
                        }
                    }
                    String UserType = e.attributeValue("UserType");
                    if (e.attributeValue("RateType") != null) {
                        String rateType = e.attributeValue("RateType");
                        if (rateType.equals("1") || rateType.equals("0")) {
                            zrate.setGeneral(1L);
                        }
                        else {
                            zrate.setGeneral(2L);
                        }
                    }
                    if (e.attributeValue("Discounts") != null) {
                        zrate.setRatevalue(Float.parseFloat(e.attributeValue("Discounts")));
                    }
                    if (e.attributeValue("Sdate") != null) {
                        zrate.setBegindate(dateToTimestamp(e.attributeValue("Sdate")));
                    }
                    if (e.attributeValue("Edate") != null) {
                        zrate.setEnddate(dateToTimestamp(e.attributeValue("Edate")));
                    }
                    if (e.attributeValue("WorkTimeBegin") != null) {
                        zrate.setWorktime(e.attributeValue("WorkTimeBegin"));
                    }
                    if (e.attributeValue("WorkTimeEnd") != null) {
                        zrate.setAfterworktime(e.attributeValue("WorkTimeEnd"));
                    }
                    if (e.attributeValue("RefundTimeEnd") != null) {
                        zrate.setOnetofivewastetime(e.attributeValue("RefundTimeEnd"));
                    }
                    String Rewards = e.attributeValue("Rewards");
                    if (e.attributeValue("Remark") != null) {
                        zrate.setRemark(e.attributeValue("Remark"));
                    }
                    //0：BSP；1：B2B；2：BSP自动出票；3：B2B自动出票；4：CRS
                    if (e.attributeValue("ET") != null) {
                        if ("0".equals(e.attributeValue("ET")) || "2".equals(e.attributeValue("ET"))) {
                            zrate.setTickettype(1);
                        }
                        else {
                            zrate.setTickettype(2);
                        }
                    }
                    if (zrate.getAgentid() != null && !zrate.getAgentid().equals("") && zrate.getAgentcode() != null
                            && !zrate.getAgentcode().equals("")) {
                        zrates.add(zrate);
                    }
                }
            }
            else {
                int templ = getRandomInt();
                WriteLog.write("今日", "getZrateByFlightNumber:" + templ + ":0:" + str.toString());
                WriteLog.write("今日", "getZrateByFlightNumber:" + templ + ":1:" + result);
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrates;
    }

    /**
     * 根据航班号查询政策(往返)
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @author chendong 2013年6月20日11:32:19
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber1(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        scity = scity.split(",")[0];
        ecity = ecity.split(",")[0];
        sdate = sdate.split(",")[0].substring(0, 10);
        flightnumber = flightnumber.split(",")[0];
        cabin = cabin.split(",")[0];
        try {
            JinRiRateServerStub stub = new JinRiRateServerStub();
            JinRiRateServerStub.GetRateList rateList = new JinRiRateServerStub.GetRateList();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Policy-Request>");
            str.append("<Request username=\"" + jinRiBook.getJRUSER() + "\" scity=\"" + scity + "\" ecity=\"" + ecity
                    + "\" date=\"" + sdate + "\" aircome=\"" + flightnumber.substring(0, 2)
                    + "\" vayagetype=\"1\" amount=\"10\" FlightNo=\"" + flightnumber + "\" cabin=\"" + cabin
                    + "\" rateway='");
            if (cabin.equals("X")) {
                str.append("1");
            }
            else {
                str.append("2");
            }
            str.append("' />");
            str.append("</JIT-Policy-Request>");
            rateList.setData(str.toString());
            int templ = getRandomInt();
            WriteLog.write("今日", "getZrateByFlightNumber:" + templ + ":0:" + str.toString());
            JinRiRateServerStub.GetRateListResponse response = stub.getRateList(rateList);
            String result = response.getGetRateListResult();
            System.out.println(result);
            WriteLog.write("今日", "getZrateByFlightNumber:" + templ + ":1:" + result);
            if (result.length() < 10) {
                return zrates;
            }
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            if (root.elements("Response").size() > 0) {
                List<org.dom4j.Element> list = root.elements("Response");
                // if (list.size() > 0) {
                for (int a = 0; a < list.size(); a++) {
                    Zrate zrate = new Zrate();
                    org.dom4j.Element e = list.get(a);
                    zrate.setAgentid(6L);
                    zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    zrate.setCreateuser("job");
                    zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    zrate.setModifyuser("job");
                    zrate.setTickettype(1);
                    zrate.setIsenable(1);
                    if (e.attributeValue("PolicyId") != null) {
                        zrate.setOutid(e.attributeValue("PolicyId"));
                    }
                    if (e.attributeValue("RateId") != null) {
                        zrate.setAgentcode(e.attributeValue("RateId"));
                    }
                    if (e.attributeValue("ScityE") != null) {
                        zrate.setDepartureport(e.attributeValue("ScityE"));
                    }
                    if (e.attributeValue("EcityE") != null) {
                        zrate.setArrivalport(e.attributeValue("EcityE"));
                    }
                    if (e.attributeValue("AirComE") != null) {
                        if (e.attributeValue("AirComE").length() > 2) {
                            zrate.setAircompanycode(e.attributeValue("AirComE").substring(0, 2));
                            zrate.setFlightnumber(e.attributeValue("AirComE"));
                        }
                        else {
                            zrate.setAircompanycode(e.attributeValue("AirComE"));
                        }
                    }
                    if (e.attributeValue("NoAirComE") != null) {
                        zrate.setWeeknum(e.attributeValue("NoAirComE"));
                    }
                    if (e.attributeValue("WorkTimeBegin") != null) {
                        zrate.setWorktime(e.attributeValue("WorkTimeBegin"));
                    }
                    if (e.attributeValue("WorkTimeEnd") != null) {
                        zrate.setAfterworktime(e.attributeValue("WorkTimeEnd"));
                    }
                    if (e.attributeValue("SP") != null) {
                        zrate.setSpeed(e.attributeValue("SP"));
                    }

                    if (e.attributeValue("Cabin") != null) {
                        zrate.setCabincode(e.attributeValue("Cabin"));
                    }
                    if (e.attributeValue("VoyageType") != null) {
                        if (e.attributeValue("VoyageType").equals("0")) {
                            zrate.setVoyagetype("1");
                        }
                        else if (e.attributeValue("VoyageType").equals("1")) {
                            zrate.setVoyagetype("2");
                        }
                        else {
                            zrate.setVoyagetype("3");
                        }
                    }
                    String UserType = e.attributeValue("UserType");
                    if (e.attributeValue("RateType") != null) {
                        String rateType = e.attributeValue("RateType");
                        if (rateType.equals("0")) {
                            zrate.setGeneral(1L);
                        }
                        else {
                            zrate.setGeneral(2L);
                        }
                    }
                    if (e.attributeValue("Discounts") != null) {
                        zrate.setRatevalue(Float.parseFloat(e.attributeValue("Discounts")));
                    }
                    if (e.attributeValue("Sdate") != null) {
                        zrate.setBegindate(dateToTimestamp(e.attributeValue("Sdate")));
                    }
                    if (e.attributeValue("Edate") != null) {
                        zrate.setEnddate(dateToTimestamp(e.attributeValue("Edate")));
                    }
                    if (e.attributeValue("WorkTimBegin") != null) {
                        zrate.setWorktime(e.attributeValue("WorkTimBegin"));
                    }
                    if (e.attributeValue("WorkTImeEnd") != null) {
                        zrate.setAfterworktime(e.attributeValue("WorkTImeEnd"));
                    }
                    String Rewards = e.attributeValue("Rewards");
                    if (e.attributeValue("Remark") != null) {
                        zrate.setRemark(e.attributeValue("Remark"));
                    }
                    if (zrate.getAfterworktime() != null && !zrate.getAfterworktime().equals("")) {
                        if (zrate.getFlightnumber() != null) {
                            if (zrate.getFlightnumber().indexOf(flightnumber) != -1) {
                                zrates.add(zrate);
                            }
                        }
                        else if (zrate.getWeeknum() != null) {
                            if (zrate.getWeeknum().indexOf(flightnumber) == -1) {
                                zrates.add(zrate);
                            }
                        }
                        else {
                            zrates.add(zrate);
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrates;
    }

    /**
     * 根据政策id获得政策详情
     * @param outid	政策id
     * @param scity	出发三字码
     * @param ecity 到达三字码
     * @param sdate 出发时间
     * @param flightnumber	航班号
     * @param cabin	舱位
     * @return
     */
    public static Zrate getZrateByZrateID(String outid, String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        Zrate zrate = new Zrate();
        return zrate;
    }

    /**
     * 今日查询航班的方法
     * @param orgAirportCode
     * @param dstAirportCode
     * @param date
     * @param airlineCode
     * @param onlyAvailableSeat
     * @param onlyNormalCommision
     * @param onlyOnWorkingCommision
     * @param onlySelfPNR
     * @return
     * @author chendong 2014-8-7 09:45:24
     */
    public static List<FlightInfo> getAvailableFlightWithPriceAndCommision(String orgAirportCode,
            String dstAirportCode, String date, String airlineCode, int onlyAvailableSeat, int onlyNormalCommision,
            int onlyOnWorkingCommision, int onlySelfPNR) {
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        try {
            JinRiFlightServerStub stub = new JinRiFlightServerStub();
            JinRiFlightServerStub.GetFlightList_V1 getFlightList_V1 = new JinRiFlightServerStub.GetFlightList_V1();
            StringBuffer str = new StringBuffer();
            str.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
            str.append("<JIT-Flight-Request>");
            str.append("<Request username=\"" + JRUSER + "\" scity=\"" + orgAirportCode + "\" ecity=\""
                    + dstAirportCode + "\" date=\"" + date + "\" cabin=\"A\" isshosspecial=\"T\" />");
            str.append("</JIT-Flight-Request>");
            System.out.println(str.toString());
            int random = new Random().nextInt(10000);
            getFlightList_V1.setXml(str.toString());
            JinRiFlightServerStub.GetFlightList_V1Response response = stub.getFlightList_V1(getFlightList_V1);
            String result = response.getGetFlightList_V1Result();
            //          String result = "<?xml version=\"1.0\" encoding=\"gb2312\"?><JIT-Flight-Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"HO1252\" AirLine=\"HO\" FlightType=\"320\" Stime=\"06:35\" Etime=\"08:45\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"R\" N=\"A\" D=\"45\" P=\"510\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"55\" P=\"620\" T=\"0\" /><Cabin C=\"V\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"65\" P=\"730\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"T\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"8\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"8\" D=\"270\" P=\"3050\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5138\" AirLine=\"MU\" FlightType=\"333\" Stime=\"07:00\" Etime=\"09:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"380\" T=\"1\" /><Cabin C=\"V\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"S\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"R\" N=\"A\" D=\"65\" P=\"730\" T=\"0\" /><Cabin C=\"N\" N=\"A\" D=\"70\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"73\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"77\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"A\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1831\" AirLine=\"CA\" FlightType=\"33A\" Stime=\"07:30\" Etime=\"09:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"V\" N=\"A\" D=\"45\" P=\"510\" T=\"0\" /><Cabin C=\"G\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"Q\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"O\" N=\"3\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"6\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"FM9108\" AirLine=\"FM\" FlightType=\"75B\" Stime=\"07:35\" Etime=\"09:45\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"380\" T=\"1\" /><Cabin C=\"V\" N=\"A\" D=\"45\" P=\"570\" T=\"0\" /><Cabin C=\"S\" N=\"A\" D=\"50\" P=\"680\" T=\"0\" /><Cabin C=\"R\" N=\"A\" D=\"55\" P=\"730\" T=\"0\" /><Cabin C=\"N\" N=\"A\" D=\"60\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"65\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"70\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"75\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"80\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"85\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"230\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"HU7607\" AirLine=\"HU\" FlightType=\"340\" Stime=\"07:50\" Etime=\"09:55\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T1,T2\"><Cabins><Cabin C=\"Q\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"A\" N=\"2\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"Z\" N=\"6\" D=\"0\" P=\"1470\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"300\" P=\"2830\" T=\"0\" /><Cabin C=\"R\" N=\"8\" D=\"380\" P=\"3500\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5102\" AirLine=\"MU\" FlightType=\"333\" Stime=\"08:00\" Etime=\"10:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"380\" T=\"1\" /><Cabin C=\"S\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"R\" N=\"A\" D=\"65\" P=\"730\" T=\"0\" /><Cabin C=\"N\" N=\"A\" D=\"70\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"73\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"77\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1501\" AirLine=\"CA\" FlightType=\"773\" Stime=\"08:30\" Etime=\"10:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"G\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"Q\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"O\" N=\"3\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"8\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"300\" P=\"3390\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5166\" AirLine=\"MU\" FlightType=\"76A\" Stime=\"08:30\" Etime=\"10:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"380\" T=\"1\" /><Cabin C=\"V\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"S\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"R\" N=\"A\" D=\"65\" P=\"730\" T=\"0\" /><Cabin C=\"N\" N=\"A\" D=\"70\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"73\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"77\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"HU7605\" AirLine=\"HU\" FlightType=\"767\" Stime=\"08:45\" Etime=\"10:55\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T1,T2\"><Cabins><Cabin C=\"Q\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"A\" N=\"7\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"Z\" N=\"A\" D=\"0\" P=\"1470\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"300\" P=\"2830\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5104\" AirLine=\"MU\" FlightType=\"333\" Stime=\"09:00\" Etime=\"11:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"450\" T=\"1\" /><Cabin C=\"S\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"R\" N=\"A\" D=\"65\" P=\"730\" T=\"0\" /><Cabin C=\"N\" N=\"A\" D=\"70\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"73\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"77\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"7\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1519\" AirLine=\"CA\" FlightType=\"77S\" Stime=\"09:30\" Etime=\"11:35\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"G\" N=\"A\" D=\"50\" P=\"570\" T=\"0\" /><Cabin C=\"Q\" N=\"A\" D=\"60\" P=\"680\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"O\" N=\"2\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"A\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5106\" AirLine=\"MU\" FlightType=\"333\" Stime=\"09:55\" Etime=\"11:55\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"450\" T=\"1\" /><Cabin C=\"N\" N=\"A\" D=\"70\" P=\"780\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"73\" P=\"820\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"77\" P=\"870\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"A\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1531\" AirLine=\"CA\" FlightType=\"77S\" Stime=\"10:30\" Etime=\"12:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"L\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"2\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5108\" AirLine=\"MU\" FlightType=\"333\" Stime=\"11:00\" Etime=\"13:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"450\" T=\"1\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1557\" AirLine=\"CA\" FlightType=\"77L\" Stime=\"11:30\" Etime=\"13:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5146\" AirLine=\"MU\" FlightType=\"76A\" Stime=\"11:30\" Etime=\"13:45\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Z\" N=\"A\" D=\"\" P=\"450\" T=\"1\" /><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"7\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5110\" AirLine=\"MU\" FlightType=\"333\" Stime=\"11:55\" Etime=\"14:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5112\" AirLine=\"MU\" FlightType=\"333\" Stime=\"12:55\" Etime=\"15:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1517\" AirLine=\"CA\" FlightType=\"773\" Stime=\"13:30\" Etime=\"15:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"300\" P=\"3390\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5114\" AirLine=\"MU\" FlightType=\"33E\" Stime=\"14:00\" Etime=\"16:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1521\" AirLine=\"CA\" FlightType=\"773\" Stime=\"14:30\" Etime=\"16:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"H\" N=\"3\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"M\" N=\"4\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /><Cabin C=\"P\" N=\"4\" D=\"300\" P=\"3390\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5140\" AirLine=\"MU\" FlightType=\"76A\" Stime=\"14:30\" Etime=\"16:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5116\" AirLine=\"MU\" FlightType=\"333\" Stime=\"15:00\" Etime=\"17:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1515\" AirLine=\"CA\" FlightType=\"773\" Stime=\"15:30\" Etime=\"17:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /><Cabin C=\"P\" N=\"8\" D=\"300\" P=\"3390\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5200\" AirLine=\"MU\" FlightType=\"333\" Stime=\"15:45\" Etime=\"18:00\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5118\" AirLine=\"MU\" FlightType=\"333\" Stime=\"16:00\" Etime=\"18:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1549\" AirLine=\"CA\" FlightType=\"77S\" Stime=\"16:30\" Etime=\"18:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5120\" AirLine=\"MU\" FlightType=\"333\" Stime=\"17:00\" Etime=\"19:15\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1855\" AirLine=\"CA\" FlightType=\"77S\" Stime=\"17:30\" Etime=\"19:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"HU7601\" AirLine=\"HU\" FlightType=\"787\" Stime=\"17:35\" Etime=\"19:45\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T1,T2\"><Cabins><Cabin C=\"L\" N=\"5\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"A\" N=\"6\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"Z\" N=\"A\" D=\"0\" P=\"1700\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"300\" P=\"2830\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5122\" AirLine=\"MU\" FlightType=\"333\" Stime=\"18:00\" Etime=\"20:15\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CZ3907\" AirLine=\"CZ\" FlightType=\"333\" Stime=\"18:05\" Etime=\"20:20\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"M\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"250\" P=\"3160\" T=\"0\" /><Cabin C=\"A\" N=\"4\" D=\"400\" P=\"4520\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1885\" AirLine=\"CA\" FlightType=\"32A\" Stime=\"18:30\" Etime=\"20:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5124\" AirLine=\"MU\" FlightType=\"333\" Stime=\"19:00\" Etime=\"21:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1857\" AirLine=\"CA\" FlightType=\"77L\" Stime=\"19:30\" Etime=\"21:50\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"W\" N=\"A\" D=\"160\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"1\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5126\" AirLine=\"MU\" FlightType=\"333\" Stime=\"20:00\" Etime=\"22:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"CA1589\" AirLine=\"CA\" FlightType=\"330\" Stime=\"20:30\" Etime=\"22:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T3,T2\"><Cabins><Cabin C=\"H\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"990\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"100\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"A\" N=\"1\" D=\"80\" P=\"2830\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"2\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"FM9106\" AirLine=\"FM\" FlightType=\"75A\" Stime=\"20:30\" Etime=\"22:45\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"230\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"HU7603\" AirLine=\"HU\" FlightType=\"738\" Stime=\"20:50\" Etime=\"22:55\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T1,T2\"><Cabins><Cabin C=\"M\" N=\"A\" D=\"70\" P=\"790\" T=\"0\" /><Cabin C=\"L\" N=\"A\" D=\"75\" P=\"850\" T=\"0\" /><Cabin C=\"K\" N=\"A\" D=\"80\" P=\"900\" T=\"0\" /><Cabin C=\"H\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"Z\" N=\"5\" D=\"0\" P=\"1470\" T=\"1\" /><Cabin C=\"F\" N=\"7\" D=\"300\" P=\"2830\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5128\" AirLine=\"MU\" FlightType=\"33E\" Stime=\"21:00\" Etime=\"23:10\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5162\" AirLine=\"MU\" FlightType=\"76A\" Stime=\"21:30\" Etime=\"23:40\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response><Response Sdate=\"2013-11-29\" Scity=\"PEK\" Ecity=\"SHA\" FlightNo=\"MU5182\" AirLine=\"MU\" FlightType=\"333\" Stime=\"22:00\" Etime=\"23:55\" Stop=\"0\" EPiao=\"E\" Tax=\"170\" AirTerminal=\"T2,T2\"><Cabins><Cabin C=\"H\" N=\"A\" D=\"81\" P=\"920\" T=\"0\" /><Cabin C=\"E\" N=\"A\" D=\"85\" P=\"960\" T=\"0\" /><Cabin C=\"M\" N=\"A\" D=\"90\" P=\"1020\" T=\"0\" /><Cabin C=\"B\" N=\"A\" D=\"95\" P=\"1070\" T=\"0\" /><Cabin C=\"Y\" N=\"A\" D=\"100\" P=\"1130\" T=\"0\" /><Cabin C=\"P\" N=\"A\" D=\"0\" P=\"1130\" T=\"1\" /><Cabin C=\"F\" N=\"A\" D=\"280\" P=\"3160\" T=\"0\" /></Cabins></Response></JIT-Flight-Response>";
            WriteLog.write("SEARCH_jinri", random + ":" + str.toString());
            if (result.length() < 10) {
                WriteLog.write("SEARCH_jinri", random + ":" + result);
                return listFlightInfoAll;
            }
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            List list = root.elements("Response");
            Iterator it = list.iterator();
            System.out.println("JINRI:" + list.size());
            while (it.hasNext()) {
                try {
                    org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                    String Sdate = elmt.attributeValue("Sdate");
                    String Scity = elmt.attributeValue("Scity");
                    String Ecity = elmt.attributeValue("Ecity");
                    String FlightNo = elmt.attributeValue("FlightNo");
                    String AirLine = elmt.attributeValue("AirLine");
                    String FlightType = elmt.attributeValue("FlightType");
                    String Stime = elmt.attributeValue("Stime");//08:15
                    String Etime = elmt.attributeValue("Etime");
                    String Stop = elmt.attributeValue("Stop");
                    String EPiao = elmt.attributeValue("EPiao");
                    String Tax = elmt.attributeValue("Tax");
                    String AirTerminal = elmt.attributeValue("AirTerminal");
                    //                  System.out.println(Sdate + "-" + Scity + "-" + Ecity + "-"
                    //                          + FlightNo + "-" + AirLine + "-" + FlightType + "-"
                    //                          + Stime + "-" + Etime + "-" + Stop + "-" + EPiao
                    //                          + "-" + Tax + "-" + AirTerminal);
                    FlightInfo flightInfo = new FlightInfo();
                    flightInfo.setStartAirport(Scity);// 起飞机场
                    flightInfo.setStartAirportName("");// 起飞起场名称
                    flightInfo.setEndAirport(Ecity);// 到达机场
                    flightInfo.setEndAirportName("");// 到达机场名称
                    flightInfo.setStartAirportCity(Scity);// 起飞起场城市名称
                    flightInfo.setAirline(FlightNo);// 航线
                    flightInfo.setAirCompany(AirLine);// 航空公司
                    flightInfo.setAirCompanyName(AirLine);// 航空公司名称
                    flightInfo.setAirportFee(50);// 机场建设费
                    flightInfo.setFuelFee(Float.parseFloat(Tax) - 50);// 燃油费
                    flightInfo.setDistance("0");// 里程数
                    flightInfo.setMeal(false);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddHH:mm");
                    Date startDate = dateFormat.parse(Sdate + Stime);
                    Date arriveDate = dateFormat.parse(Sdate + Etime);
                    flightInfo.setDepartTime(new Timestamp(startDate.getTime()));// 起飞时间
                    flightInfo.setArriveTime(new Timestamp(arriveDate.getTime()));// 到达时间
                    //              flightInfo.setYPrice(basePrice.floatValue());// 全价价格
                    //              if (codeShare != null && codeShare) {
                    //                  flightInfo.setIsShare(1);// 是否是共享航班
                    //              } else {
                    //                  flightInfo.setIsShare(0);
                    //              }
                    //              if (shareNum != null) {
                    //                  flightInfo.setShareFlightNumber(shareNum);// 共享航班号
                    //              }
                    flightInfo.setAirplaneType(FlightType);// 飞机型号
                    flightInfo.setAirplaneTypeDesc("");// 飞机型号描述
                    flightInfo.setOffPointAT(AirTerminal.split(",")[0]);//  出发航站楼
                    flightInfo.setBorderPointAT(AirTerminal.split(",")[1]);//到达航站楼
                    if (Stop.equals("1")) {
                        flightInfo.setStop(true);
                        flightInfo.setIsStopInfo("1");
                    }
                    else {
                        flightInfo.setStop(false);
                    }
                    List cabinlist = elmt.elements("Cabin");
                    List<CarbinInfo> listCabinAll = new ArrayList<CarbinInfo>();
                    CarbinInfo lowCabinInfo = new CarbinInfo();
                    for (int i = 0; i < cabinlist.size(); i++) {
                        org.dom4j.Element cabinelmt = (org.dom4j.Element) cabinlist.get(i);
                        CarbinInfo cabin = new CarbinInfo();

                        if (cabinelmt.attributeValue("N") != null && cabinelmt.attributeValue("N").equals("A")) {
                            cabin.setSeatNum("9");
                        }
                        else {
                            cabin.setSeatNum(cabinelmt.attributeValue("N"));
                        }
                        if (cabinelmt.attributeValue("D").length() > 0) {
                            cabin.setDiscount(Float.parseFloat(cabinelmt.attributeValue("D")));
                        }
                        else {
                            cabin.setDiscount(0F);
                        }
                        cabin.setCabin(cabinelmt.attributeValue("C"));
                        String cabinTypeName = "经济舱";
                        String cabinString = cabinelmt.attributeValue("C");
                        if ("A".equals(cabinString) || "P".equals(cabinString) || "C".equals(cabinString)) {
                            cabinTypeName = "商务舱";
                        }
                        else if ("F".equals(cabinString)) {
                            cabinTypeName = "头等舱";
                        }
                        cabin.setCabintypename(cabinTypeName);
                        if ("1".equals(cabinelmt.attributeValue("T"))) {
                            cabin.setSpecial(true);
                        }
                        else {
                            cabin.setSpecial(false);
                        }
                        cabin.setPrice(Float.parseFloat(cabinelmt.attributeValue("P")));
                        if ("Y".equals(cabinelmt.attributeValue("C"))) {
                            flightInfo.setYPrice(cabin.getPrice());
                        }
                        if (cabin.getPrice() > 0) {
                            cabin.setZrate(AirUtil.getbaseZrate());
                            listCabinAll.add(cabin);
                        }
                    }
                    Collections.sort(listCabinAll, new Comparator<CarbinInfo>() {
                        @Override
                        public int compare(CarbinInfo o1, CarbinInfo o2) {
                            if (o1.getPrice() == null || o2.getPrice() == null) {
                                return 1;
                            }
                            else {
                                // TODO Discount排序
                                if (o1.getPrice() > o2.getPrice()) {
                                    return 1;
                                }
                                else if (o1.getPrice() < o2.getPrice()) {
                                    return -1;

                                }
                            }
                            return 0;
                        }
                    });
                    flightInfo.setCarbins(listCabinAll);

                    for (int j = 0; j < listCabinAll.size(); j++) {
                        if ((listCabinAll.get(j).getDiscount() == 0) && flightInfo.getYPrice() != null) {
                            Float discount = listCabinAll.get(j).getPrice() / flightInfo.getYPrice() * 100;
                            DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
                            format.applyPattern("###00");
                            try {
                                String discount_S = format.format(discount);
                                listCabinAll.get(j).setDiscount(Float.parseFloat(discount_S));
                            }
                            catch (Exception e) {
                            }
                        }
                    }

                    CarbinInfo tempCabinInfo = (CarbinInfo) listCabinAll.get(0);
                    if (tempCabinInfo.getCabin() != null) {
                        lowCabinInfo.setCabin(tempCabinInfo.getCabin());
                    }
                    if (tempCabinInfo.getRatevalue() != null) {
                        lowCabinInfo.setRatevalue(tempCabinInfo.getRatevalue());
                    }
                    if (tempCabinInfo.getCabinRemark() != null) {
                        lowCabinInfo.setCabinRemark(tempCabinInfo.getCabinRemark());
                    }
                    else {
                        lowCabinInfo.setCabinRemark("");
                    }
                    if (tempCabinInfo.getCabinRules() != null) {
                        lowCabinInfo.setCabinRules(tempCabinInfo.getCabinRules());
                    }
                    else {
                        lowCabinInfo.setCabinRules("");
                    }
                    if (tempCabinInfo.getDiscount() != null) {
                        lowCabinInfo.setDiscount(tempCabinInfo.getDiscount());
                    }
                    if (tempCabinInfo.getLevel() != null) {
                        lowCabinInfo.setLevel(tempCabinInfo.getLevel());
                    }
                    else {
                        lowCabinInfo.setLevel(1);
                    }
                    if (tempCabinInfo.getPrice() != null) {
                        lowCabinInfo.setPrice(tempCabinInfo.getPrice());
                    }
                    else {
                        lowCabinInfo.setPrice(0f);
                    }
                    if (tempCabinInfo.getSeatNum() != null) {
                        lowCabinInfo.setSeatNum(tempCabinInfo.getSeatNum());
                    }
                    else {
                        lowCabinInfo.setSeatNum("0");
                    }
                    if (tempCabinInfo.getCabintypename() != null) {
                        lowCabinInfo.setCabintypename(tempCabinInfo.getCabintypename());
                    }
                    else {
                        lowCabinInfo.setCabintypename("");
                    }
                    if (tempCabinInfo.isSpecial()) {
                        lowCabinInfo.setSpecial(true);
                    }
                    else {
                        lowCabinInfo.setSpecial(false);
                    }
                    lowCabinInfo.setZrate(AirUtil.getbaseZrate());
                    flightInfo.setLowCarbin(lowCabinInfo);

                    listFlightInfoAll.add(flightInfo);
                }
                catch (ParseException e) {
                    // TODO: handle exception
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (DocumentException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return listFlightInfoAll;
    }

    public static void main(String[] args) {
        getAvailableFlightWithPriceAndCommision("PEK", "SHA", "2015-11-21", "", 0, 0, 0, 1);
    }

}
