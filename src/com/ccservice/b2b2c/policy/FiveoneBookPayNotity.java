package com.ccservice.b2b2c.policy;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.ccservice.inter.server.Server;
import com.ccservice.inter.job.WriteLog;

/**
 * 51book 支付成功同通知接口
 * @author Administrator
 *
 */
public class FiveoneBookPayNotity extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger=Logger.getLogger(this.getClass().getName());
	
	public void doGet(HttpServletRequest request,HttpServletResponse response){
		this.doPost(request, response);
	}
	public void doPost(HttpServletRequest request,HttpServletResponse response){
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String outno=request.getParameter("sequenceNo");
		String orderstatus=request.getParameter("orderStatus");
		float traemoney=0f;
		try{
		 traemoney=Float.valueOf(request.getParameter("preCharge"));
		}catch(Exception e){
			e.printStackTrace();
			logger.error("51pay", e);
		}
		WriteLog.write("51pay", request.getRemoteAddr());
		WriteLog.write("51pay", "sequenceNo:"+outno+",orderStatus:"+orderstatus+",preCharge:"+traemoney);
		String sql="UPDATE T_ORDERINFO SET C_EXTORDERPRICE="+traemoney+" , C_EXTORDERSTATUS="+2+" WHERE C_EXTORDERID='"+outno+"'";
		int num = Server.getInstance().getAirService().excuteOrderinfoBySql(sql);
		WriteLog.write("51pay", num+":"+sql);
		try {
			PrintWriter out;
			out = response.getWriter();
			if (num>0) {
				out.print("S");
			}else{
				out.print("F");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
