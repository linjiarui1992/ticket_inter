package com.ccservice.b2b2c.policy;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.util.Util;
import com.ccservice.inter.server.Server;

/**
 * 8000yi订单状态通知
 * @author 小寒
 *
 */
public class EightyiNotify extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request,HttpServletResponse response){
		System.out.println("八千翼通知访问");
		this.doPost(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response){
		Logger logger=Logger.getLogger(this.getClass().getName());
		logger.info("八千翼通知调用：");
			try {
				request.setCharacterEncoding("utf-8");
			} catch (UnsupportedEncodingException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		
		
		String platform=request.getParameter("platform");//平台名称
		String type=request.getParameter("type");//通知类型
		String outorderid=request.getParameter("orderguid");//订单号
		String orderstate=request.getParameter("orderstate");//订单状态
		String notifymsg=request.getParameter("notifymsg");//通知信息
		String key=request.getParameter("key");
		String message="platform:"+platform+".type:"+type+".outorderid:"+outorderid+"。orderstate："+orderstate+".notifymsg"+notifymsg;
		logger.info(message);
		System.out.println(message);
		System.out.println("key:"+key);
		//Key= $%^+订单号+通知信息+平台名称+$8000yi$
		String valkekstr="$%^"+outorderid+notifymsg+"8000YI"+"$8000yi$";
		String valkey="";
		try {
			valkey=HttpClient.MD5(valkekstr).toUpperCase();
		
			String s=Util.MD5(valkekstr).toUpperCase();
			System.out.println(s);
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("mykey:"+valkey);
		if((key!=null&&key.equals(valkey))||"2".equals(type)){//请先验证key,如key值不一致则不需处理该通知。由于出票key与本地加密key总
			if("2".equals(type)){//出票完成
				orderstate="3";//出票完成
			}
		  try{
			StringBuilder sql=new StringBuilder("UPDATE T_ORDERINFO SET C_EXTORDERSTATUS="+orderstate);
			if(type.equals("1")){//支付完成
				orderstate="2";
				String megs[]=notifymsg.split("\\^");
				String price=megs[megs.length-1];
				sql.append(",C_EXTORDERPRICE="+price);
			}
			if(type.equals("3")||type.equals("4")){
				//订单号^交易号^退款金额^成功或者拒绝的描述^手续费 (退票通知)
				String megs[]=notifymsg.split("\\^");
				try{
				float returnprice=Float.valueOf(megs[2]);
				sql.append(",C_EXTRETURNPRICE="+returnprice);
				}catch(Exception e){
					
				}
				
			}
			
			sql.append(" WHERE C_EXTORDERID='"+outorderid+"'");
			if(type.equals("2")){
				String megs[]=notifymsg.split("\\^");
				String names[]=megs[1].split("\\|");
				String tikcets[]=megs[2].split("\\|");				
				for(int i=0;i<names.length;i++){
				sql.append(";UPDATE T_PASSENGER SET C_TICKETNUM='"+tikcets[i]+"' WHERE C_NAME='"+names[i]+"'");
				}
			}
			
			ISystemService service=Server.getInstance().getSystemService();
			System.out.println(sql.toString());
			service.findMapResultBySql(sql.toString(), null);
			PrintWriter out;
			
				out = response.getWriter();
				out.print("true");//接受到通知返回true
			
			
		  }catch(Exception e){
			  logger.info("哎呀。八千亿通知出错了："+e.getMessage());
		  }
		}
	}
	

}
