package com.ccservice.b2b2c.policy;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ccservice.inter.job.WriteLog;

/**
 * 此类为时间公共类
 * 所有处理时间方法写到这里
 * @author cd
 *
 */
public class TimeUtil {

    /**
     * 获取当天的时间，默认返回 yyyy-MM-dd
     * 
     * @param type 什么格式的    1:yyyy-MM-dd,2:yyyy-MM-dd HH,3:yyyy-MM-dd HH:mm,4:yyyy-MM-dd HH:mm:ss
     * @return 时间的字符串
     * @time 2014年8月30日 上午11:08:14
     * @author chendong
     */
    public static String gettodaydate(int type) {
        if (type == 1) {
            return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }
        if (type == 2) {
            return new SimpleDateFormat("yyyy-MM-dd HH").format(new Date());
        }
        if (type == 3) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        }
        if (type == 4) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        }
        if (type == 5) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date());
        }
        else {
            return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }
    }

    /**
     * 返回对应格式日期字符串
     * @param type 1:yyyy-MM-dd,2:yyyy-MM-dd HH,3:yyyy-MM-dd HH:mm,4:yyyy-MM-dd HH:mm:ss  5 HH:mm
     * @param date
     * @return
     * @time 2014年10月24日 下午4:45:58
     * @author wzc
     */
    public static String gettodaydate(int type, Date date) {
        if (type == 1) {
            return new SimpleDateFormat("yyyy-MM-dd").format(date);
        }
        if (type == 2) {
            return new SimpleDateFormat("yyyy-MM-dd HH").format(date);
        }
        if (type == 3) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        }
        if (type == 4) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        }
        if (type == 5) {
            return new SimpleDateFormat("HH:mm").format(date);
        }
        else {
            return new SimpleDateFormat("yyyy-MM-dd").format(date);
        }
    }

    /**
     * 获取当前时间
     * @return
     */
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 获取当前时间的前后多少天
     * 
     * @param type 什么格式的    1:yyyy-MM-dd,2:yyyy-MM-dd HH,3:yyyy-MM-dd HH:mm,4:yyyy-MM-dd HH:mm:ss
     * @param day 当前天数的前后天数
     * @return
     * @time 2014年9月1日 下午5:46:01
     * @author chendong
     */
    public static String gettodaydatebyfrontandback(int type, int day) {
        Date nowDate = new Date();
        Calendar now = Calendar.getInstance();
        now.setTime(nowDate);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        nowDate = now.getTime();
        if (type == 1) {
            return new SimpleDateFormat("yyyy-MM-dd").format(nowDate);
        }
        if (type == 2) {
            return new SimpleDateFormat("yyyy-MM-dd HH").format(nowDate);
        }
        if (type == 3) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(nowDate);
        }
        if (type == 4) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nowDate);
        }
        else {
            return new SimpleDateFormat("yyyy-MM-dd").format(nowDate);
        }
    }

    /**
     * 把字符串的日期转换为Date类型
     * 
     * @param date
     * @param type
     * @return
     * @time 2015年1月9日 下午3:39:44
     * @author chendong
     */
    public static Date parseStringtimeToDate(String date, int type) {
        Date sdate = new Date();
        try {
            if (type == 1) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(date);
            }
            if (type == 2) {
                return new SimpleDateFormat("yyyy-MM-dd HH").parse(date);
            }
            if (type == 3) {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
            }
            if (type == 4) {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
            }
            else {
                return new SimpleDateFormat("yyyy-MM-dd").parse(date);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return sdate;
    }

    /**
     * 传一个时间格式 【07:00-23:00】
     * 判断当前时间是否在这个时间之间
     * @param date 07:00-23:00
     * @return
     * @time 2015年5月15日 下午3:46:47
     * @author fiend
     */
    public static boolean get12306WorkTime(String timestr) {
        System.out.println("当前允许时间：" + timestr + "======" + TimeUtil.gettodaydate(4));
        String timebefore = timestr.split("-")[0];
        String timeend = timestr.split("-")[1];
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        try {
            Date dateBefor = df.parse(timebefore);
            Date dateAfter = df.parse(timeend);
            Date time = df.parse(df.format(new Date()));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
            WriteLog.write("getNowTime", e + "");
        }
        return false;//现在24小时,以后有需要再改为FALSE
    }

    public static void main(String[] args) {
        //        System.out.println(get12306WorkTime());
        //        System.out.println(gettodaydatebyfrontandback(1, -30));
    }
}
