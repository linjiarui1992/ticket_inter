package com.ccservice.b2b2c.policy;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.policy.ben.TianQubook;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 
 * 
 * @time 2014年10月20日 下午4:13:38
 * @author wzc
 * 天衢政策处理方法
 */
public class TianQuPolicy {
    static TianQubook tianqu = new TianQubook();

    public static void main(String[] args) {
        String msg = "";
        String result = "{\"statuscode\":\"F\",\"OrderNo\":\"YDXA1410211653329\",\"Remark\":\"请提供支付宝账号\"}";
        JSONObject obj = JSONObject.parseObject(result);
        String Remark = obj.getString("Remark");//返回信息
        String OrderNo = obj.getString("OrderNo");
        String statuscode = obj.getString("statuscode");//支付状态
        if ("S".equals(statuscode)) {
            msg = "S|" + Remark;
        }
        else {
            msg = "F|" + Remark;
        }
        System.out.println(msg);
    }

    /**
     * 
     * 天衢自动支付方法
     * @param orderinfo
     * @return
     * @time 2014年10月20日 下午6:24:11
     * @author wzc
     */
    public static String payorder(Orderinfo orderinfo) {
        String msg = "";
        try {
            String payurl = tianqu.getRequrl() + "/ccs_interface/AutopayJSON";//自动支付url 
            WriteLog.write("自动代扣", orderinfo.getId() + ":自维护政策:" + orderinfo.getExtorderid());
            WriteLog.write("自动代扣", orderinfo.getId() + ":payurl:" + payurl);
            JSONObject req = new JSONObject();
            req.put("username", tianqu.getUsername());
            req.put("password", tianqu.getPwd());
            req.put("ordernum", orderinfo.getExtorderid());
            req.put("cmd", "AIR");
            WriteLog.write("自动代扣", orderinfo.getId() + ":请求json:" + req.toString());
            StringBuffer result = SendPostandGet.submitPost(payurl, req.toString());
            WriteLog.write("自动代扣", orderinfo.getId() + ":返回json:" + result);
            if (result != null && result.toString().length() > 5) {
                JSONObject obj = JSONObject.parseObject(result.toString());
                String Remark = obj.getString("Remark");//返回信息
                String OrderNo = obj.getString("OrderNo");
                String statuscode = obj.getString("statuscode");//支付状态
                if ("S".equals(statuscode)) {
                    msg = "S|" + Remark;
                }
                else {
                    msg = "F|" + Remark;
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("自动代扣", orderinfo.getId() + ":" + e.getMessage() + "");
            e.printStackTrace();
        }
        return msg;
    }

    /**
     * 
     * 天衢自动支付方法
     * @param orderinfo
     * @return
     * @time 2014年10月20日 下午6:24:11
     * @author wzc
     */
    public static String autoPayOrder8L(Orderinfo orderinfo) {
        Long orderId = orderinfo.getId();
        String msg = "";
        String autopayurl = PropertyUtil.getValue("tq8LAutoPayUrl", "air.properties");
        try {
            String payurl = autopayurl;//自动支付url
            WriteLog.write("自动代扣", orderId + ":自维护政策:" + orderinfo.getExtorderid());
            JSONObject req = new JSONObject();
            req.put("cmd", "air8LAutoPay");
            req.put("username", tianqu.getUsername());
            req.put("password", tianqu.getPwd());
            req.put("ordernum", orderinfo.getExtorderid());
            WriteLog.write("自动代扣", orderId + ":请求payurl:" + payurl);
            WriteLog.write("自动代扣", orderId + ":请求json:" + req.toString());
            StringBuffer result = SendPostandGet.submitPost(payurl, req.toString());
            WriteLog.write("自动代扣", orderId + ":返回json:" + result);
            if (result != null && result.toString().length() > 5) {
                JSONObject obj = JSONObject.parseObject(result.toString());
                String Remark = obj.getString("Remark");//返回信息
                String OrderNo = obj.getString("OrderNo");
                String statuscode = obj.getString("statuscode");//支付状态
                if ("S".equals(statuscode)) {
                    msg = "S|" + Remark;
                }
                else {
                    msg = "F|" + Remark;
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("自动代扣", e.getMessage() + "");
            e.printStackTrace();
        }
        return msg;
    }
}
