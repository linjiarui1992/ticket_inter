/**
 * 
 */
package com.ccservice.b2b2c.policy.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ccservice.b2b2c.base.fflight.InstancePrice;
import com.ccservice.b2b2c.base.zrate.Zrate;

/**
 * 
 * @time 2016年3月24日 下午4:50:24
 * @author chendong
 */
public class ZrateUtil {

    /**
     * 国际机票政策排序
     * 作者：邹远超
     * 日期：2014年5月29日
     * 说明：
     * @param zrates
     * @param count
     * @return
     */
    public static List<Zrate> sort(List<Zrate> zrates) {
        Collections.sort(zrates, new Comparator<Zrate>() {
            @Override
            public int compare(Zrate o1, Zrate o2) {
                return (int) (o2.getRatevalue().compareTo(o1.getRatevalue()));
            }
        });

        return zrates;
    }

    /**
     * 国际机票政策排序
     * 作者：邹远超
     * 日期：2014年5月29日
     * 说明：
     * @param zrates
     * @param count
     * @return
     */
    public static List<Zrate> sortZrateInter(List<Zrate> zrates, int count) {
        Collections.sort(zrates, new Comparator<Zrate>() {
            @Override
            public int compare(Zrate o1, Zrate o2) {
                return (int) (o2.getRatevalue().compareTo(o1.getRatevalue()));
            }
        });

        if (zrates.size() > count) {
            zrates = zrates.subList(0, count);
        }
        return zrates;
    }

    /**
     * 国际机票政策排序
     * 作者：邹远超
     * 日期：2014年5月29日
     * 说明：
     * @param zrates
     * @param count
     * @return
     */
    public static List<InstancePrice> sortInstancePrice(List<InstancePrice> instance) {
        Collections.sort(instance, new Comparator<InstancePrice>() {
            @Override
            public int compare(InstancePrice o1, InstancePrice o2) {
                return (int) (o1.getPeopleType() - o2.getPeopleType());
            }
        });
        return instance;
    }
}
