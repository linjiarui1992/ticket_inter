package com.ccservice.b2b2c.policy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataTable;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 2012年7月24日
 * 
 * @author 陈栋
 * 
 */
public class SupplyMethod {

    Log log = LogFactory.getLog(SupplyMethod.class);

    /**
       * @param format
       *            时间是什么格式的eg:HHmm
       * @param afterworktime要比较的时间
       * @param time
       *            当前时间加上多少毫秒后和afterworktime比较
       * @return 比较当前时间加上time毫秒后如果在afterworktime之前则返回true否则返回false
       */
    public static boolean compareTime(String afterworktime) {
        boolean flag = true;
        if (afterworktime.indexOf(":") > 0) {
            afterworktime = afterworktime.replace(":", "");
        }
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        String temp = sdf2.format(new Date());
        afterworktime = temp + afterworktime;
        try {
            Date nowDate = new Date();
            nowDate.setMinutes(nowDate.getMinutes());
            Date afterWorkDate = new Date();
            if (afterworktime.length() >= 12) {
                afterWorkDate = sdf1.parse(afterworktime.substring(0, 12));
            }
            else if (afterworktime.length() == 8) {
                afterWorkDate = sdf2.parse(afterworktime);
            }
            flag = afterWorkDate.after(nowDate);
        }
        catch (ParseException e) {
            WriteLog.write("compareTime_EX", afterworktime);
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * @param format
     *            时间是什么格式的eg:HHmm
     * @param afterworktime要比较的时间
     * @param time
     *            当前时间加上多少毫秒后和afterworktime比较
     * @return 比较当前时间加上time毫秒前如果在afterworktime之前则返回true否则返回false
     */
    public static boolean compareTimebefore(String worktime) {
        boolean flag = true;
        if (worktime.indexOf(":") > 0) {
            worktime = worktime.replace(":", "");
        }
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        String temp = sdf2.format(new Date());
        worktime = temp + worktime;
        try {
            Date nowDate = new Date();
            nowDate.setMinutes(nowDate.getMinutes());
            Date afterWorkDate = new Date();
            if (worktime.length() >= 12) {
                afterWorkDate = sdf1.parse(worktime.substring(0, 12));
            }
            else if (worktime.length() == 8) {
                afterWorkDate = sdf2.parse(worktime);
            }
            flag = afterWorkDate.before(nowDate);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 返回当前时间在上班时间的政策
     * @param zrates
     * @return
     */
    public List<Zrate> getBestZrate(List<Zrate> zrates) {
        List<Zrate> Tzrates = new ArrayList<Zrate>();
        try {
            if (zrates.size() > 1) {
                for (int j = 0; j < zrates.size(); j++) {
                    boolean afterWorkflag = true;
                    if (zrates.get(j).getAfterworktime() != null) {
                        afterWorkflag = compareTime(zrates.get(j).getAfterworktime());
                    }
                    // cd2012年11月8日增加供应上班时间
                    boolean beforerWorkflag = true;
                    if (zrates.get(j).getWorktime() != null) {
                        beforerWorkflag = compareTimebefore(zrates.get(j).getWorktime());
                    }
                    if (afterWorkflag && beforerWorkflag) {
                        Tzrates.add(zrates.get(j));
                    }
                    else {
                        continue;
                    }
                }
            }
            else {
                Tzrates.addAll(zrates);
            }
        }
        catch (Exception e) {
            UtilMethod.writeEx("SupplyMethod", e);
            e.printStackTrace();
        }
        if (Tzrates.size() == 0) {
            Tzrates.add(getbaseZrate());
        }
        Tzrates = swapZrates(Tzrates, 5L);
        return Tzrates;
    }

    /**
     * 调整位置 如果前两个的返点一样,并且第一个的agentid不是参数的agentid就把那个政策提到第一位 
     * 
     * @param zrates
     * @param agentid 需要提前的agentid
     * @return
     * @time 2014年9月1日 上午11:07:33
     * @author chendong
     */
    public List<Zrate> swapZrates(List<Zrate> zrates, Long agentid) {
        if (zrates.size() > 1) {
            if (zrates.get(0).getRatevalue().equals(zrates.get(1).getRatevalue())
                    && zrates.get(1).getAgentid() == agentid && zrates.get(0).getAgentid() != agentid) {
                Collections.swap(zrates, 1, 0);
            }
        }
        return zrates;
    }

    /**
     * 选出最高返点的政策
     * 
     * @param zrates
     * @return
     */
    @SuppressWarnings("static-access")
    public Zrate getBestZrate(List<Zrate> zrates, Zrate tempZrate, Orderinfo order) {
        Zrate zrate = new Zrate();
        try {
            zrates = getForWordZrate(zrates, tempZrate);
            if (zrates.size() > 1) {
                for (int j = 0; j < zrates.size(); j++) {
                    boolean afterWorkflag = compareTime(zrates.get(j).getAfterworktime());
                    // cd2012年11月8日增加供应上班时间
                    boolean beforerWorkflag = compareTimebefore(zrates.get(j).getWorktime());
                    if (afterWorkflag && beforerWorkflag) {
                        zrate = zrates.get(j);
                        break;
                    }
                    else {
                        continue;
                    }
                }
            }
        }
        catch (Exception e) {
            UtilMethod.writeEx("SupplyMethod", e);
            e.printStackTrace();
        }
        if (zrate.getOutid() == null) {
            zrate = getbaseZrate();
        }
        WriteLog.write("最优政策", "最优政策:" + zrate.getAgentid() + "-" + zrate.getRatevalue() + "-" + zrate.getGeneral()
                + "-" + zrate.getWorktime() + "-" + zrate.getAfterworktime() + "-" + zrate.getOutid());
        WriteLog.write("最优政策",
                order.getPnr() + "显示政策:" + tempZrate.getAgentid() + "-" + tempZrate.getRatevalue() + "-"
                        + tempZrate.getGeneral() + "-" + tempZrate.getWorktime() + "-" + tempZrate.getAfterworktime()
                        + "-" + tempZrate.getOutid());
        //      9:6:5.5:1:08:30:23:00:4895006
        return zrate;
    }

    @SuppressWarnings("static-access")
    private List<Zrate> getForWordZrate(List<Zrate> zrates, Zrate tempZrate) {
        List<Zrate> returnZrates = new ArrayList<Zrate>();
        zrates = sortZrate(zrates, zrates.size() - 2);
        for (int i = 0; i < zrates.size(); i++) {
            WriteLog.write("最优政策",
                    i + "-agentId:" + zrates.get(i).getAgentid() + "-返点:" + zrates.get(i).getRatevalue() + "-特殊:"
                            + zrates.get(i).getGeneral() + "-上下班时间:" + zrates.get(i).getWorktime() + "-"
                            + zrates.get(i).getAfterworktime() + "-政策ID:" + zrates.get(i).getOutid() + "-"
                            + (zrates.get(i).getAgentcode() != null ? zrates.get(i).getAgentcode() : "") + "-废票时间:"
                            + (zrates.get(i).getOnetofivewastetime() != null ? zrates.get(i).getOnetofivewastetime()
                                    : "")
                            + "-"
                            + (zrates.get(i).getWeekendwastetime() != null ? zrates.get(i).getWeekendwastetime() : ""));
            if (zrates.get(i).getWorktime() != null && zrates.get(i).getAfterworktime() != null
                    && tempZrate.getGeneral() != null && zrates.get(i).getGeneral() <= tempZrate.getGeneral()) {
                returnZrates.add(zrates.get(i));
            }
        }
        return returnZrates;
    }

    /**
     * 先对zrates按照返点从高到底排序再选出
     * 
     * @param zrates
     * @param count
     * @return
     */
    public List<Zrate> sortZrate(List<Zrate> zrates, int count) {
        List<Zrate> temp_zrates_1 = getListZrate(zrates, 1);
        List<Zrate> temp_zrates_2 = getListZrate(zrates, 2);
        if (temp_zrates_2.size() >= count) {
            temp_zrates_2 = temp_zrates_2.size() >= 15 ? temp_zrates_2.subList(0, 14) : temp_zrates_2;
            temp_zrates_1.addAll(temp_zrates_2);
            zrates = temp_zrates_1;
        }

        Collections.sort(zrates, new Comparator<Zrate>() {
            @Override
            public int compare(Zrate o1, Zrate o2) {
                return (int) (o2.getRatevalue().compareTo(o1.getRatevalue()));
            }
        });

        if (zrates.size() > count) {
            zrates = zrates.subList(0, count);
        }
        //只取上班时间的政策
        zrates = getBestZrate(zrates);
        return zrates;
    }

    /**
     * 国际机票政策排序
     * 作者：邹远超
     * 日期：2014年5月29日
     * 说明：
     * @param zrates
     * @param count
     * @return
     */
    public List<Zrate> sortZrateInter(List<Zrate> zrates, int count) {
        Collections.sort(zrates, new Comparator<Zrate>() {
            @Override
            public int compare(Zrate o1, Zrate o2) {
                return (int) (o2.getRatevalue().compareTo(o1.getRatevalue()));
            }
        });

        if (zrates.size() > count) {
            zrates = zrates.subList(0, count);
        }
        return zrates;
    }

    /**
     * 选择listzrate里面的政策如果i=1选择普通的,如果i=2选择特殊的政策
     * @param listzrate
     * @param i
     * @author chendong 2013年6月24日15:52:57
     * @return
     */
    private List<Zrate> getListZrate(List<Zrate> listzrate, int i) {
        List<Zrate> tlistzrate = new ArrayList<Zrate>();
        for (int j = 0; j < listzrate.size(); j++) {
            if (listzrate.get(j).getGeneral() != null && listzrate.get(j).getGeneral() == i) {
                tlistzrate.add(listzrate.get(j));
            }
        }
        return tlistzrate;
    }

    /**
     * 删除
     * 
     * @param zrate1看到政策
     * @param zrate匹配的最优政策
     */
    @SuppressWarnings("unused")
    private void deleteServerZrate11(Zrate zrate1, Zrate zrate) {
        String url = "http://123.196.113.28:8580/ticket_inter/PiaoMeng";
        if (zrate1.getId() > 1 && zrate1.getOutid() != null && zrate.getRatevalue() < zrate1.getRatevalue()) {
            String paramContent = "<root><pol event=\"13\" id=\"" + zrate1.getOutid() + "\" /></root>";
            java.net.URLConnection connection = null;
            java.net.URL reqUrl = null;
            OutputStreamWriter reqOut = null;
            String param = paramContent;
            try {
                System.out.println("url=" + url + "?" + paramContent + "\n");
                System.out.println("===========post method start=========");
                reqUrl = new java.net.URL(url);
                connection = reqUrl.openConnection();
                connection.setDoOutput(true);
                reqOut = new OutputStreamWriter(connection.getOutputStream());
                reqOut.write(param);
                reqOut.flush();
                System.out.println("===========post method end=========");
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            finally {
                try {
                    reqOut.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * 获得今天的时间格式为yyyy-MM-dd
     */
    public String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }

    /**
     * java.net实现 HTTP POST方法提交
     * 
     * @param url
     * @param paramContent
     * @return
     */
    public String submitPost(String url, String paramContent) {
        StringBuffer responseMessage = null;
        java.net.URLConnection connection = null;
        java.net.URL reqUrl = null;
        OutputStreamWriter reqOut = null;
        InputStream in = null;
        BufferedReader br = null;
        String param = paramContent;
        try {
            responseMessage = new StringBuffer();
            reqUrl = new java.net.URL(url);
            connection = reqUrl.openConnection();
            connection.setDoOutput(true);
            reqOut = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            reqOut.write(param);
            reqOut.flush();
            int charCount = -1;
            in = connection.getInputStream();

            br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            while ((charCount = br.read()) != -1) {
                responseMessage.append((char) charCount);
            }

        }
        catch (Exception ex) {
            System.out.println("url=" + url + "?" + paramContent + "\n e=" + ex);
        }
        finally {
            try {
                in.close();
                reqOut.close();
            }
            catch (Exception e) {
                System.out.println("paramContent=" + paramContent + "|err=" + e);
            }
        }
        return responseMessage.toString();
    }

    /**
     * 
     * 根据政策ID获取政策
     * 
     * @param strSP
     * @return
     */
    public Zrate getremotezrateById(long zrateid) {
        Zrate zrate = new Zrate();
        String ishthyzrate = getSysconfigString("ishthyzrate");
        WriteLog.write("supply", "ishthyzrate" + ishthyzrate + ":" + zrateid);
        try {
            if (ishthyzrate.equals("1")) {
                IAirService iAirService = (IAirService) new HessianProxyFactory().create(IAirService.class,
                        getSysconfigString("hthyzrateurl") + "/cn_service/service/"
                                + IAirService.class.getSimpleName());
                zrate = iAirService.findZrate(zrateid);
            }
            else {
                zrate = Server.getInstance().getAirService().findZrate(zrateid);
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            zrate = getbaseZrate();
        }
        try {
            if (zrate.getGeneral() == null) {
                zrate.setGeneral(1L);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            zrate = getbaseZrate();
        }
        return zrate;
    }

    /**
     * 根据sysconfig的name获得value
     * 内存
     * @param name
     * @return
     */
    public String getSysconfigString(String name) {
        String result = "-1";
        try {
            if (Server.getInstance().getDateHashMap().get(name) == null) {
            	String source = PropertyUtil.getValue("DBSource", "DBSaftey.properties");
                if(!"".equals(source)){
                	String sql = "sp_T_SYSCONFIG_selectByName @name='" + name + "'";
                    DataTable table = DBHelper.GetDataTable(sql,DBSourceEnum.getDBSourceEnumByNo(Integer.parseInt(source)));
                    result = table.GetRow().get(0).GetColumnString("value");
                }
            }
            else {
                result = Server.getInstance().getDateHashMap().get(name);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigStringbydb(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    /**
     * 修改config的value
     * 
     * @param name
     * @param value
     * @time 2014年12月11日 下午10:18:43
     * @author chendong
     */
    public void changeSystemCofigbyname(String name, String value) {
        Server.getInstance().getSystemService()
                .findMapResultBySql("update T_SYSCONFIG set C_VALUE='" + value + "' where C_NAME='" + name + "'", null);

    }

    /**
     * 从全取同步库里面的政策里面查询政策信息
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @param url2
     * @return
     */
    public List<Zrate> getZrateByFlightNumberbyDBusewhere(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String url2, String... ydxagentid) {
        url2 = url2 + "/cn_service/service/";
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            zrates = getzratesbyDBuseWhere(scity, ecity, sdate, flightnumber, cabin, url2, ydxagentid);
            if (zrates == null || zrates.size() == 0) {
                zrates = new ArrayList<Zrate>();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            zrates = new ArrayList<Zrate>();
        }
        return zrates;
    }

    public List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String zrateurl) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        List list = new ArrayList();
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String url = "";
        try {
            String strSP = "[dbo].[sp_GetZrateByFlight] @chufajichang = N'" + scity + "'," + "@daodajichang = N'"
                    + ecity + "'," + "@chufariqi = N'" + sdate + "'," + "@dangqianshijian= N'" + mDateTime + "',"
                    + "@hangkonggongsi= N'" + flightnumber.substring(0, 2) + "'," + "@cangwei= N'" + cabin + "',"
                    + "@hangbanhao= N'" + flightnumber + "'," + "@ismulity=1,@isgaofan=1";
            url = zrateurl + "/cn_service/service/";
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService servier = (ISystemService) factory.create(ISystemService.class,
                    url + ISystemService.class.getSimpleName());
            list = servier.findMapResultByProcedure(strSP);
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Zrate zrate = new Zrate();
                    Map zrateMap = (Map) list.get(i);
                    if (zrateMap.get("id") != null) {
                        zrate.setId(Integer.valueOf(zrateMap.get("id").toString()));
                    }
                    if (zrateMap.get("afterworktime") != null) {
                        zrate.setAfterworktime(zrateMap.get("afterworktime").toString());
                    }
                    if (zrateMap.get("ratevalue") != null) {
                        zrate.setRatevalue(Float.valueOf(zrateMap.get("ratevalue").toString()));
                    }
                    if (zrateMap.get("remark") != null) {
                        zrate.setRemark(zrateMap.get("remark").toString());
                    }
                    if (zrateMap.get("speed") != null) {
                        zrate.setSpeed(zrateMap.get("speed").toString());
                    }
                    if (zrateMap.get("onetofivewastetime") != null) {
                        zrate.setOnetofivewastetime(zrateMap.get("onetofivewastetime").toString());
                    }
                    if (zrateMap.get("worktime") != null) {
                        zrate.setWorktime(zrateMap.get("worktime").toString());
                    }
                    if (zrateMap.get("outid") != null) {
                        zrate.setOutid(zrateMap.get("outid").toString());
                    }
                    if (zrateMap.get("general") != null) {
                        zrate.setGeneral(Long.valueOf(zrateMap.get("general").toString()));
                    }
                    if (zrateMap.get("agentid") != null) {
                        zrate.setAgentid(Long.valueOf(zrateMap.get("agentid").toString()));
                    }
                    zrates.add(zrate);
                }
            }
        }
        catch (Exception e) {
            System.out.println("Exception:" + url);
            e.printStackTrace();
        }
        return zrates;
    }

    /**
     * 获取一条最基本的政策，返点为2.5
     * @return
     */
    public Zrate getbaseChildZrate() {
        Zrate zrate = new Zrate();
        zrate.setId(1L);
        zrate.setRatevalue(0.0F);
        zrate.setGeneral(1L);
        zrate.setOutid("1");
        zrate.setAgentid(5L);
        zrate.setTickettype(1);
        zrate.setWorktime("18:00");
        zrate.setAfterworktime("23:59");
        zrate.setOnetofivewastetime("00:01");
        zrate.setSpeed("10分钟");
        zrate.setRemark("可能换编码出票，出票速度稍慢，请耐心等待。");
        return zrate;
    }

    /**
     * 获取一条最基本的政策，返点为1.5
     * @return
     */
    public static Zrate getbaseZrate() {
        Zrate zrate = new Zrate();
        zrate.setId(1L);
        zrate.setRatevalue(0.0F);
        zrate.setGeneral(1L);
        zrate.setOutid("1");
        zrate.setAgentid(5L);
        zrate.setTickettype(1);
        zrate.setWorktime("18:00");
        zrate.setAfterworktime("23:59");
        zrate.setOnetofivewastetime("00:01");
        zrate.setSpeed("10分钟");
        zrate.setRemark("可能换编码出票，出票速度稍慢，请耐心等待。");
        return zrate;
    }

    public static int getRandomInt() {
        Random random = new Random();
        return random.nextInt(10000);
    }

    public String getBigPnr(String rt, String pnr) {
        String result = "";
        try {
            int num = rt.indexOf("CA/");
            result = rt.substring(num + 3, num + 9);
            if (!result.matches("[a-zA-Z0-9]{6}")) {
                result = pnr;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
    * 转换null
    * 
    * @param <T>
    * @param t
    * @param v
    * @return
    */
    public <T> T converNull(T t, T v) {
        if (t != null) {
            return t;
        }
        return v;
    }

    /**
     * 判断支付供应价格和代理支付价格那个多
     * 
     * @param extorderprice支付供应价格
     * @param payprice代理支付价格
     * @return
     */
    public boolean comparePrice(float extorderprice, float payprice) {
        double d = Math.random();
        if (payprice > 0 && extorderprice > payprice) {
            return true;
        }
        return false;
    }

    /**
     * 选择agentid为agnetid的政策
     * @param oldzrates
     * @param agnetid
     * @return
     */
    public List<Zrate> filterzrate(List<Zrate> oldzrates, long agnetid) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        for (int i = 0; i < oldzrates.size(); i++) {
            if (oldzrates.get(i).getAgentid() == agnetid) {
                zrates.add(oldzrates.get(i));
            }
        }
        return zrates;
    }

    /**
     * 仅仅下载zip压缩包
     * 
     * @param zipUrl
     */
    public static void downZip(String zipUrl, String path) {
        // zipUrl =
        // "http://policy.jinri.cn/CacheRateDataRAR/220001-zhaohe665055-20120504.zip";
        URL url = null;
        FileOutputStream outfile = null;
        InputStream infile = null;
        URLConnection con = null;
        try {
            File tempFile = new File("D:\\" + path);
            if (!tempFile.exists()) {
                tempFile.mkdir();
            }
            url = new URL(zipUrl);
            String filepath = "D:\\" + path + "\\" + zipUrl.substring(zipUrl.lastIndexOf("/") + 1);
            new File(filepath).delete();
            outfile = new FileOutputStream(filepath, true);
            con = url.openConnection();
            infile = con.getInputStream();
            int f = 0;
            while ((f = infile.read()) != -1) {
                outfile.write(f);
            }
            infile.close();
            outfile.close();
        }
        catch (Exception e) {
            String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='JINRIISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
        }
    }

    public static void unZip(String path) {
        int buffer = 2048;
        int count = -1;
        int index = -1;
        String savepath = "";
        boolean flag = false;
        savepath = path.substring(0, path.lastIndexOf("\\")) + "\\";
        try {
            BufferedOutputStream bos = null;
            ZipEntry entry = null;
            FileInputStream fis = new FileInputStream(path);
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            while ((entry = zis.getNextEntry()) != null) {
                byte data[] = new byte[buffer];
                String temp = entry.getName();
                flag = isPics(temp);
                if (!flag)
                    continue;
                index = temp.lastIndexOf("/");
                if (index > -1)
                    temp = temp.substring(index + 1);
                temp = savepath + temp;
                File f = new File(temp);
                f.createNewFile();
                FileOutputStream fos = new FileOutputStream(f);
                bos = new BufferedOutputStream(fos, buffer);
                while ((count = zis.read(data, 0, buffer)) != -1) {
                    bos.write(data, 0, count);
                }
                bos.flush();
                bos.close();
            }
            zis.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isPics(String filename) {
        boolean flag = false;
        if (filename.endsWith(".XML") || filename.endsWith(".xml") || filename.endsWith(".bmp")
                || filename.endsWith(".png"))
            flag = true;

        return flag;
    }

    /**
     * 政策获取sql语句的公用方法
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @param ydxagentid  使用该参数表明使用自维护政策，非供应商政策
     * @return
     */
    public static String getwhereString(String scity, String ecity, String sdate, String flightnumber, String cabin,
            String... ydxagentid) {
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String where = "SELECT ID id,C_RATEVALUE ratevalue,C_TICKETTYPE tickettype,C_OUTID outid,C_AFTERWORKTIME afterworktime"
                + ",C_WORKTIME worktime,C_ONETOFIVEWASTETIME onetofivewastetime,C_SPEED speed,C_REMARK remark,C_GENERAL general"
                + ",C_AGENTID agentid,C_AGENTCODE agentcode FROM T_ZRATE with(nolock) where C_ISENABLE=1  and C_AIRCOMPANYCODE='"
                + flightnumber.substring(0, 2) + "' and C_VOYAGETYPE=1 and C_USERTYPE=1 ";
        if (ydxagentid != null && ydxagentid.length > 0) {
            where += " and C_AGENTID>= 46 ";
        }
        where += " and C_DEPARTUREPORT like '%" + scity + "%' and C_ARRIVALPORT like '%" + ecity
                + "%' and C_CABINCODE like '%" + cabin + "%' " + "and '" + sdate
                + "' between C_BEGINDATE and C_ENDDATE "
                //                + "and '" + mDateTime               + "' between C_WORKTIME and C_AFTERWORKTIME "
                + "and (C_FLIGHTNUMBER is null or  C_FLIGHTNUMBER='' or C_FLIGHTNUMBER like '%"
                + flightnumber.substring(2, flightnumber.length()) + "%')";
        return where;
    }

    /**
     * 政策获取sql语句的公用方法
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @param ydxagentid  使用该参数表明使用自维护政策，非供应商政策
     * @return
     */
    public static String getwhereStringjustwhere(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String... ydxagentid) {
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String where = " where C_AIRCOMPANYCODE='" + flightnumber.substring(0, 2)
                + "'  and C_ISENABLE=1 and C_VOYAGETYPE=1 and C_USERTYPE=1 ";
        if (ydxagentid != null && ydxagentid.length > 0) {
            where += " and C_AGENTID>= 46 ";
        }
        where += " and C_DEPARTUREPORT like '%" + scity + "%' and C_ARRIVALPORT like '%" + ecity
                + "%' and C_CABINCODE like '%" + cabin + "%' " + "and '" + sdate
                + "' between C_BEGINDATE and C_ENDDATE and '" + mDateTime + "' between C_WORKTIME and C_AFTERWORKTIME "
                + "and (C_FLIGHTNUMBER is null or  C_FLIGHTNUMBER='' or C_FLIGHTNUMBER like '%"
                + flightnumber.substring(2, flightnumber.length()) + "%')";
        return where;
    }

    public static void main(String[] args) {
        //        getwhereString("TAO", "CAN", "2014-11-29", "CZ3800", "Y");
        List<Zrate> zrates = getzratesbyDBuseWhere("PEK", "XFN", "2015-07-08", "ZH9273", "Y",
                "http://121.40.90.249:29001/cn_service/service/");
        System.out.println(zrates);
    }

    /**
     * 把map里面的数据解析成zrate对象
     * @param T_map
     * @param ydxagentid 易訂行對應的供應商id
     * @return
     */
    public static Zrate getZratebyMap(Map T_map, String... ydxagentid) {
        Zrate zrate = new Zrate();
        try {
            zrate.setId(Long.parseLong(T_map.get("id").toString()));
            zrate.setRatevalue(Float.parseFloat(T_map.get("ratevalue").toString()));
            zrate.setRemark(T_map.get("remark") == null ? "" : T_map.get("remark").toString());
            zrate.setTickettype(Integer.parseInt(T_map.get("tickettype").toString()));
            zrate.setOutid(T_map.get("outid") == null ? "" : T_map.get("outid").toString());
            zrate.setAfterworktime(isNullreturnString(T_map.get("afterworktime")));
            zrate.setWorktime(isNullreturnString(T_map.get("worktime")));
            zrate.setSpeed(T_map.get("speed") == null ? "10" : T_map.get("speed").toString());
            zrate.setOnetofivewastetime(
                    T_map.get("onetofivewastetime") == null ? "" : T_map.get("onetofivewastetime").toString());
            zrate.setGeneral(Long.parseLong(T_map.get("general") == null ? "1" : T_map.get("general").toString()));
            if (ydxagentid != null && ydxagentid.length > 0) {
                try {
                    zrate.setAgentid(Long.parseLong(ydxagentid[0]));
                }
                catch (Exception e) {//加載異常填充返回供应商
                    zrate.setAgentid(Long.parseLong(T_map.get("agentid").toString()));
                }
            }
            else {
                zrate.setAgentid(Long.parseLong(T_map.get("agentid").toString()));
            }
            if (T_map.get("agentcode") != null) {
                zrate.setAgentcode(T_map.get("agentcode").toString());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrate;
    }

    /**
     * 在指定的url服务器上，用sql语句查询政策数据
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @param url2
     * @param ydxagentid 易訂行對應的供應商id
     * @return
     */
    public static List<Zrate> getzratesbyDBuseWhere(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String url2, String... ydxagentid) {
        HessianProxyFactory factory = new HessianProxyFactory();
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            ISystemService iSystemService = (ISystemService) factory.create(ISystemService.class,
                    url2 + ISystemService.class.getSimpleName());

            String strSP = "sp_zrate_getZratebyWhere @chufajichang = N'" + scity + "',@daodajichang = N'" + ecity + "',"
                    + "@chufariqi = N'" + sdate + "',@dangqianshijian= N''," + "@hangkonggongsi= N'"
                    + flightnumber.substring(0, 2) + "'," + "@cangwei= N'" + cabin + "',@hangbanhao= N'"
                    + flightnumber.substring(2, flightnumber.length()) + "',@ismulity=0,@isgaofan=1";
            List Temp_list_zrates = iSystemService.findMapResultByProcedure(strSP);
            WriteLog.write("getzratesbyDBuseWhere", strSP);

            for (int i = 0; i < Temp_list_zrates.size(); i++) {
                Map T_map = (Map) Temp_list_zrates.get(i);
                if (T_map != null) {
                    Zrate zrate = getZratebyMap(T_map, ydxagentid);
                    if (zrate.getRatevalue() != null && zrate.getRatevalue() > 0) {
                        zrates.add(zrate);
                    }
                }
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            WriteLog.write("supplymethod_getzratesbyDBuseWhere", "error:" + url2);
            System.out.println("error:" + url2 + "=======" + e.getMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("supplymethod_getzratesbyDBuseWhere", "error:" + url2);
            System.out.println("error:" + url2 + "=======" + e.getMessage());
        }
        return zrates;
    }

    /**
     * 只取出某一个供应的政策数据
     * @param zrates
     * @param agentid
     * @return
     */
    public List<Zrate> getzratebyagentid(List<Zrate> zrates, long agentid) {
        List<Zrate> zrate_t = new ArrayList<Zrate>();
        for (int i = 0; i < zrates.size(); i++) {
            if (zrates.get(i).getAgentid() == agentid) {
                zrate_t.add(zrates.get(i));
            }
        }
        return zrate_t;
    }

    /**
     * 当支付后自动根据订单号发送提醒短信
     * 
     * @param orderinfo 订单对象
     * @return
     */
    public boolean sendSmsbyOrderOnpay(Orderinfo orderinfo) {
        boolean issend = false;
        try {
            Dnsmaintenance dns = Server.getInstance().getSystemService().findDnsmaintenance(1);
            String[] mobiles = new String[] { getSysconfigString("tq_sendsmsmobile") };
            if (dns != null && mobiles.length > 0) {
                issend = Server.getInstance().getAtomService().sendSms(mobiles,
                        "订单提醒:请注意订单:" + orderinfo.getOrdernumber() + ",已支付需要处理", orderinfo.getId(), 46, dns, 0);
            }
        }
        catch (Exception e) {
            System.out.println(e.fillInStackTrace());
            e.printStackTrace();
        }
        return issend;
    }

    /**
     * 如果是null返回"",否则返回string
     * @param object
     * @return
     */
    public static String isNullreturnString(Object object) {
        return object == null ? "" : object.toString();
    }
}
