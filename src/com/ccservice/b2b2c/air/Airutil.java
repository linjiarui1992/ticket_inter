package com.ccservice.b2b2c.air;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub;
import net._8000yi.websvr.newply.webinterface.plyintefaceservice_asmx.PlyIntefaceServiceStub;

import org.apache.axiom.om.OMElement;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.inter.server.Server;


public class Airutil {

	// BaQianYiBook baQianYiBook =BaQianYiBook.getInstance();
	BaQianYiBook baQianYiBook = new BaQianYiBook();
	private String name = baQianYiBook.getUsername8000();
	private String pwd = baQianYiBook.getPassword8000();
	private String staus = baQianYiBook.getStaus();

	static {

		// Server.getInstance()
	}

	/**
	 * 创建80000翼订单
	 * 
	 * @param pid
	 * @param PNR
	 * @return
	 * @throws RemoteException
	 */
	public String createEeightOrder(long zid, String PNR)
			throws RemoteException {
		String value = null;
		Zrate zrate = Server.getInstance().getAirService().findZrate(zid);
		OrderServiceStub stub = new OrderServiceStub();
		OrderServiceStub.CreatOrderNew create = new OrderServiceStub.CreatOrderNew();
		create.setName(name);
		create.setPwd(pwd);
		create.setPid(zrate.getOutid());
		create.setPnr(PNR);
		OrderServiceStub.CreatOrderNewResponse response = stub
				.creatOrderNew(create);
		OMElement element = response.getCreatOrderNewResult().getExtraElement();
		Iterator<OMElement> oneiterator = element.getChildElements();
		while (oneiterator.hasNext()) {
			OMElement twoome = oneiterator.next();
			Iterator<OMElement> twoiterator = twoome.getChildElements();
			while (twoiterator.hasNext()) {
				OMElement threeome = twoiterator.next();
				Iterator<OMElement> threeiterator = threeome.getChildElements();
				while (threeiterator.hasNext()) {
					OMElement rt = threeiterator.next();
					String name = rt.getLocalName().toUpperCase();
					if (name.equals("ERRINFO")) {
						System.out.println();
						System.out.println(rt.getText());
						break;
					}
					value = rt.getText();

					if ("ORDERID".equals(name)) {
						// orderinfo.setExtorderid(value);
						// orderinfo.setExtorderstatus(0);
						return value;

					}

				}
			}

		}
		return value;
	}

	/**
	 * 根据PNR获取最优政策
	 * 
	 * @param pnr
	 * @param special
	 *            0:普通。1：特殊。3.所有
	 * @return
	 * @throws RemoteException
	 * @throws ParseException
	 * @throws SQLException
	 */
	public Zrate getBestZratebyPnr(String PNR, int special)
			throws RemoteException, ParseException, SQLException {
		Zrate rate = new Zrate();
		PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
		PlyIntefaceServiceStub.SPbyPNR service = new PlyIntefaceServiceStub.SPbyPNR();
		service.setName(name);
		service.setPwd(pwd);
		service.setIsSpecial(special);
		service.setPnr(PNR);
		PlyIntefaceServiceStub.SPbyPNRResponse response = stub.sPbyPNR(service);
		OMElement ontome = response.getSPbyPNRResult().getExtraElement();
		Iterator<OMElement> oneiterator = ontome.getChildElements();
		DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
		while (oneiterator.hasNext()) {
			OMElement twoome = oneiterator.next();
			Iterator<OMElement> twoiterator = twoome.getChildElements();
			while (twoiterator.hasNext()) {
				OMElement threeome = twoiterator.next();
				Iterator<OMElement> threeiterator = threeome.getChildElements();
				boolean enable = true;
				while (threeiterator.hasNext()) {
					OMElement rt = threeiterator.next();
					String name = rt.getLocalName().toUpperCase();
					if (name.equals("ERRINFO")) {
						System.out.println();
						System.out.println(rt.getText());
						break;
					}
					String value = rt.getText();
					rate.setCreatetime(new Timestamp(System.currentTimeMillis()));
					rate.setCreateuser("job");
					rate.setModifytime(new Timestamp(System.currentTimeMillis()));
					rate.setModifyuser("job");
					if (name.equals("A1")) {//
						rate.setOutid(value);
					} else if (name.equals("A2")) {// 出发城市 A2 String
						// 出发城市三字码,多个城市用"/"分隔,当返回值为'All'时是代表所有出发城市
						rate.setDepartureport(value);
					} else if (name.equals("A3")) { // 到达城市 A3 String
						// 出发城市三字码，多个城市用"/"分隔,当返回值为'All'时是代表所有出发城市
						rate.setArrivalport(value);
					} else if (name.equals("A4")) {// 航空公司 A4 String
						// 航空公司二字码代码,如CA
						rate.setAircompanycode(value);
					} else if (name.equals("A5")) { // 适用航班号 A5 String
						// 由航空公司根据航线所规定的航班号如：CA4307，多个航班用"/"分
						if (value != null && value.length() > 0) {

							rate.setFlightnumber(value);
						}
						rate.setType(1);
					} else if (name.equals("A6")) {// 排除航班号 A6 String
						// 表示政策不包括的航班号，多个航班用"/"
						if (value != null && value.length() > 0) {

							rate.setWeeknum(value);
						}
						rate.setType(2);
					} else if ("A7".equals(name)) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
						if (value != null && value.length() > 0) {

							rate.setVoyagetype(value);
						} else {
							rate.setVoyagetype("1");
						}
					} else if (name.equals("A14")) {// VIP政策 A14 String
						// "0"表示不是VIP政策,其余表示为VIP政策
						if ("0".equals(value))
							rate.setGeneral(1l);
						try {
							rate.setIstype(Long.parseLong(value));
						} catch (Exception exx) {

						}
					} else if (name.equals("A16")) {// 政策类型 A16 String
						// 政策类型包括B2B或BSP
						// 0b2b 1bsp
						// 2b2c 3 all
						if (value.toLowerCase().equals("bsp")) {
							rate.setTickettype(1);
						} else {
							rate.setTickettype(0);
						}
					} else if (name.equals("A8")) {// 政策返点 A8 Decimal
						// 该条政策的返点,正确的返点是大于等于2.5小于50
						rate.setRatevalue(Float.parseFloat(value));
					} else if (name.equals("A9")) {// 政策适用舱位或折扣 A9 String
						// 如果查出来全是数字表示只要舱位折扣等于这个数字的就适用这个政策.全是仓位表示只要在这个仓位范围内就
						rate.setCabincode(value);
					} else if (name.equals("A10")) {// 生效日期 A10 Datetime
						rate.setBegindate(new Timestamp(fromat.parse(value)
								.getTime()));
					} else if (name.equals("A11")) {// 终止日期 A11 Datetime
						rate.setEnddate(new Timestamp(fromat.parse(value)
								.getTime()));
					} else if ("A12".equals(name)) {// 平时上下班时间 A12 String
						// 供应商平时上下班时间
						try {
							String[] times = value.replace("|", ",").split(",");
							rate.setWorktime(times[0]);
							rate.setAfterworktime(times[1]);
						} catch (Exception ex) {
						}

					} else if (name.equals("A17")) {// 政策备注
						rate.setRemark(value);
					} else if ("A22".equals(name)) {// 改后状态 A22 string 可用 /
						if (value != null) {
							if ("可用".equals(value)) {
								rate.setIsenable(1);
								enable = true;
							} else {
								rate.setIsenable(0);
								enable = false;
							}
						}

					}
					rate.setUsertype("1");
					rate.setZtype("1");
					List<Zrate> list = Server.getInstance().getAirService().findAllZrate(
									" where " + Zrate.COL_outid + "='" + rate.getOutid() + "'", "", -1, 0);

					if (list == null || list.isEmpty()) {
						rate = Server.getInstance().getAirService()
								.createZrate(rate);
					} else if (list.size() == 1) {
						rate.setId(list.get(0).getId());
						Server.getInstance().getAirService()
								.updateZrateIgnoreNull(rate);
						rate = Server.getInstance().getAirService().findZrate(list.get(0).getId());
					} else {
						Server.getInstance().getAirService().excuteZrateBySql(
										"delete from " + Zrate.TABLE
												+ " where " + Zrate.COL_outid
												+ "='" + rate.getOutid() + "'");
						rate = Server.getInstance().getAirService()
								.createZrate(rate);
					}

				}
			}
		}

		return rate;

	}

	public Zrate getCurrentZrate(long zid, String outid, String fromcity,
			String endcity, String aircode) throws RemoteException,
			ParseException {
		Zrate zrate = new Zrate();
		zrate.setId(zid);
		PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
		PlyIntefaceServiceStub.SPOne service = new PlyIntefaceServiceStub.SPOne();
		service.setName(name);
		service.setPwd(pwd);
		service.setPolicyId(Integer.valueOf(outid));
		service.setFromCity(fromcity);
		service.setToCity(endcity);
		service.setAirCo(aircode);
		PlyIntefaceServiceStub.SPOneResponse response = stub.sPOne(service);
		OMElement omelement = response.getSPOneResult().getExtraElement();
		Iterator<OMElement> oneiter = omelement.getChildElements();
		DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
		while (oneiter.hasNext()) {
			OMElement twoome = oneiter.next();
			System.out.println(twoome.getQName());
			System.out.println(twoome.getLocalName());
			Iterator<OMElement> twoiter = twoome.getChildElements();
			while (twoiter.hasNext()) {
				OMElement threeome = twoiter.next();

				boolean enable = true;
				Iterator<OMElement> threeitr = threeome.getChildElements();
				while (threeitr.hasNext()) {
					OMElement ome = threeitr.next();
					String name = ome.getLocalName().toUpperCase();
					if (name.equals("ERRINFO")) {
						System.out.println("8000yi政策核对提取异常：");
						System.out.println(ome.getText());
						break;
					}
					String value = ome.getText();
					zrate.setModifytime(new Timestamp(System
							.currentTimeMillis()));
					if (name.equals("A5")) {
						if (value != null && value.length() > 0) {
							zrate.setType(1);
							zrate.setFlightnumber(value);
						}
					} else if (name.equals("A6")) { //
						if (value != null && value.length() > 0) {
							zrate.setType(2);
							zrate.setWeeknum(value);
						}
					} else if ("A7".equals(name)) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
						if (value != null && value.length() > 0) {

							zrate.setVoyagetype(value);
						} else {
							zrate.setVoyagetype("1");
						}
					} else if (name.equals("A14")) {//
						if ("0".equals(value))
							zrate.setGeneral(1l);
						try {
							zrate.setIstype(Long.parseLong(value));
						} catch (Exception exx) {

						}
					} else if (name.equals("A16")) {//
						if (value.toLowerCase().equals("bsp")) {
							zrate.setTickettype(1);
						} else {
							zrate.setTickettype(0);
						}
					} else if (name.equals("A8")) {//
						zrate.setRatevalue(Float.parseFloat(value));
					} else if (name.equals("A9")) {//
						zrate.setCabincode(value);
					} else if (name.equals("A10")) {//
						zrate.setBegindate(new Timestamp(fromat.parse(value)
								.getTime()));
					} else if (name.equals("A11")) {//
						//
						zrate.setEnddate(new Timestamp(fromat.parse(value)
								.getTime()));

					} else if (name.equals("A17")) {//
						zrate.setRemark(value);
					} else if ("A22".equals(name)) {
						if (value != null) {
							if ("可用".equals(value) || value == null
									|| value == "") {
								zrate.setIsenable(1);
								enable = true;
							} else {
								zrate.setIsenable(0);
								enable = false;
							}
						}
					}
					zrate.setUsertype("1");
					zrate.setZtype("1");

				}
				Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
			}

		}

		return Server.getInstance().getAirService().findZrate(zrate.getId());
	}

	public static void main(String[] args) {
		try {
			new Airutil().getCurrentZrate(1815955, "1815955", "CKG", "PVG",
					"3U");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
