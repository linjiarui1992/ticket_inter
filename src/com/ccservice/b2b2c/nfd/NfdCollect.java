//package com.ccservice.b2b2c.nfd;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.Socket;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import com.ccservice.b2b2c.base.aircompany.Aircompany;
//import com.ccservice.b2b2c.base.cityairport.Cityairport;
//import com.ccservice.inter.server.Server;
//
///**
// * 采集信息
// * @author Administrator
// *
// */
//public class NfdCollect {
//	
//	public  List<String> list = new ArrayList<String>();
//	
//	public  List<String> sublist = new ArrayList<String>();
//	
//	private OutputStream out;
//	private InputStream  in;
//	
//	public NfdCollect() {
//	
//	}
//	
//	public  void startConnect(){
//		
//		try {
//			 Socket s = new Socket("192.168.0.100",351);
//			 in = s.getInputStream();
//			 out = s.getOutputStream();
//			 
//			//login
//			byte[] login = new byte[]{'d','e','m','o','2',0,0,0,0,'1','2','3','4','5','6',0,0,0};
//			out.write(login);
//			
//			byte[]buf = new byte[20480];
//			int len =0;
//			
//			StringBuffer sb = null;
//			
//			int i=0;
//			int listlen = list.size();
//			int flag = 0;
//			
//			int ign = 0;
//			
//			String bigcmd=null;
//			boolean cmdflag = true;
//			while((len=in.read(buf))>0){
//				String result = new String(buf,0,len);
//				result=result.replaceAll(new String(new byte[]{0x20,0x62,0x0D}),new String(new byte[]{0x0D}));
//				
//				System.out.println(result);
//				if(ign<3){
//					ign ++;
//					
//				}
//				String cmd = null;
//				if(ign==1){
//					if(result.startsWith("ok")){
//						cmd = list.remove(0);
//						bigcmd = cmd;
//					}else{
//						System.out.println("认证失败！");
//						break;
//					}
//				}
//				
//						
//				
//				if(ign>1){
//											
//						
//						boolean f = this.handnfd(result);
//						if(f){
//							if(sb==null){
//								sb = new StringBuffer();
//							}
//							sb.append(result);
//							
//						}else{
//							FileOutputStream fo = new FileOutputStream("nfd/"+bigcmd.replaceAll("[:/]","-"),true);
//							if(sb==null){
//								sb=new StringBuffer();
//							}
//							sb.append(result);
//							if(cmdflag){
//								this.handSB(sb.toString());
//								cmdflag=false;
//								
//								fo.write(sb.toString().getBytes());
//								
//							}else{
//								
//								fo.write(sb.toString().getBytes());
//							}
//							
//							
//							
//							sb=null;
//							fo.close();
//							
//						}
//						
//						
//						if(sublist.size()>0){
//							cmd = sublist.remove(0);
//						}else{
//							
//							if(list.size()>0){
//								cmd = list.remove(0);
//								cmdflag=true;
//								bigcmd = cmd;
//							}else{
//								break;
//							}
//						
//						}
//					
//				}	
//					if(cmd!=null){			
//						System.out.println("执行"+cmd);
//						out.write(cmd.getBytes());
//					}
//					
//				
//			}
//			
//			
//		
//		} catch (Exception e) {
//			
//			
//			e.printStackTrace();
//		}finally{
//			
//
//			try {
//				in.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			try {
//				out.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		
//	}
//	
//	/**
//	 * 判断是否需要分页
//	 * @param result
//	 * @return
//	 */
//	
//	private boolean handnfd(String result){
//		
//		
//		Pattern p = Pattern.compile("(PAGE).*([\\d+])/([\\d+])");
//		Matcher m = p.matcher(result);
//		if(m.find()){
//			
//			if(m.group(3).equals(m.group(2))){
//				return false;
//				
//			}else{
//				
//				sublist.add(0,"pn");
//				return true;
//			}
//			
//			
//		}
//		return false;
//		
//		
//	}
//	
//	private void handSB(String str){
//		
//		String[]strs =str.split("\r");
//		
//		
//  	 	Pattern p = Pattern.compile("(NFN:\\d+)",Pattern.MULTILINE);
//  	 	Pattern pr = Pattern.compile("(NFR:\\d+)",Pattern.MULTILINE);
//		
//  	 	
//		if(strs!=null){
//			for(String s:strs){
//				
//				Matcher m = p.matcher(s);
//				
//				if(m.find()){
//
//					sublist.add(m.group(0));
//					
//				}else{
//					Matcher pm = pr.matcher(s);
//					if(pm.find()){
//						sublist.add(pm.group(0));
//					}
//				}
//				
//			}
//			
//		}
//		
//	}
//	
//	public static String filetoString(File f){
//		
//		try {
//			FileInputStream fin= new FileInputStream(f);
//		
//			byte[] bs = new byte[fin.available()];
//			fin.read(bs);
//			fin.close();
//			
//			return new String(bs);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//	}
//	
//	
//	
//	public static void main(String[] args) {
//		//提取运价的航空公司
//		String[] cxrs = new String[]{"HU","CA","FM", "ZH", "CZ", "3U", "HO", "MF" ,"MU"};
//		//出发城市
//		String[] citys = new String[]{"PEK","SZX","SHA","CAN","CTU","CKG","HGH","SIA","CGQ"};
//		NfdCollect client = new NfdCollect();
//		List<Cityairport> toList = Server.getInstance().getAirService().findAllCityairport("","order by id", -1,0);
//		
//		for(String cxr:cxrs)
//		
//		for(String from:citys){
//			//			System.out.println(city);
//						for(Cityairport to:toList){
//							if(!from.equals(to.getAirportcode())){
//								
//								String str ="nfd:"+from+to.getAirportcode()+"/"+cxr;
//								String file = "nfd/nfd-"+from+to.getAirportcode()+"-"+cxr;
//								File f = new File(file);
//								if(f.exists()){
//									new NfdParsing().Parse(file); 
//								}else{
////									client.	list.add(str);
//								}
//								
//								System.out.println(str);
//								
//								
//							}
//						}
//					}
//				
////				}
//				
//				client.startConnect();
//		
//		/*
//		
//		
//		//		PEK
////		SZX
////		SHA(PVG)
////		CAN
////		CTU
////		CKG
////		HGH
////		SIA(XIY)
////		CGQ
//
//		NfdCollect client = new NfdCollect();
//		//提取所有的航空公司
////		List<Aircompany> acList = Server.getInstance().getAirService().findAllAircompany("Where id =21 ","order by id",-1,0);
////		for(Aircompany ac:acList){
////		
//			
//			List<Cityairport> fromList = Server.getInstance().getAirService().findAllCityairport("","order by id", -1,0);
//			List<Cityairport> toList = Server.getInstance().getAirService().findAllCityairport("","order by id", -1,0);
//	
//			for(Cityairport from:fromList){
//	//			System.out.println(city);
//				for(Cityairport to:toList){
//					if(!from.getAirportcode().equals(to.getAirportcode())){
//						System.out.println("ca"+"/"+from.getId()+"/"+to.getId());
//						String str ="nfd:"+from.getAirportcode()+to.getAirportcode()+"/02mar/ca";
//						String file = "nfd-"+from.getAirportcode()+to.getAirportcode()+"-02mar-ca";
//						File f = new File(file);
//						if(f.exists()){
////							new NfdParsing().Parse(file); 
//						}else{
//							client.	list.add(str);
//						}
//						
//						System.out.println(str);
//						
//						
//					}
//				}
//			}
//		
////		}
//		
//		client.startConnect();
//		*/
//		
////		new NfdParsing().Parse("nfd-peksha-02MAR-ca");
////		new NfdParsing().Parse("nfd-pekscx-02mar-ca");
////		
//
//		
//		
//		
//	/*
//		String match = filetoString(new File("nfd-nkgpek-27sep-mu"));
//	//	Pattern p = Pattern.compile("(PAGE).*([\\d+])/([\\d+])"); //是否有下页
//		
//		
//		String[]strs =match.split("\r");
//	
//	
//  	 	Pattern p = Pattern.compile("(NFN:\\d+)",Pattern.MULTILINE);
//	
//		
//  	 	
//		if(strs!=null){
//			for(String s:strs){
//				
//				Matcher m = p.matcher(s);
//				
//				
//				
//				if(m.find()){
//					System.out.println(m.groupCount());
//					
//					System.out.println(m.group(0));
//					
//					
//				}
//				
//			}
//			
//		}
//		*/
//		
//  	 
//		
//	}
//
//}
