package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;
import com.ccservice.inter.server.Server;



public class HthyRequestRefund {
	public static void main(String[] args) {
		String ss="0.00";
		String ss1="0.50";
		double d1=Double.parseDouble(ss);
		double d2=Double.parseDouble(ss1);
		System.out.println(Double.compare(d1, d2));
	    
	}

    public String requestRefund(String orderid,String refundsum) {
        String returnorder="";
        String result="";
        try {
            JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
            CtripCallback ctripCallback = new CtripCallback();
            org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
            String xmlparam=getXml(orderid,refundsum);
            WriteLog.write("Elong_差额退款请求艺龙request","订单id:"+orderid+",请求艺龙信息:"+xmlparam);
            param.setString(xmlparam);
            ctripCallback.setXml(param);
            CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
            org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
            System.out.println(ss+"");
            result=ss+"";
            WriteLog.write("Elong_差额退款请求艺龙response","订单id:"+orderid+",艺龙返回信息:"+result);
        } catch (AxisFault e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //
        try {
            Document document = DocumentHelper.parseText(result);
            Element orderResponse = document.getRootElement();
            String serviceName=orderResponse.elementText("ServiceName");//接口服务名
            String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
            String orderNumber=orderResponse.elementText("OrderNumber");//订单号
            String status=orderResponse.elementText("Status");//状态
            returnorder=status;
            Element errorResponse=orderResponse.element("ErrorResponse");
            String errorMessage=errorResponse.elementText("ErrorMessage");//信息
            if("FAIL".equals(status)){
                String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
                WriteLog.write("ELong_ticket_inter线下请求艺龙差额退款操作response_error_记录",serviceName+"--"+operationDateTime+"--"+orderNumber+"--"+status+"--"+errorMessage+"--"+errorCode);
            }else if("SUCCESS".equals(status)){
            	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            	String recordsql="INSERT INTO TrainOrderOfflineRecord(FKTrainOrderOfflineId,ProviderAgentid,DistributionTime,DealResult) values("+orderid+",0,'"+sdf.format(new Date())+"',7)";
				Server.getInstance().getSystemService().excuteAdvertisementBySql(recordsql);
				WriteLog.write("ELong_ticket_inter线下请求艺龙差额退款操作response记录","订单号:"+orderNumber+",sql:"+recordsql);
				
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return returnorder;
    }
    public static String getXml(String orderid,String refundsum) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="select T.sealPrice,T.Price,O.OrderNumberOnline,O.OrderPrice,O.OrderNumber from TrainTicketOffline T,TrainOrderOffline O where O.Id=T.OrderId and O.Id="+orderid;
        DataTable myDt = DBHelper.GetDataTable(sql);
        List<DataRow> list = myDt.GetRow();
        String ordernum="";
        double allrefund=0;
        int countnoticket=0;
        double orderprice=0;
        String ordernumtemp="T1605241424557936386";
        if(list.size()>0){
        	DataRow datarow = list.get(0);
            ordernum=datarow.GetColumnString("OrderNumberOnline");
            orderprice=Double.parseDouble(datarow.GetColumnString("OrderPrice"));
            ordernumtemp=datarow.GetColumnString("OrderNumber");
//            for(int i=0;i<list.size();i++){
//            	DataRow datarow1 = list.get(i);
//            	allrefund+= Double.parseDouble(datarow1.GetColumnString("Price"))-Double.parseDouble(datarow1.GetColumnString("sealPrice"));
//            	if(Double.compare(Double.parseDouble("0.00"),Double.parseDouble(datarow1.GetColumnString("sealPrice")))==0){
//            	    countnoticket++;
//            	}
//            }
        }
//        int refundType1=0;
//        if(countnoticket!=0 && countnoticket!=list.size()){
//            allrefund+=countnoticket*5;
//            refundType1=0;
//        }else if(countnoticket==list.size()){
//            allrefund=orderprice;
//            refundType1=1;
//        }
        
        String from=ordernumtemp.substring(2, 7);
        String seqId=from+""+System.currentTimeMillis();
        
      //退款记录表
//        String sqlinsert="insert into TrainOfflineRefundRecord(orderId,operateName,refundSum,operateTime,refundType,seqId) values("+orderid+",'系统','"+refundsum+"','"+sdf.format(new Date())+"',"+refundType1+",'"+seqId+"')";
//        Server.getInstance().getSystemService().excuteAdvertisementBySql(sqlinsert);
//        WriteLog.write("ELong_ticket_inter线下请求艺龙差额退款数据库记录","订单号:"+orderid+",sqlinsert:"+sqlinsert);
        
        WriteLog.write("elong差额退款金额","订单id:"+orderid+",allrefund:"+refundsum);
        //下单请求接口调用Response
        Document document=DocumentHelper.createDocument();
        Element orderProcessRequest=document.addElement("OrderProcessRequest");
        orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
        Element authentication=orderProcessRequest.addElement("Authentication");
        
        Element serviceName=authentication.addElement("ServiceName");
        serviceName.setText("web.order.requestRefund");
        Element partnerName=authentication.addElement("PartnerName");
        partnerName.setText("hangtian111");
        Element timeStamp=authentication.addElement("TimeStamp");
        String times=sdf.format(new Date());
        timeStamp.setText(times);
        Element messageIdentity=authentication.addElement("MessageIdentity");
        String message=getMessage(times, "web.order.requestRefund", "hangtian111");
        messageIdentity.setText(message);
        
        Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
        Element orderInfo=trainOrderService.addElement("OrderInfo");
        Element orderNumber=orderInfo.addElement("OrderNumber");
        orderNumber.setText(ordernum);//订单号
        Element orderTid=orderInfo.addElement("OrderTid");
        int randominti1 = new Random().nextInt(1000000);
        orderTid.setText("Refund"+randominti1);//OrderRefund.RefundBillNo退款bill单号-------------
        Element orderTradeNo=orderInfo.addElement("OrderTradeNo");
        int randominti2 = new Random().nextInt(1000000);
        orderTradeNo.setText("RefundBill"+randominti2);// OrderRefund.RefundBillNo退款bill单号------------
        Element orderTotleFee=trainOrderService.addElement("OrderTotleFee");
        orderTotleFee.setText("");//空值
        Element totalRefundAmount=trainOrderService.addElement("TotalRefundAmount");
        totalRefundAmount.setText(refundsum+"");
        Element reason=trainOrderService.addElement("Reason");
        reason.setText("存在票价差");//原因
        Element refundType=trainOrderService.addElement("RefundType");
        refundType.setText("0");//0差额退款、3核销退款
        return document.asXML();
    }
    public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
		System.out.println(times+serviceName+partName+"<-->"+result);
		return result.toUpperCase();
	}
}
