package com.ccservice.b2b2c.yilong;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

import sun.misc.BASE64Encoder;

public class Util {

	public static String loadFile(String fileName) {
		InputStream fis;
		try {
			fis = new FileInputStream(fileName);
			byte[] bs = new byte[fis.available()];
			fis.read(bs);
			String res = new String(bs, "utf8");
			fis.close();
			return res;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String md5EncryptAndBase64(String str) {
//	    System.out.println("base64="+encodeBase64(md5Encrypt(str)));
		return encodeBase64(md5Encrypt(str));
	}

	private static byte[] md5Encrypt(String encryptStr) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(encryptStr.getBytes("utf8"));
//			System.out.println("xmls="+encryptStr);
//			System.out.println("MD5="+md5.digest());
			return md5.digest();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static String encodeBase64(byte[] b) {
		sun.misc.BASE64Encoder base64Encode = new BASE64Encoder();
		String str = base64Encode.encode(b);
		return str;
	}

	public static void main(String[] args) {
//		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request service=\"DeliverTmService\" lang=\"zh-CN\"><Head>QCYGBJ</Head><Body><DeliverTmRequest business_type=\"15\" consigned_time=\"2015-12-24 12:00:00\"><SrcAddress province=\"广东省\" city=\"深圳\" district=\"福田\" address=\"园岭街道\"/><DestAddress province=\"浙江省\" city=\"金华\" district=\"婺城\" address=\"城中街道\"/></DeliverTmRequest></Body></Request>";
//		String checkword = "CijbADBQCqdX";
//		System.out.println(md5EncryptAndBase64(xml + checkword));
//		System.out.println(md5EncryptAndBase64("abc"));
//		System.out.println(md5EncryptAndBase64("中"));
	    String result=loadFile("D://ccc.txt");
	    System.out.println("result="+result);
	}
}
