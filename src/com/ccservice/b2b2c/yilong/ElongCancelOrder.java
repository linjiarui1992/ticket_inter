package com.ccservice.b2b2c.yilong;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class ElongCancelOrder {

	public String cancelOrder(String xml){
		String ordernum="";
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element TrainOrderService=root.element("TrainOrderService");
			String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
			ordernum=orderNumber;
			String cancelTime=TrainOrderService.elementText("CancelTime");//取消时间
			String cancelChooseReason=TrainOrderService.elementText("CancelChooseReason");//取消原因 选择
			String cancelInputReason=TrainOrderService.elementText("CancelInputReason");//取消原因 编辑
			String updatesql="UPDATE TrainOrderOffline SET AgentId=2 WHERE OrderNumberOnline='"+orderNumber+"'";
			Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
			//艺龙取消订单表加取消记录
			String sql="INSERT INTO TrainOfflineElongCancel(orderNum,cancelTime,cancelChooseReason,cancelInputReason) values('"+orderNumber+"','"+cancelTime+"',"+cancelChooseReason+",'"+cancelInputReason+"')";
			WriteLog.write("ELong线下火车票主动取消订单记录","订单号:"+orderNumber+",sql:"+sql);
			Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		String result=cancelOrderresult(ordernum);
		return result;
	}
	public String cancelOrderresult(String ordernum){
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.cancelOrder");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText(ordernum);
		Element discription=orderResponse.addElement("Discription");
		discription.setText("");//订单取消成功描述
		return document.asXML();
	
	}
}
