package com.ccservice.b2b2c.bus;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.hoteldb.HanziToPinyin;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @author wzc
 * @version 创建时间：2015年8月3日 上午10:55:21
 * 携程数据抓去
 */
public class BusCityWrapper implements Job {
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        WriteLog.write("携程城市关联抓取", "运行中：" + System.currentTimeMillis());
        try {
            System.out.println("开始运行" + com.ccservice.b2b2c.policy.TimeUtil.gettodaydate(4, new Date()));
            parseToCityData();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws Exception {
        parseFromCityData();
        parseToCityData();
        System.out.println(System.currentTimeMillis());

    }

    /**
     * 携程到达城市数据解析
     * @throws Exception
     */
    public static void parseToCityData() throws Exception {
        ISystemService service = Server.getInstance().getSystemService();
        String sql = "select * from BusCity order by Id asc";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String cityname = map.get("Name").toString();
            long fromcityid = Long.valueOf(map.get("Id").toString());
            String url = "http://bus.ctrip.com/index.php?param=/data/toCityList&fromCity="
                    + URLEncoder.encode(cityname, "UTF-8") + "&callback=busToCityCallback";
            String buf = Ctriputil.Geturl(url);
            if (buf != null && buf.contains("busToCityCallback")) {
                buf = buf.replace("busToCityCallback(", "");
            }
            buf = buf.substring(0, buf.lastIndexOf(")"));
            JSONObject result = JSONObject.fromObject(buf);
            try {
                JSONObject msg = result.getJSONObject("return");
                String data = msg.getString("data");
                String[] city = data.split("@");
                if (city.length >= 1) {
                    for (int j = 0; j < city.length; j++) {
                        String tocitypinyin = "''";
                        String tocityname = "''";
                        String tocityshortpinyin = "''";
                        if (city[j].length() > 4) {
                            String[] ans = city[j].split("\\|");
                            //System.out.println("!");
                            for (int k = 0; k < ans.length; k++) {
                                //System.out.println(ans[k] + k);
                                switch (k) {
                                case (0): {
                                    tocitypinyin = ans[0];
                                    break;
                                }
                                case (1): {
                                    tocityname = ans[1];
                                    break;
                                }
                                case (5): {
                                    tocityshortpinyin = ans[5];
                                    //System.out.println("1");
                                    break;
                                }
                                }
                            }
                            //   System.out.println(
                            //   cityname + ":" + tocitypinyin + ":" + tocityname + ":" + tocityshortpinyin);
                            List citystate = new ArrayList();
                            citystate = service.findMapResultByProcedure("sp_Insert_Bus_City '" + tocityname + "','"
                                    + tocitypinyin + "','" + tocityshortpinyin + "','0','0','0','0'");
                            List tocitystate = new ArrayList();
                            tocitystate = service.findMapResultByProcedure(
                                    "sp_Insert_BusCityAsso '" + cityname + "','" + tocityname + "'");
                        }
                    }

                }

            }
            catch (Exception e) {

            }

        }
    }

    /**
     * 携程出发城市数据解析
     * @throws Exception
     */
    public static void parseFromCityData() throws Exception {
        String strurl = "http://bus.ctrip.com/index.php?param=/data/fromCityListNew&callback=busFromCityCallback";
        URL url = new URL(strurl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        InputStreamReader input = new InputStreamReader(httpConn.getInputStream(), "utf-8");
        BufferedReader bufReader = new BufferedReader(input);
        String line = "";
        StringBuilder contentBuf = new StringBuilder();
        while ((line = bufReader.readLine()) != null) {
            contentBuf.append(line);
        }
        String buf = contentBuf.toString();
        buf = Ctriputil.unicodeDecode(buf);
        if (buf != null && buf.contains("busFromCityCallback")) {
            buf = buf.replace("busFromCityCallback(", "");
            buf = buf.substring(0, buf.lastIndexOf(")"));
            JSONObject result = JSONObject.fromObject(buf);
            ISystemService service = Server.getInstance().getSystemService();
            JSONArray hot = result.getJSONArray("hot");
            for (int i = 0; i < hot.size(); i++) {
                JSONObject hotmsg = hot.getJSONObject(i);
                String provincename = hotmsg.getString("province");
                if (provincename == null || "".equals(provincename) || "null".equals(provincename)) {
                    provincename = "";
                }
                String Pinyin = HanziToPinyin.getPinYin(provincename);
                String Abbreviation = HanziToPinyin.getPinYinHeadChar(provincename);
                List provincestate = new ArrayList();
                provincestate = service.findMapResultByProcedure(
                        " sp_Insert_Bus_Province '" + provincename + "','" + Abbreviation + "','" + Pinyin + "'");
                JSONObject info = hotmsg.getJSONObject("info");
                String cityname = info.getString("name");
                String citypinyin = info.getString("pinyin");
                String cityshortpinyin = info.getString("short_pinyin");
                String grade = info.getString("grade");
                String cityid = info.getString("city_id");
                List citystate = new ArrayList();
                //                System.out.println("1:sp_Insert_Bus_City '" + cityname + "','" + citypinyin + "','" + cityshortpinyin
                //                       + "','" + provincename + "','" + grade + "','0','" + cityid + "'");
                citystate = service.findMapResultByProcedure("sp_Insert_Bus_City '" + cityname + "','" + citypinyin
                        + "','" + cityshortpinyin + "','" + provincename + "','" + grade + "','0','" + cityid + "'");
                long pcityid = 0;
                if (citystate.size() > 0) {
                    Map map = (Map) citystate.get(0);
                    pcityid = Long.valueOf(map.get("ID").toString());
                }
                if (info.containsKey("county")) {
                    JSONArray country = info.getJSONArray("county");
                    if (country != null) {
                        for (int k = 0; k < country.size(); k++) {
                            JSONObject countryinfo = country.getJSONObject(k);
                            //System.out.println(cityinfo);
                            String countrypinyin = countryinfo.getString("pinyin");
                            String countryshortpinyin = countryinfo.getString("short_pinyin");
                            String countryname = countryinfo.getString("name");
                            String countryid = countryinfo.getString("city_id");
                            List countrystate = new ArrayList();
                            //                        System.out.println("2:sp_Insert_Bus_City '" + countryname + "','" + countrypinyin + "','"
                            //                                + countryshortpinyin + "','" + provincename + "','-1','" + pcityid + "','" + countryid
                            //                                + "'");
                            countrystate = service.findMapResultByProcedure("sp_Insert_Bus_City '" + countryname + "','"
                                    + countrypinyin + "','" + countryshortpinyin + "','" + provincename + "','-1','"
                                    + pcityid + "','" + countryid + "'");
                        }
                    }
                }
            }
            JSONArray other = result.getJSONArray("other");
            for (int i = 0; i < other.size(); i++) {
                JSONObject othermsg = other.getJSONObject(i);
                String cityname = othermsg.getString("name");
                String citypinyin = othermsg.getString("pinyin");
                String cityshortpinyin = othermsg.getString("short_pinyin");
                String provincename = othermsg.getString("province_name");
                String grade = othermsg.getString("grade");
                String cityid = othermsg.getString("city_id");
                List citystate = new ArrayList();
                if (provincename == null) {
                    provincename = "";
                }
                //                System.out.println("3:sp_Insert_Bus_City '" + cityname + "','" + citypinyin + "','" + cityshortpinyin
                //                        + "','" + provincename + "','" + grade + "','0','" + cityid + "'");
                citystate = service.findMapResultByProcedure("sp_Insert_Bus_City '" + cityname + "','" + citypinyin
                        + "','" + cityshortpinyin + "','" + provincename + "','" + grade + "','0'," + cityid + "");
            }
            JSONArray data = result.getJSONArray("data");

            for (int i = 0; i < data.size(); i++) {
                JSONArray msg = data.getJSONArray(i);
                for (int j = 0; j < msg.size(); j++) {
                    JSONObject provincemsg = msg.getJSONObject(j);
                    String provincename = provincemsg.getString("province");
                    String Pinyin = HanziToPinyin.getPinYin(provincename);
                    String Abbreviation = HanziToPinyin.getPinYinHeadChar(provincename);
                    JSONObject info = provincemsg.getJSONObject("info");
                    List provincestate = new ArrayList();
                    provincestate = service.findMapResultByProcedure(
                            "sp_Insert_Bus_Province '" + provincename + "','" + Abbreviation + "','" + Pinyin + "'");
                    String cityname = info.getString("name");
                    String citypinyin = info.getString("pinyin");
                    String cityshortpinyin = info.getString("short_pinyin");
                    String grade = info.getString("grade");
                    String cityid = info.getString("city_id");
                    List citystate = new ArrayList();
                    //                    System.out.println("4:sp_Insert_Bus_City '" + cityname + "','" + citypinyin + "','"
                    //                            + cityshortpinyin + "','" + provincename + "','" + grade + "','0','" + cityid + "'");
                    citystate = service.findMapResultByProcedure(
                            "sp_Insert_Bus_City '" + cityname + "','" + citypinyin + "','" + cityshortpinyin + "','"
                                    + provincename + "','" + grade + "','0','" + cityid + "'");
                    long pcityid = 0;
                    if (citystate.size() > 0) {
                        Map map = (Map) citystate.get(0);
                        pcityid = Long.valueOf(map.get("ID").toString());
                    }
                    if (info.containsKey("county")) {
                        JSONArray country = info.getJSONArray("county");
                        if (country != null) {
                            for (int k = 0; k < country.size(); k++) {
                                JSONObject countryinfo = country.getJSONObject(k);
                                String countrypinyin = countryinfo.getString("pinyin");
                                String countryshortpinyin = countryinfo.getString("short_pinyin");
                                String city_id = countryinfo.getString("city_id");
                                String countryname = countryinfo.getString("name");
                                List countrystate = new ArrayList();
                                //                            System.out.println("5:sp_Insert_Bus_City '" + countryname + "','" + countrypinyin + "','"
                                //                                    + countryshortpinyin + "','" + provincename + "',-1,'" + pcityid + "','" + city_id
                                //                                    + "'");
                                citystate = service.findMapResultByProcedure("sp_Insert_Bus_City '" + countryname
                                        + "','" + countrypinyin + "','" + countryshortpinyin + "','" + provincename
                                        + "',-1,'" + pcityid + "','" + city_id + "'");
                            }
                        }
                    }
                }
            }
        }
        else
            System.out.println("未获取数据");
    }
}
