package com.ccservice.newjltour.updatejob;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.newjltour.hoteldb.JlHotelDB;

/**
 * 更新酒店、房型基础信息
 * @author WH
 */

public class NewJlHotelJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        JlHotelDB.updateBaseInfo();
    }

}