package com.ccservice.newjltour.test;

import net.sf.json.JSONObject;
import com.ccservice.b2b2c.atom.qunar.PHUtil;

public class TestChangeDemo {

    public static void main(String[] args) {
        JSONObject obj = new JSONObject();
        obj.put("usercd", "BJ13558");
        obj.put("authno", "123456");
        obj.put("roomtypeids", "11963/4066/4065/4064/4057/4056/4055/4054/4053/4052/4051");
        System.out.println(PHUtil.submitPost("http://42.120.20.3:9096/ticket_inter/JlTourChangePrice", obj.toString())
                .toString());
    }

}