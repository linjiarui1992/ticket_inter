package com.ccservice.offlineExpress.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;

public class RequestStreamUtil {

    /**
     * 请求参数转换
     * 
     * @author liujun
     * @throws Exception 
     * @throws UnsupportedEncodingException 
     */
    public static JSONObject reqToJson(HttpServletRequest req) throws UnsupportedEncodingException, Exception {
        //JSON
        JSONObject result = null;
        //解析数据
        //参数
        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream(), "utf-8"));
        String line = "";
        StringBuffer buf = new StringBuffer(1024);
        while ((line = br.readLine()) != null) {
            buf.append(line);
        }
        //转换
        result = JSONObject.parseObject(buf.toString());

        //返回
        return result == null ? new JSONObject() : result;
    }
}
