package com.ccservice.offlineExpress;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.util.CommonUtil;
import com.ccservice.offlineExpress.util.RequestStreamUtil;
import com.ccservice.offlineExpress.util.RouteCallBackUtil;

public class EmsRoutecallbackServlet extends HttpServlet implements Servlet {

    private static final long serialVersionUID = 7875921500303712967L;

    private static final String logName = "线下火车票_EMS回调路由信息";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //随机
        int random = CommonUtil.randomNum();
        //编码
        request.setCharacterEncoding("utf-8");
        //结果
        JSONObject responseJson = new JSONObject();
        String success = "0";
        String message = "";
        String failmailnums = "";
        try {
            // 请求参数转换
            JSONObject param = RequestStreamUtil.reqToJson(request);
            //日志
            WriteLog.write(logName, random + "---回调信息---" + param);
            //根节点
            String listexpressmail = param.getString("listexpressmail");
            JSONArray jsonArray = JSONArray.parseArray(listexpressmail);
            if (jsonArray != null && jsonArray.size() > 0) {
                success = "1";
                message = "请求成功";
                // 循环插入
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String expressNum = jsonObject.getString("mailnum");// 快递单号
                    RouteCallBackUtil.flushExpressRoute(logName, random, expressNum);
                }
            }
            else {
                message = "请求参数为空";
            }
        }
        catch (Exception e) {
            message = "请求参数解析失败";
            //记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        // 统一返回
        responseJson.put("success", success);
        responseJson.put("failmailnums", failmailnums);
        responseJson.put("remark", message);
        String result = responseJson.toJSONString();
        WriteLog.write(logName, random + "---回调结果---" + result);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/json; charset=UTF-8");
        response.getOutputStream().write(result.getBytes(Charset.forName("UTF-8")));
    }
}
