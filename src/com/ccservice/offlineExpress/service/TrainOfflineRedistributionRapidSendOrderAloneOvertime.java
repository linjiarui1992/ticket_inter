/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offlineExpress.service;

import java.util.Random;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.tuniu.train.service.TrainTuNiuOfflineLockOrder
 * @description: TODO - 此方法只负责取消相关位置的闪送单即可
 * 
 *  需要还原相关的邮寄对象的信息
 *  
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:58:14 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRedistributionRapidSendOrderAloneOvertime implements Job {
    private static final String LOGNAME = "闪送单独下单请求启动定时任务-8min后自动取消UU跑腿的订单";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        Integer useridi = dataMap.getInt("useridi");
        String LockRapidSendWait = dataMap.getString("LockWait");

        WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到自动取消闪送单的方法-OrderId-->" + OrderId + "-useridi-->" + useridi
                + "-LockRapidSendWait-->" + LockRapidSendWait);

        if (OrderId == null || OrderId == 0L) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-OrderId不存在，请排查");
            return;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(OrderId);
        }
        catch (Exception e) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline == null) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-对象无法获取，请排查");
            return;
        }

        //自动取消的订单需要判定是否有相关的闪送接单结果之类的反馈 - 如果有的话，不要再走自动取消的逻辑 - isRapidSendCallback
        Boolean isRapidSendCallback = trainOrderOffline.getIsRapidSendCallback();
        if (!isRapidSendCallback) {
            //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            //锁单中的等待出票的单子才会走到此处
            //记录日志 - 刷新界面 - 
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(OrderId, LOGNAME);

            //取消闪送单

            UUptService uuptService = new UUptService();
            JSONObject responseJson = new JSONObject();
            JSONObject data = new JSONObject();
            data.put("orderId", OrderId);
            data.put("cancelReason", "线下票闪送订单定时任务-8min后自动取消UU跑腿订单");
            try {
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrder", data, responseJson);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }

            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-取消结果:responseJson-" + responseJson);

            //responseJson.getJSONObject("result").getBooleanValue("success");
            //Boolean responseB = responseJson.getBooleanValue("success");
            Boolean responseB = false;
            JSONObject responseJsonResult = new JSONObject();
            if (responseJson.getBooleanValue("success")) {
                responseJsonResult = responseJson.getJSONObject("result");
                responseB = responseJsonResult.getBooleanValue("success");
            }

            if (responseB) {
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送单独下单-自动取消成功");

                //重置相关的邮寄对象的信息

                /*TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送单独下单-重置快递类型");
                
                mailAddressDao.updateMailAddressExpress(OrderId.intValue(), "0", 0);
                
                //默认获取顺丰的快递时效并做相关的更新操作
                //为了兼容后期的自动取消业务，还需重置闪送业务的回调已反馈的状态字段的标识
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送单独下单-重置订单的闪送类型区分、反馈标识字段及顺丰的快递时效的显示");
                
                Integer agentid = trainOrderOffline.getAgentId();
                String totalAddress = "";
                try {
                    totalAddress = mailAddressDao.findADDRESSCITYNAMEByORDERID(OrderId).getADDRESS();
                }
                catch (Exception e2) {
                    WriteLog.write(LOGNAME, r1 + ":邮寄信息获取失败-e2:" + e2);
                
                    ExceptionTCUtil.handleTCException(e2);
                }
                String delieveStr = DelieveUtils.getDelieveStr(String.valueOf(OrderId), totalAddress);//
                trainOrderOfflineDao.updateIsRapidSendAndCallbackExpressDeliverById(OrderId, 0, delieveStr);*///

                WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到闪送单独下单-自动取消闪送单方法-取消成功");
            }
            else {
                String errorMsg = responseJsonResult.getString("message");
                if (errorMsg == null || "".equals(errorMsg)) {
                    errorMsg = responseJson.getString("message");
                }
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送单独下单-自动取消失败:" + errorMsg + "-请联系技术处理");

                WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到闪送单独下单-自动取消闪送单方法-取消失败");
                return;
            }
        }
    }
}
