package com.ccservice.offlineExpress.uupt;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offlineExpress.util.PropertyUtil;
import com.ccservice.offlineExpress.util.ServiceUtil;

/**
 * uu跑腿业务分类
 * 
 * @time 2017年10月23日 下午4:09:14
 * @author liujun
 */
public class UUptService extends ServiceUtil {

    /**
     * 操作入口
     * 
     * @time 2017年10月23日 下午4:09:03
     * @author liujun
     */
    public JSONObject operate(String logName, String expressName, long random, String method, JSONObject data,
            JSONObject responseJson) throws Exception {
        // 取消订单
        if ("cancelOrder".equals(method)) {
            responseJson = cancelOrder(logName, expressName, random, method, data, responseJson);
        }
        // 发布订单
        else if ("addOrder".equals(method)) {
            responseJson = addOrder(logName, expressName, random, method, data, responseJson);
        }
        // 查询价格
        else if ("getOrderPrice".equals(method)) {
            responseJson = getOrderPrice(logName, expressName, random, method, data, responseJson);
        }
        // 再次下单
        else if ("addOrderAgain".equals(method)) {
            responseJson = addOrderAgain(logName, expressName, random, method, data, responseJson);
        }
        // 快递路由
        else if ("getExpressRoute".equals(method)) {
            responseJson = getExpressRoute(logName, expressName, random, method, data, responseJson);
        }
        // 二次下单取消订单
        else if ("cancelOrderAgain".equals(method)) {
            responseJson = cancelOrderAgain(logName, expressName, random, method, data, responseJson);
        }
        // 返回
        return responseJson;
    }

    /**
     * 发布订单
     * 
     * @time 2017年10月23日 下午4:09:54
     * @author liujun
     */
    private JSONObject addOrder(String logName, String expressName, long random, String method, JSONObject dataJson,
            JSONObject responseJson) {
        // 订单ID
        String orderId = dataJson.getString("orderId");
        // 根据订单号，查询寄件人收件人信息
        String sql = "SELECT o.OrderNumber, m.MAILNAME, m.MAILTEL, m.ADDRESS, r.Id, r.C_AGENTCITYNAME, r.C_AGENTADDRESS FROM TrainOrderOffline o WITH (nolock) LEFT JOIN mailaddress m WITH (nolock) ON o.Id = m.ORDERID LEFT JOIN T_CUSTOMERAGENT_RAPIDSEND r WITH (nolock) ON o.AgentId = r.Id WHERE o.Id = "
                + orderId;
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName + "异常日志", e, String.valueOf(random));
        }
        if (dataTable.GetRow().size() > 0) {
            // 接口信息
            JSONObject data = new JSONObject();
            // 参数拼接
            String cityName = String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCITYNAME").GetValue());// 订单所在城市名
            data.put("cityName", cityName);
            String orderNumber = String.valueOf(dataTable.GetRow().get(0).GetColumn("OrderNumber").GetValue());
            data.put("orderNumber", orderNumber);// 订单ID
            data.put("toAddress", String.valueOf(dataTable.GetRow().get(0).GetColumn("ADDRESS").GetValue()));// 目的地
            data.put("fromAddress", String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTADDRESS").GetValue()));// 代售点地址
            String citycode = PropertyUtil.getValue(cityName, "train.properties");
            data.put("citycode", citycode);// 城市代码
            // 返回
            String reqResult = reqParam(logName, random, data, "getOrderPrice", expressName);
            responseJson = JSONObject.parseObject(reqResult);
            JSONObject resultJson = JSONObject.parseObject(responseJson.getString("result"));
            // 计算价格成功
            if (responseJson.getBoolean("success") && resultJson.getBoolean("success")) {
                WriteLog.write(logName, random + "----发布订单价格计算成功----");
                String priceToken = resultJson.getString("priceToken");
                double totalMoney = resultJson.getDouble("totalMoney");
                double payMoney = resultJson.getDouble("payMoney");
                String distance = resultJson.getString("distance");
                double freightMoney = resultJson.getDouble("freightMoney");
                String couponId = resultJson.getString("couponId");
                double couponAmount = resultJson.getDouble("couponAmount");
                double addFee = resultJson.getDouble("addFee");
                double goodsInsuranceMoney = resultJson.getDouble("goodsInsuranceMoney");
                String totalPriceOff = resultJson.getString("totalPriceOff");
                data = new JSONObject();
                // 收件人
                data.put("receiver", dataTable.GetRow().get(0).GetColumnString("MAILNAME"));
                // 收件人电话
                data.put("receiverPhone", dataTable.GetRow().get(0).GetColumnString("MAILTEL"));
                // 订单备注 最长140个汉字
                data.put("orderNote", dataTable.GetRow().get(0).GetColumnString("ADDRESS"));
                // 金额令牌
                data.put("priceToken", priceToken);
                // 订单金额
                data.put("orderPrice", String.valueOf(totalMoney));
                // 支付金额
                data.put("payMoney", String.valueOf(payMoney));
                // 推送方式（0 开放订单，1指定跑男，2商户绑定的跑男）默认传0即可，测试订单传2
                data.put("pushType", pushType);
                // 取件是否给我打电话 1需要 0不需要
                data.put("callmeWithtake", callmeWithtake);
                // 回调地址
                data.put("callbackUrl", callbackUrl);
                // 城市代码
                data.put("citycode", citycode);
                // 返回
                reqResult = reqParam(logName, random, data, "addOrder", expressName);
                responseJson = JSONObject.parseObject(reqResult);
                resultJson = JSONObject.parseObject(responseJson.getString("result"));
                // 下单成功
                if (responseJson.getBoolean("success") && resultJson.getBoolean("success")) {
                    String order_code = resultJson.getString("partnerOrderNumber");
                    WriteLog.write(logName, random + "----发布订单成功----快递单号：" + order_code);
                    String state = "1";
                    String state_text = "下单成功";
                    String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                    String sqlins = "INSERT INTO uupt_route (order_code, driver_name, driver_jobnum, driver_mobile, state, state_text, acceptTime, orderNumber, return_msg, return_code) VALUES "
                            + "('" + order_code + "', '', '', '', " + state + " , '" + state_text + "', '" + timestamp
                            + "', '" + orderNumber + "', '', 'ok')";
                    DBHelperOffline.insertSql(sqlins);
                    String sqluuptsel = "SELECT * FROM uupt_orderprice WHERE orderNumber = '" + orderNumber + "'";
                    DataTable dataTable1 = new DataTable();
                    try {
                        dataTable1 = DBHelperOffline.GetDataTable(sqluuptsel, null);
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException(logName + "异常日志", e, String.valueOf(random));
                    }
                    if (dataTable1 == null || dataTable1.GetRow().size() == 0) {
                        String sqluuptins = "INSERT INTO uupt_orderprice (orderNumber, price_token, total_money, need_paymoney, distance, freight_money, couponid, coupon_amount, addfee, goods_insurancemoney, total_priceoff, orderNumberAgain) VALUES "
                                + "('" + orderNumber + "', '" + priceToken + "', " + totalMoney + ", " + payMoney
                                + ", '" + distance + "' , " + freightMoney + ", '" + couponId + "', " + couponAmount
                                + ", " + addFee + ", " + goodsInsuranceMoney + ", '" + totalPriceOff + "', '')";
                        DBHelperOffline.insertSql(sqluuptins);
                    }
                    else {
                        String sqluuptupt = "UPDATE uupt_orderprice set price_token = '" + priceToken + "', "
                                + "total_money = " + totalMoney + ", " + "need_paymoney = " + payMoney + ", "
                                + "distance = '" + distance + "', " + "freight_money = " + freightMoney + ", "
                                + "couponid = '" + couponId + "', " + "coupon_amount = " + couponAmount + ", "
                                + "addfee = " + addFee + ", " + "goods_insurancemoney = " + goodsInsuranceMoney + ", "
                                + "total_priceoff = '" + totalPriceOff + "', "
                                + "orderNumberAgain = '' WHERE orderNumber = '" + orderNumber + "'";
                        DBHelperOffline.UpdateData(sqluuptupt);
                    }
                }
            }
        }
        return responseJson;
    }

    /**
     * 二次发布订单
     * 
     * @time 2017年10月23日 下午4:09:54
     * @author liujun
     */
    private JSONObject addOrderAgain(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        // 订单ID
        String orderId = dataJson.getString("orderId");
        // 根据订单号，查询寄件人收件人信息
        String sql = "SELECT o.OrderNumber, m.MAILNAME, m.MAILTEL, m.ADDRESS, r.Id, r.C_AGENTCITYNAME, r.C_AGENTADDRESS FROM TrainOrderOffline o WITH (nolock) LEFT JOIN mailaddress m WITH (nolock) ON o.Id = m.ORDERID LEFT JOIN T_CUSTOMERAGENT_RAPIDSEND r WITH (nolock) ON o.AgentId = r.Id WHERE o.Id = "
                + orderId;
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName + "异常日志", e, String.valueOf(random));
        }
        if (dataTable.GetRow().size() > 0) {
            // 接口信息
            JSONObject data = new JSONObject();
            // 参数拼接
            String cityName = String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCITYNAME").GetValue());// 订单所在城市名
            data.put("cityName", cityName);
            String orderNumber = dataTable.GetRow().get(0).GetColumnString("OrderNumber");
            String orderNumberAgain = dataTable.GetRow().get(0).GetColumnString("OrderNumber")
                    + (new Random().nextInt(90) + 10);
            data.put("orderNumber", orderNumberAgain);// 订单ID
            data.put("toAddress", dataTable.GetRow().get(0).GetColumnString("ADDRESS"));// 目的地
            data.put("fromAddress", dataTable.GetRow().get(0).GetColumnString("C_AGENTADDRESS"));// 代售点地址
            String citycode = PropertyUtil.getValue(cityName, "train.properties");
            data.put("citycode", citycode);// 城市代码
            // 返回
            String reqResult = reqParam(logName, random, data, "getOrderPrice", expressName);
            responseJson = JSONObject.parseObject(reqResult);
            JSONObject resultJson = JSONObject.parseObject(responseJson.getString("result"));
            // 计算价格成功
            if (responseJson.getBoolean("success") && resultJson.getBoolean("success")) {
                WriteLog.write(logName, random + "----二次发布订单价格计算成功----");
                String priceToken = resultJson.getString("priceToken");
                double totalMoney = resultJson.getDouble("totalMoney");
                double payMoney = resultJson.getDouble("payMoney");
                String distance = resultJson.getString("distance");
                double freightMoney = resultJson.getDouble("freightMoney");
                String couponId = resultJson.getString("couponId");
                double couponAmount = resultJson.getDouble("couponAmount");
                double addFee = resultJson.getDouble("addFee");
                double goodsInsuranceMoney = resultJson.getDouble("goodsInsuranceMoney");
                String totalPriceOff = resultJson.getString("totalPriceOff");
                data = new JSONObject();
                // 收件人
                data.put("receiver", dataTable.GetRow().get(0).GetColumnString("MAILNAME"));
                // 收件人电话
                data.put("receiverPhone", dataTable.GetRow().get(0).GetColumnString("MAILTEL"));
                // 订单备注 最长140个汉字
                data.put("orderNote", dataTable.GetRow().get(0).GetColumnString("ADDRESS"));
                // 金额令牌
                data.put("priceToken", priceToken);
                // 订单金额
                data.put("orderPrice", String.valueOf(totalMoney));
                // 支付金额
                data.put("payMoney", String.valueOf(payMoney));
                // 推送方式（0 开放订单，1指定跑男，2商户绑定的跑男）默认传0即可，测试订单传2
                data.put("pushType", pushType);
                //取件是否给我打电话 1需要 0不需要
                data.put("callmeWithtake", callmeWithtake);
                // 回调地址
                data.put("callbackUrl", callbackUrl);
                // 城市代码
                data.put("citycode", citycode);
                // 返回
                reqResult = reqParam(logName, random, data, "addOrder", expressName);
                responseJson = JSONObject.parseObject(reqResult);
                resultJson = JSONObject.parseObject(responseJson.getString("result"));
                // 下单成功
                if (responseJson.getBoolean("success") && resultJson.getBoolean("success")) {
                    String order_code = resultJson.getString("partnerOrderNumber");
                    WriteLog.write(logName,
                            random + "----二次发布订单成功----" + "二次发布订单号：" + orderNumberAgain + "----快递单号：" + order_code);
                    String state = "1";
                    String state_text = "下单成功";
                    String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                    String sqlins = "INSERT INTO uupt_route (order_code, driver_name, driver_jobnum, driver_mobile, state, state_text, acceptTime, orderNumber, return_msg, return_code) VALUES "
                            + "('" + order_code + "', '', '', '', " + state + " , '" + state_text + "', '" + timestamp
                            + "', '" + orderNumberAgain + "', '', 'ok')";
                    DBHelperOffline.insertSql(sqlins);
                    String sqluuptsel = "select orderNumberAgain from uupt_orderprice where orderNumber = '"
                            + orderNumber + "' ORDER BY orderNumberAgain DESC";
                    DataTable dataTable1 = new DataTable();
                    try {
                        dataTable1 = DBHelperOffline.GetDataTable(sqluuptsel, null);
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException(logName + "异常日志", e, String.valueOf(random));
                    }
                    if (dataTable1 != null && dataTable1.GetRow().size() == 1) {
                        WriteLog.write(logName, random + "----二次发布订单成功----" + "第一次发布二次订单：" + orderNumberAgain);
                        String sqluuptins = "INSERT INTO uupt_orderprice (orderNumber, price_token, total_money, need_paymoney, distance, freight_money, couponid, coupon_amount, addfee, goods_insurancemoney, total_priceoff, orderNumberAgain) VALUES "
                                + "('" + orderNumber + "', '" + priceToken + "', " + totalMoney + ", " + payMoney
                                + ", '" + distance + "' , " + freightMoney + ", '" + couponId + "', " + couponAmount
                                + ", " + addFee + ", " + goodsInsuranceMoney + ", '" + totalPriceOff + "', '"
                                + orderNumberAgain + "')";
                        DBHelperOffline.insertSql(sqluuptins);
                    }
                    else {
                        WriteLog.write(logName, random + "----二次发布订单成功----" + "第n次发布二次订单：" + orderNumberAgain);
                        String orderNumberAgainOld = String
                                .valueOf(dataTable1.GetRow().get(0).GetColumn("orderNumberAgain").GetValue());
                        String sqluuptupt = "UPDATE uupt_orderprice set price_token = '" + priceToken + "', "
                                + "total_money = " + totalMoney + ", " + "need_paymoney = " + payMoney + ", "
                                + "distance = '" + distance + "', " + "freight_money = " + freightMoney + ", "
                                + "couponid = '" + couponId + "', " + "coupon_amount = " + couponAmount + ", "
                                + "addfee = " + addFee + ", " + "goods_insurancemoney = " + goodsInsuranceMoney + ", "
                                + "total_priceoff = '" + totalPriceOff + "', " + "orderNumberAgain = '"
                                + orderNumberAgain + "' WHERE orderNumberAgain = '" + orderNumberAgainOld + "'";
                        DBHelperOffline.UpdateData(sqluuptupt);
                    }
                }
            }
        }
        return responseJson;
    }

    /**
     * 二次取消订单接口
     * 
     * @time 2017年10月23日 下午4:10:19
     * @author liujun
     */
    private JSONObject cancelOrderAgain(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        // 接口信息
        JSONObject data = new JSONObject();
        // 参数拼接
        String orderId = dataJson.getString("orderId");
        // 根据订单号，查询收件人信息
        String sql = "SELECT r.C_AGENTCITYNAME, o.orderNumber, m.ExpressNum FROM TrainOrderOffline o LEFT JOIN mailaddress m ON o.Id = m.ORDERID LEFT JOIN T_CUSTOMERAGENT_RAPIDSEND r on o.AgentId = r.ID WHERE o.Id = "
                + orderId;
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        if (dataTable.GetRow().size() > 0) {
            String orderNumber = dataTable.GetRow().get(0).GetColumnString("orderNumber");
            // 根据真是订单号订单号，查询订单信息
            String sql1 = "select orderNumberAgain from uupt_orderprice where orderNumber = '" + orderNumber
                    + "' ORDER BY orderNumberAgain DESC";
            DataTable dataTable1 = new DataTable();
            try {
                dataTable1 = DBHelperOffline.GetDataTable(sql1, null);
            }
            catch (Exception e) {
                // 记录异常日志
                ExceptionTCUtil.handleTCException(e);
                responseJson.put("message", "失败");
                responseJson.put("success", false);
            }
            // 取到数据
            if (dataTable1.GetRow().size() > 1) {
                orderNumber = String.valueOf(dataTable1.GetRow().get(0).GetColumn("orderNumberAgain").GetValue());//订单ID
            }
            String cityName = String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCITYNAME").GetValue());
            String citycode = PropertyUtil.getValue(cityName, "train.properties");
            data.put("citycode", citycode);// 城市代码
            data.put("orderNumber", orderNumber);// 平台订单号
            data.put("partnerOrderNumber", dataTable.GetRow().get(0).GetColumnString("ExpressNum"));// uu订单号
            data.put("cancelReason", dataJson.getString("cancelReason"));// 取消原因
        }
        method = method.replace("Again", "");
        // 返回
        String reqResult = reqParam(logName, random, data, method, expressName);
        responseJson = JSONObject.parseObject(reqResult);
        return responseJson;
    }

    /**
     * 取消订单接口
     * 
     * @time 2017年10月23日 下午4:10:19
     * @author liujun
     */
    private JSONObject cancelOrder(String logName, String expressName, long random, String method, JSONObject dataJson,
            JSONObject responseJson) {
        // 接口信息
        JSONObject data = new JSONObject();
        // 参数拼接
        String orderId = dataJson.getString("orderId");
        // 根据订单号，查询收件人信息
        String sql = "SELECT r.C_AGENTCITYNAME, o.orderNumber, m.ExpressNum FROM TrainOrderOffline o LEFT JOIN mailaddress m ON o.Id = m.ORDERID LEFT JOIN T_CUSTOMERAGENT_RAPIDSEND r on o.AgentId = r.ID WHERE o.Id = "
                + orderId;
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        if (dataTable.GetRow().size() > 0) {
            String cityName = String.valueOf(dataTable.GetRow().get(0).GetColumn("C_AGENTCITYNAME").GetValue());
            String citycode = PropertyUtil.getValue(cityName, "train.properties");
            data.put("citycode", citycode);// 城市代码
            data.put("orderNumber", dataTable.GetRow().get(0).GetColumnString("orderNumber"));// 平台订单号
            data.put("partnerOrderNumber", dataTable.GetRow().get(0).GetColumnString("ExpressNum"));// uu订单号
            data.put("cancelReason", dataJson.getString("cancelReason"));// 取消原因
        }
        // 返回
        String reqResult = reqParam(logName, random, data, method, expressName);
        responseJson = JSONObject.parseObject(reqResult);
        return responseJson;
    }

    /**
     * 查询价格接口
     * 
     * @time 2017年10月24日 上午9:49:30
     * @author liujun
     */
    private JSONObject getOrderPrice(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        // 接口信息
        JSONObject data = new JSONObject();
        // 参数拼接
        data.put("cityName", dataJson.getString("cityName"));// 订单所在城市名
        data.put("orderNumber", dataJson.getString("orderNumber"));// 订单ID
        data.put("toAddress", dataJson.getString("toAddress"));// 目的地
        data.put("fromAddress", dataJson.getString("fromAddress"));// 代售点地址
        String citycode = PropertyUtil.getValue(dataJson.getString("cityName"), "train.properties");
        data.put("citycode", citycode);// 城市代码
        // 返回
        String reqResult = reqParam(logName, random, data, method, expressName);
        responseJson = JSONObject.parseObject(reqResult);
        return responseJson;
    }

    /**
     * 获取快递路由
     * 
     * @time 2017年10月24日 上午9:49:30
     * @author liujun
     */
    private JSONObject getExpressRoute(String logName, String expressName, long random, String method,
            JSONObject dataJson, JSONObject responseJson) {
        String message = "成功";
        boolean success = true;
        String result = "";
        String expressNum = dataJson.getString("expressNum");
        String sql = "select * from uupt_route where order_code = '" + expressNum + "' ORDER BY acceptTime";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            message = "失败";
            success = false;
            // 记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
        if (dataTable != null && dataTable.GetRow().size() > 0) {
            JSONArray list = new JSONArray();
            for (int i = 0; i < dataTable.GetRow().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                // 表示处理时间
                String acceptTime = dataTable.GetRow().get(i).GetColumnString("acceptTime").substring(0, 19);
                // 跑男姓名(跑男接单后)
                String driver_name = dataTable.GetRow().get(i).GetColumnString("driver_name");
                // 跑男工号(跑男接单后)
                String driver_jobnum = dataTable.GetRow().get(i).GetColumnString("driver_jobnum");
                // 跑男电话(跑男接单后)
                String driver_mobile = dataTable.GetRow().get(i).GetColumnString("driver_mobile");
                // 当前状态
                String state_text = dataTable.GetRow().get(i).GetColumnString("state_text");
                String state = dataTable.GetRow().get(i).GetColumnString("state");
                // 表示处理动作
                String remark = "";
                if ("1".equals(state)) {
                    remark = "当前状态：" + state_text;
                }
                else {
                    remark = "跑男姓名：" + driver_name + "；跑男工号：" + driver_jobnum + "；跑男电话：" + driver_mobile + "；当前状态："
                            + state_text;
                }
                jsonObject.put("acceptTime", acceptTime);
                jsonObject.put("remark", remark);
                list.add(jsonObject);
            }
            result = list.toString();
        }
        //统一返回结果
        responseJson.put("result", result);
        responseJson.put("success", success);
        responseJson.put("message", message);
        return responseJson;
    }
}
