package com.ccservice.offlineExpress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.util.CommonUtil;
import com.ccservice.offlineExpress.util.RouteCallBackUtil;

/**
 * 顺丰路由信息推送接口
 * @time 2017年12月7日 上午11:33:02
 * @author zcn
 */
@SuppressWarnings("serial")
public class SFRouteCallbackServlet extends HttpServlet implements Servlet {
    //日志名称
    private static final String logName = "线下火车票_顺丰回调路由信息";

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.doPost(req, resp);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //随机
        int random = CommonUtil.randomNum();
        //编码
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
        //返回结果,统一返回OK,异常时返回失败
        String result = "<Response service='RoutePushService'><Head>OK</Head></Response>";
        //参数解析
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "utf-8"));
        String line = "";
        StringBuffer buf = new StringBuffer(1024);
        while ((line = br.readLine()) != null) {
            buf.append(line);
        }
        //请求数据
        String reqParam = buf.toString();
        //日志
        WriteLog.write(logName, random + "---推送信息---" + reqParam);
        //异常捕捉
        try {
            if (!"".equals(reqParam) && reqParam != null) {
                //URL解码
                String reqParamUrlDecoder = URLDecoder.decode(reqParam, "utf-8");
                //参数解析
                Document docbefore = Jsoup.parse(reqParamUrlDecoder);
                Elements list = docbefore.select("WaybillRoute");
                //循环
                for (int i = 0; i < list.size(); i++) {
                    //单个订单
                    Element route = list.get(i);
                    //快递运单号
                    String mailNo = route.attr("mailno");
                    //调用顺丰路由查询接口
                    RouteCallBackUtil.flushExpressRoute(logName, random, mailNo);
                }
            }
        }
        catch (Exception e) {
            result = "<Response service='RoutePushService'><Head>ERR</Head><ERROR code='4001'>系统发生数据错误或运行时异常</ERROR></Response>";
            //记录异常日志
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        finally {
            response.getWriter().write(result);
        }
    }
}
