package com.ccservice.t12306;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

public class JMail {
    final static String ip = "121.199.25.199";

    //发送邮件  
    public static void sendMail() {
        String host = ip; // 指定的smtp服务器，本机的局域网IP  
        //String host = "192.168.1.98"; // 指定的smtp服务器，本机的局域网IP  
        //        String host = "localhost"; // 本机smtp服务器  
        //String host = "smtp.163.com"; // 163的smtp服务器  
        String from = "yinshubin@51dzp.com"; // 邮件发送人的邮件地址  
        String to = "2502661041@qq.com"; // 邮件接收人的邮件地址  
        final String username = "yinshubin"; //发件人的邮件帐户  
        final String password = "123456"; //发件人的邮件密码  

        // 创建Properties 对象  
        Properties props = System.getProperties();

        // 添加smtp服务器属性  
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");

        // 创建邮件会话  
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }

        });

        try {
            // 定义邮件信息  
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //message.setSubject(transferChinese("我有自己的邮件服务器了"));  
            message.setSubject("I hava my own mail server");
            message.setText("From now, you have your own mail server, congratulation!");

            // 发送消息  
            //            session.getTransport("smtp").send(message);
            Transport.send(message); //也可以这样创建Transport对象发送  

        }
        catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String s = getMail("tc000000047", "123456", "121.199.25.199");
        System.out.println(s);
    }

    /**
     * 获取继续激活的邮件 
     * @param mailname 邮箱用户名
     * @param mailpassword 邮箱密码
     * @param host IP
     * @return
     * @time 2014年10月22日 下午4:05:33
     * @author yinshubin
     */
    public static String getMail(final String mailname, final String mailpassword, String host) {
        try {
            //            Thread.sleep(20000);
            Thread.sleep(1000);
        }
        catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //        String host = "223.4.155.3";
        String result = "";
        // 创建Properties 对象  
        //        Properties props = new Properties();
        //
        //        // 创建邮件会话  
        //        Session session = Session.getDefaultInstance(props, new Authenticator() {
        //            @Override
        //            public PasswordAuthentication getPasswordAuthentication() {
        //                return new PasswordAuthentication(mailname, mailpassword);
        //            }
        //        });

        // 创建一个有具体连接信息的Properties对象  
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "pop3");
        props.setProperty("mail.pop3.host", host);

        // 使用Properties对象获得Session对象  
        Session session = Session.getInstance(props);
        session.setDebug(true);

        try {
            // 获取邮箱的pop3存储  
            Store store = session.getStore("pop3");
            store.connect(host, mailname, mailpassword);
            // 获取inbox文件  
            Folder folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);//HOLDS_MESSAGES);// HOLDS_FOLDERS);// READ_WRITE);// READ_ONLY); //打开，打开后才能读取邮件信息  
            // 获取邮件消息  
            Message message[] = folder.getMessages();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                message[0].writeTo(baos);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            result = baos.toString();
            try {
                result = new String(result.getBytes("GBK"), "UTF-8");
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (result.contains("https://kyfw.12306.cn/otn//regist/activeAccount?userNam")) {
                result = result.split("https://kyfw.12306.cn/otn//regist/activeAccount\\?userNam")[1];
                result = ("https://kyfw.12306.cn/otn//regist/activeAccount?userNam" + result.substring(
                        result.indexOf("e")).split(">https")[0]).replace("3D", "");
            }
            // 关闭资源  
            folder.close(false);
            store.close();

        }
        catch (MessagingException e) {
            e.printStackTrace();
        }

        return result;

    }

    //邮件主题中文字符转换  
    public static String transferChinese(String strText) {
        try {
            strText = MimeUtility.encodeText(new String(strText.getBytes(), "GB2312"), "GB2312", "B");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return strText;
    }

    /**
     * 向JAMES MAIL里添加账号 
     * @param mail
     * @param mailpassword
     * @return
     * @time 2014年10月22日 下午3:57:57
     * @author yinshubin
     */
    public static String adduser(String mail, String mailpassword) {
        String result = "";
        try {
            int port = 4555;
            String user = "root";//whzf011843adm
            String password = "root";//5n0wbIrdsMe3
            NetTelnet telnet = new NetTelnet(ip, port, user, password);
            if (telnet.login(user, password)) {
                String r4 = telnet.sendCommand("adduser " + mail + " " + mailpassword, "added", "Error adding user "
                        + mail);
                if (r4 != null) {
                    result = r4;
                }
            }
            else {
                result = "telnet登录失败";
            }
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}