package com.ccservice.t12306;

public class Dynamic12306Result {

    private String key;//动态key，如ODg0Njk1

    private String value;//动态key对应的value，如NTM3YTZmZmNiNjdmNGFkOA==

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}