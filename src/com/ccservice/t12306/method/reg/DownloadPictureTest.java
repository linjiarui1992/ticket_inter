package com.ccservice.t12306.method.reg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;

/**
 * 下载图片工具类
 * 
 * @author bingbing feng 2013-03-14
 * 
 */
public class DownloadPictureTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            for (int i = 0; i < 100; i++) {
                Thread.sleep(1500L);
                long currentTime = System.currentTimeMillis();
                //            //
                String url_validate_code = "https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module=login&rand=sjrand&"
                        + currentTime;//验证码
                //            String fileName = currentTime + ".jpg";
                String dirPath = "D:/OCR_EX/";
                //            String Cookie = "JSESSIONID=6CCE2014E884F3F65D465AC6C0779EFC.TSkyServer3";
                //            getCheckCodePicFromXX(url_validate_code, fileName, dirPath, Cookie);
                //            System.out.println(URLEncoder.encode("%E5%B4%94%E5%AE%9D%E8%90%8D", "UTF-8"));
                HttpsUtil.Get_picture(url_validate_code, dirPath, currentTime + ".jpg", "");
            }
        }
        catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * @param url  请求地址
     * @param s 
     * @param fileName
     * @param dirPath
     * @param Cookie
     */
    private static void getCheckCodePicFromXX(String url, String fileName, String dirPath, String Cookie) {
        //        String url = "http://wap.xxx.com//p/ex.d?u_id=" + uid + "&m=gvcd&s=" + s;
        //        String dirPath = "D:/OCR_EX/";
        downloadPicture(url, dirPath, fileName, Cookie);
    }

    /**
     * @param url  请求地址(http)
     * @param dirPath 图片文件保存的路径
     * @param filePath 文件名
     * @param Cookie 
     * @throws NoSuchAlgorithmException 
     */
    public static String downloadPicture(String url, String dirPath, String filePath, String Cookie) {
        String resultString = "-1";

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        httpget.setHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1");
        httpget.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        //            httpget.setHeader("Cookie", Cookie);
        try {
            HttpResponse resp = httpclient.execute(httpget);
            Header[] headers = resp.getAllHeaders();
            for (int i = 0; i < headers.length; i++) {
                if (headers[i].getName().indexOf("Set-Cookie") >= 0) {
                    resultString = headers[i].getValue().split(";")[0].replaceAll("JSESSIONID=", "");
                    break;
                }
            }
            if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                HttpEntity entity = resp.getEntity();
                InputStream in = entity.getContent();
                savePicToDisk(in, dirPath, filePath);
                //                System.out.println("保存图片 " + filePath + " 成功....");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            httpclient.getConnectionManager().shutdown();
        }
        WriteLog.write("travelsky", "url:" + url);
        WriteLog.write("travelsky", "cookie:" + resultString);
        return resultString;
    }

    /**
     * @param url  请求地址(https)
     * @param dirPath 图片文件保存的路径
     * @param filePath 文件名
     * @param Cookie 
     * @throws NoSuchAlgorithmException 
     */
    public static String downloadPicture_https(String url, String dirPath, String filePath, String Cookie) {
        String resultString = "-1";

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        httpget.setHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1");
        httpget.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        //            httpget.setHeader("Cookie", Cookie);

        try {
            HttpResponse resp = httpclient.execute(httpget);
            Header[] headers = resp.getAllHeaders();
            for (int i = 0; i < headers.length; i++) {
                if (headers[i].getName().indexOf("Set-Cookie") >= 0) {
                    resultString = headers[i].getValue().split(";")[0].replaceAll("JSESSIONID=", "");
                    break;
                }
            }
            if (HttpStatus.SC_OK == resp.getStatusLine().getStatusCode()) {
                HttpEntity entity = resp.getEntity();
                InputStream in = entity.getContent();
                savePicToDisk(in, dirPath, filePath);
                //                System.out.println("保存图片 " + filePath + " 成功....");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            httpclient.getConnectionManager().shutdown();
        }
        WriteLog.write("travelsky", "url:" + url);
        WriteLog.write("travelsky", "cookie:" + resultString);
        return resultString;
    }

    /**
     * 将图片写到 硬盘指定目录下   如果文件大小小于1KB 返回false
     * @param in
     * @param dirPath
     * @param filePath
     * @param isRegist 是否是注册
     */
    public static boolean savePicToDiskBool(InputStream in, String dirPath, String filePath, boolean isRegist) {
        int buflenth = 0;
        try {
            File dir = new File(dirPath);
            if (dir == null || !dir.exists()) {
                dir.mkdirs();
            }

            //文件真实路径
            String realPath = dirPath.concat(filePath);
            File file = new File(realPath);
            if (file == null || !file.exists()) {
                file.createNewFile();
            }

            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = in.read(buf)) != -1) {
                fos.write(buf, 0, len);
                buflenth += len;
            }
            fos.flush();
            fos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                in.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        Integer picturesizemin = 1638;
        Integer picturesizemax = 12288;
        long l_sleeptime = 3000L;
        String picturesize = isRegist ? PropertyUtil.getValue("oldPictureSizeMin") : PropertyUtil
                .getValue("picturesizemin");
        String maxpicturesize = isRegist ? PropertyUtil.getValue("oldPictureSizeMax") : PropertyUtil
                .getValue("picturesizemax");
        String sleeptime = PropertyUtil.getValue("picturesleeptime");
        if (picturesize != null && !"".equals(picturesize)) {
            picturesizemin = Integer.valueOf(picturesize);
        }
        if (maxpicturesize != null && !"".equals(maxpicturesize)) {
            picturesizemax = Integer.valueOf(maxpicturesize);
        }
        if (sleeptime != null && !"".equals(sleeptime)) {
            l_sleeptime = Long.valueOf(sleeptime);
        }
        if (buflenth > picturesizemin && buflenth < picturesizemax) {
            return true;
        }
        else {
            try {
                Thread.sleep(l_sleeptime);
            }
            catch (InterruptedException e) {
            }
            return false;
        }
    }

    public static void savePicToDisk(InputStream in, String dirPath, String filePath) {

        try {
            File dir = new File(dirPath);
            if (dir == null || !dir.exists()) {
                dir.mkdirs();
            }

            //文件真实路径
            String realPath = dirPath.concat(filePath);
            File file = new File(realPath);
            if (file == null || !file.exists()) {
                file.createNewFile();
            }

            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = in.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }
            fos.flush();
            fos.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                in.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
