package com.ccservice.t12306.method.reg;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.StringUtil;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.mobileCode.IMobileCode;
import com.ccservice.mobileCode.util.MobileCodeBeanUtil;
import com.ccservice.mobileCode.util.RequestUtil;
import com.travelGuide.CCSHttpClientnew;

/**
 * 新的注册12306账号
 * @time 2015年9月25日 下午1:17:57
 * @author chendong
 */
public class Reg12306New extends SupplyMethod {

    /**
     * 
     * @param mobileCodeType //# 1:优码,2:淘码,3:y码,4:壹码,5:爱码,6:卓码,7:要码
     */
    public Reg12306New(String mobileCodeType) {
        initCookie();
        this.mobileCodeType = mobileCodeType;
        this.mobilecode = MobileCodeBeanUtil.initMobileCodeType(mobileCodeType);//初始化手机验证码 平台
        //        try {
        //            mainReg12306NewMaxReSend = Integer.parseInt(PropertyUtil.getValue("mainReg12306NewMaxReSend",
        //                    "train.reg.properties"));
        //        }
        //        catch (Exception e) {
        //        }
    }

    private String useProxy;// = "1";

    private String proxyHost;// = "112.84.130.3";

    private String proxyPort;// = "80";

    String cookieString;

    String initResult;//初始化页面

    String regInit = "https://kyfw.12306.cn/otn/regist/init";

    CCSHttpClientnew httpClient = new CCSHttpClientnew(false, 10000L);

    IMobileCode mobilecode;

    //1:云码,2:淘码,
    String mobileCodeType;

    //手机号
    String mobileNo;

    //短信验证码
    String randCode;

    boolean randCodeIsHave = false;//是否已经收到验证码的注册 true 是 false 不是

    String smsCheckCode = "";//短信验证码的内容

    String loginname;//当前正在注册的账号的用户名

    /**
     * 
     * @param jsonStr
     * @return
     * @time 2015年9月25日 下午3:23:27
     * @author chendong
     */
    public String mainReg12306New(String jsonStr) {
        String mainReg12306NewResult = "网络繁忙";
        //【第一步】判断访问12306是否正常
        if (!this.initResult.contains("您的操作频率过快")) {
            JSONObject jsonobject = JSONObject.parseObject(jsonStr);
            String loginame = jsonobject.getString("loginame");
            this.loginname = loginame;
            String logpassword = jsonobject.getString("logpassword");
            String name = jsonobject.getString("name").trim();
            String idno = jsonobject.getString("idno").trim();
            String email = "";
            this.mobileNo = jsonobject.getString("mobile");
            this.randCode = jsonobject.getString("randCode");
            if (this.randCode != null && this.randCode.length() == 6) {
                this.randCodeIsHave = true;
                this.cookieString = jsonobject.getString("cookieString");
            }
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", loginame + ":mainReg12306New:jsonStr:"
                    + jsonStr);
            //【第二步】检测当前需要注册的用户名是否存在
            boolean checkPass = checkUserName(loginame);
            if (!checkPass) {
                //                mainReg12306NewResult = mainReg12306New(name, idno, loginame, logpassword, email);
                //【第三步】获取手机号(获取新手机号或者使用已有手机号)
                //如果第一次获取,并且手机号码不是空的就先不获取手机号码
                if (this.mobileNo != null && !"".equals(this.mobileNo)) {
                    System.out.println("注册(" + loginame + ":老号码:" + this.mobileNo + ")");
                    WriteLog.write("12306Reg", "Reg12306New", "注册(" + loginame + ":老号码:" + this.mobileNo + ")");
                }
                else {
                    this.mobileNo = getRegMobile();//获取一个手机号码
                    WriteLog.write("12306Reg", "Reg12306New", "注册(" + loginame + ":获取新号码:" + this.mobileNo + ")");
                    System.out.println("注册(" + loginame + ":获取新号码:" + this.mobileNo + ")");
                }
                //获取手机号成功。。。
                if (this.mobileNo.length() == 11 && this.mobileNo.startsWith("1")) {
                    //【第四步】拿到手机号去12306模拟点击下一步并拿到返回结果
                    String getRandCodeResult = getRandCodeByFor(name, idno, loginame, logpassword, email);
                    WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", loginame + ":getRandCodeByFor("
                            + this.mobileNo + "):三四步结果:" + getRandCodeResult);
                    if ("true".equals(getRandCodeResult)) {
                        //【第五步】调用发送短信的方法
                        //【第六步】获取验证码
                        //【第七步】获取验证码成功后提交到12306
                        mainReg12306NewResult = mainReg12306New_old(name, idno, loginame, logpassword, email);
                    }
                    else {
                        mainReg12306NewResult = getRandCodeResult;
                    }
                    WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", loginame + ":mainReg12306NewResult:"
                            + mainReg12306NewResult);
                }
                else {
                    mainReg12306NewResult = "获取手机号失败";
                    //            break;
                }
            }
            else {
                mainReg12306NewResult = "用户名已经被注册";
            }
        }
        else {
            mainReg12306NewResult = "您的操作频率过快|网络繁忙";
        }
        return mainReg12306NewResult;
    }

    /**
     * 
     * @time 2015年10月15日 下午2:53:10
     * @author chendong
     */
    private String mainReg12306New_old(String name, String idno, String logname, String logpassword, String email) {
        String mainReg12306NewResult = "-1";
        String sendResult = "true";//给12306发送短信结果
        if (!this.randCodeIsHave) {//是否已经收到验证码的注册 true 是 false 不是
            //【第五步】调用发送短息的方法
            sendResult = mobilecode.send(this.mobileNo, "999");
            System.out.println(this.mobileNo + ":this.mobileCodeType:" + this.mobileCodeType + ":发短信结果:" + sendResult);
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":subDetail:result:" + this.mobileNo
                    + ":【五步】this.mobileCodeType:" + this.mobileCodeType + ":发短信结果:" + sendResult);
        }
        if ("true".equals(sendResult)) {//发送验证码成功后进行的逻辑
            //            for (int i = 0; i < 3; i++) {
            //【第六步】获取验证码
            //【第七步】获取验证码成功后提交到12306
            mainReg12306NewResult = getMainReg12306NewResult(name, idno, logname, logpassword, email);
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":mainReg12306NewResult:"
                    + this.mobileNo + ":【六七步】获取验证码提交后结果:" + sendResult);
            if (mainReg12306NewResult.contains("您输入的验证码有误")) {
                WriteLog.write("12306Reg", "mobileCodeErr", loginname + ":您输入的验证码有误:" + this.mobileNo + ":"
                        + mobilecode);
            }
        }
        else {
            System.out.println(this.mobileNo + ":给12306发短信失败");
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":getVcodeAndReleaseMobile:"
                    + this.mobileNo + ":给12306发短信失败");
            mainReg12306NewResult = "false";
        }

        return mainReg12306NewResult + "|" + this.mobileNo + "|" + this.cookieString;
    }

    /**
     * 【第六步】获取验证码
     * 【第七步】获取验证码成功后提交到12306
     * @param name
     * @param idno
     * @param logname
     * @param logpassword
     * @param email
     * @return
     * @time 2016年3月10日 下午12:02:01
     * @author chendong
     */
    private String getMainReg12306NewResult(String name, String idno, String logname, String logpassword, String email) {
        String mainReg12306NewResult = "-1";
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":randCodeIsHave:" + this.randCodeIsHave);
        if (!this.randCodeIsHave) {//如果不是传过来的验证码的才走获取验证码的逻辑
            //【第六步】获取验证码
            getRandCodeString(logname);//获取验证码
        }
        if (this.randCode.length() == 6) {
            //【第七步】获取验证码成功后提交到12306
            String subDetail = subDetail(name, idno, logname, logpassword, email);//收到短信验证码以后请求注册
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":subDetail:result:" + subDetail);
            if (subDetail.contains("您输入的验证码有误")) {
                mainReg12306NewResult = "您输入的验证码有误," + this.randCode;
            }
            else if ("true".equals(subDetail)) {
                addIgnoreList(this.mobileNo, "1");//拉黑手机号
                mainReg12306NewResult = subDetail;
            }
            else if (subDetail.contains("您填写的身份信息重复")) {
                mainReg12306NewResult = "短信验证后身份信息重复," + randCode;
            }
            else {
                System.out.println(subDetail);
                mainReg12306NewResult = subDetail;
            }
        }
        else if (this.randCode.contains("no_data")) {
            addIgnoreList(this.mobileNo, "");
            mainReg12306NewResult = "no_data";
        }
        else {
            mainReg12306NewResult = smsCheckCode;
        }
        return mainReg12306NewResult;
    }

    private void getRandCodeString(String logname) {
        int getCodecount = 0;//一共获取验证码多少次了
        //        checkIsSendSuccess();
        int getCodeCountMax = getCodeCountMax();//获取短信时间最长时间 
        do {
            try {
                Thread.sleep(3000);//停3秒再获取短信内容
            }
            catch (Exception e) {
            }
            //获取验证码并释放
            //获取短信验证码
            smsCheckCode = mobilecode.getVcodeAndReleaseMobile("", this.mobileNo, "", "", "");//获取短信内容
            System.out.println(this.loginname + ":" + this.mobileNo + ":" + getCodecount + "次(" + getCodeCountMax
                    + "):smsCheckCode:" + smsCheckCode);
            getCodecount += 1;
            if (smsCheckCode.contains("验证码")) {
                break;
            }
        }
        while (getCodecount < getCodeCountMax && ("no_data".equals(smsCheckCode) || "".equals(smsCheckCode)));
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":getVcodeAndReleaseMobile:"
                + this.mobileNo + ":" + getCodecount + "次:收到的短信:" + smsCheckCode);
        this.randCode = StringUtil.getStringByRegex(smsCheckCode, "\\d{6}");

        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":getVcodeAndReleaseMobile:"
                + this.mobileNo + ":" + getCodecount + "次:this.randCode:" + this.randCode);
        System.out.println(this.mobileNo + ":" + getCodecount + "次:randCode:" + this.randCode);
    }

    private int getCodeCountMax() {
        int getCodeCountMax = 100;
        try {
            String getCodecount_String = PropertyUtil.getValue("getCodecount_String", "train.reg.properties");
            getCodeCountMax = Integer.parseInt(getCodecount_String);
        }
        catch (Exception e) {
        }
        return getCodeCountMax;
    }

    private void checkIsSendSuccess() {
        int getCodecount = 0;//
        if ("31".equals(this.mobileCodeType)) {//如果是自己的验证码
            do {
                try {
                    Thread.sleep(3000);//停3秒
                }
                catch (Exception e) {
                }
                DataTable dt = Reg12306DBHelper.GetDataTable("select top 1 * from HthyPhoneSim where phonenumber='"
                        + this.mobileNo + "'");
                if (dt.GetRow().size() > 0) {
                    DataRow DataRow = dt.GetRow().get(0);
                    if (DataRow.GetColumnInt("SendStatus") == 3) {
                        System.out.println(this.mobileNo + ":" + getCodecount + ":次:发送成功");
                        break;
                    }
                    else {
                        System.out.println(this.mobileNo + ":" + getCodecount + ":次:还没发送成功");
                        continue;
                    }
                }
                getCodecount++;
            }
            while (getCodecount < 200 && ("no_data".equals(smsCheckCode) || "".equals(smsCheckCode)));
        }
    }

    /**
     * 
     * @param name 乘客姓名
     * @param idno 证件号 
     * @param logname 用户名
     * @param logpassword 密码
     * @param email 邮箱
     * @param mobile 手机号
     * @time 2015年9月25日 下午2:05:54
     * @author chendong
     */
    private String getRandCodeByFor(String name, String idno, String logname, String logpassword, String email) {
        Long l1 = System.currentTimeMillis();
        String getRandCodeResult = "false";
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":注册" + logname + ":" + this.mobileNo);

        //【第一次向12306发送注册请求,对应12306的操作是点击下一步】
        getRandCodeResult = getRandCodebyMobile(name, idno, logname, logpassword, email, this.mobileNo);
        WriteLog.write("12306Reg", "Reg12306New", logname + ":(" + this.mobileNo + ")【第一次向12306发送注册请求】处理后结果->"
                + getRandCodeResult);
        System.out.println("(" + logname + ":" + this.mobileNo + ")【第一次向12306发送注册请求】处理后结果结果->" + getRandCodeResult);
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":结果->" + getRandCodeResult);
        if ("true".equals(getRandCodeResult)) {
            //                break;
        }
        else if (getRandCodeResult.contains("该证件号码已被注册") || getRandCodeResult.contains("您填写的身份信息重复")) {
            //                break;//
        }
        else if (getRandCodeResult.contains("该手机号码已被注册") || getRandCodeResult.contains("获取验证码次数过多")) {
            addIgnoreList(this.mobileNo, "0");//只有提示手机号已被注册了才拉黑手机号
            //                break;
        }
        else if (getRandCodeResult.contains("网络繁忙")) {
            //不拉黑;释放手机号
            CancelSMSRecv(this.mobileNo);
            //                break;
        }
        else if ("".equals(getRandCodeResult)) {
            //                break;
        }
        else {
            //                break;
        }

        long l2 = System.currentTimeMillis() - l1;
        return getRandCodeResult;
    }

    /**
     * 
     * @param l
     * @time 2016年3月10日 上午11:42:40
     * @author chendong
     */
    private void sleep(long l) {
        try {
            Thread.sleep(200L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放手机号
     * @time 2015年9月25日 下午6:15:23
     * @author chendong
     */
    private String CancelSMSRecv(String mobile) {
        String addignresult = "-1";
        addignresult = mobilecode.CancelSMSRecv("", mobile, "");
        System.out.println(mobile + ":释放结果:" + addignresult);
        return addignresult;

    }

    /**
     * 拉黑手机号
     * @time 2015年7月31日 下午5:36:39
     * @author chendong
     * @param string 
     */
    private String addIgnoreList(String mobile, String failmsg) {
        String addignresult = "-1";
        addignresult = mobilecode.AddIgnoreList("", mobile, "", failmsg);
        System.out.println(mobile + ":拉黑结果:" + addignresult);
        return addignresult;
    }

    /**
     * 获取一个手机号码
     * @return
     * @time 2015年9月25日 下午2:48:21
     * @author chendong
     */
    public String getRegMobile() {
        String mobileNo = mobilecode.GetMobilenum("", "", "");
        return mobileNo;
    }

    /**
     * 注册12306账号--填写详细信息之后点击下一步的操作
     * 【第一次向12306发送注册请求】
     * @param name
     * @param idno
     * @param cookie
     * @param logname
     * @param logpassword
     * @param email
     * @return 如果注册成功就返回true等待接收短信           
     * @time 2015年9月25日 下午1:58:45
     * @author chendong
     */
    private String getRandCodebyMobile(String name, String idno, String logname, String logpassword, String email,
            String mobile) {
        String result = "";
        try {
            name = URLEncoder.encode(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String par = "loginUserDTO.user_name="
                + logname
                + "&userDTO.password="
                + logpassword
                + "&confirmPassWord="
                + logpassword
                + "&loginUserDTO.name="
                + name
                + "&loginUserDTO.id_type_code=1&loginUserDTO.id_no="
                + idno
                + "&userDTO.country_code=CN&userDTO.born_date="
                + getbrithday(idno)
                + "&userDTO.email=&userDTO.mobile_no="
                + mobile
                + "&passenger_type=1&studentInfoDTO.province_code=11&studentInfoDTO.school_code="
                + "&studentInfoDTO.school_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.department="
                + "&studentInfoDTO.school_class=&studentInfoDTO.student_no=&studentInfoDTO.school_system=1"
                + "&studentInfoDTO.enter_year="
                + Calendar.getInstance().get(Calendar.YEAR)
                + "&studentInfoDTO.preference_card_no="
                + "&studentInfoDTO.preference_from_station_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.preference_from_station_code="
                + "&studentInfoDTO.preference_to_station_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.preference_to_station_code=";

        WriteLog.write("12306Reg", "Reg12306New", logname + ":(" + this.mobileNo + ")【第一次向12306发送注册请求】原始请求内容->"
                + this.cookieString);
        WriteLog.write("12306Reg", "Reg12306New", logname + ":(" + this.mobileNo + ")【第一次向12306发送注册请求】原始请求内容->" + par);
        result = get12306Info_getRandCodebyMobile(par);
        WriteLog.write("12306Reg", "Reg12306New", logname + ":(" + this.mobileNo + ")【第一次向12306发送注册请求】原始结果->" + result);
        if (result.contains("网络繁忙")) {
            result = "网络繁忙";
        }
        else {
            //            {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":
            //            {"msg":"该证件号码已被注册。请确认是否您本人注册，\"是\"请使用原账号登录，
            //            \"不是\"请持该证件原件到就近的办理客运业务的铁路车站办理抢注，完成后即可继续注册，或致电12306客服协助处理。"}
            //            ,"messages":[],"validateMessages":{}}
            JSONObject jsonObject = JSONObject.parseObject(result);
            try {
                boolean status = jsonObject.getBoolean("status");
                JSONObject dataJSONObject = jsonObject.getJSONObject("data");
                String msgString = dataJSONObject.getString("msg");
                //            result = new String(urlSub.getBytes("UTF-8"), "UTF-8");
                if (msgString == null && status) {
                    result = "true";
                }
                else if (msgString.contains("用户名已经被注册")) {
                    result = "用户名已经被注册";
                }
                else if (msgString.contains("您填写的身份信息重复") || msgString.contains("证件号码输入有误")
                        || msgString.contains("该证件号码已被注册")) {
                    result = "您填写的身份信息重复";
                }
                else if (msgString.contains("该手机号码已被注册") || msgString.contains("您输入的手机号码已被其他注册用户")) {
                    //您输入的手机号码已被其他注册用户（**廷）使用，请确认是否本人注册，"是"请使用原账号登录，"不是"请更换手机号码或致电12306客服协助处理。
                    result = "该手机号码已被注册";
                }
                else if (msgString.contains("系统忙，请稍后重试")) {
                    result = "系统忙，请稍后重试";
                }
                else if (msgString.contains("注册失败！")) {
                    result = "注册失败";
                }
                else if (msgString.contains("验证码") && msgString.contains("次数过多")) {
                    result = "获取验证码次数过多";
                }
                else {
                    System.out.println("Reg12306New:12306:" + logname + ":urlSub:" + result);
                    result = msgString;
                }
            }
            catch (Exception e) {
                System.out.println("Reg12306New:12306:" + logname + ":urlSub:" + result);
                //#TODO
            }
        }
        return result;
    }

    private String get12306Info_getRandCodebyMobile(String par) {
        String urlSub = "https://kyfw.12306.cn/otn/regist/getRandCode";
        String result = "";
        try {
            Map<String, String> hmap = HttpsUtil.Spelling12306Map(false, par.length() + "", true,
                    "https://kyfw.12306.cn/otn/regist/init", this.cookieString);
            //是否用代理
            if ("1".equals(this.useProxy)) {
                result = RequestUtil.submitProxy(urlSub, par, "post", "utf-8", hmap, 5000, proxyHost,
                        Integer.parseInt(proxyPort));
            }
            else {
                for (int i = 0; i < 10; i++) {
                    result = new String(RequestUtil.submit(urlSub, par, "post", "utf-8", hmap, 20000));
                    if (result == null || "".equals(result)) {
                        Thread.sleep(1000L);
                        continue;
                    }
                    else {
                        break;
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 验证用户名是否已注册
     * https://kyfw.12306.cn/otn/regist/checkUserName?user_name=test12306com
     */

    /**
     * 
     * 验证用户名是否已注册
     * 已注册:false,未注册:true
     * @param userName
     * @return
     * @time 2016年3月10日 上午11:34:11
     * @author chendong
     */
    private boolean checkUserName(String userName) {
        String url = "https://kyfw.12306.cn/otn/regist/checkUserName?user_name=" + userName;
        Map<String, String> header = RequestUtil.getRequestHeader(true, false, false);
        header.put(HeaderCookie, this.cookieString);
        header.put(HeaderReferer, "https://kyfw.12306.cn/otn/regist/init");
        String html = "";
        //是否用代理
        if ("1".equals(this.useProxy)) {
            html = RequestUtil.submitProxy(url, "", "post", "utf-8", header, 0, proxyHost, Integer.parseInt(proxyPort));
        }
        else {
            html = RequestUtil.get(url, UTF8, header, 0);
        }
        if (html != null && html.contains("\"data\":true")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 
     * @time 2015年9月25日 下午1:34:39
     * @author chendong
     * @throws NoSuchAlgorithmException 
     * @throws KeyManagementException 
     * @throws IOException 
     */
    public void initCookie() {
        for (int j = 0; j < 3; j++) {
            if (j > 0) {
                sleep(1000L);
            }
            try {
                StringBuffer responseMessage = new StringBuffer();
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
                URL console = new URL(regInit);
                URLConnection con = console.openConnection();
                HttpsURLConnection conn = (HttpsURLConnection) con;
                conn.setSSLSocketFactory(sc.getSocketFactory());
                conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
                conn.setDoOutput(true);
                conn.connect();
                DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                out.write("".getBytes("UTF-8"));
                // 刷新、关闭
                out.flush();
                out.close();
                InputStream is = conn.getInputStream();
                if (is != null) {
                    int charCount = -1;
                    BufferedReader br = null;
                    String ContentEncoding = conn.getHeaderField("Content-Encoding");
                    if ("gzip".equals(ContentEncoding)) {
                        br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
                    }
                    else {
                        br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    }
                    try {
                        while ((charCount = br.read()) != -1) {
                            responseMessage.append((char) charCount);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.initResult = responseMessage.toString();
                String temp_cookie = "";
                List<String> sssss = conn.getHeaderFields().get("Set-Cookie");
                for (int i = 0; i < sssss.size(); i++) {
                    if (i > 0) {
                        temp_cookie += "; ";
                    }
                    String tcoo = sssss.get(i).toString();
                    tcoo = tcoo.split(";")[0];

                    temp_cookie += getCookie(tcoo);
                }
                this.cookieString = temp_cookie + " ;current_captcha_type=Z";
                //        return responseMessage.toString();
                break;
            }
            catch (KeyManagementException e) {
                //                e.printStackTrace();
                System.out.println("Reg12306New_err" + j + e.getLocalizedMessage());
                continue;
            }
            catch (NoSuchAlgorithmException e) {
                System.out.println("Reg12306New_err" + j + e.getLocalizedMessage());
                //                e.printStackTrace();
                continue;
            }
            catch (IOException e) {
                System.out.println("Reg12306New_err" + j + e.getLocalizedMessage());
                //                e.printStackTrace();
                continue;
            }
            catch (Exception e) {
                System.out.println("Reg12306New_err" + j + e.getLocalizedMessage());
                //                e.printStackTrace();
                continue;
            }
        }
    }

    /**
     * 说明：拼接COOKIE
     * @param str
     * @return
     * @time 2014年8月30日 下午2:57:08
     * @author yinshubin
     */
    public static String getCookie(String str) {
        if (str.contains("BIGipServerotn")) {
            String[] strs = str.split("; path=/,");
            strs[0] = strs[0].substring(1, strs[0].length());
            if (strs.length > 1) {
                if (strs[1].contains("current_captcha_type")) {
                    String[] strs2 = strs[1].split("; Path=/,");
                    String[] strs3 = strs2[1].split(";");
                    str = strs3[0] + "; " + strs[0] + "; " + strs2[0];
                }
                else {
                    String[] strs2 = strs[1].split(";");
                    strs2[0] = strs2[0].substring(1, strs2[0].length());
                    str = strs2[0] + "; " + strs[0];
                }
            }
        }
        else if (str.contains("JSESSIONID")) {
            String[] strs = str.split(";");
            str = strs[0].substring(1, strs[0].length());
        }
        return str;
    }

    public String getCookieString(HttpClient httpClient) {
        StringBuffer cookieString = new StringBuffer("");
        Cookie[] cookies = httpClient.getState().getCookies();
        for (int i = 0; i < cookies.length; i++) {
            Cookie cookie1 = cookies[i];
            if (i > 0) {
                cookieString.append(" ");
            }
            cookieString.append(cookie1.getName() + "=" + cookie1.getValue());
            if (i < cookies.length - 1) {
                cookieString.append(";");
            }
        }
        return cookieString.toString();
    }

    /**
     * 收到短信验证码以后请求注册
     * https://kyfw.12306.cn/otn/regist/subDetail
     * @time 2015年9月25日 下午4:59:52
     * @author chendong
     */
    private String subDetail(String name, String idno, String logname, String logpassword, String email) {
        String result = "-1";
        String urlSub = "https://kyfw.12306.cn/otn/regist/subDetail";
        try {
            name = URLEncoder.encode(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String par = "loginUserDTO.user_name="
                + logname
                + "&userDTO.password="
                + logpassword
                + "&confirmPassWord="
                + logpassword
                + "&loginUserDTO.name="
                + name
                + "&loginUserDTO.id_type_code=1&loginUserDTO.id_no="
                + idno
                + "&userDTO.country_code=CN&userDTO.born_date="
                + getbrithday(idno)
                + "&userDTO.email=&userDTO.mobile_no="
                + this.mobileNo
                + "&passenger_type=1&studentInfoDTO.province_code=11&studentInfoDTO.school_code="
                + "&studentInfoDTO.school_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.department="
                + "&studentInfoDTO.school_class=&studentInfoDTO.student_no=&studentInfoDTO.school_system=1"
                + "&studentInfoDTO.enter_year="
                + Calendar.getInstance().get(Calendar.YEAR)
                + "&studentInfoDTO.preference_card_no="
                + "&studentInfoDTO.preference_from_station_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.preference_from_station_code="
                + "&studentInfoDTO.preference_to_station_name=%E7%AE%80%E7%A0%81%2F%E6%B1%89%E5%AD%97&studentInfoDTO.preference_to_station_code=&randCode="
                + this.randCode;
        Map<String, String> hmap = HttpsUtil.Spelling12306Map(false, par.length() + "", true,
                "https://kyfw.12306.cn/otn/regist/init", this.cookieString);
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":subDetail:par:" + par);
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":subDetail:hmap:" + hmap);
        for (int i = 0; i < 5; i++) {

            //是否用代理
            if ("1".equals(this.useProxy)) {
                urlSub = RequestUtil.submitProxy(urlSub, par, "post", "utf-8", hmap, 0, proxyHost,
                        Integer.parseInt(proxyPort));
            }
            else {
                try {
                    urlSub = new String(HttpsUtil.post12306(urlSub, par, "UTF-8", hmap));
                }
                catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":" + i + "次:subDetail:" + urlSub);
            if (urlSub == null || "".equals(urlSub.trim())) {
                try {
                    Thread.sleep(3000L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
            else {
                break;
            }

        }
        WriteLog.write("12306Reg/" + this.loginname, "Reg12306New", logname + ":total:subDetail:" + urlSub);
        JSONObject jsonObject = JSONObject.parseObject(urlSub);
        boolean status = jsonObject.getBoolean("status");
        JSONObject dataJSONObject = jsonObject.getJSONObject("data");
        String msgString = dataJSONObject.getString("msg");
        if (msgString == null && status) {
            result = "true";
        }
        else {
            result = urlSub;
        }
        return result;
    }

    public String getUseProxy() {
        return useProxy;
    }

    public void setUseProxy(String useProxy) {
        this.useProxy = useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getCookieString() {
        return cookieString;
    }

    public void setCookieString(String cookieString) {
        this.cookieString = cookieString;
    }

    public String getInitResult() {
        return initResult;
    }

    public void setInitResult(String initResult) {
        this.initResult = initResult;
    }

    public String getRegInit() {
        return regInit;
    }

    public void setRegInit(String regInit) {
        this.regInit = regInit;
    }

    public CCSHttpClientnew getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(CCSHttpClientnew httpClient) {
        this.httpClient = httpClient;
    }

    public IMobileCode getMobilecode() {
        return mobilecode;
    }

    public void setMobilecode(IMobileCode mobilecode) {
        this.mobilecode = mobilecode;
    }

    public String getMobileCodeType() {
        return mobileCodeType;
    }

    public void setMobileCodeType(String mobileCodeType) {
        this.mobileCodeType = mobileCodeType;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getRandCode() {
        return randCode;
    }

    public void setRandCode(String randCode) {
        this.randCode = randCode;
    }

    public boolean isRandCodeIsHave() {
        return randCodeIsHave;
    }

    public void setRandCodeIsHave(boolean randCodeIsHave) {
        this.randCodeIsHave = randCodeIsHave;
    }

    public String getSmsCheckCode() {
        return smsCheckCode;
    }

    public void setSmsCheckCode(String smsCheckCode) {
        this.smsCheckCode = smsCheckCode;
    }

}
