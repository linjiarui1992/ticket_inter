package com.ccservice.express.cash;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;


public class ByExpressToTaoBaoCash extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ByExpressToTaoBaoCash() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String flag=request.getParameter("flag");
		boolean f1=false;
		boolean f2=false;
		if("0".equals(flag)){//全跑
		    f1=true;
		    f2=true;
		}else if("1".equals(flag)){//跑过去的
		    f1=true;
		}else if("2".equals(flag)){//跑给定日期的
		    f2=true;
        }
		if(f1){
		    //先扫一遍前N天没签收的
		    String sql2="select * from TrainOfflineTaoBaoCash where isReceive!=1 and isFinish=0";
		    List list1=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
		    for(int j=0;j<list1.size();j++){
		        Map map=(Map)list1.get(j);
		        String ordernum=map.get("orderNumber").toString();
		        String ordernumonline=map.get("taoBaoOrderNum").toString();
		        String price=map.get("orderPrice").toString();
		        String express=map.get("expressNum").toString();
		        String dateago=map.get("arriveDate").toString();
		        WriteLog.write("线下票__获取快递路由信息_TB对账_前N天信息", "ordernum:"+ordernum+"ordernumonline:"+ordernumonline+"-->price:"+price+"-->express:"+express+"-->dateago:"+dateago);
		        getExpressInfo(ordernum,ordernumonline,price,express,dateago);
		    }
		}
		if(f2){
		    //扫一遍昨天应该签收的
		    String dateString = request.getParameter("dateStr");
		    String sqlorder="SELECT O.OrderNumber,O.OrderNumberOnline,O.OrderPrice,M.ExpressNum FROM mailaddress M,TrainOrderOffline O where O.Id=M.ORDERID and O.expressDeliver like '%快递预计到达时间:"+dateString+"%' and O.CreateUId=56 and O.OrderStatus=2 order by O.Id";
		    List list = Server.getInstance().getSystemService().findMapResultBySql(sqlorder, null);
		    for(int i=0;i<list.size();i++){
		        Map map=(Map)list.get(i);
		        String ordernum=map.get("OrderNumber").toString();
		        String ordernumonline=map.get("OrderNumberOnline").toString();
		        String price=map.get("OrderPrice").toString();
		        String express=map.get("ExpressNum").toString();
		        getExpressInfo(ordernum,ordernumonline,price,express,dateString);
		    }
		}
	}
	
	public void getExpressInfo(String ordernum,String ordernumonline,String price,String express,String dateString){
        String sb1 = "";
        String urlString="http://localhost:9038/SfExpressHthy/RouteServiceServlet";
        String param="expressno="+express;
        String result=SendPostandGet.submitPost(urlString, param,
                "UTF-8").toString();
        WriteLog.write("线下票__获取快递路由信息_TB对账", "express:"+express+"urlString:"+urlString+"-->param:"+param+"-->result:"+result);
        try {
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head=root.element("Head");
            Element body=root.element("Body");
            Element routeResponse=body.element("RouteResponse");
            String sele1="select * from TrainOfflineTaoBaoCash where orderNumber='"+ordernum+"' and arriveDate='"+dateString+"'";
            List list1 = Server.getInstance().getSystemService().findMapResultBySql(sele1, null);
            if ("OK".equals(root.elementText("Head")) && result.contains("已签收")) {
                if(list1.size()==0){
                	String insert1="insert into TrainOfflineTaoBaoCash(orderNumber,taoBaoOrderNum,orderPrice,expressNum,arriveDate,isReceive,isFinish) values('"+ordernum+"','"+ordernumonline+"','"+price+"','"+express+"','"+dateString+"',1,0)";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(insert1);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "insert1:"+insert1);
                }else{
                	String update1="update TrainOfflineTaoBaoCash set isReceive=1 where orderNumber='"+ordernum+"'";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(update1);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "update1:"+update1);
                }
            }else if("OK".equals(root.elementText("Head")) && routeResponse != null && !result.contains("已签收")){
            	if(list1.size()==0){
                	String insert2="insert into TrainOfflineTaoBaoCash(orderNumber,taoBaoOrderNum,orderPrice,expressNum,arriveDate,isReceive,isFinish) values('"+ordernum+"','"+ordernumonline+"','"+price+"','"+express+"','"+dateString+"',2,0)";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(insert2);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "insert2:"+insert2);
                }else{
                	String update1="update TrainOfflineTaoBaoCash set isReceive=2 where orderNumber='"+ordernum+"'";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(update1);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "update1:"+update1);
                }
            }else{
                if(list1.size()==0){
                	String insert0="insert into TrainOfflineTaoBaoCash(orderNumber,taoBaoOrderNum,orderPrice,expressNum,arriveDate,isReceive,isFinish) values('"+ordernum+"','"+ordernumonline+"','"+price+"','"+express+"','"+dateString+"',0,0)";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(insert0);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "insert2:"+insert0);
                }else{
                	String update1="update TrainOfflineTaoBaoCash set isReceive=0 where orderNumber='"+ordernum+"'";
                	Server.getInstance().getSystemService().excuteAdvertisementBySql(update1);
                	WriteLog.write("线下票__订单信息存库_TB对账sql", "update1:"+update1);
                }
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
	}
	/**
     * 获取前某一天(-i/+i)
     * @param i
     * @return
     * guozhengju 2016-05-05
     * @throws ParseException 
     */
    public static String getNextDay(String dateString, int i) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -i);
        date = calendar.getTime();
        return sdf.format(date);
    }

    public static void main(String[] args) {
    	JSONObject responsejson =new JSONObject();
        responsejson.put("code", "200");
        responsejson.put("taobaoOrderNum", "1857467057079543");
        responsejson.put("msg", "success");
        System.out.println(responsejson.toString());
    }
}
