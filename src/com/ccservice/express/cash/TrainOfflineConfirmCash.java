package com.ccservice.express.cash;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class TrainOfflineConfirmCash extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TrainOfflineConfirmCash() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");
	    String ordernumonline=request.getParameter("taobaoOrderNum");
		String orderPrice=request.getParameter("orderPrice");
		if(orderPrice.contains("?")){
		    orderPrice=orderPrice.replace("?","");
		}
		String sql1="select * from TrainOfflineTaoBaoCash where taoBaoOrderNum='"+ordernumonline+"'";
		List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
		JSONObject responsejson =new JSONObject();
		if(list.size()>0){
			WriteLog.write("淘宝线下订单已经收到票款,修改状态", "ordernumonline="+ordernumonline+"-->orderPrice="+orderPrice);
			String updatesql="update TrainOfflineTaoBaoCash set isReceive=1,isFinish=1,realOrderPrice='"+orderPrice+"' where taoBaoOrderNum='"+ordernumonline+"'";
			WriteLog.write("淘宝线下订单已经收到票款,修改状态", "updatesql="+updatesql);
			Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
			responsejson.put("msg", "success");
		}else{
			WriteLog.write("淘宝线下订单已经收到票款,修改状态订单不存在", "ordernumonline="+ordernumonline+"-->orderPrice="+orderPrice);
			responsejson.put("msg", "fail");
		}
		
		response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        responsejson.put("code", "200");
        responsejson.put("taobaoOrderNum", ordernumonline);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
	}

}
