package com.ccservice.train.mqlistener;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.inter.server.ServerQueueMQ;

/**
 * 消费者 测试 示范类
 * 
 * @time 2015年10月27日 下午4:47:47
 * @author chendong
 */
public class MessageListenerChendongTest implements MessageListener {

    public static void main(String[] args) {
        String QUEUE_NAME = "chendong_test1";
        //        for (int i = 0; i < 100; i++) {
        //            try {
        //                ActiveMQUtil.sendMessage("tcp://192.168.0.5:61616/", QUEUE_NAME, i + "");
        //            }
        //            catch (JMSException e) {
        //                e.printStackTrace();
        //            }
        //        }

        ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.0.5:61616/");
        Connection conn = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(QUEUE_NAME + "?consumer.prefetchSize=1");
            for (int i = 0; i < 4; i++) {
                Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                //                启动一个消费者
                MessageConsumer consumer = session.createConsumer(destination);
                //                把消费者添加到全局list里,每个业务一个list
                ServerQueueMQ.TrainCreateOrderMessageConsumerList.add(consumer);
                consumer.setMessageListener(new MessageListenerChendongTest(i));
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        try {
            MessageListener messageListener0 = ServerQueueMQ.TrainCreateOrderMessageConsumerList.get(0)
                    .getMessageListener();
            System.out.println(messageListener0);
            //关闭第一个消费者
            ServerQueueMQ.TrainCreateOrderMessageConsumerList.get(0).close();
            ServerQueueMQ.TrainCreateOrderMessageConsumerList.remove(0);
            ServerQueueMQ.TrainCreateOrderMessageConsumerList.get(0).close();
            ServerQueueMQ.TrainCreateOrderMessageConsumerList.remove(0);
            ServerQueueMQ.TrainCreateOrderMessageConsumerList.get(0).close();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        System.out.println(ServerQueueMQ.TrainCreateOrderMessageConsumerList);
    }

    int index;

    /**
     * @param index
     */
    public MessageListenerChendongTest(int index) {
        this.index = index;
    }

    @Override
    public void onMessage(Message message) {
        Long l1 = System.currentTimeMillis();
        try {
            String messageString = ((TextMessage) message).getText();
            System.out.println(l1 + ":" + this.index + "-->" + messageString);
            try {
                Thread.sleep(1000L);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println(l1 + ":0");
            try {
                Thread.sleep(1000L);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println(l1 + ":1");
            try {
                Thread.sleep(1000L);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println(l1 + ":2");
            try {
                Thread.sleep(1000L);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println(l1 + ":3");
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化N个消费者
     * 
     * @param CosumerCount 多少个消费者
     * @param brokerURL "tcp://192.168.0.5:61616/"
     * @param name "chendong_test1?consumer.prefetchSize=1"
     * @time 2015年10月17日 下午2:52:33
     * @author chendong
     */
    public static int initMessageConsumer(int CosumerCount, String brokerURL, String name) {
        ConnectionFactory cf = new ActiveMQConnectionFactory(brokerURL);
        Connection conn = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(name);
            for (int i = 0; i < CosumerCount; i++) {
                Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                ServerQueueMQ.MessageListenerChendongTestList.add(consumer);
                consumer.setMessageListener(new MessageListenerChendongTest(i));
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        return ServerQueueMQ.MessageListenerChendongTestList.size();
    }
}
