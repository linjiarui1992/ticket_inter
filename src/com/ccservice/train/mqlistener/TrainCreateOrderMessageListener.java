package com.ccservice.train.mqlistener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.ActiveMQUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.TrainCreateOrder;

/**
 * 下单消费者
 * @author wzc
 *
 */
public class TrainCreateOrderMessageListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    private boolean isUse = false;//是否使用

    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean isUse) {
        this.isUse = isUse;
    }

    //    @Override
    public void onMessage(Message message) {
        isUse = true;
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("开始自动下单", System.currentTimeMillis() + ":orderNoticeResult:" + orderNoticeResult);
            if (isCanOrdering(new Date())) {
                try {
                    this.orderid = Long.valueOf(orderNoticeResult);
                }
                catch (NumberFormatException e) {
                    //同程3151
                    try {
                        //JSON格式
                        JSONObject json = JSONObject.parseObject(orderNoticeResult);
                        //JSON数据正确
                        if (json != null && json.containsKey("orderId")) {
                            //订单ID
                            orderid = json.getLongValue("orderId");
                            //ID正确
                            if (orderid > 0) {
                                //JSON转换
                                Customeruser user = json.getObject("customeruser", Customeruser.class);
                                //开始下单
                                new TrainCreateOrder(orderid).createOrderStart(user, true);
                            }
                            //中断返回
                            return;
                        }
                    }
                    catch (Exception E) {
                        this.orderid = 0;
                    }
                }
                int t = new Random().nextInt(10000);
                if (this.orderid > 0) {
                    new TrainCreateOrder(this.orderid).createOrderStart(new Customeruser(), false);
                }
                WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", t + ":" + orderNoticeResult);
            }
            else {
                try {
                    Thread.sleep(7000L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String url = getSysconfigString("activeMQ_url");
                String QUEUE_NAME = getSysconfigString("QueueMQ_trainorder_waitorder_orderid");
                try {
                    ActiveMQUtil.sendMessage(url, QUEUE_NAME, orderNoticeResult);
                    WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单");
                }
                catch (Exception e) {
                    WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单异常");
                    try {
                        ActiveMQUtil.sendMessage(url, QUEUE_NAME, orderNoticeResult);
                        WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单");
                    }
                    catch (Exception e1) {
                        WriteLog.write("12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单异常");
                    }
                }
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        isUse = false;
    }

    /**
     * 是否可以下单 06:00:00-07:01:30不能下单
     * @param date
     * @return
     * @time 2015年4月13日 下午9:58:34
     * @author fiend
     */
    private boolean isCanOrdering(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("05:00:00");
            Date dateAfter = df.parse("06:01:30");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                WriteLog.write("开始自动下单_error", "当前时间不能下单");
                return false;
            }
        }
        catch (ParseException e) {
            WriteLog.write("开始自动下单_Exception", "获取时间出错");
        }
        return true;
    }
}
