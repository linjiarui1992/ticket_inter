package com.ccservice.train.mqlistener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;
import com.ccservice.inter.server.Server;

public class TrainOrderCancelGetMQMSGListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        long l1 = System.currentTimeMillis();
        TrainCreateOrderSupplyMethod TrainCreateOrderSupplyMethod = new TrainCreateOrderSupplyMethod();
        WriteLog.write("取消订单MQ", "进入消费者取消~~");
        Customeruser use = new Customeruser();
        boolean cancelIsTrue = false;
        try {
            String orderNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("取消订单MQ", l1 + ":" + "MQ传送值：" + orderNoticeResult);
            JSONObject jsonObject = new JSONObject();
            jsonObject = JSONObject.parseObject(orderNoticeResult);
            String extnumber = jsonObject.containsKey("extnumber") ? jsonObject.getString("extnumber") : "";
            String trainorderid = jsonObject.containsKey("trainorderid") ? jsonObject.getString("trainorderid") : "0";
            //            String freeAccount = jsonObject.containsKey("freeAccount") ? jsonObject.getString("freeAccount") : "false";
            long orderid = Long.parseLong(trainorderid);
            Trainorder order = Server.getInstance().getTrainService().findTrainorder(orderid);
            String result = "";
            WriteLog.write("取消订单MQ", l1 + ":" + "MQ订单号：" + trainorderid + ",查询订单号：" + order.getId());
            use = TrainCreateOrderSupplyMethod.getCustomerUserBy12306Account(order, false);//根据订单获取账号
            WriteLog.write("取消订单MQ", l1 + ":" + "查询订单号：" + order.getSupplyaccount() + "账号获取：" + use.getLoginname());
            int i = 0;
            do {
                WriteLog.write("取消订单MQ", l1 + ":" + "取消订单参数：票号:" + extnumber + ",订单号:" + orderid);
                result = new TrainCreateOrderSupplyMethod().cancel12306Order(use, extnumber, orderid, false);
                WriteLog.write("取消订单MQ", l1 + ":" + "取消订单结果:" + result);
                if (result.contains("取消订单成功") || "无未支付订单".equals(result)) {
                    cancelIsTrue = true;
                }
                i++;
                WriteLog.write("取消订单MQ", l1 + ":" + trainorderid + "取消失败，消费者取消循环次数：" + i + "--->取消,返回:" + result);
            }
            while (!cancelIsTrue && i < 5);
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
        //释放账号
        TrainCreateOrderSupplyMethod.freeCustomeruser(use,
                cancelIsTrue ? AccountSystem.FreeNoCare : AccountSystem.FreeCurrent, AccountSystem.OneFree,
                AccountSystem.OneCancel, AccountSystem.NullDepartTime);
    }
}
