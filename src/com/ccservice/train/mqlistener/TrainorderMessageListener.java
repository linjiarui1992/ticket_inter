package com.ccservice.train.mqlistener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.train.JobQueryMyOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;

public class TrainorderMessageListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    private long successtime;

    //    private long querySpaceTime = 20 * 1000;

    private int queryno;

    @Override
    public void onMessage(Message message) {
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            long intotime = jsonobject.containsKey("intotime") ? jsonobject.getLongValue("intotime") : 0;
            this.successtime = jsonobject.containsKey("successtime") ? jsonobject.getLongValue("successtime") : 0;
            this.queryno = jsonobject.containsKey("queryno") ? jsonobject.getIntValue("queryno") : 0;
            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            if (MQMethod.QUERY == type) {
                //系统配置的审核间隔时间
                //                long sysquerytime = Long.valueOf(getSysconfigString("querySpaceTime"));
                //                if (sysquerytime > 0) {
                //                    this.querySpaceTime = 1000 * sysquerytime;
                //                }
                //                if (System.currentTimeMillis() - intotime > this.querySpaceTime) {
                new JobQueryMyOrder().gotoQuery(this.orderid, this.successtime, this.queryno);
                //                }
                //                else {
                //                    new TrainActiveMQ(MQMethod.QUERY, this.orderid, this.successtime, intotime, this.queryno)
                //                            .sendqueryMQ();
                //                }
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
