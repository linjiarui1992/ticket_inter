package com.ccservice.train.mqlistener;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.rebaterecord.Rebaterecord;
import com.ccservice.b2b2c.base.train.TrainStudentInfo;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.ben.Payresult;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.AppBusinessType;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.account.CookieLogic;
import com.ccservice.train.mqlistener.Method.OcsMethod;
import com.weixin.util.RequestUtil;

/**
 * 改签审核
 * @author 彩娜
 * @time 2016年1月15日 下午7:15:52
 * @version 1.0
 */

public class TrainorderChangePayMessageListener extends TrainSupplyMethod implements MessageListener {

    private long changeId;

    private String changeNoticeResult;

    //审核等待，单位：毫秒
    private static final long checkWait = 3 * 1000;

    //审核每次超时，单位：毫秒
    private static final int checkTimeout = 30 * 1000;

    //审核总时间，单位：毫秒
    private static final long checkTotal = 3 * 60 * 1000;

    public void onMessage(Message message) {
        try {
            changeNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("开始改签支付审核", System.currentTimeMillis() + ":changeNoticeResult:" + changeNoticeResult);
            try {
                changeId = Long.parseLong(changeNoticeResult);
            }
            catch (Exception e) {
                changeId = 0;
            }
            //支付审核操作
            if (changeId > 0) {
                RepOperate();
            }
        }
        catch (Exception e) {
        }
    }

    public void RepOperate() {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            WriteLog.write("重复进入改签支付审核队列", random + "-->改签ID-->" + changeId + "-->" + isAsync);
            return;
        }
        //订单ID
        long orderId = trainOrderChange.getOrderid();
        int tcstatus = trainOrderChange.getTcstatus();
        float changePrice = trainOrderChange.getTcprice();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        int oldStatus = trainOrderChange.getTcpaystatus();//改签审核状态>>0:默认；1:审核中；2:审核成功；3:已扣款；4:已退款
        //非默认
        if (oldStatus != 0 || isQuestionChange != 0 || changePrice <= 0
                || (tcstatus != Trainorderchange.CHANGEPAYING && tcstatus != Trainorderchange.CHANGEWAITPAY)) {
            WriteLog.write("重复进入改签支付审核队列", random + "-->改签ID-->" + changeId);
            return;
        }
        else {
            WriteLog.write("t同程火车票接口_4.14确认改签审核", random + "-->" + orderId + "-->" + changeId + "-->开始审核");
        }
        //根据订单号获取订单信息
        Trainorder order = Server.getInstance().getTrainService().findTrainorder(orderId);
        //确认改签结果
        JSONObject resultjson = checkOrder(order, trainOrderChange, random);
        //记录日志
        WriteLog.write("t同程火车票接口_4.14确认改签审核", random + "-->" + orderId + "-->" + changeId + "-->审核结果-->" + resultjson);
        //回调正确的
        callBackTrue(resultjson, order, trainOrderChange, random);
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月18日 下午2:14:16
     * @Description 在原方法上加入参数
     * @param changeId
     */
    public void RepOperateV2(long changeIdd) {
        int random = new Random().nextInt(1000000);
        //赋值
        changeId = changeIdd;
        //改签信息
        Trainorderchange trainOrderChange = findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            WriteLog.write("重复进入改签支付审核队列", random + "-->改签ID-->" + changeId + "-->" + isAsync);
            return;
        }
        //订单ID
        long orderId = trainOrderChange.getOrderid();
        int tcstatus = trainOrderChange.getTcstatus();
        float changePrice = trainOrderChange.getTcprice();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        int oldStatus = trainOrderChange.getTcpaystatus();//改签审核状态>>0:默认；1:审核中；2:审核成功；3:已扣款；4:已退款
        //非默认
        if (oldStatus != 0 || isQuestionChange != 0 || changePrice <= 0
                || (tcstatus != Trainorderchange.CHANGEPAYING && tcstatus != Trainorderchange.CHANGEWAITPAY)) {
            WriteLog.write("重复进入改签支付审核队列", random + "-->改签ID-->" + changeId);
            return;
        }
        else {
            WriteLog.write("t同程火车票接口_4.14确认改签审核Rabbit", random + "-->" + orderId + "-->" + changeId + "-->开始审核");
        }
        //根据订单号获取订单信息
        Trainorder order = findTrainorder(orderId);
        //确认改签结果
        JSONObject resultjson = checkOrder(order, trainOrderChange, random);
        //记录日志
        WriteLog.write("t同程火车票接口_4.14确认改签审核Rabbit", random + "-->" + orderId + "-->" + changeId + "-->审核结果-->"
                + resultjson);
        //回调正确的
        callBackTrue(resultjson, order, trainOrderChange, random);
    }

    /**
     * @describe 查找订单信息
     */
    public static Trainorder findTrainorder(long orderid) {
        //根据sql 查找唯一的订单信息
        String sql = "[sp_TRAINORDER_selectTrainOrderByOrderId]  @orderId=" + orderid;
        Trainorder trainorder = null;
        try {
            DataTable table = DBHelper.GetDataTable(sql, dbSourceEnum);
            if (table != null) {
                List<DataRow> orderMessages = table.GetRow();
                if (orderMessages != null && !orderMessages.isEmpty()) {
                    TrainCreateOrderDBMethod trainCreateOrderDBMethod = new TrainCreateOrderDBMethod();
                    List<Trainpassenger> trainpassengers = new ArrayList<>();
                    for (DataRow row : orderMessages) {
                        if (trainorder == null) {
                            //**********************************填充 订单信息 开始****************************************************//
                            trainorder = new Trainorder();
                            //订单id
                            trainCreateOrderDBMethod.paddingOrder(trainorder, row);
                            //**********************************填充 订单信息 结束****************************************************//
                        }
                        //**********************************填充 乘客信息 结束****************************************************//
                        if (trainCreateOrderDBMethod.isNotExistTrainPassengers(row, trainpassengers)) {
                            Trainpassenger trainpassenger = new Trainpassenger();
                            //乘客中的订单id
                            trainCreateOrderDBMethod.paddingPassenger(row, trainpassenger);
                            //**********************************填充 乘客信息 结束****************************************************//

                            //**********************************填充 学生信息 开始****************************************************//
                            if (row.GetColumn("SIID").GetValue() != null) {
                                List<TrainStudentInfo> trainStudentInfos = new ArrayList<TrainStudentInfo>();
                                if (trainCreateOrderDBMethod.isNotExistTrainStudents(row, trainStudentInfos)) {
                                    TrainStudentInfo trainStudentInfo = new TrainStudentInfo();
                                    trainCreateOrderDBMethod.paddingStudent(row, trainStudentInfo);
                                    trainStudentInfos.add(trainStudentInfo);
                                }
                                trainpassenger.setTrainstudentinfos(trainStudentInfos);
                            }
                            //**********************************填充 学生信息 结束****************************************************//

                            //**********************************填充 车票信息 开始****************************************************//
                            List<Trainticket> traintickets = new ArrayList<Trainticket>();
                            if (trainCreateOrderDBMethod.isNotExistTrainTickets(row, traintickets)) {
                                Trainticket trainticket = new Trainticket();
                                trainCreateOrderDBMethod.paddingTicket(row, traintickets, trainticket);
                                traintickets.add(trainticket);
                            }
                            trainpassenger.setTraintickets(traintickets);
                            trainpassengers.add(trainpassenger);
                            //**********************************填充 车票信息 结束****************************************************//
                        }
                        trainorder.setPassengers(trainpassengers);
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainorderChangePayMessageListener_Exception", e, orderid + "-->" + sql);
        }
        return trainorder;
    }

    /**
     * TODO
     * 使用哪个数据库
     */
    private static DBSourceEnum dbSourceEnum = DBSourceEnum.TONGCHENG_DB;

    /**
     * 查找改签订单通过改签票ID
     * @param changeId2
     * @return
     */
    private Trainorderchange findTrainOrderChangeById(long changeId) {
        String sql = "[sp_Trainorderchange_findTrainOrderChangeById]  @ChangeId=" + changeId;
        Trainorderchange trainorderchange = new Trainorderchange();
        DataTable resultset = null;
        try {
            resultset = DBHelper.GetDataTable(sql, dbSourceEnum);
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainorderChangePayMessageListener_err", e,
                    "findTrainOrderChangeById>>>>>sql=[" + sql + "]>>>错误");
        }
        List<DataRow> getRow = resultset.GetRow();
        for (DataRow dataRow : getRow) {
            List<DataColumn> getColumn = dataRow.GetColumn();
            for (DataColumn dataColumn : getColumn) {
                String key = dataColumn.GetKey();
                Object getValue = dataColumn.GetValue();

                if ("ID".equals(key) && getValue != null) {
                    trainorderchange.setId(Long.parseLong(getValue.toString()));
                }
                if ("C_ORDERID".equals(key) && getValue != null) {
                    trainorderchange.setOrderid(Long.parseLong(getValue.toString()));
                }
                if ("C_TCNUMBER".equals(key) && getValue != null) {
                    trainorderchange.setTcnumber(getValue.toString());
                }
                if ("C_TCTRAINNO".equals(key) && getValue != null) {
                    trainorderchange.setTctrainno(getValue.toString());
                }
                if ("C_TCTICKETTYPE".equals(key) && getValue != null) {
                    trainorderchange.setTctickettype(Integer.parseInt(getValue.toString()));
                }
                if ("C_TCSEATTYPE".equals(key) && getValue != null) {
                    trainorderchange.setTcseattype(getValue.toString());
                }
                if ("C_TCPRICE".equals(key) && getValue != null) {
                    trainorderchange.setTcprice(Float.parseFloat(getValue.toString()));
                }
                if ("C_TCDEPARTTIME".equals(key) && getValue != null) {
                    trainorderchange.setTcdeparttime(getValue.toString());
                }
                if ("C_TCSTATUS".equals(key) && getValue != null) {
                    trainorderchange.setTcstatus(Integer.parseInt(getValue.toString()));
                }
                if ("C_TCPROCEDURE".equals(key) && getValue != null) {
                    trainorderchange.setTcprocedure(Float.parseFloat(getValue.toString()));
                }
                if ("C_TCMEMO".equals(key) && getValue != null) {
                    trainorderchange.setTcmemo(getValue.toString());
                }
                if ("C_TCCREATETIME".equals(key) && getValue != null) {
                    trainorderchange.setTccreatetime(getValue.toString());
                }
                if ("C_TCPAYSTATUS".equals(key) && getValue != null) {
                    trainorderchange.setTcpaystatus(Integer.parseInt(getValue.toString()));
                }
                if ("C_TCISCHANGEREFUND".equals(key) && getValue != null) {
                    trainorderchange.setTcischangerefund(Integer.parseInt(getValue.toString()));
                }
                if ("C_TCORIGINALPRICE".equals(key) && getValue != null) {
                    trainorderchange.setTcoriginalprice(Float.parseFloat(getValue.toString()));
                }
                if ("C_TCISLOWCHANGE".equals(key) && getValue != null) {
                    trainorderchange.setTcislowchange(Integer.parseInt(getValue.toString()));
                }
                if ("C_PAYADDRESS".equals(key) && getValue != null) {
                    trainorderchange.setPayaddress(getValue.toString());
                }
                if ("C_PAYACCOUNT".equals(key) && getValue != null) {
                    trainorderchange.setPayaccount(getValue.toString());
                }
                if ("C_SUPPLYTRADENO".equals(key) && getValue != null) {
                    trainorderchange.setSupplytradeno(getValue.toString());
                }
                if ("C_SUPPLYPAYMETHOD".equals(key) && getValue != null) {
                    trainorderchange.setSupplypaymethod(Integer.parseInt(getValue.toString()));
                }
                if ("C_SUPPLYPRICE".equals(key) && getValue != null) {
                    trainorderchange.setSupplyprice(Float.parseFloat(getValue.toString()));
                }
                if ("C_PAYFLAG".equals(key) && getValue != null) {
                    trainorderchange.setPayflag(getValue.toString());
                }
                if ("C_OPERATEUID".equals(key) && getValue != null) {
                    trainorderchange.setOperateUid(Long.parseLong(getValue.toString()));
                }
                if ("C_OPERATEUSER".equals(key) && getValue != null) {
                    trainorderchange.setOperateUser(getValue.toString());
                }
                if ("C_ISQUESTIONCHANGE".equals(key) && getValue != null) {
                    trainorderchange.setIsQuestionChange(Integer.parseInt(getValue.toString()));
                }
                if ("C_ISPLACEING".equals(key) && getValue != null) {
                    trainorderchange.setIsPlaceing(Integer.parseInt(getValue.toString()));
                }
                if ("C_STATUS12306".equals(key) && getValue != null) {
                    trainorderchange.setStatus12306(Integer.parseInt(getValue.toString()));
                }
                if ("C_STATION_NAME".equals(key) && getValue != null) {
                    trainorderchange.setStationName(getValue.toString());
                }
                if ("C_PASSENGER_NAME".equals(key) && getValue != null) {
                    trainorderchange.setPassengerName(getValue.toString());
                }
                if ("C_TCTICKETID".equals(key) && getValue != null) {
                    trainorderchange.setTcTicketId(getValue.toString());
                }
                if ("C_CHANGEPROCEDURE".equals(key) && getValue != null) {
                    trainorderchange.setChangeProcedure(Float.parseFloat(getValue.toString()));
                }
                if ("C_CHANGERATE".equals(key) && getValue != null) {
                    trainorderchange.setChangeRate(Float.parseFloat(getValue.toString()));
                }
                if ("C_REQUESTISASYNC".equals(key) && getValue != null) {
                    trainorderchange.setRequestIsAsync(Integer.parseInt(getValue.toString()));
                }
                if ("C_REQUESTCALLBACKURL".equals(key) && getValue != null) {
                    trainorderchange.setRequestCallBackUrl(getValue.toString());
                }
                if ("C_REQUESTREQTOKEN".equals(key) && getValue != null) {
                    trainorderchange.setRequestReqtoken(getValue.toString());
                }
                if ("C_CONFIRMISASYNC".equals(key) && getValue != null) {
                    trainorderchange.setConfirmIsAsync(Integer.parseInt(getValue.toString()));
                }
                if ("C_CONFIRMCALLBACKURL".equals(key) && getValue != null) {
                    trainorderchange.setConfirmCallBackUrl(getValue.toString());
                }
                if ("C_CONFIRMREQTOKEN".equals(key) && getValue != null) {
                    trainorderchange.setConfirmReqtoken(getValue.toString());
                }
                if ("C_FROMSTATIONCODE".equals(key) && getValue != null) {
                    trainorderchange.setFromStationCode(getValue.toString());
                }
                if ("C_FROMSTATIONNAME".equals(key) && getValue != null) {
                    trainorderchange.setFromStationName(getValue.toString());
                }
                if ("C_TOSTATIONCODE".equals(key) && getValue != null) {
                    trainorderchange.setToStationCode(getValue.toString());
                }
                if ("C_TOSTATIONNAME".equals(key) && getValue != null) {
                    trainorderchange.setToStationName(getValue.toString());
                }
                if ("C_SEATTYPECODE".equals(key) && getValue != null) {
                    trainorderchange.setSeatTypeCode(getValue.toString());
                }
                if ("C_TAOBAOAPPLYID".equals(key) && getValue != null) {
                    trainorderchange.setTaobaoapplyid(getValue.toString());
                }
                if ("C_ISCANREFUNDONLINE".equals(key) && getValue != null) {
                    trainorderchange.setIscanrefundonline(Integer.parseInt(getValue.toString()));
                }
                if ("C_CHANGETIMEOUT".equals(key) && getValue != null) {
                    trainorderchange.setChangetimeout((Timestamp) getValue);
                }
                if ("C_ORDERAGENTID".equals(key) && getValue != null) {
                    trainorderchange.setOrderAgentId(Integer.parseInt(getValue.toString()));
                }
                if ("C_ARRIVETIME".equals(key) && getValue != null) {
                    trainorderchange.setArriveTime(getValue.toString());
                }
                if ("Wicket".equals(key) && getValue != null) {
                    trainorderchange.setWicket(getValue.toString());
                }
            }
        }
        return trainorderchange;
    }

    /**
     * 确认改签审核
     */
    public JSONObject checkOrder(Trainorder order, Trainorderchange trainOrderChange, int random) {
        //变更到站
        boolean isTs = trainOrderChange.getChangeArrivalFlag() == 1;
        //改签退
        long changeType = trainOrderChange.getTcischangerefund();
        //改签数据
        String tcTicketNo = "";
        String passenger_name = "";
        out: for (Trainpassenger passenger : order.getPassengers()) {
            //车票
            List<Trainticket> traintickets = passenger.getTraintickets();
            //改签车票
            for (Trainticket trainTicket : traintickets) {
                if (trainTicket.getChangeid() == changeId) {
                    passenger_name = passenger.getName();
                    tcTicketNo = trainTicket.getTcticketno();
                    break out;
                }
            }
        }
        //日志内容
        String changeFlag = isTs ? "变更到站" : "改签";
        //更新审核中成功，走支付审核
        if (updateCheckStatus(changeId, 1, 0, -1) == 1) {
            //日志内容
            String content = "[确认 - " + changeId + "]" + changeFlag + "订单开始审核";
            createtrainorderrc(1, content, order.getId(), changeId, 0, changeType == 1 ? "自动改签退" : "自动审核");//记录日志
            //12306审核支付成功、更新改签状态成功
            if (check12306pay(order, trainOrderChange, changeId, tcTicketNo, passenger_name, isTs, random)
                    && updateCheckStatus(changeId, 2, 1, 0) == 1) {
                return paySuccess(order, trainOrderChange, random);
            }
            //12306审核支付失败
            else {
                //更新状态
                int flag = updateCheckStatus(changeId, 0, 1, 2);
                //直接返回
                JSONObject result = new JSONObject();
                result.put("success", false);
                result.put("msg", "审核未支付、异常或状态变化>>" + flag);
                return result;
            }
        }
        //更新数据失败，不直接回调，仅用于记录日志
        else {
            //支付问题
            int flag = updateCheckStatus(changeId, 0, 0, 2);
            //直接返回
            JSONObject result = new JSONObject();
            result.put("success", false);
            result.put("msg", "更新改签为审核中失败或状态变化>>" + flag);
            return result;
        }
    }

    /**
     * 审核成功
     */
    private JSONObject paySuccess(Trainorder order, Trainorderchange trainOrderChange, int random) {
        //OCS
        deleteMemCached(order, trainOrderChange);
        //改签退
        int changeType = trainOrderChange.getTcischangerefund();
        //订单标识
        String changeFlag = trainOrderChange.getChangeArrivalFlag() == 1 ? "变更到站" : "改签";
        //记录日志
        String content = "[确认 - " + changeId + "]支付" + changeFlag + "订单成功，已审核";
        createtrainorderrc(1, content, order.getId(), changeId, 0, changeType == 1 ? "自动改签退" : "支付审核");
        //更新改签
        String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + Trainorderchange.FINISHCHANGE
                + ", C_STATUS12306 = " + Trainorderchange.ORDEREDPAYED + " where ID = " + changeId;
        //更新数据
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        //更新车票
        JSONArray ticketmsg = updateTicket(order, trainOrderChange, random);
        //返回结果
        JSONObject obj = new JSONObject();
        obj.put("code", "100");
        obj.put("success", true);
        obj.put("msg", "确认改签成功");
        obj.put("oldticketchangeserial", "");
        obj.put("newticketchangeserial", "");
        obj.put("newticketcxins", ticketmsg);
        obj.put("agentId", order.getAgentid());
        obj.put("ticketpricediffchangeserial", "");
        obj.put("orderid", order.getQunarOrdernumber());
        //返回结果
        return obj;
    }

    /**
     * 更新车票
     */
    private JSONArray updateTicket(Trainorder order, Trainorderchange trainOrderChange, int random) {
        JSONArray ticketmsg = new JSONArray();
        //车票ID
        String ids = "";
        //订单乘客
        List<Trainpassenger> passengers = order.getPassengers();
        //循环乘客
        for (Trainpassenger passenger : passengers) {
            //车票
            List<Trainticket> traintickets = passenger.getTraintickets();
            //循环
            for (Trainticket changeTicket : traintickets) {
                //改签车票
                if (changeTicket.getChangeid() == changeId) {
                    //同程返回
                    JSONObject ticket = new JSONObject();
                    ticket.put("old_ticket_no", changeTicket.getTicketno());
                    ticket.put("new_ticket_no", changeTicket.getTcticketno());
                    //乘客ID
                    if (!ElongHotelInterfaceUtil.StringIsNull(changeTicket.getTcPassengerId())) {
                        ticket.put("passengerid", changeTicket.getTcPassengerId());
                    }
                    ticket.put("cxin", changeTicket.getTccoach() + "车厢," + changeTicket.getTcseatno());
                    //车票信息
                    ticketmsg.add(ticket);
                    //车票ID
                    ids += changeTicket.getId() + ",";
                    //中断当前
                    break;
                }
            }
        }
        if (ids.endsWith(",")) {
            //状态
            int status = Trainticket.FINISHCHANGE;
            int status12306 = Trainticket.CHANGEDPAYED;
            //车票ID
            ids = ids.substring(0, ids.length() - 1);
            //SQL
            String updateSql = "";
            //改签退>>C_CHANGETYPE>>改签类型 1:线上改签、2:改签退
            if (trainOrderChange.getTcischangerefund() == 1) {
                updateSql = "update T_TRAINTICKET set C_CHANGETYPE = 2, C_STATE12306 = " + status12306
                        + " where ID in (" + ids + ")";
            }
            else {
                updateSql = "update T_TRAINTICKET set C_CHANGETYPE = 1, C_TCNEWPRICE = C_TCPRICE, C_STATUS = " + status
                        + ", C_STATE12306 = " + status12306 + " where ID in (" + ids + ") and C_STATUS >= 12";
            }
            //更新车票
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        }
        return ticketmsg;
    }

    /**
     * 判断走手机端
     * @author WH
     * @time 2017年7月24日 下午4:09:29
     * @version 1.0
     */
    private boolean goApp(Trainorder order, boolean oldGoApp) {
        return goApp(order, AppBusinessType.CHANGE_CHECK, oldGoApp);
    }

    /**
     * 支付审核
     */
    private boolean check12306pay(Trainorder order, Trainorderchange trainOrderChange, long changeId,
            String tcTicketNo, String passenger_name, boolean isTs, int random) {
        //账号次数
        int getCount = 0;
        int freeCount = 0;
        //走手机端
        boolean goApp = false;
        //支付成功
        boolean isTrue = false;
        //是否刷新
        boolean refreshCookie = false;
        //不能重试
        boolean dontRetryLogin = false;
        //检票口用
        int requestWebSiteWicket = 0;//web端，次数
        //下单账号
        Customeruser user = new Customeruser();
        //开始时间
        long startTime = System.currentTimeMillis();
        //循环处理
        while (true) {
            try {
                //走手机端
                goApp = goApp(order, goApp);
                //取Cookie
                if (refreshCookie || ElongHotelInterfaceUtil.StringIsNull(user.getCardnunber())) {
                    getCount++;
                    user = getCustomerUserBy12306Account(order, refreshCookie, goApp);
                    user = user == null ? new Customeruser() : user;//重置账号，防止空
                }
                //不能重试
                if (user.isDontRetryLogin()) {
                    dontRetryLogin = true;
                    break;
                }
                //为空
                if (ElongHotelInterfaceUtil.StringIsNull(user.getCardnunber())) {
                    freeCount++;
                    FreeNoCare(user);
                    continue;
                }
                //REP
                RepServerBean rep = RepServerUtil.getRepServer(user, false);
                //地址
                String url = rep.getUrl();
                //为空
                if (ElongHotelInterfaceUtil.StringIsNull(url)) {
                    continue;
                }
                //WEB检票口
                if (!goApp && requestWebSiteWicket == 0) {
                    //次数+1
                    requestWebSiteWicket++;
                    //请求REP
                    webSiteWicket(trainOrderChange, user, rep);
                }
                //参数
                JSONObject params = new JSONObject();
                //票号
                JSONArray ticket_no = new JSONArray();
                ticket_no.add(tcTicketNo);
                params.put("ticket_no", ticket_no);
                //其他参数
                params.put("cookie", user.getCardnunber());
                params.put("passenger_name", passenger_name);
                params.put("sequence_no", order.getExtnumber());
                params.put("order_date", order.getCreatetime().toString().split(" ")[0]);
                //REP标识
                String datatypeflag = goApp ? "refundCheckByApp" : "105";
                //请求参数URLEncoder
                String jsonStr = URLEncoder.encode(params.toString(), "UTF-8");
                //请求参数
                String param = "datatypeflag=" + datatypeflag + "&jsonStr=" + jsonStr
                        + JoinCommonAccountInfo(user, rep);
                //请求REP
                String retdata = RequestUtil.post(url, param, "UTF-8", new HashMap<String, String>(), checkTimeout);
                //用户未登录
                if (retdata.contains("用户未登录") && RepServerUtil.changeRepServer(user)) {
                    //切换REP
                    rep = RepServerUtil.getTaoBaoTuoGuanRepServer(user, false);
                    //类型正确
                    if (rep.getType() == 1) {
                        //地址
                        url = rep.getUrl();
                        //重拼参数
                        param = "datatypeflag=" + datatypeflag + "&jsonStr=" + jsonStr
                                + JoinCommonAccountInfo(user, rep);
                        //重新请求
                        retdata = RequestUtil.post(url, param, "UTF-8", new HashMap<String, String>(), checkTimeout);
                    }
                }
                //日志
                WriteLog.write("t同程火车票接口_4.14确认改签审核", random + "--->" + goApp + "-->" + order.getId() + "-->"
                        + changeId + "-->" + url + "-->" + retdata);
                //刷新Cookie
                refreshCookie = Account12306Util.accountNoLogin(retdata, user);
                //刷新cookie
                try {
                    CookieLogic.getInstance().refresh(retdata, user);//返回结果，登录名
                }
                catch (Exception e) {
                    WriteLog.write("t同程火车票接口_4.14确认改签审核_newCookie_Error",
                            order.getId() + ">>>>>cookie刷新异常：" + e.getMessage());
                }
                //未登录释放
                if (refreshCookie) {
                    freeCount++;
                    FreeNoLogin(user);
                }
                //返回数据
                else if (retdata.contains("refunds")) {
                    //解析数据
                    JSONArray refunds = JSONObject.parseObject(retdata).getJSONArray("refunds");
                    //查询到车票
                    if (refunds != null && refunds.size() == 1) {
                        JSONObject refund = refunds.getJSONObject(0);
                        if (tcTicketNo.equals(refund.getString("ticket_no"))) {
                            //状态名称
                            String ticket_status_name = refund.getString("ticket_status_name");
                            //已出票
                            if ("已出票".equals(ticket_status_name)) {
                                isTrue = true;
                            }
                            //已退票
                            else if (ticket_status_name.contains("已退票")) {
                                isTrue = true;
                            }
                            //改签成功
                            else if ("改签票".equals(ticket_status_name) || "变更到站票".equals(ticket_status_name)) {
                                isTrue = true;
                            }
                            //app检票口信息
                            if (goApp && isTrue) {
                                //检票口4(二层)、候车地点：二楼候车
                                String flat_msg = refund.getString("flat_msg");
                                //信息非空
                                if (!ElongHotelInterfaceUtil.StringIsNull(flat_msg) && flat_msg.startsWith("检票口")) {
                                    //保存数据
                                    saveWicketInfo(trainOrderChange, flat_msg);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) {

            }
            finally {
                //成功或超时
                if (isTrue || dontRetryLogin || System.currentTimeMillis() - startTime >= checkTotal) {
                    break;
                }
                //等待一会重试
                else {
                    try {
                        Thread.sleep(checkWait);
                    }
                    catch (Exception e) {
                        System.out.println("改签审核，等待异常>>" + ElongHotelInterfaceUtil.errormsg(e));
                    }
                }
            }
        }
        //审核成功
        if (isTrue) {
            getCount++;//占座时+1
        }
        //释放账号
        if (getCount > freeCount) {
            //释放次数
            freeCount = getCount - freeCount;
            //改签占座成功已释放REP
            //REP信息置空，不同时释放
            //审核成功、当前取和释放次数一样(isTrue时，getCount + 1)
            if (isTrue && freeCount == 1) {
                user.setMemberemail("");
            }
            //释放账号
            FreeUserFromAccountSystem(user, AccountSystem.FreeNoCare, freeCount, AccountSystem.ZeroCancel,
                    AccountSystem.NullDepartTime);
        }
        //返回结果
        return isTrue;
    }

    /**
     * WEB端获取检票口信息（在支付后才有此数据）
     */
    private void webSiteWicket(Trainorderchange trainOrderChange, Customeruser user, RepServerBean rep) {
        try {
            //车次号
            String traino = trainOrderChange.getTctrainno();
            //改签退
            int changeType = trainOrderChange.getTcischangerefund();
            //非改签退、高铁或动车、没有检票口
            if (changeType != 1 && (traino.toUpperCase().startsWith("G") || traino.toUpperCase().startsWith("D"))
                    && ElongHotelInterfaceUtil.StringIsNull(trainOrderChange.getWicket())) {
                //地址
                String url = rep.getUrl();
                //cookie
                String cookie = user.getCardnunber();
                //请求参数
                String param = "datatypeflag=99&cookie=" + cookie + JoinCommonAccountInfo(user, rep);
                //请求REP
                String result = RequestUtil.post(url, param, "UTF-8", new HashMap<String, String>(), checkTimeout);
                //记录日志
                WriteLog.write("TongChengChange_SaveWicket", trainOrderChange.getId() + "--->" + url + "--->" + result);
                //有检票口
                if (result.contains("检票口")) {
                    result = JSONObject.parseObject(result).getString("wicket").trim();
                    //保存数据
                    saveWicketInfo(trainOrderChange, result);
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("webSiteWicket_Exception", e,
                    "获取检票口信息异常--->>改签单id:" + trainOrderChange.getId());
        }
    }

    /**
     * 保存检票口信息
     */
    private void saveWicketInfo(Trainorderchange trainOrderChange, String wicket) {
        //赋值
        trainOrderChange.setWicket(wicket);
        //SQL
        String procedure = "[sp_T_TRAINORDERCHANGE_update_Wicket] @ID=" + trainOrderChange.getId() + ",@Wicket='"
                + wicket + "'";
        //记录日志
        WriteLog.write("TongChengChange_SaveWicket", trainOrderChange.getOrderid() + "--->" + procedure);
        //数据入库
        DBHelper.executeSql(procedure, dbSourceEnum);
    }

    /**
     * 改签回调
     */
    private void callBackTrue(JSONObject json, Trainorder order, Trainorderchange change, int random) {
        //确认成功
        if (json.getBooleanValue("success")) {
            //参数
            json.put("method", "train_confirm_change");
            json.put("reqtoken", change.getConfirmReqtoken());
            json.put("callBackUrl", change.getConfirmCallBackUrl());
            //检票口
            json.put("ticketcheckinfo", ticketCheckInfo(order, change));
            //记录日志
            WriteLog.write("t同程火车票接口_4.14确认改签审核", random + ":callBackReq:" + json);
            //订单ID
            long orderId = order.getId();
            long changeId = change.getId();
            //改签退
            int changeType = change.getTcischangerefund();
            //接口类型
            int interfacetype = order.getInterfacetype() != null && order.getInterfacetype() > 0 ? order
                    .getInterfacetype() : getOrderAttribution(orderId);
            //回调结果
            String callbackresult = "";
            //改签退
            if (changeType == 1) {
                callbackresult = callBackChangeRefund(json, order, change);
            }
            //回调淘宝
            else if (interfacetype == TrainInterfaceMethod.TAOBAO) {
                callbackresult = callBackTaoBao(json, order, change);
            }
            //回调同程
            else {
                callbackresult = callBackTongCheng(json, order, change);
            }
            //记录日志
            WriteLog.write("t同程火车票接口_4.14确认改签审核", random + ":callBackRes:" + callbackresult);
            //回调成功
            boolean callbacktrue = "success".equalsIgnoreCase(callbackresult) ? true : false;
            //采购问题
            if (!callbacktrue) {
                String updateSql = "[sp_T_TRAINORDERCHANGE_updateISQUESTIONCHANGEById] @C_ISQUESTIONCHANGE = "
                        + Trainorderchange.CAIGOUQUESTION + ", @ID = " + changeId;
                DBHelper.executeSql(updateSql, dbSourceEnum);
            }
            //回调结果
            String callbackResult = callbacktrue ? "成功" : "失败---" + callbackresult;
            //订单标识
            String changeFlag = change.getChangeArrivalFlag() == 1 ? "变更到站" : "改签";
            //记录日志
            createtrainorderrc(1, "[确认 - " + changeId + "]---" + changeFlag + "确认成功---回调" + callbackResult, orderId,
                    changeId, Trainticket.FINISHCHANGE, changeType == 1 ? "自动改签退" : "系统");
            //扣款、退款
            if (callbacktrue && changeType != 1) {
                //最新状态
                int currentStatus = currentStatus(changeId);
                //审核状态>>0:默认；1:审核中；2:审核成功；3:已扣款；4:已退款
                if (currentStatus == 3 || currentStatus == 4) {
                    createtrainorderrc(1, "[确认 - " + changeId + "]" + changeFlag + "已扣款，中断后续扣款动作", orderId, changeId,
                            Trainticket.FINISHCHANGE, "系统");
                    return;
                }
                //问题类型
                int question = -1;
                //审核状态
                int newStatus = currentStatus;
                //扣新票款
                Payresult payResult = changePayPrice(changeId, order.getOrdernumber(), change.getTcprice(),
                        order.getQunarOrdernumber(), order.getAgentid());
                //扣款问题
                if (payResult == null || !payResult.isPaysuccess()) {
                    question = Trainorderchange.CUTPRICEQUESTION;
                }
                //支付成功，返还原票价
                else {
                    //已扣款
                    newStatus = 3;
                    //记录日志
                    createtrainorderrc(1, "[确认 - " + changeId + "]" + changeFlag + "扣款成功：" + change.getTcprice(),
                            orderId, changeId, Trainticket.FINISHCHANGE, "系统");
                    //退款处理
                    Payresult returnResult = changeReturnPrice(changeId, order.getOrdernumber(),
                            change.getTcoriginalprice(), change.getChangeProcedure(), order.getQunarOrdernumber(),
                            order.getAgentid());
                    //退款问题
                    if (returnResult == null || !returnResult.isPaysuccess()) {
                        question = Trainorderchange.RETURNPRICEQUESTION;
                    }
                    //记录日志
                    else {
                        //已退款
                        newStatus = 4;
                        //退款
                        float price = ElongHotelInterfaceUtil.floatSubtract(change.getTcoriginalprice(),
                                change.getChangeProcedure());
                        //保存
                        createtrainorderrc(1, "[确认 - " + changeId + "]" + changeFlag + "退款成功：" + price, orderId,
                                changeId, Trainticket.FINISHCHANGE, "系统");
                    }
                }
                //更新数据
                updateCheckStatus(changeId, newStatus, currentStatus, question);
                //扣除手续费
                //sendDeductionMQGQ(orderId, changeId);
            }
        }
    }

    /**
     * 同程改签回调检票口信息
     */
    private JSONArray ticketCheckInfo(Trainorder order, Trainorderchange change) {
        //结果
        JSONArray array;
        //检票口信息
        String wicket = change.getWicket();
        //信息为空
        if (ElongHotelInterfaceUtil.StringIsNull(wicket)) {
            array = null;
        }
        //信息非空
        else {
            //初始化
            array = new JSONArray();
            //遍历乘客
            for (Trainpassenger passenger : order.getPassengers()) {
                //乘客车票
                Trainticket ticket = passenger.getTraintickets().get(0);
                //结果赋值
                JSONObject object = new JSONObject();
                object.put("checkinfo", wicket);
                object.put("passengersename", passenger.getName());
                object.put("passportseno", passenger.getIdnumber());
                object.put("piaotypename", ticket.getTickettypestr() + "票");
                object.put("passporttypeseidname", passenger.getIdtypestr());
                object.put("piaotype", String.valueOf(ticket.getTickettype()));
                object.put("passporttypeseid", getIdtype12306(passenger.getIdtype()));
                //添加结果
                array.add(object);
            }
        }
        //返回
        return array;
    }

    /**
     * 取最新状态，判断是否可扣款
     */
    private int currentStatus(long changeId) {
        return findTrainOrderChangeById(changeId).getTcpaystatus();
    }

    /**
     * 改签退回调
     */
    private String callBackChangeRefund(JSONObject retobj, Trainorder trainOrder, Trainorderchange trainOrderChange) {
        //参数
        retobj.put("changeId", changeId);
        retobj.put("transactionid", trainOrder.getOrdernumber());
        retobj.put("trainOrderId", trainOrderChange.getOrderid());
        //回调参数
        String jsonStr = "changeType=2&jsonStr=" + retobj;
        //回调地址
        String changeRefundCallBackUrl = trainOrderChange.getConfirmCallBackUrl();
        //回调
        return RequestUtil.post(changeRefundCallBackUrl, jsonStr, "UTF-8", new HashMap<String, String>(), 0);
    }

    /**
     * 回调、扣款、退款
     */
    private String callBackTongCheng(JSONObject retobj, Trainorder trainOrder, Trainorderchange trainOrderChange) {
        //地址
        String url = getSysconfigString("tcTrainCallBack");
        //回调
        return RequestUtil.post(url, retobj.toString(), "UTF-8", new HashMap<String, String>(), 0);
    }

    /**
     * 回调TAOBAO
     */
    private String callBackTaoBao(JSONObject retobj, Trainorder trainOrder, Trainorderchange trainOrderChange) {
        //参数
        retobj.put("changeorderid", trainOrderChange.getId());
        retobj.put("changealipaytradeno", trainOrderChange.getSupplytradeno());
        //地址
        String url = PropertyUtil.getValue("TaoBao_Change_CallBack_Url", "train.properties");
        //回调
        return RequestUtil.post(url, retobj.toString(), "UTF-8", new HashMap<String, String>(), 0);
    }

    /**
     * 改签结束删除MemCached中的账号
     */
    public void deleteMemCached(Trainorder order, Trainorderchange change) {
        try {
            //客人账号或改签退
            if (order.getOrdertype() == 3 || order.getOrdertype() == 4 || change.getTcischangerefund() == 1) {
                return;
            }
            //账号
            String supplyaccount = order.getSupplyaccount();
            //删除
            if (!ElongHotelInterfaceUtil.StringIsNull(supplyaccount) && !"".equals(supplyaccount.split("/")[0])) {
                OcsMethod.getInstance().remove("TrainChange=" + supplyaccount.split("/")[0]);
            }
        }
        catch (Exception e) {

        }
    }

    /**
     * 改签扣款和退差价
     * @param orderid 订单ID
     * @param ordernumber 订单号
     * @param orderprice 订单支付金额
     * @param refordernum  同城关联订单号
     * @param agentid 代理ID
     * @remark 业务类型 3.火车票（已无支付操作）31 火车票退票    32 改签退款  33改签扣款 34 线下改签
     */
    private Payresult changePayPrice(long changeId, String ordernumber, float orderprice, String refordernum,
            long agentid) {
        return vmonyPay(changeId, ordernumber, orderprice, 33, refordernum, agentid);
    }

    private Payresult changeReturnPrice(long changeId, String ordernumber, float orderprice, float changeProcedure,
            String refordernum, long agentid) {
        orderprice = ElongHotelInterfaceUtil.floatSubtract(orderprice, changeProcedure);
        return vmonyPayReturn(changeId, ordernumber, -orderprice, 32, refordernum, agentid);
    }

    /**
     * 改签扣手续费
     */
    //    private void sendDeductionMQGQ(long orderId, long changeId) {
    //        try {
    //            //地址
    //            String url = getSysconfigString("activeMQ_url");
    //            //名称
    //            String MQ_NAME = "QueueMQ_TrainorderDeductionGq";
    //            //参数
    //            JSONObject json = new JSONObject();
    //            json.put("type", 1);
    //            json.put("orderid", orderId);
    //            json.put("changeid", changeId);
    //            //发送MQ
    //            ActiveMQUtil.sendMessage(url, MQ_NAME, json.toString());
    //        }
    //        catch (Exception e) {
    //            ExceptionUtil.writelogByException("改签扣手续费异常", e, orderId + "-->" + changeId);
    //        }
    //    }

    /**
     * 更新改签支付审核状态   newStatus：0默认 1审核中 2审核成功 3审核失败 4已扣款 5扣款失败
     * @param oldStatus>=0时，更新时判断原状态
     * @param question>=0时，更新问题改签
     */
    private int updateCheckStatus(long changeId, int newStatus, int oldStatus, int question) {
        //SQL
        //问题改签
        //        String updateSql = "";
        /*if (question >= 0) {
            updateSql = "update T_TRAINORDERCHANGE set C_TCPAYSTATUS = " + newStatus + ", C_ISQUESTIONCHANGE = "
                    + question + " where ID = " + changeId;
        }
        else {
            updateSql = "update T_TRAINORDERCHANGE set C_TCPAYSTATUS = " + newStatus + " where ID = " + changeId;
        }
        //判断状态
        if (oldStatus >= 0) {
            updateSql += " and C_TCPAYSTATUS = " + oldStatus;
        }*/
        String updateSql = "[sp_T_TRAINORDERCHANGE_updateOrderChangeStatusPaymentStatus] @changeId=" + changeId
                + ",@newStatus = " + newStatus + ",@oldStatus =" + oldStatus + ",@question=" + question;
        return DBHelper.UpdateData(updateSql, dbSourceEnum);
    }

    /**
     * 创建火车票的操作记录
     */
    private void createtrainorderrc(int ywtype, String content, Long orderid, Long ticketid, int status,
            String createuser) {
        try {
            if (content != null && content.length() > 190) {
                content = content.substring(0, 190) + "...";
            }
            String insertSQL = "[insertTrainOrderRC] @orderId = " + orderid + ", @ticketId = " + ticketid
                    + " , @status = " + status + " , @createUser = " + createuser + " , @content ='" + content
                    + "' , @ywType = " + ywtype;
            DBHelper.executeSql(insertSQL, dbSourceEnum);

        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("TrainorderChangePayMessageListener_err", e,
                    "改签审核消费者_createtrainorderrc方法_创建火车票的操作记录错误");
        }
    }

    /**
     * 虚拟账户退款
     */
    public Payresult vmonyPayReturn(long changeId, String ordernumber, float orderprice, int ywtype,
            String refordernum, long agentid) {
        WriteLog.write("tc虚拟账户vmonyPayReturn", "订单号ID:" + changeId + ":订单号:" + ordernumber + ":订单价格:" + orderprice
                + ":业务类型:" + ywtype);
        Payresult payresult = new Payresult();
        payresult.setPaysuccess(true);
        payresult.setResultmessage("支付成功");
        //升级针对虚拟账户的扣款操作
        try {
            String memo = "退款" + (0 - orderprice) + "元";
            String sql = "TrainorderDeduction @orderid=" + changeId + ",@ordernumber='" + ordernumber + "',@yewutye="
                    + ywtype + ",@rebateagentid=" + agentid + ",@rebatemoney=" + (0D - orderprice)
                    + ",@vmenble=1,@rebatetype=" + Rebaterecord.TUIKUAN + ",@rebatememo='" + memo
                    + "',@customerid=0,@refordernumber='" + refordernum + "',@paymethod=10,@paystate=0";
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            if (null != list && list.size() > 0) {
                Map map = (Map) list.get(0);
                String result = map.get("result").toString();
                String error = map.get("message").toString();
                if (!"success".equals(result)) {// 失败
                    payresult.setPaysuccess(false);
                    payresult.setResultmessage(error);
                }
                else if ("success".equals(result)) { // 成功
                    payresult.setPaysuccess(true);
                    System.out.println("扣款成功" + result);
                }
                WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + "--->result:" + result
                        + "--->error:" + error);
            }
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + ":支付成功--->sql:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
            payresult.setPaysuccess(false);
            payresult.setResultmessage("支付失败！");
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + changeId + ":订单号：" + ordernumber + "," + e.getMessage());
        }
        return payresult;
    }

}