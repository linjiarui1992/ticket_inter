package com.ccservice.train.mqlistener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.rule.TaobaoTrainRule;
import com.ccservice.util.httpclient.HttpsClientUtils;

/**
 * 支付处理消费者
 * @author wzc
 *
 */
public class TrainorderPayMessageListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    private String alipayurlset;

    private long order_timeout_time = 10 * 60 * 1000l;//淘宝支付前给后续操作的预留时间

    public TrainorderPayMessageListener(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

    @Override
    public void onMessage(Message message) {
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            int paytimes = jsonobject.containsKey("paytimes") ? jsonobject.getIntValue("paytimes") : 0;
            int t = new Random().nextInt(10000);
            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":" + orderNoticeResult);
            if (MQMethod.ORDERPAY == type) {//订单支付
                int paytype = Integer.parseInt(getSysconfigString("getpayurltype"));
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Long.valueOf(orderid));
                //TODO 淘宝超时 不再支付
                if (taobaoTimeout(trainorder)) {
                    return;
                }
                if (trainorder.getOrderstatus() == Trainorder.WAITISSUE
                        && trainorder.getState12306() == Trainorder.ORDEREDWAITPAY) {//等待出票，12306等待支付
                    String payurl = "-1";
                    String i_url = alipayurlset;
                    if (paytype == 2) {//支付宝支付
                        System.out.println("支付宝支付");
                        if (trainorder.getSupplyaccount() != null
                                && trainorder.getSupplyaccount().contains("alipayurl")) {
                            payurl = getSysconfigString(trainorder.getSupplyaccount().split("/")[1]);
                            i_url = trainorder.getSupplyaccount().split("/")[1].replaceAll("alipayurl", "");
                            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":二次支付：" + payurl + ",支付索引："
                                    + i_url + ",支付账户：" + trainorder.getSupplyaccount());
                        }
                        else {
                            payurl = getSysconfigString("alipayurl" + alipayurlset);
                        }
                        autoalipayPay(trainorder, i_url, payurl, t, paytimes);
                    }
                    else if (paytype == 1) {//网银支付
                        System.out.println("网银支付");
                        if (trainorder.getSupplyaccount() != null && trainorder.getSupplyaccount().contains("UNIONPAY")) {
                            payurl = getSysconfigString(trainorder.getSupplyaccount().split("/")[1]);
                            i_url = trainorder.getSupplyaccount().split("/")[1].replaceAll("UNIONPAY", "");
                            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":二次支付：" + payurl + ",支付索引："
                                    + i_url + ",支付账户：" + trainorder.getSupplyaccount());
                        }
                        else {
                            payurl = getSysconfigString("unionpayurl" + alipayurlset);
                        }
                        autoUnionPay(trainorder, i_url, t, payurl, paytimes);
                    }
                    WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":支付：" + payurl + ",支付索引：" + i_url
                            + ",支付账户：" + trainorder.getSupplyaccount() + ":支付模式：" + paytype);
                }
                else {
                    WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + "订单id:" + trainorder.getId()
                            + ":不符合支付条件,订单状态：" + trainorder.getOrderstatus() + ",12306状态：" + trainorder.getState12306());
                }
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 银联自动支付
     * @param trainorder 订单
     * @param t 随机数
     * @param 访问地址url
     * @param times 支付次数
     * @return
     */
    public boolean autoUnionPay(Trainorder trainorder, String i, int t, String url, int times) {
        WriteLog.write("12306银联自动支付", "订单号：" + trainorder.getOrdernumber() + "对应支付银联地址：" + url);
        trainorder.setOrderstatus(0);//支付订单不改变订单状态
        boolean flag = false;
        String payurl = trainorder.getAutounionpayurl();
        if (payurl != null && !"".equals(payurl)) {
            JSONObject jso = new JSONObject();
            jso.put("username", "trainone");
            jso.put("sign", "FF1E07242CE2C86A");
            jso.put("paymode", "1");
            jso.put("servid", "1");
            jso.put("ordernum", trainorder.getOrdernumber());
            jso.put("paytype", "2");
            jso.put("paymoney", trainorder.getOrderprice());
            try {
                jso.put("postdata", payurl);
                try {
                    if (times > 0) {
                        jso.put("RePay", "true");
                        WriteLog.write("12306银联自动支付", t + ":" + times + "次支付:" + jso.getString("RePay"));
                    }
                }
                catch (Exception e1) {
                }
                WriteLog.write("12306银联自动支付", t + ":获取银联支付链接:" + jso.getString("postdata"));
            }
            catch (Exception e2) {
                WriteLog.write("12306银联自动支付", t + ":获取银联支付链接异常" + e2.getMessage());
                e2.printStackTrace();
            }
            try {
                trainRC(trainorder.getId(), "开始支付订单");
                trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/UNIONPAY" + i);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                String supplyaccount = trainorder.getSupplyaccount();
                Map listdata = new HashMap();
                listdata.put("data", jso.toString());
                WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",支付前支付账号：" + supplyaccount);
                String result = HttpsClientUtils.posthttpclientdata(url, listdata, 70000l);
                WriteLog.write("12306银联自动支付", t + ":地址：" + url + ",请求数据：" + jso.toString() + ",返回结果：" + result);
                if ("".equals(result) || !result.contains("status")) {//没返回任何数据
                    changeQuestionOrder(trainorder, "多次支付失败");
                    flag = false;
                }
                JSONObject jsoresult = JSONObject.parseObject(result);
                if (jsoresult.getString("status").equals("1") && "支付成功".equals(jsoresult.getString("info"))) {
                    try {
                        trainRC(trainorder.getId(), "支付成功");
                        trainorder.setSupplytradeno(jsoresult.getString("SerialNum"));
                        trainorder.setAutounionpayurlsecond(jsoresult.getString("NetUserName"));//使用的银行卡号
                        trainorder.setState12306(Trainorder.ORDEREDPAYED);//12306订单支付成功
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                                System.currentTimeMillis(), 0).sendqueryMQ();
                        String now = TimeUtil.gettodaydatebyfrontandback(4, 0);
                        String sql = "delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                                + ";insert T_TRAINREFINFO(C_ORDERID,C_PAYSUCCESSTIME) values(" + trainorder.getId()
                                + ",'" + now + "')";
                        WriteLog.write("12306银联自动支付", t + ":更新sql：" + sql);
                        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",提交自动支付成功");
                        flag = true;
                    }
                    catch (Exception e1) {
                        WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "保持银联交易号失败");
                        e1.printStackTrace();
                    }
                }
                else {
                    new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(trainorder, 1, times);
                    flag = false;
                }
            }
            catch (Exception e) {
                WriteLog.write("12306银联自动支付", t + ":3订单号：" + trainorder.getOrdernumber() + "银联支付失败");
                flag = false;
            }
        }
        else {
            new TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
            WriteLog.write("12306银联自动支付", t + ":" + trainorder.getOrdernumber() + ":发送支付链接队列。");
        }
        return flag;

    }

    public static void main(String args[]) {
        String url = "http://192.168.0.109:8080/ccs_paymodule/unionpay";
        JSONObject jso = new JSONObject();
        jso.put("username", "trainone");
        jso.put("sign", "FF1E07242CE2C86A");
        jso.put("paymode", "1");
        jso.put("servid", "1");
        jso.put("ordernum", new Random().nextInt(1000));
        jso.put("paytype", "2");
        jso.put("paymoney", "8");
        jso.put("postdata",
                "https://ebank.cmbc.com.cn/weblogic/servlets/EService/CSM/B2C/servlet/ReceiveMerchantCMBCTxReqServlet?orderinfo=AYgEAAAAAX1tZeih6Tiehkw6B28ew4vDfvHXAffUwppt623aQbImORDy80h3%2FPaHUDNJlRT3Bltm%2By6lY8wpns1idojuR%2BILmF1s8kmKs6s3UFpHx1hWM8RFLZue%2FXw%2BvPK6M2OvjPEPNQ6dOfGd1IQFSNTlQuuyxWstJ9sFqtkOvloIxhnA%2FQenFSiJdFnNz2XfNdp48ADkf0T8spdzWTp7eKDlVAQkHs4pKnIH6vJaxzUNSU3Aecwscr5BTOQLppTWSY%2B96bOB2JrFrwnq6FH8K2YbFOWIHjcTiAYZREfgg8sede5F%2BfzwuaW6MIffmyWXFt%2FvsldooaONhs1rurIGFfxvn124ht1BmFOya%2B6GJ9%2F6trbCcFq4CpTPgi9yR%2FCcQFOLrovN9WDMdXZRTSw6xjSUN3mwPFeJ4jnHfOqmwzSDdZPqHFkDklSBPNyRulRk98n1t5vexiwexArzZud2i8cnuY6YXUpwepdzslI3Gu9OkHry3t%2Blz2M%2BUrzx3QCA2XahkdYmBGDka0LnOByKxSSsqweTPBI9ym87KUMrcDmrflq1ka8Z78VhYlMVi6ejmSjwvLB%2FLl9WKfBacBnuaWBNPZPH%2F8bgUTavjrO8MO9i7hs6OT11mI35n4Evl2SsANtCTFsYN%2FSV2B8NDRXrUq7%2FJ4uf%2FHp3ZHCyBmr3gjLiRxJ62D6r0V8p2kY0CkBUpVNfE6wOqnoOLX76CDfBox2gH2yrGh0Ulmrt%2Fvrz8oGq4OzrgoZZ%2Bfn8170%2BdJVMstsx7K742XCbWQYAcAE5MhcC98tQULuLPPJLZ%2FF8kUYu5BpeXImD%2B5XqDx46x2tHdVIrng048R8coFDZWe3LD4e2Q3saFsFJMqxcAroLn5RIOgohn0c3USjDDCL49k80Mff%2B1zDWOIqqafPXw1xPru0JMgvVbe0zRxjFi2HrkmPcO3Jvxwf7fYz5MtgAY%2BxEoJ6B0hReQxdzDmqv%2BxtgeP1zJKfmx%2BX%2FC9ZAfz6quYvxG6rTwj%2B9%2F78uqRhhImGyXvTSZ%2FSH4UiqgInT58NAWeRsb47qRJ2ZB6MB1W%2BzoQRFi5Gk3s9lRcKWkorSo6MwBXBxA2ml0hWhOV1z32%2FImEpLw7A05l9A1ExMO5pRSl64uQS1q1W3Mb80WKlZkVCqDdCNAdc3mqKOK63yplScWT5enAowUWAtQEKhO7y3IvPU7rTe3i0moeYSKQxnZa88qHq%2BLdK%2FdxsFsrYeQuhcnuoshk9k6T%2FQexTzz%2Bw4nIUCM5%2Bx%2B2tPconQJpgeKzilucn4rOP%2Fef%2BMbMEsuaj6a0L64sUTgI0tUqbJibhkHwPcYJ8oNQ1SdREwcK3SURu%2FEEdrqmDSUzDjvZdtpcHVjV%2F22%2F6UfAXP7Oh%2B30LemcO47riJxvphnWDgTLulmK1O3Cp4JLxjB2oRaQWYCKsaza1Zs1zY5g2g0oDQUUEn0dJIK2bsKDv2FgxTYxyYdAIazXonLuIqy8hMD%2F1r%2F5hEgApCrUsvtbAlAX8tacqb%2B2Q%2FNdlPdXDkdVGppvaqtugesZDdFwBWt4RuTFlLD8x%2FdRbFDZaFnVow1dvsuyM%2BXpnCTMUEattOGrpqm3F4%2Fb3xT2k4wqwxUGtepCi2QoOadX5Ex3nLEpiRFLEcHGvif6U5mXbq46elEtqaeI4%2FxqtHs8ArL0ZwjPIq9BA%2BZ0jUaSmtGJVz58ER3Nwe8ES0pFYPgsJDaKfNrkH0hjWW3sQx%2FEMHunx43MX4j%2BEYJTDZkMYKgdfDN%2FTHOP0xyXxD2%2Fb%2FVj1K8EDmvkhOBFEZRiyj7xjjgK1mhHSd9HTLqYB2Tst2jJKYxeMhqKNKl19I6FcmAwDs207UxUbcNX9LxlBnrzlWqU8nyA3n3nPO%2FEoP4yZ%2BlMRL2RHUEf%2FDhQ%3D%3D");
        Map listdata = new HashMap();
        listdata.put("data", jso.toString());
        String result = HttpsClientUtils.posthttpclientdata(url, listdata, 70000l);
        System.out.println(result);
    }

    /**
     *交通获取交易金额 
     * @param args
     * @time 2014年12月4日 下午8:01:13
     * @author wzc
     */
    public static String searWnumber(String params) {
        String retrunval = "";
        try {
            Pattern p = Pattern.compile("ord_id_ext=W\\d*");
            Matcher m = p.matcher(params);
            retrunval = "";
            if (m.find()) {
                String count = m.group();
                retrunval = count.replace("ord_id_ext=", "");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if ("".equals(retrunval)) {
            retrunval = searWnumberTradeNum(params);
        }
        return retrunval;
    }

    /**
     *交通获取交易金额 
     * @param args
     * @time 2014年12月4日 下午8:01:13
     * @author wzc
     */
    public static String searWnumberTradeNum(String params) {
        String retrunval = "";
        try {
            String[] param = params.split("&");
            for (String str : param) {
                if (str.contains("out_trade_no")) {
                    retrunval = str.replaceAll("out_trade_no=", "");
                    break;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return retrunval;
    }

    /**
     * 支付宝支付方法
     * @param trainorder 订单
     * @param i 支付索引
     * @param url 支付ur
     * @param t 标识
     * @param times 支付此时
     * @return
     */
    public boolean autoalipayPay(Trainorder trainorder, String i, String url, int t, int times) {
        boolean flag = false;
        trainorder.setOrderstatus(0);//支付订单不改变订单状态
        String payurl = trainorder.getChangesupplytradeno();
        if ((payurl != null) && (!("".equals(payurl)))) {
            String wnumber = "";
            if (payurl.contains("gateway.do")) {
                wnumber = searWnumber(payurl);
            }
            JSONObject jso = new JSONObject();
            jso.put("username", "trainone");
            jso.put("sign", "FF1E07242CE2C86A");
            jso.put("postdata", payurl);
            jso.put("paymode", "1");
            jso.put("servid", "1");
            jso.put("ordernum", trainorder.getOrdernumber());
            jso.put("paytype", "1");
            jso.put("WNumber", wnumber);
            int paycount = 0;
            try {
                paycount = Integer.valueOf(getSysconfigString("paycount"));//支付次数
                WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":支付次数：" + times);
                if (times > paycount) {
                    changeQuestionOrder(trainorder, "多次支付失败");
                    return flag;
                }
                if (times > 0) {
                    jso.put("RePay", "true");
                    WriteLog.write("12306_TrainorderPayMessageListener_MQ",
                            t + ":" + times + "次支付:" + jso.getString("RePay"));
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            trainRC(trainorder.getId(), "开始支付订单");
            trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/alipayurl" + i);
            String supplyaccount = trainorder.getSupplyaccount();
            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":订单号：" + trainorder.getOrdernumber()
                    + ",请求数据：" + jso.toString() + ",支付hou支付账号：" + supplyaccount);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
            Map listdata = new HashMap();
            listdata.put("data", jso.toString());
            String result = HttpsClientUtils.posthttpclientdata(url, listdata, 90000l);
            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":90t返回结果：" + result);
            if ("".equals(result)) {
                changeQuestionOrder(trainorder, "支付超时");
                return flag;
            }
            else if (!(result.contains("status"))) {
                changeQuestionOrder(trainorder, "支付异常");
                return flag;
            }
            //支付成功
            //{"status":true,"info":"Success","OrderID":"T150204070050667635522",
            //"AliTradeNo":"2015020421001004270098590118","AliOrderID":"0204b93360d1ce49d45aa86015463277",
            //"AliPayUserName":"hangtianhy002@126.com","out_trade_no":"","Amount":29.5,"Banlance":42462.4,
            //"RefundAmount":0.0,"orderstatus":"支付成功","PayRetUrl":"","PayRetParam":""}

            //支付失败
            //{"status":false,"info":"订单状态不对暂时不能支付","OrderID":"T150204101104367736741"}

            if (result.contains("已经存在") || result.contains("订单状态不对暂时不能支付")) {
                String par = "{'ordernum':'" + trainorder.getOrdernumber() + "','cmd':'seachliushuihao','paytype':'1'}";
                Map listdatatt = new HashMap();
                listdatatt.put("data", par);
                result = HttpsClientUtils.posthttpclientdata(url, listdatatt, 60000l);
                WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ",订单已存在，查询订单结果：" + result);
            }
            if (result.contains("status")) {
                JSONObject obj = JSONObject.parseObject(result);
                boolean status = obj.getBoolean("status");
                if (status) {
                    String liushuihao = obj.getString("AliTradeNo");
                    String state = "";
                    if (obj.containsKey("orderstatus")) {
                        state = obj.getString("orderstatus");
                    }
                    //查询失败成功信息
                    String msg = "";
                    if (obj.containsKey("info")) {
                        msg = obj.getString("info");
                    }
                    String AliPayUserName = "";
                    if (obj.containsKey("AliPayUserName")) {
                        AliPayUserName = obj.getString("AliPayUserName");//当前支付宝账号
                    }
                    float balance = -1;
                    String Banlancestr = "";
                    if (obj.containsKey("Banlance")) {
                        Banlancestr = obj.getString("Banlance");
                        if (Banlancestr != null && !"".equals(Banlancestr) && !"null".equals(Banlancestr)
                                && "支付成功".equals(state)) {
                            balance = Float.valueOf(Banlancestr);//当前账户余额
                        }
                    }
                    //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                    if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "支付成功".equals(state)) {
                        payclock(AliPayUserName, balance, t);
                        String sqlpaytime = "delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                                + ";insert T_TRAINREFINFO(C_ORDERID,C_PAYSUCCESSTIME) values(" + trainorder.getId()
                                + ",'" + TimeUtil.gettodaydate(4) + "')";
                        List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpaytime, null);
                        WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":" + sqlpaytime + ":" + liushuihao);
                        trainorder.setSupplytradeno(liushuihao);
                        trainorder.setAutounionpayurlsecond(AliPayUserName);//使用的银行卡号
                        trainorder.setState12306(Trainorder.ORDEREDPAYED);//更新12306订单为  已支付订单
                        trainorder.setPaysupplystatus(0);
                        trainRC(trainorder.getId(), "支付成功");
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":" + trainorder.getOrdernumber()
                                + ":" + msg);
                        new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                                System.currentTimeMillis(), 0).sendqueryMQ();
                        WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":更新结束");
                        flag = true;
                    }
                    else if ("支付失败".equals(state) && getpaycontrol(msg)) {//运行错误
                        WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":更新结束");
                        new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(trainorder, 1, times);
                        return flag;
                    }
                    else {
                        WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":该状态下不用处理。");
                    }
                }
                else {
                    WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":未支付成功，重新发送队列支付：支付次数：" + times);
                    new TrainpayMqMSGUtil(MQMethod.ORDERPAY_NAME).sendPayMQmsg(trainorder, 1, times);
                    return flag;
                }
            }
        }
        else {
            new TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
            WriteLog.write("12306_TrainorderPayMessageListener_MQ", t + ":" + trainorder.getOrdernumber()
                    + ":发送支付链接队列。");
        }
        return flag;
    }

    
   
    /**
     * 根据返回信息是否确定支付失败
     * @param info
     * @return
     */
    public boolean getpaycontrol(String info) {
        if (info != null && !"".equals(info)) {
            if ("支付失败:CASHIER_ACCESS_GAP_CONTROL_TIP".equals(info)) {
                return false;
            }
            //            else if (info.contains("登录失效")) {
            //                return false;
            //            }
        }
        return true;
    }

    /**
     * 支付预警
     */
    public void payclock(String AliPayUserName, Float balance, int t) {
        try {
            WriteLog.write("12306tz资金账户余额", t + ":" + AliPayUserName + ",余额：" + balance);
            if (balance >= 0) {
                String sqlmoney = "SELECT C_CLOCKMONEY,C_BANLANCE FROM T_PAYACCOUNTINFO WHERE C_ACCOUNT='"
                        + AliPayUserName + "'";
                List yuetixinglistt = Server.getInstance().getSystemService().findMapResultBySql(sqlmoney, null);
                float C_CLOCKMONEY = 20000;
                float C_BANLANCE = 0;
                if (yuetixinglistt.size() > 0) {
                    Map map = (Map) yuetixinglistt.get(0);
                    C_CLOCKMONEY = Float.valueOf(map.get("C_CLOCKMONEY").toString());
                    C_BANLANCE = Float.valueOf(map.get("C_BANLANCE").toString());
                    WriteLog.write("12306tz资金账户余额", t + ":支付前：" + C_BANLANCE + ",支付后：" + balance + ",提醒金额："
                            + C_CLOCKMONEY);
                    if (C_BANLANCE > C_CLOCKMONEY && balance < C_CLOCKMONEY) {
                        String content = AliPayUserName + "账户余额" + balance + ",请尽快充值。";
                        WriteLog.write("12306tz资金账户余额", t + ":" + content);
                        String tetctelphonenum = getSysconfigString("payremandtel");
                        sendSmspublic(content, tetctelphonenum, 46);
                    }
                }
                else {
                    String content = "账户数据库没找到支付账号" + AliPayUserName;
                    WriteLog.write("12306tz资金账户余额", t + ":" + content);
                    sendSmspublic(content, "15811073432", 46);
                }
                String sql = "update T_PAYACCOUNTINFO set C_BANLANCE=" + balance + ",C_LASTUPDATETIME='"
                        + TimeUtil.gettodaydate(4) + "' where C_ACCOUNT='" + AliPayUserName + "'";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
        }
        catch (Exception e) {
        }
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 获取支付索引
     * @return
     */
    public int getPayIndex() {
        int payindex = 0;
        Map<String, String> datavale = Server.getInstance().getDateHashMap();
        if (datavale.containsKey("payindex")) {
            try {
                payindex = Integer.parseInt(datavale.get("payindex"));
            }
            catch (NumberFormatException e) {
            }
        }
        else {
            datavale.put("payindex", payindex + "");
        }
        return payindex;
    }

    /**
     * 增加索引
     * @return
     */
    public int addPayIndex() {
        int payindex = 0;
        Map<String, String> datavale = Server.getInstance().getDateHashMap();
        if (datavale.containsKey("payindex")) {
            try {
                payindex = Integer.parseInt(datavale.get("payindex"));
                if (payindex > 500) {
                    datavale.put("payindex", "0");
                }
                else {
                    payindex++;
                    datavale.put("payindex", "" + payindex);
                }
            }
            catch (NumberFormatException e) {
            }
        }
        else {
            datavale.put("payindex", payindex + "");
        }
        return payindex;
    }

    /**
     * 淘宝是否超时,超时回调淘宝出票失败,返回true
     * @param trainorder
     * @return
     * @time 2015年3月31日 上午5:35:39
     * @author fiend
     */
    public boolean taobaoTimeout(Trainorder trainorder) {
        try {
            int interfacetype = trainorder.getInterfacetype() != null && trainorder.getInterfacetype() > 0 ? trainorder
                    .getInterfacetype() : Server.getInstance().getInterfaceTypeService()
                    .getTrainInterfaceType(trainorder.getId());
            if (TrainInterfaceMethod.TAOBAO == interfacetype) {
                try {
                    String pay_order_timeout_time_str = getSysconfigString("pay_order_timeout_time");
                    this.order_timeout_time = Long.valueOf(pay_order_timeout_time_str) * 60 * 1000;
                }
                catch (Exception e) {
                    this.order_timeout_time = 10 * 60 * 1000L;
                }
            }
            if (TaobaoTrainRule.isTimeout(trainorder, interfacetype, this.order_timeout_time)) {
                refundTaobao(trainorder, "支付超时", "下单时间已超过规定时间");
                return true;
            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getAlipayurlset() {
        return alipayurlset;
    }

    public void setAlipayurlset(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

}
