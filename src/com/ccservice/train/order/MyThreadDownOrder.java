package com.ccservice.train.order;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 下单线程
 *
 */
public class MyThreadDownOrder extends Thread {
    private String ordermsg;//发送订单信息

    private String orderurl;//访问地址

    private long sid;//消息id

    static Logger log = Logger.getLogger(MyThreadDownOrder.class);

    public MyThreadDownOrder(String ordermsg, String orderurl, long sid) {
        this.ordermsg = ordermsg;
        this.orderurl = orderurl;
        this.sid = sid;
    }

    /**
     * 发送请求
     */
    public void run() {
        WriteLog.write("t4.5收单下单", sid + ":" + ordermsg);
        String msg = "";
        try {
            msg = SendPostandGet.submitPost(orderurl, "jsonStr=" + URLEncoder.encode(ordermsg, "UTF-8"), "UTF-8")
                    .toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        WriteLog.write("t4.5收单下单", "返回：" + sid + ":" + msg);
        String updatesql = "";
        if (msg != null && msg.length() > 0) {
            try {
                if ("success".equals(msg)) {
                    updatesql = "update T_TRAINORDERMSG set c_state=3 where id= " + sid;
                    Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                }
                else {
                    JSONObject obj = JSONObject.parseObject(msg);
                    String ordernumber = obj.getString("orderid");
                    boolean flag = obj.getBooleanValue("success");
                    if (flag) {
                        updatesql = "update T_TRAINORDERMSG set c_state=3,C_ORDERNUMBER='" + ordernumber
                                + "' where id= " + sid;
                        Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                    }
                }
            }
            catch (Exception e) {
                log.error(e);
            }
        }
        else {
            updatesql = "update T_TRAINORDERMSG set c_state=5 where id= " + sid;
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
        }
    }

    public long getSid() {
        return sid;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getOrderurl() {
        return orderurl;
    }

    public void setOrderurl(String orderurl) {
        this.orderurl = orderurl;
    }

    public MyThreadDownOrder(String ordermsg) {
        this.ordermsg = ordermsg;
    }

    public String getOrdermsg() {
        return ordermsg;
    }

    public void setOrdermsg(String ordermsg) {
        this.ordermsg = ordermsg;
    }

}
