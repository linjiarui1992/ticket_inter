package com.ccservice.train.rule;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

/**
 * 淘宝火车票规则类
 * @author fiend
 *
 */
public class TaobaoTrainRule {

    /**
     * 是否是超时订单，淘宝适用
     * @param trainorder
     * @param interfacetype
     * @param order_timeout_time
     * @return
     * @time 2015年3月31日 上午5:05:06
     * @author fiend
     */
    public static boolean isTimeout(Trainorder trainorder, int interfacetype, long order_timeout_time) {
        Timestamp ordertimeout = trainorder.getOrdertimeout();
        WriteLog.write("TaobaoTrainRule_isTimeout", trainorder.getId() + ":interfacetype:" + interfacetype
                + ":Ordertimeout:" + ordertimeout + ":order_timeout_time:" + order_timeout_time);
        if (TrainInterfaceMethod.TAOBAO != interfacetype) {
            return false;
        }
        long l1 = System.currentTimeMillis();
        if (ordertimeout == null) {
            //创建订单时间+30分钟
            long ordertimeout_l = trainorder.getCreatetime().getTime() + 30 * 60 * 1000l;
            //创建订单30分钟后减淘宝发起支付预留时间,如果小于当前时间判定为超时返回true
            WriteLog.write("TaobaoTrainRule_isTimeout", trainorder.getId() + ":ordertimeout:" + ordertimeout);
            if ((ordertimeout_l - order_timeout_time) > l1) {
                return true;
            }
            else {
                return false;
            }
        }
        else if ((ordertimeout.getTime() - order_timeout_time) > l1) {
            return false;
        }
        return true;
    }

    /**
     * 淘宝下单成功后是否可以支付规则
     * @param trainorder
     * @return
     * @author fiend
     */
    public static boolean isCanGenerate(Trainorder trainorder) {
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getPrice() > trainticket.getPayprice()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 淘宝下单成功后是否有充足的时间去支付
     * 拒发车时间超过30分钟
     * @param trainorder
     * @return
     * @author fiend
     */
    public static boolean isSufficientTime(Trainorder trainorder) {
        Trainticket trainticket = trainorder.getPassengers().get(0).getTraintickets().get(0);
        String depTimeStr = trainticket.getDeparttime();
        try {
            long depTimeLong = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(depTimeStr).getTime();
            int waitPayMinutes = 30;
            try {
                String sql = " [sp_T_SysConfig_selectByName] @name='TaoBaoWaitPayTime'";
                List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
                Map map = (Map) list.get(0);
                waitPayMinutes = Integer.valueOf(map.get("value").toString());
            }
            catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if ((depTimeLong - System.currentTimeMillis()) < waitPayMinutes * 60 * 1000) {
                WriteLog.write("距发车时间不足" + waitPayMinutes + "分钟_拒单", trainorder.getId() + "");
                return false;
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 淘宝回调后 生成操作记录
     * @param loginname
     * @param trainorder
     * @param isordered
     * @param isbudanstr
     * @time 2015年3月31日 上午4:01:05
     * @author fiend
     */
    public void taobaoRc(Trainorder trainorder, String loginname, boolean isordered, boolean iscallbacktrue) {
        String contentstr = isordered ? "---占座成功---调用支付" : (iscallbacktrue ? "---占座失败----回调成功" : "---占座失败---回调失败");
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(contentstr);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        try {
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorder.getId() + ":content:" + contentstr);
        }
    }

    /**
     * 淘宝锁单
     * 
     * @return
     * @time 2015年11月20日 下午12:54:50
     * @author fiend
     */
    public boolean handleTaobaoOrder(Trainorder trainorder) {
        boolean success = true;
        try {
            createTrainorderrc(trainorder.getId(), "taobao订单开始尝试锁单，锁单过程请勿人工参与！", "taobao锁单",
                    trainorder.getOrderstatus());
            //拼接锁单请求
            String taobao_handle_url = PropertyUtil.getValue("taobao_handle_url", "train.properties");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("orderid", trainorder.getId());
            jsonObject.put("taobao_order", trainorder.getQunarOrdernumber());
            WriteLog.write("淘宝锁单", jsonObject.toString());
            //虚拟锁单返回结果
            String result = "{\"success\":false,\"msg\":\"请求咱家自己的淘宝服务器异常！请联系技术！\"}";
            JSONObject resultJsonObject = JSONObject.parseObject(result);
            //为避免服务端更新等导致请求异常，特加10次循环共5分钟，防止死订单
            for (int i = 0; i < 10; i++) {
                try {
                    String resultreal = SendPostandGet.submitGet(
                            taobao_handle_url + "?jsonStr=" + jsonObject.toString(), "UTF-8").toString();
                    WriteLog.write("淘宝锁单", trainorder.getId() + "--->" + resultreal);
                    JSONObject resultJsonObjectreal = JSONObject.parseObject(resultreal);
                    if (resultJsonObjectreal.containsKey("success") && resultJsonObjectreal.containsKey("msg")) {
                        resultJsonObject = resultJsonObjectreal;
                        break;
                    }
                    try {
                        Thread.sleep(30000L);
                    }
                    catch (Exception e) {
                    }
                }
                catch (Exception e) {
                    WriteLog.write("error_淘宝锁单", "订单号:" + trainorder.getId() + "--->请求锁单异常");
                    ExceptionUtil.writelogByException("error_淘宝锁单", e);
                }
            }
            success = resultJsonObject.getBooleanValue("success");
            createTrainorderrc(trainorder.getId(), "taobao锁单结束--->" + resultJsonObject.getString("msg"), "taobao锁单",
                    trainorder.getOrderstatus());
        }
        catch (Exception e) {
            WriteLog.write("error_淘宝锁单", "订单号:" + trainorder.getId());
            ExceptionUtil.writelogByException("error_淘宝锁单", e);
        }
        return success;
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId
     *            火车票订单id
     * @param content
     *            内容
     * @param createuser
     *            用户
     * @param status
     *            状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        try {
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(trainorderId);
            rc.setContent(content);
            rc.setStatus(status);
            rc.setCreateuser(createuser);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
        catch (Exception e) {
            WriteLog.write("操作记录失败", trainorderId + ":content:" + content);
        }
    }
}
