package com.ccservice.train.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBUtil;

public class SeachTrainRule {

    /**
     * 如果为空返回"0"
     */
    private String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    /**
     * 根据存储过程获取信息
     */
    @SuppressWarnings("rawtypes")
    private String getSysConfigByProcedure(String name) {
        return TrainCreateOrderDBUtil.getSysconfigString(name);
    }

    /**
     * 获得使用查询接口的查询方的AgentId集合
     */
    public List<String> getSeacherTrain() {
        List<String> agentId = new ArrayList<String>();
        String SearchAgentId = getSysConfigByProcedure("UseHthyTrainSearchAgentId");
        String[] args = SearchAgentId.split(",");
        for (int i = 0; i < args.length; i++) {
            agentId.add(args[i]);
        }
        return agentId;
    }

    /**
     * 判断使用查询接口方价格不符规则
     */
    public boolean isCanGenerate(Trainorder trainorder, int interfacetype) {
        String agentId = String.valueOf(trainorder.getAgentid());
        List<String> agentIdList = getSeacherTrain();
        boolean flag = false;
        for (String agent : agentIdList) {
            if (agent.equalsIgnoreCase(agentId)) {
                flag = true;
                break;
            }
        }
        if (flag) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    if (trainticket.getPrice() == null) {
                        return false;
                    }
                    else if (trainticket.getPrice() != trainticket.getPayprice()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}