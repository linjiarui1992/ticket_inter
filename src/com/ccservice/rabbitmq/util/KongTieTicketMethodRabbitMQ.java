package com.ccservice.rabbitmq.util; 

import javax.jms.JMSException;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.compareprice.PropertyUtil;

/**
 * 
 * ticket 中rabbit 方法放置
 * @author RRRRRR
 * @time 2016年11月18日 下午8:32:18
 */
public class KongTieTicketMethodRabbitMQ {
    
    /**
     * 
     * @author RRRRRR
     * @time 2016年11月18日 下午8:35:18
     * @Description TODO
     * @param url
     * @param queueName
     * @param json
     */
    public void sendMsgRabbitCreateOrderQueueLineWait(String url,String queueName,String json){
        int isRoA=return1O0();
        if(isRoA==1){
            RabbitMQUtil.sendOnemessageV2(json, queueName);
        }else{
            try {
                ActiveMQUtil.sendMessage(url, queueName, json);
            }
            catch (JMSException e) {
                e.printStackTrace();
            } 
        }
        
    }
    
    /**
     * 
     * @author RRRRRR
     * @time 2016年11月18日 下午8:37:13
     * @Description 根据配置文件返回是否是美团
     * @return
     */
    public int return1O0(){
        int isMeiTuanMQ =Integer.valueOf(PropertyUtil.getValue("isMeiTuanMQ",
                "rabbitMQ.properties"));
        return isMeiTuanMQ;
    }
    
}
