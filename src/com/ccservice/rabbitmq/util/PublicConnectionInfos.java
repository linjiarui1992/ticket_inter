package com.ccservice.rabbitmq.util;

/**
 * rabbitMQ   队列用户信息
 * 
 * @author 之至
 * @time 2016年12月20日 下午1:22:17
 */
public class PublicConnectionInfos {
    //消费者维护job 维护数量开始时间
    public static final String STARTTIME = "00:00:00";

    //消费者维护job 维护数量结束时间
    public static final String ENDTIME = "01:00:00";

    //发送短信目标地址
    public static final String SMSURL = "http://203.81.21.34/send/gsend.asp";

    //发送短信用户名
    public static final String SMSNAME = "tongzhi";

    //发送短信密码
    public static final String SMSPWD = "asd123456";

    public static final String USERNAME = "admin";

    //空铁
    public static final String KTHOST = "43.241.233.67";

    public static final String KTPASSWORD = "auUaaWcF";

    //同程
    public static final String TCHOST = "43.241.234.100";

    public static final String TCPASSWORD = "NWsfTYiD";

    //下单消费者队列名称
    public static final String RABBITWAITORDER = "RabbitWaitOrder";
    //下单消费者队列名称[托管专用的]
    public static final String RABBITWAITORDERTuoGuan = "RabbitWaitOrderTuoGuan";

    //下单排队消费者队列名称
    public static final String QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI = "QueueMQ_trainorder_waitorder_orderid_PaiDui";

    //审核消费者队列名称
    public static final String RABBIT_QUEUEMQ_QUERYORDER = "Rabbit_QueueMQ_QueryOrder";

    //扣款消费者队列名称
    public static final String RABBIT_QUEUEMQ_DEDUCTION = "Rabbit_QueueMQ_Deduction";

    //申请改签消费者队列名称
    public static final String CHANGEWAITORDER = "changeWaitOrder";

    //确认改签消费者队列名称
    public static final String CHANGECONFIRMORDER = "changeConfirmOrder";

    //改签审核消费者队列名称
    public static final String CHANGEPAYEXAMINE = "changePayExamine";

    //改签排队消费者队列名称
    public static final String RABBIT_CHANGEORDER_PAIDUI = "Rabbit_ChangeOrder_PaiDui";

    //改签扣款消费者队列名称
    public static final String QUEUEMQ_TRAINORDERDEDUCTIONGQ = "QueueMQ_TrainorderDeductionGq";

    //取消消费者队列名称
    public static final String QUEUEMQ_CANCELORDER = "QueueMQ_CancelOrder";

    //淘宝改签特有消费者队列名称
    public static final String TB_CHANGE_ORDER = "TB_Change_Order";

    //同程退票消费者队列名称
    public static final String QUEUEMQ_TRAINTICKET_REFUNDTICKET = "QueueMQ_TrainTicket_RefundTicket";

    //更新帐号消费者队列名称
    public static final String QUEUEMQ_12306ACCOUNTSYSTEM_EXCUTEUPDATE = "QueueMQ_12306AccountSystem_ExcuteUpdate";

    //各个队列默认消费者数
    public static final int RabbitWaitOrder_V = 70;

    public static final int QueueMQ_trainorder_waitorder_orderid_PaiDui_V = 70;

    public static final int Rabbit_QueueMQ_QueryOrder_V = 70;

    public static final int Rabbit_QueueMQ_Deduction_V = 1;

    public static final int changeWaitOrder_V = 70;

    public static final int changeConfirmOrder_V = 70;

    public static final int changePayExamine_V = 70;

    public static final int Rabbit_ChangeOrder_PaiDui_V = 70;

    public static final int QueueMQ_TrainorderDeductionGq_V = 1;

    public static final int QueueMQ_CancelOrder_V = 50;

    public static final int TB_Change_Order_V = 40;

    public static final int QueueMQ_TrainTicket_RefundTicket_V = 80;

    public static final int QueueMQ_12306AccountSystem_ExcuteUpdate_V = 10;
}
