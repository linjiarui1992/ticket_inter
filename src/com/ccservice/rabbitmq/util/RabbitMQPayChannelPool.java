package com.ccservice.rabbitmq.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.rabbitmq.bean.RabbitMQConnectsBean;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 
 * 美团云mq 连接池
 * @author RRRRRR
 * @time 2016年11月9日 下午2:26:27
 */
public class RabbitMQPayChannelPool {

    private static ConnectionFactory factory;

    private static Connection connection;

    private static RabbitMQPayChannelPool rabbitMQChannelPool = null;

    //创建保存对象的向量 , 初始时有 0 个元素     
    //连接池
    private List<RabbitMQConnectsBean> rmqcbs;

    private int numSize = Integer.valueOf(PropertyUtil.getValue("NUMSIZE_PAY", "rabbitMQ.properties")); // 对象池的大小     

    private int maxSize = Integer.valueOf(PropertyUtil.getValue("MAXSIZE_PAY", "rabbitMQ.properties")); // 对象池最大的大小     

    private String host = PropertyUtil.getValue("HOST_PAY", "rabbitMQ.properties");//ip地址

    private String userName = PropertyUtil.getValue("USERNAME_PAY", "rabbitMQ.properties");//用户名

    private String passWord = PropertyUtil.getValue("PASSWORD_PAY", "rabbitMQ.properties");//密码

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月11日 上午11:08:14
     * @Description 创建本类的唯一对象
     * @return
     */
    public static synchronized RabbitMQPayChannelPool getinstance() {
        if (rabbitMQChannelPool == null) {
            synchronized (RabbitMQPayChannelPool.class) {
                if (rabbitMQChannelPool == null) {
                    rabbitMQChannelPool = new RabbitMQPayChannelPool();
                }
            }

        }
        return rabbitMQChannelPool;
    }

    private RabbitMQPayChannelPool() {
        createMQChannelPool();
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月11日 上午11:09:52
     * @Description 创建一个对象池
     */
    public void createMQChannelPool() {
        if (rmqcbs == null) {
            rmqcbs = new ArrayList<RabbitMQConnectsBean>();
        }
        for (int i = 0; i < numSize; i++) {
            RabbitMQConnectsBean rmcb = createRabbitBean();
            rmqcbs.add(rmcb);
            System.out.println(rmqcbs.size());
        }
        System.out.println("===" + numSize);
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月11日 下午5:59:28
     * @Description 新建一个连接
     * @param host
     * @param userName
     * @param passWord
     * @return
     */
    private static Connection createConnecttion(String host, String userName, String passWord) {
        try {
            factory = new ConnectionFactory();
            factory.setHost(host);
            factory.setUsername(userName);
            factory.setPassword(passWord);
            // 创建一个新的消息队列服务器实体的连接
            try {
                connection = factory.newConnection();
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("RabbitMQPayChannelPool_createConnecttion_Exception", e, host
                        + "--->" + userName + "--->" + passWord);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月15日 下午5:58:15
     * @Description 创建一个可用的连接对象
     * @return
     */
    private RabbitMQConnectsBean createRabbitBean() {
        RabbitMQConnectsBean rmmb = new RabbitMQConnectsBean();
        try {
            rmmb.setConnection(createConnecttion(host, userName, passWord));
            rmmb.setKey(UUID.randomUUID().toString());
            try {
                rmmb.setChannel(rmmb.getConnection().createChannel());
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("RabbitMQPayChannelPool_createRabbitBean_Exception", e, host + "--->"
                        + userName + "--->" + passWord);
            }
            rmmb.setUse(false);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQPayChannelPool_createRabbitBean_Exception", e, host + "--->"
                    + userName + "--->" + passWord);
        }
        return rmmb;
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月15日 下午5:59:19
     * @Description 新创建一个可用对象 并返回
     * @return
     */
    public RabbitMQConnectsBean createNewRabbitMQConnectsBean() {
        RabbitMQConnectsBean rmmb = new RabbitMQConnectsBean();
        if (rmqcbs.size() < maxSize) {
            rmmb = createRabbitBean();
            rmqcbs.add(rmmb);
        }
        else {
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
            }
            try {
                rmmb = RabbitMQUtil.getOneRabbitMQConnectsBean();
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("RabbitMQPayChannelPool_createNewRabbitMQConnectsBean_Exception", e,
                        "获得可用连接出错---->" + (rmmb != null ? rmmb.isUse() : "null"));
            }
        }
        return rmmb;
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月15日 下午5:49:43
     * @Description 归还连接
     * @param rmb
     */
    public void returnRabbitMQMethod(RabbitMQConnectsBean rmb) {
        try {
            if (rmb != null && rmqcbs.size() > 0) {
                for (int i = 0; i < rmqcbs.size(); i++) {
                    if (rmb.getKey().equals(rmqcbs.get(i).getKey())) {
                        rmqcbs.get(i).setUse(false);
                    }
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQPayChannelPool_returnRabbitMQMethod_Exception", e,
                    "连接重置空闲出错---->" + (rmb != null ?  rmb.isUse() : "null"));
        }
    }

    public List<RabbitMQConnectsBean> getRmqcbs() {
        return rmqcbs;
    }

    public void setRmqcbs(List<RabbitMQConnectsBean> rmqcbs) {
        this.rmqcbs = rmqcbs;
    }

}
