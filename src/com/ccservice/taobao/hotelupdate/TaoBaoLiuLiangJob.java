package com.ccservice.taobao.hotelupdate;

import java.io.File;
import java.util.List;
import java.util.Calendar;
import java.io.PrintWriter;
import java.io.FileOutputStream;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.taobaoagent.TaoBaoAgent;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 淘宝酒店买家流量统计、重置
 */
public class TaoBaoLiuLiangJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("开始=====统计上一天淘宝酒店买家流量。");
        countLiuLiang();
        System.out.println("结束=====统计上一天淘宝酒店买家流量。");

        System.out.println("开始=====重置当天淘宝酒店买家流量为零。");
        resetLiuLiang();
        System.out.println("结束=====重置当天淘宝酒店买家流量为零。");
    }

    public static void main(String[] args) {
        System.out.println("开始=====统计上一天淘宝酒店买家流量。");
        new TaoBaoLiuLiangJob().countLiuLiang();
        System.out.println("结束=====统计上一天淘宝酒店买家流量。");

        System.out.println("开始=====重置当天淘宝酒店买家流量为零。");
        new TaoBaoLiuLiangJob().resetLiuLiang();
        System.out.println("结束=====重置当天淘宝酒店买家流量为零。");
    }

    //每天流量统计、每个买家一个文件
    public void countLiuLiang() {
        List<TaoBaoAgent> list = Server.getInstance().getHotelService().findAllTaoBaoAgent("", "", -1, 0);
        for (TaoBaoAgent t : list) {
            long agentid = t.getAgentid() == null ? 0 : t.getAgentid().longValue();
            long liulinag = t.getLiuliang() == null ? 0 : t.getLiuliang().longValue();
            write(agentid, liulinag);
        }
    }

    //重置当天流量
    public void resetLiuLiang() {
        String sql = "update T_TAOBAOAGENT set C_LIULIANG = 0";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }

    private void write(long agentid, long liuliang) {
        try {
            Calendar cd = Calendar.getInstance();
            int year = cd.get(Calendar.YEAR);
            String month = addZero(cd.get(Calendar.MONTH) + 1);
            String day = addZero(cd.get(Calendar.DAY_OF_MONTH));
            String hour = addZero(cd.get(Calendar.HOUR_OF_DAY));
            String min = addZero(cd.get(Calendar.MINUTE));
            String sec = addZero(cd.get(Calendar.SECOND));
            //文件路径
            String filePath = "D:/hotellog/TaoBaoLiuLiang/" + year;
            File dir = new File(filePath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String logname = filePath + "/" + agentid + ".log";
            //紧接文件尾写入日志字符串
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(logname, true));
            String content = "统计时间：" + year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "，流量："
                    + liuliang + "次。";
            printWriter.println(content);
            printWriter.flush();
            printWriter.close();
        }
        catch (Exception e) {
        }
    }

    private String addZero(int i) {
        if (i < 10) {
            return "0" + i;
        }
        else {
            return String.valueOf(i);
        }
    }
}
