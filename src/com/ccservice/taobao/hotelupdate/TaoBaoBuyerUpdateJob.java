package com.ccservice.taobao.hotelupdate;

import java.util.*;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccervice.huamin.update.PHUtil;
import com.ccervice.huamin.update.WriteLog;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.taobaoroomtype.TaoBaoRoomType;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.hpricecontrolrecord.HpriceControlRecord;
import com.ccservice.b2b2c.base.hotelpricecontroltype.HotelPriceControlType;

/**
 * 淘宝酒店买家价格、房量更新
 * 单次最多30个
 */
public class TaoBaoBuyerUpdateJob implements Job {

    private int maxsize = 1;//淘宝一次最多30个商品更新、暂时固定为1、即时更新

    private SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println(formatTime.format(new Date()) + "---开始更新---淘宝买家---持续更新酒店价格、房量Job...");
        updateChange();
        System.out.println(formatTime.format(new Date()) + "---结束更新---淘宝买家---持续更新酒店价格、房量Job...");
    }

    public static void main(String[] args) {
        System.out.println("开始更新---淘宝买家---持续更新酒店价格、房量Job...");
        new TaoBaoBuyerUpdateJob().updateChange();
        System.out.println("结束更新---淘宝买家---持续更新酒店价格、房量Job...");
    }

    //价格、房量更新
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void updateChange() {
        String propertyInterval = PropertyUtil.getValue("TaoBaoBuyerUpdateInterval");
        if (ElongHotelInterfaceUtil.StringIsNull(propertyInterval)) {
            propertyInterval = "3600";//一小时
        }
        long TaoBaoBuyerUpdateInterval = Long.parseLong(propertyInterval.trim()) * 1000;//更新间隔
        Map<Long, Hotel> hotelMap = new HashMap<Long, Hotel>();
        while (true) {
            long start = System.currentTimeMillis();//更新开始时间
            try {
                //淘宝买家
                Map<Long, String> map = new HashMap<Long, String>();//AgentId,SessionKey
                loadbuyer(map, 0);
                if (map.size() == 0) {
                    throw new Exception("无卖家，中断更新。");
                }
                String agentids = map.keySet().toString().substring(1);
                agentids = agentids.substring(0, agentids.length() - 1);
                //淘宝房型套餐
                List list = loadTbRoom(agentids);
                if (list == null || list.size() == 0) {
                    throw new Exception("无符合条件商品。");
                }
                //日期
                String currentDate = ElongHotelInterfaceUtil.getCurrentDate();
                //淘宝可发布90天
                String addDate = ElongHotelInterfaceUtil.getAddDate(currentDate, 90);
                //Map，取值用
                Map<Long, Map<Long, List<Hmhotelprice>>> maps = new HashMap<Long, Map<Long, List<Hmhotelprice>>>();//key：AgentId
                Map<Long, Long> gidmap = new HashMap<Long, Long>();//key：gid；value：TaoBaoRoomTableId
                Map<Long, Long> timemap = new HashMap<Long, Long>();//key：gid；value：ModifyTime
                //加价、留点用，避免多次查询
                Map<Long, String> hotelModeMap = new HashMap<Long, String>();//AgentId,HotelMode
                Map<Long, List> liudianInfoMap = new HashMap<Long, List>();//AgentId,LiudianInfo
                Map<Long, Map> jiaJiaMap = new HashMap<Long, Map>();//AgentId,JiaJia
                //流量用完代理，忽略更新期间的数据更新(如修改流量等)
                List<Long> LiuLiangOver = new ArrayList<Long>();
                //循环房型套餐
                for (int i = 0; i < list.size(); i++) {
                    try {
                        Map m = (Map) list.get(i);
                        long hotelid = Long.parseLong(m.get("hotelid").toString());
                        long roomid = Long.parseLong(m.get("roomid").toString());
                        String prod = m.get("prod").toString();
                        long bf = Long.parseLong(m.get("bf").toString());
                        //同一房型套餐下的淘宝房型
                        List tbrs = loadTbDetail(agentids, hotelid, roomid, prod, bf);
                        if (tbrs == null || tbrs.size() == 0) {
                            //可能性小
                            throw new Exception("查询淘宝套餐失败，" + hotelid + " / " + roomid + " / " + prod + " / " + bf);
                        }
                        //价格
                        String where = "where C_PRICE > 0 and C_HOTELID = " + hotelid + " and C_ROOMTYPEID = " + roomid
                                + " and C_STATEDATE >= '" + currentDate + "' and C_STATEDATE <= '" + addDate
                                + "' and C_PROD = '" + prod + "' and C_BF = " + bf;
                        List<Hmhotelprice> prices = Server.getInstance().getHotelService()
                                .findAllHmhotelprice(where, "order by C_STATEDATE", -1, 0);
                        //无价格
                        if (prices == null || prices.size() == 0) {
                            //查询是否有其他套餐价格，有价格换套餐，无价格下架商品
                            String pricesql = "select top 1 C_PROD, C_BF from T_HMHOTELPRICE"
                                    + " where C_PRICE > 0 and C_HOTELID = " + hotelid + " and C_ROOMTYPEID = " + roomid
                                    + " and C_STATEDATE >= '" + currentDate + "' and C_PROD > 0 and C_BF >= 0";
                            List pricelist = Server.getInstance().getSystemService().findMapResultBySql(pricesql, null);
                            if (pricelist != null && pricelist.size() > 0) {
                                Map priceMap = (Map) pricelist.get(0);
                                String newprod = priceMap.get("C_PROD").toString();
                                long newbf = Long.parseLong(priceMap.get("C_BF").toString());
                                //重新查价格
                                where = "where C_PRICE > 0 and C_HOTELID = " + hotelid + " and C_ROOMTYPEID = "
                                        + roomid + " and C_STATEDATE >= '" + currentDate + "' and C_STATEDATE <= '"
                                        + addDate + "' and C_PROD = '" + newprod + "' and C_BF = " + newbf;
                                prices = Server.getInstance().getHotelService()
                                        .findAllHmhotelprice(where, "order by C_STATEDATE", -1, 0);
                                //套餐替换
                                if (prices != null && prices.size() > 0) {
                                    String replaceSql = "update r set r.C_LOCALPROD = '" + newprod
                                            + "', r.C_LOCALBF = " + newbf + " from T_TAOBAOROOMTYPE r, T_TAOBAOHOTEL h"
                                            + " where r.C_TAOBAOTABLEID = h.ID and h.C_NATIVEHOTELID = " + hotelid
                                            + " and r.C_NATIVEROOMTYPEID = " + roomid + " and r.C_LOCALPROD = '" + prod
                                            + "' and r.C_LOCALBF = " + bf;
                                    Server.getInstance().getSystemService().findMapResultBySql(replaceSql, null);
                                    //重新加载同一房型套餐下的淘宝房型
                                    prod = newprod;
                                    bf = newbf;
                                    tbrs = loadTbDetail(agentids, hotelid, roomid, prod, bf);
                                    System.out.println("替换商品套餐：" + hotelid + " / " + roomid);
                                    if (tbrs == null || tbrs.size() == 0) {
                                        //可能性小
                                        throw new Exception("查询淘宝套餐失败，" + hotelid + " / " + roomid + " / " + prod
                                                + " / " + bf);
                                    }
                                }
                            }
                        }
                        boolean haveprice = prices != null && prices.size() > 0 ? true : false;
                        long PriceTime = 0l;
                        if (haveprice) {
                            Map<Long, List<Hmhotelprice>> tempMap = loadPriceTime(prices);
                            for (long key : tempMap.keySet()) {
                                PriceTime = key;
                                prices = tempMap.get(key);
                            }
                            if (PriceTime == 0) {
                                continue;
                            }
                        }
                        for (int j = 0; j < tbrs.size(); j++) {
                            try {
                                Map tbr = (Map) tbrs.get(j);
                                long agentid = Long.parseLong(tbr.get("agentid").toString());
                                //流量用完
                                if (LiuLiangOver.contains(agentid)) {
                                    continue;
                                }
                                if (!checkLiuLiang(agentid)) {
                                    LiuLiangOver.add(agentid);
                                    continue;
                                }
                                long gid = Long.parseLong(tbr.get("gid").toString());
                                long trid = Long.parseLong(tbr.get("trid").toString());
                                long state = Long.parseLong(tbr.get("state").toString());//商品状态
                                double addprice = tbr.get("addprice") == null
                                        || ElongHotelInterfaceUtil.StringIsNull(tbr.get("addprice").toString()) ? 0d
                                        : Double.parseDouble(tbr.get("addprice").toString());
                                //无价格，商品下架
                                if (!haveprice) {
                                    //非下架
                                    if (state != 2) {
                                        productDown(trid, gid, map.get(agentid), agentid);
                                    }
                                    else {
                                        System.out.println("无价格，商品已下架，无需请求淘宝：" + agentid + " / " + trid + " / " + gid);
                                    }
                                    continue;
                                }
                                //最后更新时间
                                long TbrTime = loadTbrTime(tbr);
                                //比较
                                if (PriceTime > TbrTime) {
                                    System.out.println("=====淘宝酒店买家价格、房量更新=====agentid：" + agentid + "---hotelid ："
                                            + hotelid + "---roomid ：" + roomid + "---prod ：" + prod + "---bf ：" + bf
                                            + "---gid：" + gid);
                                    //加价
                                    List<Hmhotelprice> newPrices = new ArrayList<Hmhotelprice>();
                                    for (Hmhotelprice p : prices) {
                                        //酒店模式
                                        if (!hotelModeMap.containsKey(agentid)) {
                                            hotelModeMap.put(agentid, loadHotelData(agentid));
                                        }
                                        String hotelmode = hotelModeMap.get(agentid); //1：留点；2：加价
                                        //加价
                                        if (!liudianInfoMap.containsKey(agentid)) {
                                            liudianInfoMap.put(agentid, getLiudianInfo(2l, agentid));
                                        }
                                        List liudianinfo = liudianInfoMap.get(agentid);
                                        //留点
                                        if (!jiaJiaMap.containsKey(agentid)) {
                                            jiaJiaMap.put(agentid, getJiaJia(agentid, hotelmode));
                                        }
                                        Map jiajia = jiaJiaMap.get(agentid);
                                        //星级
                                        Integer star = 0;
                                        if (!"2".equals(hotelmode)) {
                                            if (!hotelMap.containsKey(hotelid)) {
                                                Hotel localHotel = Server.getInstance().getHotelService()
                                                        .findHotel(hotelid);
                                                hotelMap.put(hotelid, localHotel);
                                            }
                                            star = hotelMap.get(hotelid).getStar();
                                        }
                                        //计算价格
                                        double price = getPriceFanInfo(p.getPrice().doubleValue(), star, liudianinfo,
                                                jiajia, agentid, hotelmode);
                                        //加价
                                        if (addprice > 0) {
                                            //0：不加价；1：固定值；2：比例
                                            String mode = String.valueOf(addprice).substring(0, 1);
                                            //实际加价
                                            double realAddprice = Double.parseDouble(String.valueOf(addprice)
                                                    .substring(1));
                                            if ("1".equals(mode)) {
                                                price = ElongHotelInterfaceUtil.add(price, realAddprice);
                                            }
                                            else if ("2".equals(mode)) {
                                                Double tempprice = ElongHotelInterfaceUtil.multiply(price,
                                                        realAddprice * 0.01);
                                                price = ElongHotelInterfaceUtil.add(price, tempprice.intValue());
                                            }
                                        }
                                        p.setPrice(price);
                                        newPrices.add(p);
                                    }
                                    Map<Long, List<Hmhotelprice>> prodPrices = new HashMap<Long, List<Hmhotelprice>>();//key：GID
                                    if (maps.containsKey(agentid)) {
                                        prodPrices = maps.get(agentid);
                                    }
                                    prodPrices.put(gid, newPrices);
                                    maps.put(agentid, prodPrices);
                                    gidmap.put(gid, trid);
                                    timemap.put(gid, PriceTime);
                                    //请求
                                    if (prodPrices.size() >= maxsize) {
                                        reqtaobao(map, maps, gidmap, timemap);
                                    }
                                }
                                else {
                                    System.out.println("价格无变化，不更新：" + agentid + " / " + trid + " / " + gid);
                                }
                            }
                            catch (Exception e) {
                                String msg = ElongHotelInterfaceUtil.StringIsNull(e.getMessage()) ? "出现异常。" : e
                                        .getMessage().trim();
                                StackTraceElement stack = e.getStackTrace()[0];
                                WriteLog.write("淘宝酒店买家价格、房量更新",
                                        "异常类：" + stack.getFileName() + " ；方法： " + stack.getMethodName() + " ；行数： "
                                                + stack.getLineNumber() + "；错误信息：" + msg);
                            }
                        }
                    }
                    catch (Exception e) {
                        String msg = ElongHotelInterfaceUtil.StringIsNull(e.getMessage()) ? "出现异常。" : e.getMessage()
                                .trim();
                        StackTraceElement stack = e.getStackTrace()[0];
                        WriteLog.write("淘宝酒店买家价格、房量更新", "异常类：" + stack.getFileName() + " ；方法： " + stack.getMethodName()
                                + " ；行数： " + stack.getLineNumber() + "；错误信息：" + msg);
                    }
                }
                if (maps.size() > 0) {
                    reqtaobao(map, maps, gidmap, timemap);
                }
            }
            catch (Exception e) {
                String msg = ElongHotelInterfaceUtil.StringIsNull(e.getMessage()) ? "出现异常。" : e.getMessage().trim();
                StackTraceElement stack = e.getStackTrace()[0];
                WriteLog.write("淘宝酒店买家价格、房量更新", "异常类：" + stack.getFileName() + " ；方法： " + stack.getMethodName()
                        + " ；行数： " + stack.getLineNumber() + "；错误信息：" + msg);
                System.out.println("淘宝酒店买家价格、房量更新抛异常，异常信息为：" + msg);
            }
            finally {
                long end = System.currentTimeMillis();//更新结束时间
                long interval = end - start;
                if (interval < TaoBaoBuyerUpdateInterval) {
                    try {
                        long longtime = TaoBaoBuyerUpdateInterval - interval;
                        System.out.println("当前更新完毕，休息" + (longtime / 1000) + "秒。");
                        Thread.sleep(longtime);
                    }
                    catch (Exception e) {
                    }
                }
            }
        }
    }

    //验证流量
    private boolean checkLiuLiang(long agentid) {
        Map<Long, String> map = new HashMap<Long, String>();
        loadbuyer(map, agentid);
        if (map.size() == 0) {
            return false;
        }
        else {
            return true;
        }
    }

    //下架操作
    private void productDown(long trid, long gid, String sessionkey, long agentid) {
        try {
            String json = Server.getInstance().getTaoBaoHotelSerice().productUpdate(gid, null, null, 2l, sessionkey);//2：下架
            JSONObject obj = JSONObject.fromObject(json);
            if (obj.containsKey("hotel_room_update_response")) {
                obj = obj.getJSONObject("hotel_room_update_response");
                obj = obj.getJSONObject("room");
                long status = obj.getLong("status");
                long iid = obj.getLong("iid");
                if (status == 2) {
                    String sql = "update T_TAOBAOROOMTYPE set C_GOODSSTATUS = 2 where ID = " + trid + " and C_IID = "
                            + iid;
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    System.out.println("无价格，商品下架：" + agentid + " / " + trid + " / " + gid);
                }
            }
        }
        catch (Exception e) {
        }
    }

    //请求淘宝
    private void reqtaobao(Map<Long, String> map, Map<Long, Map<Long, List<Hmhotelprice>>> maps,
            Map<Long, Long> gidmap, Map<Long, Long> timemap) {
        for (long agentid : map.keySet()) {
            try {
                Map<Long, List<Hmhotelprice>> prodPrices = maps.get(agentid);
                if (prodPrices != null && prodPrices.size() > 0) {
                    String sessionkey = map.get(agentid);
                    //request
                    String json = Server.getInstance().getTaoBaoHotelSerice()
                            .productBatchUpdate(prodPrices, sessionkey);
                    //{"hotel_rooms_update_response":{"gids":{"string":["6582137","6582139","6582138","6582141","6582140","6582142"]}}}
                    if (!ElongHotelInterfaceUtil.StringIsNull(json) && json.contains("hotel_rooms_update_response")) {
                        JSONObject obj = JSONObject.fromObject(json);
                        if (obj.containsKey("hotel_rooms_update_response")) {
                            obj = obj.getJSONObject("hotel_rooms_update_response");
                            if (obj.containsKey("gids")) {
                                obj = obj.getJSONObject("gids");
                                if (obj.containsKey("string")) {
                                    JSONArray array = obj.getJSONArray("string");
                                    if (array != null && array.size() > 0) {
                                        for (int m = 0; m < array.size(); m++) {
                                            try {
                                                long gid = array.getLong(m);
                                                long trid = gidmap.get(gid);
                                                TaoBaoRoomType tbr = new TaoBaoRoomType();
                                                tbr.setId(trid);
                                                tbr.setGoodsmodifytime(new Timestamp(timemap.get(gid)));
                                                Server.getInstance().getHotelService()
                                                        .updateTaoBaoRoomTypeIgnoreNull(tbr);
                                                gidmap.remove(gid);
                                                timemap.remove(gid);
                                                System.out.println("AgentId：" + agentid + "---Gid：" + gid + "更新成功...");
                                            }
                                            catch (Exception e) {
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (gidmap.size() > 0) {
                            Map<Long, Long> failureGidMap = gidmap;
                            for (long gid : failureGidMap.keySet()) {
                                gidmap.remove(gid);
                                timemap.remove(gid);
                                System.out.println("AgentId：" + agentid + "---Gid：" + gid + "更新失败...");
                            }
                        }
                    }
                    else {
                        System.out.println("淘宝酒店买家价格、房量更新失败---AgentId：" + agentid + "---TaoBaoReturn：" + json);
                    }
                }
            }
            catch (Exception e) {
                String msg = ElongHotelInterfaceUtil.StringIsNull(e.getMessage()) ? "出现异常。" : e.getMessage().trim();
                StackTraceElement stack = e.getStackTrace()[0];
                WriteLog.write("淘宝酒店买家价格、房量更新", "异常类：" + stack.getFileName() + " ；方法： " + stack.getMethodName()
                        + " ；行数： " + stack.getLineNumber() + "；错误信息：" + msg);
            }
            finally {
                if (maps.containsKey(agentid)) {
                    maps.remove(agentid);
                }
            }
        }
    }

    /**
     *需更新的房型套餐
     *酒店、房型、商品非停售
     *@return hotelid、roomid、prod、bf
     */
    private List loadTbRoom(String agentids) {
        String sql = "select distinct th.C_NATIVEHOTELID hotelid, tr.C_NATIVEROOMTYPEID roomid, tr.C_LOCALPROD prod, tr.C_LOCALBF bf "
                + "from T_TAOBAOHOTEL th "
                + "join T_TAOBAOROOMTYPE tr on tr.C_TAOBAOTABLEID = th.ID "
                + "where th.C_AGENTID in ("
                + agentids
                + ") and th.C_NATIVEHOTELID > 0 and th.C_HID > 0 and th.C_STATUS = 1 "
                + "and tr.C_NATIVEROOMTYPEID > 0 and tr.C_RID > 0 and tr.C_ROOMTYPESTATUS = 1 "
                + "and tr.C_GID > 0 and tr.C_GOODSSTATUS in (1,2) and tr.C_LOCALPROD > 0 and tr.C_LOCALBF >= 0";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        return list;
    }

    /**
     * 淘宝房型套餐详情
     * @return agentid、trid 淘宝房型ID、gid 淘宝商品ID、modifytime 修改时间
     */
    private List loadTbDetail(String agentids, long hotelid, long roomid, String prod, long bf) {
        String sql = "select th.C_AGENTID agentid, tr.ID trid, tr.C_GID gid, tr.C_GOODSSTATUS state, tr.C_ADDPRICE addprice, tr.C_GOODSMODIFYTIME modifytime "
                + "from T_TAOBAOHOTEL th "
                + "join T_TAOBAOROOMTYPE tr on tr.C_TAOBAOTABLEID = th.ID where th.C_AGENTID in ("
                + agentids
                + ") and th.C_NATIVEHOTELID = "
                + hotelid
                + " and th.C_HID > 0 and th.C_STATUS = 1 "
                + "and tr.C_NATIVEROOMTYPEID = "
                + roomid
                + " and tr.C_RID > 0 and tr.C_ROOMTYPESTATUS = 1 "
                + "and tr.C_GID > 0 and tr.C_GOODSSTATUS in (1,2) and tr.C_LOCALPROD = '"
                + prod
                + "' and tr.C_LOCALBF = " + bf;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        return list;
    }

    private Map<Long, List<Hmhotelprice>> loadPriceTime(List<Hmhotelprice> prices) throws Exception {
        //第一个价格
        Hmhotelprice firstPrice = prices.get(0);
        //最后有价格日期
        String lastDate = "";
        //某天重复，后续不传
        String currentDate = ElongHotelInterfaceUtil.getCurrentDate();
        Map<String, Hmhotelprice> tempMap = new LinkedHashMap<String, Hmhotelprice>();//key：日期
        for (Hmhotelprice p : prices) {
            String key = p.getStatedate();
            //商品价格重复、重复的关房
            if (tempMap.containsKey(key)) {
                Hmhotelprice repeat = tempMap.get(key);//第一个重复数据
                repeat.setRoomstatus(1l);//关房
                tempMap.put(key, repeat);//覆盖第一个重复
                break;
            }
            lastDate = key;
            tempMap.put(key, p);
        }
        //取连续价格
        List<Hmhotelprice> templist = new ArrayList<Hmhotelprice>();
        //有价格天数
        int days = ElongHotelInterfaceUtil.getSubDays(currentDate, lastDate) + 1;
        for (int i = 0; i < days; i++) {
            String current = ElongHotelInterfaceUtil.getAddDate(currentDate, i);
            //存在价格
            if (tempMap.containsKey(current)) {
                templist.add(tempMap.get(current));
            }
            //不存在价格
            else {
                Hmhotelprice temp = copyprice(firstPrice);
                temp.setRoomstatus(1l);//关房
                temp.setStatedate(current);
                templist.add(temp);
            }
        }
        if (templist.size() == 0) {
            Hmhotelprice temp = copyprice(firstPrice);
            temp.setRoomstatus(1l);//关房
            temp.setStatedate(currentDate);
            templist.add(temp);
        }
        if (templist.size() == 1) {
            Hmhotelprice temp = copyprice(firstPrice);
            temp.setRoomstatus(1l);//关房
            temp.setStatedate(ElongHotelInterfaceUtil.getAddDate(currentDate, 1));
            templist.add(temp);
        }
        prices = templist;
        //时间
        long time = 0l;
        for (Hmhotelprice p : prices) {
            String updatetime = p.getUpdatetime();
            if (!ElongHotelInterfaceUtil.StringIsNull(updatetime)) {
                long t = formatTime.parse(updatetime).getTime();
                if (t > time) {
                    time = t;
                }
            }
        }
        Map<Long, List<Hmhotelprice>> map = new HashMap<Long, List<Hmhotelprice>>();
        map.put(time, prices);
        return map;
    }

    private Hmhotelprice copyprice(Hmhotelprice old) {
        Hmhotelprice temp = new Hmhotelprice();
        temp.setAbleornot(old.getAbleornot());
        temp.setAdvancedday(old.getAdvancedday());
        temp.setAllot(old.getAllot());
        temp.setAllotmenttype(old.getAllotmenttype());
        temp.setBf(old.getBf());
        temp.setBreakfasttype(old.getBreakfasttype());
        temp.setCanceladvance(old.getCanceladvance());
        temp.setCanceldesc(old.getCanceldesc());
        temp.setCanceltime(old.getCanceltime());
        temp.setCanceltype(old.getCanceltype());
        temp.setCheckflag(old.getCheckflag());
        temp.setCityid(old.getCityid());
        temp.setContractid(old.getContractid());
        temp.setContractver(old.getContractver());
        temp.setCountryid(old.getCountryid());
        temp.setCountryname(old.getCountryname());
        temp.setCur(old.getCur());
        temp.setDeadline(old.getDeadline());
        temp.setGuamoneytype(old.getGuamoneytype());
        temp.setHotelid(old.getHotelid());
        temp.setId(old.getId());
        temp.setIsallot(old.getIsallot());
        temp.setJlkeyid(old.getJlkeyid());
        temp.setJltime(old.getJltime());
        temp.setMaxday(old.getMaxday());
        temp.setMinday(old.getMinday());
        temp.setNoeditcancel(old.getNoeditcancel());
        temp.setNoedittype(old.getNoedittype());
        temp.setPaytype(old.getPaytype());
        temp.setPrice(old.getPrice());
        temp.setPriceoffer(old.getPriceoffer());
        temp.setProd(old.getProd());
        temp.setQunarprice(old.getQunarprice());
        temp.setRatetype(old.getRatetype());
        temp.setRoomstatus(old.getRoomstatus());
        temp.setRoomtypeid(old.getRoomtypeid());
        temp.setServ(old.getServ());
        temp.setSourcetype(old.getSourcetype());
        temp.setStatedate(old.getStatedate());
        temp.setSupplyid(old.getSupplyid());
        temp.setTicket(old.getTicket());
        temp.setType(old.getType());
        temp.setUpdatetime(old.getUpdatetime());
        temp.setYuliuNum(old.getYuliuNum());
        temp.setZshotelid(old.getZshotelid());
        temp.setZsroomtypeid(old.getZsroomtypeid());
        return temp;
    }

    private long loadTbrTime(Map tbr) throws Exception {
        long time = 0l;
        if (tbr.get("modifytime") != null) {
            String t = tbr.get("modifytime").toString();
            time = Timestamp.valueOf(t).getTime();
        }
        return time;
    }

    //淘宝买家，checkAgentId：大于0时，用于验证代理流量
    private void loadbuyer(Map<Long, String> map, long checkAgentId) {
        String sql = "select t.C_AGENTID agentid, t.C_SESSIONKEY sessionkey, t.C_SESSIONKEYMODIFYTIME modifytime, t.C_SESSIONKEYACTIVETIME activetime "
                + "from T_TAOBAOAGENT t join T_CUSTOMERAGENT c on c.ID = t.C_AGENTID "
                + "where (c.C_AGENTISENABLE is null or c.C_AGENTISENABLE = 1) and (C_AGENTVEDATE is null or C_AGENTVEDATE > CONVERT(varchar(100), GETDATE(), 20)) "
                + "and t.C_TAOBAOUSERNAME !='' and t.C_SESSIONKEY !='' and (t.C_TOTALLIULIANG = -1 or t.C_TOTALLIULIANG > t.C_LIULIANG)";
        if (checkAgentId > 0) {
            sql += " and c.ID = " + checkAgentId;
        }
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map m = (Map) list.get(i);
                if (m.get("agentid") == null || m.get("sessionkey") == null || m.get("modifytime") == null
                        || m.get("activetime") == null) {
                    continue;
                }
                String agentid = m.get("agentid").toString();
                String sessionkey = m.get("sessionkey").toString();
                String modifytime = m.get("modifytime").toString();
                String activetime = m.get("activetime").toString();
                if (ElongHotelInterfaceUtil.StringIsNull(agentid) || ElongHotelInterfaceUtil.StringIsNull(sessionkey)
                        || ElongHotelInterfaceUtil.StringIsNull(modifytime)
                        || ElongHotelInterfaceUtil.StringIsNull(activetime)) {
                    continue;
                }
                try {
                    Timestamp sessionkeymodifytime = Timestamp.valueOf(modifytime);
                    long start = sessionkeymodifytime.getTime();
                    long end = System.currentTimeMillis();
                    long result = (end - start) / 1000;
                    if (result > Long.valueOf(activetime).longValue()) {
                        throw new Exception("SessionKey过期!");
                    }
                }
                catch (Exception e) {
                    continue;
                }
                map.put(Long.parseLong(agentid), sessionkey);
            }
        }
    }

    /**
     * 加价留点
     */
    private double getPriceFanInfo(Double suppliersellprice, Integer star, List localliudian, Map jiajia, long agentId,
            String hotelmode) throws Exception {
        double price = 0d;
        try {
            if (suppliersellprice == null || suppliersellprice.intValue() <= 0) {
                throw new Exception("获取价格失败。");
            }
            star = star == null ? 0 : star;
            //加价
            if ("2".equals(hotelmode)) {
                long tempagent = 0;
                if (localliudian != null && localliudian.size() > 0) {
                    for (int i = 0; i < localliudian.size(); i++) {
                        Map m = (Map) localliudian.get(i);
                        Long agentid = Long.parseLong(m.get("agentid").toString());
                        if (tempagent == agentid) {
                            continue;
                        }
                        Float start = Float.parseFloat(m.get("start").toString());
                        Float end = Float.parseFloat(m.get("endt").toString());
                        Float liudian = Float.parseFloat(m.get("liudian").toString());
                        if (start <= suppliersellprice && suppliersellprice < end) {
                            tempagent = agentid;
                            suppliersellprice = ElongHotelInterfaceUtil.add(suppliersellprice, liudian);
                        }
                    }
                }
                price = suppliersellprice;
            }
            //留点
            else {
                Long jiajiatype = Long.valueOf(jiajia.get("type").toString());
                if (jiajiatype.longValue() == 1) {//星级比例
                    Double addallprice = 0d;
                    Double fandian = 0.0d;
                    Double totaldian = 0.0d;
                    Double lirun = 0.0d;
                    List record = (List) jiajia.get("data");
                    if (record != null && record.size() > 0) {
                        for (int i = 0; i < record.size(); i++) {
                            Map<String, Float> jiajiat = (Map) record.get(i);
                            Float start = jiajiat.get("start");
                            Float addprice = jiajiat.get("addprice");
                            if (start.intValue() == star) {
                                addallprice = suppliersellprice + (int) (suppliersellprice * addprice / 100);
                                fandian = (suppliersellprice * addprice / 100) / Math.ceil(addallprice);
                                break;
                            }
                        }
                    }
                    fandian = fandian * 100;
                    totaldian = fandian;
                    long tempagent = 0;
                    Double sellprice = suppliersellprice;
                    if (localliudian != null && localliudian.size() > 0) {
                        for (int i = 0; i < localliudian.size(); i++) {
                            Map lliudian = (Map) localliudian.get(i);
                            Long agentid = Long.valueOf(lliudian.get("agentid").toString());
                            Long liudianagent = Long.parseLong(lliudian.get("liudianagent").toString());
                            if (tempagent == agentid) {
                                continue;
                            }
                            Float start = Float.valueOf(lliudian.get("start").toString());
                            Float end = Float.valueOf(lliudian.get("endt").toString());
                            Float liudian = Float.valueOf(lliudian.get("liudian").toString());
                            if (start <= fandian && fandian < end) {
                                tempagent = agentid;
                                if (liudian >= fandian) {
                                    liudian = fandian.floatValue();
                                    fandian = 0d;
                                }
                                else {
                                    fandian -= liudian;
                                }
                            }
                        }
                    }
                    if (agentId != 1) {
                        lirun = Math.floor(addallprice * Double.valueOf(formatFloatData(fandian.doubleValue())) / 100);
                        sellprice = addallprice - lirun;
                    }
                    price = (int) Math.ceil(sellprice);
                }
                else if (jiajiatype == 2) {//星级金额
                    Double addallprice = 0d;
                    Double fandian = 0.0d;
                    Double totaldian = 0.0d;
                    Double lirun = 0.0d;
                    List record = (List) jiajia.get("data");
                    if (record != null && record.size() > 0) {
                        for (int i = 0; i < record.size(); i++) {
                            Map<String, Float> jiajiat = (Map) record.get(i);
                            Float start = jiajiat.get("start");
                            Float addprice = jiajiat.get("addprice");
                            if (start.intValue() == star) {
                                addallprice = suppliersellprice + addprice;
                                fandian = addprice / Math.ceil(addallprice);
                                break;
                            }
                        }
                    }
                    fandian = fandian * 100;
                    totaldian = fandian;
                    long tempagent = 0;
                    Double sellprice = suppliersellprice;
                    if (localliudian != null && localliudian.size() > 0) {
                        for (int i = 0; i < localliudian.size(); i++) {
                            Map lliudian = (Map) localliudian.get(i);
                            Long agentid = Long.valueOf(lliudian.get("agentid").toString());
                            Long liudianagent = Long.parseLong(lliudian.get("liudianagent").toString());
                            if (tempagent == agentid) {
                                continue;
                            }
                            Float start = Float.valueOf(lliudian.get("start").toString());
                            Float end = Float.valueOf(lliudian.get("endt").toString());
                            Float liudian = Float.valueOf(lliudian.get("liudian").toString());
                            if (start <= fandian && fandian < end) {
                                tempagent = agentid;
                                if (liudian >= fandian) {
                                    liudian = fandian.floatValue();
                                    fandian = 0d;
                                }
                                else {
                                    fandian -= liudian;
                                }
                            }
                        }
                    }
                    if (agentId != 1) {
                        lirun = Math.floor(addallprice * Double.valueOf(formatFloatData(fandian.doubleValue())) / 100);
                        sellprice = addallprice - lirun;
                    }
                    price = (int) Math.ceil(sellprice);
                }
                else if (jiajiatype == 3) {//价格范围比例
                    Double addallprice = 0d;
                    Double fandian = 0.0d;
                    Double totaldian = 0.0d;
                    Double lirun = 0.0d;
                    List record = (List) jiajia.get("data");
                    if (record != null && record.size() > 0) {
                        for (int i = 0; i < record.size(); i++) {
                            Map<String, Float> jiajiat = (Map) record.get(i);
                            Float start = jiajiat.get("start");
                            Float end = jiajiat.get("ends");
                            Float addprice = jiajiat.get("addprice");
                            if (suppliersellprice >= start && suppliersellprice < end) {
                                addallprice = suppliersellprice + (int) (suppliersellprice * addprice / 100);
                                fandian = (suppliersellprice * addprice / 100) / Math.ceil(addallprice);
                                break;
                            }
                        }
                    }
                    fandian = fandian * 100;
                    totaldian = fandian;
                    long tempagent = 0;
                    Double sellprice = suppliersellprice;
                    for (int i = 0; i < localliudian.size(); i++) {
                        Map lliudian = (Map) localliudian.get(i);
                        Long agentid = Long.valueOf(lliudian.get("agentid").toString());
                        Long liudianagent = Long.parseLong(lliudian.get("liudianagent").toString());
                        if (tempagent == agentid) {
                            continue;
                        }
                        Float start = Float.valueOf(lliudian.get("start").toString());
                        Float end = Float.valueOf(lliudian.get("endt").toString());
                        Float liudian = Float.valueOf(lliudian.get("liudian").toString());
                        if (start <= fandian && fandian < end) {
                            tempagent = agentid;
                            if (liudian >= fandian) {
                                liudian = fandian.floatValue();
                                fandian = 0d;
                            }
                            else {
                                fandian -= liudian;
                            }
                        }
                    }
                    if (agentId != 1) {
                        lirun = Math.floor(addallprice * Double.valueOf(formatFloatData(fandian.doubleValue())) / 100);
                        sellprice = addallprice - lirun;
                    }
                    price = (int) Math.ceil(sellprice);
                }
                else if (jiajiatype == 4) {//价格范围金额 
                    Double addallprice = 0d;
                    Double fandian = 0.0d;
                    Double totaldian = 0.0d;
                    Double lirun = 0.0d;
                    List record = (List) jiajia.get("data");
                    if (record != null && record.size() > 0) {
                        for (int i = 0; i < record.size(); i++) {
                            Map<String, Float> jiajiat = (Map) record.get(i);
                            Float start = jiajiat.get("start");
                            Float end = jiajiat.get("ends");
                            Float addprice = jiajiat.get("addprice");
                            if (suppliersellprice >= start && suppliersellprice < end) {
                                addallprice = suppliersellprice + addprice;
                                fandian = addprice / Math.ceil(addallprice);
                                break;
                            }
                        }
                    }
                    fandian = fandian * 100;
                    totaldian = fandian;
                    long tempagent = 0;
                    Double sellprice = suppliersellprice;
                    for (int i = 0; i < localliudian.size(); i++) {
                        Map lliudian = (Map) localliudian.get(i);
                        Long liudianagent = Long.valueOf(lliudian.get("liudianagent").toString());
                        Long agentid = Long.valueOf(lliudian.get("agentid").toString());
                        if (tempagent == agentid) {
                            continue;
                        }
                        Float start = Float.valueOf(lliudian.get("start").toString());
                        Float end = Float.valueOf(lliudian.get("endt").toString());
                        Float liudian = Float.valueOf(lliudian.get("liudian").toString());
                        if (start <= fandian && fandian < end) {
                            tempagent = agentid;
                            if (liudian >= fandian) {
                                liudian = fandian.floatValue();
                                fandian = 0d;
                            }
                            else {
                                fandian -= liudian;
                            }
                        }
                    }
                    if (agentId != 1) {
                        lirun = Math.floor(addallprice * Double.valueOf(formatFloatData(fandian.doubleValue())) / 100);
                        sellprice = addallprice - lirun;
                    }
                    price = (int) Math.ceil(sellprice);
                }

            }
        }
        catch (Exception e) {
        }
        if (price <= 0) {
            price = suppliersellprice;
        }
        return price;
    }

    private String formatFloatData(Double num) {
        try {
            DecimalFormat format = null;
            format = (DecimalFormat) NumberFormat.getInstance();
            format.applyPattern("###0.0");
            String result = format.format(num);
            return result;
        }
        catch (Exception e) {
            return num.toString();
        }
    }

    private List getLiudianInfo(Long busstype, long agentId) {
        List list = new ArrayList();
        String sql = "SELECT LI.C_FANDIANSTART start, LI.C_FANDIANEND endt, C_LIUDIAN liudian, S.C_AGENTID liudianagent,"
                + " LR.C_AGENTID agentid, S.C_LIUDIANID hotelmode FROM T_LIUDIANRECORD LI"
                + " LEFT JOIN T_LIUDIANREFINFO LR ON LR.C_TYPEID=LI.C_TYPEID"
                + " LEFT JOIN T_CUSTOMERAGENT C ON C.ID=LR.C_AGENTID "
                + " LEFT JOIN T_SETTLEMENTTYPE S ON S.ID=LR.C_TYPEID "
                + " WHERE LR.C_BUSSTYPE="
                + busstype
                + " and (CHARINDEX(','+CONVERT(NVARCHAR,C.ID)+',',','+(SELECT C_PARENTSTR FROM T_CUSTOMERAGENT WHERE ID="
                + agentId + ")+',')>0 OR C.ID=" + agentId + ") order by LR.C_AGENTID";
        list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list == null) {
            list = new ArrayList();
        }
        return list;
    }

    private Map getJiaJia(long agentId, String hotelmode) {
        Map map = new HashMap();
        if (!"2".equals(hotelmode)) {
            if (getorderConfig().equals("3")) {
                map = getLocalJiajia();
            }
            else {
                map = getYdxJiajia();
            }
        }
        if (map == null) {
            map = new HashMap();
        }
        return map;
    }

    private String loadHotelData(long agentId) {
        String hotelmode = "";
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='ordercontrol'", "", -1, 0);
        if (configs.size() == 1 && !"3".equals(configs.get(0).getValue())) {
            Eaccount count = Server.getInstance().getSystemService().findEaccount(20);
            if (count != null) {
                String pwd = count.getPassword() == null ? "" : count.getPassword();
                String param = "{username:'" + count.getUsername() + "',pwd:'" + pwd
                        + "',method:'liudian',busstype:'2'}";
                String url = Server.getInstance().geturlAtom();
                String result = PHUtil.submitPost(url.substring(0, url.indexOf("/service")) + "/LiudianInforInter",
                        param).toString();
                if (!ElongHotelInterfaceUtil.StringIsNull(result)) {
                    JSONArray array = JSONArray.fromObject(result);
                    if (array != null && array.size() > 0) {
                        JSONObject obj = array.getJSONObject(0);
                        hotelmode = obj.get("hotelmode").toString();
                    }
                    else {
                        hotelmode = "2";
                    }
                }
            }
        }
        if (ElongHotelInterfaceUtil.StringIsNull(hotelmode)) {
            String sql = "SELECT top 1 S.C_LIUDIANID mode FROM T_LIUDIANRECORD LI "
                    + "LEFT JOIN T_LIUDIANREFINFO LR ON LR.C_TYPEID=LI.C_TYPEID "
                    + "LEFT JOIN T_CUSTOMERAGENT C ON C.ID=LR.C_AGENTID "
                    + "LEFT JOIN T_SETTLEMENTTYPE S ON S.ID=LR.C_TYPEID "
                    + "WHERE LR.C_BUSSTYPE=2 and (CHARINDEX(','+CONVERT(NVARCHAR,C.ID)+',',','+(SELECT C_PARENTSTR FROM T_CUSTOMERAGENT WHERE ID="
                    + agentId + ")+',')>0 OR C.ID=" + agentId + ") order by LR.C_AGENTID";
            List modelist = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (modelist != null && modelist.size() > 0) {
                Map mode = (Map) modelist.get(0);
                hotelmode = mode.get("mode").toString();
            }
            else {
                hotelmode = "2";
            }
        }
        return hotelmode;
    }

    private Map getJiaJia(String hotelmode) {
        if (!"2".equals(hotelmode)) {
            if ("3".equals(getorderConfig())) {
                return getLocalJiajia();
            }
            else {
                return getYdxJiajia();
            }
        }
        else {
            return new HashMap();
        }
    }

    private String getorderConfig() {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='ordercontrol'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "";
    }

    private Map getLocalJiajia() {
        Map map = new HashMap();
        List list = new ArrayList();
        List<HotelPriceControlType> type = Server.getInstance().getHotelService()
                .findAllHotelPriceControlType("where c_status=1", "", -1, 0);
        if (type != null && type.size() == 1) {
            map.put("type", type.get(0).getType());
            List<HpriceControlRecord> record = Server.getInstance().getHotelService()
                    .findAllHpriceControlRecord("where c_typeid=" + type.get(0).getId(), "", -1, 0);
            for (int i = 0; i < record.size(); i++) {
                Map<String, Float> liudian = new HashMap<String, Float>();
                liudian.put("addprice", record.get(i).getAddprice());
                liudian.put("start", record.get(i).getStart());
                if (record.get(i).getEnds() == null) {
                    liudian.put("ends", -1f);
                }
                else {
                    liudian.put("ends", record.get(i).getEnds());
                }
                liudian.put("typeid", record.get(i).getTypeid().floatValue());
                list.add(liudian);
            }
            map.put("data", list);
        }
        return map;
    }

    private Map getYdxJiajia() {
        Map map = new HashMap();
        List list = new ArrayList();
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where C_NAME='ordercontrol'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            if (config.getValue().equals("1") || config.getValue().equals("2")) {
                Eaccount count = Server.getInstance().getSystemService().findEaccount(20);
                if (count != null) {
                    String pwd = count.getPassword() == null ? "" : count.getPassword();
                    String param = "{username:'" + count.getUsername() + "',pwd:'" + pwd + "',method:'jiajia'}";
                    String url = Server.getInstance().geturlAtom();
                    String result = SendPostandGet.submitPost(
                            url.substring(0, url.indexOf("/service")) + "/LiudianInforInter", param, "utf-8")
                            .toString();
                    if (result == null || "".equals(result.trim())) {
                        return null;
                    }
                    JSONObject rs = JSONObject.fromObject(result);
                    String type = rs.getString("type");
                    map.put("type", type);
                    JSONArray data = rs.getJSONArray("data");
                    if (data != null && data.size() > 0) {
                        for (int i = 0; i < data.size(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Map<String, Float> liudian = new HashMap<String, Float>();
                            liudian.put("addprice", Float.valueOf(obj.get("addprice").toString()));
                            liudian.put("start", Float.valueOf(obj.get("start").toString()));
                            liudian.put("ends", Float.valueOf(obj.get("ends").toString()));
                            liudian.put("typeid", Float.valueOf(obj.get("typeid").toString()));
                            list.add(liudian);
                        }
                    }
                    map.put("data", list);
                }
            }
        }
        return map;
    }
}