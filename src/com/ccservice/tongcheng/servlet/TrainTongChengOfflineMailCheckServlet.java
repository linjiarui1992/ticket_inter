/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOfflineCannotDeliveryDao;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.domain.TrainOfflineCannotDelivery;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO -
 * 
 * 同程线下票 - 同程线下票送票上门地址核验接口
 * 
 *  取消的逻辑 - 
 *  
 *      锁单之前可以取消
 *  
 *      超时之后可以取消
 *          出票超时 - 订单自动取消 - 无需再次取消 - 且取消没有回调 - 半小时 或者 1个小时
 *          锁单超时 - 之后，一般只会出现拒单的操作 - 调用出票要提示出票的锁单超时，最好操作拒单 - 之后，可以选择取消 - 10分钟或者20分钟
 *  
 *      锁单之中不能取消 - 已锁单的状态
 * 
 * 20171122-110922 更新相关快递时效的新的计算方案
 * 
 * 快递类型获取2.0
 * 
 *  能送顺丰，不送闪送
 *  能送EMS，不送顺丰
 *  
 *      闪送-时效匹配
 *      
 *      顺丰不到，匹配闪送，闪送不匹配
 *      
 * 快递类型获取1.0里面 - 与同程的二号进单接口保持一致
 * 
 * 先判定是否是闪送单 - 
 * 
 *      而后，通过获取快递类型，判定快递类型
 *      
 *      在分取相关的快递时效，
 *      
 *      无法正常反馈的情况，走默认的时效接口反馈
 * 
 * 
 * 获取UU跑腿的快递时效里面，如果该代售点在当天未上班的话，表明无法进行相关快递的配送
 * 
 * 在这个时效接口里面，按照默认的时效进行后续的反馈，自写判定逻辑进行接单，不考虑代售点的上下班的情况
 * 
 * 
 * @author: 郑州-技术-郭伟强 E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25
 * @version: v 1.0
 * @since
 *
 */
public class TrainTongChengOfflineMailCheckServlet extends HttpServlet {
    private static final String LOGNAME = "同程线下票送票上门地址核验接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    private TrainOfflineCannotDeliveryDao trainOfflineCannotDeliveryDao = new TrainOfflineCannotDeliveryDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验信息请求-reqBody-->" + reqBody);

        //幂等响应的初始化操作放在最开始接收到请求的地方

        //结果的返回
        JSONObject resResult = new JSONObject();

        PrintWriter out = response.getWriter();

        Boolean resultFlag = true;

        //同程没有加解密的设计，无需进行格式的替换反转
        //String reqBodyTemp = reqBody.substring(reqBody.indexOf("data")+7, reqBody.length()-2);

        //幂等的设计和实现
        String idempotentFlag = "TongChengMailCheck";

        //更换为数据库的持久化形式
        String idempotentLockKey = reqBody + idempotentFlag + "Lock";

        //String OrderLock = TrainOrderOfflineUtil.TnIdempotent.get(reqBody+idempotentFlag+"Lock");

        Integer PKID = 0;

        TrainOfflineIdempotent trainOfflineIdempotent = null;

        b: for (int i = 0; i < TrainOrderOfflineUtil.TNIDEMPOTENTRETRY; i++) {//幂等的后续处理-最多尝试三次 - 主要是跳出当前的并发处理逻辑-作为其它请求

            try {
                trainOfflineIdempotent = trainOfflineIdempotentDao
                        .findTrainOfflineIdempotentByLockKey(idempotentLockKey);
            }
            catch (Exception e1) {
                resResult = ExceptionTCUtil.handleTCException(e1);

                //记录请求信息日志
                WriteLog.write("同程线下票送票上门地址核验信息请求幂等判定", r1 + ":同程线下票送票上门地址核验信息请求幂等设计-幂等数据交互流程报错");

                out.print(resResult.toJSONString());
                out.flush();
                out.close();

                return;
            }

            if (trainOfflineIdempotent == null) {//锁定状态
                try {
                    PKID = Integer
                            .valueOf(trainOfflineIdempotentDao.addTrainOfflineIdempotent(idempotentLockKey, true));
                }
                catch (Exception e) {
                    resResult = ExceptionTCUtil.handleTCException(e);

                    //记录请求信息日志
                    WriteLog.write("同程线下票送票上门地址核验信息请求幂等判定", r1 + ":同程线下票送票上门地址核验信息请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //初始化之后进行赋值，方便后续使用
                try {
                    trainOfflineIdempotent = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                }
                catch (Exception e) {
                    resResult = ExceptionTCUtil.handleTCException(e);

                    //记录请求信息日志
                    WriteLog.write("同程线下票送票上门地址核验信息请求幂等判定", r1 + ":同程线下票送票上门地址核验信息请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //做二次健壮性判断
                Integer lockCount = 0;
                try {
                    lockCount = trainOfflineIdempotentDao.findCountByLockKey(idempotentLockKey);
                }
                catch (Exception e) {
                    ExceptionTCUtil.handleTCException(e);
                }
                if (lockCount > 1) {
                    //WriteLog.write("幂等判定流程测试", "进入重复请求处理流程-lockCount:"+lockCount);

                    //删除当前增加，并重新走b循环的流程
                    Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(PKID);
                    if (flagTemp < 1) {
                        //记录请求信息日志
                        WriteLog.write("同程线下票送票上门地址核验信息请求幂等判定", "同程线下票送票上门地址核验信息请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                    }
                    continue b;
                }

                break b;
            }
            else {
                PKID = trainOfflineIdempotent.getPKID();

                trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutReqNum(trainOfflineIdempotent);//其它次数的请求

                int loopNum = 1;

                a: while (true) {
                    String idempotentResultValue = trainOfflineIdempotent.getIdempotentResultValue();

                    //此处应做实时查询的动作之前，先进行一次判定
                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        try {
                            idempotentResultValue = trainOfflineIdempotentDao.findResultValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTCUtil.handleTCException(e1);

                            //记录请求信息日志
                            WriteLog.write("同程线下票送票上门地址核验信息请求幂等判定", r1 + ":同程线下票送票上门地址核验信息请求幂等设计-幂等数据交互流程报错");

                            continue a;
                        }
                    }

                    //等待相同的结果出现之后直接进行反馈

                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        //需要再取出一次做是否已删除的尝试判定 - 
                        //防止循环对象的提前删除
                        Boolean idempotentLockValue = null;
                        try {
                            idempotentLockValue = trainOfflineIdempotentDao.findLockValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTCUtil.handleTCException(e1);

                            //记录请求信息日志
                            WriteLog.write("同程线下票取消请求幂等判定", r1 + ":同程线下票取消请求幂等设计-幂等数据交互流程报错");

                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();

                            return;
                        }

                        if (idempotentLockValue == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTCUtil.handleTCException(e);
                            }
                            continue b;
                        }

                        try {
                            Thread.sleep(5 * 1000);
                        }
                        catch (InterruptedException e) {
                            ExceptionTCUtil.handleTCException(e);
                        }

                        loopNum++;

                        //记录请求信息日志
                        //WriteLog.write("同程线下票送票上门地址核验幂等判定", r1 + ":同程线下票送票上门地址核验幂等设计-等待反馈中:"+TrainOrderOfflineUtil.getNowDateStr());

                        //一分钟之后自动结束响应，避免报错之下引起的无限循环对程序本身造成困扰 - 在此处的可重复次数为 - 13次

                        if (loopNum >= TrainOrderOfflineUtil.TNIDEMPOTENTRETRY) {
                            Boolean isSuccess = false;
                            Integer msgCode = 231099;//isSuccess true用231000，false用231099

                            resResult = new JSONObject();

                            resResult.put("isSuccess", isSuccess);
                            resResult.put("msgCode", msgCode);

                            WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验接口-报错之下引起的无限循环的卡断程序启动，接口反馈异常信息");

                            //resResult = JSONObject.parseObject(OrderHandleOverResultFlag);
                            resultFlag = false;

                            break b;
                        }

                        continue a;
                    }
                    else {
                        //幂等中的一次响应 - 该方法涉及到获取和存储，设计成同步的
                        //在这内里做了另外形式的判断
                        trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutResNum(trainOfflineIdempotent);//减少次数的响应

                        if (trainOfflineIdempotent == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTCUtil.handleTCException(e);
                            }

                            continue b;
                        }

                        resResult = JSONObject.parseObject(idempotentResultValue);
                        resultFlag = false;

                        break b;
                    }
                }
            }

        }

        if (resultFlag) {
            resResult = new JSONObject();

            //校验通过的话，可以存储数据
            resResult = tongChengOfflineMailCheck(resResult, reqBody, trainOfflineIdempotent);

            WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验结果-resResult" + resResult);

        }

        out.print(resResult.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject tongChengOfflineMailCheck(JSONObject resResult, String reqBody,
            TrainOfflineIdempotent trainOfflineIdempotent) {
        Boolean isSuccess = false;
        Integer msgCode = 231099;//isSuccess true用231000，false用231099
        String msgInfo = "";//

        JSONObject reqData = JSONObject.parseObject(reqBody);

        WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验-进入到方法-tongChengOfflineMailCheck--->reqData:" + reqData);

        String fromStation = reqData.getString("fromStation");//是   出发站名称
        //String trainNo = reqData.getString("trainNo");//是   车次号
        String departureTime = reqData.getString("departureTime");//是   出发日期 格式：”2017-08-24 10:20”
        String province = reqData.getString("province");//是   目的地省
        String city = reqData.getString("city");//是   目的地市
        String district = reqData.getString("district");//是   目的地区
        String address = reqData.getString("address");//是   目的地地址

        //北京-北京市-顺义区-仁和 石门市场主楼336
        String totalAddress = "";
        if (province != null && !province.equals("")) {
            totalAddress = totalAddress + province + "-";
        }
        if (city != null && !city.equals("")) {
            totalAddress = totalAddress + city + "-";
        }
        if (district != null && !district.equals("")) {
            totalAddress = totalAddress + district + "-";
        }
        if (address != null && !address.equals("")) {
            totalAddress = totalAddress + address;
        }

        /**
         * 快递类型获取1.0里面 - 与同程的二号进单接口保持一致
         * 
         * 先判定是否是闪送单 - 
         * 
         *      而后，通过获取快递类型，判定快递类型
         *      
         *      在分取相关的快递时效，
         *      
         *      无法正常反馈的情况，走默认的时效接口反馈
         * 
         */

        Integer isRapidSend = 0;//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

        Double rapidSendPrice = 0D;//闪送订单的价格 - 在下单的时候做反馈

        String agentidtemp = "414";

        String createUid = "81";
        //String createUser = "同程";

        JSONObject isRapidSendResult = new JSONObject();//闪送的判定和分单逻辑的相关内容并入到了一起，此处作为一个总的全局变量，方便后续的再次使用

        String departureTimeRapidSend = departureTime + ":00";//2017-11-23 15:55

        //价格匹配的单号里面只需要给的一个随机值就可以了

        //生成自己系统平台的订单号
        String orderNumberRapidSend = "TC" + TrainOrderOfflineUtil.timeMinIDRapidSend();

        isRapidSendResult = TrainOrderOfflineUtil.isRapidSend(orderNumberRapidSend, totalAddress, city,
                departureTimeRapidSend);

        Boolean isRapidSendB = isRapidSendResult.getBooleanValue("isRapidSend");
        if (isRapidSendB) {// 0: 普通订单,1: 闪送订单
            isRapidSend = 1;
        }

        if (isRapidSend == 0) {
            try {
                agentidtemp = TrainOrderOfflineUtil.distribution(totalAddress, Integer.valueOf(createUid));
            }
            catch (Exception e) {
                WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验-无法获取到分单代售点");

                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }
        }
        else {
            agentidtemp = isRapidSendResult.getString("agentId");
            rapidSendPrice = isRapidSendResult.getDouble("need_paymoney");

            WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验-闪送分单匹配成功-代售点-agentid:" + agentidtemp + ",匹配的价格-rapidSendPrice:"
                    + rapidSendPrice);
        }

        String arriveTime = "";//预计到达时间

        Integer delieveType = -1;//获取除了UU跑腿之外的快递类型 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】

        int expressType = 0; //快递类型 0:顺丰，1:EMS

        if (isRapidSend == 1) {//是闪送单
            delieveType = 10;//闪送
            expressType = 0;
            arriveTime = TrainOrderOfflineUtil.ungetUUPTDelieveTime();//yyyy-MM-dd HH:mm:ss
        }
        else {//不是闪送单，需要再次区分是顺丰还是EMS？ - 调用获取不同的快递时效 - 0-顺丰 - 1-EMS
                  //快递类型判断 - :ss - 暂且不送EMS - 系统内容尚有问题待处理
            delieveType = TrainOrderOfflineUtil.getDelieveType(province, city, district, address,
                    departureTimeRapidSend, Integer.valueOf(agentidtemp));//

            if (delieveType == 0) {//顺丰
                expressType = 0;
                //利用顺丰的接口,获取快递时效信息 - 46 - 系统
                arriveTime = DelieveUtils.getDelieveStr2(agentidtemp, totalAddress);

                /*try {
                
                }
                catch (Exception e) {
                    WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验-顺丰快递时效获取失败");
                
                    //幂等的设计和实现
                    JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                    TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                    return jsonTemp;
                }*/

                /**
                0-该地址信息错误，请联系客户.
                1-警示：该地区不能送达，请选择其他快递。
                2-获取快递时间失败！请上官网核验快递送达时间。
                 * 
                 * arriveTime = "0";//测试
                 */
                if (arriveTime.equals("0") || arriveTime.equals("2")) {//不拦截
                    //匹配新的快递时效的机制 - 公式
                    arriveTime = TrainOrderOfflineUtil.ungetDelieveTime(totalAddress, agentidtemp, LOGNAME, r1);//yyyy-MM-dd HH:mm:ss
                }
                else if (arriveTime.equals("1")) {
                    //匹配获取EMS的快递时效信息 - 自动切换EMS的逻辑
                    arriveTime = getEMSArriveTime(totalAddress, agentidtemp);
                }
                else {
                    arriveTime = arriveTime.substring(0, arriveTime.indexOf(","));//2017-09-07 18:00:00
                }
            }
            else if (delieveType == 1) {//EMS
                expressType = 1;
                //EMS的快递时效的获取的方法和内容执行
                arriveTime = getEMSArriveTime(totalAddress, agentidtemp);
            }
        }

        WriteLog.write(LOGNAME,
                r1 + ":同程线下票送票上门地址核验-快递类型-delieveType:" + delieveType + ",预计送达时间-arriveTime:" + arriveTime);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Boolean canDelivery = false; //是否可配送
        if (arriveTime != null && !"".equals(arriveTime)) {
            try {
                Date arriveTimeDf = df.parse(arriveTime);
                Date departureTimeDf = df.parse(departureTime);

                long interval = (departureTimeDf.getTime() - arriveTimeDf.getTime()) / (1000 * 60);
                if (interval > (2 * 60)) {//判断出发时间和送达时间是否相差2小时。大于2小时，可以配送，小于2小时无法配送。
                    canDelivery = true;
                }
                isSuccess = true;
                msgCode = 231000;
            }
            catch (ParseException e) {
                WriteLog.write(LOGNAME, r1 + ":同程线下票送票上门地址核验-时间类型转换失败");

                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }
        }
        else {
            msgInfo = "无法获取相关的快递时效信息，请联系技术解决";
            WriteLog.write(LOGNAME, r1 + ":" + msgInfo);
        }

        //将相关的数据入库，便于后期的产品的优化上线分析
        if (!canDelivery) {//不可配送
            /**
             * TrainOfflineCannotDelivery
             * 
            [2017-11-22 15:03:23.50] 2896736:同程线下票送票上门地址核验信息请求-reqBody-->
            {"fromStation":"柴河","trainNo":"K40",
            "departureTime":"2017-11-23 19:01",
            "province":"黑龙江省","city":"牡丹江市","district":"海林市","address":"柴河林业局友谊家园5号楼5单元501"}
            
            [2017-11-22 15:03:23.503] 2896736:同程线下票送票上门地址核验结果-resResult
            {"arriveTime":"2017-11-24 18:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":false}
             * 
             * 送票的地址信息 - 
             * 省市区地址
             * 
             * 发车日期-发车站
             * departureTimeRapidSend[yyyy-MM-dd HH:mm:ss]-fromStation
             * 
             * 预计送达时间，匹配到的快递类型 - 0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
             * arriveTime-delieveType
             */
            TrainOfflineCannotDelivery trainOfflineCannotDelivery = new TrainOfflineCannotDelivery();

            trainOfflineCannotDelivery.setFromStation(fromStation);//
            trainOfflineCannotDelivery.setProvince(province);//
            trainOfflineCannotDelivery.setCity(city);//
            trainOfflineCannotDelivery.setDistrict(district);//
            trainOfflineCannotDelivery.setAddress(address);//
            trainOfflineCannotDelivery.setDepartureTime(departureTimeRapidSend);//
            trainOfflineCannotDelivery.setDelieveType(delieveType);//
            trainOfflineCannotDelivery.setArriveTime(arriveTime);//

            try {
                trainOfflineCannotDeliveryDao.addTrainOfflineCannotDelivery(trainOfflineCannotDelivery);
            }
            catch (Exception e) {
                msgInfo = "不可配送的信息入库失败，请联系技术解决";
                WriteLog.write(LOGNAME, r1 + ":" + msgInfo);
            }
        }

        resResult.put("canDelivery", canDelivery);
        resResult.put("arriveTime", arriveTime);
        resResult.put("expressType", expressType);
        resResult.put("isSuccess", isSuccess);
        resResult.put("msgCode", msgCode);
        resResult.put("msgInfo", msgInfo);

        //幂等的设计和实现
        TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

        return resResult;
    }

    private String getEMSArriveTime(String totalAddress, String agentidtemp) {
        String arriveTime = "";
        String arriveTimeEms = DelieveUtils.getEmsDelieveStr2(agentidtemp, totalAddress);
        if (arriveTimeEms.equals("0")) {//该地址信息错误，请联系客户.
            arriveTime = TrainOrderOfflineUtil.ungetEMSDelieveTime(totalAddress, agentidtemp, LOGNAME, r1);//yyyy-MM-dd HH:mm:ss
        }
        else {
            arriveTime = arriveTimeEms.substring(0, arriveTimeEms.indexOf(","));//2017-09-07 18:00:00
        }
        return arriveTime;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        //获取快递类型里面 - 5天以内默认顺丰 - 1.0

        //String data = "{\"fromStation\":\"乌鲁木齐\",\"trainNo\":\"G2\",\"departureTime\":\"2017-11-24 20:01\",\"province\":\"新疆维吾尔自治区\",\"city\":\"乌鲁木齐市\",\"district\":\"新市区\",\"address\":\"天津北路 海盈银城大厦A座19一01室\"}";
        //{"arriveTime":"2017-11-24 18:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":true}

        //String data = "{\"fromStation\":\"乌鲁木齐\",\"trainNo\":\"G2\",\"departureTime\":\"2017-11-30 20:01\",\"province\":\"新疆维吾尔自治区\",\"city\":\"乌鲁木齐市\",\"district\":\"新市区\",\"address\":\"天津北路 海盈银城大厦A座19一01室\"}";
        //{"arriveTime":"2017-11-26 18:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":1,"canDelivery":true}

        //String data = "{\"fromStation\":\"上海虹桥\",\"trainNo\":\"G2\",\"departureTime\":\"2017-11-24 20:01\",\"province\":\"上海市\",\"city\":\"上海市\",\"district\":\"普陀区\",\"address\":\"中江路388号\"}";
        //{"arriveTime":"2017-11-24 12:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":true}

        //String data = "{\"fromStation\":\"西安\",\"trainNo\":\"K558\",\"departureTime\":\"2017-11-26 17:14\",\"province\":\"陕西省\",\"city\":\"商洛市\",\"district\":\"丹凤县\",\"address\":\"铁峪铺镇，东川村，张塬组。\"}";
        //{"arriveTime":"2017-11-26 12:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":true}

        String data = "{\"fromStation\":\"西安\",\"trainNo\":\"K558\",\"departureTime\":\"2017-11-30 17:14\",\"province\":\"陕西省\",\"city\":\"商洛市\",\"district\":\"丹凤县\",\"address\":\"铁峪铺镇，东川村，张塬组。\"}";
        //{"arriveTime":"2017-11-26 12:00:00","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":1,"canDelivery":true}

        //String data = "{\"fromStation\":\"郑州\",\"trainNo\":\"G2\",\"departureTime\":\"2017-11-22 20:01\",\"province\":\"河南省\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"二七纪念塔388号\"}";
        //{"arriveTime":"2017-11-22 17:37:50","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":true}
        //String data = "{\"fromStation\":\"郑州\",\"trainNo\":\"G2\",\"departureTime\":\"2017-11-22 20:01\",\"province\":\"河南省\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"庆丰街与淮河路北20米路东\"}";
        //{"arriveTime":"2017-11-22 17:31:18","msgCode":231000,"msgInfo":"","isSuccess":true,"expressType":0,"canDelivery":true}

        System.out.println(data);

        //本地测试地址
        String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineMailCheck";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineMailCheck";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/TrainTongChengOfflineMailCheck";

        //正式环境地址
        //String url = "http://121.40.241.126:9020/ticket_inter/TrainTongChengOfflineMailCheck";
        //String url = "http://ws.peisong.51kongtie.com/ticket_inter/TrainTongChengOfflineMailCheck";

        System.out.println(httpPostJsonUtil.doPost(url, data));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
