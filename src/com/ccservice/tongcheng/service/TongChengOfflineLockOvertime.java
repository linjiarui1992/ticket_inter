/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tongcheng.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;

import org.eclipse.jetty.util.UrlEncoded;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.service.TrainTuNiuOfflineLockOrder
 * @description: TODO - 途牛线下票锁票请求发起接口-直接定义在相关的方法中
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:58:14 
 * @version: v 1.0
 * @since 
 *
 */
public class TongChengOfflineLockOvertime implements Job {
    private static final String LOGNAME = "同程线下票锁票启动定时任务-30min后自动解锁";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        Integer useridi = dataMap.getInt("useridi");
        String LockWait = dataMap.getString("LockWait");
        
        WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-进入到自动解锁方法-OrderId-->"+OrderId+"-useridi-->"+useridi+"-LockWait-->"+LockWait);

        if (OrderId == null || OrderId == 0L) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-OrderId不存在，请排查");
            return;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(OrderId);
        }
        catch (Exception e) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline == null) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-对象无法获取，请排查");
            return;
        }
        
        if (trainOrderOffline.getOrderStatus() == 1 && trainOrderOffline.getLockedStatus() == 1) {//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            //记录日志 - 刷新界面 - 
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(OrderId, LOGNAME);
            
            //如果是锁票状态，则变为非锁票的状态，并能给出相关的提示 - 只处理等待出票
            try {
                WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-进入到自动解锁方法-开始修改-->"+4);

                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(OrderId, 4);//0表示未被锁定，1表示被锁定
            }
            catch (Exception e) {
                //记录操作记录
                WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-无法修改锁票状态");
                return;
            }
        }

        WriteLog.write(LOGNAME, r1 + ":"+LOGNAME+"-进入到自动解锁方法-修改锁单状态为锁单超时");
    }

}
