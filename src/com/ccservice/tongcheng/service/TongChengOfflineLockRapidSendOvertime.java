/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tongcheng.service;

import java.util.Random;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.tuniu.train.service.TrainTuNiuOfflineLockOrder
 * @description: TODO - 途牛线下票锁票请求发起接口-直接定义在相关的方法中
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:58:14 
 * @version: v 1.0
 * @since 
 *
 */
public class TongChengOfflineLockRapidSendOvertime implements Job {
    private static final String LOGNAME = "同程线下票闪送订单锁票启动定时任务-8min后自动取消UU跑腿的订单";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        Integer useridi = dataMap.getInt("useridi");
        String LockRapidSendWait = dataMap.getString("LockWait");

        WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到自动取消闪送单的方法-OrderId-->" + OrderId + "-useridi-->" + useridi
                + "-LockRapidSendWait-->" + LockRapidSendWait);

        if (OrderId == null || OrderId == 0L) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-OrderId不存在，请排查");
            return;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(OrderId);
        }
        catch (Exception e) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline == null) {
            //记录操作记录
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-对象无法获取，请排查");
            return;
        }

        if (trainOrderOffline.getOrderStatus() == 1 && trainOrderOffline.getLockedStatus() == 3) {//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            //锁单中的等待出票的单子才会走到此处
            //记录日志 - 刷新界面 - 
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(OrderId, LOGNAME);

            //取消闪送单

            UUptService uuptService = new UUptService();
            JSONObject responseJson = new JSONObject();
            JSONObject data = new JSONObject();
            data.put("orderId", OrderId);
            data.put("cancelReason", "线下票闪送订单定时任务-8min后自动取消UU跑腿订单");
            try {
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrder", data, responseJson);
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }

            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-取消结果:responseJson-" + responseJson);

            //responseJson.getJSONObject("result").getBooleanValue("success");
            //Boolean responseB = responseJson.getBooleanValue("success");
            Boolean responseB = false;
            JSONObject responseJsonResult = new JSONObject();
            if (responseJson.getBooleanValue("success")) {
                responseJsonResult = responseJson.getJSONObject("result");
                responseB = responseJsonResult.getBooleanValue("success");
            }

            if (responseB) {
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送自动取消成功");

                //如果是锁票状态，则变为非锁票的状态，并能给出相关的提示 - 只处理等待出票 - 新增状态5
                try {
                    WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到自动取消闪送单的方法-开始修改-->" + 5);

                    trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(OrderId, 5);//0表示未被锁定，1表示被锁定
                }
                catch (Exception e) {
                    //记录操作记录
                    WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-无法修改锁票状态");
                    return;
                }
                WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到自动取消闪送单方法-取消成功");
            }
            else {
                String errorMsg = responseJsonResult.getString("message");
                if (errorMsg == null || "".equals(errorMsg)) {
                    errorMsg = responseJson.getString("message");
                }
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送自动取消失败:" + errorMsg + "-请联系技术处理");

                WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-进入到自动取消闪送单方法-取消失败");
                return;
            }
        }
    }
}
