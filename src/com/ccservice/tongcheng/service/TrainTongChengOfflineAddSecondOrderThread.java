/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tongcheng.service;

import java.util.List;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.NewDelieveUtils;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tongcheng.service.TrainTongChengOfflineAddOrderThread
 * @description: TODO - 
 * 
 * 同程线下票假驳回的二次出票请求 - 线程队列
 * 
 * 此处主要处理几个逻辑
 * 
 * 所有的修改更新操作全部需要记录相关的操作记录，因为 - Id 已经是已知的【操作记录最好体现到原先的值以及正在更新成为的新值】
 * 
 * 车次本身的信息是不能做更改的，但是可以更改相关的乘车日期【TrainTicketOffline 的 三个相关字段，如果变化的话，需要计算之后更改三个信息】 - 除此之外，席别本身，原则上也是不能更改的
 * 
 * 1、找到原先拒单操作的分配的代售点的ID，重新分给该代售点，修改相关订单的 - AgentId字段
 * 
 * 2、修正相关的二次进单的订单状态和锁单状态 - 并记录相关内容的操作记录 - 等待出票-1 未锁单-0
 * 
 * 3、锁单的状态反馈之类的，全部走相通的逻辑，进行相关内容的重置 - isLockCallback - isRapidSend - isRapidSendSuccess - isRapidSendCallback
 * 
 * 4、所有的后续定制内容，全部走相关的字段的后续补充内容展示。
 * 
 * 
 * 此接口的重点设计是相关二次进单的操作记录的处理和展示
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月27日 上午11:26:07 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineAddSecondOrderThread implements Runnable {
    private static final String LOGNAME = "同程线下票假驳回的二次出票请求线程队列";

    private int r1 = new Random().nextInt(10000000);

    private JSONObject reqData = new JSONObject();

    public TrainTongChengOfflineAddSecondOrderThread(JSONObject reqData) {
        this.reqData = reqData;
    }

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    private TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();

    @Override
    public void run() {
        WriteLog.write(LOGNAME,
                r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->reqData:" + reqData);

        JSONObject resResult = new JSONObject();

        Boolean isSuccess = false;//程序是否执行成功
        Integer msgCode = 231099;//错误代码 true用231000,false用231099

        String orderNo = reqData.getString("orderNo");//同程订单号 - 例:FT599U57UF210D174002304643
        resResult.put("hsOrderNo", orderNo);

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderNo);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
            return jsonTemp;*/

            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
            return;
        }

        WriteLog.write(LOGNAME,
                r1 + ":同程线下票假驳回的二次出票请求--orderNo-->" + orderNo + ",trainOrderOffline-->" + trainOrderOffline);

        //在进入到该线程之前已经做了相关位置的判断，此处需要进行后续逻辑的处理和判定

        Long orderId = trainOrderOffline.getId();

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId, "进入同程二次进单请求处理线程");

        //每次的二次进单的首要操作就是先还原修正相关的日期的修改更新状态为 - 未被操作过 - 
        trainOrderOfflineDao.updateIsSecondDateCallbackByOrderId(orderId, 0);

        //首先判定，出发时间是否发生了变化
        List<TrainTicketOffline> trainTicketOfflineList = null;
        try {
            trainTicketOfflineList = trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(orderId);
        }
        catch (Exception e1) {
            JSONObject resultJSON = ExceptionTCUtil.handleTCException(e1);
            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
            return;
        }

        if (trainTicketOfflineList == null) {
            WriteLog.write(LOGNAME, r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->车票信息获取失败");
            return;
        }

        TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(0);

        /**
         * 相关的时间的修改和设计
         * 
         * 下单的时候，同程传递的是 - 
        startTime   string  是   出发时间,格式：2017-08-24 13:15:00
        endTime string  是   到达时间,格式：2017-08-24 15:15:00
         * 
         * 此次进单，同程传递的是 - 
        出发时间,格式：2017-08-24
         * 
         * DepartTime=2017-09-22 13:35:00.0
         * 
         * CostTime=0:34
         * StartTime=13:35
         * ArrivalTime=14:09
         */
        String departTime = trainTicketOffline.getDepartTime();//2017-09-22 13:35:00.0
        String departDate = departTime.substring(0, 10);//2017-09-22

        String departDateTCNEW = reqData.getString("departTime");//出发时间,格式：2017-08-24

        JSONObject isRapidSendResult = new JSONObject();//闪送的判定和分单逻辑的相关内容并入到了一起，此处作为一个总的全局变量，方便后续的再次使用
        Double rapidSendPrice = 0D;//闪送订单的价格 - 在下单的时候做反馈

        //需要在后面做更新的操作
        Integer isRapidSend = 0;//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

        Integer isRapidSendAgo = trainOrderOffline.getIsRapidSend();//

        //默认取最新一次拒单的反馈的代售点的相关的信息的agentId即可
        Integer AgentId = 0;
        try {
            AgentId = trainOrderOfflineRecordDao.findLastRefuseProviderAgentidByOrderId(orderId);
        }
        catch (Exception e1) {
            JSONObject resultJSON = ExceptionTCUtil.handleTCException(e1);
            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
            return;
        }

        if (AgentId == 0) {
            WriteLog.write(LOGNAME, r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->原分配代售点获取失败");
            return;
        }

        String totalAddress = "";//后面更新快递时效可能需要使用到
        Integer delieveType = 0;//后面更新快递类型可能需要使用到
        /**
         * 根据新的发车时间判定是否是闪送订单
         * 
         * 判断的前提是 - 不是送票到站-是普通的邮寄票 - //送票到站标识 ??? 0-不送票到站 1-送票到站 - 默认值是 0 
         * 
         * 抢票订单目前也没有走闪送的逻辑 - //同程抢票订单标识  0: 普通订单,1: 抢票订单 - 默认值是 0 
         * 
         * 原先不是闪送单，变成了闪送单 - 
         * 
         * 原先是闪送单，变成了非闪送单 - 也可能还是闪送单 - 需要单独创建逻辑进行后续处理 - //20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单
         * 
         */
        MailAddress mailAddress = null;
        try {
            mailAddress = mailAddressDao.findADDRESSCITYNAMEByORDERID(orderId);
        }
        catch (Exception e) {
            JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
            return;
        }

        if (mailAddress == null) {
            WriteLog.write(LOGNAME, r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->邮寄信息获取失败");
            return;
        }

        totalAddress = mailAddress.getADDRESS();
        delieveType = mailAddress.getExpressAgent();
        String city = mailAddress.getCITYNAME();
        WriteLog.write(LOGNAME, r1 + "二次进单地址：" + totalAddress + "二次进单快递类型：" + delieveType + "二次进单城市：" + city);

        if (!departDate.equals(departDateTCNEW)) {
            //需要比对是否是新增的闪送单 - 历时和出发时间、到达时间都应该是不变的 - 而后需要更新这些日期和时间
            String StartTime = trainTicketOffline.getStartTime();//13:35

            String departTimeTCNEW = departDateTCNEW + " " + StartTime + ":00.000";

            WriteLog.write(LOGNAME, r1 + ":同程线下票假驳回的二次出票请求--发车日期发生变化，进行更新-->" + departTime + "变为-->" + departTimeTCNEW);

            //需要更新相关的票表
            try {
                trainTicketOfflineDao.updateDepartTimeByOrderId(departTimeTCNEW, orderId);
            }
            catch (Exception e) {
                JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
                return;
            }

            //修改相关日期的更新状态为 - 被更改
            trainOrderOfflineDao.updateIsSecondDateCallbackByOrderId(orderId, 1);

            //记录日志
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId,
                    "发车日期发生变化，" + departTime + "变为-->" + departTimeTCNEW);

            String isRapidSendDepartTime = departTimeTCNEW.substring(0, departTimeTCNEW.indexOf("."));

            if (trainOrderOffline.getIsRapidSend() == 0) {//普通订单 - 直接判定相关的
                if (trainOrderOffline.getIsDelivery() == 0 && trainOrderOffline.getIsGrab() == 0) {
                    /**
                     * 是 出发时间,格式:2017-08-24 13:15:00 - yyyy-MM-dd HH:mm:ss
                     * 
                     * result.put("isRapidSend", true);
                    result.put("need_paymoney", need_paymoney);
                    result.put("agentId", agentId);
                     */
                    isRapidSendResult = TrainOrderOfflineUtil.isRapidSend(trainOrderOffline.getOrderNumber(),
                            totalAddress, city, isRapidSendDepartTime);

                    Boolean isRapidSendB = isRapidSendResult.getBooleanValue("isRapidSend");
                    if (isRapidSendB) {//非闪送单变成了闪送单 - 走的是新的分单逻辑
                        isRapidSend = 1;//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单
                        AgentId = Integer.valueOf(isRapidSendResult.getString("agentId"));
                        rapidSendPrice = isRapidSendResult.getDouble("need_paymoney");//这个是后面需要单独进行更新的操作
                    }
                    /*else {//获取原先的AgentId
                    
                    }*/
                } /* else {//获取原先的AgentId
                     
                  }*/
            }
            else {//闪送订单 - 
                /**
                 * 原先就是闪送订单的话，
                 * 
                 * 目前的逻辑 - 肯定不是抢票和送票到站的订单 - 只能是普通的订单
                 * 
                 * 首先，根据新的时间判定还是不是闪送订单，如果不是的话，重新走普通订单的分单逻辑 - 修改相关的闪送订单的标识为 普通订单 - 
                 * 相关的标志位也需要进行还原
                 * 
                 */
                if (!TrainOrderOfflineUtil.isRapidSendTime(totalAddress, city, isRapidSendDepartTime)) {//yyyy-MM-dd HH:mm:ss - 闪送单变成了非闪送单
                    String createUid = "81";
                    String createUser = "同程";

                    try {
                        AgentId = Integer
                                .valueOf(TrainOrderOfflineUtil.distribution(totalAddress, Integer.valueOf(createUid)));
                    }
                    catch (Exception e) {
                        JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
                        WriteLog.write(LOGNAME,
                                r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:"
                                        + resultJSON);
                        return;
                    }

                    if (AgentId == 0) {
                        WriteLog.write(LOGNAME,
                                r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->原分配代售点获取失败");
                        return;
                    }

                }
            }
        }
        else {//获取原先的AgentId - 不变
            isRapidSend = isRapidSendAgo;
        }

        /**
         * 更新并还原相关性的初始化标志位
         * OrderStatus - 1;//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理 - 
         * lockedStatus - 0;//0表示未被锁定，1表示被锁定 - 2-锁单失败 3-锁单中 - 4-锁单超时 -【途牛】 状态5-闪送单的锁单超时 - 6-客服的闪送单的二次下单锁单操作
         * 
         * isLockCallback - false
         * 
         * lockTime - 1905-03-14 00:00:00.000
         * 
         * isRapidSendSuccess - false
         * isRapidSendCallback - false
         * 
         * 邮寄信息在拒单流程中不涉及，所以不做任何处理 - 只不过，在原先的出票失败的锁单流程中，出现了订单号的反馈和存储 - 需要在此处做相关操作的还原
         * 
         * 后续的锁单流程需要做是否二次的判断
         * 
         */

        //邮寄信息的更新 - 快递时效的更新与否
        //String delieveStr = trainOrderOffline.getExpressDeliver();

        if (isRapidSendAgo == 1) {//闪送变闪送 - 是二次闪送下单 - 需要做还原， 变 非闪送 - 也还是需要做还原，只不过多了一个价格的更新
            //记录日志
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId, "原闪送订单号还原为0");
            mailAddressDao.updateExpressNumByORDERID(orderId.intValue(), "0");

            if (isRapidSend == 0) {//闪送变闪送，价格无需更新 - 若变非闪送，价格需要归零

                /**
                 * 原先是闪送单 - 非闪送单 
                 * 
                 * 需要重新匹配快递类型
                 * 
                 */
                //记录日志
                TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId,
                        "原闪送订单价格还原为0,更新普通快递时效显示信息和快递类型：" + delieveType);

                //还需要更新快递类型的相关显示处理
                //mailAddressDao.updateRapidSendPriceByORDERID(orderId.intValue(), 0D);
                mailAddressDao.updateRapidSendPriceExpressAgentByORDERID(orderId.intValue(), 0D, 0);

                //重新查询获取快递时效，并做更新操作

                //不是闪送单，需要再次区分是顺丰还是EMS？ - 调用获取不同的快递时效 - 0-顺丰 - 1-EMS

                //暂且不送EMS - 系统内容尚有问题待处理

                //暂时不用，全部取0 - 顺丰
                //delieveType = TrainOrderOfflineUtilCS.getDelieveTypeCS(province, city, district, address, startTime, Integer.valueOf(agentidtemp));//

                String delieveStr = "";//

                //                Integer delieveType = -1;//获取除了UU跑腿之外的快递类型 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
                if (delieveType != 99) {
                    delieveType = TrainOrderOfflineUtil.getDelieveTypeTotalAddress(totalAddress, departDateTCNEW,
                            AgentId);//
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票快递类型请求结果:" + delieveType);
                    // 还原快递单号和快递代理商
                    mailAddressDao.updateMailAddressExpress(orderId.intValue(), "0", delieveType);
                }
                else {
                    delieveStr = "送票到站";
                }

                if (delieveType == 0) {//顺丰
                    //利用顺丰的接口,获取快递时效信息 - 46 - 系统
                    try {
                        //                        delieveStr = DelieveUtils.getDelieveStr(String.valueOf(AgentId), totalAddress);//
                        NewDelieveUtils util = new NewDelieveUtils();
                        delieveStr = util.getDelieveStr("0", String.valueOf(AgentId), totalAddress, 81);
                    }
                    catch (Exception e) {
                        JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
                        WriteLog.write(LOGNAME,
                                r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:"
                                        + resultJSON);
                        return;
                    }
                }
                else if (delieveType == 1) {//EMS
                    //EMS的快递时效的获取的方法和内容执行
                    //                    delieveStr = DelieveUtils.getEmsDelieveStr(String.valueOf(AgentId), totalAddress);//
                    NewDelieveUtils util = new NewDelieveUtils();
                    delieveStr = util.getDelieveStr("1", String.valueOf(AgentId), totalAddress, 81);
                }

                //覆盖原先的快递时效的信息
                trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(orderId, delieveStr);
            }
        }
        else {//非闪送 - 无需还原订单号 - 但是 变闪送 - 需要更新价格
            if (isRapidSend == 1) {
                //记录日志
                TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId,
                        "更新新的闪送订单价格为:" + rapidSendPrice + ",更新新的闪送订单的快递类型和快递时效显示信息");

                //还需要更新快递类型的相关显示处理
                //mailAddressDao.updateRapidSendPriceByORDERID(orderId.intValue(), rapidSendPrice);
                mailAddressDao.updateRapidSendPriceExpressAgentByORDERID(orderId.intValue(), rapidSendPrice, 10);
                // uu跑腿快递时效
                String delieveStr = DelieveUtils.getUUptDelieveStr(String.valueOf(AgentId), totalAddress);//
                //覆盖原先的快递时效的信息
                trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(orderId, delieveStr);
            }
            else {

                String delieveStr = "";//
                /**
                 * 原先是非闪送单的正常订单 - 非闪送单 - 由于日期发生了变化，需要重新匹配快递类型 - TODO - 
                 * 
                 */
                if (delieveType != 99) {
                    delieveType = TrainOrderOfflineUtil.getDelieveTypeTotalAddress(totalAddress, departDateTCNEW,
                            AgentId);//
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票快递类型请求结果:" + delieveType);
                    // 还原快递单号和快递代理商
                    mailAddressDao.updateMailAddressExpress(orderId.intValue(), "0", delieveType);
                }
                else {
                    delieveStr = "送票到站";
                }
                if (delieveType == 0) {//顺丰
                    //利用顺丰的接口,获取快递时效信息 - 46 - 系统
                    try {
                        //                        delieveStr = DelieveUtils.getDelieveStr(String.valueOf(AgentId), totalAddress);//
                        NewDelieveUtils util = new NewDelieveUtils();
                        delieveStr = util.getDelieveStr("0", String.valueOf(AgentId), totalAddress, 81);
                    }
                    catch (Exception e) {
                        JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
                        WriteLog.write(LOGNAME,
                                r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:"
                                        + resultJSON);
                        return;
                    }
                }
                else if (delieveType == 1) {//EMS
                    //EMS的快递时效的获取的方法和内容执行
                    //                    delieveStr = DelieveUtils.getEmsDelieveStr(String.valueOf(AgentId), totalAddress);//
                    NewDelieveUtils util = new NewDelieveUtils();
                    delieveStr = util.getDelieveStr("1", String.valueOf(AgentId), totalAddress, 81);
                }
                //覆盖原先的快递时效的信息
                trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(orderId, delieveStr);
            }
        }

        /**
         * 订单信息的初始化更新 - 后续可能出现变化的相关字段的后续处理
         * 更新相关二次进单的标识字段并完成后期的显示
         */
        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId, "订单信息的初始化更新");

        try {
            trainOrderOfflineDao.initSecondOrder(orderId, AgentId, isRapidSend);//需要对是否是闪送的字段做相关的更新动作
        }
        catch (Exception e) {
            JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票假驳回的二次出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
            return;
        }

        //更新定制信息服务的字段 - 相关操作另起方法完善

        String rejectRemark = reqData.getString("rejectRemark");//接受同包厢中铺两张

        String TradeNo = trainOrderOffline.getTradeNo();
        if (rejectRemark == null) {
            if (!TradeNo.contains("二次进单,定制服务并未发生变化")) {
                if (TradeNo.contains("二次进单,定制服务发生变化")) {
                    String TradeNoTemp1 = TradeNo.substring(0, TradeNo.indexOf("["));
                    String TradeNoTemp2 = TradeNo.substring(TradeNo.indexOf("]") + 2);
                    TradeNo = TradeNoTemp1 + TradeNoTemp2;
                }
                TradeNo = TradeNo + "[二次进单,定制服务并未发生变化,如需出票,请联系客服处理]";
            }
        }
        else {
            if (TradeNo.contains("二次进单,定制服务并未发生变化")) {
                String TradeNoTemp1 = TradeNo.substring(0, TradeNo.indexOf("[二次进单,定制服务并未发生变化"));
                TradeNo = TradeNoTemp1;
            }

            if (TradeNo.contains("二次进单,定制服务发生变化")) {
                String TradeNoTemp1 = TradeNo.substring(0, TradeNo.indexOf("["));
                String TradeNoTemp2 = TradeNo.substring(TradeNo.indexOf("]") + 2);
                TradeNo = TradeNoTemp1 + TradeNoTemp2;
            }
            TradeNo = TradeNo + "[二次进单,定制服务发生变化,请以该后续内容为准进行出票操作]:{" + rejectRemark + "}。";
        }

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderId, "订单定制信息服务更新");
        trainOrderOfflineDao.updateTrainOrderOfflineTradeNoById(orderId, TradeNo);

        //重置相关进单的操作记录
        //修改原操作记录中 0 - 为 13
        trainOrderOfflineRecordDao.initOrderDealResult(orderId);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderId, AgentId, "订单接收成功", 0);//0 - 订单已发放 - 刚接受订单
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        String orderNo = "FT59B63596210D354010954424";

        //送票到家的测试订单的信息 - 有新定制
        String data = "{\"orderNo\":\"" + orderNo + "\",\"departTime\":\"2017-12-30\",\"rejectRemark\":\"你猜你猜\"}";

        // - 无新定制
        //String data = "{\"orderNo\":\""+orderNo+"\",\"departTime\":\"2017-11-07\"}";

        System.out.println(data);

        JSONObject reqData = JSONObject.parseObject(data);

        //如果不符合上述要求 - 执行入库流程操作 - 先做反馈
        new Thread(new TrainTongChengOfflineAddSecondOrderThread(reqData)).start();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间: " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间: " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间: " + runTimeS / 60 + "min:" + runTimeS % 60 + "s");//分
    }

}
