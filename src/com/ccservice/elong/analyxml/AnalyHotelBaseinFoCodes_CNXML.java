package com.ccservice.elong.analyxml;

import java.io.File;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
public class AnalyHotelBaseinFoCodes_CNXML {
	public static void main(String[] args) throws Exception {
		readXML("F:\\航天华有\\酒店文档\\艺龙酒店API接口说明文档_V1.1.6(1)\\hotelbaseinfocodes_cn.xml");
	}

	@SuppressWarnings("unchecked")
	public static void readXML(String filename) {
		// 解析器
		SAXReader reader = new SAXReader();
		// 指定XML文件
		File file = new File(filename);
		try {
			Document doc = reader.read(file);
			Element rootElement = doc.getRootElement();
			// 星级代码
			List starCodes = rootElement.elements("starCodes");
			// 担保类别代码
			List guaranteeTypeCodes = rootElement
					.elements("guaranteeTypeCodes");
			// 订单状态代码
			List orderStatusCodes = rootElement.elements("orderStatusCodes");
			// 客人类别代码
			List guestTypeCodes = rootElement.elements("guestTypeCodes");
			// 支付方式代码
			List paymentTypeCodes = rootElement.elements("paymentTypeCodes");
			// 货币代码
			List currencyCodes = rootElement.elements("currencyCodes");
			// 确认方式代码
			List confirmTypeCodes = rootElement.elements("confirmTypeCodes");
			// 语言代码
			List languageCodes = rootElement.elements("languageCodes");
			// 性别代码
			List genderCodes = rootElement.elements("genderCodes");
			// 证件类别代码
			List idTypeCodes = rootElement.elements("idTypeCodes");
			// 订单取消原因代码
			List CancelCodes = rootElement.elements("CancelCodes");
			// 交通工具
			List TransportationCode = rootElement
					.elements("TransportationCode");
			// 酒店类别
			List TypologyCodes = rootElement.elements("TypologyCodes");
			// 房态代码
			List InventoryCodes = rootElement.elements("InventoryCodes");
			// 增值服务业务代码
			List BusinessCodes = rootElement.elements("BusinessCodes");
			// 预定规则类别代码
			List BookingRuleTypeCodes = rootElement
					.elements("BookingRuleTypeCodes");
			// 促销规则编码
			List DRRTypeCodes = rootElement.elements("DRRTypeCodes");
			// 取消规则类型代码
			List CancelRuleTypeCodes = rootElement
					.elements("CancelRuleTypeCodes");

			starCodes(starCodes);
			guaranteeTypeCodes(guaranteeTypeCodes);
			orderStatusCodes(orderStatusCodes);
			guestTypeCodes(guestTypeCodes);
			paymentTypeCodes(paymentTypeCodes);
			currencyCodes(currencyCodes);
			confirmTypeCodes(confirmTypeCodes);
			languageCodes(languageCodes);
			genderCodes(genderCodes);
			idTypeCodes(idTypeCodes);
			CancelCodes(CancelCodes);
			TransportationCode(TransportationCode);
			TypologyCodes(TypologyCodes);
			InventoryCodes(InventoryCodes);
			BusinessCodes(BusinessCodes);
			BookingRuleTypeCodes(BookingRuleTypeCodes);
			DRRTypeCodes(DRRTypeCodes);
			CancelRuleTypeCodes(CancelRuleTypeCodes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 取消规则类型代码
	@SuppressWarnings("unchecked")
	public static void CancelRuleTypeCodes(List list) {
		System.out.println("取消规则类型代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("CancelRuleType") .equals("")) {
				List list2 = element.elements("CancelRuleType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 促销规则编码
	@SuppressWarnings("unchecked")
	public static void DRRTypeCodes(List list) {
		System.out.println("促销规则编码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("DRRType") .equals("")) {
				List list2 = element.elements("DRRType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 预定规则类别代码
	@SuppressWarnings("unchecked")
	public static void BookingRuleTypeCodes(List list) {
		System.out.println("定规则类别代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("BookingRuleType") .equals("")) {
				List list2 = element.elements("BookingRuleType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 增值服务业务代码
	@SuppressWarnings("unchecked")
	public static void BusinessCodes(List list) {
		System.out.println("增值服务业务代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("Business") .equals("")) {
				List list2 = element.elements("Business");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 房态代码
	@SuppressWarnings("unchecked")
	public static void InventoryCodes(List list) {
		System.out.println("房态代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("Inventory") .equals("")) {
				List list2 = element.elements("Inventory");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 酒店类别
	@SuppressWarnings("unchecked")
	public static void TypologyCodes(List list) {
		System.out.println("酒店类别......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("Typology") .equals("")) {
				List list2 = element.elements("Typology");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 交通工具
	@SuppressWarnings("unchecked")
	public static void TransportationCode(List list) {
		System.out.println("交通工具......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("Transportation") .equals("")) {
				List list2 = element.elements("Transportation");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 订单取消原因的代码
	@SuppressWarnings("unchecked")
	public static void CancelCodes(List list) {
		System.out.println("订单取消原因的代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("Cancel") .equals("")) {
				List list2 = element.elements("Cancel");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 证件类别代码
	@SuppressWarnings("unchecked")
	public static void idTypeCodes(List list) {
		System.out.println("证件类别代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("idType") .equals("")) {
				List list2 = element.elements("idType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 性别代码
	@SuppressWarnings("unchecked")
	public static void genderCodes(List list) {
		System.out.println("性别代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("gender") .equals("")) {
				List list2 = element.elements("gender");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 语言代码
	@SuppressWarnings("unchecked")
	public static void languageCodes(List list) {
		System.out.println("语言代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("language") .equals("")) {
				List list2 = element.elements("language");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 确认方式代码
	@SuppressWarnings("unchecked")
	public static void confirmTypeCodes(List list) {
		System.out.println("确认方式代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("confirmType") .equals("")) {
				List list2 = element.elements("confirmType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 货币代码
	@SuppressWarnings("unchecked")
	public static void currencyCodes(List list) {
		System.out.println("货币代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("currencyId") .equals("")) {
				List list2 = element.elements("currencyId");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 支付方式代码
	@SuppressWarnings("unchecked")
	public static void paymentTypeCodes(List list) {
		System.out.println("支付方式代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("paymentType") .equals("")) {
				List list2 = element.elements("paymentType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 客人类别代码
	@SuppressWarnings("unchecked")
	public static void guestTypeCodes(List list) {
		System.out.println("客人类别代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("guestType") .equals("")) {
				List list2 = element.elements("guestType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 担保类别代码
	@SuppressWarnings("unchecked")
	public static void guaranteeTypeCodes(List list) {
		System.out.println("担保类别代码...........");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("guaranteeType") .equals("")) {
				List list2 = element.elements("guaranteeType");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 星级代码
	@SuppressWarnings("unchecked")
	public static void starCodes(List list) throws SQLException {
		System.out.println("星级代码......");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("star") .equals("")) {
				List list2 = element.elements("star");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}

	// 订单状态代码
	@SuppressWarnings("unchecked")
	public static void orderStatusCodes(List list) {
		System.out.println("订单状态代码...........");
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			Iterator atts = element.attributeIterator();
			while (atts.hasNext()) {
				Attribute att = (Attribute) atts.next();
				System.out.println(att.getName() + "=" + att.getValue());
			}
			if (!element.elementText("orderStatusCodes") .equals("")) {

				List list2 = element.elements("orderStatus");
				// System.out.println(list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					Element ele = (Element) iterator.next();
					// System.out.println(ele.attributeValue("value"));
					System.out.println("value=" + ele.attributeValue("value")
							+ " " + ele.getText());
				}
			}
		}
		System.out.println();
	}
}
