package com.ccservice.elong.analyxml;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.hotellandmark.Hotellandmark;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.b2b2c.base.nearlandmark.Nearlandmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.hoteldb.GetHotelPrice;
import com.ccservice.elong.inter.DateSwitch;

public class UpdateCityID implements Job{
	public static String url = "http://localhost:8080/cn_service/service/";
	public static HessianProxyFactory factory = new HessianProxyFactory();
	public static ISystemService systemservier;
	public static IHotelService servier;
	public static String findHotel = "SELECT ID,C_HOTELCODE,C_NAME FROM T_HOTEL WHERE C_STATE=3 AND C_REGIONID2 IS NULL ORDER BY C_HOTELCODE DESC";
	public static String findAllRoomtype = "SELECT ID,C_HOTELID,C_NAME FROM T_ROOMTYPE";
	
	public static String findCity = "SELECT ID,C_ELONGCITYID FROM T_CITY WHERE C_TYPE=1 ORDER BY C_ELONGCITYID DESC";
	public static String findProvince = "SELECT ID,C_CODE FROM T_PROVINCE";
	public static String findRegion = "SELECT ID,C_REGIONID,C_CITYID FROM T_REGION";
	public static String findChainInfo = "SELECT ID,C_BRANDID FROM T_CHAININFO";
	public static String findLandmark = "SELECT ID,C_ELONGMARKID,C_CITYID FROM T_LANDMARK";public static String findHotelImage = "SELECT ID,C_PATH FROM T_HOTELIMAGE";
	static {
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url + IHotelService.class.getSimpleName());
			systemservier = (ISystemService) factory.create(ISystemService.class, url + ISystemService.class.getSimpleName());
		} catch (Exception e) {
		}
	}

	public static void main(String[] args) {
		System.out.println("start updatecityid.........");
		updateCityid();
		System.out.println("start updatecityid.........");
	}

	@SuppressWarnings("unchecked")
	public static void updateCityid() {
		List<Hotel> hotellist = systemservier.findMapResultBySql(findHotel, null);
		for (int i = 0; i < hotellist.size(); i++) {
			Map map = (Map) hotellist.get(i);
			String hotelCode = map.get("C_HOTELCODE").toString();
			long hotelId=Long.parseLong(map.get("ID").toString());
			updateHotel(hotelCode,hotelId);
		}
	}

	@SuppressWarnings("unchecked")
	public static void updateHotel(String hotelCode,long hotelId) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelCode + ".xml";
		System.out.println("urltemp==" + urltemp);
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			//设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(60000);
			//设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(60000);
			//是否想url输出
			con.setDoOutput(true);
			//设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			String sTotalString;
			sCurrentLine = "";
			sTotalString = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString += sCurrentLine;
			}
			// System.out.println(sTotalString);
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString);
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			//HotelDB.parseHotelXML(nodelist,hotelId);
			parseXML(nodelist);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void parseXML(List list) throws Exception {
		List<City> cityList = systemservier.findMapResultBySql(findCity, null);
		List<Province> provinceList = systemservier.findMapResultBySql(findProvince, null);
		List<Region> regionList = systemservier.findMapResultBySql(findRegion, null);
		List<Chaininfo> chaininfoList = systemservier.findMapResultBySql(findChainInfo, null);
		List<Landmark> landmarkList = systemservier.findMapResultBySql(findLandmark, null);
		long startTime = System.currentTimeMillis();
		System.out.println("一次循环开始时间:" + startTime);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Hotel hotel = new Hotel();
			Element element = (Element) it.next();
			// 酒店id
			if (!element.elementText("id") .equals("")) {
				System.out.println("酒店ID:" + element.elementText("id"));
				hotel.setHotelcode(element.elementText("id"));
			}
			// 酒店名称
			if (!element.elementText("name") .equals("")) {
				hotel.setName(element.elementText("name"));
			}
			// 酒店地址
			if (!element.elementText("address") .equals("")) {
				hotel.setAddress(element.elementText("address"));
			}
			// 酒店所在地邮编
			if (!element.elementText("zip") .equals("")) {
				hotel.setPostcode(element.elementText("zip"));
			}
			// 酒店房间总数
			if (!element.elementText("roomNumber") .equals("")) {
				hotel.setRooms(Integer.valueOf(element.elementText("roomNumber")));
			}
			// 酒店在艺龙的排序
			if (!element.elementText("elongRanking") .equals("")) {
				hotel.setSort(Long.valueOf(element.elementText("elongRanking")));
			}
			// 酒店所在位置的纬度
			if (!element.elementText("lat") .equals("")) {
				hotel.setLat(Double.valueOf(element.elementText("lat")));
			}
			// 酒店所在位置的经度
			if (!element.elementText("lon") .equals("")) {
				hotel.setLng(Double.valueOf(element.elementText("lon")));
			}
			// 酒店所在国家
			if (!element.elementText("country") .equals("")) {
				hotel.setCountryid(168l);
			}
			// 酒店所在省份
			if (!element.elementText("province").equals("")) {
				System.out.println("省份:" + element.elementText("province"));
				String provinceid = element.elementText("province");
				for (int i = 0; i < provinceList.size(); i++) {
					Map map = (Map) provinceList.get(i);
					if (provinceid.equals(map.get("C_CODE"))) {
						hotel.setProvinceid(Long.valueOf(map.get("ID").toString()));
						System.out.println("省份id:入库~~");
					}
				}
			}
			// 酒店所在城市
			System.out.println("城市:" + element.elementText("city"));
			long cityid = 0l;
			if (!element.elementText("city").equals("")) {
				cityid = Long.parseLong(element.elementText("city"));
				for (int i = 0; i < cityList.size(); i++) {
					Map map = (Map) cityList.get(i);
					if (cityid == Long.parseLong(map.get("C_ELONGCITYID").toString())) {
						System.out.println("CITY表中的ID:" + Long.valueOf(map.get("ID").toString()));
						hotel.setCityid(Long.parseLong(map.get("ID").toString()));
						System.out.println("城市id：" + hotel.getCityid());
					}
				}
				// 酒店所在行政区 region1
				if (!element.elementText("district").equals("")) {
					long regionid1 = Long.parseLong(element.elementText("district"));
					System.out.println("行政区:"+regionid1);
					for (int i = 0; i < regionList.size(); i++) {
						Map map = (Map) regionList.get(i);
						//System.out.println("行政区:C_REGIONID:"+map.get("C_REGIONID").toString());
						if (regionid1==Long.parseLong(map.get("C_REGIONID").toString()) && 
								(hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString()))) {
							hotel.setRegionid1(Long.parseLong(map.get("ID").toString()));
							System.out.println("region1 ok....");
						}
					}
				}
				// 酒店所在商业区 region2
				if (!element.elementText("businessZone").equals("")) {
					long regionid2 = Long.parseLong(element.elementText("businessZone"));
					System.out.println("商业区:"+regionid2);
					for (int i = 0; i < regionList.size(); i++) {
						Map map = (Map) regionList.get(i);
						//System.out.println("商业区:C_REGIONID:"+map.get("C_REGIONID").toString());
						if (regionid2==Long.parseLong(map.get("C_REGIONID").toString()) && 
								(hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString()))) {
							hotel.setRegionid2(Long.parseLong(map.get("ID").toString()));
							System.out.println("region2 ok....");
						}
					}
				}

			}
			// 酒店介绍信息
			if (!element.elementText("introEditor").equals("")) {
				hotel.setDescription(element.elementText("introEditor").trim());
			}
			// 酒店描述
			if (!element.elementText("description").equals("")) {
				System.out.println(element.elementText("description"));
			}
			// 可支持的信用卡
			if (!element.elementText("ccAccepted").equals("")) {
				hotel.setCarttype(element.elementText("ccAccepted"));
			}
			// 酒店免费接机信息
			if (!element.elementText("airportPickUpService").equals("")) {
				List image = element.elements("airportPickUpService");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					hotel.setAirportservice("免费接机开始日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartDate"))) + "<br/>"
							+ "免费接机结束日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndDate"))) + "<br/>" + "接机每天开始时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartTime"))) + "<br/>" + "接机每天结束时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndTime"))));
				}
			}
			// 酒店服务设施信息
			if (!element.elementText("generalAmenities").equals("")) {
				List image = element.elements("generalAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setServiceitem(ele.elementText("Overview"));
						System.out.println("房间服务设施概述：" + ele.elementText("Overview").trim());
					}
//					// 服务设施列表
//					if (!ele.elementText("AmenitySimpleList").equals("")) {
//						System.out.println("酒店服务设施列表~~~~~~~~~~~~~~");
//						List l = ele.elements("AmenitySimpleList");
//						System.out.println("           "+ele.elementText("AmenitySimpleList"));
//						String serviceItem = "";
//						for (int i = 0; i < l.size(); i++) {
//							System.out.println("酒店服务设施列表项:" + l.size());
//							Element ele2 = (Element) l.get(i);
//							System.out.println(ele2.elementText("string"));
//							System.out.println(ele2.elementText("string"));
//							System.out.println(ele2.elementText("string"));
//							System.out.println(ele2.elementText("string"));
//							System.out.println(ele2.elementText("string"));
//							
//							List list2=ele2.elements("string");
//							for(Iterator itr=list2.iterator();itr.hasNext();){
//								if (!ele2.elementText("string").equals("")) {
//									serviceItem = ele2.elementText("string") + "、";
//								}
//							}
//							hotel.setServiceitem(serviceItem);
//						}
//					}
				}
			}
			// 房间服务设施信息
			if (!element.elementText("roomAmenities").equals("")) {
				List image = element.elements("roomAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setRoomAmenities(ele.elementText("Overview").trim());
					}
//					// 服务设施列表
//					if (!ele.elementText("AmenitySimpleList").equals("")) {
//						System.out.println("房间服务设施列表~~~~~~~~~~~~~~~");
//						List l = ele.elements("AmenitySimpleList");
//						String fuwu = "";
//						for (int i = 0; i < l.size(); i++) {
//							System.out.println("房间服务设施列表项:" + l.size());
//							Element ele2 = (Element) l.get(i);
//							List list2=ele2.elements("string");
//							for(Iterator itr=list2.iterator();itr.hasNext();){
//								if (!ele2.elementText("string").equals("")) {
//									fuwu = ele2.elementText("string") + "、";
//								}
//							}
//							hotel.setRoomAmenities(fuwu.trim());
//						}
//					}
				}
			}
			// 休闲服务设施信息
			if (!element.elementText("recreationAmenities").equals("")) {
				List image = element.elements("recreationAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 休闲服务设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setPlayitem(ele.elementText("Overview").trim());
					}
//					// 服务设施列表
//					if (!ele.elementText("AmenitySimpleList").equals("")) {
//						System.out.println("休闲服务设施列表~~~~~~~~~~~");
//						List l = ele.elements("AmenitySimpleList");
//						String fuwu = "";
//						for (int i = 0; i < l.size(); i++) {
//							System.out.println("休闲服务设施列表项:" + l.size());
//							Element ele2 = (Element) l.get(i);
//							List list2=ele2.elements("string");
//							for(Iterator itr=list2.iterator();itr.hasNext();){
//								if (!ele2.elementText("string").equals("")) {
//									fuwu = ele2.elementText("string") + "、";
//								}
//							}
//							hotel.setPlayitem(fuwu.trim());
//						}
//					}
				}
			}
			// 会议服务设施信息
			if (!element.elementText("conferenceAmenities").equals("")) {
				List image = element.elements("conferenceAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店会议室设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setMeetingitem(ele.elementText("Overview").trim());
					}
					// // 会议室列表
					// if (!ele.elementText("AmenityList").equals("")) {
					//
					// }
				}
			}
			// 餐饮服务设施信息
			if (!element.elementText("diningAmenities").equals("")) {
				List image = element.elements("diningAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店餐厅设施概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setFootitem(ele.elementText("Overview").trim());
					}
				}
			}
			// 酒店特色信息
			if (!element.elementText("featureInfo").equals("")) {
				List image = element.elements("featureInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店特色信息概述
					String sellPonint = "";
					if (!ele.elementText("DrivingGuide").equals("")) {
						sellPonint += ele.elementText("DrivingGuide").trim() + "<br/>";
					}
					if (!ele.elementText("PropertyOtherHightlights").equals("")) {
						sellPonint += ele.elementText("PropertyOtherHightlights").trim() + "<br/>";
					}
					if (!ele.elementText("PropertyAmenitiesHightlights").equals("")) {
						sellPonint += ele.elementText("PropertyAmenitiesHightlights").trim() + "<br/>";
					}
					if (!ele.elementText("LocationHighlights").equals("")) {
						sellPonint += ele.elementText("LocationHighlights").trim() + "<br/>";
					}
					if (!ele.elementText("Overview").equals("")) {
						sellPonint += ele.elementText("Overview").trim();
					}
					hotel.setSellpoint(sellPonint);
				}
			}
			// 酒店电话(前台)
			if (!element.elementText("Phone").equals("")) {
				hotel.setMarkettell(element.elementText("Phone"));
			}
			// 酒店传真(前台)
			if (!element.elementText("Fax").equals("")) {
				hotel.setFax1(element.elementText("Fax"));
			}
			// 酒店开业日期
			if (!element.elementText("OpeningDate").equals("")) {
				String OpeningDate = element.elementText("OpeningDate");
				java.sql.Date date = DateSwitch.SwitchSqlDate(DateSwitch.SwitchCalendar(OpeningDate));
				hotel.setOpendate(date);
			}
			// 酒店装修日期
			if (!element.elementText("RenovationDate").equals("")) {
				hotel.setRepaildate(element.elementText("RenovationDate"));
			}
			// 酒店挂牌星级 0-无星级；1-一星级；2-二星级；3-三星级；4-四星级；5-五星级
			if (!element.elementText("star").equals("")) {
				hotel.setStar(Integer.valueOf(element.elementText("star")));
			}
			// 酒店所属连锁品牌ID
			if (!element.elementText("brandID").equals("")) {
				String brandId = element.elementText("brandID");
				for (int i = 0; i < chaininfoList.size(); i++) {
					Map map = (Map) chaininfoList.get(i);
					if (brandId.equals(map.get("C_BRANDID").toString())) {
						hotel.setChaininfoid(Long.parseLong(map.get("ID").toString()));
					}
				}
			}
			hotel.setType(1);
			hotel.setLanguage(0);
			// 周边交通信息 1
			if (!element.elementText("trafficAndAroundInformations").equals("")) {
				System.out.println("周边交通信息~~~~~~~~~~~~~~~");
				List traffics = element.elements("trafficAndAroundInformations");
				for (int i = 0; i < traffics.size(); i++) {
					Element ele = (Element) traffics.get(i);
					// // 暂时不用
					// if (ele.elementText("ExtensionData") !=null) {
					// System.out.println("ExtensionData:" +
					// ele.elementText("ExtensionData"));
					// }
					// 周边交通信息概述
					if (!ele.elementText("Overview").equals("")) {
						hotel.setTrafficinfo(ele.elementText("Overview"));
					}
					// 周边交通信息列表
					if (!ele.elementText("TrafficAndAroundInformationList").equals("")) {
						List trafficList = ele.elements("TrafficAndAroundInformationList");
						for (int j = 0; j < trafficList.size(); j++) {
							Element ele2 = (Element) trafficList.get(j);
							if (!ele2.elementText("HotelTrafficAndAroundInformation").equals("")) {
								List trafficList2 = ele2.elements("HotelTrafficAndAroundInformation");
								for (int m = 0; m < trafficList2.size(); m++) {
									Nearlandmark nearlandmark = new Nearlandmark();
									Element ele3 = (Element) trafficList2.get(m);
									// // 暂时不用
									// if(!element3.elementText("ExtensionData").equals("")){}
									// 到酒店距离
									if (!ele3.elementText("Distances") .equals("")) {
										nearlandmark.setLandmarkdistance(ele3.elementText("Distances"));
									}
									// 周边交通名称
									if (!ele3.elementText("Name").equals("")) {
										if(ele3.elementText("Name").contains("'")){
											nearlandmark.setLandmarkname(ele3.elementText("Name").replace("'", "‘"));
										}
									}
									// 备注信息
									if (!ele3.elementText("Note") .equals("")) {
										nearlandmark.setLandmarkdesc(ele3.elementText("Note"));
									}
									// 所用时间
									if (!ele3.elementText("TimeTaken") .equals("")) {
										nearlandmark.setWastetime(ele3.elementText("TimeTaken"));
									}
									// 交通花费费用
									if (!ele3.elementText("TransportFee") .equals("")) {
										nearlandmark.setTraficfee(ele3.elementText("TransportFee"));
									}
									// 乘坐的交通工具
									if (!ele3.elementText("Transportations") .equals("")) {
										if (ele3.elementText("Transportations").equals("1")) {
											nearlandmark.setTraficmethod("出租车");
										}
										if (ele3.elementText("Transportations").equals("2")) {
											nearlandmark.setTraficmethod("公交车");
										}
										if (ele3.elementText("Transportations").equals("3")) {
											nearlandmark.setTraficmethod("地铁");
										}
										if (ele3.elementText("Transportations").equals("4")) {
											nearlandmark.setTraficmethod("步行");
										}
										if (ele3.elementText("Transportations").equals("5")) {
											nearlandmark.setTraficmethod("自行车");
										}
										if (ele3.elementText("Transportations").equals("6")) {
											nearlandmark.setTraficmethod("火车");
										}
									}
									nearlandmark.setHotelid(hotel.getId());
									nearlandmark.setLandmarktype(1);
									List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark(" WHERE " + Nearlandmark.COL_hotelid + "="
											+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '"
											+ nearlandmark.getLandmarkname() + "'", "", -1, 0);
									if (nearlandmarkList.size() > 0) {
										nearlandmark.setId(nearlandmarkList.get(0).getId());
										servier.updateNearlandmarkIgnoreNull(nearlandmark);
									} else {
										nearlandmark = servier.createNearlandmark(nearlandmark);
									}
								}
							}
						}
					}
				}
			}
			// 周边餐饮信息 2
			if (!element.elementText("surroundingRestaurants") .equals("")) {
				System.out.println("周边餐饮信息~~~~~~~~~~~~~~~");
				List restaurants = element.elements("surroundingRestaurants");
				for (int i = 0; i < restaurants.size(); i++) {
					Element ele = (Element) restaurants.get(i);
					if (!ele.elementText("surroundingRestaurant") .equals("")) {
						List restaurantList = ele.elements("surroundingRestaurant");
						for (int j = 0; j < restaurantList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) restaurantList.get(j);
							// 餐厅描述
							if (!ele2.elementText("Description") .equals("")) {
								nearlandmark.setLandmarkdesc(ele2.elementText("Description"));
							}
							// 到酒店距离
							if (!ele2.elementText("Distances") .equals("")) {
								nearlandmark.setLandmarkdistance(ele2.elementText("Distances"));
							}
							// 周边餐厅名字
							if (!ele2.elementText("Name") .equals("")) {
								if(ele2.elementText("Name").contains("'")){
									nearlandmark.setLandmarkname(ele2.elementText("Name").replace("'", "‘"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(2);
							List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark(" WHERE " + Nearlandmark.COL_hotelid + "="
									+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname()
									+ "'", "", -1, 0);
							if (nearlandmarkList.size() > 0) {
								nearlandmark.setId(nearlandmarkList.get(0).getId());
								servier.updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = servier.createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边景致信息 3
			if (!element.elementText("surroundingAttractions") .equals("")) {
				System.out.println("周边景致信息~~~~~~~~~~~~~~~");
				List attractions = element.elements("surroundingAttractions");
				for (int i = 0; i < attractions.size(); i++) {
					Element ele = (Element) attractions.get(i);
					if (!ele.elementText("surroundingAttraction") .equals("")) {
						List attractionList = ele.elements("surroundingAttraction");
						for (int j = 0; j < attractionList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) attractionList.get(j);
							// 到酒店距离
							if (!ele2.elementText("Distances") .equals("")) {
								nearlandmark.setLandmarkdistance(ele2.elementText("Distances"));
							}
							// 周边餐厅名字
							if (!ele2.elementText("Name") .equals("")) {
								if(ele2.elementText("Name").contains("'")){
									nearlandmark.setLandmarkname(ele2.elementText("Name").replace("'", "‘"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(3);
							List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark(" WHERE " + Nearlandmark.COL_hotelid + "="
									+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname()
									+ "'", "", -1, 0);
							if (nearlandmarkList.size() > 0) {
								nearlandmark.setId(nearlandmarkList.get(0).getId());
								servier.updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = servier.createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边购物信息 4
			if (!element.elementText("surroundingShops") .equals("")) {
				System.out.println("周边购物信息~~~~~~~~~~~~~~~");
				List shops = element.elements("surroundingShops");
				for (int i = 0; i < shops.size(); i++) {
					Element ele = (Element) shops.get(i);
					if (!ele.elementText("surroundingShop") .equals("")) {
						List shopList = ele.elements("surroundingShop");
						for (int j = 0; j < shopList.size(); j++) {
							Nearlandmark nearlandmark = new Nearlandmark();
							Element ele2 = (Element) shopList.get(j);
							// 商业区描述
							if (!ele2.elementText("Description") .equals("")) {
								nearlandmark.setLandmarkdesc(ele2.elementText("Description"));
							}
							// 到酒店距离
							if (!ele2.elementText("Distances") .equals("")) {
								nearlandmark.setLandmarkdistance(ele2.elementText("Distances"));
							}
							// 商业区名称
							if (!ele2.elementText("Name") .equals("")) {
								System.out.println("商业区名称：" + ele2.elementText("Name"));
								if(ele2.elementText("Name").contains("'")){
									nearlandmark.setLandmarkname(ele2.elementText("Name").replace("'", "‘"));
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(4);
							List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark(" WHERE " + Nearlandmark.COL_hotelid + "="
									+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname()
									+ "'", "", -1, 0);
							if (nearlandmarkList.size() > 0) {
								nearlandmark.setId(nearlandmarkList.get(0).getId());
								servier.updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = servier.createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			// 周边商务信息 5
			if (!element.elementText("surroundingCommerces") .equals("")) {
				System.out.println("周边商务信息~~~~~~~~~~~~~~~");
				List commerces = element.elements("surroundingCommerces");
				// System.out.println(element.elements("surroundingCommerces"));
				for (int i = 0; i < commerces.size(); i++) {
					Element ele = (Element) commerces.get(i);
					if (!ele.elementText("surroundingCommerce") .equals("")) {
						List commerceList = ele.elements("surroundingCommerce");
						for (int j = 0; j < commerceList.size(); j++) {
							Element ele2 = (Element) commerceList.get(j);
							Nearlandmark nearlandmark = new Nearlandmark();
							// 到酒店距离
							if (!ele2.elementText("Distances") .equals("")) {
								System.out.println("到酒店距离：" + ele2.elementText("Distances"));
								nearlandmark.setLandmarkdistance(ele2.elementText("Distances"));
							}
							// 周边商业区名称
							if (!ele2.elementText("Name") .equals("")) {
								System.out.println("周边商业区名称：" + ele2.elementText("Name"));
								if(ele2.elementText("Name").contains("'")){
									nearlandmark.setLandmarkname(ele2.elementText("Name").replace("'", "‘"));
								}
							}
							// 备注信息
							if (!ele2.elementText("Note") .equals("")) {
								System.out.println("备注信息：" + ele2.elementText("Note"));
								nearlandmark.setLandmarkdesc(ele.elementText("Note"));
							}
							// 所花时间
							if (!ele2.elementText("TimeTaken") .equals("")) {
								System.out.println("所花时间：" + ele2.elementText("TimeTaken"));
								nearlandmark.setWastetime(ele.elementText("TimeTaken"));
							}
							// 交通话费费用
							if (!ele2.elementText("TransportFee") .equals("")) {
								nearlandmark.setTraficfee(ele2.elementText("TransportFee"));
							}
							// 乘坐的交通工具
							if (!ele2.elementText("Transportations") .equals("")) {
								System.out.println("交通工具:" + ele2.elementText("Transportations"));
								if (ele2.elementText("Transportations").equals("1")) {
									nearlandmark.setTraficmethod("出租车");
								}
								if (ele2.elementText("Transportations").equals("2")) {
									nearlandmark.setTraficmethod("公交车");
								}
								if (ele2.elementText("Transportations").equals("3")) {
									nearlandmark.setTraficmethod("地铁");
								}
								if (ele2.elementText("Transportations").equals("4")) {
									nearlandmark.setTraficmethod("步行");
								}
								if (ele2.elementText("Transportations").equals("5")) {
									nearlandmark.setTraficmethod("自行车");
								}
								if (ele2.elementText("Transportations").equals("6")) {
									nearlandmark.setTraficmethod("火车");
								}
							}
							nearlandmark.setHotelid(hotel.getId());
							nearlandmark.setLandmarktype(5);
							List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark(" WHERE " + Nearlandmark.COL_hotelid + "="
									+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname()
									+ "'", "", -1, 0);
							if (nearlandmarkList.size() > 0) {
								nearlandmark.setId(nearlandmarkList.get(0).getId());
								servier.updateNearlandmarkIgnoreNull(nearlandmark);
							} else {
								nearlandmark = servier.createNearlandmark(nearlandmark);
							}
						}
					}
				}
			}
			List<Hotel> hotelList = servier.findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotel.getHotelcode() + "'", "", -1, 0);
			if (hotelList.size() > 0) {
				hotel.setId(hotelList.get(0).getId());
				servier.updateHotelIgnoreNull(hotel);
			}
			// 酒店附近标志物
			if (!element.elementText("Landmarks") .equals("")) {
				List landmarks = element.elements("Landmarks");
				for (int i = 0; i < landmarks.size(); i++) {
					Element ele = (Element) landmarks.get(i);
					if (!ele.elementText("HotelLandMark") .equals("")) {
						List list2 = ele.elements("HotelLandMark");
						for (int j = 0; j < list2.size(); j++) {
							Hotellandmark hotellandmark = new Hotellandmark();
							Element element2 = (Element) list2.get(j);
							hotellandmark.setHotelid(hotel.getId());
							// 酒店附近标志物ID
							if (!element2.elementText("LandMarkID") .equals("")) {
								String landmarkID = element2.elementText("LandMarkID");
								for (int n = 0; n < landmarkList.size(); n++) {
									//System.out.println("landmarkList.size():" + landmarkList.size());
									Map map = (Map) landmarkList.get(n);
									//System.out.println("hotel.getCityid():" + hotel.getCityid());
									// System.out.println("landmark表中的城市id;"+map.get("C_CITYID").toString());
									if (landmarkID.equals(map.get("C_ELONGMARKID").toString())
											&& (hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString()))) {
										hotellandmark.setLandmarkid(Long.parseLong(map.get("ID").toString()));
									}
								}
							}
							hotellandmark.setLanguage(0);
							hotellandmark.setHotelid(hotel.getId());
							//System.out.println("酒店id~!~~~~~:" + hotel.getId());
							List<Hotellandmark> hotelandmarkList = servier.findAllHotellandmark(" where " + Hotellandmark.COL_landmarkid + "="
									+ hotellandmark.getLandmarkid(), "", -1, 0);
							if (hotelandmarkList.size() > 0) {
								hotellandmark.setId(hotelandmarkList.get(0).getId());
								servier.updateHotellandmarkIgnoreNull(hotellandmark);
							} else {
								hotellandmark = servier.createHotellandmark(hotellandmark);
							}
						}
					}
				}
			}
			// 房间类型信息
			if (!element.elementText("roomInfo") .equals("")) {
				List image = element.elements("roomInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (!ele.elementText("room") .equals("")) {
						List list2 = ele.elements("room");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Roomtype roomtype = new Roomtype();
							Element element2 = (Element) iterator.next();
							// 房型ID
							if (!element2.elementText("roomTypeId") .equals("")) {
								roomtype.setRoomcode(element2.elementText("roomTypeId"));
							}
							// 房型名称
							if (!element2.elementText("roomName") .equals("")) {
								roomtype.setName(element2.elementText("roomName"));
							}
							// 房间面积
							if (!element2.elementText("area") .equals("")) {
								roomtype.setAreadesc(element2.elementText("area"));
							}
							// 房间所在楼层
							if (!element2.elementText("floor") .equals("")) {
								roomtype.setLayer(element2.elementText("floor"));
							}
							// 是否有带宽 0表示无宽带，1 表示免费 2表示收费
							if (!element2.elementText("hasBroadnet") .equals("")) {
								roomtype.setWideband(Integer.valueOf(element2.elementText("hasBroadnet")));
							}
							// 宽待是否收费 0 表示免费 1 表示收费
							if (!element2.elementText("broadnetFee") .equals("")) {
								if (element2.elementText("broadnetFee").equals("1")) {
									roomtype.setWideband(2);
								}
							}
							// 备注
							if (!element2.elementText("note") .equals("")) {
								roomtype.setNote(element2.elementText("note").trim());
							}
							// 房间描述
							if (!element2.elementText("bedDescription") .equals("")) {
								roomtype.setRoomdesc(element2.elementText("bedDescription").trim());
							}
							// 床型描述信息 1 单人床 2 大床 3 双床 4 大或双 5 其他
							if (!element2.elementText("bedType") .equals("")) {
								if (element2.elementText("bedType").contains("单人床")) {
									roomtype.setBed(1);
								} else if (element2.elementText("bedType").contains("大床")) {
									roomtype.setBed(2);
								} else if (element2.elementText("bedType").contains("双床")) {
									roomtype.setBed(3);
								} else if (element2.elementText("bedType").contains("大或双")) {
									roomtype.setBed(4);
								} else {
									roomtype.setBed(5);
								}
							}
							roomtype.setLanguage(0);
							roomtype.setHotelid(hotel.getId());
							List<Roomtype> roomTypeList = servier.findAllRoomtype("where " + Roomtype.COL_name + "='" + roomtype.getName() + "'"
									+ " and " + Roomtype.COL_hotelid + "='" + roomtype.getHotelid() + "'", "", -1, 0);
							if (roomTypeList.size() > 0) {
								roomtype.setId(roomTypeList.get(0).getId());
								servier.updateRoomtypeIgnoreNull(roomtype);
							} else {
								roomtype = servier.createRoomtype(roomtype);
							}
						}
					}
				}
				System.out.println("roomType........ok!!!");
			}
			// 酒店图片信息
			if (!element.elementText("images") .equals("")) {
				List image = element.elements("images");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					System.out.println();
					Element ele = (Element) Image.next();
					List image2 = ele.elements("image");
					for (Iterator Image2 = image2.iterator(); Image2.hasNext();) {
						Hotelimage hotelImage = new Hotelimage();
						Element ele2 = (Element) Image2.next();
						// 图片URL地址
						if (!ele2.elementText("imgUrl") .equals("")) {
							hotelImage.setPath(ele2.elementText("imgUrl"));
						}
						// 图片类型 各值表示如下：0-展示图；1-餐厅；2-休闲室；3-会议室
						// ；4-服务；5-酒店外观
						// ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
						if (!ele2.elementText("imgType") .equals("")) {
							hotelImage.setType(Integer.valueOf(ele2.elementText("imgType")));
						}
						// 图片标题
						if (!ele2.elementText("title").equals("")) {
							hotelImage.setDescription(ele2.elementText("title"));
						}
						hotelImage.setHotelid(hotel.getId());
						hotelImage.setLanguage(0);
						List<Hotelimage> hotelImageList = servier.findAllHotelimage("where " + Hotelimage.COL_path + "='" + hotelImage.getPath()
								+ "'", "", -1, 0);
						if (hotelImageList.size() > 0) {
							hotelImage.setId(hotelImageList.get(0).getId());
							servier.updateHotelimageIgnoreNull(hotelImage);
						} else {
							hotelImage = servier.createHotelimage(hotelImage);
						}
					}
				}
				System.out.println("hotelImage......ok!!!!");
			}
			System.out.println("ok!!!");
			//GetHotelPrice.getHotelPrice(hotel.getName(), hotel.getHotelcode(), cityid+"");
		}
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		System.out.println("start updatecityid.........");
		updateCityid();
		System.out.println("start updatecityid.........");
		
	}
}
