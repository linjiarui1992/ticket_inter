package com.ccservice.elong.analyxml;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.hotellandmark.Hotellandmark;
import com.ccservice.b2b2c.base.nearlandmark.Nearlandmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.DateSwitch;

public class AnalyHotelIdXML {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		Map map = new HashMap();
		map.put("q1:", "http://api.elong.com/staticInfo");
		SAXReader reader = new SAXReader();
		File file = new File("F:\\航天华有\\酒店文档\\艺龙酒店API接口说明文档_V1.1.6(1)\\静态数据\\已入库数据\\01144001.xml");
		Document document = reader.read(file);
		XPath path = document.createXPath("HotelDetail");
		path.setNamespaceURIs(map);
		List nodelist = path.selectNodes(document);
		parseXML(nodelist);
	}

	@SuppressWarnings( { "unchecked", "unchecked" })
	public static void parseXML(List list) throws Exception {
		String url = "http://localhost:8080/cn_service/service/";

		HessianProxyFactory factory = new HessianProxyFactory();
		IHotelService servier = null;
		ISystemService systemservier = null;
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url + IHotelService.class.getSimpleName());
			systemservier = (ISystemService) factory.create(ISystemService.class, url + ISystemService.class.getSimpleName());
		} catch (Exception e) {
		}
		String findCity = "SELECT ID,C_SORT FROM T_CITY ORDER BY C_SORT";
		String findProvince = "SELECT ID,C_CODE FROM T_PROVINCE";
		String findRegion = "SELECT ID,C_REGIONID FROM T_REGION";
		List<City> cityList = systemservier.findMapResultBySql(findCity, null);
		List<Province> provinceList = systemservier.findMapResultBySql(findProvince, null);
		List<Region> regionList = systemservier.findMapResultBySql(findRegion, null);

		long startTime = System.currentTimeMillis();
		System.out.println("一次循环开始时间:" + startTime);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Hotel hotel = new Hotel();
			City city = new City();
			Province province = new Province();
			Region region = new Region();
			Element element = (Element) it.next();
			// 酒店id
			if (!element.elementText("id") .equals("")) {
				System.out.println("酒店ID:" + element.elementText("id"));
				hotel.setHotelcode(element.elementText("id"));
			}
			// // 酒店最新更新时间
			// if (!element.elementText("dateUpdated") .equals("")) {
			// System.out.println("dateUpdated:"
			// + element.elementText("dateUpdated"));
			// }
			// 酒店名称
			if (!element.elementText("name") .equals("")) {
				hotel.setName(element.elementText("name"));
			}
			// 酒店地址
			if (!element.elementText("address") .equals("")) {
				hotel.setAddress(element.elementText("address"));
			}
			// 酒店所在地邮编
			if (!element.elementText("zip") .equals("")) {
				hotel.setPostcode(element.elementText("zip"));
			}
			// // 酒店星级 此为酒店对外的挂牌星级，
			// if (!element.elementText("category") .equals("")) {
			// System.out.println("category:"
			// + element.elementText("category"));
			//				
			// }
			// // 酒店类别 目前静态文件中只有"H"类型
			// if (!element.elementText("typology") .equals("")) {
			// System.out.println("typology:"
			// + element.elementText("typology"));
			// }
			// 酒店房间总数
			if (!element.elementText("roomNumber") .equals("")) {
				hotel.setRooms(Integer.valueOf(element.elementText("roomNumber")));
			}
			// // 酒店特殊信息提示
			// if (!element.elementText("availPolicy") .equals("")) {
			// System.out.println("availPolicy:"
			// + element.elementText("availPolicy"));
			// }
			// // 暂时不用
			// if (!element.elementText("activationDate") .equals("")) {
			// System.out.println("activationDate:"
			// + element.elementText("activationDate"));
			// }
			// // 用户评分,目前暂时不用
			// if (!element.elementText("usersRating") .equals("")) {
			// System.out.println("usersRating:"
			// + element.elementText("usersRating"));
			// }
			// 酒店在艺龙的排序
			if (!element.elementText("elongRanking") .equals("")) {
				hotel.setSort(Long.valueOf(element.elementText("elongRanking")));
			}
			// // 暂时不用
			// if (!element.elementText("templateType") .equals("")) {
			// System.out.println("templateType:"
			// + element.elementText("templateType"));
			// }
			// // 暂时不用
			// if (!element.elementText("translations") .equals("")) {
			// System.out.println("translations:"
			// + element.elementText("translations"));
			// }
			// // 暂时不用
			// if (!element.elementText("doublePriceMin") .equals("")) {
			// System.out.println("doublePriceMin:"
			// + element.elementText("doublePriceMin"));
			// }
			// // 暂时不用
			// if (!element.elementText("doublePriceMax") .equals("")) {
			// System.out.println("doublePriceMax:"
			// + element.elementText("doublePriceMax"));
			// }
			// // 暂时不用
			// if (!element.elementText("currency") .equals("")) {
			// System.out.println("currency:"
			// + element.elementText("currency"));
			// }
			// 酒店所在位置的纬度
			if (!element.elementText("lat") .equals("")) {
				// System.out.println("lat:" + element.elementText("lat"));
				hotel.setLat(Double.valueOf(element.elementText("lat")));
			}
			// 酒店所在位置的经度
			if (!element.elementText("lon") .equals("")) {
				// System.out.println("lon:" + element.elementText("lon"));
				hotel.setLng(Double.valueOf(element.elementText("lon")));
			}
			// 酒店所在国家
			if (!element.elementText("country") .equals("")) {
				// System.out.println("country:" +
				// element.elementText("country"));
				hotel.setCountryid(168l);
			}
			// // 酒店区域
			// if (!element.elementText("region") .equals("")) {
			// System.out.println("region:" +
			// element.elementText("region"));
			// }
			// 酒店所在省份
			if (!element.elementText("province") .equals("")) {
				System.out.println("省份:" + element.elementText("province"));
				province.setCode(element.elementText("province"));
				System.out.println("Code:" + province.getCode());
				for (int i = 0; i < provinceList.size(); i++) {
					Map map = (Map) provinceList.get(i);
					if (province.getCode().equals(map.get("C_CODE"))) {
						hotel.setProvinceid(Long.valueOf(map.get("ID").toString()));
						System.out.println("provinceid  ok  !!");
					}
				}
			}
			// 酒店所在城市
			System.out.println("city:::::" + element.elementText("city"));
			if (!element.elementText("city") .equals("")) {
				// System.out.println("city:::::"+element.elementText("city"));
				city.setSort(Integer.parseInt(element.elementText("city")));
				for (int i = 0; i < cityList.size(); i++) {
					Map map = (Map) cityList.get(i);
					// System.out.println("!!!:"+city.getSort());
					// System.out.println("c_sort:"+map.get("C_SORT").toString());
					if (city.getSort() == Integer.parseInt(map.get("C_SORT").toString())) {
						System.out.println("CITY表中的ID:" + Long.valueOf(map.get("ID").toString()));
						hotel.setCityid(Long.parseLong(map.get("ID").toString()));
						System.out.println("酒店ID;" + hotel.getCityid());
					}
				}
			}
			// 酒店所在商业区 region2
			if (!element.elementText("businessZone") .equals("")) {
				// System.out.println("businessZone:" +
				// element.elementText("businessZone"));
				region.setRegionid(element.elementText("businessZone"));
				for (int i = 0; i < regionList.size(); i++) {
					Map map = (Map) regionList.get(i);
					if (region.getRegionid().equals(Long.parseLong(map.get("C_REGIONID").toString()))) {
						hotel.setRegionid2(Long.parseLong(map.get("ID").toString()));
						System.out.println("你好,北京");
					}
				}
			}
			// 酒店所在行政区 region1
			if (!element.elementText("district") .equals("")) {
				// System.out.println("district:" +
				// element.elementText("district"));
				region.setRegionid(element.elementText("district"));
				for (int i = 0; i < regionList.size(); i++) {
					Map map = (Map) regionList.get(i);
					if (region.getRegionid().equals(map.get("C_REGIONID").toString())) {
						hotel.setRegionid1(Long.parseLong(map.get("ID").toString()));
						System.out.println("Nihao,beijing");
					}
				}
			}
			// // 酒店在www.elong.com网站中的详细页URL
			// if (!element.elementText("propertyUrl") .equals("")) {
			// System.out.println("propertyUrl:"
			// + element.elementText("propertyUrl"));
			// }
			// 酒店介绍信息
			if (!element.elementText("introEditor") .equals("")) {
				hotel.setDescription(element.elementText("introEditor"));
			}
			// 可支持的信用卡
			if (!element.elementText("ccAccepted") .equals("")) {
				hotel.setCarttype(element.elementText("ccAccepted"));
			}
			// 酒店描述
			if (!element.elementText("description") .equals("")) {
				// System.out.println("description:" +
				// element.elementText("description"));
			}
			if (!element.elementText("airportPickUpService") .equals("")) {
				List image = element.elements("airportPickUpService");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 暂时不用
					if (!ele.elementText("ExtensionData") .equals("")) {
						// System.out.println("ExtensionData:" +
						// ele.elementText("ExtensionData"));
					}
					// 免费接机结束日期
					if (!ele.elementText("EndDate") .equals("")) {
						// System.out.println("EndDate:" +
						// ele.elementText("EndDate"));
					}
					// 接机每天结束时间
					if (!ele.elementText("EndTime") .equals("")) {
						// System.out.println("EndTime:" +
						// ele.elementText("EndTime"));
					}
					// 免费接机开始日期
					if (!ele.elementText("StartDate") .equals("")) {
						// System.out.println("StartDate:" +
						// ele.elementText("StartDate"));
					}
					// 接机每天开始时间
					if (!ele.elementText("StartTime") .equals("")) {
						// System.out.println("StartTime:" +
						// ele.elementText("StartTime"));
					}
					hotel.setAirportservice("免费接机开始日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartDate")))
							+ "免费接机结束日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndDate"))) + "接机每天开始时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartTime"))) + "接机每天结束时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndTime"))));
				}
			}
			// 酒店服务设施信息
			if (!element.elementText("roomAmenities") .equals("")) {
				List image = element.elements("roomAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 暂时不用
					if (!ele.elementText("ExtensionData") .equals("")) {
						// System.out.println("ExtensionData:" +
						// ele.elementText("ExtensionData"));
					}
					// 服务设施列表
					if (!ele.elementText("AmenitySimpleList") .equals("")) {
						hotel.setRoomAmenities(ele.elementText("AmenitySimpleList").trim());
					}
					// 酒店服务设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setServiceitem(ele.elementText("Overview"));
					}
				}
			}
			// 休闲服务设施信息
			if (!element.elementText("recreationAmenities") .equals("")) {
				List image = element.elements("recreationAmenities");
				String str = "";
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 暂时不用
					if (!ele.elementText("ExtensionData") .equals("")) {
						// System.out.println("ExtensionData:" +
						// ele.elementText("ExtensionData"));
					}
					// 服务设施列表
					if (!ele.elementText("AmenitySimpleList") .equals("")) {

					}
					// 房间服务设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setPlayitem(ele.elementText("AmenitySimpleList").trim());
					}
				}
			}
			// 会议服务设施信息
			if (!element.elementText("conferenceAmenities") .equals("")) {
				List image = element.elements("conferenceAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店会议室设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setMeetingitem(ele.elementText("Overview"));
					}
				}
			}
			// 餐饮服务设施信息
			if (!element.elementText("diningAmenities") .equals("")) {
				List image = element.elements("diningAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 餐厅列表
					if (!ele.elementText("AmenityList") .equals("")) {
						// System.out.println("AmenityList:" +
						// ele.elementText("AmenityList"));
					}
					// 酒店餐厅设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setFootitem(ele.elementText("Overview"));
					}
				}
			}
			// 酒店特色信息
			if (!element.elementText("featureInfo") .equals("")) {

				List image = element.elements("featureInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// // 酒店ID
					// if (!ele.elementText("HotelID") .equals("")) {
					// System.out.println("HotelID:"
					// + ele.elementText("HotelID"));
					// }
					// // 驾车指南
					// if (!ele.elementText("DrivingGuide") .equals("")) {
					// System.out.println("DrivingGuide:"
					// + ele.elementText("DrivingGuide"));
					// }
					// // 酒店其他特色
					// if (!ele.elementText("PropertyOtherHightlights") !=
					// "") {
					// System.out.println("PropertyOtherHightlights:"
					// + ele.elementText("PropertyOtherHightlights"));
					// }
					// // 服务设施特色
					// if (!ele.elementText("PropertyAmenitiesHightlights")
					// .equals("")) {
					// System.out
					// .println("PropertyAmenitiesHightlights:"
					// + ele
					// .elementText("PropertyAmenitiesHightlights"));
					// }
					// // 地理位置特色
					// if (!ele.elementText("LocationHighlights") .equals("")) {
					// System.out.println("LocationHighlights:"
					// + ele.elementText("LocationHighlights"));
					// }
					// 酒店特色信息概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setSellpoint(ele.elementText("Overview"));
					}
				}
			}
			// 酒店电话(前台)
			if (!element.elementText("Phone") .equals("")) {
				hotel.setMarkettell(element.elementText("Phone"));
			}
			// 酒店传真(前台)
			if (!element.elementText("Fax") .equals("")) {
				hotel.setFax1(element.elementText("Fax"));
			}
			// 酒店开业日期
			if (!element.elementText("OpeningDate") .equals("")) {
				String OpeningDate = element.elementText("OpeningDate");
				java.sql.Date date = DateSwitch.SwitchSqlDate(DateSwitch.SwitchCalendar(OpeningDate));
				hotel.setOpendate(date);
			}
			// 酒店装修日期
			if (!element.elementText("RenovationDate") .equals("")) {
				hotel.setRepaildate(element.elementText("RenovationDate"));
			}
			// 酒店附近标志物
			if (!element.elementText("Landmarks") .equals("")) {
				List image = element.elements("Landmarks");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (!ele.elementText("HotelLandMark") .equals("")) {
						List list2 = ele.elements("HotelLandMark");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Hotellandmark hotellandmark = new Hotellandmark();
							Element element2 = (Element) iterator.next();
							// // 暂时不用
							// if (!element2.elementText("ExtensionData") !=
							// "") {
							// System.out
							// .println("ExtensionData:"
							// + element2
							// .elementText("ExtensionData"));
							// }
							// 酒店ID
							if (!element2.elementText("HotelID") .equals("")) {
								hotellandmark.setHotelid(hotel.getId());
							}
							// 酒店附近标志物ID
							if (!element2.elementText("LandMarkID") .equals("")) {
								hotellandmark.setLandmarkid(Long.valueOf(element2.elementText("LandMarkID")));
							}
							// // 酒店附近标志物名称
							// if (!element2.elementText("LandMarkName") !=
							// "") {
							// System.out.println("LandMarkName:"
							// + element2.elementText("LandMarkName"));
							// }
							// // 酒店附近标志物英文名称
							// if (!element2.elementText("LandMarkNameEn") !=
							// "") {
							// System.out.println("LandMarkNameEn:"
							// + element2
							// .elementText("LandMarkNameEn"));
							// }
							hotellandmark.setLanguage(0);
							hotellandmark.setHotelid(hotel.getId());
							List<Hotellandmark> hotelandmarkList = servier.findAllHotellandmark(" where " + Hotellandmark.COL_landmarkid + "='"
									+ hotellandmark.getLandmarkid(), "", -1, 0);
							if (hotelandmarkList.size() > 0) {
								hotellandmark.setId(hotelandmarkList.get(0).getId());
								servier.updateHotellandmarkIgnoreNull(hotellandmark);
							} else {
								hotellandmark = servier.createHotellandmark(hotellandmark);
							}
						}
					}
				}
			}
			// 酒店挂牌星级 0-无星级；1-一星级；2-二星级；3-三星级；4-四星级；5-五星级
			if (!element.elementText("star") .equals("")) {
				hotel.setStar(Integer.valueOf(element.elementText("star")));
			}
			// //
			// if (!element.elementText("groupID") .equals("")) {
			// System.out.println("groupID:" +
			// element.elementText("groupID"));
			// }
			// 酒店所属连锁品牌ID
			if (!element.elementText("brandID") .equals("")) {
				System.out.println("brandID:" + element.elementText("brandID"));
				// hotel.setChaininfoid(Long.valueOf(element.elementText("brandID")));
			}
			// 是否是经济型酒店 默认值为0，1代表是经济型酒店
			if (!element.elementText("iseconomic") .equals("")) {
				// System.out.println("是否是经济型酒店:" + "iseconomic:" +
				// element.elementText("iseconomic"));
				// hotel.setType(Integer.valueOf(element.elementText("iseconomic")));
			}
			// // 是否是酒店式公寓
			// if (!element.elementText("isapartment") .equals("")) {
			// System.out.println("isapartment:"
			// + element.elementText("isapartment"));
			// }
			hotel.setLanguage(0);
			List<Hotel> hotelList = servier.findAllHotel("where " + Hotel.COL_name + "='" + hotel.getName() + "'" + " and " + Hotel.COL_hotelcode
					+ "='" + hotel.getHotelcode() + "'", "", -1, 0);
			if (hotelList.size() > 0) {
				hotel.setId(hotelList.get(0).getId());
				servier.updateHotelIgnoreNull(hotel);
			} else {
				hotel = servier.createHotel(hotel);
			}
			System.out.println();
			// 周边交通信息 1
			if (!element.elementText("trafficAndAroundInformations") .equals("")) {

				List image = element.elements("trafficAndAroundInformations");
				// System.out.println(image.size());
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Nearlandmark nearlandmark = new Nearlandmark();
					Element ele = (Element) Image.next();
					// 暂时不用
					if (!ele.elementText("ExtensionData") .equals("")) {
						// System.out.println("ExtensionData:" +
						// ele.elementText("ExtensionData"));
					}
					// 周边交通信息概述
					if (!ele.elementText("Overview") .equals("")) {
						nearlandmark.setLandmarkdesc(ele.elementText("Overview"));
					}
					// 周边交通信息列表
					if (!element.elementText("TrafficAndAroundInformationList") .equals("")) {
						List list2 = ele.elements("TrafficAndAroundInformationList");
						// System.out.println(image.size());
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Element element2 = (Element) iterator.next();
							//
							System.out.println("输出<q1:HotelTrafficAndAroundInformation></q1:HotelTrafficAndAroundInformation>之间的元素");
							if (!ele.elementText("HotelTrafficAndAroundInformation") .equals("")) {
								List list3 = element2.elements("HotelTrafficAndAroundInformation");
								// System.out.println(list3.size());
								for (Iterator iterator2 = list3.iterator(); iterator2.hasNext();) {
									System.out.println();
									Element element3 = (Element) iterator2.next();
									// 暂时不用
									if (!element3.elementText("ExtensionData") .equals("")) {
										// System.out.println("ExtensionData:" +
										// element3.elementText("ExtensionData"));
									}
									// 到酒店距离
									if (!element3.elementText("Distances") .equals("")) {
										nearlandmark.setLandmarkdistance(element3.elementText("Distances"));
										// System.out.println("Distances:" +
										// element3.elementText("Distances"));
									}
									// 周边交通名称
									if (!element3.elementText("Name") .equals("")) {
										nearlandmark.setLandmarkname(element3.elementText("Name"));
										// System.out.println("Name:" +
										// element3.elementText("Name"));
									}
									// 备注信息
									if (!element3.elementText("Note") .equals("")) {
										// System.out.println("Note:" +
										// element3.elementText("Note"));
									}
									// 所用时间
									if (!element3.elementText("TimeTaken") .equals("")) {
										nearlandmark.setWastetime(element3.elementText("TimeTaken"));
										// System.out.println("TimeTaken:" +
										// element3.elementText("TimeTaken"));
									}
									// 交通花费费用
									if (!element3.elementText("TransportFee") .equals("")) {
										nearlandmark.setTraficfee(element3.elementText("TransportFee"));
										// System.out.println("TransportFee:" +
										// element3.elementText("TransportFee"));
									}
									// 乘坐的交通工具
									if (!element3.elementText("Transportations") .equals("")) {
										nearlandmark.setTraficmethod(element3.elementText("Transportations"));
										// System.out.println("Transportations:"
										// +
										// element3.elementText("Transportations"));
									}
									nearlandmark.setHotelid(hotel.getId());
									nearlandmark.setLandmarktype(1);
									List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark("WHERE " + Nearlandmark.COL_hotelid + "="
											+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '"
											+ nearlandmark.getLandmarkname() + "'", "", -1, 0);
									if (nearlandmarkList.size() > 0) {
										nearlandmark.setId(nearlandmarkList.get(0).getId());
										servier.updateNearlandmarkIgnoreNull(nearlandmark);
									} else {
										nearlandmark = servier.createNearlandmark(nearlandmark);
									}
								}
							}
						}
					}
				}
			}
			// 周边餐饮信息 2
			if (!element.elementText("surroundingRestaurants") .equals("")) {
				List image = element.elements("surroundingRestaurants");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Nearlandmark nearlandmark = new Nearlandmark();
					Element ele = (Element) Image.next();
					// 餐厅描述
					if (!ele.elementText("Description") .equals("")) {
						System.out.println("Description:" + ele.elementText("Description"));
						nearlandmark.setLandmarkdesc(ele.elementText("Description"));
					}
					// 到酒店距离
					if (!ele.elementText("Distances") .equals("")) {
						System.out.println("Distances:" + ele.elementText("Distances"));
						nearlandmark.setLandmarkdistance(ele.elementText("Distances"));
					}
					// 周边餐厅名字
					if (!ele.elementText("Name") .equals("")) {
						nearlandmark.setLandmarkname(ele.elementText("Name"));
					}
					nearlandmark.setHotelid(hotel.getId());
					nearlandmark.setLandmarktype(2);
					List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark("WHERE " + Nearlandmark.COL_hotelid + "="
							+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname() + "'",
							"", -1, 0);
					if (nearlandmarkList.size() > 0) {
						nearlandmark.setId(nearlandmarkList.get(0).getId());
						servier.updateNearlandmarkIgnoreNull(nearlandmark);
					} else {
						nearlandmark = servier.createNearlandmark(nearlandmark);
					}
				}
			}
			// 周边景致信息 3
			if (!element.elementText("surroundingAttractions") .equals("")) {
				List image = element.elements("surroundingAttractions");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Nearlandmark nearlandmark = new Nearlandmark();
					Element ele = (Element) Image.next();
					// 到酒店距离
					if (!ele.elementText("Distances") .equals("")) {
						nearlandmark.setLandmarkdistance(ele.elementText("Distances"));
						System.out.println("Distances:" + ele.elementText("Distances"));
					}
					// 周边餐厅名字
					if (!ele.elementText("Name") .equals("")) {
						nearlandmark.setLandmarkname(ele.elementText("Name"));
					}
					nearlandmark.setHotelid(hotel.getId());
					nearlandmark.setLandmarktype(3);
					List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark("WHERE " + Nearlandmark.COL_hotelid + "="
							+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname() + "'",
							"", -1, 0);
					if (nearlandmarkList.size() > 0) {
						nearlandmark.setId(nearlandmarkList.get(0).getId());
						servier.updateNearlandmarkIgnoreNull(nearlandmark);
					} else {
						nearlandmark = servier.createNearlandmark(nearlandmark);
					}
				}
			}
			// 周边购物信息 4
			if (!element.elementText("surroundingShops") .equals("")) {
				List image = element.elements("surroundingShops");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Nearlandmark nearlandmark = new Nearlandmark();
					Element ele = (Element) Image.next();
					// 商业区描述
					if (!ele.elementText("Description") .equals("")) {
						nearlandmark.setLandmarkdesc(ele.elementText("Description"));
						System.out.println("Description:" + ele.elementText("Description"));
					}
					// 到酒店距离
					if (!ele.elementText("Distances") .equals("")) {
						nearlandmark.setLandmarkdistance(ele.elementText("Distances"));
						System.out.println("Distances:" + ele.elementText("Distances"));
					}
					// 商业区名称
					if (!ele.elementText("Name") .equals("")) {
						nearlandmark.setLandmarkname(ele.elementText("Name"));
					}
					nearlandmark.setHotelid(hotel.getId());
					nearlandmark.setLandmarktype(4);
					List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark("WHERE " + Nearlandmark.COL_hotelid + "="
							+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname() + "'",
							"", -1, 0);
					if (nearlandmarkList.size() > 0) {
						nearlandmark.setId(nearlandmarkList.get(0).getId());
						servier.updateNearlandmarkIgnoreNull(nearlandmark);
					} else {
						nearlandmark = servier.createNearlandmark(nearlandmark);
					}
				}
			}
			 // 周边商务信息 5
			if (!element.elementText("surroundingCommerces") .equals("")) {
				System.out.println("surroundingCommerces:" + element.elementText("surroundingCommerces"));
				List image = element.elements("surroundingCommerces");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Nearlandmark nearlandmark = new Nearlandmark();
					System.out.println();
					Element ele = (Element) Image.next();
					// 到酒店距离
					if (!ele.elementText("Distances") .equals("")) {
						nearlandmark.setLandmarkdistance(ele.elementText("Distances"));
						System.out.println("Distances:" + ele.elementText("Distances"));
					}
					// 周边商业区名称
					if (!ele.elementText("Name") .equals("")) {
						nearlandmark.setLandmarkname(ele.elementText("Name"));
						System.out.println("Name:" + ele.elementText("Name"));
					}
					// 备注信息
					if (!ele.elementText("Note") .equals("")) {
						System.out.println("Note:" + ele.elementText("Note"));
					}
					// 所花时间
					if (!ele.elementText("TimeTaken") .equals("")) {
						nearlandmark.setWastetime(ele.elementText("TimeTaken"));
						System.out.println("TimeTaken:" + ele.elementText("TimeTaken"));
					}
					// 交通话费费用
					if (!ele.elementText("TransportFee") .equals("")) {
						nearlandmark.setTraficfee(ele.elementText("TransportFee"));
						System.out.println("TransportFee:" + ele.elementText("TransportFee"));
					}
					// 乘坐的交通工具
					if (!ele.elementText("Transportations") .equals("")) {
						nearlandmark.setTraficmethod(ele.elementText("Transportations"));
						System.out.println("Transportations:" + ele.elementText("Transportations"));
					}
					nearlandmark.setHotelid(hotel.getId());
					nearlandmark.setLandmarktype(5);
					List<Nearlandmark> nearlandmarkList = servier.findAllNearlandmark("WHERE " + Nearlandmark.COL_hotelid + "="
							+ nearlandmark.getHotelid() + " AND " + Nearlandmark.COL_landmarkname + " = '" + nearlandmark.getLandmarkname() + "'",
							"", -1, 0);
					if (nearlandmarkList.size() > 0) {
						nearlandmark.setId(nearlandmarkList.get(0).getId());
						servier.updateNearlandmarkIgnoreNull(nearlandmark);
					} else {
						nearlandmark = servier.createNearlandmark(nearlandmark);
					}
				}
			}
			// 房间类型信息
			if (!element.elementText("roomInfo") .equals("")) {
				List image = element.elements("roomInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (!ele.elementText("room") .equals("")) {
						List list2 = ele.elements("room");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Roomtype roomtype = new Roomtype();
							Element element2 = (Element) iterator.next();
							// 房型ID
							if (!element2.elementText("roomTypeId") .equals("")) {
								roomtype.setRoomcode(element2.elementText("roomTypeId"));
							}
							// 房型名称
							if (!element2.elementText("roomName") .equals("")) {
								roomtype.setName(element2.elementText("roomName"));
							}
							// // 房型数量
							// if (!element2.elementText("roomTypeNum") !=
							// "") {
							// System.out.println("roomTypeNum:"
							// + element2.elementText("roomTypeNum"));
							// }
							// 房间面积
							if (!element2.elementText("area") .equals("")) {
								roomtype.setAreadesc(element2.elementText("area"));
							}
							// 房间所在楼层
							if (!element2.elementText("floor") .equals("")) {
								roomtype.setLayer(element2.elementText("floor"));
							}
							// 是否有带宽 0表示无宽带，1 表示免费 2表示收费
							if (!element2.elementText("hasBroadnet") .equals("")) {
								roomtype.setWideband(Integer.valueOf(element2.elementText("hasBroadnet")));
							}
							// 宽待是否收费 0 表示免费 1 表示收费
							if (!element2.elementText("broadnetFee") .equals("")) {
								if (element2.elementText("broadnetFee").equals("1")) {
									roomtype.setWideband(2);
								}
							}
							// // 备注
							// if (!element2.elementText("note") .equals("")) {
							// System.out.println("note:"
							// + element2.elementText("note"));
							// }
							// 房间描述
							if (!element2.elementText("bedDescription") .equals("")) {
								roomtype.setRoomdesc(element2.elementText("bedDescription"));
							}
							// 床型描述信息 1 单人床 2 大床 3 双床 4 大或双 5 其他
							if (!element2.elementText("bedType") .equals("")) {
								if (element2.elementText("bedType").contains("单人床")) {
									roomtype.setBed(1);
								} else if (element2.elementText("bedType").contains("大床")) {
									roomtype.setBed(2);
								} else if (element2.elementText("bedType").contains("双床")) {
									roomtype.setBed(3);
								} else if (element2.elementText("bedType").contains("大或双")) {
									roomtype.setBed(4);
								} else {
									roomtype.setBed(5);
								}
							}
							roomtype.setLanguage(0);
							roomtype.setHotelid(hotel.getId());
							List<Roomtype> roomTypeList = servier.findAllRoomtype("where " + Roomtype.COL_name + "='" + roomtype.getName() + "'"
									+ " and " + Roomtype.COL_hotelid + "='" + roomtype.getHotelid() + "'", "", -1, 0);
							if (roomTypeList.size() > 0) {
								roomtype.setId(roomTypeList.get(0).getId());
								servier.updateRoomtypeIgnoreNull(roomtype);
							} else {
								roomtype = servier.createRoomtype(roomtype);
							}
						}
					}
				}
				System.out.println("roomType........ok!!!");
			}
			// 酒店描述
			if (!element.elementText("images") .equals("")) {
				List image = element.elements("images");
				// System.out.println(image.size());
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					System.out.println();
					Element ele = (Element) Image.next();
					List image2 = ele.elements("image");
					// System.out.println(image2.size());
					for (Iterator Image2 = image2.iterator(); Image2.hasNext();) {
						Hotelimage hotelImage = new Hotelimage();
						Element ele2 = (Element) Image2.next();
						// 图片URL地址
						if (!ele2.elementText("imgUrl") .equals("")) {
							hotelImage.setPath(ele2.elementText("imgUrl"));
						}
						// 图片类型 各值表示如下：0-展示图；1-餐厅；2-休闲室；3-会议室 ；4-服务；5-酒店外观
						// ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
						if (!ele2.elementText("imgType") .equals("")) {
							hotelImage.setType(Integer.valueOf(ele2.elementText("imgType")));
						}
						// 图片标题
						if (!ele2.elementText("title") .equals("")) {
							hotelImage.setDescription(ele2.elementText("title"));
						}
						// if (!ele2.elementText("imgNum") .equals("")) {
						// System.out.println("imgNum:"
						// + ele2.elementText("imgNum"));
						// }
						hotelImage.setHotelid(hotel.getId());
						hotelImage.setLanguage(0);
						// System.out.println("hotelId:" + hotel.getId());
						List<Hotelimage> hotelImageList = servier.findAllHotelimage("where " + Hotelimage.COL_path + "='" + hotelImage.getPath()
								+ "'", "", -1, 0);
						if (hotelImageList.size() > 0) {
							hotelImage.setId(hotelImageList.get(0).getId());
							servier.updateHotelimageIgnoreNull(hotelImage);
						} else {
							hotelImage = servier.createHotelimage(hotelImage);
						}
					}
				}
				System.out.println("hotelImage......ok!!!!");
			}
		}
		System.out.println("ok!!!");
		long endTime = System.currentTimeMillis();
		System.out.println("一次循环结束时间:" + endTime);
		System.out.println("插入一条数据需要:" + DateSwitch.showTime(endTime - startTime));
	}

}
