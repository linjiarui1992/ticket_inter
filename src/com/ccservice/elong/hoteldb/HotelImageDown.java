package com.ccservice.elong.hoteldb;

import java.io.FileInputStream;

import org.dom4j.ElementHandler;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author wzc 下载酒店图片
 */
public class HotelImageDown implements Job {
	
	
	public static void main(String[] args) throws Exception {
		readImageXML("D:\\酒店数据\\hotellist.xml");
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			// DownFile.download("http://114-svc.elong.com/xml/v1.2/hotellist.xml",
			// "D:\\酒店数据\\hotellist.xml");
			readImageXML("D:\\酒店数据\\hotellist.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public static void readImageXML(String filename) throws Exception {
		FileInputStream fis = new FileInputStream(filename);
		// 解析器
		SAXReader reader = new SAXReader();
		// 建立MyElementHandler的实例
		ElementHandler addHandler = new MyElementHandlerImage();
		reader.addHandler("/ArrayOfHotelInfoForIndex/HotelInfoForIndex",
				addHandler);
		reader.read(fis);
	}

}
