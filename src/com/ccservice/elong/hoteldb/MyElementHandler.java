package com.ccservice.elong.hoteldb;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.XPath;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.inter.server.Server;

public class MyElementHandler implements ElementHandler {
	
	Map<String, Long> keyvalues=new HashMap<String, Long>();

	public MyElementHandler() {
		long t1=System.currentTimeMillis();
		List result=Server.getInstance().getSystemService().findMapResultBySql("select id,c_hotelcode from t_hotel where c_sourcetype=1", null);
		for (int i = 0; i < result.size(); i++) {
			Map m=(Map)result.get(i);
			String hotelcode=m.get("c_hotelcode").toString();
			Long id=Long.parseLong(m.get("id").toString());
			keyvalues.put(hotelcode, id);
		}
		long t2=System.currentTimeMillis();
		System.out.println("用时："+(t2-t1)/1000+"s");
	}
	@Override
	public void onEnd(ElementPath path) {
		// TODO Auto-generated method stub
		// 获取当前节点
		Element row = path.getCurrent();
		// System.out.println("row:"+row);
		// 对节点进行操作...
		String tree = path.getPath().substring(26);
		// System.out.println("tree:"+tree);
		if (tree.equals("HotelInfoForIndex")) {
			updateHotel(row);
		}
		// 处理当前节点后,将其从dom树种剪除
		row.detach();
	}

	@Override
	public void onStart(ElementPath path) {
	}

	@SuppressWarnings("unchecked")
	public void updateHotel(Element root) {
		long t1=System.currentTimeMillis();
		System.out.println("...............updateHotel..............");
		List<Element> list = root.elements();
		Hotel hotel = new Hotel();
		String isOk = "";
		String hotelCode = "";
		String deltime = "";
		String addtime = "";
		String modifytime = "";
		String ExtensionData="";
		for (int i = 0; i < list.size(); i++) {
			Element e=list.get(i);
			switch (i) {
			case 0:
				ExtensionData=e.getText();
				break;
			case 1:
				addtime = e.getText();
				break;
			case 2:
				// 此酒店下线停止销售时间
				deltime = e.getText();
				break;
			case 3:
				// 酒店id（酒店在艺龙系统中的唯一标识HotelID）
				hotelCode = e.getText();
				System.out.println("酒店ID:" + hotelCode);
				hotel.setHotelcode(hotelCode);
				break;
			case 4:
				hotel.setName(e.getText());// 酒店中文名
				break;
			case 5:
				hotel.setEnname(e.getText());// 酒店英文名
				break;
			case 6:
				isOk = e.getText();// 酒店是否可用
				break;
			case 7:
				modifytime=e.getText();// 最新修改时间
				break;
			default:
				break;
			}
		}
		System.out.println("T_HOTEL表中是否存在有记录:" + keyvalues.containsKey(hotelCode));
		if (deltime.equals("")) {
			if (isOk.equals("0")) {
				try {
					hotel.setState(3);
					hotel.setStatedesc("该酒店可用");
					System.out.println("可用!!!!!!");
					if (keyvalues.containsKey(hotelCode)) {
						hotel.setId(keyvalues.get(hotelCode));
						Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
						addOrUpdateHotelByHotelid(hotelCode, hotel.getId());
						GetHotelPrice.getHotelPrice(hotel.getName(), hotel.getHotelcode(), hotel.getCityid()+"");
					} else {
						hotel = Server.getInstance().getHotelService().createHotel(hotel);
						addOrUpdateHotelByHotelid(hotelCode, hotel.getId());
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				if (keyvalues.containsKey(hotelCode)) {
					hotel.setState(0);
					hotel.setStatedesc("该酒店暂不可用");
					System.out.println("不可用\\\\");
					hotel.setId(keyvalues.get(hotelCode));
					Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
				}
			}
		} else {
			Date delTime;
			try {
				delTime = DateSwitch.StringSwitchUDate(deltime);
				if (isOk.equals("0") || delTime.after(new Date())) {
					hotel.setState(3);
					hotel.setStatedesc("该酒店可用");
					System.out.println("可用@@@@@@");
					if (keyvalues.containsKey(hotelCode)) {
						hotel.setId(keyvalues.get(hotelCode));
						Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
						addOrUpdateHotelByHotelid(hotelCode, hotel.getId());
						GetHotelPrice.getHotelPrice(hotel.getName(), hotel.getHotelcode(), hotel.getCityid()+"");
					} else {
						hotel = Server.getInstance().getHotelService().createHotel(hotel);
						addOrUpdateHotelByHotelid(hotelCode, hotel.getId());
					}
				} else {
					if (keyvalues.containsKey(hotelCode)) {
						hotel.setState(0);
						hotel.setStatedesc("该酒店暂不可用");
						System.out.println("不可用....");
						hotel.setId(keyvalues.get(hotelCode));
						Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (addtime.equals("")) {
			if (isOk.equals("0")) {
				Date addTime;
				Date modifyTime;
				try {
					addTime = DateSwitch.StringSwitchUDate(addtime);
					modifyTime = DateSwitch.StringSwitchUDate(modifytime);
					if (addTime.after(DateSwitch.StringSwitchUDate(DateSwitch.CatchBeforeDay() + " 04:00:00"))
							|| modifyTime.after(DateSwitch.StringSwitchUDate(DateSwitch.CatchBeforeDay() + " 04:00:00"))) {
						addOrUpdateHotelByHotelid(hotelCode, hotel.getId());
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		System.out.println("..................over...................");
		long t2=System.currentTimeMillis();
		System.out.println("更新完一个酒店用时："+(t2-t1)/1000+"s");
	}
	@SuppressWarnings("unchecked")
	public static void addOrUpdateHotelByHotelid(String hotelCode, long hotelId) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelCode + ".xml";
		System.out.println("urltemp==" + urltemp);
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			// 设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(120000);
			// 设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(120000);
			// 是否想url输出
			con.setDoOutput(true);
			// 设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			StringBuilder sTotalString = new StringBuilder();
			sCurrentLine = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString.append(sCurrentLine);
			}
			// System.out.println(sTotalString);
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString.toString());
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			HotelDB.parseHotelXML(nodelist, hotelId);
			bf.close();
			in.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
