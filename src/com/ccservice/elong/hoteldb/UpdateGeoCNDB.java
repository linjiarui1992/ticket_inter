package com.ccservice.elong.hoteldb;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.country.Country;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.DownFile;

/**
 * 酒店省份/城市/区域/Landmark
 * 
 * @author 师卫林
 * 
 */
public class UpdateGeoCNDB implements Job {
	public static void main(String[] args) throws Exception {
		DownFile.download("http://114-svc.elong.com/xml/geo_cn.xml", "D:\\酒店数据\\geo_cn.xml");
		long start = System.currentTimeMillis();
		System.out.println("start time:" + new Date());
		read("D:\\酒店数据\\geo_cn.xml");
		HanziToPinyin.haiziToPinyin();
		CitySort.citySort();
		long end = System.currentTimeMillis();
		System.out.println("end time:" + new Date());
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		try {
			DownFile.download("http://114-svc.elong.com/xml/geo_cn.xml", "D:\\酒店数据\\geo_cn.xml");
			long start = System.currentTimeMillis();
			System.out.println("start time:" + new Date());
			read("D:\\酒店数据\\geo_cn.xml");
			HanziToPinyin.haiziToPinyin();
			CitySort.citySort();
			long end = System.currentTimeMillis();
			System.out.println("end time:" + new Date());
			System.out.println("耗时:" + DateSwitch.showTime(end - start));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void read(String filename) throws Exception {
		Map map = new HashMap();
		map.put("q1:", "http://api.elong.com/staticInfo");
		SAXReader reader = new SAXReader();
		File file = new File(filename);
		Document document = reader.read(file);
		XPath path = document.createXPath("HotelGeoList");
		path.setNamespaceURIs(map);
		List nodelist = path.selectNodes(document);
		parseXML(nodelist);
	}

	@SuppressWarnings("unchecked")
	public static void parseXML(List list) throws Exception {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Element element = (Element) it.next();
			if (!element.elementText("HotelGeo").equals("")) {
				List image = element.elements("HotelGeo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Province province = new Province();// 省份
					City city = new City();
					Element ele = (Element) Image.next();
					//国家
					String countryname = ele.elementText("country");
					System.out.println("国家名字:" + countryname);
					// 省份名称
					if (!ele.elementText("provinceName").equals("")) {
						String provinceName = ele.elementText("provinceName");
						province.setName(provinceName);
					}
					// 省份ID
					if (!ele.elementText("provinceId").equals("")) {
						province.setCode(ele.elementText("provinceId"));
						province.setLanguage(0);
					}
					province.setType(1);// 1--酒店
					List<Province> provinceList = Server.getInstance().getHotelService().findAllProvince("where " + Province.COL_code + "='" + province.getCode() + "'", "", -1, 0);
					if (provinceList.size() > 0) {
						province.setId(provinceList.get(0).getId());
						Server.getInstance().getHotelService().updateProvinceIgnoreNull(province);
					} else {
						province = Server.getInstance().getHotelService().createProvince(province);
					}
					// 城市名称
					if (!ele.elementText("cityName").equals("")) {
						String cityName = ele.elementText("cityName");
						System.out.println("城市名字:" + cityName);
						if (cityName.contains("（")) {
							city.setName(cityName.substring(0, cityName.lastIndexOf("（")));
							System.out.println("城市名字:" + cityName.substring(0, cityName.lastIndexOf("（")));
						} else {
							city.setName(cityName);
						}
					}
					// 城市ID
					if (!ele.elementText("cityCode").equals("")) {
						String cityCode = ele.elementText("cityCode");
						city.setElongcityid(cityCode);
						city.setSort(Integer.valueOf(cityCode));
						System.out.println("艺龙提供城市的id:" + city.getElongcityid());
						city.setProvinceid(province.getId());
						city.setLanguage(0);
						if(countryname.equals("中国")){
							city.setCountryid(168l);
						}else{
							List<Country> countryfromtable = Server.getInstance().getSystemService().findMapResultBySql(" SELECT ID FROM T_COUNTRY WHERE C_ZHNAME='" + countryname
									+ "'", null);
							if (countryfromtable.size() > 0) {
								Map map = (Map) countryfromtable.get(0);
								city.setCountryid(Long.parseLong(map.get("ID").toString()));
							}
						}
					}
					city.setType(1l);// 1--酒店
					List<City> cityList = Server.getInstance().getHotelService().findAllCity("WHERE " + City.COL_name + "='" + city.getName() + "' AND C_TYPE=1", "", -1, 0);
					if (cityList.size() > 0) {
						System.out.println("更新城市信息……");
						city.setId(cityList.get(0).getId());
						Server.getInstance().getHotelService().updateCityIgnoreNull(city);
					} else {
						System.out.println("新增城市信息……");
						city = Server.getInstance().getHotelService().createCity(city);
					}
					 // 行政区节点LOCATION列表
					if (!ele.elementText("districts").equals("")) {
						List list2 = ele.elements("districts");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Element element2 = (Element) iterator.next();
							if (!element2.elementText("location").equals("")) {
								List list3 = element2.elements("location");
								for (Iterator iterator2 = list3.iterator(); iterator2.hasNext();) {
									Region region = new Region();
									Element element3 = (Element) iterator2.next();
									// id
									if (!element3.elementText("id").equals("")) {
										region.setRegionid(element3.elementText("id"));
									}
									// 名称
									if (!element3.elementText("name").equals("")) {
										String name = element3.elementText("name");
										System.out.println("行政区域名字:" + name);
										region.setName(name);
										region.setCityid(city.getId());
										region.setLanguage(0);
										//1--商业区,2--行政区
										region.setType("2");
									}
									if(countryname.equals("中国")){
										region.setCountryid(168l);
									}else{
										List<Country> countryfromtable = Server.getInstance().getSystemService().findMapResultBySql(" SELECT ID FROM T_COUNTRY WHERE C_ZHNAME='" + countryname
												+ "'", null);
										if (countryfromtable.size() > 0) {
											Map map = (Map) countryfromtable.get(0);
											region.setCountryid(Long.parseLong(map.get("ID").toString()));
										}
									}
									List<Region> regionList = Server.getInstance().getHotelService().findAllRegion("WHERE " + Region.COL_cityid + "='" + region.getCityid()
											+ "' AND " + Region.COL_name + "='" + region.getName()+"'", "", -1, 0);
									if (regionList.size() > 0) {
										region.setId(regionList.get(0).getId());
										Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
									} else {
										region = Server.getInstance().getHotelService().createRegion(region);
									}
								}
							}
						}
					}
					// 商业区节点location列表
					if (!ele.elementText("commercialLocations").equals("")) {
						List list2 = ele.elements("commercialLocations");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Element element2 = (Element) iterator.next();
							if (!element2.elementText("location").equals("")) {
								List list3 = element2.elements("location");
								for (Iterator iterator2 = list3.iterator(); iterator2.hasNext();) {
									Region region = new Region();
									Element element3 = (Element) iterator2.next();
									// id
									if (!element3.elementText("id").equals("")) {
										region.setRegionid(element3.elementText("id"));
									}
									// 名称
									if (!element3.elementText("name").equals("")) {
										String name = element3.elementText("name");
										System.out.println("商业区域名字:" + name);
										region.setName(name);
										region.setCityid(city.getId());
										region.setLanguage(0);
										//1--商业区,2--行政区
										region.setType("1");
									}
									if(countryname.equals("中国")){
										region.setCountryid(168l);
									}else{
										List<Country> countryfromtable = Server.getInstance().getSystemService().findMapResultBySql(" SELECT ID FROM T_COUNTRY WHERE C_ZHNAME='" + countryname
												+ "'", null);
										if (countryfromtable.size() > 0) {
											Map map = (Map) countryfromtable.get(0);
											region.setCountryid(Long.parseLong(map.get("ID").toString()));
										}
									}
									List<Region> regionList = Server.getInstance().getHotelService().findAllRegion("where " + Region.COL_cityid + "='" + region.getCityid()
											+ "' and " + Region.COL_name + "='" + region.getName()+"'", "", -1, 0);
									if (regionList.size() > 0) {
										region.setId(regionList.get(0).getId());
										Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
									} else {
										region = Server.getInstance().getHotelService().createRegion(region);
									}
								}
							}
						}
					}
					// 城市标志物节点location列表
					if (!ele.elementText("LandmarkLocations").equals("")) {
						List list2 = ele.elements("LandmarkLocations");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Element element2 = (Element) iterator.next();
							if (!element2.elementText("location").equals("")) {
								List list3 = element2.elements("location");
								for (Iterator iterator2 = list3.iterator(); iterator2.hasNext();) {
									Landmark landmark = new Landmark();
									Element element3 = (Element) iterator2.next();
									if (!element3.elementText("id").equals("")) {
										landmark.setElongmarkid(element3.elementText("id"));
									}
									// 名称
									if (!element3.elementText("name").equals("")) {
										String name = element3.elementText("name");
										landmark.setName(name);
									}
									landmark.setCityid(city.getId());
									landmark.setLanguage(0);
									List<Landmark> landmarkList = Server.getInstance().getHotelService().findAllLandmark("where " + Landmark.COL_cityid + "='"
											+ landmark.getCityid() + "' and " + Landmark.COL_elongmarkid + "='" + landmark.getElongmarkid() + "'",
											"", -1, 0);
									if (landmarkList.size() > 0) {
										landmark.setId(landmarkList.get(0).getId());
										Server.getInstance().getHotelService().updateLandmarkIgnoreNull(landmark);
									} else {
										landmark = Server.getInstance().getHotelService().createLandmark(landmark);
									}
								}
							}
						}
					}
				}
			}
		}
		System.out.println("ok!!!");
	}
}
