package com.ccservice.elong.inter;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Calendar;
import com.ccservice.elong.base.NorthBoundAPIServiceStub;

/**
 * 酒店价格查询接口
 * 
 * @author 师卫林
 * 
 */
public class ELongGetHotelList {
	public static void main(String[] args) throws RemoteException, SQLException {
		String HotelName = "";
		String HotelId = "";
		String checkInDate="2012-5-7 00:00:00";
		String checkOutDate="2012-5-8 00:00:00";
		String CityId = "1901";
		// getHotelPrice(checkInDate, checkOutDate,HotelName, HotelId,CityId);
		getHotelPrice(CityId,HotelName, HotelId,checkInDate,checkOutDate );
	}

	public static NorthBoundAPIServiceStub.GetHotelListResponseE getHotelPrice(String cityId, String hotelName, String hotelId, String checkInDate,
			String checkOutDate) throws RemoteException, SQLException {
		NorthBoundAPIServiceStub stub = new NorthBoundAPIServiceStub();
		NorthBoundAPIServiceStub.GetHotelConditionForGetHotelList conditionForGetHotelList = new NorthBoundAPIServiceStub.GetHotelConditionForGetHotelList();
		NorthBoundAPIServiceStub.GetHotelListRequest request = new NorthBoundAPIServiceStub.GetHotelListRequest();
		NorthBoundAPIServiceStub.GetHotelListResponseE response = new NorthBoundAPIServiceStub.GetHotelListResponseE();
		NorthBoundAPIServiceStub.GetHotelList getList = new NorthBoundAPIServiceStub.GetHotelList();
		// 入住时间
		Calendar CheckInDate = DateSwitch.SwitchCalendar2(checkInDate);
		// 离开时间
		Calendar CheckOutDate = DateSwitch.SwitchCalendar2(checkOutDate);

		conditionForGetHotelList.setCheckInDate(CheckInDate);
		conditionForGetHotelList.setCheckOutDate(CheckOutDate);
		conditionForGetHotelList.setStartLongitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setStartLatitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setEndLatitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setEndLongitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setOpeningDate(DateSwitch.SwitchCalendar("0001-01-01 00:00:00"));
		conditionForGetHotelList.setDecorationDate(DateSwitch.SwitchCalendar("0001-01-01 00:00:00"));
		conditionForGetHotelList.setCityId(cityId);
		conditionForGetHotelList.setHotelId(hotelId);
		conditionForGetHotelList.setHotelName(hotelName);

		request.setRequestHead(ElongRequestHead.getRequestHead(""));
		request.setGetHotelCondition(conditionForGetHotelList);

		getList.setGetHotelListRequest(request);

		response = stub.getHotelList(getList);
		// System.out.println("结果代码:"+response.getGetHotelListResult().getResponseHead().getResultCode());
		// System.out.println("结果信息:"+response.getGetHotelListResult().getResponseHead().getResultMessage());
		NorthBoundAPIServiceStub.HotelForGetHotelList[] hotelLists=response.getGetHotelListResult().getHotels().getHotel();
		System.out.println(":::::::"+hotelLists.length);
		System.out.println("getHotelPrice执行中.....");
		if (hotelLists != null && hotelLists.length > 0) {
			System.out.println("getHotelPrice执行中!!!!");
			
			for (int i = 0; i < hotelLists.length; i++) {
				NorthBoundAPIServiceStub.HotelForGetHotelList hotelList = hotelLists[i];
				
				System.out.println("开始创建Hotel对象....");
				// 酒店名称
				System.out.println("酒店名称:" + hotelList.getHotelName());
				// 酒店ID
				System.out.println("酒店ID:"+hotelList.getHotelId());
				// 酒店最低价
				System.out.println("HotelInvStatusCode:"+hotelList.getHotelInvStatusCode());
				System.out.println("酒店最低价:"+hotelList.getLowestPrice());
			}
		}
		return response;
	}
	
}
