package com.ccservice.newelong.hoteldb;

import java.io.File;
import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.net.URLEncoder;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.dom4j.Element;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.dom4j.DocumentHelper;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.elong.inter.DownFile;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.country.Country;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 酒店省份、城市、行政区、商业区、地标
 * 艺龙2.0接口
 * @author WH
 */
public class NewGeoCNDB implements Job {
	public static void main(String[] args) throws Exception{
		long start = System.currentTimeMillis();
		System.out.println("Start Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
		//下载文件
		DownFile.download("http://api.elong.com/xml/v2.0/hotel/geo_cn.xml", "D:\\酒店数据\\geo_cn.xml");
		System.out.println("Start Analy Geo_Cn.xml");
		//解析
		read("D:\\酒店数据\\geo_cn.xml");
		System.out.println("End Analy Geo_Cn.xml");
		long end = System.currentTimeMillis();
		System.out.println("End Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			long start = System.currentTimeMillis();
			System.out.println("Start Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
			//下载文件
			DownFile.download("http://api.elong.com/xml/v2.0/hotel/geo_cn.xml", "D:\\酒店数据\\geo_cn.xml");
			System.out.println("Start Analy Geo_Cn.xml");
			//解析
			read("D:\\酒店数据\\geo_cn.xml");
			System.out.println("End Analy Geo_Cn.xml");
			long end = System.currentTimeMillis();
			System.out.println("End Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
			System.out.println("耗时:" + DateSwitch.showTime(end - start));
		} catch (Exception e) {
			
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void read(String filename) throws Exception {
		//获取XML
		SAXReader reader = new SAXReader();
		Document document = reader.read(new File(filename));
		String xml = document.asXML();
		//解析XML
		Document doc = DocumentHelper.parseText(xml.toString());
		Element root = doc.getRootElement();
		Element HotelGeoList = root.element("HotelGeoList");
		List<Element> HotelGeos = HotelGeoList.elements("HotelGeo");
		//遍历XML
		if(HotelGeos!=null && HotelGeos.size()>0){
			Map<String, City> cityMap = new HashMap<String, City>();//<艺龙城市ID,本地城市>
			List<String> countyLevelCityList = new ArrayList<String>();//县级城市 ProvinceId_CityCode_SuperiorCityName
			
			Map<String, Long> provinceMap = new HashMap<String, Long>();//<艺龙省份ID,本地省份ID>
			List<Province> localProvinces = Server.getInstance().getHotelService().findAllProvince("where c_type = 1", "", -1, 0);
			for(int i = 0 ; i < HotelGeos.size() ; i++){
				Element geo = HotelGeos.get(i);
				//国家
				String Country = geo.attributeValue("Country");
				//省份
				String ProvinceId = geo.attributeValue("ProvinceId");
				String ProvinceName = geo.attributeValue("ProvinceName");
				//城市
				String CityCode = geo.attributeValue("CityCode");
				String CityName = geo.attributeValue("CityName");
				if(UpdateElDataUtil.StringIsNull(Country) || 
						UpdateElDataUtil.StringIsNull(ProvinceId) || 
							UpdateElDataUtil.StringIsNull(ProvinceName) || 
								UpdateElDataUtil.StringIsNull(CityCode) ||
									UpdateElDataUtil.StringIsNull(CityName)){
					continue;
				}else{
					CityName = CityName.trim();
					ProvinceName = ProvinceName.trim();
				}
				//国家ID
				long countryid = 0 ;
				if("中国".equals(Country)){
					countryid = 168;
				}
				if("香港".equals(ProvinceName)){
					countryid = 244;
				}
				if("澳门".equals(ProvinceName)){
					countryid = 251;
				}
				if("台湾".equals(ProvinceName)){
					countryid = 257;
				}
				if(countryid==0){
					List<Country> localCountrys = Server.getInstance().getInterHotelService().findAllCountry("where c_zhname = '" + Country + "'", "", -1, 0);
					if(localCountrys!=null && localCountrys.size() > 0) {
						countryid = localCountrys.get(0).getId();
					}
				}
				//更新省份
				if(provinceMap.get(ProvinceId)==null || provinceMap.get(ProvinceId)<=0){
					Province province = new Province();
					province.setCode(ProvinceId);
					province.setName(ProvinceName);
					province.setLanguage(0);
					province.setType(1);// 1--酒店
					if(countryid>0){
						province.setCountryid(countryid);
					}
					if(localProvinces!=null && localProvinces.size()>0){
						for(Province local:localProvinces){
							if(ProvinceId.equals(local.getCode())){
								province.setId(local.getId());
								//更新
								if(!province.getName().equals(local.getName()) || 
											!province.getLanguage().equals(local.getLanguage()) ||
												!province.getType().equals(local.getType()) ||
													(countryid>0 && !province.getCountryid().equals(local.getCountryid()))){
									Server.getInstance().getHotelService().updateProvinceIgnoreNull(province);
								}
								break;
							}
						}
					}
					if(province.getId()==0){
						province = Server.getInstance().getHotelService().createProvince(province);
					}
					provinceMap.put(ProvinceId, province.getId());
				}
				//省份ID
				long provinceid = provinceMap.get(ProvinceId);
				//更新城市
				City city = new City();
				if (CityName.contains("（") && CityName.contains("）")) {
					city.setName(CityName.substring(0, CityName.lastIndexOf("（"))); //涪陵（重庆）
					//设置上级城市
					String superCityName = CityName.substring(CityName.lastIndexOf("（") + 1, CityName.lastIndexOf("）"));
					countyLevelCityList.add(ProvinceId + "@.@" + CityCode + "@.@" + superCityName);//ProvinceId_CityCode_SuperiorCityName
				} else {
					city.setName(CityName);
					if(("香港".equals(ProvinceName) || "澳门".equals(ProvinceName)) && !ProvinceName.equals(CityName)){
						countyLevelCityList.add(ProvinceId + "@.@" + CityCode + "@.@" + ProvinceName);
					}
				}
				city.setElongcityid(Integer.toString(Integer.parseInt(CityCode)));
				city.setSort(getCitySort(city.getName(),Integer.valueOf(CityCode)));
				city.setProvinceid(provinceid);
				city.setLanguage(0);
				if(countryid>0){
					city.setCountryid(countryid);
				}
				city.setType(1l);// 1--酒店
				//本地城市
				City localCity = new City();
				String citysql = "where c_type = 1 and (c_elongcityid = " + city.getElongcityid() + " or c_name = '" + city.getName() + "')";
				List<City> localCitys = Server.getInstance().getHotelService().findAllCity(citysql, "", -1, 0);
				if(localCitys!=null && localCitys.size()>0){
					//优先取ID、名称一致的
					for(City local:localCitys){
						if(city.getElongcityid().equals(local.getElongcityid()) && city.getName().equals(local.getName())){
							localCity = local;
							break;
						}
					}
					//ID一致或同一省份、名称一致
					if(city.getId()==0){
						for(City local:localCitys){
							if(city.getElongcityid().equals(local.getElongcityid()) || 
									(city.getName().equals(local.getName()) && city.getProvinceid().equals(local.getProvinceid()))){
								localCity = local;
								break;
							}
						}
					}
				}
				if(localCity.getId()==0){
					city = Server.getInstance().getHotelService().createCity(city);
					System.out.println("新增城市==========" + city.getName() + "==========" + city.getId());
				}else{
					city.setId(localCity.getId());
					if(localCity.getSort()!=null && localCity.getSort().intValue()>0) city.setSort(localCity.getSort());
					city.setSuperiorcity(localCity.getSuperiorcity());
					//更新
					if(!city.getName().equals(localCity.getName()) ||
									!city.getElongcityid().equals(localCity.getElongcityid()) ||
										!city.getSort().equals(localCity.getSort()) ||
											!city.getProvinceid().equals(localCity.getProvinceid()) ||
												!city.getLanguage().equals(localCity.getLanguage()) || 
													!city.getType().equals(localCity.getType()) ||
														(countryid>0 && !city.getCountryid().equals(localCity.getCountryid()))){
						Server.getInstance().getHotelService().updateCityIgnoreNull(city);
						System.out.println("更新城市==========" + city.getName() + "==========" + city.getId());
					}else{
						System.out.println("匹配城市==========" + city.getName() + "==========" + city.getId());
					}
				}
				if(cityMap.get(CityCode)==null || cityMap.get(CityCode).getId()<=0){
					cityMap.put(CityCode, city);
				}
				//行政区
				Element Districts = geo.element("Districts");
				if(Districts!=null){
					List<Element> DistrictsList = Districts.elements("Location");
					if(DistrictsList!=null && DistrictsList.size()>0){
						//本地该城市的行政区
						List<Region> localRegions = 
							Server.getInstance().getHotelService().findAllRegion("where c_type = '2' and c_cityid = " + city.getId(), "", -1, 0);
						for(Element Location:DistrictsList){
							String LocationId = Location.attributeValue("Id");
							String LocationName = Location.attributeValue("Name");
							if(UpdateElDataUtil.StringIsNull(LocationId) || UpdateElDataUtil.StringIsNull(LocationName)){
								continue;
							}
							Region region = new Region();
							region.setName(LocationName);
							region.setCityid(city.getId());
							region.setType("2");
							region.setRegionid(Integer.toString(Integer.parseInt(LocationId)));
							region.setLanguage(0);
							region.setCountryid(countryid);
							//本地行政区
							Region localRegion = new Region();
							if(localRegions!=null && localRegions.size()>0){
								for(Region r:localRegions){
									if(region.getName().equals(r.getName()) && region.getRegionid().equals(r.getRegionid())){
										localRegion = r;
										break;
									}
								}
								if(localRegion.getId()==0){
									for(Region r:localRegions){
										if(region.getRegionid().equals(r.getRegionid()) || region.getName().equals(r.getName())){
											localRegion = r;
											break;
										}
									}
								}
							}
							if(localRegion.getId()==0){
								region = Server.getInstance().getHotelService().createRegion(region);
								System.out.println("新增行政区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
							}else{
								region.setId(localRegion.getId());
								//更新
								if(!region.getName().equals(localRegion.getName()) ||
										!region.getRegionid().equals(localRegion.getRegionid()) ||
											!region.getLanguage().equals(localRegion.getLanguage()) ||
												!region.getCountryid().equals(localRegion.getCountryid())){
									Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
									System.out.println("更新行政区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
								}else{
									System.out.println("匹配行政区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
								}
							}
						}
					
					}
				}
				//商业区
				Element CommericalLocations = geo.element("CommericalLocations");
				if(CommericalLocations!=null){
					List<Element> CommericalLocationsList = CommericalLocations.elements("Location");
					if(CommericalLocationsList!=null && CommericalLocationsList.size()>0){
						//本地该城市的商业区
						List<Region> localRegions = 
							Server.getInstance().getHotelService().findAllRegion("where c_type = '1' and c_cityid = " + city.getId(), "", -1, 0);
						for(Element Location:CommericalLocationsList){
							String LocationId = Location.attributeValue("Id");
							String LocationName = Location.attributeValue("Name");
							if(UpdateElDataUtil.StringIsNull(LocationId) || UpdateElDataUtil.StringIsNull(LocationName)){
								continue;
							}
							Region region = new Region();
							region.setName(LocationName);
							region.setCityid(city.getId());
							region.setType("1");
							region.setRegionid(Integer.toString(Integer.parseInt(LocationId)));
							region.setLanguage(0);
							region.setCountryid(countryid);
							//本地商业区
							Region localRegion = new Region();
							if(localRegions!=null && localRegions.size()>0){
								for(Region r:localRegions){
									if(region.getName().equals(r.getName()) && region.getRegionid().equals(r.getRegionid())){
										localRegion = r;
										break;
									}
								}
								if(localRegion.getId()==0){
									for(Region r:localRegions){
										if(region.getRegionid().equals(r.getRegionid()) || region.getName().equals(r.getName())){
											localRegion = r;
											break;
										}
									}
								}
							}
							if(localRegion.getId()==0){
								region = Server.getInstance().getHotelService().createRegion(region);
								System.out.println("新增商业区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
							}else{
								region.setId(localRegion.getId());
								//更新
								if(!region.getName().equals(localRegion.getName()) ||
										!region.getRegionid().equals(localRegion.getRegionid()) ||
											!region.getLanguage().equals(localRegion.getLanguage()) ||
												!region.getCountryid().equals(localRegion.getCountryid())){
									Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
									System.out.println("更新商业区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
								}else{
									System.out.println("匹配商业区==========" + city.getName() + "==========" + region.getName() + "==========" + region.getId());
								}
							}
						}
					}
				}
				//地标
				Element LandmarkLocations = geo.element("LandmarkLocations");
				if(LandmarkLocations!=null){
					List<Element> LandmarkLocationsList = LandmarkLocations.elements("Location");
					if(LandmarkLocationsList!=null && LandmarkLocationsList.size()>0){
						//本地该城市的地标
						List<Landmark> localLandmarks = 
							Server.getInstance().getHotelService().findAllLandmark("where c_cityid = " + city.getId(), "", -1, 0);
						for(Element Location:LandmarkLocationsList){
							String LocationId = Location.attributeValue("Id");
							String LocationName = Location.attributeValue("Name");
							if(UpdateElDataUtil.StringIsNull(LocationId) || UpdateElDataUtil.StringIsNull(LocationName)){
								continue;
							}
							Landmark landmark = new Landmark();
							landmark.setName(LocationName);
							landmark.setCityid(city.getId());
							landmark.setLanguage(0);
							landmark.setElongmarkid(LocationId);
							//本地坐标
							Landmark localLandmark = new Landmark();
							if(localLandmarks!=null && localLandmarks.size()>0){
								for(Landmark l:localLandmarks){
									if(landmark.getName().equals(l.getName()) && landmark.getElongmarkid().equals(l.getElongmarkid())){
										localLandmark = l;
										break;
									}
								}
								if(localLandmark.getId()==0){
									for(Landmark l:localLandmarks){
										if(landmark.getName().equals(l.getName()) && landmark.getElongmarkid().equals(l.getElongmarkid())){
											localLandmark = l;
											break;
										}
									}
								}
							}
							if(localLandmark.getId()==0){
								landmark = Server.getInstance().getHotelService().createLandmark(landmark);
								System.out.println("新增地标==========" + city.getName() + "==========" + landmark.getName() + "==========" + landmark.getId());
							}else{
								landmark.setId(localLandmark.getId());
								if(!landmark.getName().equals(localLandmark.getName()) ||
										!landmark.getLanguage().equals(localLandmark.getLanguage()) ||
											!landmark.getElongmarkid().equals(localLandmark.getElongmarkid())){
									Server.getInstance().getHotelService().updateLandmarkIgnoreNull(landmark);
									System.out.println("更新地标==========" + city.getName() + "==========" + landmark.getName() + "==========" + landmark.getId());
								}else{
									System.out.println("匹配地标==========" + city.getName() + "==========" + landmark.getName() + "==========" + landmark.getId());
								}
							}
						}	
					}
				}
			}
			Map<String,String> SuperiorCityJson = new HashMap<String, String>();//上级城市名,艺龙返回JSON
			for(int i = 0 ; i < countyLevelCityList.size() ; i++){
				//ProvinceId_CityCode_SuperiorCityName
				String str = countyLevelCityList.get(i);
				String elProvinceId = str.split("@.@")[0];
				String elCityCode = str.split("@.@")[1];
				String SuperiorCityName = str.split("@.@")[2];
				//通过艺龙城市ID获取县级城市
				City countyLevelCity = cityMap.get(elCityCode);
				if(countyLevelCity!=null && countyLevelCity.getId()>0){
					Long superiorCityId = null;
					if("香港".equals(SuperiorCityName)){
						superiorCityId = cityMap.get("3201").getId();
					}else if("澳门".equals(SuperiorCityName)){
						superiorCityId = cityMap.get("3301").getId();
					}else{
						//请求艺龙
						String json = SuperiorCityJson.get(SuperiorCityName);
						if(ElongHotelInterfaceUtil.StringIsNull(json)){
							try {
								long currentTimeMillis = System.currentTimeMillis();
								String url = "http://hotel.elong.com/city/hotel/10.html?callback=jsonp"+currentTimeMillis+"&keyword="+URLEncoder.encode(SuperiorCityName, "utf-8");
								json = PHUtil.submitPost(url,"").toString();
								if(json.contains("ProvinceId") && json.contains("CityId") && 
										json.contains("CityNameCn") && json.contains("CityType")){
									SuperiorCityJson.put(SuperiorCityName, json);
								}
							} catch (Exception e) {
							}
						}
						if(!ElongHotelInterfaceUtil.StringIsNull(json) && 
								json.contains("ProvinceId") && json.contains("CityId") && 
									json.contains("CityNameCn") && json.contains("CityType")){
							json = json.substring(json.indexOf("["),json.indexOf("]")+1);
							JSONArray elCitys = JSONArray.fromObject(json);
							for(int j = 0 ; j < elCitys.size() ; j++){
								JSONObject elCity = elCitys.getJSONObject(j);
								String ProvinceId = elCity.getString("ProvinceId");
								String CityId = elCity.getString("CityId");
								String CityNameCn = elCity.getString("CityNameCn");
								String CityType = elCity.getString("CityType");
								//省份、城市名一样
								if(elProvinceId.equals(ProvinceId) && cityMap.get(CityId)!=null &&
										cityMap.get(CityId).getName().equals(CityNameCn) && "hotel".equals(CityType.toLowerCase())){
									superiorCityId = cityMap.get(CityId).getId();
									break;
								}
							}
						}
					}
					if(superiorCityId!=null && !superiorCityId.equals(countyLevelCity.getSuperiorcity())){
						Server.getInstance().getSystemService().findMapResultBySql("update T_CITY set C_SUPERIORCITY = " + superiorCityId + " where ID = " + countyLevelCity.getId(), null);
						System.out.println("更新上级城市==========" + countyLevelCity.getName() + "==========" + countyLevelCity.getId() + "==========" + superiorCityId);
					}
				}
			}
		}
	}
	/**城市排名*/
	private static int getCitySort(String cityname,int idx){
		int sort = 0;
		if("北京".equals(cityname)){
			sort = 1;
		}else if("上海".equals(cityname)){
			sort = 2;
		}else if("广州".equals(cityname)){
			sort = 3;
		}else if("深圳".equals(cityname)){
			sort = 4;
		}else if("杭州".equals(cityname)){
			sort = 5;
		}else if("苏州".equals(cityname)){
			sort = 6;
		}else if("宁波".equals(cityname)){
			sort = 7;
		}else if("南京".equals(cityname)){
			sort = 8;
		}else if("三亚".equals(cityname)){
			sort = 9;
		}else if("厦门".equals(cityname)){
			sort = 10;
		}else if("香港".equals(cityname)){
			sort = 11;
		}else if("武汉".equals(cityname)){
			sort = 12;
		}else if("成都".equals(cityname)){
			sort = 13;
		}else if("重庆".equals(cityname)){
			sort = 14;
		}else if("西安".equals(cityname)){
			sort = 15;
		}else if("昆明".equals(cityname)){
			sort = 16;
		}else if("青岛".equals(cityname)){
			sort = 17;
		}else if("大连".equals(cityname)){
			sort = 18;
		}else if("沈阳".equals(cityname)){
			sort = 19;
		}else if("天津".equals(cityname)){
			sort = 20;
		}else if("济南".equals(cityname)){
			sort = 21;
		}else if("长春".equals(cityname)){
			sort = 22;
		}else if("长沙".equals(cityname)){
			sort = 23;
		}else if("福州".equals(cityname)){
			sort = 24;
		}else if("郑州".equals(cityname)){
			sort = 25;
		}else if("哈尔滨".equals(cityname)){
			sort = 26;
		}else if("石家庄".equals(cityname)){
			sort = 27;
		}else if("东莞".equals(cityname)){
			sort = 28;
		}else if("无锡".equals(cityname)){
			sort = 29;
		}else if("烟台".equals(cityname)){
			sort = 30;
		}else if("太原".equals(cityname)){
			sort = 31;
		}else if("合肥".equals(cityname)){
			sort = 32;
		}else if("南昌".equals(cityname)){
			sort = 33;
		}else if("南宁".equals(cityname)){
			sort = 34;
		}else if("温州".equals(cityname)){
			sort = 35;
		}else if("淄博".equals(cityname)){
			sort = 36;
		}else if("呼和浩特".equals(cityname)){
			sort = 37;
		}else if("兰州".equals(cityname)){
			sort = 38;
		}else if("佛山".equals(cityname)){
			sort = 39;
		}else if("乌鲁木齐".equals(cityname)){
			sort = 40;
		}else if("贵阳".equals(cityname)){
			sort = 41;
		}else if("泉州".equals(cityname)){
			sort = 42;
		}else if("常州".equals(cityname)){
			sort = 43;
		}else if("南通".equals(cityname)){
			sort = 44;
		}else if("潍坊".equals(cityname)){
			sort = 45;
		}else if("唐山".equals(cityname)){
			sort = 46;
		}else if("海口".equals(cityname)){
			sort = 47;
		}else if("绍兴".equals(cityname)){
			sort = 48;
		}else if("大庆".equals(cityname)){
			sort = 49;
		}else if("邯郸".equals(cityname)){
			sort = 50;
		}else if("包头".equals(cityname)){
			sort = 51;
		}else if("台州".equals(cityname)){
			sort = 52;
		}else if("鞍山".equals(cityname)){
			sort = 53;
		}else if("中山".equals(cityname)){
			sort = 54;
		}else if("珠海".equals(cityname)){
			sort = 55;
		}else if("汕头".equals(cityname)){
			sort = 56;
		}else if("威海".equals(cityname)){
			sort = 57;
		}else if("泰安".equals(cityname)){
			sort = 58;
		}else if("吉林".equals(cityname)){
			sort = 59;
		}else if("柳州".equals(cityname)){
			sort = 60;
		}else if("东营".equals(cityname)){
			sort = 61;
		}else if("镇江".equals(cityname)){
			sort = 62;
		}else if("济宁".equals(cityname)){
			sort = 63;
		}else if("徐州".equals(cityname)){
			sort = 64;
		}else if("丹东".equals(cityname)){
			sort = 65;
		}else{
			sort = idx;
		}
		return sort;
	}
}
