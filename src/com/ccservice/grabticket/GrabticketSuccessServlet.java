package com.ccservice.grabticket;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.huamin.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
public class GrabticketSuccessServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String data = request.getParameter("data");
        JSONObject dataJson = JSONObject.parseObject(data);
        
        boolean success = dataJson.getBooleanValue("success");//下单是否成功
        String orderNumber = dataJson.getString("orderid");//采购商订单号
        
        JSONObject jsonObject = new JSONObject();
        // 抢票成功
        if (success) {
            try {
                int result = trainOrderOfflineDao.updateTrainOrderOfflineOrderStatus(orderNumber, 5);
                WriteLog.write("线上线下抢票接口对接", "------更新状态码------" + result);
                if (result == 1) {
                    jsonObject.put("isSuccess", true);
                    jsonObject.put("msg", "线上抢票成功订单状态更新成功");
                    WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新成功------" + orderNumber + "订单状态：" + 5);
                } else {
                    jsonObject.put("isSuccess", false);
                    jsonObject.put("msg", "线上抢票成功订单状态更新失败");
                    WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新失败------" + orderNumber + "订单状态：" + 5);
                }
            } catch (Exception e) {
                jsonObject.put("isSuccess", false);
                jsonObject.put("msg", "线上抢票成功订单状态更新失败");
                WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新失败------" + orderNumber);
            }
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(jsonObject.toJSONString().getBytes(Charset.forName("UTF-8")));
    }
}
