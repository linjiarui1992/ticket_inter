package com.ccservice.grabticket;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.grabticket.util.DateUtils;
import com.ccservice.grabticket.util.MD5Util;
import com.ccservice.huamin.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.util.httpclient.HttpsClientUtils;
public class GrabticketServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    
    private MailAddress mailAddress = new MailAddress();
    
    private List<TrainPassengerOffline> trainPassengerOfflineList = new ArrayList<TrainPassengerOffline>();
    
    private TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
    
    private TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
    
    private String search_url = "http://bespeak.kt.hangtian123.net/cn_interface/tcTrain";
    
    private Long outtime = 6000L;
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String orderId = request.getParameter("orderId");
        
        try {
            trainPassengerOfflineList = trainOrderOfflineDao.getTrainPassengerOffline(orderId);
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------乘客信息查询失败------");
        }
        try {
            mailAddress = trainOrderOfflineDao.getMailAddress(orderId);
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------邮寄信息查询失败------");
        }
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(Long.parseLong(orderId));
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------订单信息查询失败------");
        }
        try {
            trainTicketOffline = trainOrderOfflineDao.getTrainTicketOffline(orderId);
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------车次信息查询失败------");
        }
        JSONArray passengersArray = new JSONArray();
        WriteLog.write("线上线下抢票接口对接", "------乘客信息人数------" + trainPassengerOfflineList.size());
        for (TrainPassengerOffline trainPassengerOffline : trainPassengerOfflineList) {
            JSONObject passengers = new JSONObject();
            passengers.put("passportseno", trainPassengerOffline.getIdNumber());//证件号码
            String idType = trainPassengerOffline.getIdType().toString();
            passengers.put("passporttypeseid", idType);//证件类型id
            if ("1".equals(idType)) {
                passengers.put("passporttypeseidname", "二代身份证");//证件类型名称
            }  else if ("3".equals(idType)) {
                passengers.put("passporttypeseidname", "护照");//证件类型名称
            } else if ("4".equals(idType)) {
                passengers.put("passporttypeseidname", "港澳通行证");//证件类型名称
            } else if ("5".equals(idType)) {
                passengers.put("passporttypeseidname", "台湾通行证");//证件类型名称
            }
            passengers.put("passengersename", trainPassengerOffline.getName());//乘客姓名
            passengers.put("passengerid", trainPassengerOffline.getPassengerId());//乘客的顺序号
            String ticketType = trainTicketOffline.getTicketType().toString();
            passengers.put("piaotype", ticketType);//票种 ID
            if ("1".equals(ticketType)) {
                passengers.put("piaotypename", "成人票");//票种名称
            } else if ("2".equals(ticketType)) {
                passengers.put("piaotypename", "儿童票");//票种名称
            }
            WriteLog.write("线上线下抢票接口对接", passengers.toJSONString());
            passengersArray.add(passengers);
        }
        
        //查询帐号
        String partnerid = PropertyUtil.getValue("search_partnerid", "grabticket.properties");
        //查询key值
        String key = PropertyUtil.getValue("search_key", "grabticket.properties");
        String reqtime = DateUtils.dateFormat(new Date(), "yyyyMMddHHmmss");
        String method = "qiang_piao_order";
        //签名
        String sign = "";
        try {
            sign = MD5Util.MD5(partnerid + method + reqtime + MD5Util.MD5(key));
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------签名失败------");
        }
        
        JSONObject param = new JSONObject();
        param.put("partnerid", partnerid);//帐号
        param.put("method", method);//抢票下单
        param.put("reqtime", reqtime);//调用接口时间
        param.put("sign", sign);//加密校验
        param.put("qorderid", trainOrderOffline.getOrderNumber());//抢票订单号(唯一)
        param.put("callback_url", "");//占座回调地址
        param.put("qorder_type", 100);//抢票类型
        param.put("qorder_start_time", DateUtils.dateFormat(trainOrderOffline.getCreateTime(), "yyyy-MM-dd HH:mm"));//抢票任务开始时间
        String tradeNo = trainOrderOffline.getTradeNo();
        String qorder_end_time = tradeNo.substring(tradeNo.indexOf("抢票截止日期:")+7, tradeNo.indexOf("抢票截止日期:")+23);
        param.put("qorder_end_time", qorder_end_time);//抢票任务结束时间
        param.put("agree_offline", true);//是否接受线下抢票
        param.put("Address", mailAddress.getADDRESS());//邮寄地址
        param.put("Contacts", mailAddress.getMAILNAME());//联系人
        param.put("ContactNumber", mailAddress.getMAILTEL());//联系人电话
        param.put("from_station_code", "");//出发站三字码
        param.put("from_station_name", trainTicketOffline.getDeparture());//出发站
        param.put("to_station_code", "");//到达站三字码
        param.put("to_station_name", trainTicketOffline.getArrival());//到达站
        param.put("start_date", DateUtils.dateFormat(trainTicketOffline.getDepartTime(), "yyyy-MM-dd"));//出发日期
        param.put("start_begin_time", "00:00");//出发开始时间
        param.put("start_end_time", "24:00");//出发截止时间
        param.put("train_codes", trainTicketOffline.getTrainNo());//抢票的具体车次，以“ , ”隔开
        param.put("train_type", trainTicketOffline.getTrainNo().substring(0, 1));//车次类型，与具体车次对应
        param.put("seat_type", trainTicketOffline.getSeatType());//座位类型
        param.put("max_price", 0);//最大票价
        param.put("passengers", passengersArray);//乘客详细信息
        param.put("LoginName12306", "");//托管帐号
        param.put("LoginPassword12306", "");//托管密码
        param.put("hasseat", true);
        WriteLog.write("线上线下抢票接口对接", param.toJSONString());
        JSONObject jsonObject = new JSONObject();
        try {
            Map<String, String> listdata = new HashMap<>();
            listdata.put("jsonStr", param.toJSONString());
            String result = HttpsClientUtils.posthttpclientdata(search_url, listdata, outtime);
            WriteLog.write("线上线下抢票接口对接", result);
            JSONObject resultJson = JSONObject.parseObject(result);
            boolean success = resultJson.getBooleanValue("success");//抢票下单是否成功
            if (success) {
                String orderid = resultJson.getString("orderid");//抢票订单号
                WriteLog.write("线上线下抢票接口对接", "------线下抢票订单同步线上成功------" + orderid);
                
                try {
                    int returnCode = trainOrderOfflineDao.updateTrainOrderOfflineOrderStatus(orderid, 4);
                    WriteLog.write("线上线下抢票接口对接", "------更新状态码------" + returnCode);
                    if (returnCode == 1) {
                        jsonObject.put("isSuccess", true);
                        jsonObject.put("msg", "线上抢票成功订单状态更新成功");
                        WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新成功------" + orderid + "订单状态：" + 4);
                    } else {
                        jsonObject.put("isSuccess", false);
                        jsonObject.put("msg", "线上抢票成功订单状态更新失败");
                        WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新失败------" + orderid + "订单状态：" + 4);
                    }
                } catch (Exception e) {
                    jsonObject.put("isSuccess", false);
                    jsonObject.put("msg", "线上抢票成功订单状态更新失败");
                    WriteLog.write("线上线下抢票接口对接", "------线上抢票成功订单状态更新失败------" + orderid);
                }
                
            } else {
                jsonObject.put("isSuccess", false);
                jsonObject.put("msg", "线下抢票订单同步线上失败");
                String code = resultJson.getString("code");//下单结果代码
                String msg = resultJson.getString("msg");//下单结果信息
                WriteLog.write("线上线下抢票接口对接", "------线下抢票订单同步线上失败------" + code + ":" + msg);
            }
        } catch (Exception e) {
            jsonObject.put("isSuccess", false);
            jsonObject.put("msg", "线下抢票订单同步线上接口调用失败");
            WriteLog.write("线上线下抢票接口对接", "------线下抢票订单同步线上接口调用失败------");
        }
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(jsonObject.toJSONString().getBytes(Charset.forName("UTF-8")));
    }
}
